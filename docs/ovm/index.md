<!--#include virtual="/linux/layout/header" -->
# Oracle VM @ CERN
<p>
For official usage in the context of CERN/IT Oracle production databases there is a
limited number of Oracle VM licenses available at CERN. For all other tasks standard
CERN Linux distribution: <a href="/scientific/">Scientific Linux CERN</a> shall be used.
</p>
<h2>Licenses</h2>
<p>
All Oracle VM licenses are available only for IT/DB
managed systems in CERN Computer Center.
</p>
<p>
For more information please contact <a href="mailto:db.systems@cern.ch">db.systems@cern.ch</a>.
</p>
<h2>Installation</h2>
<p>
Registered systems holding valid Oracle VM license can be
installed using same methods as <a href="/scientific/">Scientific Linux CERN</a>
distributions. Please see <a href="/install/">Linux Installation at CERN</a>.
<p>
To list available Oracle VM PXE boot targets use: <i>aims2 showimg ORA\* [--full]</i>
<p>
Available Oracle VM installation paths:
<ul>
 <li>Oracle VM 2.1.5: <i>https://linuxsoft.cern.ch/oracle/vmserver/2.1.5/i386/</i>
 <li>Oracle VM 2.2:   <i>https://linuxsoft.cern.ch/oracle/vmserver/2.2/i386/</i>
 <li>Oracle VM 2.2.1: <i>https://linuxsoft.cern.ch/oracle/vmserver/2.2.1/i386/</i>
 <li>Oracle VM 2.2.2: <i>https://linuxsoft.cern.ch/oracle/vmserver/2.2.2/i386/</i>
 <li>Oracle VM 3.0.2: <i>https://linuxsoft.cern.ch/oracle/vmserver/3.0.2/x86_64/</i>
 <li>Oracle VM 3.0.3: <i>https://linuxsoft.cern.ch/oracle/vmserver/3.0.3/x86_64/</i>
 <li>Oracle VM 3.1.1: <i>https://linuxsoft.cern.ch/oracle/vmserver/3.1.1/x86_64/</i>
</ul>

<h2>Software Repositories</h2>
<p>
Software repositories described below contain mirror of Oracle VM binary packages
published by Oracle via Oracle Unbreakable Linux Network.
Access to the repositories is limited to registered systems holding valid Oracle VM
license.
</p>
<ul>
<li><a href="#ovm2">Oracle VM 2 (ovm2)</a>
<li><a href="#ovm3">Oracle VM 3 (ovm3)</a>
</ul>


<hr>
<a name="ovm2"></a><h4>Oracle VM 2 (ovm2)</h4>

In order to use these repositories on your system, please edit (create):
<pre>
/etc/yum.repos.d/ovm2.repo
</pre>
file by downloading this example <a href="ovm2/ovm2.repo">ovm2.repo</a>.
<p>

To update your system run as root:
<pre>
&#35; /usr/bin/yum update
</pre>

<hr>
<a name="ovm3"></a><h4>Oracle VM 3 (ovm3)</h4>

In order to use these repositories on your system, please edit (create):
<pre>
/etc/yum.repos.d/ovm3.repo
</pre>
file by downloading this example <a href="ovm3/ovm3.repo">ovm3.repo</a>.
<p>

To update your system run as root:
<pre>
&#35; /usr/bin/yum update
</pre>


