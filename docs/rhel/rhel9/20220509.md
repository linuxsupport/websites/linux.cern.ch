#### RHEL 9.0

* Installation target: **RHEL_9_0_X86_64**
* Installation path:   **http://linuxsoft.cern.ch/cern/rhel/9.0/baseos/x86_64/**
* Release notes:       **[RELEASE-NOTES-9.0-x86_64](Red_Hat_Enterprise_Linux-9-9.0_Release_Notes-en-US.pdf)**
