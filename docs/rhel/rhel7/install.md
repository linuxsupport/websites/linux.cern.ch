# Installing RHEL7

## OpenStack Images

The recommended way of installing RHEL7 is by using the following OpenStack image (or a newer one):

* `RHEL7 - x86_64 [2022-03-01]`

## AIMS boot targets

If installing via PXE boot, you can use the following AIMS target (or a newer one):

* `RHEL_7_9_X86_64`

## Software repositories

Install software repository definitions on your previously-installed non-CERN RHEL8 system, by running as root:

```
# wget http://cern.ch/linux/rhel/repofiles/rhel7.repo -O /etc/yum.repos.d/rhel.repo
```

You may also want to consider adding the 'CERN' repository

```
# curl -o /etc/yum.repos.d/rhel7-cern.repo https://linux.web.cern.ch/rhel/repofiles/rhel7-cern.repo
# curl -o /etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2 https://linuxsoft.cern.ch/internal/repos/RPM-GPG-KEY-kojiv2
```

Direct download: [rhel7.repo](/rhel/repofiles/rhel7.repo).

To update your system run as root:

```
# /usr/bin/yum update
```
