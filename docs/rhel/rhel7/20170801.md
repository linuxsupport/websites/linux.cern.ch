#### RHEL 7.4

* Installation target: **RHEL_7_4_X86_64**
* Installation path:   **http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.4/x86_64/**
* Release notes:       **[RELEASE-NOTES-7.4-x86_64](Red_Hat_Enterprise_Linux-7-7.4_Release_Notes-en-US.pdf)**
