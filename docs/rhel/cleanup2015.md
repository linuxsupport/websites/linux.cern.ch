<!--#include virtual="/linux/layout/header" -->
# Red Hat Enterprise Linux @ CERN: repositories cleanup 2015

Red Hat is transitioning from 'Red Hat Network' (RHN) to 'Red Hat Subscription Management' (RHSM)
(<a href="https://access.redhat.com/rhn-to-rhsm">RHM to RHSM</a>) changing
software repositories layout and naming.
<p>
In order to follow this transition we are changing repository structure
of RHEL repositories hosted on linuxsoft.cern.ch, performing a cleanup of
obsolete and unused RHEL versions on <em>Monday 11th of May 2015</em>.
<p>
Only following RHEL versions will be available after above mentioned date:
<ul>
<li>RHEL 7: 7.1, 7.0
<li>RHEL 6: 6.6, 6.5
<li>RHEL 5: 5.11, 5.10
</ul>

Please change your AIMS2 installation targets / installation paths and
software repository definitions according to information at: <a href="index">RHEL @ CERN</a>
<em>before 11.05.2015</em>.
<p>
New repositories / AIMS2 targets / installation paths and yum snaphosts
are available as of <b>Wednesday 8th of April 2015</b>. Until 11th of May 2015 both
old and new structure will be available in parallel.

<p>
On <b>Monday 11th of May 2015</b> following will be removed:

<ul>
<li>AIMS2 installation targets:
<pre>
RHEL7_U1_X86_64     (renamed)
RHEL7_U0_X86_64     (renamed)
RHEL6_U6_I386       (renamed)
RHEL6_U6_X86_64     (renamed)
RHEL6_U5_I386       (renamed)
RHEL6_U5_X86_64     (renamed)
RHEL6_U4_I386
RHEL6_U4_I386_BETA
RHEL6_U4_X86_64
RHEL6_U4_X86_64_BETA
RHEL6_U3_I386
RHEL6_U3_X86_64
RHEL6_U2_I386
RHEL6_U2_X86_64
RHEL6_U1_I386
RHEL6_U1_X86_64
RHEL6_U0_I386
RHEL6_U0_X86_64
RHES_5_U11_I386     (renamed)
RHES_5_U11_I386_XEN
RHES_5_U11_X86_64   (renamed)
RHES_5_U11_X86_64_XEN
RHES_5_U10_I386     (renamed)
RHES_5_U10_I386_XEN
RHES_5_U10_X86_64   (renamed)
RHES_5_U10_X86_64_XEN
RHES_5_U9_I386
RHES_5_U9_I386_XEN
RHES_5_U9_X86_64
RHES_5_U9_X86_64_XEN
RHES_5_U9_I386_BETA
RHES_5_U9_X86_64_BETA
RHES_5_U8_I386
RHES_5_U8_I386_XEN
RHES_5_U8_X86_64
RHES_5_U8_X86_64_XEN
RHES_5_U7_I386
RHES_5_U7_I386_XEN
RHES_5_U7_X86_64

</pre>
<li>RHEL installation paths/versions:
<pre>
http://linuxsoft.cern.ch/enterprise/7Server_U1/.. (renamed)
http://linuxsoft.cern.ch/enterprise/7Server_U0/.. (renamed)
http://linuxsoft.cern.ch/enterprise/7BETA/..
http://linuxsoft.cern.ch/enterprise/7RC/..
http://linuxsoft.cern.ch/enterprise/7Server/..
http://linuxsoft.cern.ch/enterprise/6Server_U6/.. (renamed)
http://linuxsoft.cern.ch/enterprise/6Server_U5/.. (renamed)
http://linuxsoft.cern.ch/enterprise/6Server_U4/..
http://linuxsoft.cern.ch/enterprise/6Server_U4_BETA/..
http://linuxsoft.cern.ch/enterprise/6Server_U3/..
http://linuxsoft.cern.ch/enterprise/6Server_U2/..
http://linuxsoft.cern.ch/enterprise/6Server_U1/..
http://linuxsoft.cern.ch/enterprise/6Server_U0/..
http://linuxsoft.cern.ch/enterprise/6Server/..
http://linuxsoft.cern.ch/enterprise/5Server_U11/.. (renamed)
http://linuxsoft.cern.ch/enterprise/5Server_U10/.. (renamed)
http://linuxsoft.cern.ch/enterprise/5Server_U9_BETA/..
http://linuxsoft.cern.ch/enterprise/5Server_U9/..
http://linuxsoft.cern.ch/enterprise/5Server_U8/..
http://linuxsoft.cern.ch/enterprise/5Server_U7/..
http://linuxsoft.cern.ch/enterprise/5Server_U6/..
http://linuxsoft.cern.ch/enterprise/5Server_U5/..
http://linuxsoft.cern.ch/enterprise/5Server_U4/..
http://linuxsoft.cern.ch/enterprise/5Server_U3/..
http://linuxsoft.cern.ch/enterprise/5Server_U2/..
http://linuxsoft.cern.ch/enterprise/5Server_U1/..
http://linuxsoft.cern.ch/enterprise/5Server_U0/..
http://linuxsoft.cern.ch/enterprise/5Server/..
</pre>
<li>RHEL software repositories (all repositories are renamed):
<pre>
http://linuxsoft.cern.ch/enterprise/7Server/...
http://linuxsoft.cern.ch/enterprise/6Server/...
http://linuxsoft.cern.ch/enterprise/5Server/...
http://linuxsoft.cern.ch/enterprise/updates/7Server/...
http://linuxsoft.cern.ch/enterprise/updates/6Server/...
http://linuxsoft.cern.ch/enterprise/updates/5Server/...
http://linuxsoft.cern.ch/rhel/rhel5server-i386/..
http://linuxsoft.cern.ch/rhel/rhel5server-x86_64/..
http://linuxsoft.cern.ch/rhel/rhel5server-ia64/..
http://linuxsoft.cern.ch/rhel/rhel6server-i386/..
http://linuxsoft.cern.ch/rhel/rhel6server-x86_64/..
http://linuxsoft.cern.ch/rhel/rhel7-beta-x86_64/.. (removed)
http://linuxsoft.cern.ch/rhel/rhel7-rc-x86_64/.. (removed)





</pre>
<li>RHEL AFS repository (all repositories to be removed)
<pre>
/afs/cern.ch/project/linux/redhat/..
/afs/cern.ch/project/linux/enterprise/..
</pre>
</ul>



