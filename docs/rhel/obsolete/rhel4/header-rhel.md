<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <script language="javascript" type="text/javascript">
     function search_clear() {
        document.searchform.qt.value= ""
	   }
 </script>
  <link rel="SHORTCUT ICON" href="http://cern.ch/linux/images/favicon.ico">
  <link rel="STYLESHEET" type="text/css" href="/linux/rhel/rhel.css">
  <title><!--#echo var="DOCUMENT_URI" --> (Red Hat Enterprise Linux (RHEL) pages)</title>
  <meta name="author" content="linux.support@cern.ch"> 
  <META NAME="Keywords" CONTENT="linux support Oracle CERN">
  <META NAME="Description" CONTENT="CERN linux support">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
</head>
<body>                          
<table width="100%" cellspacing="2" cellpadding="0" border="0">
 <tr>
 <td colspan="3">
 <table class="header" border="0" cellspacing="0" cellpadding="2" width="100%">
  <tr>
  <td align="left" valign="middle" width="90">&nbsp;&nbsp;</td> 
  <td align="center" valign="middle"><a class="header" href="http://www.cern.ch/">CERN</a>&nbsp;&nbsp;</td>
  <td align="center" valign="middle"><a class="header" href="http://www.cern.ch/it">IT&nbsp;Department</a>&nbsp;&nbsp;</td>
  <td align="center" valign="middle"><a class="header" href="http://www.cern.ch/it-div-fio">FIO&nbsp;Group</a>&nbsp;&nbsp;</td>
  <td align="center" valign="middle"><a class="header" href="http://cern.ch/linux/">CERN&nbsp;Linux&nbsp;Home</a></td>  
  <td align="center" valign="middle"><a class="header" href="http://cern.ch/linux/documentation/">Linux&nbsp;Documentation</a></td>   
</td>
  </tr>
  </table>
 </tr> 
 <tr>
  <td align="left" valign="middle" width="90"><img src="/linux/images/CERNlinux.gif" border="0" width="72" height="72" alt="CERN Tux"></td>
  <td align="center" valign="middle"><h1 class="header">&nbsp;&nbsp;Red&nbsp;Hat&nbsp;Enterprise&nbsp;Linux&nbsp;at&nbsp;CERN&nbsp;</h1>
  <td align="center" valign="middle"> 
   <FORM NAME="searchform" METHOD="GET" ACTION="http://www.cern.ch/cgi-bin/directory-search.pl">
    <INPUT NAME="SubSearch" TYPE="hidden" value="Yes">
    <INPUT NAME="Referer" TYPE="hidden" value="http://linux.web.cern.ch/linux/rhel/">
    <INPUT NAME="TemplateFile" TYPE="hidden" value="http://linux.web.cern.ch/linux/rhel/searchresults">
    <font STYLE="font-weight: lighter; font-family: sans-serif; font-size: 8pt;">Search&nbsp;RHEL&nbsp;pages&nbsp;<br>
    <INPUT NAME="qt" TYPE="text" SIZE="20" value="type in search terms" onFocus="javascript:search_clear();">
    </font>
    <INPUT NAME="NoForm" value ="Yes" TYPE="hidden">
   </FORM>
  </td>
 </tr>
 <tr>
 <td colspan="3">
  <table class="header" cellspacing="2" cellpadding="0" cellspacing="0" border="0" width="100%" >
 <tr>
    <td align="left" valign="middle" width="90">&nbsp;&nbsp</td>
    <td align="center" valign="middle"><a class="header" href="http://cern.ch/linux/scientific4/docs/">Documentation</a></td>
    <td align="center" valign="middle"><a class="header" href="http://cern.ch/linux/scientific4/certification/">Certification</a></td>
    <td align="center" valign="middle"><a class="header" href="http://cern.ch/linux/scientific4/">Scientific&nbsp;Linux&nbsp;CERN&nbsp;4&nbsp;Home</a></td>
    <td align="center" valign="middle">&nbsp;&nbsp;<a class="header" href="http://cern.ch/linux/install/">Linux&nbsp;Installation</a></td>
    <td align="center" valign="middle">&nbsp;&nbsp;<a class="header" href="http://cern.ch/linux/updates/">Updates/Software</a></td>
 </tr>
</table> 
 </td> 
 </tr>
</table>

<br>


<!-- end of ssi header -->
