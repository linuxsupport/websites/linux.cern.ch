<!-- start of footer -->
<!--#config timefmt="%a %b %e %T %Y" -->
<p>
<table border="0" cellpadding="4" cellspacing="0" width="100%" class="footer">
<tr>
<td align="left" valign="middle" class="footer">
<font size="-1">Last&nbsp;modification:&nbsp;<!--#echo var="LAST_MODIFIED" -->&nbsp;&nbsp;</font>
</td>
<td align="right" valign="middle" class="footer">
<a class="footer" href="mailto:linux.support@cern.ch">linux.support@cern.ch</a>
</td>
</tr>
</table>
<!-- google analytics test, Jarek Nov 2007 -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-3015287-1");
pageTracker._trackPageview();
} catch(err) {}</script>
<!--
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-3015287-1"; 
urchinTracker();
</script>
-->
</body>
</html>
