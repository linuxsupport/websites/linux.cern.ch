# Red Hat Enterprise Linux (RHEL) @ CERN

## License status

As of March 2022, CERN currently holds an academic site-license which allows for certain usage of RHEL on the CERN site.

!!! danger "End of Site License"
    Our site license ends on **May 31, 2029**, before the currently-scheduled start of Run 4. After this date, there is no guarantee that we will have another license that will allow you to continue to use Red Hat Enterprise Linux. It is not recommended to use RHEL for any usecase where you will not be able to reinstall completely (migration-in-place will not be supported) and migrate to another operating system before this date.

In addition to the above, CERN also pays for a limited number of full licenses which by default come with full support. These licenses are used for hosts that require a supported operating system as part of their own licensing conditions (Oracle database, etc).

For more information and please contact [linux.support@cern.ch](mailto:linux.support@cern.ch).

## Red Hat Enterprise Linux versions at CERN?

For the 9 family: **Red Hat Enterprise Linux 9 (RHEL9)** is provided by using the upstream content. Integration to the CERN computing environment is still possible via the addon 'CERN' repository

* RHEL9 support will end on 31.05.2032
* More information on RHEL9 can be found on the dedicated [RHEL9](/rhel/rhel9/install/) page

For the 8 family: **Red Hat Enterprise Linux 8 (RHEL8)** is provided by using the upstream content. Integration to the CERN computing environment is still possible via the addon 'CERN' repository

* RHEL8 support will end on 31.05.2029
* More information on RHEL8 can be found on the dedicated [RHEL8](/rhel/rhel8/install/) page

## Which Red Hat Enterprise Linux version should I use?

We recommend all users to use RHEL9 as this version has a longer product life.

## Can I use Red Hat Enterprise Linux from outside CERN?

Our site license only covers usage of RHEL on the CERN site. Unless you have a valid license for your own site, you cannot use RHEL. You are also
not allowed to redistribute any Red Hat software to anybody that doesn't hold a valid license (ie. anybody outside CERN), doing so is a serious
violation of the [CERN Computing Rules (Operational Circular #5)](http://cern.ch/ComputingRules).

As part of the CERN's academic site license, we negogiated special conditions for HEP sites as part of a "Extended Research Network" (ERN)
whereby sites affiliated with CERN for "research" can get up to 1000 RHEL licenses for zero cost, to be used for research purposes only.

!!! note ""
    Research: servers exclusively doing computations, analysing and storing research data. HPC, Big data.
    Non-research: computing capacity required to run organisation’s backend systems such as email servers, file servers storing various documents, HR, ERP, backup servers, etc.

Interested sites need a commercial agreement with Red Hat (although one can be created at the time of interest) and an "introduction" through
CERN (requested via [ServiceNOW](https://cern.service-now.com/service-portal?id=sc_cat_item&name=incident-linux&se=linux-desktop)). Larger HEP
sites can also apply directly to Red Hat for a Site License agreement of their own.

## Installation

### OpenStack images (VM and Ironic bare-metal)

OpenStack RHEL images (VM and Ironic bare-metal) are available as public images within [openstack.cern.ch](https://openstack.cern.ch)

### Container images (docker / podman)

The RHEL end user license agreement (EULA) does not permit the distribution of RHEL content, and as such running RHEL containers at sites that do not hold a RHEL subscription is a breach of the Red Hat license agreement.

What can be used in lieu of RHEL for containers is the "Red Hat Universal Base Image (UBI)". UBI has a different EULA which permits unrestricted distribution of content.

!!! danger "Do not add standard RHEL repos to UBI docker images"
    UBI repositories have a limited subset of packages and you may find that you're missing some dependencies. If that happens, note that you **cannot** add standard RHEL repos to the UBI image, as that would be a violation of the RHEL license agreement. If you are missing dependencies in the UBI images, you should use [AlmaLinux docker images](/almalinux/#container-images-docker-podman) instead.

Within CERN, we have UBI images mirrored from Red Hat to gitlab-registry.cern.ch:

- For [UBI7](https://gitlab.cern.ch/linuxsupport/ubi7/)
- For [UBI8](https://gitlab.cern.ch/linuxsupport/ubi8/)
- For [UBI9](https://gitlab.cern.ch/linuxsupport/ubi9/)

The UBI RPM repository is also mirrored locally to [http://linuxsoft.cern.ch/cdn-ubi.redhat.com](http://linuxsoft.cern.ch/cdn-ubi.redhat.com).

### AIMS PXE boot

Red Hat Enterprise Linux can also be installed via network boot, using same methods as [CentOS](/centos/) distributions.

To avoid getting a popup from the `subscription-manager` plugin prompting you to register your system to receive software updates, make sure that plugin is disabled within `dnf`:
<pre>
&#35; cat  /etc/dnf/plugins/subscription-manager.conf
[main]
enabled=0
</pre>

Please see [PXE Network boot](/installation/pxeboot/) and [AIMS2 client](/installation/aims/aims2client/) for more information.


## Other versions/products

In addition to the main 'Enterprise Linux' product, CERN has a limited number of licences for additional Red Hat content.

Please contact the linux team if you have a use-case to use any of the below software products

 * [Red Hat Enterprise Virtualization (RHEV)](#rhev)
 * [Red Hat JBoss Enterprise Application Platform (JB-EAP)](#jb-eap)
 * [Red Hat MRG - Realtime (RHMRG)](#rhmrg)
 * [Red Hat Enterprise Linux Extended Lifecycle Support (RHELS)](#rhels)
 * [Red Hat Enterprise Linux Extended Update Support (RHEUS)](#rheus)

## Additional information
 * [Locmap](/rhel/locmap/)
 * [Software installation and updates](#yum)
 * [Repository snapshots](#yumsnapshots)
 * [Documentation](#documentation)


<a name="jb-eap"></a>
## JBoss Enterprise Application Platform (JB-EAP)

Red Hat JBoss Enterprise Application Platform is a 'layered product': repositories listed below are to be installed in addition to base system (RHEL) repositories.

#### Software repositories for RHEL 7 (version 7)

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel7-jp-eap.repo -O /etc/yum.repos.d/rhel-jb-eap.repo
</pre>

Direct download: [rhel7-jb-eap.repo](repofiles/rhel7-jb-eap.repo)

#### Software repositories for RHEL 7 (version 7.1)

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel7-jp-eap-7.1.repo -O /etc/yum.repos.d/rhel-jb-eap-7.1.repo
</pre>

Direct download: [rhel7-jb-eap-7.1.repo](repofiles/rhel7-jb-eap-7.1.repo)

---

<a name="rhev"></a>
## Red Hat Enterprise Virtualization (RHEV)

Red Hat Enterprise Virtualization is a 'layered product': repositories listed below are to be installed in addition to base system (RHEL) repositories.

#### Available versions

##### RHEV 4.2

Installation ISOs and appliance images available at: **http://linuxsoft.cern.ch/enterprise/rhel/rhev/4.2/**

##### RHEV 4.3

Installation ISOs and appliance images available at: **http://linuxsoft.cern.ch/enterprise/rhel/rhev/4.3/**

**Note:** access to RHEV requires additional (separate) license for the system.

#### Software repositories for RHEL 7

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel7-ev.repo -O /etc/yum.repos.d/rhel-ev.repo
</pre>

Direct download: [rhel7-ev.repo](repofiles/rhel7-ev.repo)

---

<a name="rhmrg"></a>
## Red Hat Messaging Realtime Grid - Realtime (RHMRG)

Red Hat Messaging Realtime Grid is a 'layered product': repositories listed below are to be installed in addition to base system (RHEL) repositories.

**Note:** access to RHMRG requires additional (separate) license for the system.

Only 'Realtime' component is available (no 'Grid' / no 'Messaging').

#### Software repositories for RHEL 8

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel8-rt.repo -O /etc/yum.repos.d/rhel-rt.repo
</pre>

Direct download: [rhel-rt.repo](repofiles/rhel8-rt.repo)

#### Software repositories for RHEL 7

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel7-rt.repo -O /etc/yum.repos.d/rhel-rt.repo
</pre>

Direct download: [rhel-rt.repo](repofiles/rhel7-rt.repo)

#### Software repositories for RHEL 6

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel6-realtime.repo -O /etc/yum.repos.d/rhel-realtime.repo
</pre>

Direct download: [rhel6-realtime.repo](repofiles/rhel6-realtime.repo) **Note:** Available only for **x86_64**

---

<a name="rhels"></a>
## Red Hat Enterprise Linux Extended Lifecycle Support (RHELS)

Red Hat Enterprise Linux Extended Lifecycle Support is a product allowing extension of standard Red Hat Enterprise Linux Life Cycle

**Note:** access to RHELS requires additional (separate) license for the system.

#### Software repositories for RHEL 6 ELS

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel6-els.repo -O /etc/yum.repos.d/rhel-els.repo
</pre>

Direct download: [rhel6-els.repo](repofiles/rhel6-els.repo)


<a name="rheus"></a>
## Red Hat Enterprise Linux Extended Update Support (RHEUS)

Red Hat Enterprise Linux Extended Support is a product allowing extension of standard Red Hat Enteprise Linux Life Cycle for specific minor releases. Repositories listed below replace base system (RHEL) repositories.

**Note:** access to RHEUS requires additional (separate) license for the system.

---

<a name="yum"></a>
## Software installation and updates


We maintain a local mirror (updated 4 times per day) of Red Hat Enterprise Linux software repositories, and all RHEL systems at CERN are installing/updating from this mirror only.

Red Hat 'Subscription Manager' and 'Red Hat Network' are not used.

In order to update a RHEL system installed at CERN please run as root:

<pre>
&#35; /usr/bin/yum update
</pre>

Or if only security updates are to be applied: install _yum-plugin-security_

<pre>
&#35; /usr/bin/yum install yum-plugin-security
</pre>

and apply security errata by running:

<pre>
&#35;  /usr/bin/yum --security update
</pre>

**Note:** above command will install latest versions of package providing at least one security errata: non-security errata will be installed if more recent than a security one.

In order to install ONLY security errata, please run:

<pre>
&#35; yum --security update-minimal
</pre>

---
<a name="yumsnapshots"></a>
## Repository Snapshots

We maintain daily snapshots of RHEL software repositories. These snapshots can be used to reinstall a system to a known past state.

Please note that this degree of flexibility is only needed in specific cases: for most systems, the standard `yum update` (with the security plugin) should be sufficient. RHEL is a stable enterprise-class Linux distribution that evolves slowly.

To use yum repository snapshots, edit the following file based on your distribution:

- **For RHEL:** `/etc/dnf/vars/cernrhel`
- **For AlmaLinux:** `/etc/dnf/vars/cernalmalinux`

In that file, replace the line that reads `9` with a single line that reads `9-snapshots/20241021`
(if, for example, you want to downgrade to the snapshot dated the 21st of October 2024).

Once the file is edited, execute the following commands as `root`:

```bash
dnf clean all    # Clear old metadata to force DNF to fetch fresh data from snapshot repo
dnf distro-sync  # List the packages that need to be updated or downgraded
dnf distro-sync -y  # Proceed with synchronising the packages after reviewing the list
dnf check-update  # Verify that all packages are consistent with the snapshot repository
```

- **Note**: Daily snapshots of yum repositories are retained for **12 months**
- **Note**: Snapshots are immutable by design. If you encounter a problem with a snapshot (e.g., corrupted repository metadata or conflicting packages), the issue cannot be fixed. In such cases, you will need to use a snapshot from a different date.

<a name="documentation"></a>
## Documentation

Upstream RHEL related documentation can be found on Red Hat site:

*   [Red Hat Enterprise Linux](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/) documentation.
*   [Red Hat Enterprise Linux for Real Time](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux_for_Real_Time/) documentation.
*   [Red Hat Enterprise MRG](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_MRG/) documentation.
*   [Red Hat Enteprise Virtualization](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Virtualization/) documentation.
