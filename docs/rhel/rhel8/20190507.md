#### RHEL 8.0

* Installation target: **RHEL_8_0_X86_64**
* Installation path:   **http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.0/x86_64/**
* Release notes:       **[RELEASE-NOTES-8.0-x86_64](Red_Hat_Enterprise_Linux-8-8.0_Release_Notes-en-US.pdf)**
