# RHEL 8 - Install instructions

The following methods are currently available to install RHEL 8

* AIMS interactive - select the menu item `Install Red Hat Enterprise Linux 8 (RHEL8) x86_64` or `Install Red Hat Enterprise Linux 8 (RHEL8) aarch64`
* AIMS host registration - define your host to use the `RHEL8_X86_64` or `RHEL8_AARCH64`
* OpenStack VM - boot from the image `RHEL8 - x86_64` or `RHEL8 - aarch64`
* ai-bs puppet managed VM - use `ai-bs --rhel8`

For a detailed installation of a Linux Desktop, check the [step by step guide](/rhel/rhel8/stepbystep)

## Software repositories

Install software repository definitions on a RHEL8 previously-installation done without using our kickstart/image, by running as root:

```
# curl -o /etc/yum.repos.d/rhel8.repo https://linux.web.cern.ch/rhel/repofiles/rhel8.repo
```

You may also want to consider adding the 'CERN' repository

```
# dnf --repofrompath='tmpcern,https://linuxsoft.cern.ch/cern/rhel/$releasever/CERN/$basearch/' install redhat-release --nogpgcheck
```

Direct download: [rhel8.repo](/rhel/repofiles/rhel8.repo) and [rhel8-cern.repo](/rhel/repofiles/rhel8-cern.repo).
