#### RHEL 8.5

* Installation target: **RHEL_8_5_X86_64**
* Installation path:   **http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.5/x86_64/**
* Release notes:       **[RELEASE-NOTES-8.5-x86_64](Red_Hat_Enterprise_Linux-8-8.5_Release_Notes-en-US.pdf)**
