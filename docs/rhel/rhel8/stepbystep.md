# Red Hat Enterprise Linux 8 - Installation instructions

## Before you start

* Check that your [machine is properly registered](http://network.cern.ch/) (in case it is on the CERN network)

* Check that CERN Domain Name Service is updated for your machine (in case it is in the CERN network):

`host yourmachine` command should return an answer.

* Check that your machine meets minimum system requirements

  * Memory: Minimum **2 GB** (system will run with 1 GB but performance will be affected)
  * Disk space: **10 GB** (including **1 GB** user data) for default setup ( **1 GB** for minimal setup)

* Please see [Network/PXE installation](/installation/pxeboot) procedure, IF YOU USE it you may skip following points up to the system installation.

* Prepare boot media (you will need a single recordable CD or USB memory key).
  (Check the [boot media preparation](/installation/bootmedia) page for instructions how to prepare (and check) your boot media.)

* Available boot media - **boot CD/USB image**  [http://linuxsoft.cern.ch/enterprise/rhel/server/8/boot.iso](http://linuxsoft.cern.ch/enterprise/rhel/server/8/boot.iso) (Red Hat Enterprise Linux 8)

* Installation method - **http**: [http://linuxsoft.cern.ch/cern/rhel/8/baseos/x86_64/os](http://linuxsoft.cern.ch/cern/rhel/8/baseos/x86_64/os/) (Red Hat Enterprise Linux 8)

* Note: use CD/USB image installation method **ONLY** if Network/PXE installation is not possible.


## System installation

Installation language and keyboard selection:

![Language and keyboard selection](/rhel/rhel8/installshots/lang.png "Language and keyboard selection")

You will need to provide input for the items marked in red, 'Keyboard', 'Time & Date', 'Software Selection', 'Installation Destination'. It's best to start with 'Installation Destination'

![main screen](/rhel/rhel8/installshots/config.png "Main Screen")

Select the device to be used for the installation:

!!! warning
    As of 2025, all CERN-owned "endpoints" (e.g. laptops and office PCs) [must have their local storage encrypted](https://security.web.cern.ch/rules/en/ept.shtml). If this applies to you, make sure to check the "Encrypt my data" box.
    This is strongly recommended for personal devices as well.

![Select Device](/rhel/rhel8/installshots/partitioning.png "Select Device")

If selected device has been already used for previous version of operating system use 'Reclaim space':

![Reclaim space](/rhel/rhel8/installshots/reclaim.png "Reclaim space")

Set the 'root' (administrative account) password.

![Set root password and create user](/rhel/rhel8/installshots/rootpass.png "Set root password")

Select the software to install. Choose 'Software Development Workstation (CERN Recommended Setup)'
![Select software](/rhel/rhel8/installshots/software-selection.png "Select Software Development Workstation - CERN Recommended Setup")

!!! danger ""
    If you do NOT see 'Software Development Workstation (CERN Recommended Setup), you may need to add the CERN repository to the installation. You can do by clicking 'Installation Source' and referring to the following screen shot

![Add CERN Repository](/rhel/rhel8/installshots/add-cern-repo.png "Add CERN repository")

Your screen should now have no more items in 'red', and should look similar to this:
Select 'Begin installation':

![Begin Installation](/rhel/rhel8/installshots/ready-to-go.png "Begin Installation")

## Automatic CERN site specific configuration

After the successful installation, if you have selected 'Software Development Workstation (CERN Recommended Setup)' the initial configuration screen will be displayed

![Initial setup](/rhel/rhel8/installshots/initial-setup.png "Initial setup")

Upon clicking on 'CERN CUSTOMIZATIONS', you will be presented with the following options.

![Initial setup options](/rhel/rhel8/installshots/initial-setup-options.png "Initial setup options")

Depending on your choice, the locmap tool will be installed and invoked to create user accounts, provide root access, add printers, ... using information based on the ownership of device (network.cern.ch)

## Manual post-install configuration adjustment

If you have selected not to run X graphical environment on your machine, or you have not installed your system with the 'Software Development Workstation (CERN Recommended Setup)' but wish to profit from the locmap tool to perform site specific configurations, here are some small recipes:

* First become root by either logging in as root, or using sudo
  * Run `sudo su -`

* To configure automatic system updates
  * Run `dnf install dnf-autoupdate`

* Install Locmap (the tool used to define CERN specific configuration)
  * Run `dnf install locmap-release`
  * Run `dnf install locmap`

* Apply CERN site configuration defaults (full configuration)
  * Run `locmap --enable all`
  * Run `locmap --configure all`

* Install AFS client (only):
  * Run `locmap --enable afs`
  * Run `locmap --enable kerberos`
  * Run `locmap --configure all`

* Configure CVMFS (only)
  * Run `locmap --enable cvmfs`
  * Run `locmap --configure all`

* Configure Postfix (To align with [e-mail best practices](https://mailservices.docs.cern.ch/Miscellaneous/Anonymous_smtp/))
  * Run `locmap --enable postfix`
  * Run `locmap --configure all`

You can also use `/usr/bin/locmap --list` to check all the available puppet modules with their current state.

## Applying software updates

You should update your system immediately after its installation: Eventual security errata and bug fixes will be
applied this way before you start using it.

As root run:

```
/usr/bin/dnf -y update
```

to apply all available updates.

##  Remove the 'System Not Registered' notification from dnf, GNOME and Subscription-Manager Cockpit

To get rid of the System Not Registered notification, please run the following commands as root:

```
sed -i 's/enabled=1/enabled=0/' /etc/yum/pluginconf.d/subscription-manager.conf
yum remove -y subscription-manager-cockpit insights-client rhc
if [ -f /etc/xdg/autostart/org.gnome.SettingsDaemon.Subscription.desktop ]; then
  sed -i 's|^Exec=/usr/libexec/gsd-subman|#Exec=/usr/libexec/gsd-subman|' /etc/xdg/autostart/org.gnome.SettingsDaemon.Subscription.desktop
fi
if [ -f /lib/systemd/user/org.gnome.SettingsDaemon.Subscription.service ]; then
  systemctl --global mask org.gnome.SettingsDaemon.Subscription.service
fi
```
