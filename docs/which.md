# Which distribution should I use?

CERN supports both [RHEL](/rhel) and [AlmaLinux](/almalinux), and in addition [CentOS 7 ELS](/els7) - so which distribution should I use?

Of the above options, there are also several different 'versions' (families such as '8' or '9') which can also make the decision more confusing.

In general we recommend to use the **latest** version available, as this will ensure that the choice you make lasts for the longest period of time.

If you need to use a specific family, please read on:

For the **8** or **9** families you have two choices, either [RHEL](/rhel) or [AlmaLinux](/almalinux).

* Please see the table below

## AlmaLinux versus RHEL

| Situation / Use case                                                             |              Distribution to use               |
| :------------------------------------------------------------------------------- | :--------------------------------------------- |
| I use software that requires support from a vendor (eg: Oracle, WinCC OA)        |              RHEL should be used               |
| I need to distribute a virtual machine or docker image to users outside of CERN  |           AlmaLinux **must** be used (please see the [RHEL](https://linux-qa.web.cern.ch/rhel/#container-images-docker-podman) page for more details)
| My machine is not at CERN                                                        | Unless you have your own Red Hat License Agreement, AlmaLinux **must** be used. Red Hat is a licensed product and CERN's license does not cover external usage (please see the [RHEL](https://linux-qa.web.cern.ch/rhel/) page for more details) |
| My service provides interactive access to non CERN sites (eg: lxplus or lxbatch) |            AlmaLinux should be used            |
| My situation is not described above, can I use either ALMA or RHEL?              | Yes, you may use either - the choice is yours! |

## End of Support dates

| Distribution     | End of Support |
| :--------------- | :------------- |
| CentOS 7         | 30.06.2024 ([OTG0145248](https://cern.service-now.com/service-portal?id=outage&n=OTG0145248)) |
| CentOS 7 ELS     | 30.06.2026 ([ELS7](/els7)) |
| AlmaLinux/RHEL 8 | 31.05.2029     |
| AlmaLinux/RHEL 9 | 31.05.2032     |

## CC7/RHEL7 ELS7

Please refer to the dedicated [ELS7](/els7) page for all details
