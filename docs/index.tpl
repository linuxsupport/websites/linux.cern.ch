# Latest updates

* Check [AlmaLinux 9 latest updates](updates/alma9/prod/latest_updates)
* Check [AlmaLinux 8 latest updates](updates/alma8/prod/latest_updates)
* Check [Red Hat Enterprise Linux 9 latest updates](updates/rhel9/prod/latest_updates)
* Check [Red Hat Enterprise Linux 8 latest updates](updates/rhel8/prod/latest_updates)

!!! danger "CC7 was deprecated within the CERN environment on 30.06.2024, see [OTG0145248](https://cern.service-now.com/service-portal?id=outage&n=OTG0145248) for more details or refer to [Which distribution should I use](https://linux.web.cern.ch/which/)"

<br/>
# Latest news
