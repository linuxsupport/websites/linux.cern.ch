# CERN Linux Support policy

## Mandate

To provide a secure version of the Linux operating environment
that allows CERN staff and users to perform their (physics-related) work on
Linux, according to the goals of the organization.</p>
Tasks:

* **Assemble and certify a CERN Linux distribution** together with the CERN Linux user community.
* **Run central Linux installation servers** to make this distribution available on the CERN site.
* **Provide security updates and a deployment mechanism** for the CERN Linux distribution.
* **Provide general Linux support** as detailed below.

Check [KB0006508](https://cern.service-now.com/service-portal/?id=kb_article&n=KB0006508) for the official support restrictions in place.

## Support for CERN Linux distributions

Support calls will enter the Problem Tracking system via <a href="mailto:helpdesk@cern.ch">helpdesk</a>.

Urgent production issues inside IT department may be reported directly to the Linux Support 3<sup>rd</sup> level team.

Shortcutting the support structure (e.g. direct phone calls to 3<sup>rd</sup> level team members) is discouraged.

Linux system installations (including documented workarounds) and recurring documented issues will be handled by the (outsourced) 2<sup>nd</sup> level support, who will forward other completed calls to Linux Support 3rd level team. <!-- codespell:ignore nd -->

Support calls will be assigned a priority that takes into account
the impact of the problem for CERN (the reporter's opinion on the
priority will be considered, but may be overridden). Priorities affect
the order in which calls are handled, low-priority calls may be closed
without a satisfactory resolution if resources to answer them properly
are not available.

The following will **increase** the priority:

* Issues directly affecting operation of accelerators and data-taking.
* Issues (potentially) affecting a large number of users.
* Recurring and reproducible issues.

Each of the following will **lower** the priority:

* Non-standard PC hardware purchased outside CERN stores.
* PC hardware purchased from the CERN stores but not validated by Linux Support.
* Problems with add-on hardware or gadgets (local printers, Webcams, PDAs, USB storage, CD/DVD burners, ...)
* Problems with add-on software or different versions than deployed in the CERN distribution
* Per-user configuration issues
* Languages or fonts beyond official CERN languages (English and French).
* Requests from outside of CERN (CERN does not support outside institutes, even if they are running our Linux distribution. However, collaboration on [Scientific Linux](http://www.scientificlinux.org/) and bug fixes for our distribution are welcome). In case this isn't clearly stated otherwise in the call, a non-CERN mail address will be taken as a request from outside of CERN.

**Escalation:** In case of problems with the support
service, please contact the IT <a href="mailto:mod@cern.ch">Manager On Duty</a>.

## Support for other Linux distributions

IT does not support other Linux distributions. Support calls for
other recent Linux distributions opened via Helpdesk or direct mail to
Linux.Support@cern.ch will get treated with lowest priority, and
specifically **no support at all** will be provided for releases that are no
longer maintained by their respective distributor (this includes any formally de-supported CERN Linux releases).

If resources permit, calls for recent Linux distributions may be
answered, but substantial effort to diagnose the problem by the user
is assumed to occur before, and such calls may be closed anytime
without a satisfactory resolution.

Users are instead invited to ask their questions on [linux-community](https://linux-community.web.cern.ch/), in the hope that
other CERN users may be able to help, or use the mailing lists more
appropriate to the distribution they are running.

## Security

As a reminder, the [CERN computing rules](http://cern.ch/ComputingRules/) requires a user to
```
[...] take the necessary precautions to protect his personal computer or work station against unauthorized access.
```
System administrators of "file servers" (FTP, HTTP, NFS, AFS, etc)
```
[...] must take proactive and adequate measures to protect the operating system and applications from known security weaknesses.
```
This responsibility stays with the owner/user/persons defined in [LanDB](http://cern.ch/network).
Running a CERN-supported Linux version with automatic updates enabled will be considered to be
sufficient to protect a machine, as long as requests for special
actions (e.g. manual kernel upgrades) are followed timely. Such
requests will be sent to `root` on the machine concerned
(please configure your machine so that some human will read these
mails), as well as published in the [linux-announce@cern.ch](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=161390)
mailing list and on the [CERN Linux homepage](http://cern.ch/linux).
