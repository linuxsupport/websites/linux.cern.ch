# Linux support

* Support line contact: [linux.support@cern.ch](mailto:linux.support@cern.ch)
* User discussion list: [linux-users@cern.ch](mailto:linux-users@cern.ch)
* Update announce list: [linux-announce@cern.ch](mailto:linux-announce@cern.ch)

## CERN Service Portal

* [Report incident](https://cern.service-now.com/service-portal?id=sc_cat_item&name=incident&se=linux-desktop&s=linux)
* [Submit request](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=linux-desktop&s=linux)

## Linuxsoft service

* [Service availability](https://cern.service-now.com/service-portal?id=service_element&name=linux-desktop)
* [Service monitoring](https://monit-grafana.cern.ch/d/000000855/overview-service-availability?orgId=1&var-service=linuxsoft)

## Giving Supporters access to your machine

If you open a support ticket and you're asked to give the supporter temporary root access to your machine,
you should execute the following commands as root:

(If your machine is puppet-managed, be sure to stop puppet first)

```
$ yum install cern-linuxsupport-access
$ cern-linuxsupport-access enable
```

This command will give supporters root access to your machine so they can help
diagnose the issue.

Once the supporter has finished, don't forget to remove their access:

```
$ cern-linuxsupport-access disable
$ yum remove cern-linuxsupport-access
```

(If your machine is puppet-managed, don't forget to enable puppet again)


### How to write "good" support requests to Linux.Support

In order to resolve your issue as quickly as possible, please make sure that the following information is included in your support call:

* enough information to reproduce the problem, including commands used, actual output and expected result.
* machine name/hardware type for hardware related problems, exact software versions including RPM version for software trouble:

The following commands may be useful for describing the kernel and hardware (please choose the ones relevant for your problem, don't simply cut&paste all of the below):

```bash
    uname -a
    lspci -vv
    lsusb -vv
    cat /proc/scsi/scsi
    dmesg
    lsmod
```

and relevant excerpts from /var/log/messages (do not send us the whole file).

If you get a kernel panic ("Oops", blinking keyboard LEDs), please try to include the EIP (instruction pointer)/function name and stack backtrace. You won't need to copy down all the hexadecimal numbers. A "readable" digital picture of the screen can be enough.

**Software trouble**:

Please include the machine name (individual machine for clusters, i.e. lxplus123 instead of lxplus) and time -- software configuration changes over time.

```bash
    hostname
    date
    cat /etc/redhat-release
    uname -a
    rpm -qf '/name/of/misbehaving/command'
```

* If you believe this to be an issue tied to your user environment (shells/Desktop), please include your (AFS) username, and make the relevant files available for inspection (i.e. copy your ~/.bashrc, ~/.tcshrc, ~/.gnome or ~/.kde into ~/public). The support team doesn't have any special access rights on AFS.
* If you are experiencing problems on your own machine, please make sure that it is [updated to the latest released software](/updates) before reporting problems.
in case of network issues: please give the name and address of the remote end as well (the machine you are connecting to/from)
information about the frequency and other factors that could affect priority (e.g. standard setup vs customized, CERN PC shop hardware vs other, ...). See the [full priority list](/support/support-policy).
* Please give your support request a meaningful title -- this should be a summary of your problem that allows us to easily match it against past issues. Leaving it empty or just shouting "URGENT!!!! HELP!!" just means that all support personnel has to read through the whole report (and perhaps send it up the support chain), while perhaps a F.A.Q. entry could have answered it directly.

Providing this information already with your call will limit the amount of back-and-forward checking by the various support levels.

Often, you can save time by [Googling](http://google.com/) for the issue, and eventually providing a pointer to somebody else's bug report (e.g. on [Red Hat Bugzilla](http://bugzilla.redhat.com/)).