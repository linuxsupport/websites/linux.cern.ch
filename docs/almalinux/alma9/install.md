# AlmaLinux 9 - Install instructions

The following methods are currently available to install AlmaLinux 9

* AIMS interactive - select the menu item `Install AlmaLinux 9 (ALMA9) x86_64` or `Install AlmaLinux 9 (ALMA9) aarch64`
* AIMS host registration - define your host to use the `ALMA9_X86_64` or `ALMA9_AARCH64`
* OpenStack VM - boot from the image `ALMA9 - x86_64`
* ai-bs puppet managed VM - use `ai-bs --alma9`
* docker image - use `docker pull gitlab-registry.cern.ch/linuxsupport/alma9-base:latest`
* Boot ISO: [x86_64 boot.iso](https://linuxsoft.cern.ch/cern/alma/9/BaseOS/x86_64/os/images/boot.iso) or [aarch64 boot.iso](https://linuxsoft.cern.ch/cern/alma/9/BaseOS/aarch64/os/images/boot.iso)
* Minimal ISO: [x86_64 minimal.iso](https://linuxsoft.cern.ch/almalinux/9/isos/x86_64/AlmaLinux-9-latest-x86_64-minimal.iso) [aarch64 minimal.iso](https://linuxsoft.cern.ch/almalinux/9/isos/aarch64/AlmaLinux-9-latest-aarch64-minimal.iso)

!!! note "Installations from ISOs"
    When installing from an ISO image, you will need to manually add the CERN repos. From the "Installation Summary" screen, click on "Installation Source" and add an additional repository called `CERN` with the URL: `http://linuxsoft.cern.ch/cern/alma/9/CERN/x86_64/` (or `http://linuxsoft.cern.ch/cern/alma/9/CERN/aarch64/`, depending on your architecture)

For a detailed installation of a Linux Desktop, check the [step by step guide](/almalinux/alma9/stepbystep)
