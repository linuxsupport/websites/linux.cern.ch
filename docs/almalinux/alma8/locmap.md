# AlmaLinux 8 - Locmap

Locmap is typically installed by default when installing a new AlmaLinux system and selecting 'Software Development Workstation (CERN Recommended Setup)' during the installation.

In the event that locmap is not available, you may follow these instructions to install it.

```bash
$ sudo dnf -y install locmap-release
$ sudo dnf -y install locmap
$ sudo locmap --enable all
$ sudo locmap --configure all
```

### Usage

You can access the documentation by typing (as root):

```bash
# man locmap
```

You can list enabled modules:

```bash
# locmap --list
[Available Modules]
afs            [disabled]
cernbox        [disabled]
cernphone      [disabled]
chrony         [disabled]
cvmfs          [disabled]
eosclient      [disabled]
kerberos       [disabled]
lpadmin        [disabled]
postfix        [disabled]
resolved       [disabled]
ssh            [disabled]
sudo           [disabled]
zoom           [disabled]
```

You can configure all enabled module with the following command:

```bash
# locmap --configure all
```

You can configure a new module:

```bash
# locmap --enable modulename
# locmap --configure modulename
```

You can disable a module:

```bash
# locmap --disable modulename
```

You can remove root access to the Main Users of LanDB using the `--disallow_root`. It means that, only Resposibles defined in LanDB will have root access.
```bash
# locmap --disallow_root --configure modulename
# locmap --disallow_root --configure all 
```

### Locmap and Openstack
It easy to run locmap during an openstack installation. Create a script with the above commands say `install.sh`. Pass this as user data to `openstack create`.

```
openstack server create --image 'ALMA8 - x86_64' --key-name <mykey> --user-data ~/tmp/install.sh  --flavor m2.small mynode
```

alternatively the script can be uploaded via the configuration tab of the "Launch Instance" panel of OpenStack.
