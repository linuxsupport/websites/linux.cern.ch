# AlmaLinux 8 - Install instructions

The following methods are currently available to install AlmaLinux 8

* AIMS interactive - select the menu item `Install AlmaLinux 8 (ALMA8) x86_64` or `Install AlmaLinux 8 (ALMA8) aarch64`
* AIMS host registration - define your host to use the `ALMA8_X86_64` or `ALMA8_AARCH64`
* OpenStack VM - boot from the image `ALMA8 - x86_64`
* ai-bs puppet managed VM - use `ai-bs --alma8`
* docker image - use `docker pull gitlab-registry.cern.ch/linuxsupport/alma8-base:latest`
* Boot ISO: [x86_64 boot.iso](https://linuxsoft.cern.ch/cern/alma/8/BaseOS/x86_64/os/images/boot.iso) or [aarch64 boot.iso](https://linuxsoft.cern.ch/cern/alma/8/BaseOS/aarch64/os/images/boot.iso)
* Minimal ISO: [x86_64 minimal.iso](https://linuxsoft.cern.ch/almalinux/8/isos/x86_64/AlmaLinux-8-latest-x86_64-minimal.iso) [aarch64 minimal.iso](https://linuxsoft.cern.ch/almalinux/8/isos/aarch64/AlmaLinux-8-latest-aarch64-minimal.iso)

!!! note "Installations from ISOs"
    When installing from an ISO image, you will need to manually add the CERN repos. From the "Installation Summary" screen, click on "Installation Source" and add an additional repository called `CERN` with the URL: `http://linuxsoft.cern.ch/cern/alma/8/CERN/x86_64/` (or `http://linuxsoft.cern.ch/cern/alma/8/CERN/aarch64/`, depending on your architecture)

For a detailed installation of a Linux Desktop, check the [step by step guide](/almalinux/alma8/stepbystep)
