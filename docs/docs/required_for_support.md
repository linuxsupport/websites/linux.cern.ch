<!--#include virtual="/linux/layout/header" -->
# Writing Support requests
<h2>How to write "good" support requests to Linux.Support<br>
</h2>
<p>In order to resolve your issue as quickly as possible, please make
sure that the following information is included in your support call:
</p>
<ul>
  <li>enough information to reproduce the problem, including commands
used, actual output and expected result. </li>
  <li>machine name/hardware type for hardware related problems, exact
software versions including RPM version for software trouble:
    <p>The following commands may be useful for describing the kernel
and hardware (please choose the ones relevant for your problem, don't
simply cut&amp;paste all of the below): </p>
    <pre>uname -a<br>lspci -vv<br>lsusb -vv<br>cat /proc/scsi/scsi<br>dmesg<br>lsmod</pre>
and relevant excerpts from /var/log/messages (do not send us the whole
file).
    <p>If you get a kernel panic ("Oops", blinking keyboard LEDs),
please try to include the EIP (instruction pointer)/function name and
stack backtrace. You won't need to copy down all the hexadecimal
numbers. A "readable" digital picture of the screen can be enough. </p>
    <p><strong>Software trouble:</strong><br>
Please include the machine name (individual machine for clusters, i.e.
lxplus123 instead of lxplus) and time -- software configuration changes
over time. </p>
    <pre>hostname <br>date<br>cat /etc/redhat-release<br>uname -a<br>rpm -qf '/name/of/misbehaving/command'</pre>
  </li>
  <li>If you believe this to be an issue tied to your user environment
(shells/Desktop), please include your (AFS) username, and make the
relevant files available for inspection (i.e. copy your ~/.bashrc,
~/.tcshrc, ~/.gnome or ~/.kde into ~/public). The support team doesn't
have any special access rights on AFS. </li>
  <li>If you are experiencing problems on your own machine, please make
sure that it is <a href="/updates">updated to the latest released
software</a> before reporting problems. </li>
  <li>in case of network issues: please give the name and address of
the remote end as well (the machine you are connecting to/from) </li>
  <li>information about the frequency and other factors that could
affect priority (e.g. standard setup vs customized, CERN PC shop
hardware vs other, ...). See the <a href="/support/support-policy">full
priority list</a>. </li>
  <li>Please give your support request a meaningful title -- this
should be a summary of your problem that allows us to easily match it
against past issues. Leaving it empty or just shouting "URGENT!!!!
HELP!!" just means that all support personnel has to read through the
whole report (and perhaps send it up the support chain), while perhaps
a
F.A.Q. entry could have answered it directly.</li>
</ul>
<p>Providing this information already with your call will limit the
amount of back-and-forward checking by the various support levels.
</p>
<p>Often, you can save time by <a href="http://google.com">Googling</a>
for the issue, and eventually providing a pointer to somebody else's
bug report (e.g. on <a href="http://bugzilla.redhat.com">Red Hat
Bugzilla</a>).
</p>
