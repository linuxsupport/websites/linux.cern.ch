<html>
<head>
<meta HTTP-EQUIV=CONTENT-TYPE CONTENT="text/html; charset=utf-8">
<title>Slide 1</title>
</head>
<body text=#0000FF bgcolor=#FFFFFF link=#0000CC vlink=#000080 alink=#0000CC>
<p align=left><a href="JavaScript:parent.NavigateAbs(0)">Slide 1</a><br><ul></ul><li>Oracle on Linux
</p>
<p align=left><a href="JavaScript:parent.NavigateAbs(1)"><b><font color=#990000>The Good News</b></font></a><br><ul><li>OCCI client for gcc 3.2.3 now available (10g)
<ul><li><font color=#000000>Officially supported on RedHat Linux AS 3.0 &amp; Suse Linux ES 8.0</font>
<li><font color=#000000>‘Tried’ with LCG Conditions DB Application on RH7</font>
</ul><li>Oracle now has procedure in place to rebuild client libraries rapidly
<ul><li><font color=#000000>This is not a guarantee that it will happen automatically…</font>
</ul><li>Oracle 10g ‘Instant Client’ RPM kits – currently runtime only - will be extended to include ‘client developer’ kit
<ul><li><font color=#000000>Will be made available in EXPORT area in CERN /afs tree</font>
<li><font color=#000000>Also from OTN (not recommended)</font>
</ul><li>Extensive testing of 10g clients with 9i servers still to be done
<ul><li><font color=#000000>However, this is supported and not expected to be an issue</font>
</ul></ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(2)"><b><font color=#990000>Outlook</b></font></a><br><ul><li>CERN has revitalised its contacts with Oracle since ~1999
<li>New technical contact for CERN will start in June
<li>Regular ‘CERN-Oracle’ Summits: next foreseen for June/July
<li>Many features in Oracle 10g come from these interactions
<ul><li><font color=#000000>ULDB, native floats, silent install improvements, …</font>
</ul><li>We will continue to stress the need for timely adoption of new compilers
</ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(3)"><b><font color=#990000>3rd Party Products</b></font></a><br><ul><li>IT-DB does not provide support for e.g. Tora, perl-DBD-Oracle
<li>This does not mean that these tools cannot, or are not (be) used
</ul></p>
</body>
</html>