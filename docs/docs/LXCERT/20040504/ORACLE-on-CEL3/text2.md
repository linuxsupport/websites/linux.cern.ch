<html>
<head>
<meta HTTP-EQUIV=CONTENT-TYPE CONTENT="text/html; charset=utf-8">
<title>Outlook</title>
</head>
<body text=#0000FF bgcolor=#FFFFFF link=#0000CC vlink=#000080 alink=#0000CC>
<center>
<a href="text0.html">First page</a> <a href="text1.html">Back</a> <a href="text3.html">Continue</a> <a href="text3.html">Last page</a> <a href="ORACLE-on-CEL3.html">Overview</a> <a href="siframes.html">Graphics</a></center><br>
<h2><b><font color=#990000>Outlook</b></font></h2><p>
<ul><li><h2>CERN has revitalised its contacts with Oracle since ~1999</h2>
<li><h2>New technical contact for CERN will start in June</h2>
<li><h2>Regular ‘CERN-Oracle’ Summits: next foreseen for June/July</h2>
<li><h2>Many features in Oracle 10g come from these interactions</h2>
<ul><li><font color=#000000>ULDB, native floats, silent install improvements, …</font>
</ul><li><h2>We will continue to stress the need for timely adoption of new compilers</h2>
</ul></body>
</html>