<!--#include virtual="/linux/layout/header" -->
</head>

<body>

<h2>DRAFT-2 Minutes of the LXCERT meeting 21.03.2006</h2>

<h2>Invited / Attendance</h2>

<table border="1">

  <tbody>

    <tr>

      <th>Abbrev</th>

      <th>Name</th>

      <th>Affiliation</th>

      <th>Present</th>

    </tr>

    <tr>

      <td><u>AB</u></td>

      <td>Alastair Bland</td>

      <td>AB/CO-admin</td>

      <td></td>

    </tr>

    <tr>

      <td><u>AP</u></td>

      <td>Andreas Pfeiffer</td>

      <td>LCG Application Area</td>

      <td>(excused)</td>

    </tr>

    <tr>

      <td><u>BB</u></td>

      <td>Bruce M Barnett</td>

      <td>ATLAS-online</td>

      <td>(replaced by <u>MD</u>, Marc Dobson)</td>

    </tr>

    <tr>

      <td><u>BG</u></td>

      <td>Benigno Gobbo</td>

      <td>non-LHC experiments</td>

      <td></td>

    </tr>

    <tr>

      <td><u>EC</u></td>

      <td>Eric Cano</td>

      <td>CMS-online</td>
      <td>excused (Vincenzo Innocente)</td>
    </tr>

    <tr>

      <td><u>EO</u></td>

      <td>Emil Obreshkov</td>

      <td>ATLAS-offline</td>

    </tr>

    <tr>

      <td><u>FR</u></td>

      <td>Fons Rademakers</td>

      <td>ALICE-offline</td>


    </tr>

    <tr>

      <td><u>JC</u></td>

      <td>Jo&euml;l Closier</td>

      <td>LHCb-offline</td>

    </tr>

    <tr>

      <td><u>JI</u></td>

      <td>Jan Iven</td>

      <td>IT-services catchall (chair)</td>

    </tr>

    <tr>

      <td><u>JP</u></td>

      <td>Jaroslaw Polok</td>

      <td>general Desktops (secretary)</td>

    </tr>

    <tr>

      <td><u>KS</u></td>

      <td>Klaus Schossmaier</td>

      <td>ALICE-online</td>

    </tr>

    <tr>

      <td><u>NMN</u></td>

      <td>Nicolas De Metz-Noblat</td>

      <td>AB/CO-development</td>

      <td>(excused)</td>

    </tr>

    <tr>

      <td><u>NN</u></td>

      <td>Niko Neufeld</td>

      <td>LHCb-online</td>

    </tr>

    <tr>

      <td><u>VI</u></td>

      <td>Vincenzo Innocente</td>

      <td>CMS-offline</td>


    </tr>

    <tr>

      <td><u>TK</u></td>

      <td>Thorsten Kleinwort</td>

      <td>IT PLUS/BATCH service</td>

      <td></td>

    </tr>

  </tbody>
</table>

Date: Tuesday 21.03.2002, 16:00<br>
Place: 40-R-D10

<h2>Agenda</h2>

<ul>

  <li>SLC4 certification status</li>
  <li>SLC4 certification decision</li>
  <li>AOB</li>

</ul>

<H2>Summary</H2>
<p>No major obstacles for SLC4 were reported, it is still foreseen to
	certify SLC4 at the end of march 2006.
</p>

<H2>
<!--+GtkHTML:<DATA class="ClueFlow" key="orig" value="0">--><B><FONT SIZE="5">SLC4 status - Tour de table:</FONT></B>
</H2>
(several updates have been received before the meeting, including on from <U>AP:</U><BR>
LCG AppArea software:<BR>
- all recent external packages (as for configuration LCG_42) have been build successfully in local disk space and will be installed soon in AFS using the system compiler (now 3.4.5). Building of projects will start afterwards. This is for both h/w architectures: ia32 and amd64<BR>
<BR>
CERNLIB<BR>
- after some problems with libshift have been cured, cernlib will be installed into AFS <tt>/afs/cern.ch/sw/lcg/external/cernlib</tt>) in the next few days (again for the two h/w architectures as mentioned above).<BR>
There is still a warning coming from the makedepend step about missing stddef.h which should be understood (anyone has any idea ??), but this should not delay the installation. (stddef.h seems to be in the compiler's lib/include directory for slc3 but not in slc4 ???)<BR>

<BR>
<U>VI</U>: CMS-online has all their dependencies met, but will not port kernel drivers until after the magnet test is done (~June). OK to certify.<BR>
CMS-offline software compiled on SLC3 with gcc-3.2.3 and running on SLC4 (both on i386 and x86_64 in 32/64bit-mode) has passed the internal validation tests. This is good enough to allow CMS to run, OK to certify. &quot;Native&quot; tests with SLC4/gcc-3.4.5 are still pending on LCG/SPI software availability. <BR>
Interesting data points: 
<UL>
    <LI>Fermi has deployed SLF4 for &quot;all services (including Grid), except for the worker nodes&quot; and has not reported major issues. 
    <LI>32bit software compiled on SLC4 with gcc32 runs fine on SLC3.
</UL>
<BR>
<u>TK</u>: test SLC4 LXPLUS/LXBATCH
machines still expected to be available at the end of the month.

<!--test SLC4 LXPLUS/BATCH machines still not available, but expect
32bit-versions to be available at the end of the month (no issues
encountered yet), 64bit shortly afterwards (batch submission with
token extension needs looking at).-->

SLC4 LXPLUS/LXBATCH machines are nor expected to run NFS clients, so
<tt>/hebdb</tt> and <tt>/fatmen</tt> will not be available there.

Expect some iterations on the package sets in the beginning, standard
policy applies: supported packages from the "linuxsoft" repository
usually can get added quickly (except if security policy violations),
anything else needs discussion.&nbsp; No real showstoppers, OK to
certify. <BR>

<u>TK</u> raised the question whether the &quot;default&quot; eventually should point to the 32bit or 64bit variant. Unclear&nbsp; - <u>JI</u>, <u>JP</u> prefer 64bit version to get remaining 64bit issues discovered quickly, <u>JC</u> (and the majority of the participant) preferred 32bit to ease the transition. This still needs to be confirmed before the actual alias switch based on user feedback after the LXPLUS machines become available.<BR>
<u>AB</u> asked whether the LEMON versions were substantially different between SLC3 and SLC4. This appears not to be the case, except for modules that parse <tt>/proc</tt> (which shows differences between 2.4 and 2.6 kernels) and are being rewritten.&nbsp; <BR>
<BR>
<u>BG</u> gave a quick summary for the non-LHC experiments - not all have replied to his questions. The assumption is that non-replying experiments are either happy or have too little manpower to do proactive tests or have largely disappeared. NA60 tested in 32bit mode and is fine with SLC4, OPAL waits for CERNLIB (but programs recompile), COMPASS has also rebuild their code and is also waiting for CERNLIB. COMPASS had a look at 64bit mode (on SLC3), analysis software appears OK, reconstruction had some 64bit issues. <BR>
In summary, the non-LHC experiments haven't found serious issues with SLC4, OK to certify.<BR>
<BR>
<U>KS</U> reported that ALICE-online &quot;looks good&quot; after a period of intensive testing over the last days (some minor issues found and addressed), also tried it on a few desktops. Currently having trouble with <tt>/dev</tt> device files disappearing, will look into &quot;udev&quot; configuration (but in general everybody likes the consistency that udev brings).<BR>
<BR>
<U>MD</U> listed the outstanding issues for ATLAS-online: 
<UL>
    <LI>tg3 or bcm5700 driver - needs tests, but <u>AB</u> reported that tg3 is good enough for them on SLC4 [<u>JP</u> asked whether anybody was using the bcm5700 on SLC3 - turns out to be not the case, both <u>AB</u> and <u>MD</u> have their own versions on SLC3] 
    <LI>bigphysarea - longer-term issue, the driver is in SLC4 for now
</UL>
Currently, SLC3+gcc-3.4.4 is part of the nightly recompilation cycle, some issues still need fixing there. <u>MD</u> hopes that this can be done until mid-April, then switch over to SLC4+gcc-3.4.5 and fix eventual new issues until end of April. None of this will block the certification.<BR>
<BR>
<BR>
<U>AB</U> said that the most vital AB/CO servers (NFS file servers) are already running on SLC4, since the new LVM features are very interesting. They have seen several issues (nfsd crashes, kernel crashes, ext3 shrinking only while offline), but luckily started with low expectations (&quot;..as long as not all servers crash at the same time...&quot;). They noticed that 
<UL>
    <LI>sshd/login speed is acceptable (no longer the 4sec delay they see with SLC3), 
    <LI>the proprietary ATI drivers seems OK, 
    <LI>NVidida drivers causes a crash after a few X11 sessions [<u>JP</u> remarked on general (lack of) stability of this driver on SMP kernels], including one that gets triggered via the new graphical boot screen (&quot;rhgb&quot;) after reinstallations, 
    <LI>the default runlevel after Kickstart installs seems to b &quot;3&quot; even if X11 has been configure properly 
    <LI>LabView crashes (under investigation with IT/CO, worked fine under SLC3)
</UL>
Java WebStart (widely used in AB/CO) still needs testing, and they need the ORACLE instantclient and <tt>TNSNAMES.ORA</tt> updating mechanism [<u>JP</u> explained the new &quot;cernonly&quot; RPM repository]. <u>AB</u> also reported on some &quot;diskless client&quot; SLC4 setups, which triggered a short discussion since ATLAS-online and LHCb-online are doing similar things, LHCb in the scope of LinuxForControls. Some conceptual differences as to whether to use NFS-root vs RAMdisk images, and how to best update the image file, but all are invited to submit their use cases/specifications to LinuxForControls/Matthias Schr&#246;der.<BR>
<BR>

<U>AB</U> inquired on the previous &quot;go to SLC5&quot; proposal
(was changed after RHEL5 delays to &quot;go to SLC4&quot;, see <a
href="http://cern.ch/linux/documentation/LXCERT/20060207/minutes">minutes
of the previous meeting</a>), and expressed interest to look at
RHEL5betas (but timelines may not match AB/CO &quot;change
window&quot; in Dec/January). In conclusion, <U>AB</U> hoped that SLC4
would get certified ASAP.<BR>
<BR>
ALICE-offline / <U>FR</U> certified their software in 32bit and 64bit mode, no major issues seen.<BR>
<BR>
<u>JC</u> explained that LHCb-offline had had no time for significant tests (due to the overlap with DataChallenges). They will use SLC3 for the next DCs as well (no immediate need for SLC4), but have no reason to block the SLC4 certification.<BR>
&nbsp; <BR>
<u>EO</u>: ATLAS-offline has started to include SLC3+gcc-3.4.4 into their nightly rebuild exercise, which has identified a few issues (currently being fixed, but at low priority -- several other issues due to a change in release policy). The existing SLC3 version has passed all validation tests under SLC4. Two &quot;desktop&quot; installation tests failed since both were done on machines with badly-supported hardware (Vobis800+Intel EtherExpress &quot;AUI&quot; [<u>JP</u>: workaround is known for this])., OK to certify.<BR>
<BR>
<u>NN</u> confirmed the statements from the last meeting - they are
									ready to switch the remaining LHCb-online nodes to SLC4, servers already run it.&nbsp; Currently looking into PVSS issues [update from W.Salter after the meeting: these are now resolved]. OK to certify.<BR>
<BR>
For Linux support, <u>JP</u> explained that an &quot;integrated&quot; SLC43 release is currently under way (last chance to get updates and new products into the base installation), should be available before the end of March. Any later updates will be released via the usual updates mechanism. <u>JI</u> hoped that an initial Kerberos5-aware login environment and SSH would be available, but this has been delayed already considerably.<BR>
<BR>
<H2>Certification decision</H2>
All agree that SLC4 can be certified once the integrated SLC43 is available. Deadline of &quot;end of march&quot; will most likely be met.<BR>
<BR>

<h2>Next Steps</h2>

<p><u>JI</u> asked the experiments to provide input for the transition from
SLC3 to SLC4, by providing suitable time windows for migration and
listing constraints (periods of stability). "Hidden" services
(ie. those not running user code) will be migrated over the summer,
with short service interruptions scheduled as usual.</p>

<H2>
<B><FONT SIZE="5">AOBs:</FONT></B>
</H2>
<ul>
<li>There was a discussion on whether new kernels should be made the &quot;default&quot; for next reboot, this is a new update policy to minimize the number of desktops running old&amp;insecure kernels. <u>AB</u> expressed concern that this would introduce an unforeseen change after a machine crashed (e.g AB machine with hardware watchdog), some buggy instance of yum/apt actually removed drivers for the old kernel; result was unusable machines.<BR>
<u>JP</u> explained that some bugs in the updating scripts had been fixed, and advocated an even stricter policy (to reboot directly after a kernel update that got applied at boot time), as to minimize the time the new kernel sits dormant on a machine.<BR>
Action: <u>JP</u> and <u>AB</u> to come up with a example configuration that prevents kernel updates (and other problematic packages, such as the binary NVidia drivers) from going automatically to the AB/CO machines. LinuxForControls got mentioned as usual.</li>

<li>
<u>AB</u> would like a mixed 8bit/24bit mode in X11 to support legacy
(HP) applications.</li>

<li>AB asked whether the default AFS installation should limit AFS to
use only the CERN cell, after several problems seen with file browsers
hat tried to enumerate all known AFS cells. Some workarounds for this
problem exist (e.g. AFS dynroot, but this breaks some working
scripts&nbsp; that access <tt>/afs/usr/local/</tt>).</li>

<li>JP reminded the participants of proposed SLC3 lifetimes - no objection, already agreed last time.</li>
</ul>


