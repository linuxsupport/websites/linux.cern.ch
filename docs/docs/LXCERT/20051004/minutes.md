<!--#include virtual="/linux/layout/header" -->

<h2>Minutes of the LXCERT meeting 04.10.2005</h2>


<h2>Invited / Attendance</h2>
<table border="1">

<tr><th>Abbrev</th><th>Name</th><th>Affiliation</th><th>Present</th></tr>

<tr><td>AA</td><td>Alberto Aimar</td><td>LCG Application Area</td></tr>
<tr><td>AB</td><td>Alastair Bland</td><td>AB/CO-admin</td></tr>
<tr><td>AD</td><td>Andre David</td><td>non-LHC experiments</td></tr>
<tr><td>AP</td><td>Andreas Pfeiffer</td><td>LCG Application Area</td><td></td></tr>
<tr><td>BB</td><td>Bruce M Barnett</td><td>ATLAS-online</td><td>excused &rarr; MD</td></tr>
<tr><td>BG</td><td>Benigno Gobbo</td><td>non-LHC experiments</td><td>excused &rarr; AD</td></tr>
<tr><td>EC</td><td>Eric Cano</td><td>CMS-online</td></tr>
<tr><td>EO</td><td>Emil Obreshkov</td><td>ATLAS-offline</td></tr>
<tr><td>FR</td><td>Fons Rademakers</td><td>ALICE-offline</td></tr>
<tr><td>HD</td><td>Hubert Degaudenzi</td><td>LCG Application Area</td><td></td></tr>
<tr><td>JC</td><td>Jo�l Closier</td><td>LHCb-offline</td></tr>
<tr><td>JI</td><td>Jan Iven</td><td>IT-services catchall (chair)</td></tr>
<tr><td>JP</td><td>Jaroslaw Polok</td><td>general Desktops (secretary)</td></tr>
<tr><td>KS</td><td>Klaus Schossmaier</td><td>ALICE-online</td></tr>
<tr><td>MD</td><td>Marc Dobson</td><td>ATLAS-online</td></tr>
<tr><td>NMN</td><td>Nicolas De Metz-Noblat</td><td>AB/CO-development</td></tr>
<tr><td>NN</td><td>Niko Neufeld</td><td>LHCb-online</td></tr>
<tr><td>SW</td><td>Stephan Wynhoff</td><td>CMS-offline</td></tr>
<tr><td>TK</td><td>Thorsten Kleinwort</td><td>IT PLUS/BATCH service</td><td>missing</td></tr>

</table>

<h2>Agenda</h2>
<ul>
<li>status of the "compiler" certification:
  <ul>
  <li>gcc version issue
  <li>status of major packages (dependencies status for the experiments)
  </ul>
<li>proposed changes for SLC4
<li>planning (foreseen delays to certify experiment SW after dependencies
      have been met)
<li>AOB
</ul>



<h2>Issue1: status of the "compiler" certification</h2>

<P><u>AP</u> explained that the <a
href="http://lcgapp.cern.ch/project/mgmt/af.html">LCG architects forum</a> (LCG
AF) has decided in a <a
href="http://lcgapp.cern.ch/project/mgmt/AFMinutes20050922.html">recent
meeting</a> to go back to the system gcc-3.4.3 instead of gcc-3.4.4. All
"external" packages already had been compiled with 3.4.4, and no problems are
expected for recompiling them with 3.4.3. This is somewhat automated, so all the
"external" packages for gcc-3.4.3 should be available in the next days.</p>

<p>On the other hand, for some of the internal packages like POOL the port to
gcc-3.4.4 (with stricter standards compliance than on gcc-3.2) hasn't finished
yet. These packages require expert intervention, and may take
<i>O(weeks)</i>.</p>

<p>A short discussion ensued on whether the timeline for SLC4 (to be certified
until end of year) was still within reach, given that all LHC experiments
require LCG Application Area packages. But unless major obstacles occur, the
porting effort should be finished soon enough for the experiments to test their
software in time. (side note from <u>SW</u>: an initial report from a CMS
collaborator indicates that parts may already work now on SLC4, no details
available on the software/versions used etc).</p>

<p>It was also decided to take out the LCG Application Area packages from the <a
href="http://cern.ch/linux/scientific4/certification">certification dependency
tracking</a> (since the LCG AF does it own tracking), and only leave such packages if
they are used/required by non-LHC users (who may not be represented via the LCG
AF), and/or in case the package is also shipped with the distribution (e.g
CERNLIB-as-RPM).</p>



<h2>Issue2: proposed changes for SLC4</h2>

<p><u>JI</u> presented a <a href="SLC4-status/">short summary of announced
changes</a> for SLC4, both from CERN and upstream. He requested further change
request to be brought forward now, so that they could be properly announced via
the <a href="http://cern.ch/mgt-desktop-forum/">DTF</a> etc.


<p><u>MD</u> pointed out that porting drivers from 2.4 to 2.6 kernels takes long
(development and full testing cycle: <i>O(1 year)</i>). It was agreed that recurring
issues (and their solutions) should be shared via the Linux-certification
mailing list. <u>AB</u> and <u>NMN</u> reported that AB/CO have successfully
ported their drivers to 2.6, as have LHCb-online (<u>NN</u>) and ALICE-online
(<u>KS</u>). But nobody has drivers for SLC4 yet.

<p>Given that OpenAFS still hasn't reached the same level of stability on 2.6 as
on 2.4 even 2 years after 2.6 has been released, <u>FR</u> asked whether this
was a dying project (and whether CERN should drop it), or whether labs like CERN
should get more involved with OpenAFS development. It was explained that the
project was alive and other labs are using OpenAFS as well, and that the current
round of development (1.2 &rarr; 1.4) centered on the Windows client. But CERN
has less than 2 FTE on AFS support and is not in a position to do significant
development in this area.</p>

<p><u>NMN</u> and <u>AB</u> listed several problem areas with the "general
	version update" in SLC4 packages, such as

<ul>
<li>xorg-x11 (now runs without open TCP port by default, several other
members welcomed this since clear X11 connections are a security
risk), xorg-x11 Xnest ([update: Xnest has problems with backing-store,
i.e. applications displayed on the screen are not refreshed properly
when temporarly hidden by another application])

<li>NFSv3 now is the default (which has caused trouble in mixed environments,
especially with automounting). <u>MD</u> pointed out that NFSv3 in their
environment is actually behaving better than v2.

<li>GNOME-2.8 being still slow. However, it was clarified that the "slow
gnome-terminal scrolling" bug (vte) from SLC3 has been fixed.

<p>[<b>addendum:</b> gnome-2.6 should have had several other performance improvements via GTK+, which would then get carried over to
		    gnome-2.8.<br>
<a href="http://www.gnome.org/start/2.4/notes/">GNOME-2.4 release notes</a><br>
<a href="http://www.gnome.org/start/2.6/notes/">GNOME-2.6 release notes</a><br>
<a href="http://www.gnome.org/start/2.8/notes/">GNOME-2.6 release notes</a>
]</p>

<li>Kerberos/AFS: <u>NMN</u> suggest to restrict AFS clients to the CERN AFS
cell, otherwise long delays may come from autocompletion (dynroot might help as
well). This turned out to be a per-machine configuration issue.<br>

A quick discussion followed during which <u>NN</u> pointed out that LHCb-online
thinks of setting up their own AFS servers inside their CNIC domain. Other
experiments don't plan to do that, and may have to configure access to the
CERN-wide Kerberos KDC from within their domains. Following a question by
<u>AB</u>, it was clarified that the SLC4 ssh would forward AFS credentials (via
SSH-1 protocol) or get them via forwarded Kerberos5 TGTs (SSH-2)

<li>ORACLE: the ORACLE tools situation is still not clarified (first raised in
SLC3; RPMs initially promised but then retracted):

AB/CO needs to have locally-installable versions of the ORACLE libraries and
sqlplus, properly configured for CERN via <tt>tnsnames.ora</tt>, preferably
without relying on LD_LIBRARY_PATH being set in the user environment. AB/CO also
relies heavily on ProC.<br>

<b>Having locally-installed client tools could be more important to AB/CO that
central support from IT-DES. <u>NMN</u> stressed that these tools need to be
available before any meaningful validation inside AB/CO can even start</b>.

<li>AB/CO needs to have recent enough versions of
eclipse (3.1 or more recent) and ant (1.6.3 or more recent)

<li>SLC4 seems to drop support for certain processors [Pentium2?, HP6000
notebook, need to clarify]. This would need to be widely announced, since the
installation will actually start, but leave the user with a non-booting
system. Also needs to be seen whether this affects any of the "online" VMEbus
systems.

</ul>

<p><u>NN</u> stressed the need for full support for x86_64. This support is
available on the OS level, and it is up to the individual package providers to
decide whether x86_64 will be supported (for 64bit mode). Notably the LCG AF
needs to decide when it wants to have x86_64 as supported platform. Most
newly-arriving hardware can run in 64bit-mode, it will be between the
experiments and IT-FIO to schedule a transition to 64bit-mode for batch
capacity.</p>

<p><u>NN</u> and <u>FR</u> also explained that support for ia64 is
	  not required for their environments and should be the first to be
	  discontinued to limit the number of platforms. Ia64 should also not be a cause for any
	  certification delay. [addendum: <u>NN</u> pointed out that while em64t
	  and x86_64 are very similar, Opterons benefit strngly from  a
	  NUMA-aware kernel which may require different kernels]</p>

<p><u>NMN</u> and <u>EC</u> expressed strong need for a Java JDK that work with
NPTL/glibc-2.3, in a readily-installable version. This would also make the RPMs
at <a href="http://jpackage.org">jpackage.org</a> (such as Jakarta or newer
(NMN: usable) eclipse versions) immediately accessible. <u>JI</u> explained that
this is a political issue (and has been for the past few years), IT does not
have formal Java support (<u>AB</u>: may actually not even be required,
package-level support may be enough). Also the license is still very
restrictive, and the CERN user community would need to agree on a single version
of the JDK (possibly 1.5, since older version do not support x86_64
	  well).
<p>
[update: <u>NMN</u> added the following via mail:<br>
For JDK, the problem is really complex:
<ul>
<li>nobody want to become beta-tester for any version, but we finally had
to urgently upgrade under SLC3 to at least 1.4.2_06 for javaws to accept
properly signed jars.
<li>deployed version has to stay coherent with gcc and glibc for proper
usage of threads and have JNI capability.
<li>users did not wanted to change to 1.5 (fear for language
incompatibilities)...
<li>proper choice of jdk/jre for stand-alone applications might be
independent from jre required by firefox, mozilla and other web-browsers
to ensure that web applications like EDH still work in a safe
environment.
</ul>
]
</p>


<u>JI</u> presented the current <b>rumors</b> (no confirmation from RH) about a
possible delay of RHEL5 to end of 2006, which would rule out this release for
general LHC startup. This was followed by a discussion on release lifetimes,
with a requirement from <u>MD</u> that SLC3 needs to be available until the 2.6
kernel drivers have been fully validated.  <u>JP</u> pointed out that support
overlap between current and "LHC" production release will be on the order of 1.5
years.

<p>The option to only lock certain environments (like online farms) onto the
"LHC startup release" in October 2006 was discussed, with other services moving
to SLC5 sometime after that date (little issue for services hidden behind a
networked API, and e.g. little trouble for CPU nodes expected, especially if the
physics compiler is decoupled from the OS). In this case an older release could
be obsoleted for general CERN usage while still be available to certain
(restricted, e.g. via CNIC network domains) environments



<h2>Issue3: planning</h2>

<p>Original certification planning still holds: try to certify until the end of
2005. Currently the LCG application area software (blocking LHC experiments) and
ORACLE client (blocking AB/CO) are on the critical path.

<p>Issues encountered should be submitted via the "linux.support" REMEDY mailfeed.</p>

<h2>AOB</h2>

<ul>

<li>It was pointed out that the current <tt>apt/yum-autoupdate</tt> tool is
<b>not</b> a recommended solution for service-critical machines, as changes may
lead to machines becoming unusable (examples: recent XFree86 updates for AB/CO
Controls room machines). This is a design decision, (rare) unavailability on
selected desktops is preferred over (frequently) non-patched desktop machines.

<li>This lead to a discussion on CNIC/LinuxFC. Criticism was expressed for the
fact that only now the technical people are being contacted to review the
requirements and test the proposed (Quattor-based) solution. This turned out to
be partly an internal communication issue, since all concerned user groups have
formal representation inside CNIC, and have been asked to review the current
list of requirements. It was pointed out that the current round of going round
the "technical" contacts was on initiative by IT, who needs to have some
commitment to use LinuxFC in order for the project to continue.

<p> A quick poll showed general interest in CNIC, but already now for some
environments (e.g. ATLAS-online diskless farms) it may be too late to switch
management to LinuxFC. CNIC/LinuxFC will be presented further via a DTF
presentation.

</ul>


<p><u>JI:</u> asked whether a date for next meeting needed to be fixed(need 2 weeks lead for
external participants). Consensus: Not required now, no real issues yet.</p>

