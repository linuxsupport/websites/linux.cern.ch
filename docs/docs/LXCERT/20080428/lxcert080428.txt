

Linux Certification Committee

 Meeting of April, 28th, 2008

Present: Bruce Barnett, Alastair Bland, Joel Closier, Marc Dobson,  
         Enzo Genuardi, Vincenzo Innocente, Peter Kelemen, Niko Neufeld,
         Jaroslaw Polok, Fons Rademakers, Stefan Roiser, Klaus Schossmaier,
         Rainer Schwemmer, Emil Obreskov,

Jan presented the current status of the SLC5 discussion: 

Due to lack of commitment for using SLC5, the last LXCERT meeting did
not agree to start the certification process.

But in the meantime LCG has agreed to an introduction of SLC5 service
by September 08, and a phaseout of SLC4 batch and plus service by 1Q
2009.

FIO is now committed to support SLC5, with public services as of September.

So far no proper (directly-installable) SLC5 exists. FIO expects RHEL
5.2 to be released soon, and believe it will contain some major
updates, so they want to base SLC5 on that 5.2 release.

A basic SL5 infrastructure with aims targets and a reference machine
(reflsc5-ia32e) is available. For the status of the quattor tools for
SLC5 you can check https://twiki.cern.ch/twiki/bin/view/ELFms/ScientificLinux5Port

FIO-LA will try to minimise the CERN customisations for SLC5, this
should reduce the support workload and speed up the release for fixes.


Discussion about dependencies and priorities:

AB/CO now also needs LabView, PVSS, support for diskless
systems ("preempt" kernel), TSM and support for the PPC architecture.

LHCb is depending on L4C/quattor and PVSS (which probably will require
compatibility libs).

ATLAS requires TSM

Joel pointed out that the discussion should not concentrate on
experiments software or compilers, since these should be addressed by
the architects forum, and should be decoupled from the OS
certification. On the other hand the default compiler to be used on
the machines is still the compiler and the libraries provided by the
vendor.

Having binary compatibility between SLC4 and SLC5 would ease the
migration to 5 considerably.

At the time being it is very unlikely that RHEL6 would be out by
autumn, so skipping SLC5 is not a viable option.

Using CentOS instead of building SLC5 would not reduce the work for
FIO, since in one way or another the CERN specific changes still have
to be made. One disadvantage of CentOS is the release policy: the fact
that no betas are made public makes it impossible to assure that
features that are essential for us are available and working correctly.

Niko pointed out that the CERN purchasing rules result in the purchase
of a mix of machines from many different vendors, aggravating the hardware
compatibility issues we have under SLC4. Jan replied that with the
newer SLC5 we are supposed to get less hardware compatibility issues, except
for the laptops where the situation is not improving in the long run -
the hardware of the laptops is developing much too fast to be able to
provide the drivers for the new hardware under the (always relatively old)
"enterprise" kernels. While for some of the users SLC compatible
laptops are very low priority, others see them as important. But it
should be noted that it is possible to run a virtual SLC instance on
most of the laptops with Windows or any more recent Linux
distribution.

For purchasing it is too early to require SLC5 compatibility, since we
do not yet have the release ready, so vendors can not do any testing.

Klaus mentioned that they have many drivers written by themselves, and
that he is worried about the work needed for porting them to SLC5. Jan
replied that FIO can not do the porting for him, but that his section
can provide some help and general support.

IT will soon provide some nodes running SL(C)5, groups should contact
Linux-support if they want accounts on these machines. Making
available sizable amounts of CPU capacity requires quattor and elfms
to be ready, so that the machines can be configured without much
manual interventions.
   

The main difference between SLC4 and SLC5 will probably be

- no more kerberos 4 support
- newer versions of Firefox, Thunderbird, SeaMonkey
- CERN wide single sign on is in discussion, but there is no technical solution ready for implementation

It was agreed to call the release SLC5, despite the fact that there
will be much less CERN specific changes than in SLC4. Starting
directly with SLC5 makes it easier to include more CERN specifics 
in case that it's needed.

The maximum life for SLC4 has not yet been defined. It will depend on
the ease of migration to SLC5. For most services it should be
completely transparent whether they are provided under SLC4, or SLC5,
but IT does have an interest in harmonising on one OS, and not support
several versions of the OS for a extended period of time.
 
Emil pointed out that it took some of the ATLAS grid sites a year to
migrate to SLC4.

There was a general agreement that the certification process for SLC5
should be started.

The next certification meeting should be before summer, and one after
that sometimes in summer. End of June was seen as a good date for the
next meeting.
