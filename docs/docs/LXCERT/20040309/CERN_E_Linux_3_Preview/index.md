<html>
<head>
<meta HTTP-EQUIV=CONTENT-TYPE CONTENT="text/html; charset=utf-8">
<title>Title</title>
</head>
<body text=#000000 bgcolor=#FFFFFF link=#0000CC vlink=#000080 alink=#0000CC>
<center>
<h2><a href="siframes.html">Click here to start</a></h2>
</center>
<center><table width=90%><TR>
<td valign=top align=left width=50%>
<h3><u>Table of contents</u></h3><p align=left>Title</p>
<p align=left>Introduction</p>
<p align=left>CERN E.</p>
<p align=left>Custom 1</p>
<p align=left>Custom 2</p>
<p align=left>Software</p>
<p align=left>Configuration</p>
<p align=left>(Few) Details 1</p>
<p align=left>(Few) Details 2</p>
<p align=left>Legal issues</p>
<p align=left>Red Hat Compatibility</p>
<p align=left>Current Status &amp; Availability</p>
<p align=left>A screenshot</p>
</td>
<td valign=top width=50%>
<p><strong>Author:</strong> Jaros&#322;aw Polok</p>
<p><strong>E-mail:</strong> <a href="mailto:Jaroslaw.Polok@cern.ch">Jaroslaw.Polok@cern.ch</a></p>
<p><strong>Homepage:</strong> <a href="http://cern.ch/linux">http://cern.ch/linux</a> </p>
<p>Download presentation: <a href="CERN_E_Linux_3_Preview.pdf">PDF</a>, <a href="CERN_E_Linux_3_Preview.sxi">SXI</a>
</p>
</td></tr></table></center>
</body>
</html>
