<!--#include virtual="/linux/layout/header" -->

<h2>minutes of the LXCERT meeting 21.04.2005</h2>


<h2>Invited / Attendance</h2>
<table border="1">

<tr><th>Abbrev</th><th>Name</th><th>Affiliation</th><th>Present</th></tr>

<tr><td></td><td>Alberto Aimar</td><td>LCG Application Area</td><td>missing</td></tr>
<tr><td>BB</td><td>Bruce M Barnett</td><td>ATLAS-online</td></tr>
<tr><td>AB</td><td>Alastair Bland</td><td>AB/CO-admin</td></tr>
<tr><td></td><td>Eric Cano</td><td>CMS-online</td><td>missing</td></tr>
<tr><td>JC</td><td>Joel Closier</td><td>LHCb-offline</td></tr>
<tr><td>NMN</td><td>Nicolas De Metz-Noblat</td><td>AB/CO-development</td><td>excused &rarr; AB</td></tr>
<tr><td>BG</td><td>Benigno Gobbo</td><td>non-LHC experiments</td></tr>
<tr><td>JI</td><td>Jan Iven</td><td>IT-services catchall (chair)</td></tr>
<tr><td>NN</td><td>Niko Neufeld</td><td>LHCb-online</td></tr>
<tr><td>EO</td><td>Emil Obreshkov</td><td>ATLAS-offline</td></tr>
<tr><td>JP</td><td>Jarek Polok</td><td>general Desktops (secretary)</td></tr>
<tr><td></td><td>Fons Rademakers</td><td>ALICE-offline</td><td>excused &rarr; KS</td></tr>
<tr><td></td><td>Thorsten Kleinwort</td><td>IT PLUS/BATCH service</td><td>missing</td></tr>
<tr><td>KS</td><td>Klaus Schossmaier</td><td>ALICE-online</td><td>late</td></tr>
<tr><td></td><td>Stephan Wynhoff</td><td>CMS-offline</td><td>missing</td></tr>
</table>

<p>Agenda
<ul>
<li>plans for the next CERN Linux version
<li>Version proliferation &amp; LCG collaboration (physics compiler)
<li>continuous SLC evolution &amp; test releases
<li>post-mortem of SLC3, changes to the process and LXCERT membership
</ul> 


<hr>
<h2>Issue1: plans for the next Linux version</h2>


<p><u>JI:</u> Position summary from the mails sent before the meeting:
<ul>
<li>SLC4 will be made available in any case for tests, but priority
	  unclear
<li>go to SLC5 directly, don't certify SLC4:
  LHCb/Marco, ALICE/Fons, ATLAS/David
<li>need SLC4 (and some libraries):
   ATLAS-online/Bruce, 
   LHCb-online/Niko,
   ALICE-online/Klaus (agree with ATLAS)
</ul>

  &rarr; need to define the scope of SLC4 software availability and
  priority to see whether a full certification is required or not
</p>

<p><u>JI:</u> 
  (assuming compiler/OS split is OK): library availability for -online
  becomes an intra-experiment issue, please coordinate internally and
  with library providers, via AF.

<p><u>JI:</u> what are the reasons for SLC4 in "production" for
anybody (e.g kernel-2.6 availability?), and when?

<p><u>AB:</u> NMN wants SLC4 in January 06, and would prefer to have LXPLUS with
  that since it then feels "fully supported" (can direct users to IT helpdesk
  etc). May not be a "hard" requirement.

<p><u>NN:</u> LHCb "slow controls" needs PVSS.. and compatibility would need
  testing (e.g. &gt;iostream.h&lt; needs to be available..)<br>
  [JI: explicit action with ETM required during SLC3 certification]<br>
  [NN: aside: several issues still outstanding, SIGCHLD in log, /tmp-mode 0777] 

<p><u>AB:</u> from Frank Schmidt (beam simulation): don't care about
    OS and batch capacity, need compiler (+ AFS capacity).

<p><u>BB:</u> 2.6 kernel isn't only point for ATLAS-online, stability is (release
    milestone in September 2006), unlikely to be in time if we only start
    a certification in summer 2006.

<p><u>JP:</u> last experience &rarr; 6month to certify software (deep chain).
    Please remember that compat-libs only work backwards for 1
    release, so SLC3&rarr;SLC5 will need (most likely) a recompile.
<p><u>BB:</u> staying on SLC3 completely forces "revolutionary change" instead
    of  "evolution"

<p><u>NN:</u> all the LHCb data acquisition is already running on 2.6 (on SLC3), only
    own online code (including kernel drivers) . Benefit from "clean"
    LHCb separation between online and offline<br>
[AB: have 2.6 driver experience? interested..] <br>
[mini diskless discussion &rarr; redirected to Linux4Controls/CNIC]]


<p><u>JI:</u> proposal: split by OS and compiler, certify in parallel, merge,
    decide quickly before the September 2006 deadline?

<p><u>BB/EO:</u> summer 2006 is too late for any new certification to start.
   In general agree that new compiler (on old OS) and new OS should go
   in parallel, then combine OS+compiler and run another round of tests.

<p>[NN: when will support for SLC3 be dropped?<br>
 JI: other way round, decide when new stuff is available+migration
    period etc., then phase out. RHE3 lifetime is until 2009 <br>
 JP: but hardware support may push us, RHE3 only gets new drivers in
    2005, and we don't want to carry too many versions around.]</p>

<p><u>JP:</u> could have SLC4 installable in "basic setup" within a
				    few weeks. Do we need this?<br>
    Announced changes: mostly 2.6 kernel, gcc-3.4.3, general package update.<br>
[diverge: EM64T support: looking at it (works also on SLC3, some trouble compat
 between Opteron/Intel (wrong kernel))]
<br>
[diverge: updating system on SLC4? apt (have coded some dependencies
					  against it)?<br>
 JP: don't know.. x86_64 already on yum, may go to yum by default on
					    SLC4 - this is an internal
					    Linux.Support choice]

<p><u>NN:</u> could use the cert infrastructure even for SLC4 if non-production?
<p><u>JI:</u> Agree, either do "real" certification or none at
						all, certification
						does not automatically
						mean deployment.

<p><u>JP:</u> could start certification "early" on FedoraCore4, as a alpha-test for Red Hat 5,
    but current rate of changes is horrible (FC4 is still in beta, will
    cool down later). Useful for porting tests only (versus announced
    changes), random runtime breaks could occur and would need to be
    ignored.<br>
<u>AB:</u> would be mostly interested in commercial applications + Java<br>
<u>JI:</u> won't work now on FC4<br>
<u>JP:</u> ORACLE will support RHE4+5, but significant lag for native
   libraries, relies on compatibility environment until then.

<p>
[AB: assume SLC4: new control room (100 PCs about 60 Linux with
    multiscreen+NVidia): will the close-source driver model continue
    to work?<br>
 JP/JI: yes]


<p>back to basic question - SLC4 or not?</p>

<u>JI</u> summary:
<ul>
<li> need "backup" stable SLC4 in any case.
<li> rolled-out SLC4 makes migration to SLC5 easier (also for experiment)
</ul>

<p><u>JP:</u>
when is the last release date (fully certified, ready for deployment
  etc) of SLC5 that would get it into the LHC startup phase ?

<p><u>AB:</u> 1st November 2006 <b>(general agreement, also from ATLAS,
   this is a HARD limit for AB-CO and LHCb)</b>

<p>
[ AB: other changes that could go into next release:
     SingleSignOn for Windows, shared home dir on DFS?
     (would be nice for whole AB, Fermi has done SSO?)<br>
 JP: AFS not technically required for desktops/laptops,
     but depends on software environment. Password sync : see
     Linux4Controls?<br>
     DFS: No 'supported' DFS client on Linux...<br>
 JI:  cert=time for changes, please let us know, can discuss later.]


<p><u>JI:</u> explains situation to be discussed at HEPiX (May 09):
SL(C)3 now is available world-wide (good for sites + experiments),
would like to keep this situation => need sites + experiments to agree
on timescale for next release.<br> Experiments: Please contact
non-CERN sites and get requirements/deployment plans. Need to match
experiment and site plans.


<p><u>AB:</u> assumption of 'Laptop=Desktop=control machine' is nice (compile
    once, run directly), please keep if possible.<br>
<u>JI:</u> still current working assumption:
    laptop=desktop=PLUS=BATCH=online/controls/servers.
    but actual roll-out may be staggered.
<p>

<b>Conclusion:</b>
<ul>
<li> Split compiler and OS certification. Can in principle mix &amp; match,
  but system compiler is somewhat preferred for 3rd-party compatibility

<li>SLC5 would be nicer to have for LHC startup than SLC4, but carries a
  risk   => SLC4 needs to be fully &amp; formally certified in 3Q2006

<li>if RHE5 looks promising (already from FedoraCore4/5 and the
  RHE5-beta) and is on time, look whether it can get fully certified
  with whatever compiler until October 2006.

<li>October 2006: decide which OS version + compiler version to deploy
  for LHC startup.

<li> only "test" SLC4 batch capacity (probably) required until that
  decision

<li> sometime before end of 2005, SLC4 should be a fully-supported OS 
  (available for network installs, updated, helpdesk etc)
</ul>

<hr>
<h2>Issue2: Version proliferation & LCG collaboration</h2>
 (intro by JI)

'LHC AF decides on compiler for non-LHC experiments'-issue:

<p><u>BG:</u> bad, but have to see facts:<br>
    COMPASS is only running experiment in 2006<br>
    HEP is disbanded, e.g. L3 has no more people<br>
    non-LHC in general has no manpower to either port or test. 
      = "no changes" preferred, but not always possible.<br>
    concerns only offline &rarr; mostly compiler issues, if libraries are
    available - e.g. CERNLIB widely used, availability is
											  critical..<br>

[JC: confirms, nothing to do for ALEPH on SLC3 since gcc-3.2.3 was
already present]

<p><u>JI:</u>
<ul> 
<li>no manpower for tests &rarr; no veto even if formal right to do so
<li> LHC will have huge overlap in terms of requirements (FORTRAN,
       libraries etc) &rarr; will work most of the time 
    <li> non-LHC will still be "normal" client to Application Area/Compiler
       provider (just like AB), and can escalate via hierarchy if they
       feel treated unjustly
</ul>
<p>
<b>Conclusion:</b> compiler/OS split is OK.

<hr>
<h2>Issue2bis: continuous SLC evolution & test releases</h2>

(not discussed, shelved for now)

<hr>
<h2>Issue3: post-mortem of SLC3, changes to the process and LXCERT membership?</h2>

<p>(some membership changes already happened before the meeting)</p>

<p>Issue:<b>light-weight certification vs formal exercise:</b>
<p><u>NN:</u> informal tests could be nice?
<p><u>JI:</u> results are useless unless some outcome is recorded (=formal yes/no, binding),<br>
    certification infrastructure is still useful,<br>
     deployment & certification can be decoupled

<p>
<b>Conclusion:</b> keep current certification infrastructure for now.

<p>Issue:<b>SLC3 post-mortem</b>
<p><u>JI:</u> (quick rundown, will send details per mail):
 Remember: SC3 was 12 month late!<br>
[AB: but actually like the November release time]<br>
 Lost time with Red Hat negotiations (~6 months) and due to summer<br>
 (key people absent) and 'blocking' chain August-October:<br>
 CMS (and LHCb and ATLAS, but they didn't veto)  &rarr; POOL &rarr; cxxabi bug.<br>

<p>(Trivial) lessons:
<ul>
  <li> any cross-site negotiations take long
  <li> summer is bad for certifications (issue for SLC5!)
  <li> need to break down dependency chains for quick certifications (e.g
    certify "production" scripts independently from working jobs?) 
</ul>

<hr>

<H2>AOB:</H2>

<p><u>AB:</u> would like effort from IT/ORACLE for SLC4: one defined ORACLE
    client production version (compatible with system compiler),
    need Pro-C. If client distributed via AFS,
    please on replicated volumes..<br>
<u>JI:</u> need exact requirements, will forward. Explained split Physics-DB
    (tied in via AppArea) / non-Physics (same situation as before).
</p>

<p><u>BB:</u> should we open 32 or 64 discussion?<br>
<u>JI:</u> forecast: all current 3 platforms will need to be supported on OS level,
   may drop IA64 in 2-3years,
   decision on priorities/batch capacity is with AF/IT task force, after
   performance evaluation

<p><u>AB:</u>On the desktop, will we get Opterons as least as
choice from PCshop?

<p><u>JP:</u>will stay on 32bit for this year, and have additional
delay for PC purchases anyway: need to redo market survey &rarr;
nothing moves before FC in November(?)

<hr>

<h2>Actions:</h2>

<p><u>JI:</u> send post-mortem summary
<p><u>JI:</u> send minutes
<p><u>all:</u> continue discussion via mail, get external site requirements
<p><u>JI/JP:</u> report from HEPiX

<p><u>JP:</u> (slowly) set up SLC4 for initial tests.



