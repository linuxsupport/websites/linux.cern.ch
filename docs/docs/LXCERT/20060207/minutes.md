<!--#include virtual="/linux/layout/header" -->
</head>


<body>

<h2>Minutes of the LXCERT meeting 07.02.2006</h2>

<h2>Invited / Attendance</h2>

<table border="1">

  <tbody>

    <tr>

      <th>Abbrev</th>

      <th>Name</th>

      <th>Affiliation</th>

      <th>Present</th>

    </tr>

    <tr>

      <td>AB</td>

      <td>Alastair Bland</td>

      <td>AB/CO-admin</td>

      <td>(missing)</td>

    </tr>

    <tr>

      <td>AP</td>

      <td>Andreas Pfeiffer</td>

      <td>LCG Application Area</td>

      <td></td>

    </tr>

    <tr>

      <td>BB</td>

      <td>Bruce M Barnett</td>

      <td>ATLAS-online</td>

      <td></td>

    </tr>

    <tr>

      <td>BG</td>

      <td>Benigno Gobbo</td>

      <td>non-LHC experiments</td>

      <td>(only beginning, VRVS/phone)</td>

    </tr>

    <tr>

      <td>EC</td>

      <td>Eric Cano</td>

      <td>CMS-online</td>

    </tr>

    <tr>

      <td>EO</td>

      <td>Emil Obreshkov</td>

      <td>ATLAS-offline</td>

    </tr>

    <tr>

      <td>FR</td>

      <td>Fons Rademakers</td>

      <td>ALICE-offline</td>

      <td>(excused)</td>

    </tr>

    <tr>

      <td>JC</td>

      <td>Jo&euml;l Closier</td>

      <td>LHCb-offline</td>

    </tr>

    <tr>

      <td>JI</td>

      <td>Jan Iven</td>

      <td>IT-services catchall (chair)</td>

    </tr>

    <tr>

      <td>JP</td>

      <td>Jaroslaw Polok</td>

      <td>general Desktops (secretary)</td>

    </tr>

    <tr>

      <td>KS</td>

      <td>Klaus Schossmaier</td>

      <td>ALICE-online</td>

    </tr>

    <tr>

      <td>NMN</td>

      <td>Nicolas De Metz-Noblat</td>

      <td>AB/CO-development</td>

      <td>(missing)</td>

    </tr>

    <tr>

      <td>NN</td>

      <td>Niko Neufeld</td>

      <td>LHCb-online</td>

    </tr>

    <tr>

      <td>SW</td>

      <td>Stephan Wynhoff</td>

      <td>CMS-offline</td>

      <td>(missing)</td>

    </tr>

    <tr>

      <td>TK</td>

      <td>Thorsten Kleinwort</td>

      <td>IT PLUS/BATCH service</td>

      <td></td>

    </tr>

  </tbody>
</table>

Date: Tuesday 07.02.2002, 10:00
Place: 40-R-D10
VRVS: "Sky" room
<h2>Agenda</h2>

<ul>

  <li>SLC5 delay impact - discussion, new plan.</li>
  <li>SLC4 certification status</li>
  <li>initial SLC4 roll-out timeline (to be coordinated with GDB)</li>
  <li>SLC3 support lifetime (per architecture)</li>
  <li>AOB</li>

</ul>

<p>Due to technical problems with VRVS, BG was just able to make
a general statement about the concerns of the non-LHC experiments --
these have little or no manpower for OS migrations and would like to
see such changes to be kept to a minimum.</p>

<p>The slides of JI's presentation are available <a href="SLC4+5-status/SLC4+5-status.html">here</a> (<a href="SLC4+5-status/SLC4+5-status.pdf">PDF</a>, <a href="SLC4+5-status/SLC4+5-status.ppt">PPT</a>).</p>

<h2>Summary</h2>

<p> The proposed certification schedule with a deadline at the
end of March 2006 was accepted, it was agreed that no further slips
should occur. SLC4 certification will get delayed again only if major
issues are actually found.</p>

<p>It was also agreed that the Grid Deployment Board (GDB) needs
to be tightly involved in the actual SLC4 rollout coordination.</p>

<p>Target date for switching the current "LXPLUS" alias to SLC4:
October 2006.</p>

<p>proposed end-of-life timelines for SLC3 were accepted.</p>

<h2>RHEL5/SLC5 delay</h2>

<p>CERN and Fermi now expect Red Hat Enterprise Linux 5 (RHEL5)
to not be available before the end of 2006, a CERNified version (SLC5)
would only be available in 1Q2007 (i.e. too late for LHC startup). This
means the previous plan of just certifying SLC4 but no rolling it out
widely is no longer viable.</p>

<p>The current proposal now is to use SLC4 as the "LHC startup
release", and roll out SLC4 in production mode (i.e. switch the
"LXPLUS" alias to SLC4) until autumn, with the GDB being strongly
involved. Batch capacity migration will be done in stages according to
the experiments' requirements. This proposal has been accepted.</p>

<p>[update from BG after the meeting:
<ul>
  <li>non-LHC experiments "suffer" from any system/compiler
migration due to lack of manpower,</li>

  <li>but they understand the need of upgrades and consider a
frequency of about 2 years to be reasonable.</li>

  <li>whether to go from SLC3 to SLC4 or to SLC5 does not affect
these considerations.</li>
</ul>
]
</p>

<h2>SLC4 status: IT</h2>

<p>Linux Support proposes to fold in the pending changes from
RHEL4U3 (currently in beta). Several tools are not fully certified yet,
but we have encountered no major stumbling blocks. <b>TK</b>:
SLC4 PLUS/Batch test machines need a few weeks more to set up.
Kerberos5+ssh interoperability still needs more work. The focus of the
IT tests has been on i386, wider tests on EM64T/AMD64 are still
required.</p>

<p><b>JP</b> announced that the default package
updating tool on i386 will go from the current "apt" (for RPM) to
"yum", as is already the case for the 64bit platforms.</p>

<h2>SLC4: non-IT status updates</h2>

<p><b>NN</b> pointed out that CASTOR client libraries
are now required in order to go forward with the certification. <b>JC</b>
asked whether these are CASTOR-1 or CASTOR-2 [they are CASTOR-2], and
whether they would allow to connect to the current production services
that run CASTOR-1 [which they should].</p>

<p><b>EC</b> asked whether SLC4 will stay with the
current 2.6.9-* kernel, as some external projects (iSCSI) have patches
available only for newer releases. 2.6.9 is very likely to stay the
default kernel in SLC4, this has the positive side effect that online
drivers don't need to be ported to newer kernel releases continuously
[for the specific case of iSCSI, the Red Hat kernel (and hence SLC4)
should have a usable backported version].
<b>EC</b> also asked for the status of Quattor/SPMA for
multiarch systems (like EM64T/AMD64). While this is only currently
being tested, no major issues are assumed given that SPMA is rather
close to the RPM libraries, and does not do dependency resolution (a
possible source of problems) anyway.
</p>

<p><b>AP</b> send the following summary for LCG AA
and CERNLIB:</p>

<p>For the 3.4.4 compiler on SLC3, the compiler installation in
AFS
which was used for the LCG AA packages is located at:
<tt>/afs/cern.ch/sw/lcg/contrib/gcc/3.4.4/slc3_ia32_gcc344</tt>
(and, for the AMD 64 architecture:
<tt>/afs/cern.ch/sw/lcg/contrib/gcc/3.4.4/slc3_amd64_gcc344</tt>).
To set up the environment, please follow the instructions at
the end of: <a href="https://twiki.cern.ch/twiki/bin/view/SPI/FindBuildServers">https://twiki.cern.ch/twiki/bin/view/SPI/FindBuildServers</a></p>

<p>cernlib (2005 source with security patches from debian folks):</p>

<ul>

  <li>build and tested ok on slc3 3.4.4 (using libshift from
3.2.3)
  </li>

  <li>tests on slc4 ran ok with slc3/344 build
  </li>

  <li>will be installed once libshift.so is available (few days)
  </li>

</ul>

<p>LCG-AA</p>
<ul>
<li>port to gcc 3.4.4 on slc3 done (ia32 only so far; amd64 still has  
problems)</li>
<li>don't expect problems for slc-4 on 32 bit arch</li>
<li>do expect priority to raise soon (experiments interest)</li>
<li>guesstimate: builds will start to be done after CHEP (end Feb)</li>
</ul>



<p>There was a quick discussion on whether the AF compiler
(SLC3+gcc-3.4.4) would need to be deployed locally for performance
reasons; agreement was that for the upcoming tests the distribution via
AFS is expected to be fine, given that this is an interim test until
the 'natively' recompiled LCG environment becomes available. This could
be reviewed if this 'native' version is delayed or if actual
performance severely impacts the tests.</p>

<p><b>EO</b> summarized the status for ATLAS-offline:
they require GAUDI for which tests with the SLC3+gcc-3.4.4 compiler are
under way. Some concerns about the current priority/little available
manpower for these tests.</p>

<p><b>JC</b> confirmed that LHCb-offline were just
waiting for the LCG software and the CASTOR client, they are ready to
start tests.</p>

<p><b>NN</b> explained the LHCb-online strategy, the
embedded machines moved from 7.3 to SLC4 in one go. Non-visible machines and
infrastructure have also gone to SLC4 already, except for Control/Testbeam (dependency on PVSS and CASTOR-client) and  filter farms that need the offline code and will follow
LHCb offline plans (these machines are running diskless and can be
migrated very quickly if needed). The current "Quattorification"
(through LinuxForControls/CNIC) of the online nodes is stuck on
configuring diskless clients.</p>

<p><b>EC</b>: CMS-online has only 4 test machines
with SLC4 but is building a new test cluster. The kernel drivers
haven't been ported yet to 2.6. Ongoing physics activity (cosmics)
means that little may happen in this area until May. </p>

<p>[a short breakout discussion ensued on the requirement to have
the "bigphysarea" patch in SLC4 (used by {ATLAS,CMS,ALICE}-online) -
this patch is currently in the SLC4 kernel, but its usage should be
reviewed, it is unclear whether it can be ported to future versions]</p>

<p><b>KS</b>: ALICE -online has done some SLC4 tests,
no major obstacles seen. Their drivers have been ported to 2.6.9, and
both 32bit and 64bit versions are OK. They will re-test after the
latest changes have been included. Not a formal statement (need <b>FR</b>,
but ALICE-offline doesn't seem to have stuck major problems either.</p>

<p><b>BB</b> explained that ATLAS-online has done an initial port of its custom
drivers to 32bit-SLC4, kernel 2.6, and that the
higher-level code has been compiled against SLC3+gcc-3.4.4 LCG libraries
(some issues found). They plan to have SLC3+gcc-3.4.4 as part of their
code release in March 2006. When LCG on SLC4 is ready, SLC4+gcc3.4.4
will be included in the nightly builds, and should be available in a
formal release by about May (tentative.) Their goal is that SLC4 will be
the standard platform for the September ATLAS-offline milestone -
details scheduling of SLC4 deployment during 2006 at point 1 will depend
on requirements imposed by testing and cosmic running.</p>

<h2>SLC3 lifetimes</h2>

The proposed timelines were agreed on:
<ul>

  <li>support for SLC3 on ia64 and amd64 should stop at the end
of 2006</li>

  <li>support for SLC3 on i386 will stop end of October 2007
(current SL3 end of life)</li>

</ul>

<h2>AOB</h2>

<ul>

  <li>"does CVS work with Kerberos?": yes, incompatibility was
found and should be resolved.</li>

  <li>"will the HEPiX scripts go away?": not foreseen for now,
they have been ported and are currently maintained. Still need to
provide documentation for LHCb, both a high-level design document and a
    <tt>man</tt> page will be required. [ the (ancient)
documentation at <a href="http://cern.ch/wwwhepix/wg/scripts/www/shells/">http://cern.ch/wwwhepix/wg/scripts/www/shells/</a>
is still largely valid, and <a href="http://cern.ch/Peter.Kelemen/talk/2003/HEPiX/hepix/kelemen-2003-HEPiX-hepix.pdf">Peter
Kelemen's presentation to HEPiX 2003</a> has an high-level
overview. The need for a <tt>man</tt> page has been
acknowledged.]</li>

  <li>next meeting: target is 3rd week of March</li>

</ul>


