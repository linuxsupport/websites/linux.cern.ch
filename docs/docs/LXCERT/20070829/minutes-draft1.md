<!--#include virtual="/linux/layout/header" -->
</head>

  <body>
    <h2>Minutes of the LXCERT meeting 29.08.2007 - DRAFT</h2>


<h2>Invited/Attendance</h2>

<table border="1">
<tr>
<th>Name
<th>Affiliation
<th>Present?
<tr><td><a href="mailto:Bruce.Barnett@cern.ch">Bruce M Barnett</a></td>
    <td>ATLAS-online</td>  

<tr><td><a href="mailto:Alastair.Bland@cern.ch">Alastair Bland</a></td>
    <td>AB/CO-admin</td>

<tr><td><a href="mailto:Eric.Cano@cern.ch">Eric Cano</a></td>
    <td>CMS-online</td>

<tr><td><a href="mailto:Joel.Closier@cern.ch">Joel Closier</a></td>
    <td>LHCb-offline</td>
    <td>(absent)</td>

<tr><td><a href="mailto:Benigno.Gobbo@cern.ch">Benigno Gobbo</a></td>
    <td>non-LHC experiments</td>
    <td>(excused)</td>

<tr><td><a href="mailto:Louis.Poncet@cern.ch">Louis Poncet</a></td>
    <td>Grid middleware</td>
    <td>(replaced by Gergeli Debreczeni)</td>

<tr><td><a href="mailto:Jan.Iven@cern.ch">Jan Iven</a></td>
    <td>IT-services catchall (chair)</td>

<tr><td><a href="mailto:Niko.Neufeld@cern.ch">Niko Neufeld</a></td>
    <td>LHCb-online</td>
    <td>(absent)</td>


<tr><td><a href="mailto:Emil.Obreshkov@cern.ch@cern.ch">Emil Obreshkov</a></td>
    <td>ATLAS-offline</td>
    <td>(absent)</td>


<tr><td><a href="mailto:Andreas.Pfeiffer@cern.ch">Andreas Pfeiffer</a></td>
    <td>LCG Applications Area</td>  
    <td>(absent)</td>


<tr><td><a href="mailto:Jaroslaw.Polok@cern.ch">Jarek Polok</a></td> 
    <td>general Desktops (secretary)</td>


<tr><td><a href="mailto:Fons.Rademakers@cern.ch">Fons Rademakers</a></td>
    <td>ALICE-offline  </td>
    <td>(absent)</td>


<tr><td>N.N.</td>
    <td>IT PLUS/BATCH service</td>
    <td>(replaced by Ulrich Schwickerath)</td>

<tr><td><a href="mailto:Klaus.Schossmaier@cern.ch">Klaus Schossmaier</a></td>
    <td>ALICE-online</td>
    <td>(absent)</td>

<tr><td><a href="mailto:Vincenzo.Innocente@cern.ch">Vincenzo Innocente</a></td>
    <td>CMS-offline </td>

<tr><td>Peter Kelemen</td>
    <td>Linux Support</td>
</table>


<hr>


<h2> Agenda</h2>
<ol>
 <li>Presentation: Overview of possible options for SLC5 by Linux Support (<a href="Linux-LXCERT-08.2007-jpolok.pdf">slides</a>)
 <li>Discussion
</ol>

<hr>

<H2>JP: Overview of possible options for SLC5</h2>
<!--
<pre>
* crossover between 3 and 4 happend only a few months ago
* experiment sw not to be migrated before mid 2008

need feedabck, not all experiments have replied.


problem:
SLC4 is too old for new HW, will not get new HW support. New is that
even mid-old stuff (>9month) is not well supported.

could drop desktop support:

Q- vincent: what about managed desktops?
A: only have this for dedicated user groups/L4C

could have new kernel./X11 while keeping old runtime compat
lots of work, only-CERN -no more SL 

could have somethign newer SL5 without exp cert

could have SLC5+full experiment only from AFS (+native Grid)

then add fully-certified SLC5 in mid-2008.

--Need feedback on dates.



Vincent:

in the past had many services migrated to SLC4 before certification
was over. will we have service running SLC5 without full cert? would
this save money? (that could then bve used to offset cost of SLC5
support). Jan: clarify - no pressure from anybody except
laptop+desktop, rest works (or has been made to work)








JareK:laptop slide



Vincenso: in principle, Ubuntu analysis is invalid, and results could
be disputed. Is Xen actually a feasible?

agreement: yes - don;t need to re-certify, this is an exisitng solutions


Ulrich: SLC4 a problem for BATCH machines? 
JP: not now (and until March 2008)


ABCO: worriede about future purchases (HP, ). SLC4 would be ancient in 2008.
WOuld like supported SLC5 in 5th DEcember. Will buynew stuff in 2008
(DC7700 is used, DC7800 wuld be good candiate, or )
Woudl run (50) consoles on SLC5.
ABCO: needs "oracle-instantclient", YUM-DAG, old compiler, auto
install.


Vincenco: no SLC5 advantage from offline pont-of-view (SLC4 was celar:
64bit, chosen compiler; switched in winter 2007, and already ran SLC3
on 4 for some time). 
compiler: effort is spent on gcc-4.1 already (4.2 wouldbe even better,
speedup). WOuld be able ti run SLC4+gcc-4.1, full chain is there (not
C++-binding against LCG SPI)



Jarek: wider issue - is certification process needed at all

Vincenco: experiments need to make sue that env runs, also for other
sites (ex. 64bit, and issues from 32/64 mixing - ony worked this summr
to get ful stack crosscompiled fom 64 to 32).


Eric: what should we ecxpect going from SLC4->5
new kernel.
gcc-3.4 -> gcc-4, somewhat stricter on templates (Vincenzo: gcc-32 to
34 was worse)


Bruce: long-term view, SLC6 will come, jump from 4->6 will be more
difficult than 4-5-6
propose to make soething available so that people can evaluate.


GD: slc5 without certification ? HW, and could start porting
Grid. SLC4 was bad example (2 services in 6 month), WOULD LIKE TO GET
HEADTSTART GOR slc5

But SL5 is available. But would need perspective that migartion will
come, to increase priority

Vnz: already gcc-4.1 being tested, effort is spend. Estimate that
<1FTE/6months is sufficient for gcc-3,4_.gcc-4.1



JP: want commitment. Need 
AB: would put on console if LinuxSupport considers it stabl;e.


me: propose to have SLc5-light- add-on repo

me: need to have users to push us.

VInc: whats the cost to stay on SLC4? vs cost of migration?
ne: cost of  old SLC4 is borne by IT

Vinc: big switch is gfortran instead of g77 (but available from compat
package). 

ABCO: want CERNLIB+paw (and libshift)

me: woudl do normal certification, i.e use olf SLC4 dep lists

ABCO: firefox on SLC4, no SVG




Summary:
want clear mandate from community:
- go for SLC5 (with eventual deployment envisioned+promised)
- stick to SLC4 (and be prepared to suffer the consequences)

want deadlines

ABCO: PVSS, with Linux client
labview - already tested
jvm-1.6 - SUN
activeMQ??

SoniqMQ will go.
LEMON

Vinc: push needs go come 
-->

<p>The meeting started with <i>JP</i> giving an overview of the situation of
Linux at CERN:

<p> Despite having been certified more than a year ago, SLC4
has just overtaken SLC3 in terms of deployed boxes at CERN, and some
services still are not ready for SLC4.

<p>A possible SLC5 certification (as per the usual schedule of every
1.5-2 years) would take at least 6 month, and would therefore extend
into the hectic period where everybody is busy with LHC startup. At
the same time, no user group has been actively pushing for a newer
compiler or a general software update (VI: "no incentive", unlike
SLC3->4), it is therefore unlikely that the required resources for a
full certification of experiment software could be found (=more
delay).

<p>Without such a certification, a future SLC5 could not be rolled out on
the most permanent=directly user-visible services, and neither on the
desktop (assuming the need to have a "certified" desktop for
development or analysis). Without a wide roll-out pending, there would
be no incentive for other services to certify or migrate. Supporting
SLC4+SLC5 without agreed end dates is too expensive for Linux.Support.

<p>However, it becomes clear that SLC4 is near the end of its lifetime
with respect to hardware support: Compatible laptops are no longer
being sold on the market, compatible desktops are already difficult to
get by (server or batch machines still pose no major issue yet, but
this is expected to change in 2008).

<p>The pressure to migrate off SLC4 comes therefore from
hardware-purchasing groups (IT, AB/CO to some degree, perhaps
experiment "online" groups).

<p><i>JP</i> presented the available options (full certification of SLC5,
CERN-specific kernel/X11 updates, non-certified SLC5, "physics"
environment decoupled via AFS, ..), none of which would resolve the
disparity between need for new hardware vs time and resource
constraints for software/service providers.


<p>As a special case, the "Linux laptop" hardware support problems were
highlighted. This is an area where the current stability-oriented
certification model clearly does not work, due to short model
lifetimes. The current utilization of SLC on laptops appears to be low
both in relative (~20% of "Linux laptops" as per LanDB, &lt;30% on the
exemplary HP NC6400) and absolute numbers (~40 non-NICE HP NC6400; 150
DHCP updaters on linuxsoft).  The effort required to properly support
such hardware is not justified by this utilization. The proposal would
be to withdraw formal laptop hardware support (no "compatible" model
in the stores, no upfront tests) and let individual users pick their
favorite models based on community experiences, possible with a
"certified" OS in a virtual environment, and some guidance from IT to
make things work in the CERN environemnt. If required, such a model
could be extended to Linux desktops later.

<p style="margin-left: 40px;">
  This will need wider discussion (e.g. at DTF), but the participants
  of LXCERT were asked to provide feedback from their user
  communities.</p>
  

<h2>Discussion</h2>


<p><i>VI</i> asked whether "hidden" services (web/disk servers etc) would be in
a position to migrate to SLC5 even without a fully-certified
version. But the incentive for such early migrations (except for hardware
support) is currently low, no major performance/stability gains are
expected from SLC5.


<p><i>VI</i> asked for a cost-based approach - cost for SLC5
certification+rollout (e.g. to the experiments) vs SLC4 penalty for
IT/other hardware purchasers/users. While certification costs can be
estimated based on previous experiences, cost caused by
"legacy-compatible" hardware are unknown (i.e. no offers received; vague
guesses for cost of backporting hardware support), as are those caused by
lost productivity. The disparity of SLC4 vs SLC5 costs was clear - one
is largely on IT/LinuxSupport, the other on everybody.


<p><i>AB</i> was worried on the implications for staying on SLC4 for future
(2008) purchases - AB/CO is already now experiencing the same problems
as IT in finding compatible "desktop" models (e.g. for the CCC
consoles). They would be willing to move to SLC5 if it would provide
better hardware support and would be available in "IT-supported" form before
December 2007, including some "vital" packages (ORACLE-instantclient,
YUM+DAG, LabView, PVSS, perhaps JVM; CERNLIB et.al. preferred)


<p>The changes between SLC4 and a potential SLC5 were discussed. The
compiler change (gcc 3.4 -> 4.1) was expected to be at most as
difficult as the 3.2->3.4 change (some more restrictions on C++, but
ABI-compatible; several groups started to test it already), but
gfortran could be a problem (g77-gcc-3.4 is still available). Several
short/informal tests (AB/CO,CMS,EGEE,IT) with SL(no-C)5 have been done and
hint that most things should just work.


<p>The topic of results from non-certified SW stacks were mentioned - VI
indicated that in principle e.g. analysis results from a Ubuntu
machine (uncertified stack) could not be accepted by an
experiment. The question was posed whether virtual machines (Xen)
would need explicit re-certification (since kernel and glibc are
different), but the agreement was that this would a priori not be
necessary.


<p>There was some agreement that IT would need to provide some minimal
testable SLC5 environment if anything else was to happen (with AB/CO
eventually using this for production). <i>JI</i> requested a strong/formal
demand for this for the user groups to justify the cost (this would
imply a willingness on the users' side to spend effort on testing, and
eventually permit deployment). <i>BB</i> pointed out that even a
non-certified-but-tested SLC5 would ease the transition to a later
SLC6.

<p><i>GD</i> would like to have a SLC5 test platform (have SL5) as soon as
possible (given the long delays in certifying/porting to a new
platform, see SLC4 saga). Similarly to Linux.Support, some user
demand/pressure would be required to justify spending the porting
effort.


<p><i>JP</i> opened the wider discussion for whether the current CERN
certification model was still working or was still required. VI
pointed out that some kind of certification would even be required
within each experiment to be able to accept results (mentioned
32/64bit compatibility issues across sites), the current wide
compatibility RHEL/SL/SLC was appreciated since it limited effort. The
need to coordinate changes to the OS goes beyond CERN (LCG Architect Forum for
compiler choice and HEP sites for "Grid capacity rollout" were
mentioned), the current CERN model isn't ideal but part of the change
coordination infrastructure. Several "chicken-and-egg"-issues exists
and threaten to lock us into SLC4 until hardware issue becomes overwhelming
even for software providers.



<h2>Summary:</h2>

<ul>
<li>SLC5 is not perceived to be a burning issue by users not directly charged with hardware purchases.</li>

<li>Linux Support would need a clear request from LXCERT to open up a (supported) SLC5, even as "just" a  test service. IT_GD needs a similarly clear mandate for porting Grid software.</li>

<li>LXCERT members need to provide feedback if and when they would be willing to look at SLC5 for certification and eventual deployment (time constraints need to be made explicit)</li>

<li>LXCERT members need to provide feedback on the proposal to remove "SLC-compatible" Linux laptops</li>

</ul>


