<p style="font-size:smaller">as of $Date: 2009/03/06 10:22:45 $</p>
<table border="1">
<tr>
<th>Name
<th>Affiliation
<tr><td><a href="mailto:Bruce.Barnett@cern.ch">Bruce M Barnett</a></td>
    <td>ATLAS-online</td>  

<tr><td><a href="mailto:Alastair.Bland@cern.ch">Alastair Bland</a></td>
    <td>AB/CO-admin</td>

<tr><td><a href="mailto:Eric.Cano@cern.ch">Eric Cano</a></td>
    <td>CMS-online</td>

<tr><td><a href="mailto:Joel.Closier@cern.ch">Joel Closier</a></td>
    <td>LHCb-offline</td>

<tr><td><a href="mailto:Benigno.Gobbo@cern.ch">Benigno Gobbo</a></td>
    <td>non-LHC experiments</td>

<tr><td><a href="mailto:Louis.Poncet@cern.ch">Louis Poncet</a></td>
    <td>Grid middleware</td>

<tr><td><a href="mailto:Jan.van.Eldik@cern.ch">Jan van Eldik</a></td>
    <td>IT-services catchall (chair)</td>

<tr><td><a href="mailto:Niko.Neufeld@cern.ch">Niko Neufeld</a></td>
    <td>LHCb-online</td>

<tr><td><a href="mailto:Emil.Obreshkov@cern.ch@cern.ch">Emil Obreshkov</a></td>
    <td>ATLAS-offline</td>

<tr><td><a href="mailto:Stefan.Roiser@cern.ch">Stefan Roiser</a></td>
    <td>LCG Applications Area</td>  

<tr><td><a href="mailto:Jaroslaw.Polok@cern.ch">Jarek Polok</a></td> 
    <td>general Desktops (secretary)</td>

<tr><td><a href="mailto:Fons.Rademakers@cern.ch">Fons Rademakers</a></td>
    <td>ALICE-offline  </td>

<tr><td><a href="">N.N.</a></td>
    <td>IT PLUS/BATCH service</td>

<tr><td><a href="mailto:Roberto.Divia@cern.ch">Roberto Divia</a></td>
    <td>ALICE-online</td>

<tr><td><a href="mailto:Vincenzo.Innocente@cern.ch">Vincenzo Innocente</a></td>
    <td>CMS-offline </td>
</table>
