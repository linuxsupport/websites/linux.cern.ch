# Single User Mode [8 family or newer]

If you require to boot into Single User Mode you may follow these instructions:

1. Boot the system and at the grub menu, press 'e' to edit the kernel boot parameters
2. Move to the 'linux' line and add to the end of the line 'init=/bin/bash'. Press Ctl+x to boot the kernel with this parameter
3. Once you have a root shell; run the following commands:

```bash
/usr/sbin/load_policy -i
mount -o remount,rw /
passwd root
```

To continue the boot you can either run

```bash
exec /sbin/init
```

or, reboot the system with

```bash
/sbin/reboot -f
```

**Note**:

If you omit the 'load_policy' step and SELinux is enabled - you will not be able to log in, even though your password change may have been successful. If this has happened to you, execute the following commands from the root shell (repeating steps 1,2 above)

```bash
mount -o remount,rw /
touch /.autorelabel
/sbin/reboot -f
```

