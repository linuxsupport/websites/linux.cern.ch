<!--&#35;include virtual="/linux/layout/header" -->
# CERN Private Cloud operating system images: CC7/C8

<hr>
<h2>CERN Private Cloud operating system images: CC7/C8</h2>
CERN runs <a href="https://www.openstack.org/">OpenStack</a> Private Cloud infrastructure: for information please consult <a href="http://clouddocs.web.cern.ch/">documentation</a>
<p>

<hr>
<h2>Available images</h2>
We provide images of CERN supported Linux versions:
<ul>
 <li><a href="/centos8/">CentOS 8</a> / x86_64
 <li><a href="/centos7/">CERN CentOS 7 (CC7)</a> / x86_64
</ul>
these images can be selected for installation from <a href="https://openstack.cern.ch">OpenStack user interface</a> while setting up your virtual system instance. Look for image names following this schema:
<ul>
 <li>C8 - x86_64 [YYY-MM-DD]
 <li>CC7 - x86_64 [YYYY-MM-DD]
</ul>
where YYYY-MM-DD correspond to year-month-day of release.
<p>
More information can be found in the <a href="https://clouddocs.web.cern.ch/details/standard_images.html">Cloud user guide</a>, including how to access older images.

<hr>
<h2>Images content</h2>
These images are provided as a base for further customization:
<ul>
<li>provide CERN standard preconfiguration for non-puppet managed installations.
<li>contain rather minimal operating system installation.
<li>use single root partition layout with default filesystem.
<li>root filesystem is expanded to full disk capacity.
<li>do not define swap partition.
<li>automatically update installed packages.
<li>both IPv4 and IPv6 is enabled using DHCP (availability depends on hypervisor configuration).
<li>selinux is enabled.
<li>firewall is enabled and opened for ports: 22/tcp (ssh), 7001/udp (afs3-callback), 4241/tcp (arcd / acrontab).
<li>all standard updates yum repositories are enabled.
<li>only command line tools are installed.
<li>single image is provided for both puppet and non-puppet installations.
</ul>

<hr>
<h2>Images configuration</h2>
For installations to be managed by CERN Configuration Management (<a href="https://puppet.com/">Puppet</a>), please refer to <a href="https://configdocs.web.cern.ch/configdocs/">documentation</a>. (paragraph below does not apply to puppet managed systems)
<p>
Following is preconfigured on initial startup of the system from the image:
<ul>
<li>System update
<li>Kerberos
<li>Sendmail
<li>Root access with ssh key and kerberos for system owner (*)
</ul>
(*) - according to information found in <a href="https://network.cern.ch">LanDB</a> for the system.

<hr>
<h2>System configuration and management</h2>
For installations to be managed by CERN Configuration Management (<a href="https://puppet.com/">Puppet</a>), please refer to <a href="https://configdocs.web.cern.ch/configdocs/">documentation</a>. (paragraph below does not apply to puppet managed systems)
<p>
Virtual systems installed from images can be managed same way as standard hardware ones. Please refer to <a href="/linux/scientific6/">SLC6</a>, <a href="/centos7/">CC7</a>, <a href="/centos8/">C8</a> documentation for details.


<h3>Adding swap</h3>
If provided system memory is not enough for your application, you may add
swapfile this way:

<pre>
&#35; dd if=/dev/zero of=/swapfile.001 bs=1M count=1024
&#35; chmod 600 /swapfile.001
&#35; mkswap /swapfile.001
&#35; echo "/swapfile.001 swap swap defaults 0 0" >> /etc/fstab
&#35; swapon -a
</pre>

than verify that it is working correctly by running:
<pre>
&#35; free -m
              total        used        free      shared  buff/cache   available
Mem:           1829         320         104          27        1404        1276
Swap:          1023           0        1023
</pre>


<hr>
<h2>Images building information</h2>
Images are built using <a href="https://pagure.io/koji">Koji</a> CERN buildsystem. (Koji internally uses <a href="http://imgfac.org/">ImageFactory</a>, <a href="https://github.com/clalancette/oz">Oz</a> and <a href="https://libvirt.org/">libvirt</a> with <a href="http://wiki.qemu.org/">qemu-kvm</a> running <a href="http://pykickstart.readthedocs.io/en/latest/">kickstart</a> installations.)<p>
Kickstart files used to create images can be found on CERN <a href="https://gitlab.cern.ch/linuxsupport/koji-image-build/tree/master">GitLab LinuxSupport Koji-Image-Build pages</a>.

<h2>Support / Reporting problems</h2>
Please report problems to <a href="mailto:linux.support@cern.ch">linux support @ CERN</a>.

<hr>


