<!--#include virtual="/linux/layout/header" -->
# Advanced user account management using LDAP
<h2>How to configure LDAP to access user account information</h2>

There are different ways to configure user accounts at CERN.
If you only need to create a few local accounts, you should simply use
the <tt>useraddcern</tt> command with the <tt>--directory</tt> argument.
<p>
If you need to give access to larger groups of accounts, that are centrally
managed in FIM / Active Directory, read on...
<p>
This document describes how to configure a client machine to use
the central LDAP service <tt>xldap.cern.ch</tt> to give access to AFS user
groups. Examples:
<ul>
<li> How to give access to all users of some Unix groups?
<li> How to give access to all member of an E-group?
<li> How to deny access to users with disabled accounts?
<li> How to override certain attributes, like the login shell or the homedirectoy
</ul>

<h2>The <tt>xldap.cern.ch</tt> LDAP service</h2>

The <tt>xldap.cern.ch</tt> LDAP service allows anonymous read-access to the
user information in FIM / Active Directory. In particular, it contains the
account information for all AFS users, and the membership of the CERN E-groups.
<p>
The <tt>xldap.cern.ch</tt> LDAP service is described <a href="https://espace.cern.ch/identitymanagement/Wiki Pages/xldap.aspx">here</a>

<h2>What is sssd ?</h2>
SSSD provides a set of daemons to manage access to remote directories and authentication
mechanisms. It provides an NSS and PAM interface toward the system and a pluggable backend system
to connect to multiple different account sources as well as D-Bus interface.
<h2>Configuring sssd</h2>

The <tt>/etc/sssd/sssd.conf</tt> configuration describes the CERN configuration.
<p>
Note on CS8 and newer, it's possible that the package 'cern-krb5-conf' is installed.
This package provides a base minimum for kerberos configuration, but <a href="/docs/sssd.conf.example">this example</a> is more feature complete.

You can install it by following this recipe as root :
<pre>
&#35; curl -o /etc/sssd/conf.d/10_sssd.conf https://linux.web.cern.ch/docs/sssd.conf.example
&#35; chown root:root /etc/sssd/conf.d/10_sssd.conf
&#35; chmod 0600 /etc/sssd/conf.d/10_sssd.conf
&#35; restorecon /etc/sssd/conf.d/10_sssd.conf
</pre>
In the following section we will explain how it can be tweaked.

<h3>General options</h3>
This section describes the main options to be configured in <tt>/etc/sssd/conf.d/10_sssd.conf</tt>.
<ul>
    <li><tt>services</tt> is a comma separated list of services that are started when sssd itself starts. Supported services: nss, pam , sudo, autofs, ssh, pac, ifp</li>
    <li><tt>domain</tt> is a database containing user information.SSSD can use more domains at the same time.</li>
    <li><tt>filter_users, filter_groups </tt>exclude certain users from being fetched from the sss NSS database.This is particularly useful for system accounts. This option can also be set per-domain or include fully-qualified names to filter only users from the particular domain.</li>
    <li><tt>pam_id_timeout</tt> option controls how long we can cache the identity information to avoid excessive round-trips to the identity provider</li>
</ul>
<h3>Filtering results</h3>
With the general options above, your machine now has allows access from all accounts in the LDAP service. That is probable not what you want... This section gives some examples of filters that you can set to restrict the results to some useful sets of accounts.
<ul>
    <li>How to give access to all users of some Unix groups?</li>
    <ul>
        <li>Only allow the users of Unix group xx (gid 1160):</li>
        <pre>
        ldap_access_filter = (&(objectClass=user) (gidNumber=1160))
        </pre>
        <li>Allow users from Unix groups t3 and zp (gid's 1081 and 1307):</li>
        <pre>
        ldap_access_filter = (&(objectClass=user) (|(gidNumber=1160) (gidNumber=1307)))
    </ul>
    <li>How to give access to all members of an e-group?</li>
    <ul>
        <li>Allows members of lxsoft-admins (non-recursive) e-group:</li>
        <pre>
        ldap_access_filter = (&(objectClass=user) (memberOf=CN=lxsoft-admins,OU=e-groups,OU=Workgroups,DC=cern,DC=ch))
        </pre>
        <li> The above does not work for "recursive e-groups", where some members are e-groups themselves. To support such e-groups, you can modify the filter:</li>
        <pre>
        ldap_access_filter = (&(objectClass=user) (memberOf:1.2.840.113556.1.4.1941:=CN=info-experiments,OU=e-groups,OU=Workgroups,DC=cern,DC=ch))
        </pre>
    </ul>
    <li>How to deny access to users with disabled accounts?</li>
    <ul>
        <li>
        Accounts that are disabled in Active Directory can be filtered out like this:
        <pre>
        ldap_access_filter = (&;(|(|(&(objectClass=user) (cn=*))))(!(userAccountControl:1.2.840.113556.1.4.803:=2)))
        </pre>
        For an explanation, please go the <a href="http://support.microsoft.com/kb/305144">Microsoft Knowledge Base</a>
        </li>
    </ul>
</ul>
<tt>Note: ldap_access_filter</tt> accepts standard LDAP filter syntax so get as creative as you want.
<h3>Overriding attribute values</h3>
<tt>sssd-ldap</tt> allows to override certain attribute values, like the login shell or the use home directory. This may be useful on certain server machines. Example:
<pre>
ldap_user_shell = /dev/null
ldap_user_home_directory = /nfs/home/%u (man sssd.conf for allowed sequences)
</pre>
All available settings can be found in the manual
<pre>
&#35; man sssd-ldap
</pre>
<h3>Notes from linux support</h3>
<ul>
    <li>To enable verbose debug messages you can append "debug_level = 0x1310" to each section.</li>
    <li>If you have to deal with old user ID < 1000 you can use "min_id/max_id" in the [domain/CERN] section</li>
</ul>
<h2>Enable sssd</h2>
<pre>
&#35; ## FOR CC7 ONLY ##
&#35; authconfig --enablesssd --enablesssdauth --update
&#35; ## FOR CS8 AND NEWER ##
&#35; authselect select sssd with-silent-lastlog --force
</pre>
The <tt>/etc/nsswitch.conf</tt> configuration file describes the order in
which password-file lookups are performed. This file should not need to be ever changed, and should always contain content such as:
<pre>
passwd:     files sss
group:      files sss
</pre>

<h2>Run sssd</h2>
Now you need to make sure sssd runs and is enabled by default :
<pre>
&#35; systemctl enable sssd
&#35; systemctl stop sssd
&#35; systemctl start sssd
</pre>

NOTE: If you experiment with sssd and you want to be sure to clean all the
caches please run the following command :
<pre>
&#35; systemctl stop sssd
&#35; rm -f /var/lib/sss/mc/*
&#35; rm -f /var/lib/sss/db/*
&#35; systemctl start sssd
&#35; sss_cache -E
</pre>
<h2>Run a simple test</h2>
You can run the following command where "login" correspond to an
authorized CERN user :
<pre>
&#35; getent passwd login
</pre>

<h2>References</h2>

<ul>
<li> Documentation on <a href="https://espace.cern.ch/identitymanagement/Wiki Pages/xldap.aspx">xldap structure and tips</a>
<li> More generally on the <a href="http://www.cern.ch/identitymanagement">Identity Management project</a>
</ul>

<h2>Feedback</h2>

Please send feedback and comments on this document to
<a href="mailto:linux.support@cern.ch">Linux support</a>.
