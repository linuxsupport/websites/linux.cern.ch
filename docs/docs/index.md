<!--#include virtual="/linux/layout/header" -->
# Documentation

<h2>General Linux support</h2>
<ul>
<li><a href="/support/support-policy">Support policy and service mandate</a>
<li><a href="/support/#how-to-write-good-support-requests-to-linuxsupport">How to write a good support request</a>
</ul>

<h2>Documentation for CERN supported releases</h2>
<ul>
  <li><a href="/almalinux/alma9/">AlmaLinux 9 documentation</a>
  <li><a href="/almalinux/alma8/">AlmaLinux 8 documentation</a>
  <li><strike><a href="/centos7/docs/">CERN CentOS 7 documentation</a></strike> CC7 supported ended July 2024.
  <li><strike><a href="/scientific6/docs/">Scientific Linux CERN 6 documentation</a></strike> SLC6 supported ended November 2020.
  <li><strike><a href="http://linux-old.web.cern.ch/linux/scientific5/docs/">Scientific Linux CERN 5 documentation</a></strike> SLC 5 support ended March 2019.
</ul>

<h2>Other (potentially release independent)</h2>

<ul>
<li>
<a href="singleuser">Booting into Single user mode (8+)</a>.
</li>
<li>
<a href="cloudimages"> CC7 and C8 cloud images for CERN OpenStack</a>
<li>
<a href="dockerimages"> CC7 and C8 docker images for CERN OpenStack container service</a>
</li>
<li>
<a href="kerberos-access">Accessing CERN Linux machines via
	Kerberos</a>
</li>
<li>
<a href="certificate-autoenroll">CERN Host Certificate AutoEnrollement and AutoRenewal</a>
<li>
<a href="printing">Printing from Linux clients at CERN</a>
</li>
<li>
<a href="account-mgmt">Advanced user account management using LDAP</a>
<li>
<a href="http://redhat.com/support" target="_new">Red Hat support site</a>
</li>

<li>
<a href="http://kb.redhat.com/" target="_new">Red Hat Linux Support Knowledgebase</a>
</li>
