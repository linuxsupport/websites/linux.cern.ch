## 2023-08-11

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 16.20.1-2.el9_2 | |
nodejs-debuginfo | 16.20.1-2.el9_2 | |
nodejs-debugsource | 16.20.1-2.el9_2 | |
nodejs-docs | 16.20.1-2.el9_2 | |
nodejs-full-i18n | 16.20.1-2.el9_2 | |
nodejs-libs | 16.20.1-2.el9_2 | |
nodejs-libs-debuginfo | 16.20.1-2.el9_2 | |
npm | 8.19.4-1.16.20.1.2.el9_2 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs-devel | 16.20.1-2.el9_2 | |
v8-devel | 9.4.146.26-1.16.20.1.2.el9_2 | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
almalinux-release-testing | 9-1.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 16.20.1-2.el9_2 | |
nodejs-debuginfo | 16.20.1-2.el9_2 | |
nodejs-debugsource | 16.20.1-2.el9_2 | |
nodejs-docs | 16.20.1-2.el9_2 | |
nodejs-full-i18n | 16.20.1-2.el9_2 | |
nodejs-libs | 16.20.1-2.el9_2 | |
nodejs-libs-debuginfo | 16.20.1-2.el9_2 | |
npm | 8.19.4-1.16.20.1.2.el9_2 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs-devel | 16.20.1-2.el9_2 | |
v8-devel | 9.4.146.26-1.16.20.1.2.el9_2 | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
almalinux-release-testing | 9-1.el9 | |

