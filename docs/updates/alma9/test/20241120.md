## 2024-11-20

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
aspnetcore-runtime-dbg-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
aspnetcore-targeting-pack-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-apphost-pack-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-apphost-pack-9.0-debuginfo | 9.0.0-1.el9_5 | |
dotnet-host | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-host-debuginfo | 9.0.0-1.el9_5 | |
dotnet-hostfxr-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-hostfxr-9.0-debuginfo | 9.0.0-1.el9_5 | |
dotnet-runtime-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-runtime-9.0-debuginfo | 9.0.0-1.el9_5 | |
dotnet-runtime-dbg-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-sdk-9.0 | 9.0.100-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-sdk-9.0-debuginfo | 9.0.100-1.el9_5 | |
dotnet-sdk-aot-9.0 | 9.0.100-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-sdk-aot-9.0-debuginfo | 9.0.100-1.el9_5 | |
dotnet-sdk-dbg-9.0 | 9.0.100-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-targeting-pack-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-templates-9.0 | 9.0.100-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet9.0-debuginfo | 9.0.100-1.el9_5 | |
dotnet9.0-debugsource | 9.0.100-1.el9_5 | |
firefox | 128.4.0-1.el9_5 | [ALSA-2024:9554](https://errata.almalinux.org/9/ALSA-2024-9554.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10458](https://access.redhat.com/security/cve/CVE-2024-10458), [CVE-2024-10459](https://access.redhat.com/security/cve/CVE-2024-10459), [CVE-2024-10460](https://access.redhat.com/security/cve/CVE-2024-10460), [CVE-2024-10461](https://access.redhat.com/security/cve/CVE-2024-10461), [CVE-2024-10462](https://access.redhat.com/security/cve/CVE-2024-10462), [CVE-2024-10463](https://access.redhat.com/security/cve/CVE-2024-10463), [CVE-2024-10464](https://access.redhat.com/security/cve/CVE-2024-10464), [CVE-2024-10465](https://access.redhat.com/security/cve/CVE-2024-10465), [CVE-2024-10466](https://access.redhat.com/security/cve/CVE-2024-10466), [CVE-2024-10467](https://access.redhat.com/security/cve/CVE-2024-10467), [CVE-2024-9680](https://access.redhat.com/security/cve/CVE-2024-9680))
firefox-debuginfo | 128.4.0-1.el9_5 | |
firefox-debugsource | 128.4.0-1.el9_5 | |
firefox-x11 | 128.4.0-1.el9_5 | [ALSA-2024:9554](https://errata.almalinux.org/9/ALSA-2024-9554.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10458](https://access.redhat.com/security/cve/CVE-2024-10458), [CVE-2024-10459](https://access.redhat.com/security/cve/CVE-2024-10459), [CVE-2024-10460](https://access.redhat.com/security/cve/CVE-2024-10460), [CVE-2024-10461](https://access.redhat.com/security/cve/CVE-2024-10461), [CVE-2024-10462](https://access.redhat.com/security/cve/CVE-2024-10462), [CVE-2024-10463](https://access.redhat.com/security/cve/CVE-2024-10463), [CVE-2024-10464](https://access.redhat.com/security/cve/CVE-2024-10464), [CVE-2024-10465](https://access.redhat.com/security/cve/CVE-2024-10465), [CVE-2024-10466](https://access.redhat.com/security/cve/CVE-2024-10466), [CVE-2024-10467](https://access.redhat.com/security/cve/CVE-2024-10467), [CVE-2024-9680](https://access.redhat.com/security/cve/CVE-2024-9680))
netstandard-targeting-pack-2.1 | 9.0.100-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
thunderbird | 128.4.0-1.el9_5.alma.1 | [ALSA-2024:9552](https://errata.almalinux.org/9/ALSA-2024-9552.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10458](https://access.redhat.com/security/cve/CVE-2024-10458), [CVE-2024-10459](https://access.redhat.com/security/cve/CVE-2024-10459), [CVE-2024-10460](https://access.redhat.com/security/cve/CVE-2024-10460), [CVE-2024-10461](https://access.redhat.com/security/cve/CVE-2024-10461), [CVE-2024-10462](https://access.redhat.com/security/cve/CVE-2024-10462), [CVE-2024-10463](https://access.redhat.com/security/cve/CVE-2024-10463), [CVE-2024-10464](https://access.redhat.com/security/cve/CVE-2024-10464), [CVE-2024-10465](https://access.redhat.com/security/cve/CVE-2024-10465), [CVE-2024-10466](https://access.redhat.com/security/cve/CVE-2024-10466), [CVE-2024-10467](https://access.redhat.com/security/cve/CVE-2024-10467), [CVE-2024-9680](https://access.redhat.com/security/cve/CVE-2024-9680))
thunderbird-debuginfo | 128.4.0-1.el9_5.alma.1 | |
thunderbird-debugsource | 128.4.0-1.el9_5.alma.1 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-9.0-source-built-artifacts | 9.0.100-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
aspnetcore-runtime-dbg-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
aspnetcore-targeting-pack-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-apphost-pack-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-apphost-pack-9.0-debuginfo | 9.0.0-1.el9_5 | |
dotnet-host | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-host-debuginfo | 9.0.0-1.el9_5 | |
dotnet-hostfxr-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-hostfxr-9.0-debuginfo | 9.0.0-1.el9_5 | |
dotnet-runtime-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-runtime-9.0-debuginfo | 9.0.0-1.el9_5 | |
dotnet-runtime-dbg-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-sdk-9.0 | 9.0.100-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-sdk-9.0-debuginfo | 9.0.100-1.el9_5 | |
dotnet-sdk-aot-9.0 | 9.0.100-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-sdk-aot-9.0-debuginfo | 9.0.100-1.el9_5 | |
dotnet-sdk-dbg-9.0 | 9.0.100-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-targeting-pack-9.0 | 9.0.0-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet-templates-9.0 | 9.0.100-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
dotnet9.0-debuginfo | 9.0.100-1.el9_5 | |
dotnet9.0-debugsource | 9.0.100-1.el9_5 | |
firefox | 128.4.0-1.el9_5 | [ALSA-2024:9554](https://errata.almalinux.org/9/ALSA-2024-9554.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10458](https://access.redhat.com/security/cve/CVE-2024-10458), [CVE-2024-10459](https://access.redhat.com/security/cve/CVE-2024-10459), [CVE-2024-10460](https://access.redhat.com/security/cve/CVE-2024-10460), [CVE-2024-10461](https://access.redhat.com/security/cve/CVE-2024-10461), [CVE-2024-10462](https://access.redhat.com/security/cve/CVE-2024-10462), [CVE-2024-10463](https://access.redhat.com/security/cve/CVE-2024-10463), [CVE-2024-10464](https://access.redhat.com/security/cve/CVE-2024-10464), [CVE-2024-10465](https://access.redhat.com/security/cve/CVE-2024-10465), [CVE-2024-10466](https://access.redhat.com/security/cve/CVE-2024-10466), [CVE-2024-10467](https://access.redhat.com/security/cve/CVE-2024-10467), [CVE-2024-9680](https://access.redhat.com/security/cve/CVE-2024-9680))
firefox-debuginfo | 128.4.0-1.el9_5 | |
firefox-debugsource | 128.4.0-1.el9_5 | |
firefox-x11 | 128.4.0-1.el9_5 | [ALSA-2024:9554](https://errata.almalinux.org/9/ALSA-2024-9554.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10458](https://access.redhat.com/security/cve/CVE-2024-10458), [CVE-2024-10459](https://access.redhat.com/security/cve/CVE-2024-10459), [CVE-2024-10460](https://access.redhat.com/security/cve/CVE-2024-10460), [CVE-2024-10461](https://access.redhat.com/security/cve/CVE-2024-10461), [CVE-2024-10462](https://access.redhat.com/security/cve/CVE-2024-10462), [CVE-2024-10463](https://access.redhat.com/security/cve/CVE-2024-10463), [CVE-2024-10464](https://access.redhat.com/security/cve/CVE-2024-10464), [CVE-2024-10465](https://access.redhat.com/security/cve/CVE-2024-10465), [CVE-2024-10466](https://access.redhat.com/security/cve/CVE-2024-10466), [CVE-2024-10467](https://access.redhat.com/security/cve/CVE-2024-10467), [CVE-2024-9680](https://access.redhat.com/security/cve/CVE-2024-9680))
netstandard-targeting-pack-2.1 | 9.0.100-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))
thunderbird | 128.4.0-1.el9_5.alma.1 | [ALSA-2024:9552](https://errata.almalinux.org/9/ALSA-2024-9552.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10458](https://access.redhat.com/security/cve/CVE-2024-10458), [CVE-2024-10459](https://access.redhat.com/security/cve/CVE-2024-10459), [CVE-2024-10460](https://access.redhat.com/security/cve/CVE-2024-10460), [CVE-2024-10461](https://access.redhat.com/security/cve/CVE-2024-10461), [CVE-2024-10462](https://access.redhat.com/security/cve/CVE-2024-10462), [CVE-2024-10463](https://access.redhat.com/security/cve/CVE-2024-10463), [CVE-2024-10464](https://access.redhat.com/security/cve/CVE-2024-10464), [CVE-2024-10465](https://access.redhat.com/security/cve/CVE-2024-10465), [CVE-2024-10466](https://access.redhat.com/security/cve/CVE-2024-10466), [CVE-2024-10467](https://access.redhat.com/security/cve/CVE-2024-10467), [CVE-2024-9680](https://access.redhat.com/security/cve/CVE-2024-9680))
thunderbird-debuginfo | 128.4.0-1.el9_5.alma.1 | |
thunderbird-debugsource | 128.4.0-1.el9_5.alma.1 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-9.0-source-built-artifacts | 9.0.100-1.el9_5 | [ALSA-2024:9543](https://errata.almalinux.org/9/ALSA-2024-9543.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43498](https://access.redhat.com/security/cve/CVE-2024-43498), [CVE-2024-43499](https://access.redhat.com/security/cve/CVE-2024-43499))

