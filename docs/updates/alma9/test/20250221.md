## 2025-02-21

### testing x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh | 8.7p1-43.el9.alma.1 | |
openssh-askpass | 8.7p1-43.el9.alma.1 | |
openssh-clients | 8.7p1-43.el9.alma.1 | |
openssh-keycat | 8.7p1-43.el9.alma.1 | |
openssh-server | 8.7p1-43.el9.alma.1 | |
openssh-sk-dummy | 8.7p1-43.el9.alma.1 | |
pam_ssh_agent_auth | 0.10.4-5.43.el9.alma.1 | |

### testing aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh | 8.7p1-43.el9.alma.1 | |
openssh-askpass | 8.7p1-43.el9.alma.1 | |
openssh-clients | 8.7p1-43.el9.alma.1 | |
openssh-keycat | 8.7p1-43.el9.alma.1 | |
openssh-server | 8.7p1-43.el9.alma.1 | |
openssh-sk-dummy | 8.7p1-43.el9.alma.1 | |
pam_ssh_agent_auth | 0.10.4-5.43.el9.alma.1 | |

