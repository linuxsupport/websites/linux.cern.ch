## 2025-01-09

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.5.14.0_503.19.1.el9_5.al9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.4.0-503.19.1.el9_5 | |
bpftool-debuginfo | 7.4.0-503.19.1.el9_5 | |
kernel | 5.14.0-503.19.1.el9_5 | |
kernel-abi-stablelists | 5.14.0-503.19.1.el9_5 | |
kernel-core | 5.14.0-503.19.1.el9_5 | |
kernel-debug | 5.14.0-503.19.1.el9_5 | |
kernel-debug-core | 5.14.0-503.19.1.el9_5 | |
kernel-debug-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-debug-modules | 5.14.0-503.19.1.el9_5 | |
kernel-debug-modules-core | 5.14.0-503.19.1.el9_5 | |
kernel-debug-modules-extra | 5.14.0-503.19.1.el9_5 | |
kernel-debug-uki-virt | 5.14.0-503.19.1.el9_5 | |
kernel-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-debuginfo-common-x86_64 | 5.14.0-503.19.1.el9_5 | |
kernel-modules | 5.14.0-503.19.1.el9_5 | |
kernel-modules-core | 5.14.0-503.19.1.el9_5 | |
kernel-modules-extra | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-tools | 5.14.0-503.19.1.el9_5 | |
kernel-tools-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-tools-libs | 5.14.0-503.19.1.el9_5 | |
kernel-uki-virt | 5.14.0-503.19.1.el9_5 | |
kernel-uki-virt-addons | 5.14.0-503.19.1.el9_5 | |
libipa_hbac | 2.9.5-4.el9_5.4 | |
libipa_hbac-debuginfo | 2.9.5-4.el9_5.4 | |
libperf-debuginfo | 5.14.0-503.19.1.el9_5 | |
libsss_autofs | 2.9.5-4.el9_5.4 | |
libsss_autofs-debuginfo | 2.9.5-4.el9_5.4 | |
libsss_certmap | 2.9.5-4.el9_5.4 | |
libsss_certmap-debuginfo | 2.9.5-4.el9_5.4 | |
libsss_idmap | 2.9.5-4.el9_5.4 | |
libsss_idmap-debuginfo | 2.9.5-4.el9_5.4 | |
libsss_nss_idmap | 2.9.5-4.el9_5.4 | |
libsss_nss_idmap-debuginfo | 2.9.5-4.el9_5.4 | |
libsss_simpleifp | 2.9.5-4.el9_5.4 | |
libsss_simpleifp-debuginfo | 2.9.5-4.el9_5.4 | |
libsss_sudo | 2.9.5-4.el9_5.4 | |
libsss_sudo-debuginfo | 2.9.5-4.el9_5.4 | |
perf-debuginfo | 5.14.0-503.19.1.el9_5 | |
python3-libipa_hbac | 2.9.5-4.el9_5.4 | |
python3-libipa_hbac-debuginfo | 2.9.5-4.el9_5.4 | |
python3-libsss_nss_idmap | 2.9.5-4.el9_5.4 | |
python3-libsss_nss_idmap-debuginfo | 2.9.5-4.el9_5.4 | |
python3-perf | 5.14.0-503.19.1.el9_5 | |
python3-perf-debuginfo | 5.14.0-503.19.1.el9_5 | |
python3-sss | 2.9.5-4.el9_5.4 | |
python3-sss-debuginfo | 2.9.5-4.el9_5.4 | |
python3-sss-murmur | 2.9.5-4.el9_5.4 | |
python3-sss-murmur-debuginfo | 2.9.5-4.el9_5.4 | |
python3-sssdconfig | 2.9.5-4.el9_5.4 | |
sssd | 2.9.5-4.el9_5.4 | |
sssd-ad | 2.9.5-4.el9_5.4 | |
sssd-ad-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-client | 2.9.5-4.el9_5.4 | |
sssd-client-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-common | 2.9.5-4.el9_5.4 | |
sssd-common-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-common-pac | 2.9.5-4.el9_5.4 | |
sssd-common-pac-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-dbus | 2.9.5-4.el9_5.4 | |
sssd-dbus-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-debugsource | 2.9.5-4.el9_5.4 | |
sssd-ipa | 2.9.5-4.el9_5.4 | |
sssd-ipa-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-kcm | 2.9.5-4.el9_5.4 | |
sssd-kcm-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-krb5 | 2.9.5-4.el9_5.4 | |
sssd-krb5-common | 2.9.5-4.el9_5.4 | |
sssd-krb5-common-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-krb5-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-ldap | 2.9.5-4.el9_5.4 | |
sssd-ldap-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-nfs-idmap | 2.9.5-4.el9_5.4 | |
sssd-nfs-idmap-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-passkey | 2.9.5-4.el9_5.4 | |
sssd-passkey-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-polkit-rules | 2.9.5-4.el9_5.4 | |
sssd-proxy | 2.9.5-4.el9_5.4 | |
sssd-proxy-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-tools | 2.9.5-4.el9_5.4 | |
sssd-tools-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-winbind-idmap | 2.9.5-4.el9_5.4 | |
sssd-winbind-idmap-debuginfo | 2.9.5-4.el9_5.4 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-debug-devel | 5.14.0-503.19.1.el9_5 | |
kernel-debug-devel-matched | 5.14.0-503.19.1.el9_5 | |
kernel-devel | 5.14.0-503.19.1.el9_5 | |
kernel-devel-matched | 5.14.0-503.19.1.el9_5 | |
kernel-doc | 5.14.0-503.19.1.el9_5 | |
kernel-headers | 5.14.0-503.19.1.el9_5 | |
libvirt | 10.5.0-7.2.el9_5 | |
libvirt-client | 10.5.0-7.2.el9_5 | |
libvirt-client-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-client-qemu | 10.5.0-7.2.el9_5 | |
libvirt-daemon | 10.5.0-7.2.el9_5 | |
libvirt-daemon-common | 10.5.0-7.2.el9_5 | |
libvirt-daemon-common-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-config-network | 10.5.0-7.2.el9_5 | |
libvirt-daemon-config-nwfilter | 10.5.0-7.2.el9_5 | |
libvirt-daemon-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-interface | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-interface-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-network | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-network-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-nodedev | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-nodedev-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-nwfilter | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-nwfilter-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-qemu | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-qemu-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-secret | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-secret-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-core | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-core-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-disk | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-disk-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-iscsi | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-iscsi-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-logical | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-logical-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-mpath | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-mpath-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-rbd | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-rbd-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-scsi | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-scsi-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-kvm | 10.5.0-7.2.el9_5 | |
libvirt-daemon-lock | 10.5.0-7.2.el9_5 | |
libvirt-daemon-lock-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-log | 10.5.0-7.2.el9_5 | |
libvirt-daemon-log-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-plugin-lockd | 10.5.0-7.2.el9_5 | |
libvirt-daemon-plugin-lockd-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-plugin-sanlock-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-proxy | 10.5.0-7.2.el9_5 | |
libvirt-daemon-proxy-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-debugsource | 10.5.0-7.2.el9_5 | |
libvirt-libs | 10.5.0-7.2.el9_5 | |
libvirt-libs-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-nss | 10.5.0-7.2.el9_5 | |
libvirt-nss-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-ssh-proxy | 10.5.0-7.2.el9_5 | |
libvirt-ssh-proxy-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-wireshark-debuginfo | 10.5.0-7.2.el9_5 | |
perf | 5.14.0-503.19.1.el9_5 | |
rtla | 5.14.0-503.19.1.el9_5 | |
rv | 5.14.0-503.19.1.el9_5 | |
sssd-idp | 2.9.5-4.el9_5.4 | |
sssd-idp-debuginfo | 2.9.5-4.el9_5.4 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-503.19.1.el9_5 | |
kernel-rt-core | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-core | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-devel | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-modules | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-modules-core | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-modules-extra | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-rt-devel | 5.14.0-503.19.1.el9_5 | |
kernel-rt-modules | 5.14.0-503.19.1.el9_5 | |
kernel-rt-modules-core | 5.14.0-503.19.1.el9_5 | |
kernel-rt-modules-extra | 5.14.0-503.19.1.el9_5 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-cross-headers | 5.14.0-503.19.1.el9_5 | |
kernel-tools-libs-devel | 5.14.0-503.19.1.el9_5 | |
libperf | 5.14.0-503.19.1.el9_5 | |
libperf-debuginfo | 5.14.0-503.19.1.el9_5 | |
libsss_nss_idmap-devel | 2.9.5-4.el9_5.4 | |
libvirt-daemon-plugin-sanlock | 10.5.0-7.2.el9_5 | |
libvirt-devel | 10.5.0-7.2.el9_5 | |
libvirt-docs | 10.5.0-7.2.el9_5 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-503.19.1.el9_5 | |
kernel-rt-core | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-core | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-devel | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-kvm | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-modules | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-modules-core | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-modules-extra | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-rt-devel | 5.14.0-503.19.1.el9_5 | |
kernel-rt-kvm | 5.14.0-503.19.1.el9_5 | |
kernel-rt-modules | 5.14.0-503.19.1.el9_5 | |
kernel-rt-modules-core | 5.14.0-503.19.1.el9_5 | |
kernel-rt-modules-extra | 5.14.0-503.19.1.el9_5 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-debug-modules-internal | 5.14.0-503.19.1.el9_5 | |
kernel-debug-modules-partner | 5.14.0-503.19.1.el9_5 | |
kernel-debug-uki-virt-addons | 5.14.0-503.19.1.el9_5 | |
kernel-ipaclones-internal | 5.14.0-503.19.1.el9_5 | |
kernel-modules-internal | 5.14.0-503.19.1.el9_5 | |
kernel-modules-partner | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-devel-matched | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-modules-internal | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-modules-partner | 5.14.0-503.19.1.el9_5 | |
kernel-rt-devel-matched | 5.14.0-503.19.1.el9_5 | |
kernel-rt-modules-internal | 5.14.0-503.19.1.el9_5 | |
kernel-rt-modules-partner | 5.14.0-503.19.1.el9_5 | |
kernel-selftests-internal | 5.14.0-503.19.1.el9_5 | |
libipa_hbac-devel | 2.9.5-4.el9_5.4 | |
libperf-devel | 5.14.0-503.19.1.el9_5 | |
libsss_certmap-devel | 2.9.5-4.el9_5.4 | |
libsss_idmap-devel | 2.9.5-4.el9_5.4 | |
libsss_simpleifp-devel | 2.9.5-4.el9_5.4 | |
libvirt-wireshark | 10.5.0-7.2.el9_5 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.5.14.0_503.19.1.el9_5.al9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.4.0-503.19.1.el9_5 | |
bpftool-debuginfo | 7.4.0-503.19.1.el9_5 | |
kernel | 5.14.0-503.19.1.el9_5 | |
kernel-64k | 5.14.0-503.19.1.el9_5 | |
kernel-64k-core | 5.14.0-503.19.1.el9_5 | |
kernel-64k-debug | 5.14.0-503.19.1.el9_5 | |
kernel-64k-debug-core | 5.14.0-503.19.1.el9_5 | |
kernel-64k-debug-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-64k-debug-modules | 5.14.0-503.19.1.el9_5 | |
kernel-64k-debug-modules-core | 5.14.0-503.19.1.el9_5 | |
kernel-64k-debug-modules-extra | 5.14.0-503.19.1.el9_5 | |
kernel-64k-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-64k-modules | 5.14.0-503.19.1.el9_5 | |
kernel-64k-modules-core | 5.14.0-503.19.1.el9_5 | |
kernel-64k-modules-extra | 5.14.0-503.19.1.el9_5 | |
kernel-abi-stablelists | 5.14.0-503.19.1.el9_5 | |
kernel-core | 5.14.0-503.19.1.el9_5 | |
kernel-debug | 5.14.0-503.19.1.el9_5 | |
kernel-debug-core | 5.14.0-503.19.1.el9_5 | |
kernel-debug-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-debug-modules | 5.14.0-503.19.1.el9_5 | |
kernel-debug-modules-core | 5.14.0-503.19.1.el9_5 | |
kernel-debug-modules-extra | 5.14.0-503.19.1.el9_5 | |
kernel-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-debuginfo-common-aarch64 | 5.14.0-503.19.1.el9_5 | |
kernel-modules | 5.14.0-503.19.1.el9_5 | |
kernel-modules-core | 5.14.0-503.19.1.el9_5 | |
kernel-modules-extra | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-tools | 5.14.0-503.19.1.el9_5 | |
kernel-tools-debuginfo | 5.14.0-503.19.1.el9_5 | |
kernel-tools-libs | 5.14.0-503.19.1.el9_5 | |
libipa_hbac | 2.9.5-4.el9_5.4 | |
libipa_hbac-debuginfo | 2.9.5-4.el9_5.4 | |
libperf-debuginfo | 5.14.0-503.19.1.el9_5 | |
libsss_autofs | 2.9.5-4.el9_5.4 | |
libsss_autofs-debuginfo | 2.9.5-4.el9_5.4 | |
libsss_certmap | 2.9.5-4.el9_5.4 | |
libsss_certmap-debuginfo | 2.9.5-4.el9_5.4 | |
libsss_idmap | 2.9.5-4.el9_5.4 | |
libsss_idmap-debuginfo | 2.9.5-4.el9_5.4 | |
libsss_nss_idmap | 2.9.5-4.el9_5.4 | |
libsss_nss_idmap-debuginfo | 2.9.5-4.el9_5.4 | |
libsss_simpleifp | 2.9.5-4.el9_5.4 | |
libsss_simpleifp-debuginfo | 2.9.5-4.el9_5.4 | |
libsss_sudo | 2.9.5-4.el9_5.4 | |
libsss_sudo-debuginfo | 2.9.5-4.el9_5.4 | |
perf-debuginfo | 5.14.0-503.19.1.el9_5 | |
python3-libipa_hbac | 2.9.5-4.el9_5.4 | |
python3-libipa_hbac-debuginfo | 2.9.5-4.el9_5.4 | |
python3-libsss_nss_idmap | 2.9.5-4.el9_5.4 | |
python3-libsss_nss_idmap-debuginfo | 2.9.5-4.el9_5.4 | |
python3-perf | 5.14.0-503.19.1.el9_5 | |
python3-perf-debuginfo | 5.14.0-503.19.1.el9_5 | |
python3-sss | 2.9.5-4.el9_5.4 | |
python3-sss-debuginfo | 2.9.5-4.el9_5.4 | |
python3-sss-murmur | 2.9.5-4.el9_5.4 | |
python3-sss-murmur-debuginfo | 2.9.5-4.el9_5.4 | |
python3-sssdconfig | 2.9.5-4.el9_5.4 | |
sssd | 2.9.5-4.el9_5.4 | |
sssd-ad | 2.9.5-4.el9_5.4 | |
sssd-ad-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-client | 2.9.5-4.el9_5.4 | |
sssd-client-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-common | 2.9.5-4.el9_5.4 | |
sssd-common-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-common-pac | 2.9.5-4.el9_5.4 | |
sssd-common-pac-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-dbus | 2.9.5-4.el9_5.4 | |
sssd-dbus-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-debugsource | 2.9.5-4.el9_5.4 | |
sssd-ipa | 2.9.5-4.el9_5.4 | |
sssd-ipa-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-kcm | 2.9.5-4.el9_5.4 | |
sssd-kcm-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-krb5 | 2.9.5-4.el9_5.4 | |
sssd-krb5-common | 2.9.5-4.el9_5.4 | |
sssd-krb5-common-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-krb5-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-ldap | 2.9.5-4.el9_5.4 | |
sssd-ldap-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-nfs-idmap | 2.9.5-4.el9_5.4 | |
sssd-nfs-idmap-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-passkey | 2.9.5-4.el9_5.4 | |
sssd-passkey-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-polkit-rules | 2.9.5-4.el9_5.4 | |
sssd-proxy | 2.9.5-4.el9_5.4 | |
sssd-proxy-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-tools | 2.9.5-4.el9_5.4 | |
sssd-tools-debuginfo | 2.9.5-4.el9_5.4 | |
sssd-winbind-idmap | 2.9.5-4.el9_5.4 | |
sssd-winbind-idmap-debuginfo | 2.9.5-4.el9_5.4 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-64k-debug-devel | 5.14.0-503.19.1.el9_5 | |
kernel-64k-debug-devel-matched | 5.14.0-503.19.1.el9_5 | |
kernel-64k-devel | 5.14.0-503.19.1.el9_5 | |
kernel-64k-devel-matched | 5.14.0-503.19.1.el9_5 | |
kernel-debug-devel | 5.14.0-503.19.1.el9_5 | |
kernel-debug-devel-matched | 5.14.0-503.19.1.el9_5 | |
kernel-devel | 5.14.0-503.19.1.el9_5 | |
kernel-devel-matched | 5.14.0-503.19.1.el9_5 | |
kernel-doc | 5.14.0-503.19.1.el9_5 | |
kernel-headers | 5.14.0-503.19.1.el9_5 | |
libvirt | 10.5.0-7.2.el9_5 | |
libvirt-client | 10.5.0-7.2.el9_5 | |
libvirt-client-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-client-qemu | 10.5.0-7.2.el9_5 | |
libvirt-daemon | 10.5.0-7.2.el9_5 | |
libvirt-daemon-common | 10.5.0-7.2.el9_5 | |
libvirt-daemon-common-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-config-network | 10.5.0-7.2.el9_5 | |
libvirt-daemon-config-nwfilter | 10.5.0-7.2.el9_5 | |
libvirt-daemon-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-interface | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-interface-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-network | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-network-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-nodedev | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-nodedev-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-nwfilter | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-nwfilter-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-qemu | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-qemu-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-secret | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-secret-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-core | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-core-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-disk | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-disk-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-iscsi | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-iscsi-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-logical | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-logical-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-mpath | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-mpath-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-rbd | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-rbd-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-scsi | 10.5.0-7.2.el9_5 | |
libvirt-daemon-driver-storage-scsi-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-kvm | 10.5.0-7.2.el9_5 | |
libvirt-daemon-lock | 10.5.0-7.2.el9_5 | |
libvirt-daemon-lock-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-log | 10.5.0-7.2.el9_5 | |
libvirt-daemon-log-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-plugin-lockd | 10.5.0-7.2.el9_5 | |
libvirt-daemon-plugin-lockd-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-plugin-sanlock-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-daemon-proxy | 10.5.0-7.2.el9_5 | |
libvirt-daemon-proxy-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-debugsource | 10.5.0-7.2.el9_5 | |
libvirt-libs | 10.5.0-7.2.el9_5 | |
libvirt-libs-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-nss | 10.5.0-7.2.el9_5 | |
libvirt-nss-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-ssh-proxy | 10.5.0-7.2.el9_5 | |
libvirt-ssh-proxy-debuginfo | 10.5.0-7.2.el9_5 | |
libvirt-wireshark-debuginfo | 10.5.0-7.2.el9_5 | |
perf | 5.14.0-503.19.1.el9_5 | |
rtla | 5.14.0-503.19.1.el9_5 | |
rv | 5.14.0-503.19.1.el9_5 | |
sssd-idp | 2.9.5-4.el9_5.4 | |
sssd-idp-debuginfo | 2.9.5-4.el9_5.4 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-cross-headers | 5.14.0-503.19.1.el9_5 | |
kernel-tools-libs-devel | 5.14.0-503.19.1.el9_5 | |
libperf | 5.14.0-503.19.1.el9_5 | |
libperf-debuginfo | 5.14.0-503.19.1.el9_5 | |
libsss_nss_idmap-devel | 2.9.5-4.el9_5.4 | |
libvirt-daemon-plugin-sanlock | 10.5.0-7.2.el9_5 | |
libvirt-devel | 10.5.0-7.2.el9_5 | |
libvirt-docs | 10.5.0-7.2.el9_5 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-64k-debug-modules-internal | 5.14.0-503.19.1.el9_5 | |
kernel-64k-debug-modules-partner | 5.14.0-503.19.1.el9_5 | |
kernel-64k-modules-internal | 5.14.0-503.19.1.el9_5 | |
kernel-64k-modules-partner | 5.14.0-503.19.1.el9_5 | |
kernel-debug-modules-internal | 5.14.0-503.19.1.el9_5 | |
kernel-debug-modules-partner | 5.14.0-503.19.1.el9_5 | |
kernel-modules-internal | 5.14.0-503.19.1.el9_5 | |
kernel-modules-partner | 5.14.0-503.19.1.el9_5 | |
kernel-rt | 5.14.0-503.19.1.el9_5 | |
kernel-rt-core | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-core | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-devel | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-devel-matched | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-kvm | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-modules | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-modules-core | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-modules-extra | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-modules-internal | 5.14.0-503.19.1.el9_5 | |
kernel-rt-debug-modules-partner | 5.14.0-503.19.1.el9_5 | |
kernel-rt-devel | 5.14.0-503.19.1.el9_5 | |
kernel-rt-devel-matched | 5.14.0-503.19.1.el9_5 | |
kernel-rt-kvm | 5.14.0-503.19.1.el9_5 | |
kernel-rt-modules | 5.14.0-503.19.1.el9_5 | |
kernel-rt-modules-core | 5.14.0-503.19.1.el9_5 | |
kernel-rt-modules-extra | 5.14.0-503.19.1.el9_5 | |
kernel-rt-modules-internal | 5.14.0-503.19.1.el9_5 | |
kernel-rt-modules-partner | 5.14.0-503.19.1.el9_5 | |
kernel-selftests-internal | 5.14.0-503.19.1.el9_5 | |
libipa_hbac-devel | 2.9.5-4.el9_5.4 | |
libperf-devel | 5.14.0-503.19.1.el9_5 | |
libsss_certmap-devel | 2.9.5-4.el9_5.4 | |
libsss_idmap-devel | 2.9.5-4.el9_5.4 | |
libsss_simpleifp-devel | 2.9.5-4.el9_5.4 | |
libvirt-wireshark | 10.5.0-7.2.el9_5 | |

