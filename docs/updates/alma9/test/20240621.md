## 2024-06-21

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ghostscript | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-debuginfo | 9.54.0-16.el9_4 | |
ghostscript-debugsource | 9.54.0-16.el9_4 | |
ghostscript-doc | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-gtk-debuginfo | 9.54.0-16.el9_4 | |
ghostscript-tools-dvipdf | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-tools-fonts | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-tools-printing | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-x11 | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-x11-debuginfo | 9.54.0-16.el9_4 | |
libgs | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
libgs-debuginfo | 9.54.0-16.el9_4 | |
qemu-guest-agent | 8.2.0-11.el9_4.3 | |
qemu-guest-agent-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-img | 8.2.0-11.el9_4.3 | |
qemu-img-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm | 8.2.0-11.el9_4.3 | |
qemu-kvm-audio-pa | 8.2.0-11.el9_4.3 | |
qemu-kvm-audio-pa-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-blkio | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-blkio-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-curl | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-curl-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-rbd | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-rbd-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-common | 8.2.0-11.el9_4.3 | |
qemu-kvm-common-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-core | 8.2.0-11.el9_4.3 | |
qemu-kvm-core-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-debugsource | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-display-virtio-gpu | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-display-virtio-gpu-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-display-virtio-gpu-pci | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-display-virtio-gpu-pci-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-display-virtio-vga | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-display-virtio-vga-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-usb-host | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-usb-host-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-usb-redirect | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-usb-redirect-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-docs | 8.2.0-11.el9_4.3 | |
qemu-kvm-tools | 8.2.0-11.el9_4.3 | |
qemu-kvm-tools-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-ui-egl-headless | 8.2.0-11.el9_4.3 | |
qemu-kvm-ui-egl-headless-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-ui-opengl | 8.2.0-11.el9_4.3 | |
qemu-kvm-ui-opengl-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-pr-helper | 8.2.0-11.el9_4.3 | |
qemu-pr-helper-debuginfo | 8.2.0-11.el9_4.3 | |
thunderbird | 115.12.1-1.el9_4.alma.1 | [ALSA-2024:4002](https://errata.almalinux.org/9/ALSA-2024-4002.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5688](https://access.redhat.com/security/cve/CVE-2024-5688), [CVE-2024-5690](https://access.redhat.com/security/cve/CVE-2024-5690), [CVE-2024-5691](https://access.redhat.com/security/cve/CVE-2024-5691), [CVE-2024-5693](https://access.redhat.com/security/cve/CVE-2024-5693), [CVE-2024-5696](https://access.redhat.com/security/cve/CVE-2024-5696), [CVE-2024-5700](https://access.redhat.com/security/cve/CVE-2024-5700), [CVE-2024-5702](https://access.redhat.com/security/cve/CVE-2024-5702))
thunderbird-debuginfo | 115.12.1-1.el9_4.alma.1 | |
thunderbird-debugsource | 115.12.1-1.el9_4.alma.1 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libgs-devel | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ghostscript-gtk | 9.54.0-16.el9_4 | |
qemu-kvm-audio-dbus | 8.2.0-11.el9_4.3 | |
qemu-kvm-audio-dbus-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-tests | 8.2.0-11.el9_4.3 | |
qemu-kvm-tests-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-ui-dbus | 8.2.0-11.el9_4.3 | |
qemu-kvm-ui-dbus-debuginfo | 8.2.0-11.el9_4.3 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ghostscript | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-debuginfo | 9.54.0-16.el9_4 | |
ghostscript-debugsource | 9.54.0-16.el9_4 | |
ghostscript-doc | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-gtk-debuginfo | 9.54.0-16.el9_4 | |
ghostscript-tools-dvipdf | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-tools-fonts | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-tools-printing | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-x11 | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-x11-debuginfo | 9.54.0-16.el9_4 | |
libgs | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
libgs-debuginfo | 9.54.0-16.el9_4 | |
qemu-guest-agent | 8.2.0-11.el9_4.3 | |
qemu-guest-agent-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-img | 8.2.0-11.el9_4.3 | |
qemu-img-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm | 8.2.0-11.el9_4.3 | |
qemu-kvm-audio-pa | 8.2.0-11.el9_4.3 | |
qemu-kvm-audio-pa-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-blkio | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-blkio-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-curl | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-curl-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-rbd | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-rbd-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-common | 8.2.0-11.el9_4.3 | |
qemu-kvm-common-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-core | 8.2.0-11.el9_4.3 | |
qemu-kvm-core-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-debugsource | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-display-virtio-gpu | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-display-virtio-gpu-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-display-virtio-gpu-pci | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-display-virtio-gpu-pci-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-usb-host | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-usb-host-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-usb-redirect | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-usb-redirect-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-docs | 8.2.0-11.el9_4.3 | |
qemu-kvm-tools | 8.2.0-11.el9_4.3 | |
qemu-kvm-tools-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-pr-helper | 8.2.0-11.el9_4.3 | |
qemu-pr-helper-debuginfo | 8.2.0-11.el9_4.3 | |
thunderbird | 115.12.1-1.el9_4.alma.1 | [ALSA-2024:4002](https://errata.almalinux.org/9/ALSA-2024-4002.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5688](https://access.redhat.com/security/cve/CVE-2024-5688), [CVE-2024-5690](https://access.redhat.com/security/cve/CVE-2024-5690), [CVE-2024-5691](https://access.redhat.com/security/cve/CVE-2024-5691), [CVE-2024-5693](https://access.redhat.com/security/cve/CVE-2024-5693), [CVE-2024-5696](https://access.redhat.com/security/cve/CVE-2024-5696), [CVE-2024-5700](https://access.redhat.com/security/cve/CVE-2024-5700), [CVE-2024-5702](https://access.redhat.com/security/cve/CVE-2024-5702))
thunderbird-debuginfo | 115.12.1-1.el9_4.alma.1 | |
thunderbird-debugsource | 115.12.1-1.el9_4.alma.1 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libgs-devel | 9.54.0-16.el9_4 | [ALSA-2024:3999](https://errata.almalinux.org/9/ALSA-2024-3999.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ghostscript-gtk | 9.54.0-16.el9_4 | |
qemu-kvm-audio-dbus | 8.2.0-11.el9_4.3 | |
qemu-kvm-audio-dbus-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-tests | 8.2.0-11.el9_4.3 | |
qemu-kvm-tests-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-ui-dbus | 8.2.0-11.el9_4.3 | |
qemu-kvm-ui-dbus-debuginfo | 8.2.0-11.el9_4.3 | |

