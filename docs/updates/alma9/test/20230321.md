## 2023-03-21

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.5.0-1.el9.alma | |
sos-audit | 4.5.0-1.el9.alma | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.9.0-3.el9_1.alma | |
firefox-debuginfo | 102.9.0-3.el9_1.alma | |
firefox-debugsource | 102.9.0-3.el9_1.alma | |
firefox-x11 | 102.9.0-3.el9_1.alma | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.5.0-1.el9.alma | |
sos-audit | 4.5.0-1.el9.alma | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.9.0-3.el9_1.alma | |
firefox-debuginfo | 102.9.0-3.el9_1.alma | |
firefox-debugsource | 102.9.0-3.el9_1.alma | |
firefox-x11 | 102.9.0-3.el9_1.alma | |

