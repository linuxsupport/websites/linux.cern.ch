## 2025-01-13

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dpdk | 23.11-2.el9_5 | [ALSA-2025:0210](https://errata.almalinux.org/9/ALSA-2025-0210.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11614](https://access.redhat.com/security/cve/CVE-2024-11614))
dpdk-debuginfo | 23.11-2.el9_5 | |
dpdk-debugsource | 23.11-2.el9_5 | |
dpdk-devel | 23.11-2.el9_5 | [ALSA-2025:0210](https://errata.almalinux.org/9/ALSA-2025-0210.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11614](https://access.redhat.com/security/cve/CVE-2024-11614))
dpdk-doc | 23.11-2.el9_5 | [ALSA-2025:0210](https://errata.almalinux.org/9/ALSA-2025-0210.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11614](https://access.redhat.com/security/cve/CVE-2024-11614))
dpdk-tools | 23.11-2.el9_5 | [ALSA-2025:0210](https://errata.almalinux.org/9/ALSA-2025-0210.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11614](https://access.redhat.com/security/cve/CVE-2024-11614))
firefox | 128.6.0-1.el9_5 | [ALSA-2025:0080](https://errata.almalinux.org/9/ALSA-2025-0080.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-0237](https://access.redhat.com/security/cve/CVE-2025-0237), [CVE-2025-0238](https://access.redhat.com/security/cve/CVE-2025-0238), [CVE-2025-0239](https://access.redhat.com/security/cve/CVE-2025-0239), [CVE-2025-0240](https://access.redhat.com/security/cve/CVE-2025-0240), [CVE-2025-0241](https://access.redhat.com/security/cve/CVE-2025-0241), [CVE-2025-0242](https://access.redhat.com/security/cve/CVE-2025-0242), [CVE-2025-0243](https://access.redhat.com/security/cve/CVE-2025-0243))
firefox-debuginfo | 128.6.0-1.el9_5 | |
firefox-debugsource | 128.6.0-1.el9_5 | |
firefox-x11 | 128.6.0-1.el9_5 | [ALSA-2025:0080](https://errata.almalinux.org/9/ALSA-2025-0080.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-0237](https://access.redhat.com/security/cve/CVE-2025-0237), [CVE-2025-0238](https://access.redhat.com/security/cve/CVE-2025-0238), [CVE-2025-0239](https://access.redhat.com/security/cve/CVE-2025-0239), [CVE-2025-0240](https://access.redhat.com/security/cve/CVE-2025-0240), [CVE-2025-0241](https://access.redhat.com/security/cve/CVE-2025-0241), [CVE-2025-0242](https://access.redhat.com/security/cve/CVE-2025-0242), [CVE-2025-0243](https://access.redhat.com/security/cve/CVE-2025-0243))
iperf3 | 3.9-13.el9_5.1 | [ALSA-2025:0161](https://errata.almalinux.org/9/ALSA-2025-0161.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53580](https://access.redhat.com/security/cve/CVE-2024-53580))
iperf3-debuginfo | 3.9-13.el9_5.1 | |
iperf3-debugsource | 3.9-13.el9_5.1 | |
thunderbird | 128.6.0-3.el9_5.alma.1 | [ALSA-2025:0147](https://errata.almalinux.org/9/ALSA-2025-0147.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-0242](https://access.redhat.com/security/cve/CVE-2025-0242), [CVE-2025-0243](https://access.redhat.com/security/cve/CVE-2025-0243))
thunderbird-debuginfo | 128.6.0-3.el9_5.alma.1 | |
thunderbird-debugsource | 128.6.0-3.el9_5.alma.1 | |
webkit2gtk3 | 2.46.5-1.el9_5 | [ALSA-2025:0146](https://errata.almalinux.org/9/ALSA-2025-0146.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54479](https://access.redhat.com/security/cve/CVE-2024-54479), [CVE-2024-54502](https://access.redhat.com/security/cve/CVE-2024-54502), [CVE-2024-54505](https://access.redhat.com/security/cve/CVE-2024-54505), [CVE-2024-54508](https://access.redhat.com/security/cve/CVE-2024-54508))
webkit2gtk3-debuginfo | 2.46.5-1.el9_5 | |
webkit2gtk3-debugsource | 2.46.5-1.el9_5 | |
webkit2gtk3-devel | 2.46.5-1.el9_5 | [ALSA-2025:0146](https://errata.almalinux.org/9/ALSA-2025-0146.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54479](https://access.redhat.com/security/cve/CVE-2024-54479), [CVE-2024-54502](https://access.redhat.com/security/cve/CVE-2024-54502), [CVE-2024-54505](https://access.redhat.com/security/cve/CVE-2024-54505), [CVE-2024-54508](https://access.redhat.com/security/cve/CVE-2024-54508))
webkit2gtk3-devel-debuginfo | 2.46.5-1.el9_5 | |
webkit2gtk3-jsc | 2.46.5-1.el9_5 | [ALSA-2025:0146](https://errata.almalinux.org/9/ALSA-2025-0146.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54479](https://access.redhat.com/security/cve/CVE-2024-54479), [CVE-2024-54502](https://access.redhat.com/security/cve/CVE-2024-54502), [CVE-2024-54505](https://access.redhat.com/security/cve/CVE-2024-54505), [CVE-2024-54508](https://access.redhat.com/security/cve/CVE-2024-54508))
webkit2gtk3-jsc-debuginfo | 2.46.5-1.el9_5 | |
webkit2gtk3-jsc-devel | 2.46.5-1.el9_5 | [ALSA-2025:0146](https://errata.almalinux.org/9/ALSA-2025-0146.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54479](https://access.redhat.com/security/cve/CVE-2024-54479), [CVE-2024-54502](https://access.redhat.com/security/cve/CVE-2024-54502), [CVE-2024-54505](https://access.redhat.com/security/cve/CVE-2024-54505), [CVE-2024-54508](https://access.redhat.com/security/cve/CVE-2024-54508))
webkit2gtk3-jsc-devel-debuginfo | 2.46.5-1.el9_5 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iperf3-devel | 3.9-13.el9_5.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dpdk | 23.11-2.el9_5 | [ALSA-2025:0210](https://errata.almalinux.org/9/ALSA-2025-0210.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11614](https://access.redhat.com/security/cve/CVE-2024-11614))
dpdk-debuginfo | 23.11-2.el9_5 | |
dpdk-debugsource | 23.11-2.el9_5 | |
dpdk-devel | 23.11-2.el9_5 | [ALSA-2025:0210](https://errata.almalinux.org/9/ALSA-2025-0210.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11614](https://access.redhat.com/security/cve/CVE-2024-11614))
dpdk-doc | 23.11-2.el9_5 | [ALSA-2025:0210](https://errata.almalinux.org/9/ALSA-2025-0210.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11614](https://access.redhat.com/security/cve/CVE-2024-11614))
dpdk-tools | 23.11-2.el9_5 | [ALSA-2025:0210](https://errata.almalinux.org/9/ALSA-2025-0210.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11614](https://access.redhat.com/security/cve/CVE-2024-11614))
firefox | 128.6.0-1.el9_5 | [ALSA-2025:0080](https://errata.almalinux.org/9/ALSA-2025-0080.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-0237](https://access.redhat.com/security/cve/CVE-2025-0237), [CVE-2025-0238](https://access.redhat.com/security/cve/CVE-2025-0238), [CVE-2025-0239](https://access.redhat.com/security/cve/CVE-2025-0239), [CVE-2025-0240](https://access.redhat.com/security/cve/CVE-2025-0240), [CVE-2025-0241](https://access.redhat.com/security/cve/CVE-2025-0241), [CVE-2025-0242](https://access.redhat.com/security/cve/CVE-2025-0242), [CVE-2025-0243](https://access.redhat.com/security/cve/CVE-2025-0243))
firefox-debuginfo | 128.6.0-1.el9_5 | |
firefox-debugsource | 128.6.0-1.el9_5 | |
firefox-x11 | 128.6.0-1.el9_5 | [ALSA-2025:0080](https://errata.almalinux.org/9/ALSA-2025-0080.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-0237](https://access.redhat.com/security/cve/CVE-2025-0237), [CVE-2025-0238](https://access.redhat.com/security/cve/CVE-2025-0238), [CVE-2025-0239](https://access.redhat.com/security/cve/CVE-2025-0239), [CVE-2025-0240](https://access.redhat.com/security/cve/CVE-2025-0240), [CVE-2025-0241](https://access.redhat.com/security/cve/CVE-2025-0241), [CVE-2025-0242](https://access.redhat.com/security/cve/CVE-2025-0242), [CVE-2025-0243](https://access.redhat.com/security/cve/CVE-2025-0243))
iperf3 | 3.9-13.el9_5.1 | [ALSA-2025:0161](https://errata.almalinux.org/9/ALSA-2025-0161.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53580](https://access.redhat.com/security/cve/CVE-2024-53580))
iperf3-debuginfo | 3.9-13.el9_5.1 | |
iperf3-debugsource | 3.9-13.el9_5.1 | |
thunderbird | 128.6.0-3.el9_5.alma.1 | [ALSA-2025:0147](https://errata.almalinux.org/9/ALSA-2025-0147.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-0242](https://access.redhat.com/security/cve/CVE-2025-0242), [CVE-2025-0243](https://access.redhat.com/security/cve/CVE-2025-0243))
thunderbird-debuginfo | 128.6.0-3.el9_5.alma.1 | |
thunderbird-debugsource | 128.6.0-3.el9_5.alma.1 | |
webkit2gtk3 | 2.46.5-1.el9_5 | [ALSA-2025:0146](https://errata.almalinux.org/9/ALSA-2025-0146.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54479](https://access.redhat.com/security/cve/CVE-2024-54479), [CVE-2024-54502](https://access.redhat.com/security/cve/CVE-2024-54502), [CVE-2024-54505](https://access.redhat.com/security/cve/CVE-2024-54505), [CVE-2024-54508](https://access.redhat.com/security/cve/CVE-2024-54508))
webkit2gtk3-debuginfo | 2.46.5-1.el9_5 | |
webkit2gtk3-debugsource | 2.46.5-1.el9_5 | |
webkit2gtk3-devel | 2.46.5-1.el9_5 | [ALSA-2025:0146](https://errata.almalinux.org/9/ALSA-2025-0146.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54479](https://access.redhat.com/security/cve/CVE-2024-54479), [CVE-2024-54502](https://access.redhat.com/security/cve/CVE-2024-54502), [CVE-2024-54505](https://access.redhat.com/security/cve/CVE-2024-54505), [CVE-2024-54508](https://access.redhat.com/security/cve/CVE-2024-54508))
webkit2gtk3-devel-debuginfo | 2.46.5-1.el9_5 | |
webkit2gtk3-jsc | 2.46.5-1.el9_5 | [ALSA-2025:0146](https://errata.almalinux.org/9/ALSA-2025-0146.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54479](https://access.redhat.com/security/cve/CVE-2024-54479), [CVE-2024-54502](https://access.redhat.com/security/cve/CVE-2024-54502), [CVE-2024-54505](https://access.redhat.com/security/cve/CVE-2024-54505), [CVE-2024-54508](https://access.redhat.com/security/cve/CVE-2024-54508))
webkit2gtk3-jsc-debuginfo | 2.46.5-1.el9_5 | |
webkit2gtk3-jsc-devel | 2.46.5-1.el9_5 | [ALSA-2025:0146](https://errata.almalinux.org/9/ALSA-2025-0146.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54479](https://access.redhat.com/security/cve/CVE-2024-54479), [CVE-2024-54502](https://access.redhat.com/security/cve/CVE-2024-54502), [CVE-2024-54505](https://access.redhat.com/security/cve/CVE-2024-54505), [CVE-2024-54508](https://access.redhat.com/security/cve/CVE-2024-54508))
webkit2gtk3-jsc-devel-debuginfo | 2.46.5-1.el9_5 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iperf3-devel | 3.9-13.el9_5.1 | |

