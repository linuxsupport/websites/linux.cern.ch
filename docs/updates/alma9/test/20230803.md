## 2023-08-03

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
curl | 7.76.1-23.el9_2.2 | |
curl-debuginfo | 7.76.1-23.el9_2.2 | |
curl-debugsource | 7.76.1-23.el9_2.2 | |
curl-minimal | 7.76.1-23.el9_2.2 | |
curl-minimal-debuginfo | 7.76.1-23.el9_2.2 | |
libcurl | 7.76.1-23.el9_2.2 | |
libcurl-debuginfo | 7.76.1-23.el9_2.2 | |
libcurl-minimal | 7.76.1-23.el9_2.2 | |
libcurl-minimal-debuginfo | 7.76.1-23.el9_2.2 | |
libeconf | 0.4.1-3.el9_2 | |
libeconf-debuginfo | 0.4.1-3.el9_2 | |
libeconf-debugsource | 0.4.1-3.el9_2 | |
libeconf-utils-debuginfo | 0.4.1-3.el9_2 | |
libxml2 | 2.9.13-3.el9_2.1 | |
libxml2-debuginfo | 2.9.13-3.el9_2.1 | |
libxml2-debugsource | 2.9.13-3.el9_2.1 | |
openssh | 8.7p1-30.el9_2 | [ALSA-2023:4412](https://errata.almalinux.org/9/ALSA-2023-4412.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-clients | 8.7p1-30.el9_2 | [ALSA-2023:4412](https://errata.almalinux.org/9/ALSA-2023-4412.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-clients-debuginfo | 8.7p1-30.el9_2 | |
openssh-debuginfo | 8.7p1-30.el9_2 | |
openssh-debugsource | 8.7p1-30.el9_2 | |
openssh-keycat | 8.7p1-30.el9_2 | [ALSA-2023:4412](https://errata.almalinux.org/9/ALSA-2023-4412.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-keycat-debuginfo | 8.7p1-30.el9_2 | |
openssh-server | 8.7p1-30.el9_2 | [ALSA-2023:4412](https://errata.almalinux.org/9/ALSA-2023-4412.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-server-debuginfo | 8.7p1-30.el9_2 | |
openssh-sk-dummy-debuginfo | 8.7p1-30.el9_2 | |
python3-libxml2 | 2.9.13-3.el9_2.1 | |
python3-libxml2-debuginfo | 2.9.13-3.el9_2.1 | |
python3-requests | 2.25.1-7.el9_2 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cjose | 0.6.1-13.el9_2.alma | |
cjose-debuginfo | 0.6.1-13.el9_2.alma | |
cjose-debugsource | 0.6.1-13.el9_2.alma | |
libcurl-devel | 7.76.1-23.el9_2.2 | |
libxml2-devel | 2.9.13-3.el9_2.1 | |
nodejs | 16.20.1-1.el9_2 | |
nodejs | 18.16.1-1.module_el9.2.0+31+cbae0c8e | |
nodejs-debuginfo | 16.20.1-1.el9_2 | |
nodejs-debuginfo | 18.16.1-1.module_el9.2.0+31+cbae0c8e | |
nodejs-debugsource | 16.20.1-1.el9_2 | |
nodejs-debugsource | 18.16.1-1.module_el9.2.0+31+cbae0c8e | |
nodejs-devel | 18.16.1-1.module_el9.2.0+31+cbae0c8e | |
nodejs-docs | 16.20.1-1.el9_2 | |
nodejs-docs | 18.16.1-1.module_el9.2.0+31+cbae0c8e | |
nodejs-full-i18n | 16.20.1-1.el9_2 | |
nodejs-full-i18n | 18.16.1-1.module_el9.2.0+31+cbae0c8e | |
nodejs-libs | 16.20.1-1.el9_2 | |
nodejs-libs-debuginfo | 16.20.1-1.el9_2 | |
npm | 8.19.4-1.16.20.1.1.el9_2 | |
npm | 9.5.1-1.18.16.1.1.module_el9.2.0+31+cbae0c8e | |
openssh-askpass | 8.7p1-30.el9_2 | [ALSA-2023:4412](https://errata.almalinux.org/9/ALSA-2023-4412.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-askpass-debuginfo | 8.7p1-30.el9_2 | |
openssh-sk-dummy-debuginfo | 8.7p1-30.el9_2 | |
pam_ssh_agent_auth | 0.10.4-5.30.el9_2 | [ALSA-2023:4412](https://errata.almalinux.org/9/ALSA-2023-4412.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
pam_ssh_agent_auth-debuginfo | 0.10.4-5.30.el9_2 | |
postgresql | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-contrib | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-contrib-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-debugsource | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-docs | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-docs-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-plperl | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-plperl-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-plpython3 | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-plpython3-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-pltcl | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-pltcl-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-private-devel | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-private-libs | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-private-libs-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-server | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-server-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-server-devel | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-server-devel-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-static | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-test | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-test-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-test-rpm-macros | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-upgrade | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-upgrade-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-upgrade-devel | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-upgrade-devel-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
python3-requests+security | 2.25.1-7.el9_2 | |
python3-requests+socks | 2.25.1-7.el9_2 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cjose-devel | 0.6.1-13.el9_2.alma | |
libeconf-devel | 0.4.1-3.el9_2 | |
libeconf-utils | 0.4.1-3.el9_2 | |
libxml2-static | 2.9.13-3.el9_2.1 | |
nodejs-devel | 16.20.1-1.el9_2 | |
openssh-sk-dummy | 8.7p1-30.el9_2 | |
v8-devel | 9.4.146.26-1.16.20.1.1.el9_2 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
curl | 7.76.1-23.el9_2.2 | |
curl-debuginfo | 7.76.1-23.el9_2.2 | |
curl-debugsource | 7.76.1-23.el9_2.2 | |
curl-minimal | 7.76.1-23.el9_2.2 | |
curl-minimal-debuginfo | 7.76.1-23.el9_2.2 | |
libcurl | 7.76.1-23.el9_2.2 | |
libcurl-debuginfo | 7.76.1-23.el9_2.2 | |
libcurl-minimal | 7.76.1-23.el9_2.2 | |
libcurl-minimal-debuginfo | 7.76.1-23.el9_2.2 | |
libeconf | 0.4.1-3.el9_2 | |
libeconf-debuginfo | 0.4.1-3.el9_2 | |
libeconf-debugsource | 0.4.1-3.el9_2 | |
libeconf-utils-debuginfo | 0.4.1-3.el9_2 | |
libxml2 | 2.9.13-3.el9_2.1 | |
libxml2-debuginfo | 2.9.13-3.el9_2.1 | |
libxml2-debugsource | 2.9.13-3.el9_2.1 | |
openssh | 8.7p1-30.el9_2 | [ALSA-2023:4412](https://errata.almalinux.org/9/ALSA-2023-4412.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-clients | 8.7p1-30.el9_2 | [ALSA-2023:4412](https://errata.almalinux.org/9/ALSA-2023-4412.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-clients-debuginfo | 8.7p1-30.el9_2 | |
openssh-debuginfo | 8.7p1-30.el9_2 | |
openssh-debugsource | 8.7p1-30.el9_2 | |
openssh-keycat | 8.7p1-30.el9_2 | [ALSA-2023:4412](https://errata.almalinux.org/9/ALSA-2023-4412.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-keycat-debuginfo | 8.7p1-30.el9_2 | |
openssh-server | 8.7p1-30.el9_2 | [ALSA-2023:4412](https://errata.almalinux.org/9/ALSA-2023-4412.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-server-debuginfo | 8.7p1-30.el9_2 | |
openssh-sk-dummy-debuginfo | 8.7p1-30.el9_2 | |
python3-libxml2 | 2.9.13-3.el9_2.1 | |
python3-libxml2-debuginfo | 2.9.13-3.el9_2.1 | |
python3-requests | 2.25.1-7.el9_2 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cjose | 0.6.1-13.el9_2.alma | |
cjose-debuginfo | 0.6.1-13.el9_2.alma | |
cjose-debugsource | 0.6.1-13.el9_2.alma | |
libcurl-devel | 7.76.1-23.el9_2.2 | |
libxml2-devel | 2.9.13-3.el9_2.1 | |
nodejs | 16.20.1-1.el9_2 | |
nodejs | 18.16.1-1.module_el9.2.0+31+cbae0c8e | |
nodejs-debuginfo | 16.20.1-1.el9_2 | |
nodejs-debuginfo | 18.16.1-1.module_el9.2.0+31+cbae0c8e | |
nodejs-debugsource | 16.20.1-1.el9_2 | |
nodejs-debugsource | 18.16.1-1.module_el9.2.0+31+cbae0c8e | |
nodejs-devel | 18.16.1-1.module_el9.2.0+31+cbae0c8e | |
nodejs-docs | 16.20.1-1.el9_2 | |
nodejs-docs | 18.16.1-1.module_el9.2.0+31+cbae0c8e | |
nodejs-full-i18n | 16.20.1-1.el9_2 | |
nodejs-full-i18n | 18.16.1-1.module_el9.2.0+31+cbae0c8e | |
nodejs-libs | 16.20.1-1.el9_2 | |
nodejs-libs-debuginfo | 16.20.1-1.el9_2 | |
npm | 8.19.4-1.16.20.1.1.el9_2 | |
npm | 9.5.1-1.18.16.1.1.module_el9.2.0+31+cbae0c8e | |
openssh-askpass | 8.7p1-30.el9_2 | [ALSA-2023:4412](https://errata.almalinux.org/9/ALSA-2023-4412.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-askpass-debuginfo | 8.7p1-30.el9_2 | |
openssh-sk-dummy-debuginfo | 8.7p1-30.el9_2 | |
pam_ssh_agent_auth | 0.10.4-5.30.el9_2 | [ALSA-2023:4412](https://errata.almalinux.org/9/ALSA-2023-4412.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
pam_ssh_agent_auth-debuginfo | 0.10.4-5.30.el9_2 | |
postgresql | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-contrib | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-contrib-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-debugsource | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-docs | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-docs-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-plperl | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-plperl-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-plpython3 | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-plpython3-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-pltcl | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-pltcl-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-private-devel | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-private-libs | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-private-libs-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-server | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-server-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-server-devel | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-server-devel-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-static | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-test | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-test-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-test-rpm-macros | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-upgrade | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-upgrade-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-upgrade-devel | 15.3-1.module_el9.2.0+32+f3c125b5 | |
postgresql-upgrade-devel-debuginfo | 15.3-1.module_el9.2.0+32+f3c125b5 | |
python3-requests+security | 2.25.1-7.el9_2 | |
python3-requests+socks | 2.25.1-7.el9_2 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cjose-devel | 0.6.1-13.el9_2.alma | |
libeconf-devel | 0.4.1-3.el9_2 | |
libeconf-utils | 0.4.1-3.el9_2 | |
libxml2-static | 2.9.13-3.el9_2.1 | |
nodejs-devel | 16.20.1-1.el9_2 | |
openssh-sk-dummy | 8.7p1-30.el9_2 | |
v8-devel | 9.4.146.26-1.16.20.1.1.el9_2 | |

