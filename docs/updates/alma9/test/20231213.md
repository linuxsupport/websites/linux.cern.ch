## 2023-12-13

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tracker-miners | 3.1.2-4.el9_3 | [ALSA-2023:7712](https://errata.almalinux.org/9/ALSA-2023-7712.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5557](https://access.redhat.com/security/cve/CVE-2023-5557))
tracker-miners-debuginfo | 3.1.2-4.el9_3 | |
tracker-miners-debugsource | 3.1.2-4.el9_3 | |
webkit2gtk3 | 2.40.5-1.el9_3.1 | [ALSA-2023:7715](https://errata.almalinux.org/9/ALSA-2023-7715.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-42917](https://access.redhat.com/security/cve/CVE-2023-42917))
webkit2gtk3-debuginfo | 2.40.5-1.el9_3.1 | |
webkit2gtk3-debugsource | 2.40.5-1.el9_3.1 | |
webkit2gtk3-devel | 2.40.5-1.el9_3.1 | [ALSA-2023:7715](https://errata.almalinux.org/9/ALSA-2023-7715.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-42917](https://access.redhat.com/security/cve/CVE-2023-42917))
webkit2gtk3-devel-debuginfo | 2.40.5-1.el9_3.1 | |
webkit2gtk3-jsc | 2.40.5-1.el9_3.1 | [ALSA-2023:7715](https://errata.almalinux.org/9/ALSA-2023-7715.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-42917](https://access.redhat.com/security/cve/CVE-2023-42917))
webkit2gtk3-jsc-debuginfo | 2.40.5-1.el9_3.1 | |
webkit2gtk3-jsc-devel | 2.40.5-1.el9_3.1 | [ALSA-2023:7715](https://errata.almalinux.org/9/ALSA-2023-7715.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-42917](https://access.redhat.com/security/cve/CVE-2023-42917))
webkit2gtk3-jsc-devel-debuginfo | 2.40.5-1.el9_3.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tracker-miners | 3.1.2-4.el9_3 | [ALSA-2023:7712](https://errata.almalinux.org/9/ALSA-2023-7712.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5557](https://access.redhat.com/security/cve/CVE-2023-5557))
tracker-miners-debuginfo | 3.1.2-4.el9_3 | |
tracker-miners-debugsource | 3.1.2-4.el9_3 | |
webkit2gtk3 | 2.40.5-1.el9_3.1 | [ALSA-2023:7715](https://errata.almalinux.org/9/ALSA-2023-7715.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-42917](https://access.redhat.com/security/cve/CVE-2023-42917))
webkit2gtk3-debuginfo | 2.40.5-1.el9_3.1 | |
webkit2gtk3-debugsource | 2.40.5-1.el9_3.1 | |
webkit2gtk3-devel | 2.40.5-1.el9_3.1 | [ALSA-2023:7715](https://errata.almalinux.org/9/ALSA-2023-7715.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-42917](https://access.redhat.com/security/cve/CVE-2023-42917))
webkit2gtk3-devel-debuginfo | 2.40.5-1.el9_3.1 | |
webkit2gtk3-jsc | 2.40.5-1.el9_3.1 | [ALSA-2023:7715](https://errata.almalinux.org/9/ALSA-2023-7715.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-42917](https://access.redhat.com/security/cve/CVE-2023-42917))
webkit2gtk3-jsc-debuginfo | 2.40.5-1.el9_3.1 | |
webkit2gtk3-jsc-devel | 2.40.5-1.el9_3.1 | [ALSA-2023:7715](https://errata.almalinux.org/9/ALSA-2023-7715.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-42917](https://access.redhat.com/security/cve/CVE-2023-42917))
webkit2gtk3-jsc-devel-debuginfo | 2.40.5-1.el9_3.1 | |

