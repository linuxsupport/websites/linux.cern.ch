## 2023-06-13

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
c-ares | 1.17.1-5.el9_2.1 | |
c-ares-debuginfo | 1.17.1-5.el9_2.1 | |
c-ares-debugsource | 1.17.1-5.el9_2.1 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
c-ares-devel | 1.17.1-5.el9_2.1 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
c-ares | 1.17.1-5.el9_2.1 | |
c-ares-debuginfo | 1.17.1-5.el9_2.1 | |
c-ares-debugsource | 1.17.1-5.el9_2.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
c-ares-devel | 1.17.1-5.el9_2.1 | |

