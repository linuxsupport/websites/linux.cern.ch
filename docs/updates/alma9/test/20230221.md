## 2023-02-21

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.8.0-2.el9_1.alma | |
firefox-debuginfo | 102.8.0-2.el9_1.alma | |
firefox-debugsource | 102.8.0-2.el9_1.alma | |
firefox-x11 | 102.8.0-2.el9_1.alma | |
thunderbird | 102.8.0-2.el9_1.alma | |
thunderbird-debuginfo | 102.8.0-2.el9_1.alma | |
thunderbird-debugsource | 102.8.0-2.el9_1.alma | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.8.0-2.el9_1.alma.plus | |
thunderbird-debuginfo | 102.8.0-2.el9_1.alma.plus | |
thunderbird-debugsource | 102.8.0-2.el9_1.alma.plus | |
thunderbird-librnp-rnp | 102.8.0-2.el9_1.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.8.0-2.el9_1.alma.plus | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
elrepo-release | 9.1-1.el9.elrepo | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.8.0-2.el9_1.alma | |
firefox-debuginfo | 102.8.0-2.el9_1.alma | |
firefox-debugsource | 102.8.0-2.el9_1.alma | |
firefox-x11 | 102.8.0-2.el9_1.alma | |
thunderbird | 102.8.0-2.el9_1.alma | |
thunderbird-debuginfo | 102.8.0-2.el9_1.alma | |
thunderbird-debugsource | 102.8.0-2.el9_1.alma | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.8.0-2.el9_1.alma.plus | |
thunderbird-debuginfo | 102.8.0-2.el9_1.alma.plus | |
thunderbird-debugsource | 102.8.0-2.el9_1.alma.plus | |
thunderbird-librnp-rnp | 102.8.0-2.el9_1.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.8.0-2.el9_1.alma.plus | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
elrepo-release | 9.1-1.el9.elrepo | |

