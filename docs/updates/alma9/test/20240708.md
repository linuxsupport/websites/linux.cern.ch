## 2024-07-08

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh | 8.7p1-38.el9_4.1 | [ALSA-2024:4312](https://errata.almalinux.org/9/ALSA-2024-4312.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6387](https://access.redhat.com/security/cve/CVE-2024-6387))
openssh-clients | 8.7p1-38.el9_4.1 | [ALSA-2024:4312](https://errata.almalinux.org/9/ALSA-2024-4312.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6387](https://access.redhat.com/security/cve/CVE-2024-6387))
openssh-clients-debuginfo | 8.7p1-38.el9_4.1 | |
openssh-debuginfo | 8.7p1-38.el9_4.1 | |
openssh-debugsource | 8.7p1-38.el9_4.1 | |
openssh-keycat | 8.7p1-38.el9_4.1 | [ALSA-2024:4312](https://errata.almalinux.org/9/ALSA-2024-4312.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6387](https://access.redhat.com/security/cve/CVE-2024-6387))
openssh-keycat-debuginfo | 8.7p1-38.el9_4.1 | |
openssh-server | 8.7p1-38.el9_4.1 | [ALSA-2024:4312](https://errata.almalinux.org/9/ALSA-2024-4312.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6387](https://access.redhat.com/security/cve/CVE-2024-6387))
openssh-server-debuginfo | 8.7p1-38.el9_4.1 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh-askpass | 8.7p1-38.el9_4.1 | [ALSA-2024:4312](https://errata.almalinux.org/9/ALSA-2024-4312.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6387](https://access.redhat.com/security/cve/CVE-2024-6387))
openssh-askpass-debuginfo | 8.7p1-38.el9_4.1 | |
pam_ssh_agent_auth | 0.10.4-5.38.el9_4.1 | [ALSA-2024:4312](https://errata.almalinux.org/9/ALSA-2024-4312.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6387](https://access.redhat.com/security/cve/CVE-2024-6387))
pam_ssh_agent_auth-debuginfo | 0.10.4-5.38.el9_4.1 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh-sk-dummy | 8.7p1-38.el9_4.1 | |
openssh-sk-dummy-debuginfo | 8.7p1-38.el9_4.1 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh | 8.7p1-38.el9_4.1 | [ALSA-2024:4312](https://errata.almalinux.org/9/ALSA-2024-4312.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6387](https://access.redhat.com/security/cve/CVE-2024-6387))
openssh-clients | 8.7p1-38.el9_4.1 | [ALSA-2024:4312](https://errata.almalinux.org/9/ALSA-2024-4312.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6387](https://access.redhat.com/security/cve/CVE-2024-6387))
openssh-clients-debuginfo | 8.7p1-38.el9_4.1 | |
openssh-debuginfo | 8.7p1-38.el9_4.1 | |
openssh-debugsource | 8.7p1-38.el9_4.1 | |
openssh-keycat | 8.7p1-38.el9_4.1 | [ALSA-2024:4312](https://errata.almalinux.org/9/ALSA-2024-4312.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6387](https://access.redhat.com/security/cve/CVE-2024-6387))
openssh-keycat-debuginfo | 8.7p1-38.el9_4.1 | |
openssh-server | 8.7p1-38.el9_4.1 | [ALSA-2024:4312](https://errata.almalinux.org/9/ALSA-2024-4312.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6387](https://access.redhat.com/security/cve/CVE-2024-6387))
openssh-server-debuginfo | 8.7p1-38.el9_4.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh-askpass | 8.7p1-38.el9_4.1 | [ALSA-2024:4312](https://errata.almalinux.org/9/ALSA-2024-4312.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6387](https://access.redhat.com/security/cve/CVE-2024-6387))
openssh-askpass-debuginfo | 8.7p1-38.el9_4.1 | |
pam_ssh_agent_auth | 0.10.4-5.38.el9_4.1 | [ALSA-2024:4312](https://errata.almalinux.org/9/ALSA-2024-4312.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6387](https://access.redhat.com/security/cve/CVE-2024-6387))
pam_ssh_agent_auth-debuginfo | 0.10.4-5.38.el9_4.1 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh-sk-dummy | 8.7p1-38.el9_4.1 | |
openssh-sk-dummy-debuginfo | 8.7p1-38.el9_4.1 | |

