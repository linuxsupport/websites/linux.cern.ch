## 2024-09-27

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git-lfs | 3.4.1-4.el9_4 | [ALSA-2024:7136](https://errata.almalinux.org/9/ALSA-2024-7136.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-34156](https://access.redhat.com/security/cve/CVE-2024-34156))
git-lfs-debuginfo | 3.4.1-4.el9_4 | |
git-lfs-debugsource | 3.4.1-4.el9_4 | |
osbuild-composer | 101-2.el9.alma.2 | |
osbuild-composer-core | 101-2.el9.alma.2 | |
osbuild-composer-core-debuginfo | 101-2.el9.alma.2 | |
osbuild-composer-debuginfo | 101-2.el9.alma.2 | |
osbuild-composer-debugsource | 101-2.el9.alma.2 | |
osbuild-composer-worker | 101-2.el9.alma.2 | |
osbuild-composer-worker-debuginfo | 101-2.el9.alma.2 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
osbuild-composer-tests | 101-2.el9.alma.2 | |
osbuild-composer-tests-debuginfo | 101-2.el9.alma.2 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git-lfs | 3.4.1-4.el9_4 | [ALSA-2024:7136](https://errata.almalinux.org/9/ALSA-2024-7136.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-34156](https://access.redhat.com/security/cve/CVE-2024-34156))
git-lfs-debuginfo | 3.4.1-4.el9_4 | |
git-lfs-debugsource | 3.4.1-4.el9_4 | |
osbuild-composer | 101-2.el9.alma.2 | |
osbuild-composer-core | 101-2.el9.alma.2 | |
osbuild-composer-core-debuginfo | 101-2.el9.alma.2 | |
osbuild-composer-debuginfo | 101-2.el9.alma.2 | |
osbuild-composer-debugsource | 101-2.el9.alma.2 | |
osbuild-composer-worker | 101-2.el9.alma.2 | |
osbuild-composer-worker-debuginfo | 101-2.el9.alma.2 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
osbuild-composer-tests | 101-2.el9.alma.2 | |
osbuild-composer-tests-debuginfo | 101-2.el9.alma.2 | |

