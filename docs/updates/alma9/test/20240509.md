## 2024-05-09

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git-lfs | 3.4.1-2.el9_4 | |
git-lfs-debuginfo | 3.4.1-2.el9_4 | |
git-lfs-debugsource | 3.4.1-2.el9_4 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git-lfs | 3.4.1-2.el9_4 | |
git-lfs-debuginfo | 3.4.1-2.el9_4 | |
git-lfs-debugsource | 3.4.1-2.el9_4 | |

