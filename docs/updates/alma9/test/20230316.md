## 2023-03-16

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.15-1.el9_1 | |
aspnetcore-runtime-7.0 | 7.0.4-1.el9_1 | |
aspnetcore-targeting-pack-6.0 | 6.0.15-1.el9_1 | |
aspnetcore-targeting-pack-7.0 | 7.0.4-1.el9_1 | |
dotnet-apphost-pack-6.0 | 6.0.15-1.el9_1 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-apphost-pack-7.0 | 7.0.4-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-host | 7.0.4-1.el9_1 | |
dotnet-host-debuginfo | 7.0.4-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.15-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-hostfxr-7.0 | 7.0.4-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.15-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-runtime-7.0 | 7.0.4-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.115-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet-sdk-7.0 | 7.0.104-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.15-1.el9_1 | |
dotnet-targeting-pack-7.0 | 7.0.4-1.el9_1 | |
dotnet-templates-6.0 | 6.0.115-1.el9_1 | |
dotnet-templates-7.0 | 7.0.104-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet6.0-debugsource | 6.0.115-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet7.0-debugsource | 7.0.104-1.el9_1 | |
netstandard-targeting-pack-2.1 | 7.0.104-1.el9_1 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-host-debuginfo | 7.0.4-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.115-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.104-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet6.0-debugsource | 6.0.115-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet7.0-debugsource | 7.0.104-1.el9_1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.15-1.el9_1 | |
aspnetcore-runtime-7.0 | 7.0.4-1.el9_1 | |
aspnetcore-targeting-pack-6.0 | 6.0.15-1.el9_1 | |
aspnetcore-targeting-pack-7.0 | 7.0.4-1.el9_1 | |
dotnet-apphost-pack-6.0 | 6.0.15-1.el9_1 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-apphost-pack-7.0 | 7.0.4-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-host | 7.0.4-1.el9_1 | |
dotnet-host-debuginfo | 7.0.4-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.15-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-hostfxr-7.0 | 7.0.4-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.15-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-runtime-7.0 | 7.0.4-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.115-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet-sdk-7.0 | 7.0.104-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.15-1.el9_1 | |
dotnet-targeting-pack-7.0 | 7.0.4-1.el9_1 | |
dotnet-templates-6.0 | 6.0.115-1.el9_1 | |
dotnet-templates-7.0 | 7.0.104-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet6.0-debugsource | 6.0.115-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet7.0-debugsource | 7.0.104-1.el9_1 | |
netstandard-targeting-pack-2.1 | 7.0.104-1.el9_1 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-host-debuginfo | 7.0.4-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.115-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.104-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet6.0-debugsource | 6.0.115-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet7.0-debugsource | 7.0.104-1.el9_1 | |

