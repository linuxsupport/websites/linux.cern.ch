## 2023-04-06

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 1.1-1.al9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 1.1-1.al9.cern | |

