## 2023-05-05

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.16.4-103.el9_1 | |
libsmbclient | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
libsmbclient-debuginfo | 4.16.4-103.el9_1 | |
libwbclient | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
libwbclient-debuginfo | 4.16.4-103.el9_1 | |
python3-samba | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
python3-samba-debuginfo | 4.16.4-103.el9_1 | |
samba | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-client-debuginfo | 4.16.4-103.el9_1 | |
samba-client-libs | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-client-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-common | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-common-libs | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-common-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-common-tools | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-common-tools-debuginfo | 4.16.4-103.el9_1 | |
samba-debuginfo | 4.16.4-103.el9_1 | |
samba-debugsource | 4.16.4-103.el9_1 | |
samba-krb5-printing-debuginfo | 4.16.4-103.el9_1 | |
samba-libs | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-test-debuginfo | 4.16.4-103.el9_1 | |
samba-test-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-vfs-iouring-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-winbind-clients-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-krb5-locator-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-modules | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-winbind-modules-debuginfo | 4.16.4-103.el9_1 | |
samba-winexe-debuginfo | 4.16.4-103.el9_1 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.16.4-103.el9_1 | |
libsmbclient-debuginfo | 4.16.4-103.el9_1 | |
libwbclient-debuginfo | 4.16.4-103.el9_1 | |
python3-samba-debuginfo | 4.16.4-103.el9_1 | |
samba-client | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-client-debuginfo | 4.16.4-103.el9_1 | |
samba-client-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-common-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-common-tools-debuginfo | 4.16.4-103.el9_1 | |
samba-debuginfo | 4.16.4-103.el9_1 | |
samba-debugsource | 4.16.4-103.el9_1 | |
samba-krb5-printing | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-krb5-printing-debuginfo | 4.16.4-103.el9_1 | |
samba-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-test-debuginfo | 4.16.4-103.el9_1 | |
samba-test-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-vfs-iouring | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-vfs-iouring-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-clients | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-winbind-clients-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-krb5-locator | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-winbind-krb5-locator-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-modules-debuginfo | 4.16.4-103.el9_1 | |
samba-winexe | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-winexe-debuginfo | 4.16.4-103.el9_1 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb | 4.16.4-103.el9_1 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.16.4-103.el9_1 | |
libsmbclient-debuginfo | 4.16.4-103.el9_1 | |
libsmbclient-devel | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
libwbclient-debuginfo | 4.16.4-103.el9_1 | |
libwbclient-devel | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
python3-samba-debuginfo | 4.16.4-103.el9_1 | |
samba-client-debuginfo | 4.16.4-103.el9_1 | |
samba-client-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-common-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-common-tools-debuginfo | 4.16.4-103.el9_1 | |
samba-debuginfo | 4.16.4-103.el9_1 | |
samba-debugsource | 4.16.4-103.el9_1 | |
samba-devel | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-krb5-printing-debuginfo | 4.16.4-103.el9_1 | |
samba-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-pidl | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-test | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-test-debuginfo | 4.16.4-103.el9_1 | |
samba-test-libs | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-test-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-vfs-iouring-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-clients-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-krb5-locator-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-modules-debuginfo | 4.16.4-103.el9_1 | |
samba-winexe-debuginfo | 4.16.4-103.el9_1 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-samba-devel | 4.16.4-103.el9_1 | |
python3-samba-test | 4.16.4-103.el9_1 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.16.4-103.el9_1 | |
libsmbclient | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
libsmbclient-debuginfo | 4.16.4-103.el9_1 | |
libwbclient | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
libwbclient-debuginfo | 4.16.4-103.el9_1 | |
python3-samba | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
python3-samba-debuginfo | 4.16.4-103.el9_1 | |
samba | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-client-debuginfo | 4.16.4-103.el9_1 | |
samba-client-libs | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-client-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-common | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-common-libs | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-common-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-common-tools | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-common-tools-debuginfo | 4.16.4-103.el9_1 | |
samba-debuginfo | 4.16.4-103.el9_1 | |
samba-debugsource | 4.16.4-103.el9_1 | |
samba-krb5-printing-debuginfo | 4.16.4-103.el9_1 | |
samba-libs | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-test-debuginfo | 4.16.4-103.el9_1 | |
samba-test-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-vfs-iouring-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-winbind-clients-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-krb5-locator-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-modules | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-winbind-modules-debuginfo | 4.16.4-103.el9_1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.16.4-103.el9_1 | |
libsmbclient-debuginfo | 4.16.4-103.el9_1 | |
libwbclient-debuginfo | 4.16.4-103.el9_1 | |
python3-samba-debuginfo | 4.16.4-103.el9_1 | |
samba-client | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-client-debuginfo | 4.16.4-103.el9_1 | |
samba-client-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-common-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-common-tools-debuginfo | 4.16.4-103.el9_1 | |
samba-debuginfo | 4.16.4-103.el9_1 | |
samba-debugsource | 4.16.4-103.el9_1 | |
samba-krb5-printing | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-krb5-printing-debuginfo | 4.16.4-103.el9_1 | |
samba-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-test-debuginfo | 4.16.4-103.el9_1 | |
samba-test-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-vfs-iouring | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-vfs-iouring-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-clients | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-winbind-clients-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-krb5-locator | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-winbind-krb5-locator-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-modules-debuginfo | 4.16.4-103.el9_1 | |

### ResilientStorage aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb | 4.16.4-103.el9_1 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.16.4-103.el9_1 | |
libsmbclient-debuginfo | 4.16.4-103.el9_1 | |
libsmbclient-devel | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
libwbclient-debuginfo | 4.16.4-103.el9_1 | |
libwbclient-devel | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
python3-samba-debuginfo | 4.16.4-103.el9_1 | |
samba-client-debuginfo | 4.16.4-103.el9_1 | |
samba-client-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-common-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-common-tools-debuginfo | 4.16.4-103.el9_1 | |
samba-debuginfo | 4.16.4-103.el9_1 | |
samba-debugsource | 4.16.4-103.el9_1 | |
samba-devel | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-krb5-printing-debuginfo | 4.16.4-103.el9_1 | |
samba-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-pidl | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-test | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-test-debuginfo | 4.16.4-103.el9_1 | |
samba-test-libs | 4.16.4-103.el9_1 | [RHSA-2023:2127](https://access.redhat.com/errata/RHSA-2023:2127) | <div class="adv_s">Security Advisory</div> ([CVE-2022-38023](https://access.redhat.com/security/cve/CVE-2022-38023))
samba-test-libs-debuginfo | 4.16.4-103.el9_1 | |
samba-vfs-iouring-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-clients-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-krb5-locator-debuginfo | 4.16.4-103.el9_1 | |
samba-winbind-modules-debuginfo | 4.16.4-103.el9_1 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-samba-devel | 4.16.4-103.el9_1 | |
python3-samba-test | 4.16.4-103.el9_1 | |

