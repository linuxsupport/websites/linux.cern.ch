## 2024-08-15

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.33-1.el9_4 | |
aspnetcore-runtime-8.0 | 8.0.8-1.el9_4 | |
aspnetcore-runtime-dbg-8.0 | 8.0.8-1.el9_4 | |
aspnetcore-targeting-pack-6.0 | 6.0.33-1.el9_4 | |
aspnetcore-targeting-pack-8.0 | 8.0.8-1.el9_4 | |
dotnet-apphost-pack-6.0 | 6.0.33-1.el9_4 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.33-1.el9_4 | |
dotnet-apphost-pack-8.0 | 8.0.8-1.el9_4 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.8-1.el9_4 | |
dotnet-host | 8.0.8-1.el9_4 | |
dotnet-host-debuginfo | 8.0.8-1.el9_4 | |
dotnet-hostfxr-6.0 | 6.0.33-1.el9_4 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.33-1.el9_4 | |
dotnet-hostfxr-8.0 | 8.0.8-1.el9_4 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.8-1.el9_4 | |
dotnet-runtime-6.0 | 6.0.33-1.el9_4 | |
dotnet-runtime-6.0-debuginfo | 6.0.33-1.el9_4 | |
dotnet-runtime-8.0 | 8.0.8-1.el9_4 | |
dotnet-runtime-8.0-debuginfo | 8.0.8-1.el9_4 | |
dotnet-runtime-dbg-8.0 | 8.0.8-1.el9_4 | |
dotnet-sdk-6.0 | 6.0.133-1.el9_4 | |
dotnet-sdk-6.0-debuginfo | 6.0.133-1.el9_4 | |
dotnet-sdk-8.0 | 8.0.108-1.el9_4 | |
dotnet-sdk-8.0-debuginfo | 8.0.108-1.el9_4 | |
dotnet-sdk-dbg-8.0 | 8.0.108-1.el9_4 | |
dotnet-targeting-pack-6.0 | 6.0.33-1.el9_4 | |
dotnet-targeting-pack-8.0 | 8.0.8-1.el9_4 | |
dotnet-templates-6.0 | 6.0.133-1.el9_4 | |
dotnet-templates-8.0 | 8.0.108-1.el9_4 | |
dotnet6.0-debuginfo | 6.0.133-1.el9_4 | |
dotnet6.0-debugsource | 6.0.133-1.el9_4 | |
dotnet8.0-debuginfo | 8.0.108-1.el9_4 | |
dotnet8.0-debugsource | 8.0.108-1.el9_4 | |
firefox | 115.14.0-2.el9_4.alma.1 | |
firefox-debuginfo | 115.14.0-2.el9_4.alma.1 | |
firefox-debugsource | 115.14.0-2.el9_4.alma.1 | |
firefox-x11 | 115.14.0-2.el9_4.alma.1 | |
netstandard-targeting-pack-2.1 | 8.0.108-1.el9_4 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.133-1.el9_4 | |
dotnet-sdk-8.0-source-built-artifacts | 8.0.108-1.el9_4 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.33-1.el9_4 | |
aspnetcore-runtime-8.0 | 8.0.8-1.el9_4 | |
aspnetcore-runtime-dbg-8.0 | 8.0.8-1.el9_4 | |
aspnetcore-targeting-pack-6.0 | 6.0.33-1.el9_4 | |
aspnetcore-targeting-pack-8.0 | 8.0.8-1.el9_4 | |
dotnet-apphost-pack-6.0 | 6.0.33-1.el9_4 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.33-1.el9_4 | |
dotnet-apphost-pack-8.0 | 8.0.8-1.el9_4 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.8-1.el9_4 | |
dotnet-host | 8.0.8-1.el9_4 | |
dotnet-host-debuginfo | 8.0.8-1.el9_4 | |
dotnet-hostfxr-6.0 | 6.0.33-1.el9_4 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.33-1.el9_4 | |
dotnet-hostfxr-8.0 | 8.0.8-1.el9_4 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.8-1.el9_4 | |
dotnet-runtime-6.0 | 6.0.33-1.el9_4 | |
dotnet-runtime-6.0-debuginfo | 6.0.33-1.el9_4 | |
dotnet-runtime-8.0 | 8.0.8-1.el9_4 | |
dotnet-runtime-8.0-debuginfo | 8.0.8-1.el9_4 | |
dotnet-runtime-dbg-8.0 | 8.0.8-1.el9_4 | |
dotnet-sdk-6.0 | 6.0.133-1.el9_4 | |
dotnet-sdk-6.0-debuginfo | 6.0.133-1.el9_4 | |
dotnet-sdk-8.0 | 8.0.108-1.el9_4 | |
dotnet-sdk-8.0-debuginfo | 8.0.108-1.el9_4 | |
dotnet-sdk-dbg-8.0 | 8.0.108-1.el9_4 | |
dotnet-targeting-pack-6.0 | 6.0.33-1.el9_4 | |
dotnet-targeting-pack-8.0 | 8.0.8-1.el9_4 | |
dotnet-templates-6.0 | 6.0.133-1.el9_4 | |
dotnet-templates-8.0 | 8.0.108-1.el9_4 | |
dotnet6.0-debuginfo | 6.0.133-1.el9_4 | |
dotnet6.0-debugsource | 6.0.133-1.el9_4 | |
dotnet8.0-debuginfo | 8.0.108-1.el9_4 | |
dotnet8.0-debugsource | 8.0.108-1.el9_4 | |
firefox | 115.14.0-2.el9_4.alma.1 | |
firefox-debuginfo | 115.14.0-2.el9_4.alma.1 | |
firefox-debugsource | 115.14.0-2.el9_4.alma.1 | |
firefox-x11 | 115.14.0-2.el9_4.alma.1 | |
netstandard-targeting-pack-2.1 | 8.0.108-1.el9_4 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.133-1.el9_4 | |
dotnet-sdk-8.0-source-built-artifacts | 8.0.108-1.el9_4 | |

