## 2024-06-26

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3 | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-libs | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3.9-debuginfo | 3.9.18-3.el9_4.1 | |
python3.9-debugsource | 3.9.18-3.el9_4.1 | |
sos | 4.7.1-3.el9.alma.1 | |
sos-audit | 4.7.1-3.el9.alma.1 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-all | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-core | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-core-debuginfo | 2.43.5-1.el9_4 | |
git-core-doc | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-credential-libsecret | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-credential-libsecret-debuginfo | 2.43.5-1.el9_4 | |
git-daemon | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-daemon-debuginfo | 2.43.5-1.el9_4 | |
git-debuginfo | 2.43.5-1.el9_4 | |
git-debugsource | 2.43.5-1.el9_4 | |
git-email | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-gui | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-instaweb | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-subtree | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-svn | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
gitk | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
gitweb | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
mesa-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-debugsource | 23.3.3-1.el9.alma.1 | |
mesa-dri-drivers | 23.3.3-1.el9.alma.1 | |
mesa-dri-drivers-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-filesystem | 23.3.3-1.el9.alma.1 | |
mesa-libEGL | 23.3.3-1.el9.alma.1 | |
mesa-libEGL-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-libEGL-devel | 23.3.3-1.el9.alma.1 | |
mesa-libgbm | 23.3.3-1.el9.alma.1 | |
mesa-libgbm-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-libgbm-devel | 23.3.3-1.el9.alma.1 | |
mesa-libGL | 23.3.3-1.el9.alma.1 | |
mesa-libGL-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-libGL-devel | 23.3.3-1.el9.alma.1 | |
mesa-libglapi | 23.3.3-1.el9.alma.1 | |
mesa-libglapi-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-libOSMesa-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-libxatracker | 23.3.3-1.el9.alma.1 | |
mesa-libxatracker-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-vdpau-drivers-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-vulkan-drivers | 23.3.3-1.el9.alma.1 | |
mesa-vulkan-drivers-debuginfo | 23.3.3-1.el9.alma.1 | |
perl-Git | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
perl-Git-SVN | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
python-unversioned-command | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-devel | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-tkinter | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3.11 | 3.11.7-1.el9_4.1 | [ALSA-2024:4077](https://errata.almalinux.org/9/ALSA-2024-4077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-debuginfo | 3.11.7-1.el9_4.1 | |
python3.11-debugsource | 3.11.7-1.el9_4.1 | |
python3.11-devel | 3.11.7-1.el9_4.1 | [ALSA-2024:4077](https://errata.almalinux.org/9/ALSA-2024-4077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-libs | 3.11.7-1.el9_4.1 | [ALSA-2024:4077](https://errata.almalinux.org/9/ALSA-2024-4077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-tkinter | 3.11.7-1.el9_4.1 | [ALSA-2024:4077](https://errata.almalinux.org/9/ALSA-2024-4077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
mesa-libOSMesa | 23.3.3-1.el9.alma.1 | |
mesa-libOSMesa-devel | 23.3.3-1.el9.alma.1 | |
python3-debug | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-idle | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-test | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3.11-debug | 3.11.7-1.el9_4.1 | [ALSA-2024:4077](https://errata.almalinux.org/9/ALSA-2024-4077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-idle | 3.11.7-1.el9_4.1 | [ALSA-2024:4077](https://errata.almalinux.org/9/ALSA-2024-4077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-test | 3.11.7-1.el9_4.1 | [ALSA-2024:4077](https://errata.almalinux.org/9/ALSA-2024-4077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
mesa-libxatracker-devel | 23.3.3-1.el9.alma.1 | |
mesa-vdpau-drivers | 23.3.3-1.el9.alma.1 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3 | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-libs | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3.9-debuginfo | 3.9.18-3.el9_4.1 | |
python3.9-debugsource | 3.9.18-3.el9_4.1 | |
sos | 4.7.1-3.el9.alma.1 | |
sos-audit | 4.7.1-3.el9.alma.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-all | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-core | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-core-debuginfo | 2.43.5-1.el9_4 | |
git-core-doc | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-credential-libsecret | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-credential-libsecret-debuginfo | 2.43.5-1.el9_4 | |
git-daemon | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-daemon-debuginfo | 2.43.5-1.el9_4 | |
git-debuginfo | 2.43.5-1.el9_4 | |
git-debugsource | 2.43.5-1.el9_4 | |
git-email | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-gui | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-instaweb | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-subtree | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-svn | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
gitk | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
gitweb | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
mesa-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-debugsource | 23.3.3-1.el9.alma.1 | |
mesa-dri-drivers | 23.3.3-1.el9.alma.1 | |
mesa-dri-drivers-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-filesystem | 23.3.3-1.el9.alma.1 | |
mesa-libEGL | 23.3.3-1.el9.alma.1 | |
mesa-libEGL-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-libEGL-devel | 23.3.3-1.el9.alma.1 | |
mesa-libgbm | 23.3.3-1.el9.alma.1 | |
mesa-libgbm-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-libgbm-devel | 23.3.3-1.el9.alma.1 | |
mesa-libGL | 23.3.3-1.el9.alma.1 | |
mesa-libGL-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-libGL-devel | 23.3.3-1.el9.alma.1 | |
mesa-libglapi | 23.3.3-1.el9.alma.1 | |
mesa-libglapi-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-libOSMesa-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-libxatracker-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-vdpau-drivers-debuginfo | 23.3.3-1.el9.alma.1 | |
mesa-vulkan-drivers-debuginfo | 23.3.3-1.el9.alma.1 | |
perl-Git | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
perl-Git-SVN | 2.43.5-1.el9_4 | [ALSA-2024:4083](https://errata.almalinux.org/9/ALSA-2024-4083.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
python-unversioned-command | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-devel | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-tkinter | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3.11 | 3.11.7-1.el9_4.1 | [ALSA-2024:4077](https://errata.almalinux.org/9/ALSA-2024-4077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-debuginfo | 3.11.7-1.el9_4.1 | |
python3.11-debugsource | 3.11.7-1.el9_4.1 | |
python3.11-devel | 3.11.7-1.el9_4.1 | [ALSA-2024:4077](https://errata.almalinux.org/9/ALSA-2024-4077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-libs | 3.11.7-1.el9_4.1 | [ALSA-2024:4077](https://errata.almalinux.org/9/ALSA-2024-4077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-tkinter | 3.11.7-1.el9_4.1 | [ALSA-2024:4077](https://errata.almalinux.org/9/ALSA-2024-4077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
mesa-libOSMesa | 23.3.3-1.el9.alma.1 | |
mesa-libOSMesa-devel | 23.3.3-1.el9.alma.1 | |
python3-debug | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-idle | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-test | 3.9.18-3.el9_4.1 | [ALSA-2024:4078](https://errata.almalinux.org/9/ALSA-2024-4078.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3.11-debug | 3.11.7-1.el9_4.1 | [ALSA-2024:4077](https://errata.almalinux.org/9/ALSA-2024-4077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-idle | 3.11.7-1.el9_4.1 | [ALSA-2024:4077](https://errata.almalinux.org/9/ALSA-2024-4077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-test | 3.11.7-1.el9_4.1 | [ALSA-2024:4077](https://errata.almalinux.org/9/ALSA-2024-4077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
mesa-libxatracker | 23.3.3-1.el9.alma.1 | |
mesa-libxatracker-devel | 23.3.3-1.el9.alma.1 | |
mesa-vdpau-drivers | 23.3.3-1.el9.alma.1 | |
mesa-vulkan-drivers | 23.3.3-1.el9.alma.1 | |

