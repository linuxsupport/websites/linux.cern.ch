## 2024-01-31

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.5.14.0_362.18.1.el9_3.al9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.2.0-362.18.1.el9_3 | |
bpftool-debuginfo | 7.2.0-362.18.1.el9_3 | |
gnutls | 3.7.6-23.el9_3.3 | [ALSA-2024:0533](https://errata.almalinux.org/9/ALSA-2024-0533.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981), [CVE-2024-0553](https://access.redhat.com/security/cve/CVE-2024-0553), [CVE-2024-0567](https://access.redhat.com/security/cve/CVE-2024-0567))
gnutls-c++-debuginfo | 3.7.6-23.el9_3.3 | |
gnutls-dane-debuginfo | 3.7.6-23.el9_3.3 | |
gnutls-debuginfo | 3.7.6-23.el9_3.3 | |
gnutls-debugsource | 3.7.6-23.el9_3.3 | |
gnutls-utils-debuginfo | 3.7.6-23.el9_3.3 | |
kernel | 5.14.0-362.18.1.el9_3 | |
kernel-abi-stablelists | 5.14.0-362.18.1.el9_3 | |
kernel-core | 5.14.0-362.18.1.el9_3 | |
kernel-debug | 5.14.0-362.18.1.el9_3 | |
kernel-debug-core | 5.14.0-362.18.1.el9_3 | |
kernel-debug-debuginfo | 5.14.0-362.18.1.el9_3 | |
kernel-debug-modules | 5.14.0-362.18.1.el9_3 | |
kernel-debug-modules-core | 5.14.0-362.18.1.el9_3 | |
kernel-debug-modules-extra | 5.14.0-362.18.1.el9_3 | |
kernel-debug-uki-virt | 5.14.0-362.18.1.el9_3 | |
kernel-debuginfo | 5.14.0-362.18.1.el9_3 | |
kernel-debuginfo-common-x86_64 | 5.14.0-362.18.1.el9_3 | |
kernel-modules | 5.14.0-362.18.1.el9_3 | |
kernel-modules-core | 5.14.0-362.18.1.el9_3 | |
kernel-modules-extra | 5.14.0-362.18.1.el9_3 | |
kernel-tools | 5.14.0-362.18.1.el9_3 | |
kernel-tools-debuginfo | 5.14.0-362.18.1.el9_3 | |
kernel-tools-libs | 5.14.0-362.18.1.el9_3 | |
kernel-uki-virt | 5.14.0-362.18.1.el9_3 | |
python3-perf | 5.14.0-362.18.1.el9_3 | |
python3-perf-debuginfo | 5.14.0-362.18.1.el9_3 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gnutls-c++ | 3.7.6-23.el9_3.3 | [ALSA-2024:0533](https://errata.almalinux.org/9/ALSA-2024-0533.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981), [CVE-2024-0553](https://access.redhat.com/security/cve/CVE-2024-0553), [CVE-2024-0567](https://access.redhat.com/security/cve/CVE-2024-0567))
gnutls-dane | 3.7.6-23.el9_3.3 | [ALSA-2024:0533](https://errata.almalinux.org/9/ALSA-2024-0533.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981), [CVE-2024-0553](https://access.redhat.com/security/cve/CVE-2024-0553), [CVE-2024-0567](https://access.redhat.com/security/cve/CVE-2024-0567))
gnutls-devel | 3.7.6-23.el9_3.3 | [ALSA-2024:0533](https://errata.almalinux.org/9/ALSA-2024-0533.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981), [CVE-2024-0553](https://access.redhat.com/security/cve/CVE-2024-0553), [CVE-2024-0567](https://access.redhat.com/security/cve/CVE-2024-0567))
gnutls-utils | 3.7.6-23.el9_3.3 | [ALSA-2024:0533](https://errata.almalinux.org/9/ALSA-2024-0533.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981), [CVE-2024-0553](https://access.redhat.com/security/cve/CVE-2024-0553), [CVE-2024-0567](https://access.redhat.com/security/cve/CVE-2024-0567))
kernel-debug-devel | 5.14.0-362.18.1.el9_3 | |
kernel-debug-devel-matched | 5.14.0-362.18.1.el9_3 | |
kernel-devel | 5.14.0-362.18.1.el9_3 | |
kernel-devel-matched | 5.14.0-362.18.1.el9_3 | |
kernel-doc | 5.14.0-362.18.1.el9_3 | |
kernel-headers | 5.14.0-362.18.1.el9_3 | |
perf | 5.14.0-362.18.1.el9_3 | |
perf-debuginfo | 5.14.0-362.18.1.el9_3 | |
rtla | 5.14.0-362.18.1.el9_3 | |
rv | 5.14.0-362.18.1.el9_3 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-362.18.1.el9_3 | |
kernel-rt-core | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-core | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-debuginfo | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-devel | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-modules | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-modules-core | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-modules-extra | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debuginfo | 5.14.0-362.18.1.el9_3 | |
kernel-rt-devel | 5.14.0-362.18.1.el9_3 | |
kernel-rt-modules | 5.14.0-362.18.1.el9_3 | |
kernel-rt-modules-core | 5.14.0-362.18.1.el9_3 | |
kernel-rt-modules-extra | 5.14.0-362.18.1.el9_3 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-cross-headers | 5.14.0-362.18.1.el9_3 | |
kernel-tools-libs-devel | 5.14.0-362.18.1.el9_3 | |
libperf | 5.14.0-362.18.1.el9_3 | |
libperf-debuginfo | 5.14.0-362.18.1.el9_3 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-362.18.1.el9_3 | |
kernel-rt-core | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-core | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-debuginfo | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-devel | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-kvm | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-modules | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-modules-core | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-modules-extra | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debuginfo | 5.14.0-362.18.1.el9_3 | |
kernel-rt-devel | 5.14.0-362.18.1.el9_3 | |
kernel-rt-kvm | 5.14.0-362.18.1.el9_3 | |
kernel-rt-modules | 5.14.0-362.18.1.el9_3 | |
kernel-rt-modules-core | 5.14.0-362.18.1.el9_3 | |
kernel-rt-modules-extra | 5.14.0-362.18.1.el9_3 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-debug-modules-internal | 5.14.0-362.18.1.el9_3 | |
kernel-debug-modules-partner | 5.14.0-362.18.1.el9_3 | |
kernel-ipaclones-internal | 5.14.0-362.18.1.el9_3 | |
kernel-modules-internal | 5.14.0-362.18.1.el9_3 | |
kernel-modules-partner | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-devel-matched | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-modules-internal | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-modules-partner | 5.14.0-362.18.1.el9_3 | |
kernel-rt-devel-matched | 5.14.0-362.18.1.el9_3 | |
kernel-rt-modules-internal | 5.14.0-362.18.1.el9_3 | |
kernel-rt-modules-partner | 5.14.0-362.18.1.el9_3 | |
kernel-selftests-internal | 5.14.0-362.18.1.el9_3 | |
libperf-devel | 5.14.0-362.18.1.el9_3 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.5.14.0_362.18.1.el9_3.al9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.2.0-362.18.1.el9_3 | |
bpftool-debuginfo | 7.2.0-362.18.1.el9_3 | |
gnutls | 3.7.6-23.el9_3.3 | [ALSA-2024:0533](https://errata.almalinux.org/9/ALSA-2024-0533.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981), [CVE-2024-0553](https://access.redhat.com/security/cve/CVE-2024-0553), [CVE-2024-0567](https://access.redhat.com/security/cve/CVE-2024-0567))
gnutls-c++-debuginfo | 3.7.6-23.el9_3.3 | |
gnutls-dane-debuginfo | 3.7.6-23.el9_3.3 | |
gnutls-debuginfo | 3.7.6-23.el9_3.3 | |
gnutls-debugsource | 3.7.6-23.el9_3.3 | |
gnutls-utils-debuginfo | 3.7.6-23.el9_3.3 | |
kernel | 5.14.0-362.18.1.el9_3 | |
kernel-64k | 5.14.0-362.18.1.el9_3 | |
kernel-64k-core | 5.14.0-362.18.1.el9_3 | |
kernel-64k-debug | 5.14.0-362.18.1.el9_3 | |
kernel-64k-debug-core | 5.14.0-362.18.1.el9_3 | |
kernel-64k-debug-debuginfo | 5.14.0-362.18.1.el9_3 | |
kernel-64k-debug-modules | 5.14.0-362.18.1.el9_3 | |
kernel-64k-debug-modules-core | 5.14.0-362.18.1.el9_3 | |
kernel-64k-debug-modules-extra | 5.14.0-362.18.1.el9_3 | |
kernel-64k-debuginfo | 5.14.0-362.18.1.el9_3 | |
kernel-64k-modules | 5.14.0-362.18.1.el9_3 | |
kernel-64k-modules-core | 5.14.0-362.18.1.el9_3 | |
kernel-64k-modules-extra | 5.14.0-362.18.1.el9_3 | |
kernel-abi-stablelists | 5.14.0-362.18.1.el9_3 | |
kernel-core | 5.14.0-362.18.1.el9_3 | |
kernel-debug | 5.14.0-362.18.1.el9_3 | |
kernel-debug-core | 5.14.0-362.18.1.el9_3 | |
kernel-debug-debuginfo | 5.14.0-362.18.1.el9_3 | |
kernel-debug-modules | 5.14.0-362.18.1.el9_3 | |
kernel-debug-modules-core | 5.14.0-362.18.1.el9_3 | |
kernel-debug-modules-extra | 5.14.0-362.18.1.el9_3 | |
kernel-debuginfo | 5.14.0-362.18.1.el9_3 | |
kernel-debuginfo-common-aarch64 | 5.14.0-362.18.1.el9_3 | |
kernel-modules | 5.14.0-362.18.1.el9_3 | |
kernel-modules-core | 5.14.0-362.18.1.el9_3 | |
kernel-modules-extra | 5.14.0-362.18.1.el9_3 | |
kernel-tools | 5.14.0-362.18.1.el9_3 | |
kernel-tools-debuginfo | 5.14.0-362.18.1.el9_3 | |
kernel-tools-libs | 5.14.0-362.18.1.el9_3 | |
python3-perf | 5.14.0-362.18.1.el9_3 | |
python3-perf-debuginfo | 5.14.0-362.18.1.el9_3 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gnutls-c++ | 3.7.6-23.el9_3.3 | [ALSA-2024:0533](https://errata.almalinux.org/9/ALSA-2024-0533.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981), [CVE-2024-0553](https://access.redhat.com/security/cve/CVE-2024-0553), [CVE-2024-0567](https://access.redhat.com/security/cve/CVE-2024-0567))
gnutls-dane | 3.7.6-23.el9_3.3 | [ALSA-2024:0533](https://errata.almalinux.org/9/ALSA-2024-0533.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981), [CVE-2024-0553](https://access.redhat.com/security/cve/CVE-2024-0553), [CVE-2024-0567](https://access.redhat.com/security/cve/CVE-2024-0567))
gnutls-devel | 3.7.6-23.el9_3.3 | [ALSA-2024:0533](https://errata.almalinux.org/9/ALSA-2024-0533.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981), [CVE-2024-0553](https://access.redhat.com/security/cve/CVE-2024-0553), [CVE-2024-0567](https://access.redhat.com/security/cve/CVE-2024-0567))
gnutls-utils | 3.7.6-23.el9_3.3 | [ALSA-2024:0533](https://errata.almalinux.org/9/ALSA-2024-0533.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981), [CVE-2024-0553](https://access.redhat.com/security/cve/CVE-2024-0553), [CVE-2024-0567](https://access.redhat.com/security/cve/CVE-2024-0567))
kernel-64k-debug-devel | 5.14.0-362.18.1.el9_3 | |
kernel-64k-debug-devel-matched | 5.14.0-362.18.1.el9_3 | |
kernel-64k-devel | 5.14.0-362.18.1.el9_3 | |
kernel-64k-devel-matched | 5.14.0-362.18.1.el9_3 | |
kernel-debug-devel | 5.14.0-362.18.1.el9_3 | |
kernel-debug-devel-matched | 5.14.0-362.18.1.el9_3 | |
kernel-devel | 5.14.0-362.18.1.el9_3 | |
kernel-devel-matched | 5.14.0-362.18.1.el9_3 | |
kernel-doc | 5.14.0-362.18.1.el9_3 | |
kernel-headers | 5.14.0-362.18.1.el9_3 | |
perf | 5.14.0-362.18.1.el9_3 | |
perf-debuginfo | 5.14.0-362.18.1.el9_3 | |
rtla | 5.14.0-362.18.1.el9_3 | |
rv | 5.14.0-362.18.1.el9_3 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-cross-headers | 5.14.0-362.18.1.el9_3 | |
kernel-tools-libs-devel | 5.14.0-362.18.1.el9_3 | |
libperf | 5.14.0-362.18.1.el9_3 | |
libperf-debuginfo | 5.14.0-362.18.1.el9_3 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-64k-debug-modules-internal | 5.14.0-362.18.1.el9_3 | |
kernel-64k-debug-modules-partner | 5.14.0-362.18.1.el9_3 | |
kernel-64k-modules-internal | 5.14.0-362.18.1.el9_3 | |
kernel-64k-modules-partner | 5.14.0-362.18.1.el9_3 | |
kernel-debug-modules-internal | 5.14.0-362.18.1.el9_3 | |
kernel-debug-modules-partner | 5.14.0-362.18.1.el9_3 | |
kernel-modules-internal | 5.14.0-362.18.1.el9_3 | |
kernel-modules-partner | 5.14.0-362.18.1.el9_3 | |
kernel-rt | 5.14.0-362.18.1.el9_3 | |
kernel-rt-core | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-core | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-debuginfo | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-devel | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-devel-matched | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-kvm | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-modules | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-modules-core | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-modules-extra | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-modules-internal | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debug-modules-partner | 5.14.0-362.18.1.el9_3 | |
kernel-rt-debuginfo | 5.14.0-362.18.1.el9_3 | |
kernel-rt-devel | 5.14.0-362.18.1.el9_3 | |
kernel-rt-devel-matched | 5.14.0-362.18.1.el9_3 | |
kernel-rt-kvm | 5.14.0-362.18.1.el9_3 | |
kernel-rt-modules | 5.14.0-362.18.1.el9_3 | |
kernel-rt-modules-core | 5.14.0-362.18.1.el9_3 | |
kernel-rt-modules-extra | 5.14.0-362.18.1.el9_3 | |
kernel-rt-modules-internal | 5.14.0-362.18.1.el9_3 | |
kernel-rt-modules-partner | 5.14.0-362.18.1.el9_3 | |
kernel-selftests-internal | 5.14.0-362.18.1.el9_3 | |
libperf-devel | 5.14.0-362.18.1.el9_3 | |

