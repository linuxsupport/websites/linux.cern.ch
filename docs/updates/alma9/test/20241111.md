## 2024-11-11

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-aarch64 | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-ovmf | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-aarch64 | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-debugsource | 20231122-6.el9_4.4 | |
edk2-ovmf | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-tools | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-tools-debuginfo | 20231122-6.el9_4.4 | |
edk2-tools-doc | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-aarch64 | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-ovmf | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-aarch64 | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-debugsource | 20231122-6.el9_4.4 | |
edk2-ovmf | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-tools | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-tools-debuginfo | 20231122-6.el9_4.4 | |
edk2-tools-doc | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))

