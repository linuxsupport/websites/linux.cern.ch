## 2023-08-29

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iwl100-firmware | 39.31.5.1-134.el9_2.alma.1 | |
iwl1000-firmware | 39.31.5.1-134.el9_2.alma.1 | |
iwl105-firmware | 18.168.6.1-134.el9_2.alma.1 | |
iwl135-firmware | 18.168.6.1-134.el9_2.alma.1 | |
iwl2000-firmware | 18.168.6.1-134.el9_2.alma.1 | |
iwl2030-firmware | 18.168.6.1-134.el9_2.alma.1 | |
iwl3160-firmware | 25.30.13.0-134.el9_2.alma.1 | |
iwl5000-firmware | 8.83.5.1_1-134.el9_2.alma.1 | |
iwl5150-firmware | 8.24.2.2-134.el9_2.alma.1 | |
iwl6000g2a-firmware | 18.168.6.1-134.el9_2.alma.1 | |
iwl6000g2b-firmware | 18.168.6.1-134.el9_2.alma.1 | |
iwl6050-firmware | 41.28.5.1-134.el9_2.alma.1 | |
iwl7260-firmware | 25.30.13.0-134.el9_2.alma.1 | |
libertas-sd8787-firmware | 20230310-134.el9_2.alma.1 | |
linux-firmware | 20230310-134.el9_2.alma.1 | |
linux-firmware-whence | 20230310-134.el9_2.alma.1 | |
microcode_ctl | 20220809-2.20230808.1.el9_2.alma | |
netronome-firmware | 20230310-134.el9_2.alma.1 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iwl3945-firmware | 15.32.2.9-134.el9_2.alma.1 | |
iwl4965-firmware | 228.61.2.24-134.el9_2.alma.1 | |
iwl6000-firmware | 9.221.4.1-134.el9_2.alma.1 | |
libertas-sd8686-firmware | 20230310-134.el9_2.alma.1 | |
libertas-usb8388-firmware | 20230310-134.el9_2.alma.1 | |
libertas-usb8388-olpc-firmware | 20230310-134.el9_2.alma.1 | |
liquidio-firmware | 20230310-134.el9_2.alma.1 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iwl100-firmware | 39.31.5.1-134.el9_2.alma.1 | |
iwl1000-firmware | 39.31.5.1-134.el9_2.alma.1 | |
iwl105-firmware | 18.168.6.1-134.el9_2.alma.1 | |
iwl135-firmware | 18.168.6.1-134.el9_2.alma.1 | |
iwl2000-firmware | 18.168.6.1-134.el9_2.alma.1 | |
iwl2030-firmware | 18.168.6.1-134.el9_2.alma.1 | |
iwl3160-firmware | 25.30.13.0-134.el9_2.alma.1 | |
iwl5000-firmware | 8.83.5.1_1-134.el9_2.alma.1 | |
iwl5150-firmware | 8.24.2.2-134.el9_2.alma.1 | |
iwl6000g2a-firmware | 18.168.6.1-134.el9_2.alma.1 | |
iwl6000g2b-firmware | 18.168.6.1-134.el9_2.alma.1 | |
iwl6050-firmware | 41.28.5.1-134.el9_2.alma.1 | |
iwl7260-firmware | 25.30.13.0-134.el9_2.alma.1 | |
libertas-sd8787-firmware | 20230310-134.el9_2.alma.1 | |
linux-firmware | 20230310-134.el9_2.alma.1 | |
linux-firmware-whence | 20230310-134.el9_2.alma.1 | |
microcode_ctl | 20220809-2.20230808.1.el9_2.alma | |
netronome-firmware | 20230310-134.el9_2.alma.1 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iwl3945-firmware | 15.32.2.9-134.el9_2.alma.1 | |
iwl4965-firmware | 228.61.2.24-134.el9_2.alma.1 | |
iwl6000-firmware | 9.221.4.1-134.el9_2.alma.1 | |
libertas-sd8686-firmware | 20230310-134.el9_2.alma.1 | |
libertas-usb8388-firmware | 20230310-134.el9_2.alma.1 | |
libertas-usb8388-olpc-firmware | 20230310-134.el9_2.alma.1 | |
liquidio-firmware | 20230310-134.el9_2.alma.1 | |

