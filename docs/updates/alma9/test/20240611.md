## 2024-06-11

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ruby | 3.3.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
ruby-bundled-gems | 3.3.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
ruby-bundled-gems-debuginfo | 3.3.1-2.module_el9.4.0+102+68a93853 | |
ruby-debuginfo | 3.3.1-2.module_el9.4.0+102+68a93853 | |
ruby-debugsource | 3.3.1-2.module_el9.4.0+102+68a93853 | |
ruby-default-gems | 3.3.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
ruby-devel | 3.3.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
ruby-doc | 3.3.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
ruby-libs | 3.3.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
ruby-libs-debuginfo | 3.3.1-2.module_el9.4.0+102+68a93853 | |
rubygem-bigdecimal | 3.1.5-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-bigdecimal-debuginfo | 3.1.5-2.module_el9.4.0+102+68a93853 | |
rubygem-bundler | 2.5.9-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-io-console | 0.7.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-io-console-debuginfo | 0.7.1-2.module_el9.4.0+102+68a93853 | |
rubygem-irb | 1.11.0-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-json | 2.7.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-json-debuginfo | 2.7.1-2.module_el9.4.0+102+68a93853 | |
rubygem-minitest | 5.20.0-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-power_assert | 2.0.3-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-psych | 5.1.2-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-psych-debuginfo | 5.1.2-2.module_el9.4.0+102+68a93853 | |
rubygem-racc | 1.7.3-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-racc-debuginfo | 1.7.3-2.module_el9.4.0+102+68a93853 | |
rubygem-rake | 13.1.0-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-rbs | 3.4.0-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-rbs-debuginfo | 3.4.0-2.module_el9.4.0+102+68a93853 | |
rubygem-rdoc | 6.6.3.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-rexml | 3.2.6-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-rss | 0.3.0-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-test-unit | 3.6.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-typeprof | 0.21.9-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygems | 3.5.9-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygems-devel | 3.5.9-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ceph-reef | 1.0-1.el9 | |
centos-release-nfs-ganesha5 | 1.0-1.el9 | |
centos-release-okd-4.13 | 1-1.el9 | |
centos-release-okd-4.14 | 1-1.el9 | |
centos-release-okd-4.15 | 1-1.el9 | |
centos-release-okd-4.16 | 1-1.el9 | |
centos-release-openstack-antelope | 1-4.el9 | |
centos-release-openstack-bobcat | 1-1.el9 | |
centos-release-openstack-caracal | 1-1.el9 | |
centos-release-openstack-zed | 1-4.el9 | |
centos-release-samba418 | 1.0-1.el9 | |
centos-release-samba419 | 1.0-1.el9 | |
centos-release-samba420 | 1.0-1.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ruby | 3.3.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
ruby-bundled-gems | 3.3.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
ruby-bundled-gems-debuginfo | 3.3.1-2.module_el9.4.0+102+68a93853 | |
ruby-debuginfo | 3.3.1-2.module_el9.4.0+102+68a93853 | |
ruby-debugsource | 3.3.1-2.module_el9.4.0+102+68a93853 | |
ruby-default-gems | 3.3.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
ruby-devel | 3.3.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
ruby-doc | 3.3.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
ruby-libs | 3.3.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
ruby-libs-debuginfo | 3.3.1-2.module_el9.4.0+102+68a93853 | |
rubygem-bigdecimal | 3.1.5-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-bigdecimal-debuginfo | 3.1.5-2.module_el9.4.0+102+68a93853 | |
rubygem-bundler | 2.5.9-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-io-console | 0.7.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-io-console-debuginfo | 0.7.1-2.module_el9.4.0+102+68a93853 | |
rubygem-irb | 1.11.0-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-json | 2.7.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-json-debuginfo | 2.7.1-2.module_el9.4.0+102+68a93853 | |
rubygem-minitest | 5.20.0-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-power_assert | 2.0.3-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-psych | 5.1.2-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-psych-debuginfo | 5.1.2-2.module_el9.4.0+102+68a93853 | |
rubygem-racc | 1.7.3-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-racc-debuginfo | 1.7.3-2.module_el9.4.0+102+68a93853 | |
rubygem-rake | 13.1.0-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-rbs | 3.4.0-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-rbs-debuginfo | 3.4.0-2.module_el9.4.0+102+68a93853 | |
rubygem-rdoc | 6.6.3.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-rexml | 3.2.6-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-rss | 0.3.0-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-test-unit | 3.6.1-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygem-typeprof | 0.21.9-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygems | 3.5.9-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))
rubygems-devel | 3.5.9-2.module_el9.4.0+102+68a93853 | [ALSA-2024:3671](https://errata.almalinux.org/9/ALSA-2024-3671.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27280](https://access.redhat.com/security/cve/CVE-2024-27280), [CVE-2024-27281](https://access.redhat.com/security/cve/CVE-2024-27281), [CVE-2024-27282](https://access.redhat.com/security/cve/CVE-2024-27282))

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ceph-reef | 1.0-1.el9 | |
centos-release-nfs-ganesha5 | 1.0-1.el9 | |
centos-release-okd-4.13 | 1-1.el9 | |
centos-release-okd-4.14 | 1-1.el9 | |
centos-release-okd-4.15 | 1-1.el9 | |
centos-release-okd-4.16 | 1-1.el9 | |
centos-release-openstack-antelope | 1-4.el9 | |
centos-release-openstack-bobcat | 1-1.el9 | |
centos-release-openstack-caracal | 1-1.el9 | |
centos-release-openstack-zed | 1-4.el9 | |
centos-release-samba418 | 1.0-1.el9 | |
centos-release-samba419 | 1.0-1.el9 | |
centos-release-samba420 | 1.0-1.el9 | |

