## 2023-10-31

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.12-1.al9.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libguestfs-winsupport | 9.2-2.el9_2 | |
nginx | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-all-modules | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-core | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-core-debuginfo | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
nginx-debuginfo | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
nginx-debugsource | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
nginx-filesystem | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-devel | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-image-filter | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-image-filter-debuginfo | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
nginx-mod-http-perl | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-perl-debuginfo | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
nginx-mod-http-xslt-filter | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-xslt-filter-debuginfo | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
nginx-mod-mail | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-mail-debuginfo | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
nginx-mod-stream | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-stream-debuginfo | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
toolbox | 0.0.99.3-10.el9_2 | [ALSA-2023:6077](https://errata.almalinux.org/9/ALSA-2023-6077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
toolbox-debuginfo | 0.0.99.3-10.el9_2 | |
toolbox-debugsource | 0.0.99.3-10.el9_2 | |
toolbox-tests | 0.0.99.3-10.el9_2 | [ALSA-2023:6077](https://errata.almalinux.org/9/ALSA-2023-6077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.12-1.al9.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libguestfs-winsupport | 9.2-2.el9_2 | |
nginx | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-all-modules | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-core | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-core-debuginfo | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
nginx-debuginfo | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
nginx-debugsource | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
nginx-filesystem | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-devel | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-image-filter | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-image-filter-debuginfo | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
nginx-mod-http-perl | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-perl-debuginfo | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
nginx-mod-http-xslt-filter | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-xslt-filter-debuginfo | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
nginx-mod-mail | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-mail-debuginfo | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
nginx-mod-stream | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | [ALSA-2023:6120](https://errata.almalinux.org/9/ALSA-2023-6120.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-stream-debuginfo | 1.22.1-3.module_el9.2.0+44+f932b372.1.alma.1 | |
toolbox | 0.0.99.3-10.el9_2 | [ALSA-2023:6077](https://errata.almalinux.org/9/ALSA-2023-6077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
toolbox-debuginfo | 0.0.99.3-10.el9_2 | |
toolbox-debugsource | 0.0.99.3-10.el9_2 | |
toolbox-tests | 0.0.99.3-10.el9_2 | [ALSA-2023:6077](https://errata.almalinux.org/9/ALSA-2023-6077.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))

