## 2025-03-14

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libxml2 | 2.9.13-6.el9_5.2 | [ALSA-2025:2679](https://errata.almalinux.org/9/ALSA-2025-2679.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-56171](https://access.redhat.com/security/cve/CVE-2024-56171), [CVE-2025-24928](https://access.redhat.com/security/cve/CVE-2025-24928))
libxml2-debuginfo | 2.9.13-6.el9_5.2 | |
libxml2-debugsource | 2.9.13-6.el9_5.2 | |
python3-libxml2 | 2.9.13-6.el9_5.2 | [ALSA-2025:2679](https://errata.almalinux.org/9/ALSA-2025-2679.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-56171](https://access.redhat.com/security/cve/CVE-2024-56171), [CVE-2025-24928](https://access.redhat.com/security/cve/CVE-2025-24928))
python3-libxml2-debuginfo | 2.9.13-6.el9_5.2 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-8.0 | 8.0.14-1.el9_5 | |
aspnetcore-runtime-9.0 | 9.0.3-1.el9_5 | |
aspnetcore-runtime-dbg-8.0 | 8.0.14-1.el9_5 | |
aspnetcore-runtime-dbg-9.0 | 9.0.3-1.el9_5 | |
aspnetcore-targeting-pack-8.0 | 8.0.14-1.el9_5 | |
aspnetcore-targeting-pack-9.0 | 9.0.3-1.el9_5 | |
dotnet-apphost-pack-8.0 | 8.0.14-1.el9_5 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.14-1.el9_5 | |
dotnet-apphost-pack-9.0 | 9.0.3-1.el9_5 | |
dotnet-apphost-pack-9.0-debuginfo | 9.0.3-1.el9_5 | |
dotnet-host | 9.0.3-1.el9_5 | |
dotnet-host-debuginfo | 9.0.3-1.el9_5 | |
dotnet-hostfxr-8.0 | 8.0.14-1.el9_5 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.14-1.el9_5 | |
dotnet-hostfxr-9.0 | 9.0.3-1.el9_5 | |
dotnet-hostfxr-9.0-debuginfo | 9.0.3-1.el9_5 | |
dotnet-runtime-8.0 | 8.0.14-1.el9_5 | |
dotnet-runtime-8.0-debuginfo | 8.0.14-1.el9_5 | |
dotnet-runtime-9.0 | 9.0.3-1.el9_5 | |
dotnet-runtime-9.0-debuginfo | 9.0.3-1.el9_5 | |
dotnet-runtime-dbg-8.0 | 8.0.14-1.el9_5 | |
dotnet-runtime-dbg-9.0 | 9.0.3-1.el9_5 | |
dotnet-sdk-8.0 | 8.0.114-1.el9_5 | |
dotnet-sdk-8.0-debuginfo | 8.0.114-1.el9_5 | |
dotnet-sdk-9.0 | 9.0.104-1.el9_5 | |
dotnet-sdk-9.0-debuginfo | 9.0.104-1.el9_5 | |
dotnet-sdk-aot-9.0 | 9.0.104-1.el9_5 | |
dotnet-sdk-aot-9.0-debuginfo | 9.0.104-1.el9_5 | |
dotnet-sdk-dbg-8.0 | 8.0.114-1.el9_5 | |
dotnet-sdk-dbg-9.0 | 9.0.104-1.el9_5 | |
dotnet-targeting-pack-8.0 | 8.0.14-1.el9_5 | |
dotnet-targeting-pack-9.0 | 9.0.3-1.el9_5 | |
dotnet-templates-8.0 | 8.0.114-1.el9_5 | |
dotnet-templates-9.0 | 9.0.104-1.el9_5 | |
dotnet8.0-debuginfo | 8.0.114-1.el9_5 | |
dotnet8.0-debugsource | 8.0.114-1.el9_5 | |
dotnet9.0-debuginfo | 9.0.104-1.el9_5 | |
dotnet9.0-debugsource | 9.0.104-1.el9_5 | |
libxml2-devel | 2.9.13-6.el9_5.2 | [ALSA-2025:2679](https://errata.almalinux.org/9/ALSA-2025-2679.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-56171](https://access.redhat.com/security/cve/CVE-2024-56171), [CVE-2025-24928](https://access.redhat.com/security/cve/CVE-2025-24928))
netstandard-targeting-pack-2.1 | 9.0.104-1.el9_5 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-8.0-source-built-artifacts | 8.0.114-1.el9_5 | |
dotnet-sdk-9.0-source-built-artifacts | 9.0.104-1.el9_5 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libxml2-static | 2.9.13-6.el9_5.2 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libxml2 | 2.9.13-6.el9_5.2 | [ALSA-2025:2679](https://errata.almalinux.org/9/ALSA-2025-2679.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-56171](https://access.redhat.com/security/cve/CVE-2024-56171), [CVE-2025-24928](https://access.redhat.com/security/cve/CVE-2025-24928))
libxml2-debuginfo | 2.9.13-6.el9_5.2 | |
libxml2-debugsource | 2.9.13-6.el9_5.2 | |
python3-libxml2 | 2.9.13-6.el9_5.2 | [ALSA-2025:2679](https://errata.almalinux.org/9/ALSA-2025-2679.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-56171](https://access.redhat.com/security/cve/CVE-2024-56171), [CVE-2025-24928](https://access.redhat.com/security/cve/CVE-2025-24928))
python3-libxml2-debuginfo | 2.9.13-6.el9_5.2 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-8.0 | 8.0.14-1.el9_5 | |
aspnetcore-runtime-9.0 | 9.0.3-1.el9_5 | |
aspnetcore-runtime-dbg-8.0 | 8.0.14-1.el9_5 | |
aspnetcore-runtime-dbg-9.0 | 9.0.3-1.el9_5 | |
aspnetcore-targeting-pack-8.0 | 8.0.14-1.el9_5 | |
aspnetcore-targeting-pack-9.0 | 9.0.3-1.el9_5 | |
dotnet-apphost-pack-8.0 | 8.0.14-1.el9_5 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.14-1.el9_5 | |
dotnet-apphost-pack-9.0 | 9.0.3-1.el9_5 | |
dotnet-apphost-pack-9.0-debuginfo | 9.0.3-1.el9_5 | |
dotnet-host | 9.0.3-1.el9_5 | |
dotnet-host-debuginfo | 9.0.3-1.el9_5 | |
dotnet-hostfxr-8.0 | 8.0.14-1.el9_5 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.14-1.el9_5 | |
dotnet-hostfxr-9.0 | 9.0.3-1.el9_5 | |
dotnet-hostfxr-9.0-debuginfo | 9.0.3-1.el9_5 | |
dotnet-runtime-8.0 | 8.0.14-1.el9_5 | |
dotnet-runtime-8.0-debuginfo | 8.0.14-1.el9_5 | |
dotnet-runtime-9.0 | 9.0.3-1.el9_5 | |
dotnet-runtime-9.0-debuginfo | 9.0.3-1.el9_5 | |
dotnet-runtime-dbg-8.0 | 8.0.14-1.el9_5 | |
dotnet-runtime-dbg-9.0 | 9.0.3-1.el9_5 | |
dotnet-sdk-8.0 | 8.0.114-1.el9_5 | |
dotnet-sdk-8.0-debuginfo | 8.0.114-1.el9_5 | |
dotnet-sdk-9.0 | 9.0.104-1.el9_5 | |
dotnet-sdk-9.0-debuginfo | 9.0.104-1.el9_5 | |
dotnet-sdk-aot-9.0 | 9.0.104-1.el9_5 | |
dotnet-sdk-aot-9.0-debuginfo | 9.0.104-1.el9_5 | |
dotnet-sdk-dbg-8.0 | 8.0.114-1.el9_5 | |
dotnet-sdk-dbg-9.0 | 9.0.104-1.el9_5 | |
dotnet-targeting-pack-8.0 | 8.0.14-1.el9_5 | |
dotnet-targeting-pack-9.0 | 9.0.3-1.el9_5 | |
dotnet-templates-8.0 | 8.0.114-1.el9_5 | |
dotnet-templates-9.0 | 9.0.104-1.el9_5 | |
dotnet8.0-debuginfo | 8.0.114-1.el9_5 | |
dotnet8.0-debugsource | 8.0.114-1.el9_5 | |
dotnet9.0-debuginfo | 9.0.104-1.el9_5 | |
dotnet9.0-debugsource | 9.0.104-1.el9_5 | |
libxml2-devel | 2.9.13-6.el9_5.2 | [ALSA-2025:2679](https://errata.almalinux.org/9/ALSA-2025-2679.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-56171](https://access.redhat.com/security/cve/CVE-2024-56171), [CVE-2025-24928](https://access.redhat.com/security/cve/CVE-2025-24928))
netstandard-targeting-pack-2.1 | 9.0.104-1.el9_5 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-8.0-source-built-artifacts | 8.0.114-1.el9_5 | |
dotnet-sdk-9.0-source-built-artifacts | 9.0.104-1.el9_5 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libxml2-static | 2.9.13-6.el9_5.2 | |

