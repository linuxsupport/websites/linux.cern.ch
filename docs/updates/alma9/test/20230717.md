## 2023-07-17

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-targeting-pack-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-host | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-host-debuginfo | 7.0.9-1.el9_2 | |
dotnet-hostfxr-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-hostfxr-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-runtime-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-runtime-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-sdk-7.0 | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet-targeting-pack-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-templates-7.0 | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet7.0-debugsource | 7.0.109-1.el9_2 | |
firefox | 102.13.0-2.el9_2.alma | |
firefox-debuginfo | 102.13.0-2.el9_2.alma | |
firefox-debugsource | 102.13.0-2.el9_2.alma | |
firefox-x11 | 102.13.0-2.el9_2.alma | |
netstandard-targeting-pack-2.1 | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
open-vm-tools | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-debuginfo | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-debugsource | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-desktop | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-desktop-debuginfo | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-salt-minion | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-sdmp | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-sdmp-debuginfo | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-test | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-test-debuginfo | 12.1.5-1.el9_2.1.alma | |
thunderbird | 102.13.0-2.el9_2.alma | |
thunderbird-debuginfo | 102.13.0-2.el9_2.alma | |
thunderbird-debugsource | 102.13.0-2.el9_2.alma | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-7.0-source-built-artifacts | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
open-vm-tools-devel | 12.1.5-1.el9_2.1.alma | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.13.0-2.el9_2.alma.plus | |
thunderbird-debuginfo | 102.13.0-2.el9_2.alma.plus | |
thunderbird-debugsource | 102.13.0-2.el9_2.alma.plus | |
thunderbird-librnp-rnp | 102.13.0-2.el9_2.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.13.0-2.el9_2.alma.plus | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-targeting-pack-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-host | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-host-debuginfo | 7.0.9-1.el9_2 | |
dotnet-hostfxr-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-hostfxr-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-runtime-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-runtime-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-sdk-7.0 | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet-targeting-pack-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-templates-7.0 | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet7.0-debugsource | 7.0.109-1.el9_2 | |
firefox | 102.13.0-2.el9_2.alma | |
firefox-debuginfo | 102.13.0-2.el9_2.alma | |
firefox-debugsource | 102.13.0-2.el9_2.alma | |
firefox-x11 | 102.13.0-2.el9_2.alma | |
netstandard-targeting-pack-2.1 | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
open-vm-tools | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-debuginfo | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-debugsource | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-desktop | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-desktop-debuginfo | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-test | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-test-debuginfo | 12.1.5-1.el9_2.1.alma | |
thunderbird | 102.13.0-2.el9_2.alma | |
thunderbird-debuginfo | 102.13.0-2.el9_2.alma | |
thunderbird-debugsource | 102.13.0-2.el9_2.alma | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-7.0-source-built-artifacts | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
open-vm-tools-devel | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-sdmp | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-sdmp-debuginfo | 12.1.5-1.el9_2.1.alma | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.13.0-2.el9_2.alma.plus | |
thunderbird-debuginfo | 102.13.0-2.el9_2.alma.plus | |
thunderbird-debugsource | 102.13.0-2.el9_2.alma.plus | |
thunderbird-librnp-rnp | 102.13.0-2.el9_2.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.13.0-2.el9_2.alma.plus | |

