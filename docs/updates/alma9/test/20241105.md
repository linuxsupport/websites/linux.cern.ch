## 2024-11-05

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 128.4.0-1.el9_4 | |
firefox-debuginfo | 128.4.0-1.el9_4 | |
firefox-debugsource | 128.4.0-1.el9_4 | |
firefox-x11 | 128.4.0-1.el9_4 | |
openexr | 3.1.1-2.el9_4.1 | |
openexr-debuginfo | 3.1.1-2.el9_4.1 | |
openexr-debugsource | 3.1.1-2.el9_4.1 | |
openexr-libs | 3.1.1-2.el9_4.1 | |
openexr-libs-debuginfo | 3.1.1-2.el9_4.1 | |
thunderbird | 128.4.0-1.el9_4.alma.1 | |
thunderbird-debuginfo | 128.4.0-1.el9_4.alma.1 | |
thunderbird-debugsource | 128.4.0-1.el9_4.alma.1 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openexr-devel | 3.1.1-2.el9_4.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 128.4.0-1.el9_4 | |
firefox-debuginfo | 128.4.0-1.el9_4 | |
firefox-debugsource | 128.4.0-1.el9_4 | |
firefox-x11 | 128.4.0-1.el9_4 | |
openexr | 3.1.1-2.el9_4.1 | |
openexr-debuginfo | 3.1.1-2.el9_4.1 | |
openexr-debugsource | 3.1.1-2.el9_4.1 | |
openexr-libs | 3.1.1-2.el9_4.1 | |
openexr-libs-debuginfo | 3.1.1-2.el9_4.1 | |
thunderbird | 128.4.0-1.el9_4.alma.1 | |
thunderbird-debuginfo | 128.4.0-1.el9_4.alma.1 | |
thunderbird-debugsource | 128.4.0-1.el9_4.alma.1 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openexr-devel | 3.1.1-2.el9_4.1 | |

