## 2024-07-03

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
go-toolset | 1.21.11-1.el9_4 | |
golang | 1.21.11-1.el9_4 | |
golang-bin | 1.21.11-1.el9_4 | |
golang-docs | 1.21.11-1.el9_4 | |
golang-misc | 1.21.11-1.el9_4 | |
golang-src | 1.21.11-1.el9_4 | |
golang-tests | 1.21.11-1.el9_4 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
go-toolset | 1.21.11-1.el9_4 | |
golang | 1.21.11-1.el9_4 | |
golang-bin | 1.21.11-1.el9_4 | |
golang-docs | 1.21.11-1.el9_4 | |
golang-misc | 1.21.11-1.el9_4 | |
golang-src | 1.21.11-1.el9_4 | |
golang-tests | 1.21.11-1.el9_4 | |

