## 2024-03-15

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-runtime-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-targeting-pack-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-targeting-pack-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-apphost-pack-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-apphost-pack-7.0-debuginfo | 7.0.17-1.el9_3 | |
dotnet-apphost-pack-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-apphost-pack-8.0-debuginfo | 8.0.3-2.el9_3 | |
dotnet-host | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-host-debuginfo | 8.0.3-2.el9_3 | |
dotnet-hostfxr-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-hostfxr-7.0-debuginfo | 7.0.17-1.el9_3 | |
dotnet-hostfxr-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-hostfxr-8.0-debuginfo | 8.0.3-2.el9_3 | |
dotnet-runtime-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-runtime-7.0-debuginfo | 7.0.17-1.el9_3 | |
dotnet-runtime-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-runtime-8.0-debuginfo | 8.0.3-2.el9_3 | |
dotnet-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-7.0 | 7.0.117-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-7.0-debuginfo | 7.0.117-1.el9_3 | |
dotnet-sdk-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-8.0-debuginfo | 8.0.103-2.el9_3 | |
dotnet-sdk-dbg-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-targeting-pack-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-targeting-pack-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-templates-7.0 | 7.0.117-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-templates-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet7.0-debuginfo | 7.0.117-1.el9_3 | |
dotnet7.0-debugsource | 7.0.117-1.el9_3 | |
dotnet8.0-debuginfo | 8.0.103-2.el9_3 | |
dotnet8.0-debugsource | 8.0.103-2.el9_3 | |
netstandard-targeting-pack-2.1 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-7.0-source-built-artifacts | 7.0.117-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-8.0-source-built-artifacts | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-dbg-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-runtime-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-targeting-pack-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-targeting-pack-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-apphost-pack-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-apphost-pack-7.0-debuginfo | 7.0.17-1.el9_3 | |
dotnet-apphost-pack-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-apphost-pack-8.0-debuginfo | 8.0.3-2.el9_3 | |
dotnet-host | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-host-debuginfo | 8.0.3-2.el9_3 | |
dotnet-hostfxr-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-hostfxr-7.0-debuginfo | 7.0.17-1.el9_3 | |
dotnet-hostfxr-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-hostfxr-8.0-debuginfo | 8.0.3-2.el9_3 | |
dotnet-runtime-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-runtime-7.0-debuginfo | 7.0.17-1.el9_3 | |
dotnet-runtime-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-runtime-8.0-debuginfo | 8.0.3-2.el9_3 | |
dotnet-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-7.0 | 7.0.117-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-7.0-debuginfo | 7.0.117-1.el9_3 | |
dotnet-sdk-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-8.0-debuginfo | 8.0.103-2.el9_3 | |
dotnet-sdk-dbg-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-targeting-pack-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-targeting-pack-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-templates-7.0 | 7.0.117-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-templates-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet7.0-debuginfo | 7.0.117-1.el9_3 | |
dotnet7.0-debugsource | 7.0.117-1.el9_3 | |
dotnet8.0-debuginfo | 8.0.103-2.el9_3 | |
dotnet8.0-debugsource | 8.0.103-2.el9_3 | |
netstandard-targeting-pack-2.1 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-7.0-source-built-artifacts | 7.0.117-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-8.0-source-built-artifacts | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-dbg-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))

