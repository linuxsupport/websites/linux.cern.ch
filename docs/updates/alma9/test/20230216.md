## 2023-02-16

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.114-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.114-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet6.0-debugsource | 6.0.114-1.el9_1 | |
osbuild-composer | 62.1-3.el9_1.alma.1 | |
osbuild-composer-core | 62.1-3.el9_1.alma.1 | |
osbuild-composer-core-debuginfo | 62.1-3.el9_1.alma.1 | |
osbuild-composer-debugsource | 62.1-3.el9_1.alma.1 | |
osbuild-composer-dnf-json | 62.1-3.el9_1.alma.1 | |
osbuild-composer-tests-debuginfo | 62.1-3.el9_1.alma.1 | |
osbuild-composer-worker | 62.1-3.el9_1.alma.1 | |
osbuild-composer-worker-debuginfo | 62.1-3.el9_1.alma.1 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.114-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet6.0-debugsource | 6.0.114-1.el9_1 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
osbuild-composer-tests | 62.1-3.el9_1.alma.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.114-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.14-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.114-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet6.0-debugsource | 6.0.114-1.el9_1 | |
osbuild-composer | 62.1-3.el9_1.alma.1 | |
osbuild-composer-core | 62.1-3.el9_1.alma.1 | |
osbuild-composer-core-debuginfo | 62.1-3.el9_1.alma.1 | |
osbuild-composer-debugsource | 62.1-3.el9_1.alma.1 | |
osbuild-composer-dnf-json | 62.1-3.el9_1.alma.1 | |
osbuild-composer-tests-debuginfo | 62.1-3.el9_1.alma.1 | |
osbuild-composer-worker | 62.1-3.el9_1.alma.1 | |
osbuild-composer-worker-debuginfo | 62.1-3.el9_1.alma.1 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.114-1.el9_1 | [RHBA-2023:0782](https://access.redhat.com/errata/RHBA-2023:0782) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet6.0-debugsource | 6.0.114-1.el9_1 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
osbuild-composer-tests | 62.1-3.el9_1.alma.1 | |

