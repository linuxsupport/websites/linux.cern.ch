## 2024-08-20

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
curl | 7.76.1-29.el9_4.1 | |
curl-debuginfo | 7.76.1-29.el9_4.1 | |
curl-debugsource | 7.76.1-29.el9_4.1 | |
curl-minimal | 7.76.1-29.el9_4.1 | |
curl-minimal-debuginfo | 7.76.1-29.el9_4.1 | |
libcurl | 7.76.1-29.el9_4.1 | |
libcurl-debuginfo | 7.76.1-29.el9_4.1 | |
libcurl-minimal | 7.76.1-29.el9_4.1 | |
libcurl-minimal-debuginfo | 7.76.1-29.el9_4.1 | |
python3-setuptools | 53.0.0-12.el9_4.1 | |
python3-setuptools-wheel | 53.0.0-12.el9_4.1 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libcurl-devel | 7.76.1-29.el9_4.1 | |
python3.12-setuptools | 68.2.2-3.el9_4.1 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3.12-setuptools-wheel | 68.2.2-3.el9_4.1 | |

### synergy x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
oz | 0.18.1-13.el9 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
curl | 7.76.1-29.el9_4.1 | |
curl-debuginfo | 7.76.1-29.el9_4.1 | |
curl-debugsource | 7.76.1-29.el9_4.1 | |
curl-minimal | 7.76.1-29.el9_4.1 | |
curl-minimal-debuginfo | 7.76.1-29.el9_4.1 | |
libcurl | 7.76.1-29.el9_4.1 | |
libcurl-debuginfo | 7.76.1-29.el9_4.1 | |
libcurl-minimal | 7.76.1-29.el9_4.1 | |
libcurl-minimal-debuginfo | 7.76.1-29.el9_4.1 | |
python3-setuptools | 53.0.0-12.el9_4.1 | |
python3-setuptools-wheel | 53.0.0-12.el9_4.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libcurl-devel | 7.76.1-29.el9_4.1 | |
python3.12-setuptools | 68.2.2-3.el9_4.1 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3.12-setuptools-wheel | 68.2.2-3.el9_4.1 | |

### synergy aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
oz | 0.18.1-13.el9 | |

