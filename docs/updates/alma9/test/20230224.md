## 2023-02-24

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cloud-init | 22.1-5.el9.alma.1 | |
webkit2gtk3 | 2.36.7-1.el9_1.2 | |
webkit2gtk3-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-debugsource | 2.36.7-1.el9_1.2 | |
webkit2gtk3-devel | 2.36.7-1.el9_1.2 | |
webkit2gtk3-devel-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-devel | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-devel-debuginfo | 2.36.7-1.el9_1.2 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
webkit2gtk3-doc | 2.36.7-1.el9_1.2 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cloud-init | 22.1-5.el9.alma.1 | |
webkit2gtk3 | 2.36.7-1.el9_1.2 | |
webkit2gtk3-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-debugsource | 2.36.7-1.el9_1.2 | |
webkit2gtk3-devel | 2.36.7-1.el9_1.2 | |
webkit2gtk3-devel-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-devel | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-devel-debuginfo | 2.36.7-1.el9_1.2 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
webkit2gtk3-doc | 2.36.7-1.el9_1.2 | |

