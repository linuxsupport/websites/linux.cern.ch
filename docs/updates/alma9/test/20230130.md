## 2023-01-30

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-1.8.0-openjdk | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-debugsource | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-demo | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-devel | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-headless | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-javadoc | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-javadoc-zip | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-src | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
thunderbird | 102.7.1-1.el9_1.alma | |
thunderbird-debuginfo | 102.7.1-1.el9_1.alma | |
thunderbird-debugsource | 102.7.1-1.el9_1.alma | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-1.8.0-openjdk-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-debugsource | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-demo-fastdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-demo-fastdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-demo-slowdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-devel-fastdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-devel-fastdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-devel-slowdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-fastdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-fastdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-headless-fastdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-headless-fastdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-headless-slowdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-slowdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-src-fastdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-src-slowdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.7.1-1.el9_1.alma.plus | |
thunderbird-debuginfo | 102.7.1-1.el9_1.alma.plus | |
thunderbird-debugsource | 102.7.1-1.el9_1.alma.plus | |
thunderbird-librnp-rnp | 102.7.1-1.el9_1.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.7.1-1.el9_1.alma.plus | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-1.8.0-openjdk | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-debugsource | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-demo | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-devel | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-headless | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-javadoc | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-javadoc-zip | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-src | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
thunderbird | 102.7.1-1.el9_1.alma | |
thunderbird-debuginfo | 102.7.1-1.el9_1.alma | |
thunderbird-debugsource | 102.7.1-1.el9_1.alma | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-1.8.0-openjdk-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-debugsource | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-demo-fastdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-demo-fastdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-demo-slowdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-devel-fastdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-devel-fastdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-devel-slowdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-fastdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-fastdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-headless-fastdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-headless-fastdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-headless-slowdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-slowdebug-debuginfo | 1.8.0.362.b09-2.el9_1 | |
java-1.8.0-openjdk-src-fastdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-1.8.0-openjdk-src-slowdebug | 1.8.0.362.b09-2.el9_1 | [RHSA-2023:0210](https://access.redhat.com/errata/RHSA-2023:0210) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21830](https://access.redhat.com/security/cve/CVE-2023-21830), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.7.1-1.el9_1.alma.plus | |
thunderbird-debuginfo | 102.7.1-1.el9_1.alma.plus | |
thunderbird-debugsource | 102.7.1-1.el9_1.alma.plus | |
thunderbird-librnp-rnp | 102.7.1-1.el9_1.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.7.1-1.el9_1.alma.plus | |

