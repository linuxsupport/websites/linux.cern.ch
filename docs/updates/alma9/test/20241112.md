## 2024-11-12

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20230808-2.20240910.1.el9_4 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
podman | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-debuginfo | 4.9.4-16.el9_4 | |
podman-debugsource | 4.9.4-16.el9_4 | |
podman-docker | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-plugins | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-plugins-debuginfo | 4.9.4-16.el9_4 | |
podman-remote | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-remote-debuginfo | 4.9.4-16.el9_4 | |
podman-tests | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20230808-2.20240910.1.el9_4 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20230808-2.20240910.1.el9_4 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
podman | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-debuginfo | 4.9.4-16.el9_4 | |
podman-debugsource | 4.9.4-16.el9_4 | |
podman-docker | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-plugins | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-plugins-debuginfo | 4.9.4-16.el9_4 | |
podman-remote | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-remote-debuginfo | 4.9.4-16.el9_4 | |
podman-tests | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20230808-2.20240910.1.el9_4 | |

