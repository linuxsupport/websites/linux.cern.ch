## 2023-01-16

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.13-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
aspnetcore-runtime-7.0 | 7.0.2-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.13-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
aspnetcore-targeting-pack-7.0 | 7.0.2-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0 | 6.0.13-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
dotnet-apphost-pack-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-apphost-pack-7.0 | 7.0.2-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-host | 7.0.2-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host-debuginfo | 7.0.2-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.13-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
dotnet-hostfxr-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-hostfxr-7.0 | 7.0.2-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.13-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
dotnet-runtime-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-runtime-7.0 | 7.0.2-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.113-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
dotnet-sdk-6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet-sdk-7.0 | 7.0.102-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.13-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
dotnet-targeting-pack-7.0 | 7.0.2-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.113-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
dotnet-templates-7.0 | 7.0.102-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet6.0-debugsource | 6.0.113-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet7.0-debugsource | 7.0.102-1.el9_1 | |
netstandard-targeting-pack-2.1 | 7.0.102-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-host-debuginfo | 7.0.2-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.113-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
dotnet-sdk-7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.102-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet6.0-debugsource | 6.0.113-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet7.0-debugsource | 7.0.102-1.el9_1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.13-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
aspnetcore-runtime-7.0 | 7.0.2-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.13-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
aspnetcore-targeting-pack-7.0 | 7.0.2-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0 | 6.0.13-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
dotnet-apphost-pack-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-apphost-pack-7.0 | 7.0.2-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-host | 7.0.2-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host-debuginfo | 7.0.2-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.13-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
dotnet-hostfxr-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-hostfxr-7.0 | 7.0.2-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.13-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
dotnet-runtime-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-runtime-7.0 | 7.0.2-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.113-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
dotnet-sdk-6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet-sdk-7.0 | 7.0.102-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.13-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
dotnet-targeting-pack-7.0 | 7.0.2-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.113-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
dotnet-templates-7.0 | 7.0.102-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet6.0-debugsource | 6.0.113-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet7.0-debugsource | 7.0.102-1.el9_1 | |
netstandard-targeting-pack-2.1 | 7.0.102-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-host-debuginfo | 7.0.2-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.113-1.el9_1 | [RHSA-2023:0077](https://access.redhat.com/errata/RHSA-2023:0077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21538](https://access.redhat.com/security/cve/CVE-2023-21538))
dotnet-sdk-7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.102-1.el9_1 | [RHBA-2023:0080](https://access.redhat.com/errata/RHBA-2023:0080) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet6.0-debugsource | 6.0.113-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet7.0-debugsource | 7.0.102-1.el9_1 | |

