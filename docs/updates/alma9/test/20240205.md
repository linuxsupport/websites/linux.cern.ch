## 2024-02-05

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.7.0-1.el9_3.alma | |
firefox-debuginfo | 115.7.0-1.el9_3.alma | |
firefox-debugsource | 115.7.0-1.el9_3.alma | |
firefox-x11 | 115.7.0-1.el9_3.alma | |
thunderbird | 115.7.0-1.el9_3.alma | |
thunderbird-debuginfo | 115.7.0-1.el9_3.alma | |
thunderbird-debugsource | 115.7.0-1.el9_3.alma | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 115.7.0-1.el9_3.alma.plus | |
thunderbird-debuginfo | 115.7.0-1.el9_3.alma.plus | |
thunderbird-debugsource | 115.7.0-1.el9_3.alma.plus | |
thunderbird-librnp-rnp | 115.7.0-1.el9_3.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 115.7.0-1.el9_3.alma.plus | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.7.0-1.el9_3.alma | |
firefox-debuginfo | 115.7.0-1.el9_3.alma | |
firefox-debugsource | 115.7.0-1.el9_3.alma | |
firefox-x11 | 115.7.0-1.el9_3.alma | |
thunderbird | 115.7.0-1.el9_3.alma | |
thunderbird-debuginfo | 115.7.0-1.el9_3.alma | |
thunderbird-debugsource | 115.7.0-1.el9_3.alma | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 115.7.0-1.el9_3.alma.plus | |
thunderbird-debuginfo | 115.7.0-1.el9_3.alma.plus | |
thunderbird-debugsource | 115.7.0-1.el9_3.alma.plus | |
thunderbird-librnp-rnp | 115.7.0-1.el9_3.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 115.7.0-1.el9_3.alma.plus | |

