## 2025-02-25

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind9.18 | 9.18.29-1.el9_5.1 | [ALSA-2025:1670](https://errata.almalinux.org/9/ALSA-2025-1670.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11187](https://access.redhat.com/security/cve/CVE-2024-11187), [CVE-2024-12705](https://access.redhat.com/security/cve/CVE-2024-12705))
bind9.18-chroot | 9.18.29-1.el9_5.1 | [ALSA-2025:1670](https://errata.almalinux.org/9/ALSA-2025-1670.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11187](https://access.redhat.com/security/cve/CVE-2024-11187), [CVE-2024-12705](https://access.redhat.com/security/cve/CVE-2024-12705))
bind9.18-debuginfo | 9.18.29-1.el9_5.1 | |
bind9.18-debugsource | 9.18.29-1.el9_5.1 | |
bind9.18-dnssec-utils | 9.18.29-1.el9_5.1 | [ALSA-2025:1670](https://errata.almalinux.org/9/ALSA-2025-1670.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11187](https://access.redhat.com/security/cve/CVE-2024-11187), [CVE-2024-12705](https://access.redhat.com/security/cve/CVE-2024-12705))
bind9.18-dnssec-utils-debuginfo | 9.18.29-1.el9_5.1 | |
bind9.18-libs | 9.18.29-1.el9_5.1 | [ALSA-2025:1670](https://errata.almalinux.org/9/ALSA-2025-1670.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11187](https://access.redhat.com/security/cve/CVE-2024-11187), [CVE-2024-12705](https://access.redhat.com/security/cve/CVE-2024-12705))
bind9.18-libs-debuginfo | 9.18.29-1.el9_5.1 | |
bind9.18-utils | 9.18.29-1.el9_5.1 | [ALSA-2025:1670](https://errata.almalinux.org/9/ALSA-2025-1670.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11187](https://access.redhat.com/security/cve/CVE-2024-11187), [CVE-2024-12705](https://access.redhat.com/security/cve/CVE-2024-12705))
bind9.18-utils-debuginfo | 9.18.29-1.el9_5.1 | |
postgresql | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-contrib | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-contrib-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-debugsource | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-docs | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-docs-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-plperl | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-plperl-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-plpython3 | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-plpython3-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-pltcl | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-pltcl-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-private-devel | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-private-libs | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-private-libs-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-server | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-server-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-server-devel | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-server-devel-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-static | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-test | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-test-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-test-rpm-macros | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-upgrade | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-upgrade-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-upgrade-devel | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-upgrade-devel-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind9.18-devel | 9.18.29-1.el9_5.1 | [ALSA-2025:1670](https://errata.almalinux.org/9/ALSA-2025-1670.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11187](https://access.redhat.com/security/cve/CVE-2024-11187), [CVE-2024-12705](https://access.redhat.com/security/cve/CVE-2024-12705))
bind9.18-doc | 9.18.29-1.el9_5.1 | [ALSA-2025:1670](https://errata.almalinux.org/9/ALSA-2025-1670.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11187](https://access.redhat.com/security/cve/CVE-2024-11187), [CVE-2024-12705](https://access.redhat.com/security/cve/CVE-2024-12705))

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind9.18 | 9.18.29-1.el9_5.1 | [ALSA-2025:1670](https://errata.almalinux.org/9/ALSA-2025-1670.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11187](https://access.redhat.com/security/cve/CVE-2024-11187), [CVE-2024-12705](https://access.redhat.com/security/cve/CVE-2024-12705))
bind9.18-chroot | 9.18.29-1.el9_5.1 | [ALSA-2025:1670](https://errata.almalinux.org/9/ALSA-2025-1670.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11187](https://access.redhat.com/security/cve/CVE-2024-11187), [CVE-2024-12705](https://access.redhat.com/security/cve/CVE-2024-12705))
bind9.18-debuginfo | 9.18.29-1.el9_5.1 | |
bind9.18-debugsource | 9.18.29-1.el9_5.1 | |
bind9.18-dnssec-utils | 9.18.29-1.el9_5.1 | [ALSA-2025:1670](https://errata.almalinux.org/9/ALSA-2025-1670.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11187](https://access.redhat.com/security/cve/CVE-2024-11187), [CVE-2024-12705](https://access.redhat.com/security/cve/CVE-2024-12705))
bind9.18-dnssec-utils-debuginfo | 9.18.29-1.el9_5.1 | |
bind9.18-libs | 9.18.29-1.el9_5.1 | [ALSA-2025:1670](https://errata.almalinux.org/9/ALSA-2025-1670.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11187](https://access.redhat.com/security/cve/CVE-2024-11187), [CVE-2024-12705](https://access.redhat.com/security/cve/CVE-2024-12705))
bind9.18-libs-debuginfo | 9.18.29-1.el9_5.1 | |
bind9.18-utils | 9.18.29-1.el9_5.1 | [ALSA-2025:1670](https://errata.almalinux.org/9/ALSA-2025-1670.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11187](https://access.redhat.com/security/cve/CVE-2024-11187), [CVE-2024-12705](https://access.redhat.com/security/cve/CVE-2024-12705))
bind9.18-utils-debuginfo | 9.18.29-1.el9_5.1 | |
postgresql | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-contrib | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-contrib-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-debugsource | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-docs | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-docs-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-plperl | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-plperl-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-plpython3 | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-plpython3-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-pltcl | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-pltcl-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-private-devel | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-private-libs | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-private-libs-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-server | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-server-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-server-devel | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-server-devel-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-static | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-test | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-test-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-test-rpm-macros | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-upgrade | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-upgrade-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |
postgresql-upgrade-devel | 16.8-1.module_el9.5.0+141+f4b0f9b7 | [ALSA-2025:1743](https://errata.almalinux.org/9/ALSA-2025-1743.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-upgrade-devel-debuginfo | 16.8-1.module_el9.5.0+141+f4b0f9b7 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind9.18-devel | 9.18.29-1.el9_5.1 | [ALSA-2025:1670](https://errata.almalinux.org/9/ALSA-2025-1670.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11187](https://access.redhat.com/security/cve/CVE-2024-11187), [CVE-2024-12705](https://access.redhat.com/security/cve/CVE-2024-12705))
bind9.18-doc | 9.18.29-1.el9_5.1 | [ALSA-2025:1670](https://errata.almalinux.org/9/ALSA-2025-1670.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11187](https://access.redhat.com/security/cve/CVE-2024-11187), [CVE-2024-12705](https://access.redhat.com/security/cve/CVE-2024-12705))

