## 2023-11-20

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firewalld | 1.2.1-1.el9.0.1 | |
firewalld-filesystem | 1.2.1-1.el9.0.1 | |
python3-firewall | 1.2.1-1.el9.0.1 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firewall-applet | 1.2.1-1.el9.0.1 | |
firewall-config | 1.2.1-1.el9.0.1 | |
java-21-openjdk | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-debugsource | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-demo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-devel | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-devel-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-devel-fastdebug-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-devel-slowdebug-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-fastdebug-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-headless | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-headless-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-headless-fastdebug-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-headless-slowdebug-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-javadoc | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-javadoc-zip | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-jmods | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-slowdebug-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-src | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-static-libs | 21.0.1.0.12-3.el9.alma.1 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-21-openjdk-demo-fastdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-demo-slowdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-devel-fastdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-devel-slowdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-fastdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-headless-fastdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-headless-slowdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-jmods-fastdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-jmods-slowdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-slowdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-src-fastdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-src-slowdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-static-libs-fastdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-static-libs-slowdebug | 21.0.1.0.12-3.el9.alma.1 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firewalld | 1.2.1-1.el9.0.1 | |
firewalld-filesystem | 1.2.1-1.el9.0.1 | |
python3-firewall | 1.2.1-1.el9.0.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firewall-applet | 1.2.1-1.el9.0.1 | |
firewall-config | 1.2.1-1.el9.0.1 | |
java-21-openjdk | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-debugsource | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-demo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-devel | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-devel-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-devel-fastdebug-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-devel-slowdebug-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-fastdebug-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-headless | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-headless-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-headless-fastdebug-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-headless-slowdebug-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-javadoc | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-javadoc-zip | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-jmods | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-slowdebug-debuginfo | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-src | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-static-libs | 21.0.1.0.12-3.el9.alma.1 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-21-openjdk-demo-fastdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-demo-slowdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-devel-fastdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-devel-slowdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-fastdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-headless-fastdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-headless-slowdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-jmods-fastdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-jmods-slowdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-slowdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-src-fastdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-src-slowdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-static-libs-fastdebug | 21.0.1.0.12-3.el9.alma.1 | |
java-21-openjdk-static-libs-slowdebug | 21.0.1.0.12-3.el9.alma.1 | |

