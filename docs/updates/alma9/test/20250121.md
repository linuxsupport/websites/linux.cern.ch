## 2025-01-21

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-9.0 | 9.0.1-1.el9_5 | |
aspnetcore-runtime-dbg-9.0 | 9.0.1-1.el9_5 | |
aspnetcore-targeting-pack-9.0 | 9.0.1-1.el9_5 | |
dotnet-apphost-pack-9.0 | 9.0.1-1.el9_5 | |
dotnet-apphost-pack-9.0-debuginfo | 9.0.1-1.el9_5 | |
dotnet-host | 9.0.1-1.el9_5 | |
dotnet-host-debuginfo | 9.0.1-1.el9_5 | |
dotnet-hostfxr-9.0 | 9.0.1-1.el9_5 | |
dotnet-hostfxr-9.0-debuginfo | 9.0.1-1.el9_5 | |
dotnet-runtime-9.0 | 9.0.1-1.el9_5 | |
dotnet-runtime-9.0-debuginfo | 9.0.1-1.el9_5 | |
dotnet-runtime-dbg-9.0 | 9.0.1-1.el9_5 | |
dotnet-sdk-9.0 | 9.0.102-1.el9_5 | |
dotnet-sdk-9.0-debuginfo | 9.0.102-1.el9_5 | |
dotnet-sdk-aot-9.0 | 9.0.102-1.el9_5 | |
dotnet-sdk-aot-9.0-debuginfo | 9.0.102-1.el9_5 | |
dotnet-sdk-dbg-9.0 | 9.0.102-1.el9_5 | |
dotnet-targeting-pack-9.0 | 9.0.1-1.el9_5 | |
dotnet-templates-9.0 | 9.0.102-1.el9_5 | |
dotnet9.0-debuginfo | 9.0.102-1.el9_5 | |
dotnet9.0-debugsource | 9.0.102-1.el9_5 | |
netstandard-targeting-pack-2.1 | 9.0.102-1.el9_5 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-9.0-source-built-artifacts | 9.0.102-1.el9_5 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-9.0 | 9.0.1-1.el9_5 | |
aspnetcore-runtime-dbg-9.0 | 9.0.1-1.el9_5 | |
aspnetcore-targeting-pack-9.0 | 9.0.1-1.el9_5 | |
dotnet-apphost-pack-9.0 | 9.0.1-1.el9_5 | |
dotnet-apphost-pack-9.0-debuginfo | 9.0.1-1.el9_5 | |
dotnet-host | 9.0.1-1.el9_5 | |
dotnet-host-debuginfo | 9.0.1-1.el9_5 | |
dotnet-hostfxr-9.0 | 9.0.1-1.el9_5 | |
dotnet-hostfxr-9.0-debuginfo | 9.0.1-1.el9_5 | |
dotnet-runtime-9.0 | 9.0.1-1.el9_5 | |
dotnet-runtime-9.0-debuginfo | 9.0.1-1.el9_5 | |
dotnet-runtime-dbg-9.0 | 9.0.1-1.el9_5 | |
dotnet-sdk-9.0 | 9.0.102-1.el9_5 | |
dotnet-sdk-9.0-debuginfo | 9.0.102-1.el9_5 | |
dotnet-sdk-aot-9.0 | 9.0.102-1.el9_5 | |
dotnet-sdk-aot-9.0-debuginfo | 9.0.102-1.el9_5 | |
dotnet-sdk-dbg-9.0 | 9.0.102-1.el9_5 | |
dotnet-targeting-pack-9.0 | 9.0.1-1.el9_5 | |
dotnet-templates-9.0 | 9.0.102-1.el9_5 | |
dotnet9.0-debuginfo | 9.0.102-1.el9_5 | |
dotnet9.0-debugsource | 9.0.102-1.el9_5 | |
netstandard-targeting-pack-2.1 | 9.0.102-1.el9_5 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-9.0-source-built-artifacts | 9.0.102-1.el9_5 | |

