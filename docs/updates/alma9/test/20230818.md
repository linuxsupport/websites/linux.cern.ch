## 2023-08-18

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
almalinux-release-synergy | 9-1.el9 | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
almalinux-release-synergy | 9-1.el9 | |

