## 2024-06-19

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.12.0-1.el9_4.alma.1 | |
firefox-debuginfo | 115.12.0-1.el9_4.alma.1 | |
firefox-debugsource | 115.12.0-1.el9_4.alma.1 | |
firefox-x11 | 115.12.0-1.el9_4.alma.1 | |
flatpak | 1.12.9-1.el9_4 | |
flatpak-debuginfo | 1.12.9-1.el9_4 | |
flatpak-debugsource | 1.12.9-1.el9_4 | |
flatpak-libs | 1.12.9-1.el9_4 | |
flatpak-libs-debuginfo | 1.12.9-1.el9_4 | |
flatpak-selinux | 1.12.9-1.el9_4 | |
flatpak-session-helper | 1.12.9-1.el9_4 | |
flatpak-session-helper-debuginfo | 1.12.9-1.el9_4 | |
flatpak-tests-debuginfo | 1.12.9-1.el9_4 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
flatpak-devel | 1.12.9-1.el9_4 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
flatpak-tests | 1.12.9-1.el9_4 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.12.0-1.el9_4.alma.1 | |
firefox-debuginfo | 115.12.0-1.el9_4.alma.1 | |
firefox-debugsource | 115.12.0-1.el9_4.alma.1 | |
firefox-x11 | 115.12.0-1.el9_4.alma.1 | |
flatpak | 1.12.9-1.el9_4 | |
flatpak-debuginfo | 1.12.9-1.el9_4 | |
flatpak-debugsource | 1.12.9-1.el9_4 | |
flatpak-libs | 1.12.9-1.el9_4 | |
flatpak-libs-debuginfo | 1.12.9-1.el9_4 | |
flatpak-selinux | 1.12.9-1.el9_4 | |
flatpak-session-helper | 1.12.9-1.el9_4 | |
flatpak-session-helper-debuginfo | 1.12.9-1.el9_4 | |
flatpak-tests-debuginfo | 1.12.9-1.el9_4 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
flatpak-devel | 1.12.9-1.el9_4 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
flatpak-tests | 1.12.9-1.el9_4 | |

