## 2024-05-31

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
less | 590-4.el9_4 | |
less-debuginfo | 590-4.el9_4 | |
less-debugsource | 590-4.el9_4 | |
libnghttp2 | 1.43.0-5.el9_4.3 | |
libnghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
nghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
nghttp2-debugsource | 1.43.0-5.el9_4.3 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libnghttp2-devel | 1.43.0-5.el9_4.3 | |
nghttp2 | 1.43.0-5.el9_4.3 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
less | 590-4.el9_4 | |
less-debuginfo | 590-4.el9_4 | |
less-debugsource | 590-4.el9_4 | |
libnghttp2 | 1.43.0-5.el9_4.3 | |
libnghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
nghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
nghttp2-debugsource | 1.43.0-5.el9_4.3 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libnghttp2-devel | 1.43.0-5.el9_4.3 | |
nghttp2 | 1.43.0-5.el9_4.3 | |

