## 2024-11-06

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2024b-2.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libtiff | 4.4.0-12.el9_4.1 | |
libtiff-debuginfo | 4.4.0-12.el9_4.1 | |
libtiff-debugsource | 4.4.0-12.el9_4.1 | |
libtiff-devel | 4.4.0-12.el9_4.1 | |
libtiff-tools-debuginfo | 4.4.0-12.el9_4.1 | |
tzdata-java | 2024b-2.el9 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libtiff-tools | 4.4.0-12.el9_4.1 | |
libtiff-tools-debuginfo | 4.4.0-12.el9_4.1 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libtiff-static | 4.4.0-12.el9_4.1 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2024b-2.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libtiff | 4.4.0-12.el9_4.1 | |
libtiff-debuginfo | 4.4.0-12.el9_4.1 | |
libtiff-debugsource | 4.4.0-12.el9_4.1 | |
libtiff-devel | 4.4.0-12.el9_4.1 | |
libtiff-tools-debuginfo | 4.4.0-12.el9_4.1 | |
tzdata-java | 2024b-2.el9 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libtiff-tools | 4.4.0-12.el9_4.1 | |
libtiff-tools-debuginfo | 4.4.0-12.el9_4.1 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libtiff-static | 4.4.0-12.el9_4.1 | |

