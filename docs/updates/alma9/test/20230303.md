## 2023-03-03

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.5-2.al9.cern | |
cern-sssd-conf-domain-cernch | 1.5-2.al9.cern | |
cern-sssd-conf-global | 1.5-2.al9.cern | |
cern-sssd-conf-global-cernch | 1.5-2.al9.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.5-2.al9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.5-2.al9.cern | |
cern-sssd-conf-domain-cernch | 1.5-2.al9.cern | |
cern-sssd-conf-global | 1.5-2.al9.cern | |
cern-sssd-conf-global-cernch | 1.5-2.al9.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.5-2.al9.cern | |

