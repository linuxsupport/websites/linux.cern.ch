## 2024-09-06

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bubblewrap | 0.4.1-7.el9_4.alma.1 | [ALSA-2024:6356](https://errata.almalinux.org/9/ALSA-2024-6356.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-42472](https://access.redhat.com/security/cve/CVE-2024-42472))
bubblewrap-debuginfo | 0.4.1-7.el9_4.alma.1 | |
bubblewrap-debugsource | 0.4.1-7.el9_4.alma.1 | |
NetworkManager | 1.46.0-19.el9_4 | |
NetworkManager-adsl | 1.46.0-19.el9_4 | |
NetworkManager-adsl-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-bluetooth | 1.46.0-19.el9_4 | |
NetworkManager-bluetooth-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-config-server | 1.46.0-19.el9_4 | |
NetworkManager-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-debugsource | 1.46.0-19.el9_4 | |
NetworkManager-initscripts-updown | 1.46.0-19.el9_4 | |
NetworkManager-libnm | 1.46.0-19.el9_4 | |
NetworkManager-libnm-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-team | 1.46.0-19.el9_4 | |
NetworkManager-team-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-tui | 1.46.0-19.el9_4 | |
NetworkManager-tui-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-wifi | 1.46.0-19.el9_4 | |
NetworkManager-wifi-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-wwan | 1.46.0-19.el9_4 | |
NetworkManager-wwan-debuginfo | 1.46.0-19.el9_4 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
augeas | 1.13.0-6.el9_4 | |
augeas-debuginfo | 1.13.0-6.el9_4 | |
augeas-debugsource | 1.13.0-6.el9_4 | |
augeas-libs | 1.13.0-6.el9_4 | |
augeas-libs-debuginfo | 1.13.0-6.el9_4 | |
flatpak | 1.12.9-3.el9_4 | [ALSA-2024:6356](https://errata.almalinux.org/9/ALSA-2024-6356.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-42472](https://access.redhat.com/security/cve/CVE-2024-42472))
flatpak-debuginfo | 1.12.9-3.el9_4 | |
flatpak-debugsource | 1.12.9-3.el9_4 | |
flatpak-libs | 1.12.9-3.el9_4 | [ALSA-2024:6356](https://errata.almalinux.org/9/ALSA-2024-6356.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-42472](https://access.redhat.com/security/cve/CVE-2024-42472))
flatpak-libs-debuginfo | 1.12.9-3.el9_4 | |
flatpak-selinux | 1.12.9-3.el9_4 | [ALSA-2024:6356](https://errata.almalinux.org/9/ALSA-2024-6356.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-42472](https://access.redhat.com/security/cve/CVE-2024-42472))
flatpak-session-helper | 1.12.9-3.el9_4 | [ALSA-2024:6356](https://errata.almalinux.org/9/ALSA-2024-6356.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-42472](https://access.redhat.com/security/cve/CVE-2024-42472))
flatpak-session-helper-debuginfo | 1.12.9-3.el9_4 | |
gcc-toolset-12-gcc | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-c++ | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-c++-debuginfo | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-debuginfo | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-gfortran | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-gfortran-debuginfo | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-plugin-annobin | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-plugin-annobin-debuginfo | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-plugin-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-plugin-devel-debuginfo | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libasan-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libatomic-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libgccjit | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libgccjit-debuginfo | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libgccjit-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libgccjit-docs | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libitm-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-liblsan-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libquadmath-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libstdc++-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libstdc++-docs | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libtsan-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libubsan-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-offload-nvptx | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-offload-nvptx-debuginfo | 12.2.1-7.7.el9_4 | |
NetworkManager-cloud-setup | 1.46.0-19.el9_4 | |
NetworkManager-cloud-setup-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-config-connectivity-redhat | 1.46.0-19.el9_4 | |
NetworkManager-dispatcher-routing-rules | 1.46.0-19.el9_4 | |
NetworkManager-ovs | 1.46.0-19.el9_4 | |
NetworkManager-ovs-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-ppp | 1.46.0-19.el9_4 | |
NetworkManager-ppp-debuginfo | 1.46.0-19.el9_4 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
augeas-devel | 1.13.0-6.el9_4 | |
flatpak-devel | 1.12.9-3.el9_4 | [ALSA-2024:6356](https://errata.almalinux.org/9/ALSA-2024-6356.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-42472](https://access.redhat.com/security/cve/CVE-2024-42472))
NetworkManager-libnm-devel | 1.46.0-19.el9_4 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
augeas-static | 1.13.0-6.el9_4 | |
flatpak-tests | 1.12.9-3.el9_4 | |
flatpak-tests-debuginfo | 1.12.9-3.el9_4 | |
libasan8 | 12.2.1-7.7.el9_4 | |
libasan8-debuginfo | 12.2.1-7.7.el9_4 | |
libtsan2 | 12.2.1-7.7.el9_4 | |
libtsan2-debuginfo | 12.2.1-7.7.el9_4 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bubblewrap | 0.4.1-7.el9_4.alma.1 | [ALSA-2024:6356](https://errata.almalinux.org/9/ALSA-2024-6356.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-42472](https://access.redhat.com/security/cve/CVE-2024-42472))
bubblewrap-debuginfo | 0.4.1-7.el9_4.alma.1 | |
bubblewrap-debugsource | 0.4.1-7.el9_4.alma.1 | |
NetworkManager | 1.46.0-19.el9_4 | |
NetworkManager-adsl | 1.46.0-19.el9_4 | |
NetworkManager-adsl-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-bluetooth | 1.46.0-19.el9_4 | |
NetworkManager-bluetooth-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-config-server | 1.46.0-19.el9_4 | |
NetworkManager-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-debugsource | 1.46.0-19.el9_4 | |
NetworkManager-initscripts-updown | 1.46.0-19.el9_4 | |
NetworkManager-libnm | 1.46.0-19.el9_4 | |
NetworkManager-libnm-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-team | 1.46.0-19.el9_4 | |
NetworkManager-team-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-tui | 1.46.0-19.el9_4 | |
NetworkManager-tui-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-wifi | 1.46.0-19.el9_4 | |
NetworkManager-wifi-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-wwan | 1.46.0-19.el9_4 | |
NetworkManager-wwan-debuginfo | 1.46.0-19.el9_4 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
augeas | 1.13.0-6.el9_4 | |
augeas-debuginfo | 1.13.0-6.el9_4 | |
augeas-debugsource | 1.13.0-6.el9_4 | |
augeas-libs | 1.13.0-6.el9_4 | |
augeas-libs-debuginfo | 1.13.0-6.el9_4 | |
flatpak | 1.12.9-3.el9_4 | [ALSA-2024:6356](https://errata.almalinux.org/9/ALSA-2024-6356.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-42472](https://access.redhat.com/security/cve/CVE-2024-42472))
flatpak-debuginfo | 1.12.9-3.el9_4 | |
flatpak-debugsource | 1.12.9-3.el9_4 | |
flatpak-libs | 1.12.9-3.el9_4 | [ALSA-2024:6356](https://errata.almalinux.org/9/ALSA-2024-6356.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-42472](https://access.redhat.com/security/cve/CVE-2024-42472))
flatpak-libs-debuginfo | 1.12.9-3.el9_4 | |
flatpak-selinux | 1.12.9-3.el9_4 | [ALSA-2024:6356](https://errata.almalinux.org/9/ALSA-2024-6356.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-42472](https://access.redhat.com/security/cve/CVE-2024-42472))
flatpak-session-helper | 1.12.9-3.el9_4 | [ALSA-2024:6356](https://errata.almalinux.org/9/ALSA-2024-6356.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-42472](https://access.redhat.com/security/cve/CVE-2024-42472))
flatpak-session-helper-debuginfo | 1.12.9-3.el9_4 | |
gcc-toolset-12-gcc | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-c++ | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-c++-debuginfo | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-debuginfo | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-gfortran | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-gfortran-debuginfo | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-plugin-annobin | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-plugin-annobin-debuginfo | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-plugin-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-gcc-plugin-devel-debuginfo | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libasan-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libatomic-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libgccjit | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libgccjit-debuginfo | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libgccjit-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libgccjit-docs | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libitm-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-liblsan-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libstdc++-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libstdc++-docs | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libtsan-devel | 12.2.1-7.7.el9_4 | |
gcc-toolset-12-libubsan-devel | 12.2.1-7.7.el9_4 | |
NetworkManager-cloud-setup | 1.46.0-19.el9_4 | |
NetworkManager-cloud-setup-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-config-connectivity-redhat | 1.46.0-19.el9_4 | |
NetworkManager-dispatcher-routing-rules | 1.46.0-19.el9_4 | |
NetworkManager-ovs | 1.46.0-19.el9_4 | |
NetworkManager-ovs-debuginfo | 1.46.0-19.el9_4 | |
NetworkManager-ppp | 1.46.0-19.el9_4 | |
NetworkManager-ppp-debuginfo | 1.46.0-19.el9_4 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
augeas-devel | 1.13.0-6.el9_4 | |
flatpak-devel | 1.12.9-3.el9_4 | [ALSA-2024:6356](https://errata.almalinux.org/9/ALSA-2024-6356.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-42472](https://access.redhat.com/security/cve/CVE-2024-42472))
NetworkManager-libnm-devel | 1.46.0-19.el9_4 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
augeas-static | 1.13.0-6.el9_4 | |
flatpak-tests | 1.12.9-3.el9_4 | |
flatpak-tests-debuginfo | 1.12.9-3.el9_4 | |
libasan8 | 12.2.1-7.7.el9_4 | |
libasan8-debuginfo | 12.2.1-7.7.el9_4 | |
libtsan2 | 12.2.1-7.7.el9_4 | |
libtsan2-debuginfo | 12.2.1-7.7.el9_4 | |

