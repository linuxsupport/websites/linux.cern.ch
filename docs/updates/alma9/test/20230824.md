## 2023-08-24

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-release | 9-5.al9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libdnf-plugin-subscription-manager | 1.29.33.1-2.el9_2.alma.1 | [ALSA-2023:4708](https://errata.almalinux.org/9/ALSA-2023-4708.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
python3-cloud-what | 1.29.33.1-2.el9_2.alma.1 | [ALSA-2023:4708](https://errata.almalinux.org/9/ALSA-2023-4708.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
python3-subscription-manager-rhsm | 1.29.33.1-2.el9_2.alma.1 | [ALSA-2023:4708](https://errata.almalinux.org/9/ALSA-2023-4708.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
subscription-manager | 1.29.33.1-2.el9_2.alma.1 | [ALSA-2023:4708](https://errata.almalinux.org/9/ALSA-2023-4708.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
subscription-manager-plugin-ostree | 1.29.33.1-2.el9_2.alma.1 | [ALSA-2023:4708](https://errata.almalinux.org/9/ALSA-2023-4708.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-release | 9-5.el9 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-release | 9-5.al9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libdnf-plugin-subscription-manager | 1.29.33.1-2.el9_2.alma.1 | [ALSA-2023:4708](https://errata.almalinux.org/9/ALSA-2023-4708.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
python3-cloud-what | 1.29.33.1-2.el9_2.alma.1 | [ALSA-2023:4708](https://errata.almalinux.org/9/ALSA-2023-4708.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
python3-subscription-manager-rhsm | 1.29.33.1-2.el9_2.alma.1 | [ALSA-2023:4708](https://errata.almalinux.org/9/ALSA-2023-4708.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
subscription-manager | 1.29.33.1-2.el9_2.alma.1 | [ALSA-2023:4708](https://errata.almalinux.org/9/ALSA-2023-4708.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
subscription-manager-plugin-ostree | 1.29.33.1-2.el9_2.alma.1 | [ALSA-2023:4708](https://errata.almalinux.org/9/ALSA-2023-4708.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-release | 9-5.el9 | |

