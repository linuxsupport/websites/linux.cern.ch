## 2023-07-03

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.5.4-1.el9.alma | |
sos-audit | 4.5.4-1.el9.alma | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.5.4-1.el9.alma | |
sos-audit | 4.5.4-1.el9.alma | |

