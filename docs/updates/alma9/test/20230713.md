## 2023-07-13

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
locmap-release | 1.0-9.al9.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grafana | 9.0.9-3.el9_2.alma | |
grafana-debuginfo | 9.0.9-3.el9_2.alma | |
grafana-debugsource | 9.0.9-3.el9_2.alma | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
locmap-release | 1.0-9.al9.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grafana | 9.0.9-3.el9_2.alma | |
grafana-debuginfo | 9.0.9-3.el9_2.alma | |
grafana-debugsource | 9.0.9-3.el9_2.alma | |

