## 2024-04-04

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.5.14.0_362.24.2.el9_3.al9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.2.0-362.24.2.el9_3 | |
bpftool-debuginfo | 7.2.0-362.24.2.el9_3 | |
expat | 2.5.0-1.el9_3.1 | [ALSA-2024:1530](https://errata.almalinux.org/9/ALSA-2024-1530.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-52425](https://access.redhat.com/security/cve/CVE-2023-52425), [CVE-2024-28757](https://access.redhat.com/security/cve/CVE-2024-28757))
expat-debuginfo | 2.5.0-1.el9_3.1 | |
expat-debugsource | 2.5.0-1.el9_3.1 | |
kernel | 5.14.0-362.24.2.el9_3 | |
kernel-abi-stablelists | 5.14.0-362.24.2.el9_3 | |
kernel-core | 5.14.0-362.24.2.el9_3 | |
kernel-debug | 5.14.0-362.24.2.el9_3 | |
kernel-debug-core | 5.14.0-362.24.2.el9_3 | |
kernel-debug-debuginfo | 5.14.0-362.24.2.el9_3 | |
kernel-debug-modules | 5.14.0-362.24.2.el9_3 | |
kernel-debug-modules-core | 5.14.0-362.24.2.el9_3 | |
kernel-debug-modules-extra | 5.14.0-362.24.2.el9_3 | |
kernel-debug-uki-virt | 5.14.0-362.24.2.el9_3 | |
kernel-debuginfo | 5.14.0-362.24.2.el9_3 | |
kernel-debuginfo-common-x86_64 | 5.14.0-362.24.2.el9_3 | |
kernel-modules | 5.14.0-362.24.2.el9_3 | |
kernel-modules-core | 5.14.0-362.24.2.el9_3 | |
kernel-modules-extra | 5.14.0-362.24.2.el9_3 | |
kernel-tools | 5.14.0-362.24.2.el9_3 | |
kernel-tools-debuginfo | 5.14.0-362.24.2.el9_3 | |
kernel-tools-libs | 5.14.0-362.24.2.el9_3 | |
kernel-uki-virt | 5.14.0-362.24.2.el9_3 | |
python3-perf | 5.14.0-362.24.2.el9_3 | |
python3-perf-debuginfo | 5.14.0-362.24.2.el9_3 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
expat-devel | 2.5.0-1.el9_3.1 | [ALSA-2024:1530](https://errata.almalinux.org/9/ALSA-2024-1530.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-52425](https://access.redhat.com/security/cve/CVE-2023-52425), [CVE-2024-28757](https://access.redhat.com/security/cve/CVE-2024-28757))
firefox | 115.9.1-1.el9_3.alma.1 | [ALSA-2024:1485](https://errata.almalinux.org/9/ALSA-2024-1485.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388), [CVE-2024-0743](https://access.redhat.com/security/cve/CVE-2024-0743), [CVE-2024-2607](https://access.redhat.com/security/cve/CVE-2024-2607), [CVE-2024-2608](https://access.redhat.com/security/cve/CVE-2024-2608), [CVE-2024-2610](https://access.redhat.com/security/cve/CVE-2024-2610), [CVE-2024-2611](https://access.redhat.com/security/cve/CVE-2024-2611), [CVE-2024-2612](https://access.redhat.com/security/cve/CVE-2024-2612), [CVE-2024-2614](https://access.redhat.com/security/cve/CVE-2024-2614), [CVE-2024-2616](https://access.redhat.com/security/cve/CVE-2024-2616), [CVE-2024-29944](https://access.redhat.com/security/cve/CVE-2024-29944))
firefox-debuginfo | 115.9.1-1.el9_3.alma.1 | |
firefox-debugsource | 115.9.1-1.el9_3.alma.1 | |
firefox-x11 | 115.9.1-1.el9_3.alma.1 | [ALSA-2024:1485](https://errata.almalinux.org/9/ALSA-2024-1485.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388), [CVE-2024-0743](https://access.redhat.com/security/cve/CVE-2024-0743), [CVE-2024-2607](https://access.redhat.com/security/cve/CVE-2024-2607), [CVE-2024-2608](https://access.redhat.com/security/cve/CVE-2024-2608), [CVE-2024-2610](https://access.redhat.com/security/cve/CVE-2024-2610), [CVE-2024-2611](https://access.redhat.com/security/cve/CVE-2024-2611), [CVE-2024-2612](https://access.redhat.com/security/cve/CVE-2024-2612), [CVE-2024-2614](https://access.redhat.com/security/cve/CVE-2024-2614), [CVE-2024-2616](https://access.redhat.com/security/cve/CVE-2024-2616), [CVE-2024-29944](https://access.redhat.com/security/cve/CVE-2024-29944))
grafana | 9.2.10-8.el9_3.alma.1 | [ALSA-2024:1501](https://errata.almalinux.org/9/ALSA-2024-1501.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))
grafana-debuginfo | 9.2.10-8.el9_3.alma.1 | |
grafana-debugsource | 9.2.10-8.el9_3.alma.1 | |
grafana-pcp | 5.1.1-2.el9_3.alma.1 | [ALSA-2024:1502](https://errata.almalinux.org/9/ALSA-2024-1502.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))
grafana-pcp-debuginfo | 5.1.1-2.el9_3.alma.1 | |
grafana-pcp-debugsource | 5.1.1-2.el9_3.alma.1 | |
kernel-debug-devel | 5.14.0-362.24.2.el9_3 | |
kernel-debug-devel-matched | 5.14.0-362.24.2.el9_3 | |
kernel-devel | 5.14.0-362.24.2.el9_3 | |
kernel-devel-matched | 5.14.0-362.24.2.el9_3 | |
kernel-doc | 5.14.0-362.24.2.el9_3 | |
kernel-headers | 5.14.0-362.24.2.el9_3 | |
nodejs | 18.19.1-1.module_el9.3.0+59+28b95644 | |
nodejs-debuginfo | 18.19.1-1.module_el9.3.0+59+28b95644 | |
nodejs-debugsource | 18.19.1-1.module_el9.3.0+59+28b95644 | |
nodejs-devel | 18.19.1-1.module_el9.3.0+59+28b95644 | |
nodejs-docs | 18.19.1-1.module_el9.3.0+59+28b95644 | |
nodejs-full-i18n | 18.19.1-1.module_el9.3.0+59+28b95644 | |
npm | 10.2.4-1.18.19.1.1.module_el9.3.0+59+28b95644 | |
perf | 5.14.0-362.24.2.el9_3 | |
perf-debuginfo | 5.14.0-362.24.2.el9_3 | |
rtla | 5.14.0-362.24.2.el9_3 | |
ruby | 3.1.4-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
ruby-bundled-gems | 3.1.4-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
ruby-bundled-gems-debuginfo | 3.1.4-143.module_el9.3.0+60+5ebc989a | |
ruby-debuginfo | 3.1.4-143.module_el9.3.0+60+5ebc989a | |
ruby-debugsource | 3.1.4-143.module_el9.3.0+60+5ebc989a | |
ruby-default-gems | 3.1.4-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
ruby-devel | 3.1.4-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
ruby-doc | 3.1.4-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
ruby-libs | 3.1.4-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
ruby-libs-debuginfo | 3.1.4-143.module_el9.3.0+60+5ebc989a | |
rubygem-bigdecimal | 3.1.1-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-bigdecimal-debuginfo | 3.1.1-143.module_el9.3.0+60+5ebc989a | |
rubygem-bundler | 2.3.26-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-io-console | 0.5.11-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-io-console-debuginfo | 0.5.11-143.module_el9.3.0+60+5ebc989a | |
rubygem-irb | 1.4.1-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-json | 2.6.1-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-json-debuginfo | 2.6.1-143.module_el9.3.0+60+5ebc989a | |
rubygem-minitest | 5.15.0-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-power_assert | 2.0.1-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-psych | 4.0.4-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-psych-debuginfo | 4.0.4-143.module_el9.3.0+60+5ebc989a | |
rubygem-rake | 13.0.6-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-rbs | 2.7.0-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-rbs-debuginfo | 2.7.0-143.module_el9.3.0+60+5ebc989a | |
rubygem-rdoc | 6.4.0-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-rexml | 3.2.5-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-rss | 0.2.9-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-test-unit | 3.5.3-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-typeprof | 0.21.3-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygems | 3.3.26-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygems-devel | 3.3.26-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rv | 5.14.0-362.24.2.el9_3 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-362.24.2.el9_3 | |
kernel-rt-core | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-core | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-debuginfo | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-devel | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-modules | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-modules-core | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-modules-extra | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debuginfo | 5.14.0-362.24.2.el9_3 | |
kernel-rt-devel | 5.14.0-362.24.2.el9_3 | |
kernel-rt-modules | 5.14.0-362.24.2.el9_3 | |
kernel-rt-modules-core | 5.14.0-362.24.2.el9_3 | |
kernel-rt-modules-extra | 5.14.0-362.24.2.el9_3 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-cross-headers | 5.14.0-362.24.2.el9_3 | |
kernel-tools-libs-devel | 5.14.0-362.24.2.el9_3 | |
libperf | 5.14.0-362.24.2.el9_3 | |
libperf-debuginfo | 5.14.0-362.24.2.el9_3 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-362.24.2.el9_3 | |
kernel-rt-core | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-core | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-debuginfo | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-devel | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-kvm | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-modules | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-modules-core | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-modules-extra | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debuginfo | 5.14.0-362.24.2.el9_3 | |
kernel-rt-devel | 5.14.0-362.24.2.el9_3 | |
kernel-rt-kvm | 5.14.0-362.24.2.el9_3 | |
kernel-rt-modules | 5.14.0-362.24.2.el9_3 | |
kernel-rt-modules-core | 5.14.0-362.24.2.el9_3 | |
kernel-rt-modules-extra | 5.14.0-362.24.2.el9_3 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
expat-static | 2.5.0-1.el9_3.1 | |
kernel-debug-modules-internal | 5.14.0-362.24.2.el9_3 | |
kernel-debug-modules-partner | 5.14.0-362.24.2.el9_3 | |
kernel-ipaclones-internal | 5.14.0-362.24.2.el9_3 | |
kernel-modules-internal | 5.14.0-362.24.2.el9_3 | |
kernel-modules-partner | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-devel-matched | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-modules-internal | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-modules-partner | 5.14.0-362.24.2.el9_3 | |
kernel-rt-devel-matched | 5.14.0-362.24.2.el9_3 | |
kernel-rt-modules-internal | 5.14.0-362.24.2.el9_3 | |
kernel-rt-modules-partner | 5.14.0-362.24.2.el9_3 | |
kernel-selftests-internal | 5.14.0-362.24.2.el9_3 | |
libperf-devel | 5.14.0-362.24.2.el9_3 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.5.14.0_362.24.2.el9_3.al9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.2.0-362.24.2.el9_3 | |
bpftool-debuginfo | 7.2.0-362.24.2.el9_3 | |
expat | 2.5.0-1.el9_3.1 | [ALSA-2024:1530](https://errata.almalinux.org/9/ALSA-2024-1530.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-52425](https://access.redhat.com/security/cve/CVE-2023-52425), [CVE-2024-28757](https://access.redhat.com/security/cve/CVE-2024-28757))
expat-debuginfo | 2.5.0-1.el9_3.1 | |
expat-debugsource | 2.5.0-1.el9_3.1 | |
kernel | 5.14.0-362.24.2.el9_3 | |
kernel-64k | 5.14.0-362.24.2.el9_3 | |
kernel-64k-core | 5.14.0-362.24.2.el9_3 | |
kernel-64k-debug | 5.14.0-362.24.2.el9_3 | |
kernel-64k-debug-core | 5.14.0-362.24.2.el9_3 | |
kernel-64k-debug-debuginfo | 5.14.0-362.24.2.el9_3 | |
kernel-64k-debug-modules | 5.14.0-362.24.2.el9_3 | |
kernel-64k-debug-modules-core | 5.14.0-362.24.2.el9_3 | |
kernel-64k-debug-modules-extra | 5.14.0-362.24.2.el9_3 | |
kernel-64k-debuginfo | 5.14.0-362.24.2.el9_3 | |
kernel-64k-modules | 5.14.0-362.24.2.el9_3 | |
kernel-64k-modules-core | 5.14.0-362.24.2.el9_3 | |
kernel-64k-modules-extra | 5.14.0-362.24.2.el9_3 | |
kernel-abi-stablelists | 5.14.0-362.24.2.el9_3 | |
kernel-core | 5.14.0-362.24.2.el9_3 | |
kernel-debug | 5.14.0-362.24.2.el9_3 | |
kernel-debug-core | 5.14.0-362.24.2.el9_3 | |
kernel-debug-debuginfo | 5.14.0-362.24.2.el9_3 | |
kernel-debug-modules | 5.14.0-362.24.2.el9_3 | |
kernel-debug-modules-core | 5.14.0-362.24.2.el9_3 | |
kernel-debug-modules-extra | 5.14.0-362.24.2.el9_3 | |
kernel-debuginfo | 5.14.0-362.24.2.el9_3 | |
kernel-debuginfo-common-aarch64 | 5.14.0-362.24.2.el9_3 | |
kernel-modules | 5.14.0-362.24.2.el9_3 | |
kernel-modules-core | 5.14.0-362.24.2.el9_3 | |
kernel-modules-extra | 5.14.0-362.24.2.el9_3 | |
kernel-tools | 5.14.0-362.24.2.el9_3 | |
kernel-tools-debuginfo | 5.14.0-362.24.2.el9_3 | |
kernel-tools-libs | 5.14.0-362.24.2.el9_3 | |
python3-perf | 5.14.0-362.24.2.el9_3 | |
python3-perf-debuginfo | 5.14.0-362.24.2.el9_3 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
expat-devel | 2.5.0-1.el9_3.1 | [ALSA-2024:1530](https://errata.almalinux.org/9/ALSA-2024-1530.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-52425](https://access.redhat.com/security/cve/CVE-2023-52425), [CVE-2024-28757](https://access.redhat.com/security/cve/CVE-2024-28757))
firefox | 115.9.1-1.el9_3.alma.1 | [ALSA-2024:1485](https://errata.almalinux.org/9/ALSA-2024-1485.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388), [CVE-2024-0743](https://access.redhat.com/security/cve/CVE-2024-0743), [CVE-2024-2607](https://access.redhat.com/security/cve/CVE-2024-2607), [CVE-2024-2608](https://access.redhat.com/security/cve/CVE-2024-2608), [CVE-2024-2610](https://access.redhat.com/security/cve/CVE-2024-2610), [CVE-2024-2611](https://access.redhat.com/security/cve/CVE-2024-2611), [CVE-2024-2612](https://access.redhat.com/security/cve/CVE-2024-2612), [CVE-2024-2614](https://access.redhat.com/security/cve/CVE-2024-2614), [CVE-2024-2616](https://access.redhat.com/security/cve/CVE-2024-2616), [CVE-2024-29944](https://access.redhat.com/security/cve/CVE-2024-29944))
firefox-debuginfo | 115.9.1-1.el9_3.alma.1 | |
firefox-debugsource | 115.9.1-1.el9_3.alma.1 | |
firefox-x11 | 115.9.1-1.el9_3.alma.1 | [ALSA-2024:1485](https://errata.almalinux.org/9/ALSA-2024-1485.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388), [CVE-2024-0743](https://access.redhat.com/security/cve/CVE-2024-0743), [CVE-2024-2607](https://access.redhat.com/security/cve/CVE-2024-2607), [CVE-2024-2608](https://access.redhat.com/security/cve/CVE-2024-2608), [CVE-2024-2610](https://access.redhat.com/security/cve/CVE-2024-2610), [CVE-2024-2611](https://access.redhat.com/security/cve/CVE-2024-2611), [CVE-2024-2612](https://access.redhat.com/security/cve/CVE-2024-2612), [CVE-2024-2614](https://access.redhat.com/security/cve/CVE-2024-2614), [CVE-2024-2616](https://access.redhat.com/security/cve/CVE-2024-2616), [CVE-2024-29944](https://access.redhat.com/security/cve/CVE-2024-29944))
grafana | 9.2.10-8.el9_3.alma.1 | [ALSA-2024:1501](https://errata.almalinux.org/9/ALSA-2024-1501.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))
grafana-debuginfo | 9.2.10-8.el9_3.alma.1 | |
grafana-debugsource | 9.2.10-8.el9_3.alma.1 | |
grafana-pcp | 5.1.1-2.el9_3.alma.1 | [ALSA-2024:1502](https://errata.almalinux.org/9/ALSA-2024-1502.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))
grafana-pcp-debuginfo | 5.1.1-2.el9_3.alma.1 | |
grafana-pcp-debugsource | 5.1.1-2.el9_3.alma.1 | |
kernel-64k-debug-devel | 5.14.0-362.24.2.el9_3 | |
kernel-64k-debug-devel-matched | 5.14.0-362.24.2.el9_3 | |
kernel-64k-devel | 5.14.0-362.24.2.el9_3 | |
kernel-64k-devel-matched | 5.14.0-362.24.2.el9_3 | |
kernel-debug-devel | 5.14.0-362.24.2.el9_3 | |
kernel-debug-devel-matched | 5.14.0-362.24.2.el9_3 | |
kernel-devel | 5.14.0-362.24.2.el9_3 | |
kernel-devel-matched | 5.14.0-362.24.2.el9_3 | |
kernel-doc | 5.14.0-362.24.2.el9_3 | |
kernel-headers | 5.14.0-362.24.2.el9_3 | |
nodejs | 18.19.1-1.module_el9.3.0+59+28b95644 | |
nodejs-debuginfo | 18.19.1-1.module_el9.3.0+59+28b95644 | |
nodejs-debugsource | 18.19.1-1.module_el9.3.0+59+28b95644 | |
nodejs-devel | 18.19.1-1.module_el9.3.0+59+28b95644 | |
nodejs-docs | 18.19.1-1.module_el9.3.0+59+28b95644 | |
nodejs-full-i18n | 18.19.1-1.module_el9.3.0+59+28b95644 | |
npm | 10.2.4-1.18.19.1.1.module_el9.3.0+59+28b95644 | |
perf | 5.14.0-362.24.2.el9_3 | |
perf-debuginfo | 5.14.0-362.24.2.el9_3 | |
rtla | 5.14.0-362.24.2.el9_3 | |
ruby | 3.1.4-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
ruby-bundled-gems | 3.1.4-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
ruby-bundled-gems-debuginfo | 3.1.4-143.module_el9.3.0+60+5ebc989a | |
ruby-debuginfo | 3.1.4-143.module_el9.3.0+60+5ebc989a | |
ruby-debugsource | 3.1.4-143.module_el9.3.0+60+5ebc989a | |
ruby-default-gems | 3.1.4-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
ruby-devel | 3.1.4-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
ruby-doc | 3.1.4-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
ruby-libs | 3.1.4-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
ruby-libs-debuginfo | 3.1.4-143.module_el9.3.0+60+5ebc989a | |
rubygem-bigdecimal | 3.1.1-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-bigdecimal-debuginfo | 3.1.1-143.module_el9.3.0+60+5ebc989a | |
rubygem-bundler | 2.3.26-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-io-console | 0.5.11-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-io-console-debuginfo | 0.5.11-143.module_el9.3.0+60+5ebc989a | |
rubygem-irb | 1.4.1-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-json | 2.6.1-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-json-debuginfo | 2.6.1-143.module_el9.3.0+60+5ebc989a | |
rubygem-minitest | 5.15.0-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-power_assert | 2.0.1-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-psych | 4.0.4-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-psych-debuginfo | 4.0.4-143.module_el9.3.0+60+5ebc989a | |
rubygem-rake | 13.0.6-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-rbs | 2.7.0-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-rbs-debuginfo | 2.7.0-143.module_el9.3.0+60+5ebc989a | |
rubygem-rdoc | 6.4.0-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-rexml | 3.2.5-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-rss | 0.2.9-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-test-unit | 3.5.3-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygem-typeprof | 0.21.3-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygems | 3.3.26-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rubygems-devel | 3.3.26-143.module_el9.3.0+60+5ebc989a | [ALSA-2024:1576](https://errata.almalinux.org/9/ALSA-2024-1576.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-33621](https://access.redhat.com/security/cve/CVE-2021-33621), [CVE-2023-28755](https://access.redhat.com/security/cve/CVE-2023-28755), [CVE-2023-28756](https://access.redhat.com/security/cve/CVE-2023-28756), [CVE-2023-36617](https://access.redhat.com/security/cve/CVE-2023-36617))
rv | 5.14.0-362.24.2.el9_3 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-cross-headers | 5.14.0-362.24.2.el9_3 | |
kernel-tools-libs-devel | 5.14.0-362.24.2.el9_3 | |
libperf | 5.14.0-362.24.2.el9_3 | |
libperf-debuginfo | 5.14.0-362.24.2.el9_3 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
expat-static | 2.5.0-1.el9_3.1 | |
kernel-64k-debug-modules-internal | 5.14.0-362.24.2.el9_3 | |
kernel-64k-debug-modules-partner | 5.14.0-362.24.2.el9_3 | |
kernel-64k-modules-internal | 5.14.0-362.24.2.el9_3 | |
kernel-64k-modules-partner | 5.14.0-362.24.2.el9_3 | |
kernel-debug-modules-internal | 5.14.0-362.24.2.el9_3 | |
kernel-debug-modules-partner | 5.14.0-362.24.2.el9_3 | |
kernel-modules-internal | 5.14.0-362.24.2.el9_3 | |
kernel-modules-partner | 5.14.0-362.24.2.el9_3 | |
kernel-rt | 5.14.0-362.24.2.el9_3 | |
kernel-rt-core | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-core | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-debuginfo | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-devel | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-devel-matched | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-kvm | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-modules | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-modules-core | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-modules-extra | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-modules-internal | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debug-modules-partner | 5.14.0-362.24.2.el9_3 | |
kernel-rt-debuginfo | 5.14.0-362.24.2.el9_3 | |
kernel-rt-devel | 5.14.0-362.24.2.el9_3 | |
kernel-rt-devel-matched | 5.14.0-362.24.2.el9_3 | |
kernel-rt-kvm | 5.14.0-362.24.2.el9_3 | |
kernel-rt-modules | 5.14.0-362.24.2.el9_3 | |
kernel-rt-modules-core | 5.14.0-362.24.2.el9_3 | |
kernel-rt-modules-extra | 5.14.0-362.24.2.el9_3 | |
kernel-rt-modules-internal | 5.14.0-362.24.2.el9_3 | |
kernel-rt-modules-partner | 5.14.0-362.24.2.el9_3 | |
kernel-selftests-internal | 5.14.0-362.24.2.el9_3 | |
libperf-devel | 5.14.0-362.24.2.el9_3 | |

