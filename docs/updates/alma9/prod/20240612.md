## 2024-06-12

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
less | 590-4.el9_4 | [ALSA-2024:3513](https://errata.almalinux.org/9/ALSA-2024-3513.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32487](https://access.redhat.com/security/cve/CVE-2024-32487))
less-debuginfo | 590-4.el9_4 | |
less-debugsource | 590-4.el9_4 | |
libnghttp2 | 1.43.0-5.el9_4.3 | [ALSA-2024:3501](https://errata.almalinux.org/9/ALSA-2024-3501.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
libnghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
nghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
nghttp2-debugsource | 1.43.0-5.el9_4.3 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libnghttp2-devel | 1.43.0-5.el9_4.3 | [ALSA-2024:3501](https://errata.almalinux.org/9/ALSA-2024-3501.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nghttp2 | 1.43.0-5.el9_4.3 | [ALSA-2024:3501](https://errata.almalinux.org/9/ALSA-2024-3501.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-gluster11 | 1.0-1.el9 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
less | 590-4.el9_4 | [ALSA-2024:3513](https://errata.almalinux.org/9/ALSA-2024-3513.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32487](https://access.redhat.com/security/cve/CVE-2024-32487))
less-debuginfo | 590-4.el9_4 | |
less-debugsource | 590-4.el9_4 | |
libnghttp2 | 1.43.0-5.el9_4.3 | [ALSA-2024:3501](https://errata.almalinux.org/9/ALSA-2024-3501.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
libnghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
nghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
nghttp2-debugsource | 1.43.0-5.el9_4.3 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libnghttp2-devel | 1.43.0-5.el9_4.3 | [ALSA-2024:3501](https://errata.almalinux.org/9/ALSA-2024-3501.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nghttp2 | 1.43.0-5.el9_4.3 | [ALSA-2024:3501](https://errata.almalinux.org/9/ALSA-2024-3501.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-gluster11 | 1.0-1.el9 | |

