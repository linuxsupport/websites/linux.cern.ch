## 2022-12-08

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 18.12.1-1.module_el9.1.0+16+91bc168f | |
nodejs-debuginfo | 18.12.1-1.module_el9.1.0+16+91bc168f | |
nodejs-debugsource | 18.12.1-1.module_el9.1.0+16+91bc168f | |
nodejs-devel | 18.12.1-1.module_el9.1.0+16+91bc168f | |
nodejs-docs | 18.12.1-1.module_el9.1.0+16+91bc168f | |
nodejs-full-i18n | 18.12.1-1.module_el9.1.0+16+91bc168f | |
nodejs-nodemon | 2.0.20-1.module_el9.1.0+16+91bc168f | |
npm | 8.19.2-1.18.12.1.1.module_el9.1.0+16+91bc168f | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-mock | 3.0.5-17.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 18.12.1-1.module_el9.1.0+16+91bc168f | |
nodejs-debuginfo | 18.12.1-1.module_el9.1.0+16+91bc168f | |
nodejs-debugsource | 18.12.1-1.module_el9.1.0+16+91bc168f | |
nodejs-devel | 18.12.1-1.module_el9.1.0+16+91bc168f | |
nodejs-docs | 18.12.1-1.module_el9.1.0+16+91bc168f | |
nodejs-full-i18n | 18.12.1-1.module_el9.1.0+16+91bc168f | |
nodejs-nodemon | 2.0.20-1.module_el9.1.0+16+91bc168f | |
npm | 8.19.2-1.18.12.1.1.module_el9.1.0+16+91bc168f | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-mock | 3.0.5-17.el9 | |

