## 2024-11-20

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20230808-2.20240910.1.el9_4 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-aarch64 | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-ovmf | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
podman | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-debuginfo | 4.9.4-16.el9_4 | |
podman-debugsource | 4.9.4-16.el9_4 | |
podman-docker | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-plugins | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-plugins-debuginfo | 4.9.4-16.el9_4 | |
podman-remote | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-remote-debuginfo | 4.9.4-16.el9_4 | |
podman-tests | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-aarch64 | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-debugsource | 20231122-6.el9_4.4 | |
edk2-ovmf | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-tools | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-tools-debuginfo | 20231122-6.el9_4.4 | |
edk2-tools-doc | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20230808-2.20240910.1.el9_4 | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-openstack-dalmatian | 1-1.el9 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20230808-2.20240910.1.el9_4 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-aarch64 | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-ovmf | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
podman | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-debuginfo | 4.9.4-16.el9_4 | |
podman-debugsource | 4.9.4-16.el9_4 | |
podman-docker | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-plugins | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-plugins-debuginfo | 4.9.4-16.el9_4 | |
podman-remote | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))
podman-remote-debuginfo | 4.9.4-16.el9_4 | |
podman-tests | 4.9.4-16.el9_4 | [ALSA-2024:9051](https://errata.almalinux.org/9/ALSA-2024-9051.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9407](https://access.redhat.com/security/cve/CVE-2024-9407), [CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675), [CVE-2024-9676](https://access.redhat.com/security/cve/CVE-2024-9676))

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-aarch64 | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-debugsource | 20231122-6.el9_4.4 | |
edk2-ovmf | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-tools | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-tools-debuginfo | 20231122-6.el9_4.4 | |
edk2-tools-doc | 20231122-6.el9_4.4 | [ALSA-2024:8935](https://errata.almalinux.org/9/ALSA-2024-8935.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20230808-2.20240910.1.el9_4 | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-openstack-dalmatian | 1-1.el9 | |

