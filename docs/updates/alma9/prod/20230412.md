## 2023-04-12

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.1-1.al9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2023c-1.el9 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tigervnc | 1.12.0-5.el9_1.2 | |
tigervnc-debuginfo | 1.12.0-5.el9_1.2 | |
tigervnc-debugsource | 1.12.0-5.el9_1.2 | |
tigervnc-icons | 1.12.0-5.el9_1.2 | |
tigervnc-license | 1.12.0-5.el9_1.2 | |
tigervnc-selinux | 1.12.0-5.el9_1.2 | |
tigervnc-server | 1.12.0-5.el9_1.2 | |
tigervnc-server-debuginfo | 1.12.0-5.el9_1.2 | |
tigervnc-server-minimal | 1.12.0-5.el9_1.2 | |
tigervnc-server-minimal-debuginfo | 1.12.0-5.el9_1.2 | |
tigervnc-server-module | 1.12.0-5.el9_1.2 | |
tigervnc-server-module-debuginfo | 1.12.0-5.el9_1.2 | |
tzdata-java | 2023c-1.el9 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pcs | 0.11.3-4.el9_1.3 | |
pcs-snmp | 0.11.3-4.el9_1.3 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pcs | 0.11.3-4.el9_1.3 | |
pcs-snmp | 0.11.3-4.el9_1.3 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.1-1.al9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2023c-1.el9 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tigervnc | 1.12.0-5.el9_1.2 | |
tigervnc-debuginfo | 1.12.0-5.el9_1.2 | |
tigervnc-debugsource | 1.12.0-5.el9_1.2 | |
tigervnc-icons | 1.12.0-5.el9_1.2 | |
tigervnc-license | 1.12.0-5.el9_1.2 | |
tigervnc-selinux | 1.12.0-5.el9_1.2 | |
tigervnc-server | 1.12.0-5.el9_1.2 | |
tigervnc-server-debuginfo | 1.12.0-5.el9_1.2 | |
tigervnc-server-minimal | 1.12.0-5.el9_1.2 | |
tigervnc-server-minimal-debuginfo | 1.12.0-5.el9_1.2 | |
tigervnc-server-module | 1.12.0-5.el9_1.2 | |
tigervnc-server-module-debuginfo | 1.12.0-5.el9_1.2 | |
tzdata-java | 2023c-1.el9 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pcs | 0.11.3-4.el9_1.3 | |
pcs-snmp | 0.11.3-4.el9_1.3 | |

