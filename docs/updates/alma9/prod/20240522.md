## 2024-05-22

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.5.14.0_427.16.1.el9_4.al9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.3.0-427.16.1.el9_4 | |
bpftool-debuginfo | 7.3.0-427.16.1.el9_4 | |
kernel | 5.14.0-427.16.1.el9_4 | |
kernel-abi-stablelists | 5.14.0-427.16.1.el9_4 | |
kernel-core | 5.14.0-427.16.1.el9_4 | |
kernel-debug | 5.14.0-427.16.1.el9_4 | |
kernel-debug-core | 5.14.0-427.16.1.el9_4 | |
kernel-debug-debuginfo | 5.14.0-427.16.1.el9_4 | |
kernel-debug-modules | 5.14.0-427.16.1.el9_4 | |
kernel-debug-modules-core | 5.14.0-427.16.1.el9_4 | |
kernel-debug-modules-extra | 5.14.0-427.16.1.el9_4 | |
kernel-debug-uki-virt | 5.14.0-427.16.1.el9_4 | |
kernel-debuginfo | 5.14.0-427.16.1.el9_4 | |
kernel-debuginfo-common-x86_64 | 5.14.0-427.16.1.el9_4 | |
kernel-modules | 5.14.0-427.16.1.el9_4 | |
kernel-modules-core | 5.14.0-427.16.1.el9_4 | |
kernel-modules-extra | 5.14.0-427.16.1.el9_4 | |
kernel-tools | 5.14.0-427.16.1.el9_4 | |
kernel-tools-debuginfo | 5.14.0-427.16.1.el9_4 | |
kernel-tools-libs | 5.14.0-427.16.1.el9_4 | |
kernel-uki-virt | 5.14.0-427.16.1.el9_4 | |
python3-perf | 5.14.0-427.16.1.el9_4 | |
python3-perf-debuginfo | 5.14.0-427.16.1.el9_4 | |
shim-x64 | 15.8-4.el9_3.alma.1 | [ALSA-2024:1903](https://errata.almalinux.org/9/ALSA-2024-1903.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40546](https://access.redhat.com/security/cve/CVE-2023-40546), [CVE-2023-40547](https://access.redhat.com/security/cve/CVE-2023-40547), [CVE-2023-40548](https://access.redhat.com/security/cve/CVE-2023-40548), [CVE-2023-40549](https://access.redhat.com/security/cve/CVE-2023-40549), [CVE-2023-40550](https://access.redhat.com/security/cve/CVE-2023-40550), [CVE-2023-40551](https://access.redhat.com/security/cve/CVE-2023-40551))

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gdm | 40.1-23.el9.alma.1 | |
gdm-debuginfo | 40.1-23.el9.alma.1 | |
gdm-debugsource | 40.1-23.el9.alma.1 | |
git-lfs | 3.4.1-2.el9_4 | [ALSA-2024:2724](https://errata.almalinux.org/9/ALSA-2024-2724.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-45288](https://access.redhat.com/security/cve/CVE-2023-45288), [CVE-2023-45289](https://access.redhat.com/security/cve/CVE-2023-45289), [CVE-2023-45290](https://access.redhat.com/security/cve/CVE-2023-45290), [CVE-2024-24783](https://access.redhat.com/security/cve/CVE-2024-24783))
git-lfs-debuginfo | 3.4.1-2.el9_4 | |
git-lfs-debugsource | 3.4.1-2.el9_4 | |
kernel-debug-devel | 5.14.0-427.16.1.el9_4 | |
kernel-debug-devel-matched | 5.14.0-427.16.1.el9_4 | |
kernel-devel | 5.14.0-427.16.1.el9_4 | |
kernel-devel-matched | 5.14.0-427.16.1.el9_4 | |
kernel-doc | 5.14.0-427.16.1.el9_4 | |
kernel-headers | 5.14.0-427.16.1.el9_4 | |
nodejs | 18.20.2-2.module_el9.4.0+99+a01f7676 | [ALSA-2024:2779](https://errata.almalinux.org/9/ALSA-2024-2779.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-debuginfo | 18.20.2-2.module_el9.4.0+99+a01f7676 | |
nodejs-debugsource | 18.20.2-2.module_el9.4.0+99+a01f7676 | |
nodejs-devel | 18.20.2-2.module_el9.4.0+99+a01f7676 | [ALSA-2024:2779](https://errata.almalinux.org/9/ALSA-2024-2779.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-docs | 18.20.2-2.module_el9.4.0+99+a01f7676 | [ALSA-2024:2779](https://errata.almalinux.org/9/ALSA-2024-2779.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-full-i18n | 18.20.2-2.module_el9.4.0+99+a01f7676 | [ALSA-2024:2779](https://errata.almalinux.org/9/ALSA-2024-2779.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
npm | 10.5.0-1.18.20.2.2.module_el9.4.0+99+a01f7676 | [ALSA-2024:2779](https://errata.almalinux.org/9/ALSA-2024-2779.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
perf | 5.14.0-427.16.1.el9_4 | |
perf-debuginfo | 5.14.0-427.16.1.el9_4 | |
rtla | 5.14.0-427.16.1.el9_4 | |
rv | 5.14.0-427.16.1.el9_4 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-427.16.1.el9_4 | |
kernel-rt-core | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-core | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-debuginfo | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-devel | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-modules | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-modules-core | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-modules-extra | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debuginfo | 5.14.0-427.16.1.el9_4 | |
kernel-rt-devel | 5.14.0-427.16.1.el9_4 | |
kernel-rt-modules | 5.14.0-427.16.1.el9_4 | |
kernel-rt-modules-core | 5.14.0-427.16.1.el9_4 | |
kernel-rt-modules-extra | 5.14.0-427.16.1.el9_4 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gdm-devel | 40.1-23.el9.alma.1 | |
gdm-pam-extensions-devel | 40.1-23.el9.alma.1 | |
kernel-cross-headers | 5.14.0-427.16.1.el9_4 | |
kernel-tools-libs-devel | 5.14.0-427.16.1.el9_4 | |
libperf | 5.14.0-427.16.1.el9_4 | |
libperf-debuginfo | 5.14.0-427.16.1.el9_4 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-427.16.1.el9_4 | |
kernel-rt-core | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-core | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-debuginfo | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-devel | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-kvm | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-modules | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-modules-core | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-modules-extra | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debuginfo | 5.14.0-427.16.1.el9_4 | |
kernel-rt-devel | 5.14.0-427.16.1.el9_4 | |
kernel-rt-kvm | 5.14.0-427.16.1.el9_4 | |
kernel-rt-modules | 5.14.0-427.16.1.el9_4 | |
kernel-rt-modules-core | 5.14.0-427.16.1.el9_4 | |
kernel-rt-modules-extra | 5.14.0-427.16.1.el9_4 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-debug-modules-internal | 5.14.0-427.16.1.el9_4 | |
kernel-debug-modules-partner | 5.14.0-427.16.1.el9_4 | |
kernel-ipaclones-internal | 5.14.0-427.16.1.el9_4 | |
kernel-modules-internal | 5.14.0-427.16.1.el9_4 | |
kernel-modules-partner | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-devel-matched | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-modules-internal | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-modules-partner | 5.14.0-427.16.1.el9_4 | |
kernel-rt-devel-matched | 5.14.0-427.16.1.el9_4 | |
kernel-rt-modules-internal | 5.14.0-427.16.1.el9_4 | |
kernel-rt-modules-partner | 5.14.0-427.16.1.el9_4 | |
kernel-selftests-internal | 5.14.0-427.16.1.el9_4 | |
libperf-devel | 5.14.0-427.16.1.el9_4 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.5.14.0_427.16.1.el9_4.al9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.3.0-427.16.1.el9_4 | |
bpftool-debuginfo | 7.3.0-427.16.1.el9_4 | |
kernel | 5.14.0-427.16.1.el9_4 | |
kernel-64k | 5.14.0-427.16.1.el9_4 | |
kernel-64k-core | 5.14.0-427.16.1.el9_4 | |
kernel-64k-debug | 5.14.0-427.16.1.el9_4 | |
kernel-64k-debug-core | 5.14.0-427.16.1.el9_4 | |
kernel-64k-debug-debuginfo | 5.14.0-427.16.1.el9_4 | |
kernel-64k-debug-modules | 5.14.0-427.16.1.el9_4 | |
kernel-64k-debug-modules-core | 5.14.0-427.16.1.el9_4 | |
kernel-64k-debug-modules-extra | 5.14.0-427.16.1.el9_4 | |
kernel-64k-debuginfo | 5.14.0-427.16.1.el9_4 | |
kernel-64k-modules | 5.14.0-427.16.1.el9_4 | |
kernel-64k-modules-core | 5.14.0-427.16.1.el9_4 | |
kernel-64k-modules-extra | 5.14.0-427.16.1.el9_4 | |
kernel-abi-stablelists | 5.14.0-427.16.1.el9_4 | |
kernel-core | 5.14.0-427.16.1.el9_4 | |
kernel-debug | 5.14.0-427.16.1.el9_4 | |
kernel-debug-core | 5.14.0-427.16.1.el9_4 | |
kernel-debug-debuginfo | 5.14.0-427.16.1.el9_4 | |
kernel-debug-modules | 5.14.0-427.16.1.el9_4 | |
kernel-debug-modules-core | 5.14.0-427.16.1.el9_4 | |
kernel-debug-modules-extra | 5.14.0-427.16.1.el9_4 | |
kernel-debuginfo | 5.14.0-427.16.1.el9_4 | |
kernel-debuginfo-common-aarch64 | 5.14.0-427.16.1.el9_4 | |
kernel-modules | 5.14.0-427.16.1.el9_4 | |
kernel-modules-core | 5.14.0-427.16.1.el9_4 | |
kernel-modules-extra | 5.14.0-427.16.1.el9_4 | |
kernel-tools | 5.14.0-427.16.1.el9_4 | |
kernel-tools-debuginfo | 5.14.0-427.16.1.el9_4 | |
kernel-tools-libs | 5.14.0-427.16.1.el9_4 | |
python3-perf | 5.14.0-427.16.1.el9_4 | |
python3-perf-debuginfo | 5.14.0-427.16.1.el9_4 | |
shim-aa64 | 15.8-4.el9_3.alma.1 | [ALSA-2024:1903](https://errata.almalinux.org/9/ALSA-2024-1903.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40546](https://access.redhat.com/security/cve/CVE-2023-40546), [CVE-2023-40547](https://access.redhat.com/security/cve/CVE-2023-40547), [CVE-2023-40548](https://access.redhat.com/security/cve/CVE-2023-40548), [CVE-2023-40549](https://access.redhat.com/security/cve/CVE-2023-40549), [CVE-2023-40550](https://access.redhat.com/security/cve/CVE-2023-40550), [CVE-2023-40551](https://access.redhat.com/security/cve/CVE-2023-40551))

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gdm | 40.1-23.el9.alma.1 | |
gdm-debuginfo | 40.1-23.el9.alma.1 | |
gdm-debugsource | 40.1-23.el9.alma.1 | |
git-lfs | 3.4.1-2.el9_4 | [ALSA-2024:2724](https://errata.almalinux.org/9/ALSA-2024-2724.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-45288](https://access.redhat.com/security/cve/CVE-2023-45288), [CVE-2023-45289](https://access.redhat.com/security/cve/CVE-2023-45289), [CVE-2023-45290](https://access.redhat.com/security/cve/CVE-2023-45290), [CVE-2024-24783](https://access.redhat.com/security/cve/CVE-2024-24783))
git-lfs-debuginfo | 3.4.1-2.el9_4 | |
git-lfs-debugsource | 3.4.1-2.el9_4 | |
kernel-64k-debug-devel | 5.14.0-427.16.1.el9_4 | |
kernel-64k-debug-devel-matched | 5.14.0-427.16.1.el9_4 | |
kernel-64k-devel | 5.14.0-427.16.1.el9_4 | |
kernel-64k-devel-matched | 5.14.0-427.16.1.el9_4 | |
kernel-debug-devel | 5.14.0-427.16.1.el9_4 | |
kernel-debug-devel-matched | 5.14.0-427.16.1.el9_4 | |
kernel-devel | 5.14.0-427.16.1.el9_4 | |
kernel-devel-matched | 5.14.0-427.16.1.el9_4 | |
kernel-doc | 5.14.0-427.16.1.el9_4 | |
kernel-headers | 5.14.0-427.16.1.el9_4 | |
nodejs | 18.20.2-2.module_el9.4.0+99+a01f7676 | [ALSA-2024:2779](https://errata.almalinux.org/9/ALSA-2024-2779.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-debuginfo | 18.20.2-2.module_el9.4.0+99+a01f7676 | |
nodejs-debugsource | 18.20.2-2.module_el9.4.0+99+a01f7676 | |
nodejs-devel | 18.20.2-2.module_el9.4.0+99+a01f7676 | [ALSA-2024:2779](https://errata.almalinux.org/9/ALSA-2024-2779.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-docs | 18.20.2-2.module_el9.4.0+99+a01f7676 | [ALSA-2024:2779](https://errata.almalinux.org/9/ALSA-2024-2779.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-full-i18n | 18.20.2-2.module_el9.4.0+99+a01f7676 | [ALSA-2024:2779](https://errata.almalinux.org/9/ALSA-2024-2779.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
npm | 10.5.0-1.18.20.2.2.module_el9.4.0+99+a01f7676 | [ALSA-2024:2779](https://errata.almalinux.org/9/ALSA-2024-2779.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
perf | 5.14.0-427.16.1.el9_4 | |
perf-debuginfo | 5.14.0-427.16.1.el9_4 | |
rtla | 5.14.0-427.16.1.el9_4 | |
rv | 5.14.0-427.16.1.el9_4 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gdm-devel | 40.1-23.el9.alma.1 | |
gdm-pam-extensions-devel | 40.1-23.el9.alma.1 | |
kernel-cross-headers | 5.14.0-427.16.1.el9_4 | |
kernel-tools-libs-devel | 5.14.0-427.16.1.el9_4 | |
libperf | 5.14.0-427.16.1.el9_4 | |
libperf-debuginfo | 5.14.0-427.16.1.el9_4 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-64k-debug-modules-internal | 5.14.0-427.16.1.el9_4 | |
kernel-64k-debug-modules-partner | 5.14.0-427.16.1.el9_4 | |
kernel-64k-modules-internal | 5.14.0-427.16.1.el9_4 | |
kernel-64k-modules-partner | 5.14.0-427.16.1.el9_4 | |
kernel-debug-modules-internal | 5.14.0-427.16.1.el9_4 | |
kernel-debug-modules-partner | 5.14.0-427.16.1.el9_4 | |
kernel-modules-internal | 5.14.0-427.16.1.el9_4 | |
kernel-modules-partner | 5.14.0-427.16.1.el9_4 | |
kernel-rt | 5.14.0-427.16.1.el9_4 | |
kernel-rt-core | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-core | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-debuginfo | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-devel | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-devel-matched | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-kvm | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-modules | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-modules-core | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-modules-extra | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-modules-internal | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debug-modules-partner | 5.14.0-427.16.1.el9_4 | |
kernel-rt-debuginfo | 5.14.0-427.16.1.el9_4 | |
kernel-rt-devel | 5.14.0-427.16.1.el9_4 | |
kernel-rt-devel-matched | 5.14.0-427.16.1.el9_4 | |
kernel-rt-kvm | 5.14.0-427.16.1.el9_4 | |
kernel-rt-modules | 5.14.0-427.16.1.el9_4 | |
kernel-rt-modules-core | 5.14.0-427.16.1.el9_4 | |
kernel-rt-modules-extra | 5.14.0-427.16.1.el9_4 | |
kernel-rt-modules-internal | 5.14.0-427.16.1.el9_4 | |
kernel-rt-modules-partner | 5.14.0-427.16.1.el9_4 | |
kernel-selftests-internal | 5.14.0-427.16.1.el9_4 | |
libperf-devel | 5.14.0-427.16.1.el9_4 | |

