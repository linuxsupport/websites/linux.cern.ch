## 2024-11-06

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3 | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3-libs | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.9-debuginfo | 3.9.18-3.el9_4.6 | |
python3.9-debugsource | 3.9.18-3.el9_4.6 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
buildah | 1.33.10-1.el9_4 | [ALSA-2024:8563](https://errata.almalinux.org/9/ALSA-2024-8563.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675))
buildah-debuginfo | 1.33.10-1.el9_4 | |
buildah-debugsource | 1.33.10-1.el9_4 | |
buildah-tests | 1.33.10-1.el9_4 | [ALSA-2024:8563](https://errata.almalinux.org/9/ALSA-2024-8563.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675))
buildah-tests-debuginfo | 1.33.10-1.el9_4 | |
python-unversioned-command | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3-devel | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3-tkinter | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.11 | 3.11.7-1.el9_4.6 | [ALSA-2024:8374](https://errata.almalinux.org/9/ALSA-2024-8374.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.11-debuginfo | 3.11.7-1.el9_4.6 | |
python3.11-debugsource | 3.11.7-1.el9_4.6 | |
python3.11-devel | 3.11.7-1.el9_4.6 | [ALSA-2024:8374](https://errata.almalinux.org/9/ALSA-2024-8374.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.11-libs | 3.11.7-1.el9_4.6 | [ALSA-2024:8374](https://errata.almalinux.org/9/ALSA-2024-8374.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.11-tkinter | 3.11.7-1.el9_4.6 | [ALSA-2024:8374](https://errata.almalinux.org/9/ALSA-2024-8374.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.12 | 3.12.1-4.el9_4.4 | [ALSA-2024:8447](https://errata.almalinux.org/9/ALSA-2024-8447.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.12-debuginfo | 3.12.1-4.el9_4.4 | |
python3.12-debugsource | 3.12.1-4.el9_4.4 | |
python3.12-devel | 3.12.1-4.el9_4.4 | [ALSA-2024:8447](https://errata.almalinux.org/9/ALSA-2024-8447.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.12-libs | 3.12.1-4.el9_4.4 | [ALSA-2024:8447](https://errata.almalinux.org/9/ALSA-2024-8447.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.12-tkinter | 3.12.1-4.el9_4.4 | [ALSA-2024:8447](https://errata.almalinux.org/9/ALSA-2024-8447.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
rpm-ostree | 2024.3-6.el9_4.alma.1 | |
rpm-ostree-debuginfo | 2024.3-6.el9_4.alma.1 | |
rpm-ostree-debugsource | 2024.3-6.el9_4.alma.1 | |
rpm-ostree-libs | 2024.3-6.el9_4.alma.1 | |
rpm-ostree-libs-debuginfo | 2024.3-6.el9_4.alma.1 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-debug | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3-idle | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3-test | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.11-debug | 3.11.7-1.el9_4.6 | [ALSA-2024:8374](https://errata.almalinux.org/9/ALSA-2024-8374.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.11-idle | 3.11.7-1.el9_4.6 | [ALSA-2024:8374](https://errata.almalinux.org/9/ALSA-2024-8374.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.11-test | 3.11.7-1.el9_4.6 | [ALSA-2024:8374](https://errata.almalinux.org/9/ALSA-2024-8374.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.12-debug | 3.12.1-4.el9_4.4 | [ALSA-2024:8447](https://errata.almalinux.org/9/ALSA-2024-8447.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.12-idle | 3.12.1-4.el9_4.4 | [ALSA-2024:8447](https://errata.almalinux.org/9/ALSA-2024-8447.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.12-test | 3.12.1-4.el9_4.4 | [ALSA-2024:8447](https://errata.almalinux.org/9/ALSA-2024-8447.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rpm-ostree-devel | 2024.3-6.el9_4.alma.1 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3 | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3-libs | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.9-debuginfo | 3.9.18-3.el9_4.6 | |
python3.9-debugsource | 3.9.18-3.el9_4.6 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
buildah | 1.33.10-1.el9_4 | [ALSA-2024:8563](https://errata.almalinux.org/9/ALSA-2024-8563.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675))
buildah-debuginfo | 1.33.10-1.el9_4 | |
buildah-debugsource | 1.33.10-1.el9_4 | |
buildah-tests | 1.33.10-1.el9_4 | [ALSA-2024:8563](https://errata.almalinux.org/9/ALSA-2024-8563.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9675](https://access.redhat.com/security/cve/CVE-2024-9675))
buildah-tests-debuginfo | 1.33.10-1.el9_4 | |
python-unversioned-command | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3-devel | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3-tkinter | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.11 | 3.11.7-1.el9_4.6 | [ALSA-2024:8374](https://errata.almalinux.org/9/ALSA-2024-8374.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.11-debuginfo | 3.11.7-1.el9_4.6 | |
python3.11-debugsource | 3.11.7-1.el9_4.6 | |
python3.11-devel | 3.11.7-1.el9_4.6 | [ALSA-2024:8374](https://errata.almalinux.org/9/ALSA-2024-8374.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.11-libs | 3.11.7-1.el9_4.6 | [ALSA-2024:8374](https://errata.almalinux.org/9/ALSA-2024-8374.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.11-tkinter | 3.11.7-1.el9_4.6 | [ALSA-2024:8374](https://errata.almalinux.org/9/ALSA-2024-8374.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.12 | 3.12.1-4.el9_4.4 | [ALSA-2024:8447](https://errata.almalinux.org/9/ALSA-2024-8447.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.12-debuginfo | 3.12.1-4.el9_4.4 | |
python3.12-debugsource | 3.12.1-4.el9_4.4 | |
python3.12-devel | 3.12.1-4.el9_4.4 | [ALSA-2024:8447](https://errata.almalinux.org/9/ALSA-2024-8447.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.12-libs | 3.12.1-4.el9_4.4 | [ALSA-2024:8447](https://errata.almalinux.org/9/ALSA-2024-8447.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.12-tkinter | 3.12.1-4.el9_4.4 | [ALSA-2024:8447](https://errata.almalinux.org/9/ALSA-2024-8447.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
rpm-ostree | 2024.3-6.el9_4.alma.1 | |
rpm-ostree-debuginfo | 2024.3-6.el9_4.alma.1 | |
rpm-ostree-debugsource | 2024.3-6.el9_4.alma.1 | |
rpm-ostree-libs | 2024.3-6.el9_4.alma.1 | |
rpm-ostree-libs-debuginfo | 2024.3-6.el9_4.alma.1 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-debug | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3-idle | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3-test | 3.9.18-3.el9_4.6 | [ALSA-2024:8446](https://errata.almalinux.org/9/ALSA-2024-8446.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.11-debug | 3.11.7-1.el9_4.6 | [ALSA-2024:8374](https://errata.almalinux.org/9/ALSA-2024-8374.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.11-idle | 3.11.7-1.el9_4.6 | [ALSA-2024:8374](https://errata.almalinux.org/9/ALSA-2024-8374.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.11-test | 3.11.7-1.el9_4.6 | [ALSA-2024:8374](https://errata.almalinux.org/9/ALSA-2024-8374.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.12-debug | 3.12.1-4.el9_4.4 | [ALSA-2024:8447](https://errata.almalinux.org/9/ALSA-2024-8447.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.12-idle | 3.12.1-4.el9_4.4 | [ALSA-2024:8447](https://errata.almalinux.org/9/ALSA-2024-8447.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python3.12-test | 3.12.1-4.el9_4.4 | [ALSA-2024:8447](https://errata.almalinux.org/9/ALSA-2024-8447.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rpm-ostree-devel | 2024.3-6.el9_4.alma.1 | |

