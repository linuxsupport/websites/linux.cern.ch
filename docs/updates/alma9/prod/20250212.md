## 2025-02-12

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-release | 9-9.al9.cern | |
hepix | 4.10.14-0.al9.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libsoup | 2.72.0-8.el9_5.3 | [ALSA-2025:0791](https://errata.almalinux.org/9/ALSA-2025-0791.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-52531](https://access.redhat.com/security/cve/CVE-2024-52531))
libsoup-debuginfo | 2.72.0-8.el9_5.3 | |
libsoup-debugsource | 2.72.0-8.el9_5.3 | |
libsoup-devel | 2.72.0-8.el9_5.3 | [ALSA-2025:0791](https://errata.almalinux.org/9/ALSA-2025-0791.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-52531](https://access.redhat.com/security/cve/CVE-2024-52531))
redis | 6.2.17-1.el9_5 | [ALSA-2025:0693](https://errata.almalinux.org/9/ALSA-2025-0693.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis | 7.2.7-1.module_el9.5.0+134+2e645600 | [ALSA-2025:0692](https://errata.almalinux.org/9/ALSA-2025-0692.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981), [CVE-2024-51741](https://access.redhat.com/security/cve/CVE-2024-51741))
redis-debuginfo | 6.2.17-1.el9_5 | |
redis-debuginfo | 7.2.7-1.module_el9.5.0+134+2e645600 | |
redis-debugsource | 6.2.17-1.el9_5 | |
redis-debugsource | 7.2.7-1.module_el9.5.0+134+2e645600 | |
redis-devel | 6.2.17-1.el9_5 | [ALSA-2025:0693](https://errata.almalinux.org/9/ALSA-2025-0693.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis-devel | 7.2.7-1.module_el9.5.0+134+2e645600 | [ALSA-2025:0692](https://errata.almalinux.org/9/ALSA-2025-0692.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981), [CVE-2024-51741](https://access.redhat.com/security/cve/CVE-2024-51741))
redis-doc | 6.2.17-1.el9_5 | [ALSA-2025:0693](https://errata.almalinux.org/9/ALSA-2025-0693.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis-doc | 7.2.7-1.module_el9.5.0+134+2e645600 | [ALSA-2025:0692](https://errata.almalinux.org/9/ALSA-2025-0692.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981), [CVE-2024-51741](https://access.redhat.com/security/cve/CVE-2024-51741))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-next-release | 9-9.el9 | |
libsoup-doc | 2.72.0-8.el9_5.3 | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-release | 9-9.el9 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-release | 9-9.al9.cern | |
hepix | 4.10.14-0.al9.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libsoup | 2.72.0-8.el9_5.3 | [ALSA-2025:0791](https://errata.almalinux.org/9/ALSA-2025-0791.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-52531](https://access.redhat.com/security/cve/CVE-2024-52531))
libsoup-debuginfo | 2.72.0-8.el9_5.3 | |
libsoup-debugsource | 2.72.0-8.el9_5.3 | |
libsoup-devel | 2.72.0-8.el9_5.3 | [ALSA-2025:0791](https://errata.almalinux.org/9/ALSA-2025-0791.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-52531](https://access.redhat.com/security/cve/CVE-2024-52531))
redis | 6.2.17-1.el9_5 | [ALSA-2025:0693](https://errata.almalinux.org/9/ALSA-2025-0693.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis | 7.2.7-1.module_el9.5.0+134+2e645600 | [ALSA-2025:0692](https://errata.almalinux.org/9/ALSA-2025-0692.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981), [CVE-2024-51741](https://access.redhat.com/security/cve/CVE-2024-51741))
redis-debuginfo | 6.2.17-1.el9_5 | |
redis-debuginfo | 7.2.7-1.module_el9.5.0+134+2e645600 | |
redis-debugsource | 6.2.17-1.el9_5 | |
redis-debugsource | 7.2.7-1.module_el9.5.0+134+2e645600 | |
redis-devel | 6.2.17-1.el9_5 | [ALSA-2025:0693](https://errata.almalinux.org/9/ALSA-2025-0693.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis-devel | 7.2.7-1.module_el9.5.0+134+2e645600 | [ALSA-2025:0692](https://errata.almalinux.org/9/ALSA-2025-0692.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981), [CVE-2024-51741](https://access.redhat.com/security/cve/CVE-2024-51741))
redis-doc | 6.2.17-1.el9_5 | [ALSA-2025:0693](https://errata.almalinux.org/9/ALSA-2025-0693.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis-doc | 7.2.7-1.module_el9.5.0+134+2e645600 | [ALSA-2025:0692](https://errata.almalinux.org/9/ALSA-2025-0692.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981), [CVE-2024-51741](https://access.redhat.com/security/cve/CVE-2024-51741))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-next-release | 9-9.el9 | |
libsoup-doc | 2.72.0-8.el9_5.3 | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-release | 9-9.el9 | |

