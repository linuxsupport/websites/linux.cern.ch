## 2024-03-13

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
opensc | 0.23.0-4.el9_3 | [ALSA-2024:0966](https://errata.almalinux.org/9/ALSA-2024-0966.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5992](https://access.redhat.com/security/cve/CVE-2023-5992))
opensc-debuginfo | 0.23.0-4.el9_3 | |
opensc-debugsource | 0.23.0-4.el9_3 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-dbg-8.0 | 8.0.2-2.el9_3 | [ALSA-2024:0848](https://errata.almalinux.org/9/ALSA-2024-0848.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-dbg-8.0 | 8.0.2-2.el9_3 | [ALSA-2024:0848](https://errata.almalinux.org/9/ALSA-2024-0848.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-dbg-8.0 | 8.0.102-2.el9_3 | [ALSA-2024:0848](https://errata.almalinux.org/9/ALSA-2024-0848.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
firefox | 115.8.0-1.el9_3.alma | [ALSA-2024:0952](https://errata.almalinux.org/9/ALSA-2024-0952.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1546](https://access.redhat.com/security/cve/CVE-2024-1546), [CVE-2024-1547](https://access.redhat.com/security/cve/CVE-2024-1547), [CVE-2024-1548](https://access.redhat.com/security/cve/CVE-2024-1548), [CVE-2024-1549](https://access.redhat.com/security/cve/CVE-2024-1549), [CVE-2024-1550](https://access.redhat.com/security/cve/CVE-2024-1550), [CVE-2024-1551](https://access.redhat.com/security/cve/CVE-2024-1551), [CVE-2024-1552](https://access.redhat.com/security/cve/CVE-2024-1552), [CVE-2024-1553](https://access.redhat.com/security/cve/CVE-2024-1553))
firefox-debuginfo | 115.8.0-1.el9_3.alma | |
firefox-debugsource | 115.8.0-1.el9_3.alma | |
firefox-x11 | 115.8.0-1.el9_3.alma | [ALSA-2024:0952](https://errata.almalinux.org/9/ALSA-2024-0952.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1546](https://access.redhat.com/security/cve/CVE-2024-1546), [CVE-2024-1547](https://access.redhat.com/security/cve/CVE-2024-1547), [CVE-2024-1548](https://access.redhat.com/security/cve/CVE-2024-1548), [CVE-2024-1549](https://access.redhat.com/security/cve/CVE-2024-1549), [CVE-2024-1550](https://access.redhat.com/security/cve/CVE-2024-1550), [CVE-2024-1551](https://access.redhat.com/security/cve/CVE-2024-1551), [CVE-2024-1552](https://access.redhat.com/security/cve/CVE-2024-1552), [CVE-2024-1553](https://access.redhat.com/security/cve/CVE-2024-1553))
postgresql | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-contrib | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-contrib | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-contrib-debuginfo | 13.14-1.el9_3 | |
postgresql-contrib-debuginfo | 15.6-1.module_el9.3.0+55+d62f4779 | |
postgresql-debuginfo | 13.14-1.el9_3 | |
postgresql-debuginfo | 15.6-1.module_el9.3.0+55+d62f4779 | |
postgresql-debugsource | 13.14-1.el9_3 | |
postgresql-debugsource | 15.6-1.module_el9.3.0+55+d62f4779 | |
postgresql-docs | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-docs-debuginfo | 15.6-1.module_el9.3.0+55+d62f4779 | |
postgresql-plperl | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plperl | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plperl-debuginfo | 13.14-1.el9_3 | |
postgresql-plperl-debuginfo | 15.6-1.module_el9.3.0+55+d62f4779 | |
postgresql-plpython3 | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plpython3 | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plpython3-debuginfo | 13.14-1.el9_3 | |
postgresql-plpython3-debuginfo | 15.6-1.module_el9.3.0+55+d62f4779 | |
postgresql-pltcl | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-pltcl | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-pltcl-debuginfo | 13.14-1.el9_3 | |
postgresql-pltcl-debuginfo | 15.6-1.module_el9.3.0+55+d62f4779 | |
postgresql-private-devel | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-private-libs | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-private-libs | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-private-libs-debuginfo | 13.14-1.el9_3 | |
postgresql-private-libs-debuginfo | 15.6-1.module_el9.3.0+55+d62f4779 | |
postgresql-server | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server-debuginfo | 13.14-1.el9_3 | |
postgresql-server-debuginfo | 15.6-1.module_el9.3.0+55+d62f4779 | |
postgresql-server-devel | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server-devel-debuginfo | 15.6-1.module_el9.3.0+55+d62f4779 | |
postgresql-static | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test-debuginfo | 15.6-1.module_el9.3.0+55+d62f4779 | |
postgresql-test-rpm-macros | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade-debuginfo | 13.14-1.el9_3 | |
postgresql-upgrade-debuginfo | 15.6-1.module_el9.3.0+55+d62f4779 | |
postgresql-upgrade-devel | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade-devel-debuginfo | 15.6-1.module_el9.3.0+55+d62f4779 | |
python3-unbound | 1.16.2-3.el9_3.1 | [ALSA-2024:0977](https://errata.almalinux.org/9/ALSA-2024-0977.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
python3-unbound-debuginfo | 1.16.2-3.el9_3.1 | |
scap-security-guide | 0.1.72-1.el9_3.alma.1 | |
scap-security-guide-doc | 0.1.72-1.el9_3.alma.1 | |
thunderbird | 115.8.0-1.el9_3.alma | [ALSA-2024:0963](https://errata.almalinux.org/9/ALSA-2024-0963.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1546](https://access.redhat.com/security/cve/CVE-2024-1546), [CVE-2024-1547](https://access.redhat.com/security/cve/CVE-2024-1547), [CVE-2024-1548](https://access.redhat.com/security/cve/CVE-2024-1548), [CVE-2024-1549](https://access.redhat.com/security/cve/CVE-2024-1549), [CVE-2024-1550](https://access.redhat.com/security/cve/CVE-2024-1550), [CVE-2024-1551](https://access.redhat.com/security/cve/CVE-2024-1551), [CVE-2024-1552](https://access.redhat.com/security/cve/CVE-2024-1552), [CVE-2024-1553](https://access.redhat.com/security/cve/CVE-2024-1553))
thunderbird-debuginfo | 115.8.0-1.el9_3.alma | |
thunderbird-debugsource | 115.8.0-1.el9_3.alma | |
unbound | 1.16.2-3.el9_3.1 | [ALSA-2024:0977](https://errata.almalinux.org/9/ALSA-2024-0977.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
unbound-debuginfo | 1.16.2-3.el9_3.1 | |
unbound-debugsource | 1.16.2-3.el9_3.1 | |
unbound-libs | 1.16.2-3.el9_3.1 | [ALSA-2024:0977](https://errata.almalinux.org/9/ALSA-2024-0977.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
unbound-libs-debuginfo | 1.16.2-3.el9_3.1 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
postgresql-docs | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-docs-debuginfo | 13.14-1.el9_3 | |
postgresql-private-devel | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server-devel | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server-devel-debuginfo | 13.14-1.el9_3 | |
postgresql-static | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test-debuginfo | 13.14-1.el9_3 | |
postgresql-upgrade-devel | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade-devel-debuginfo | 13.14-1.el9_3 | |
unbound-devel | 1.16.2-3.el9_3.1 | [ALSA-2024:0977](https://errata.almalinux.org/9/ALSA-2024-0977.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
postgresql-test-rpm-macros | 13.14-1.el9_3 | |
scap-security-guide-rule-playbooks | 0.1.72-1.el9_3.alma.1 | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 115.8.0-1.el9_3.alma.plus | [ALSA-2024:0963](https://errata.almalinux.org/9/ALSA-2024-0963.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1546](https://access.redhat.com/security/cve/CVE-2024-1546), [CVE-2024-1547](https://access.redhat.com/security/cve/CVE-2024-1547), [CVE-2024-1548](https://access.redhat.com/security/cve/CVE-2024-1548), [CVE-2024-1549](https://access.redhat.com/security/cve/CVE-2024-1549), [CVE-2024-1550](https://access.redhat.com/security/cve/CVE-2024-1550), [CVE-2024-1551](https://access.redhat.com/security/cve/CVE-2024-1551), [CVE-2024-1552](https://access.redhat.com/security/cve/CVE-2024-1552), [CVE-2024-1553](https://access.redhat.com/security/cve/CVE-2024-1553))
thunderbird-debuginfo | 115.8.0-1.el9_3.alma.plus | |
thunderbird-debugsource | 115.8.0-1.el9_3.alma.plus | |
thunderbird-librnp-rnp | 115.8.0-1.el9_3.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 115.8.0-1.el9_3.alma.plus | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
opensc | 0.23.0-4.el9_3 | [ALSA-2024:0966](https://errata.almalinux.org/9/ALSA-2024-0966.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5992](https://access.redhat.com/security/cve/CVE-2023-5992))
opensc-debuginfo | 0.23.0-4.el9_3 | |
opensc-debugsource | 0.23.0-4.el9_3 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-dbg-8.0 | 8.0.2-2.el9_3 | [ALSA-2024:0848](https://errata.almalinux.org/9/ALSA-2024-0848.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-dbg-8.0 | 8.0.2-2.el9_3 | [ALSA-2024:0848](https://errata.almalinux.org/9/ALSA-2024-0848.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-dbg-8.0 | 8.0.102-2.el9_3 | [ALSA-2024:0848](https://errata.almalinux.org/9/ALSA-2024-0848.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
firefox | 115.8.0-1.el9_3.alma | [ALSA-2024:0952](https://errata.almalinux.org/9/ALSA-2024-0952.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1546](https://access.redhat.com/security/cve/CVE-2024-1546), [CVE-2024-1547](https://access.redhat.com/security/cve/CVE-2024-1547), [CVE-2024-1548](https://access.redhat.com/security/cve/CVE-2024-1548), [CVE-2024-1549](https://access.redhat.com/security/cve/CVE-2024-1549), [CVE-2024-1550](https://access.redhat.com/security/cve/CVE-2024-1550), [CVE-2024-1551](https://access.redhat.com/security/cve/CVE-2024-1551), [CVE-2024-1552](https://access.redhat.com/security/cve/CVE-2024-1552), [CVE-2024-1553](https://access.redhat.com/security/cve/CVE-2024-1553))
firefox-x11 | 115.8.0-1.el9_3.alma | [ALSA-2024:0952](https://errata.almalinux.org/9/ALSA-2024-0952.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1546](https://access.redhat.com/security/cve/CVE-2024-1546), [CVE-2024-1547](https://access.redhat.com/security/cve/CVE-2024-1547), [CVE-2024-1548](https://access.redhat.com/security/cve/CVE-2024-1548), [CVE-2024-1549](https://access.redhat.com/security/cve/CVE-2024-1549), [CVE-2024-1550](https://access.redhat.com/security/cve/CVE-2024-1550), [CVE-2024-1551](https://access.redhat.com/security/cve/CVE-2024-1551), [CVE-2024-1552](https://access.redhat.com/security/cve/CVE-2024-1552), [CVE-2024-1553](https://access.redhat.com/security/cve/CVE-2024-1553))
postgresql | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-contrib | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-contrib | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-docs | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plperl | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plperl | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plpython3 | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plpython3 | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-pltcl | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-pltcl | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-private-devel | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-private-libs | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-private-libs | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server-devel | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-static | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test-rpm-macros | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade-devel | 15.6-1.module_el9.3.0+55+d62f4779 | [ALSA-2024:0950](https://errata.almalinux.org/9/ALSA-2024-0950.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
python3-unbound | 1.16.2-3.el9_3.1 | [ALSA-2024:0977](https://errata.almalinux.org/9/ALSA-2024-0977.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
scap-security-guide | 0.1.72-1.el9_3.alma.1 | |
scap-security-guide-doc | 0.1.72-1.el9_3.alma.1 | |
thunderbird | 115.8.0-1.el9_3.alma | [ALSA-2024:0963](https://errata.almalinux.org/9/ALSA-2024-0963.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1546](https://access.redhat.com/security/cve/CVE-2024-1546), [CVE-2024-1547](https://access.redhat.com/security/cve/CVE-2024-1547), [CVE-2024-1548](https://access.redhat.com/security/cve/CVE-2024-1548), [CVE-2024-1549](https://access.redhat.com/security/cve/CVE-2024-1549), [CVE-2024-1550](https://access.redhat.com/security/cve/CVE-2024-1550), [CVE-2024-1551](https://access.redhat.com/security/cve/CVE-2024-1551), [CVE-2024-1552](https://access.redhat.com/security/cve/CVE-2024-1552), [CVE-2024-1553](https://access.redhat.com/security/cve/CVE-2024-1553))
unbound | 1.16.2-3.el9_3.1 | [ALSA-2024:0977](https://errata.almalinux.org/9/ALSA-2024-0977.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
unbound-libs | 1.16.2-3.el9_3.1 | [ALSA-2024:0977](https://errata.almalinux.org/9/ALSA-2024-0977.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
postgresql-docs | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-docs-debuginfo | 13.14-1.el9_3 | |
postgresql-private-devel | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server-devel | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server-devel-debuginfo | 13.14-1.el9_3 | |
postgresql-static | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test-debuginfo | 13.14-1.el9_3 | |
postgresql-upgrade-devel | 13.14-1.el9_3 | [ALSA-2024:0951](https://errata.almalinux.org/9/ALSA-2024-0951.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade-devel-debuginfo | 13.14-1.el9_3 | |
unbound-devel | 1.16.2-3.el9_3.1 | [ALSA-2024:0977](https://errata.almalinux.org/9/ALSA-2024-0977.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
postgresql-test-rpm-macros | 13.14-1.el9_3 | |
scap-security-guide-rule-playbooks | 0.1.72-1.el9_3.alma.1 | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 115.8.0-1.el9_3.alma.plus | [ALSA-2024:0963](https://errata.almalinux.org/9/ALSA-2024-0963.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1546](https://access.redhat.com/security/cve/CVE-2024-1546), [CVE-2024-1547](https://access.redhat.com/security/cve/CVE-2024-1547), [CVE-2024-1548](https://access.redhat.com/security/cve/CVE-2024-1548), [CVE-2024-1549](https://access.redhat.com/security/cve/CVE-2024-1549), [CVE-2024-1550](https://access.redhat.com/security/cve/CVE-2024-1550), [CVE-2024-1551](https://access.redhat.com/security/cve/CVE-2024-1551), [CVE-2024-1552](https://access.redhat.com/security/cve/CVE-2024-1552), [CVE-2024-1553](https://access.redhat.com/security/cve/CVE-2024-1553))
thunderbird-debuginfo | 115.8.0-1.el9_3.alma.plus | |
thunderbird-debugsource | 115.8.0-1.el9_3.alma.plus | |
thunderbird-librnp-rnp | 115.8.0-1.el9_3.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 115.8.0-1.el9_3.alma.plus | |

