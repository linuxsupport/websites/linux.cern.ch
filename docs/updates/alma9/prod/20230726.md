## 2023-07-26

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
locmap-release | 1.0-9.al9.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.20-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-runtime-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-targeting-pack-6.0 | 6.0.20-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-targeting-pack-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
bind | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-chroot | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-debuginfo | 9.16.23-11.el9_2.1 | |
bind-debugsource | 9.16.23-11.el9_2.1 | |
bind-dnssec-doc | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils-debuginfo | 9.16.23-11.el9_2.1 | |
bind-libs | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-libs-debuginfo | 9.16.23-11.el9_2.1 | |
bind-license | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-utils | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-utils-debuginfo | 9.16.23-11.el9_2.1 | |
dotnet-apphost-pack-6.0 | 6.0.20-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-apphost-pack-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-host | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-host-debuginfo | 7.0.9-1.el9_2 | |
dotnet-hostfxr-6.0 | 6.0.20-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-hostfxr-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-hostfxr-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-hostfxr-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-runtime-6.0 | 6.0.20-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-runtime-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-runtime-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-runtime-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-sdk-6.0 | 6.0.120-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-6.0-debuginfo | 6.0.120-1.el9_2 | |
dotnet-sdk-7.0 | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet-targeting-pack-6.0 | 6.0.20-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-targeting-pack-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-templates-6.0 | 6.0.120-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-templates-7.0 | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet6.0-debuginfo | 6.0.120-1.el9_2 | |
dotnet6.0-debugsource | 6.0.120-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet7.0-debugsource | 7.0.109-1.el9_2 | |
firefox | 102.13.0-2.el9_2.alma | [ALSA-2023:4071](https://errata.almalinux.org/9/ALSA-2023-4071.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37201](https://access.redhat.com/security/cve/CVE-2023-37201), [CVE-2023-37202](https://access.redhat.com/security/cve/CVE-2023-37202), [CVE-2023-37207](https://access.redhat.com/security/cve/CVE-2023-37207), [CVE-2023-37208](https://access.redhat.com/security/cve/CVE-2023-37208), [CVE-2023-37211](https://access.redhat.com/security/cve/CVE-2023-37211))
firefox-debuginfo | 102.13.0-2.el9_2.alma | |
firefox-debugsource | 102.13.0-2.el9_2.alma | |
firefox-x11 | 102.13.0-2.el9_2.alma | [ALSA-2023:4071](https://errata.almalinux.org/9/ALSA-2023-4071.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37201](https://access.redhat.com/security/cve/CVE-2023-37201), [CVE-2023-37202](https://access.redhat.com/security/cve/CVE-2023-37202), [CVE-2023-37207](https://access.redhat.com/security/cve/CVE-2023-37207), [CVE-2023-37208](https://access.redhat.com/security/cve/CVE-2023-37208), [CVE-2023-37211](https://access.redhat.com/security/cve/CVE-2023-37211))
grafana | 9.0.9-3.el9_2.alma | [ALSA-2023:4030](https://errata.almalinux.org/9/ALSA-2023-4030.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3128](https://access.redhat.com/security/cve/CVE-2023-3128))
grafana-debuginfo | 9.0.9-3.el9_2.alma | |
grafana-debugsource | 9.0.9-3.el9_2.alma | |
iperf3 | 3.9-9.el9_2.1.alma | |
iperf3-debuginfo | 3.9-9.el9_2.1.alma | |
iperf3-debugsource | 3.9-9.el9_2.1.alma | |
netstandard-targeting-pack-2.1 | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
open-vm-tools | 12.1.5-1.el9_2.1.alma | [ALSA-2023:3948](https://errata.almalinux.org/9/ALSA-2023-3948.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-20867](https://access.redhat.com/security/cve/CVE-2023-20867))
open-vm-tools-debuginfo | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-debugsource | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-desktop | 12.1.5-1.el9_2.1.alma | [ALSA-2023:3948](https://errata.almalinux.org/9/ALSA-2023-3948.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-20867](https://access.redhat.com/security/cve/CVE-2023-20867))
open-vm-tools-desktop-debuginfo | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-salt-minion | 12.1.5-1.el9_2.1.alma | [ALSA-2023:3948](https://errata.almalinux.org/9/ALSA-2023-3948.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-20867](https://access.redhat.com/security/cve/CVE-2023-20867))
open-vm-tools-sdmp | 12.1.5-1.el9_2.1.alma | [ALSA-2023:3948](https://errata.almalinux.org/9/ALSA-2023-3948.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-20867](https://access.redhat.com/security/cve/CVE-2023-20867))
open-vm-tools-sdmp-debuginfo | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-test | 12.1.5-1.el9_2.1.alma | [ALSA-2023:3948](https://errata.almalinux.org/9/ALSA-2023-3948.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-20867](https://access.redhat.com/security/cve/CVE-2023-20867))
open-vm-tools-test-debuginfo | 12.1.5-1.el9_2.1.alma | |
python3-bind | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
thunderbird | 102.13.0-2.el9_2.alma | [ALSA-2023:4064](https://errata.almalinux.org/9/ALSA-2023-4064.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37201](https://access.redhat.com/security/cve/CVE-2023-37201), [CVE-2023-37202](https://access.redhat.com/security/cve/CVE-2023-37202), [CVE-2023-37207](https://access.redhat.com/security/cve/CVE-2023-37207), [CVE-2023-37208](https://access.redhat.com/security/cve/CVE-2023-37208), [CVE-2023-37211](https://access.redhat.com/security/cve/CVE-2023-37211))
thunderbird-debuginfo | 102.13.0-2.el9_2.alma | |
thunderbird-debugsource | 102.13.0-2.el9_2.alma | |
webkit2gtk3 | 2.38.5-1.el9_2.3 | [ALSA-2023:4201](https://errata.almalinux.org/9/ALSA-2023-4201.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-debugsource | 2.38.5-1.el9_2.3 | |
webkit2gtk3-devel | 2.38.5-1.el9_2.3 | [ALSA-2023:4201](https://errata.almalinux.org/9/ALSA-2023-4201.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-devel-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-jsc | 2.38.5-1.el9_2.3 | [ALSA-2023:4201](https://errata.almalinux.org/9/ALSA-2023-4201.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-jsc-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-jsc-devel | 2.38.5-1.el9_2.3 | [ALSA-2023:4201](https://errata.almalinux.org/9/ALSA-2023-4201.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-jsc-devel-debuginfo | 2.38.5-1.el9_2.3 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-devel | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-doc | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
catatonit | 0.1.7-9.el9_2 | |
catatonit-debuginfo | 0.1.7-9.el9_2 | |
catatonit-debugsource | 0.1.7-9.el9_2 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.120-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-7.0-source-built-artifacts | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iperf3-devel | 3.9-9.el9_2.1.alma | |
open-vm-tools-devel | 12.1.5-1.el9_2.1.alma | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.13.0-2.el9_2.alma.plus | [ALSA-2023:4064](https://errata.almalinux.org/9/ALSA-2023-4064.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37201](https://access.redhat.com/security/cve/CVE-2023-37201), [CVE-2023-37202](https://access.redhat.com/security/cve/CVE-2023-37202), [CVE-2023-37207](https://access.redhat.com/security/cve/CVE-2023-37207), [CVE-2023-37208](https://access.redhat.com/security/cve/CVE-2023-37208), [CVE-2023-37211](https://access.redhat.com/security/cve/CVE-2023-37211))
thunderbird-debuginfo | 102.13.0-2.el9_2.alma.plus | |
thunderbird-debugsource | 102.13.0-2.el9_2.alma.plus | |
thunderbird-librnp-rnp | 102.13.0-2.el9_2.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.13.0-2.el9_2.alma.plus | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
locmap-release | 1.0-9.al9.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.20-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-runtime-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-targeting-pack-6.0 | 6.0.20-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-targeting-pack-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
bind | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-chroot | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-debuginfo | 9.16.23-11.el9_2.1 | |
bind-debugsource | 9.16.23-11.el9_2.1 | |
bind-dnssec-doc | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils-debuginfo | 9.16.23-11.el9_2.1 | |
bind-libs | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-libs-debuginfo | 9.16.23-11.el9_2.1 | |
bind-license | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-utils | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-utils-debuginfo | 9.16.23-11.el9_2.1 | |
dotnet-apphost-pack-6.0 | 6.0.20-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-apphost-pack-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-host | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-host-debuginfo | 7.0.9-1.el9_2 | |
dotnet-hostfxr-6.0 | 6.0.20-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-hostfxr-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-hostfxr-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-hostfxr-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-runtime-6.0 | 6.0.20-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-runtime-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-runtime-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-runtime-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-sdk-6.0 | 6.0.120-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-6.0-debuginfo | 6.0.120-1.el9_2 | |
dotnet-sdk-7.0 | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet-targeting-pack-6.0 | 6.0.20-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-targeting-pack-7.0 | 7.0.9-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-templates-6.0 | 6.0.120-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-templates-7.0 | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet6.0-debuginfo | 6.0.120-1.el9_2 | |
dotnet6.0-debugsource | 6.0.120-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet7.0-debugsource | 7.0.109-1.el9_2 | |
firefox | 102.13.0-2.el9_2.alma | [ALSA-2023:4071](https://errata.almalinux.org/9/ALSA-2023-4071.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37201](https://access.redhat.com/security/cve/CVE-2023-37201), [CVE-2023-37202](https://access.redhat.com/security/cve/CVE-2023-37202), [CVE-2023-37207](https://access.redhat.com/security/cve/CVE-2023-37207), [CVE-2023-37208](https://access.redhat.com/security/cve/CVE-2023-37208), [CVE-2023-37211](https://access.redhat.com/security/cve/CVE-2023-37211))
firefox-debuginfo | 102.13.0-2.el9_2.alma | |
firefox-debugsource | 102.13.0-2.el9_2.alma | |
firefox-x11 | 102.13.0-2.el9_2.alma | [ALSA-2023:4071](https://errata.almalinux.org/9/ALSA-2023-4071.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37201](https://access.redhat.com/security/cve/CVE-2023-37201), [CVE-2023-37202](https://access.redhat.com/security/cve/CVE-2023-37202), [CVE-2023-37207](https://access.redhat.com/security/cve/CVE-2023-37207), [CVE-2023-37208](https://access.redhat.com/security/cve/CVE-2023-37208), [CVE-2023-37211](https://access.redhat.com/security/cve/CVE-2023-37211))
grafana | 9.0.9-3.el9_2.alma | [ALSA-2023:4030](https://errata.almalinux.org/9/ALSA-2023-4030.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3128](https://access.redhat.com/security/cve/CVE-2023-3128))
grafana-debuginfo | 9.0.9-3.el9_2.alma | |
grafana-debugsource | 9.0.9-3.el9_2.alma | |
iperf3 | 3.9-9.el9_2.1.alma | |
iperf3-debuginfo | 3.9-9.el9_2.1.alma | |
iperf3-debugsource | 3.9-9.el9_2.1.alma | |
netstandard-targeting-pack-2.1 | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
open-vm-tools | 12.1.5-1.el9_2.1.alma | [ALSA-2023:3948](https://errata.almalinux.org/9/ALSA-2023-3948.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-20867](https://access.redhat.com/security/cve/CVE-2023-20867))
open-vm-tools-debuginfo | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-debugsource | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-desktop | 12.1.5-1.el9_2.1.alma | [ALSA-2023:3948](https://errata.almalinux.org/9/ALSA-2023-3948.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-20867](https://access.redhat.com/security/cve/CVE-2023-20867))
open-vm-tools-desktop-debuginfo | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-test | 12.1.5-1.el9_2.1.alma | [ALSA-2023:3948](https://errata.almalinux.org/9/ALSA-2023-3948.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-20867](https://access.redhat.com/security/cve/CVE-2023-20867))
open-vm-tools-test-debuginfo | 12.1.5-1.el9_2.1.alma | |
python3-bind | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
thunderbird | 102.13.0-2.el9_2.alma | [ALSA-2023:4064](https://errata.almalinux.org/9/ALSA-2023-4064.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37201](https://access.redhat.com/security/cve/CVE-2023-37201), [CVE-2023-37202](https://access.redhat.com/security/cve/CVE-2023-37202), [CVE-2023-37207](https://access.redhat.com/security/cve/CVE-2023-37207), [CVE-2023-37208](https://access.redhat.com/security/cve/CVE-2023-37208), [CVE-2023-37211](https://access.redhat.com/security/cve/CVE-2023-37211))
thunderbird-debuginfo | 102.13.0-2.el9_2.alma | |
thunderbird-debugsource | 102.13.0-2.el9_2.alma | |
webkit2gtk3 | 2.38.5-1.el9_2.3 | [ALSA-2023:4201](https://errata.almalinux.org/9/ALSA-2023-4201.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-debugsource | 2.38.5-1.el9_2.3 | |
webkit2gtk3-devel | 2.38.5-1.el9_2.3 | [ALSA-2023:4201](https://errata.almalinux.org/9/ALSA-2023-4201.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-devel-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-jsc | 2.38.5-1.el9_2.3 | [ALSA-2023:4201](https://errata.almalinux.org/9/ALSA-2023-4201.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-jsc-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-jsc-devel | 2.38.5-1.el9_2.3 | [ALSA-2023:4201](https://errata.almalinux.org/9/ALSA-2023-4201.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-jsc-devel-debuginfo | 2.38.5-1.el9_2.3 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-devel | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-doc | 9.16.23-11.el9_2.1 | [ALSA-2023:4099](https://errata.almalinux.org/9/ALSA-2023-4099.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
catatonit | 0.1.7-9.el9_2 | |
catatonit-debuginfo | 0.1.7-9.el9_2 | |
catatonit-debugsource | 0.1.7-9.el9_2 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.120-1.el9_2 | [ALSA-2023:4060](https://errata.almalinux.org/9/ALSA-2023-4060.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-7.0-source-built-artifacts | 7.0.109-1.el9_2 | [ALSA-2023:4057](https://errata.almalinux.org/9/ALSA-2023-4057.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iperf3-devel | 3.9-9.el9_2.1.alma | |
open-vm-tools-devel | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-sdmp | 12.1.5-1.el9_2.1.alma | |
open-vm-tools-sdmp-debuginfo | 12.1.5-1.el9_2.1.alma | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.13.0-2.el9_2.alma.plus | [ALSA-2023:4064](https://errata.almalinux.org/9/ALSA-2023-4064.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37201](https://access.redhat.com/security/cve/CVE-2023-37201), [CVE-2023-37202](https://access.redhat.com/security/cve/CVE-2023-37202), [CVE-2023-37207](https://access.redhat.com/security/cve/CVE-2023-37207), [CVE-2023-37208](https://access.redhat.com/security/cve/CVE-2023-37208), [CVE-2023-37211](https://access.redhat.com/security/cve/CVE-2023-37211))
thunderbird-debuginfo | 102.13.0-2.el9_2.alma.plus | |
thunderbird-debugsource | 102.13.0-2.el9_2.alma.plus | |
thunderbird-librnp-rnp | 102.13.0-2.el9_2.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.13.0-2.el9_2.alma.plus | |

