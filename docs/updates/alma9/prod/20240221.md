## 2024-02-21

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20230808-2.20231009.1.el9_3 | |
selinux-policy | 38.1.23-1.el9_3.2 | |
selinux-policy-doc | 38.1.23-1.el9_3.2 | |
selinux-policy-mls | 38.1.23-1.el9_3.2 | |
selinux-policy-sandbox | 38.1.23-1.el9_3.2 | |
selinux-policy-targeted | 38.1.23-1.el9_3.2 | |
sos | 4.6.1-1.el9.alma.1 | |
sos-audit | 4.6.1-1.el9.alma.1 | |
tzdata | 2024a-1.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gimp | 2.99.8-4.el9_3 | [ALSA-2024:0675](https://errata.almalinux.org/9/ALSA-2024-0675.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44441](https://access.redhat.com/security/cve/CVE-2023-44441), [CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44443](https://access.redhat.com/security/cve/CVE-2023-44443), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
gimp-debuginfo | 2.99.8-4.el9_3 | |
gimp-debugsource | 2.99.8-4.el9_3 | |
gimp-libs | 2.99.8-4.el9_3 | [ALSA-2024:0675](https://errata.almalinux.org/9/ALSA-2024-0675.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44441](https://access.redhat.com/security/cve/CVE-2023-44441), [CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44443](https://access.redhat.com/security/cve/CVE-2023-44443), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
gimp-libs-debuginfo | 2.99.8-4.el9_3 | |
osbuild | 93-1.el9_3.1.alma.1 | |
osbuild-composer | 88.3-1.el9_3.alma.2 | |
osbuild-composer-core | 88.3-1.el9_3.alma.2 | |
osbuild-composer-core-debuginfo | 88.3-1.el9_3.alma.2 | |
osbuild-composer-debuginfo | 88.3-1.el9_3.alma.2 | |
osbuild-composer-debugsource | 88.3-1.el9_3.alma.2 | |
osbuild-composer-dnf-json | 88.3-1.el9_3.alma.2 | |
osbuild-composer-tests-debuginfo | 88.3-1.el9_3.alma.2 | |
osbuild-composer-worker | 88.3-1.el9_3.alma.2 | |
osbuild-composer-worker-debuginfo | 88.3-1.el9_3.alma.2 | |
osbuild-luks2 | 93-1.el9_3.1.alma.1 | |
osbuild-lvm2 | 93-1.el9_3.1.alma.1 | |
osbuild-ostree | 93-1.el9_3.1.alma.1 | |
osbuild-selinux | 93-1.el9_3.1.alma.1 | |
python3-osbuild | 93-1.el9_3.1.alma.1 | |
selinux-policy-devel | 38.1.23-1.el9_3.2 | |
tzdata-java | 2024a-1.el9 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gimp-devel | 2.99.8-4.el9_3 | |
gimp-devel-tools | 2.99.8-4.el9_3 | |
gimp-devel-tools-debuginfo | 2.99.8-4.el9_3 | |
osbuild-composer-tests | 88.3-1.el9_3.alma.2 | |
osbuild-tools | 93-1.el9_3.1.alma.1 | |
selinux-policy-minimum | 38.1.23-1.el9_3.2 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20230808-2.20231009.1.el9_3 | |
selinux-policy | 38.1.23-1.el9_3.2 | |
selinux-policy-doc | 38.1.23-1.el9_3.2 | |
selinux-policy-mls | 38.1.23-1.el9_3.2 | |
selinux-policy-sandbox | 38.1.23-1.el9_3.2 | |
selinux-policy-targeted | 38.1.23-1.el9_3.2 | |
sos | 4.6.1-1.el9.alma.1 | |
sos-audit | 4.6.1-1.el9.alma.1 | |
tzdata | 2024a-1.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gimp | 2.99.8-4.el9_3 | [ALSA-2024:0675](https://errata.almalinux.org/9/ALSA-2024-0675.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44441](https://access.redhat.com/security/cve/CVE-2023-44441), [CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44443](https://access.redhat.com/security/cve/CVE-2023-44443), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
gimp-debuginfo | 2.99.8-4.el9_3 | |
gimp-debugsource | 2.99.8-4.el9_3 | |
gimp-libs | 2.99.8-4.el9_3 | [ALSA-2024:0675](https://errata.almalinux.org/9/ALSA-2024-0675.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44441](https://access.redhat.com/security/cve/CVE-2023-44441), [CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44443](https://access.redhat.com/security/cve/CVE-2023-44443), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
gimp-libs-debuginfo | 2.99.8-4.el9_3 | |
osbuild | 93-1.el9_3.1.alma.1 | |
osbuild-composer | 88.3-1.el9_3.alma.2 | |
osbuild-composer-core | 88.3-1.el9_3.alma.2 | |
osbuild-composer-core-debuginfo | 88.3-1.el9_3.alma.2 | |
osbuild-composer-debuginfo | 88.3-1.el9_3.alma.2 | |
osbuild-composer-debugsource | 88.3-1.el9_3.alma.2 | |
osbuild-composer-dnf-json | 88.3-1.el9_3.alma.2 | |
osbuild-composer-tests-debuginfo | 88.3-1.el9_3.alma.2 | |
osbuild-composer-worker | 88.3-1.el9_3.alma.2 | |
osbuild-composer-worker-debuginfo | 88.3-1.el9_3.alma.2 | |
osbuild-luks2 | 93-1.el9_3.1.alma.1 | |
osbuild-lvm2 | 93-1.el9_3.1.alma.1 | |
osbuild-ostree | 93-1.el9_3.1.alma.1 | |
osbuild-selinux | 93-1.el9_3.1.alma.1 | |
python3-osbuild | 93-1.el9_3.1.alma.1 | |
selinux-policy-devel | 38.1.23-1.el9_3.2 | |
tzdata-java | 2024a-1.el9 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gimp-devel | 2.99.8-4.el9_3 | |
gimp-devel-tools | 2.99.8-4.el9_3 | |
gimp-devel-tools-debuginfo | 2.99.8-4.el9_3 | |
osbuild-composer-tests | 88.3-1.el9_3.alma.2 | |
osbuild-tools | 93-1.el9_3.1.alma.1 | |
selinux-policy-minimum | 38.1.23-1.el9_3.2 | |

