## 2023-03-15

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.5-2.al9.cern | |
cern-sssd-conf-domain-cernch | 1.5-2.al9.cern | |
cern-sssd-conf-global | 1.5-2.al9.cern | |
cern-sssd-conf-global-cernch | 1.5-2.al9.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.5-2.al9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gnutls | 3.7.6-18.el9_1 | [RHSA-2023:1141](https://access.redhat.com/errata/RHSA-2023:1141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0361](https://access.redhat.com/security/cve/CVE-2023-0361))
gnutls-c++-debuginfo | 3.7.6-18.el9_1 | |
gnutls-dane-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debugsource | 3.7.6-18.el9_1 | |
gnutls-utils-debuginfo | 3.7.6-18.el9_1 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gnutls-c++ | 3.7.6-18.el9_1 | [RHSA-2023:1141](https://access.redhat.com/errata/RHSA-2023:1141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0361](https://access.redhat.com/security/cve/CVE-2023-0361))
gnutls-c++-debuginfo | 3.7.6-18.el9_1 | |
gnutls-dane | 3.7.6-18.el9_1 | [RHSA-2023:1141](https://access.redhat.com/errata/RHSA-2023:1141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0361](https://access.redhat.com/security/cve/CVE-2023-0361))
gnutls-dane-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debugsource | 3.7.6-18.el9_1 | |
gnutls-devel | 3.7.6-18.el9_1 | [RHSA-2023:1141](https://access.redhat.com/errata/RHSA-2023:1141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0361](https://access.redhat.com/security/cve/CVE-2023-0361))
gnutls-utils | 3.7.6-18.el9_1 | [RHSA-2023:1141](https://access.redhat.com/errata/RHSA-2023:1141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0361](https://access.redhat.com/security/cve/CVE-2023-0361))
gnutls-utils-debuginfo | 3.7.6-18.el9_1 | |
libjpeg-turbo | 2.0.90-6.el9_1 | [RHSA-2023:1068](https://access.redhat.com/errata/RHSA-2023:1068) | <div class="adv_s">Security Advisory</div> ([CVE-2021-46822](https://access.redhat.com/security/cve/CVE-2021-46822))
libjpeg-turbo-debuginfo | 2.0.90-6.el9_1 | |
libjpeg-turbo-debugsource | 2.0.90-6.el9_1 | |
libjpeg-turbo-devel | 2.0.90-6.el9_1 | [RHSA-2023:1068](https://access.redhat.com/errata/RHSA-2023:1068) | <div class="adv_s">Security Advisory</div> ([CVE-2021-46822](https://access.redhat.com/security/cve/CVE-2021-46822))
libjpeg-turbo-utils | 2.0.90-6.el9_1 | [RHSA-2023:1068](https://access.redhat.com/errata/RHSA-2023:1068) | <div class="adv_s">Security Advisory</div> ([CVE-2021-46822](https://access.redhat.com/security/cve/CVE-2021-46822))
libjpeg-turbo-utils-debuginfo | 2.0.90-6.el9_1 | |
pesign | 115-6.el9_1 | [RHSA-2023:1067](https://access.redhat.com/errata/RHSA-2023:1067) | <div class="adv_s">Security Advisory</div> ([CVE-2022-3560](https://access.redhat.com/security/cve/CVE-2022-3560))
pesign-debuginfo | 115-6.el9_1 | |
pesign-debugsource | 115-6.el9_1 | |
turbojpeg-debuginfo | 2.0.90-6.el9_1 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libjpeg-turbo-debuginfo | 2.0.90-6.el9_1 | |
libjpeg-turbo-debugsource | 2.0.90-6.el9_1 | |
libjpeg-turbo-utils-debuginfo | 2.0.90-6.el9_1 | |
turbojpeg | 2.0.90-6.el9_1 | [RHSA-2023:1068](https://access.redhat.com/errata/RHSA-2023:1068) | <div class="adv_s">Security Advisory</div> ([CVE-2021-46822](https://access.redhat.com/security/cve/CVE-2021-46822))
turbojpeg-debuginfo | 2.0.90-6.el9_1 | |
turbojpeg-devel | 2.0.90-6.el9_1 | [RHSA-2023:1068](https://access.redhat.com/errata/RHSA-2023:1068) | <div class="adv_s">Security Advisory</div> ([CVE-2021-46822](https://access.redhat.com/security/cve/CVE-2021-46822))

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.5-2.al9.cern | |
cern-sssd-conf-domain-cernch | 1.5-2.al9.cern | |
cern-sssd-conf-global | 1.5-2.al9.cern | |
cern-sssd-conf-global-cernch | 1.5-2.al9.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.5-2.al9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gnutls | 3.7.6-18.el9_1 | [RHSA-2023:1141](https://access.redhat.com/errata/RHSA-2023:1141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0361](https://access.redhat.com/security/cve/CVE-2023-0361))
gnutls-c++-debuginfo | 3.7.6-18.el9_1 | |
gnutls-dane-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debugsource | 3.7.6-18.el9_1 | |
gnutls-utils-debuginfo | 3.7.6-18.el9_1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gnutls-c++ | 3.7.6-18.el9_1 | [RHSA-2023:1141](https://access.redhat.com/errata/RHSA-2023:1141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0361](https://access.redhat.com/security/cve/CVE-2023-0361))
gnutls-c++-debuginfo | 3.7.6-18.el9_1 | |
gnutls-dane | 3.7.6-18.el9_1 | [RHSA-2023:1141](https://access.redhat.com/errata/RHSA-2023:1141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0361](https://access.redhat.com/security/cve/CVE-2023-0361))
gnutls-dane-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debugsource | 3.7.6-18.el9_1 | |
gnutls-devel | 3.7.6-18.el9_1 | [RHSA-2023:1141](https://access.redhat.com/errata/RHSA-2023:1141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0361](https://access.redhat.com/security/cve/CVE-2023-0361))
gnutls-utils | 3.7.6-18.el9_1 | [RHSA-2023:1141](https://access.redhat.com/errata/RHSA-2023:1141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0361](https://access.redhat.com/security/cve/CVE-2023-0361))
gnutls-utils-debuginfo | 3.7.6-18.el9_1 | |
libjpeg-turbo | 2.0.90-6.el9_1 | [RHSA-2023:1068](https://access.redhat.com/errata/RHSA-2023:1068) | <div class="adv_s">Security Advisory</div> ([CVE-2021-46822](https://access.redhat.com/security/cve/CVE-2021-46822))
libjpeg-turbo-debuginfo | 2.0.90-6.el9_1 | |
libjpeg-turbo-debugsource | 2.0.90-6.el9_1 | |
libjpeg-turbo-devel | 2.0.90-6.el9_1 | [RHSA-2023:1068](https://access.redhat.com/errata/RHSA-2023:1068) | <div class="adv_s">Security Advisory</div> ([CVE-2021-46822](https://access.redhat.com/security/cve/CVE-2021-46822))
libjpeg-turbo-utils | 2.0.90-6.el9_1 | [RHSA-2023:1068](https://access.redhat.com/errata/RHSA-2023:1068) | <div class="adv_s">Security Advisory</div> ([CVE-2021-46822](https://access.redhat.com/security/cve/CVE-2021-46822))
libjpeg-turbo-utils-debuginfo | 2.0.90-6.el9_1 | |
pesign | 115-6.el9_1 | [RHSA-2023:1067](https://access.redhat.com/errata/RHSA-2023:1067) | <div class="adv_s">Security Advisory</div> ([CVE-2022-3560](https://access.redhat.com/security/cve/CVE-2022-3560))
pesign-debuginfo | 115-6.el9_1 | |
pesign-debugsource | 115-6.el9_1 | |
turbojpeg-debuginfo | 2.0.90-6.el9_1 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libjpeg-turbo-debuginfo | 2.0.90-6.el9_1 | |
libjpeg-turbo-debugsource | 2.0.90-6.el9_1 | |
libjpeg-turbo-utils-debuginfo | 2.0.90-6.el9_1 | |
turbojpeg | 2.0.90-6.el9_1 | [RHSA-2023:1068](https://access.redhat.com/errata/RHSA-2023:1068) | <div class="adv_s">Security Advisory</div> ([CVE-2021-46822](https://access.redhat.com/security/cve/CVE-2021-46822))
turbojpeg-debuginfo | 2.0.90-6.el9_1 | |
turbojpeg-devel | 2.0.90-6.el9_1 | [RHSA-2023:1068](https://access.redhat.com/errata/RHSA-2023:1068) | <div class="adv_s">Security Advisory</div> ([CVE-2021-46822](https://access.redhat.com/security/cve/CVE-2021-46822))

