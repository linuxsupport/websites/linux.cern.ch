## 2024-03-27

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.28-2.el9_3 | |
aspnetcore-runtime-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-runtime-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-targeting-pack-6.0 | 6.0.28-2.el9_3 | |
aspnetcore-targeting-pack-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-targeting-pack-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dnsmasq | 2.85-14.el9_3.1 | [ALSA-2024:1334](https://errata.almalinux.org/9/ALSA-2024-1334.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
dnsmasq-debuginfo | 2.85-14.el9_3.1 | |
dnsmasq-debugsource | 2.85-14.el9_3.1 | |
dnsmasq-utils | 2.85-14.el9_3.1 | [ALSA-2024:1334](https://errata.almalinux.org/9/ALSA-2024-1334.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
dnsmasq-utils-debuginfo | 2.85-14.el9_3.1 | |
dotnet-apphost-pack-6.0 | 6.0.28-2.el9_3 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.28-2.el9_3 | |
dotnet-apphost-pack-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-apphost-pack-7.0-debuginfo | 7.0.17-1.el9_3 | |
dotnet-apphost-pack-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-apphost-pack-8.0-debuginfo | 8.0.3-2.el9_3 | |
dotnet-host | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-host-debuginfo | 8.0.3-2.el9_3 | |
dotnet-hostfxr-6.0 | 6.0.28-2.el9_3 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.28-2.el9_3 | |
dotnet-hostfxr-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-hostfxr-7.0-debuginfo | 7.0.17-1.el9_3 | |
dotnet-hostfxr-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-hostfxr-8.0-debuginfo | 8.0.3-2.el9_3 | |
dotnet-runtime-6.0 | 6.0.28-2.el9_3 | |
dotnet-runtime-6.0-debuginfo | 6.0.28-2.el9_3 | |
dotnet-runtime-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-runtime-7.0-debuginfo | 7.0.17-1.el9_3 | |
dotnet-runtime-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-runtime-8.0-debuginfo | 8.0.3-2.el9_3 | |
dotnet-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-6.0 | 6.0.128-2.el9_3 | |
dotnet-sdk-6.0-debuginfo | 6.0.128-2.el9_3 | |
dotnet-sdk-7.0 | 7.0.117-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-7.0-debuginfo | 7.0.117-1.el9_3 | |
dotnet-sdk-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-8.0-debuginfo | 8.0.103-2.el9_3 | |
dotnet-sdk-dbg-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-targeting-pack-6.0 | 6.0.28-2.el9_3 | |
dotnet-targeting-pack-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-targeting-pack-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-templates-6.0 | 6.0.128-2.el9_3 | |
dotnet-templates-7.0 | 7.0.117-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-templates-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet6.0-debuginfo | 6.0.128-2.el9_3 | |
dotnet6.0-debugsource | 6.0.128-2.el9_3 | |
dotnet7.0-debuginfo | 7.0.117-1.el9_3 | |
dotnet7.0-debugsource | 7.0.117-1.el9_3 | |
dotnet8.0-debuginfo | 8.0.103-2.el9_3 | |
dotnet8.0-debugsource | 8.0.103-2.el9_3 | |
ipa-client | 4.10.2-8.el9_3.alma.1 | |
ipa-client-common | 4.10.2-8.el9_3.alma.1 | |
ipa-client-debuginfo | 4.10.2-8.el9_3.alma.1 | |
ipa-client-epn | 4.10.2-8.el9_3.alma.1 | |
ipa-client-samba | 4.10.2-8.el9_3.alma.1 | |
ipa-common | 4.10.2-8.el9_3.alma.1 | |
ipa-debuginfo | 4.10.2-8.el9_3.alma.1 | |
ipa-debugsource | 4.10.2-8.el9_3.alma.1 | |
ipa-selinux | 4.10.2-8.el9_3.alma.1 | |
ipa-server | 4.10.2-8.el9_3.alma.1 | |
ipa-server-common | 4.10.2-8.el9_3.alma.1 | |
ipa-server-debuginfo | 4.10.2-8.el9_3.alma.1 | |
ipa-server-dns | 4.10.2-8.el9_3.alma.1 | |
ipa-server-trust-ad | 4.10.2-8.el9_3.alma.1 | |
ipa-server-trust-ad-debuginfo | 4.10.2-8.el9_3.alma.1 | |
netstandard-targeting-pack-2.1 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
python3-ipaclient | 4.10.2-8.el9_3.alma.1 | |
python3-ipalib | 4.10.2-8.el9_3.alma.1 | |
python3-ipaserver | 4.10.2-8.el9_3.alma.1 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.128-2.el9_3 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.117-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-8.0-source-built-artifacts | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
python3-ipatests | 4.10.2-8.el9_3.alma.1 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-dbg-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
ipa-python-compat | 4.10.2-8.el9_3.alma.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.28-2.el9_3 | |
aspnetcore-runtime-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-runtime-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-targeting-pack-6.0 | 6.0.28-2.el9_3 | |
aspnetcore-targeting-pack-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
aspnetcore-targeting-pack-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dnsmasq | 2.85-14.el9_3.1 | [ALSA-2024:1334](https://errata.almalinux.org/9/ALSA-2024-1334.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
dnsmasq-debuginfo | 2.85-14.el9_3.1 | |
dnsmasq-debugsource | 2.85-14.el9_3.1 | |
dnsmasq-utils | 2.85-14.el9_3.1 | [ALSA-2024:1334](https://errata.almalinux.org/9/ALSA-2024-1334.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
dnsmasq-utils-debuginfo | 2.85-14.el9_3.1 | |
dotnet-apphost-pack-6.0 | 6.0.28-2.el9_3 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.28-2.el9_3 | |
dotnet-apphost-pack-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-apphost-pack-7.0-debuginfo | 7.0.17-1.el9_3 | |
dotnet-apphost-pack-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-apphost-pack-8.0-debuginfo | 8.0.3-2.el9_3 | |
dotnet-host | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-host-debuginfo | 8.0.3-2.el9_3 | |
dotnet-hostfxr-6.0 | 6.0.28-2.el9_3 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.28-2.el9_3 | |
dotnet-hostfxr-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-hostfxr-7.0-debuginfo | 7.0.17-1.el9_3 | |
dotnet-hostfxr-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-hostfxr-8.0-debuginfo | 8.0.3-2.el9_3 | |
dotnet-runtime-6.0 | 6.0.28-2.el9_3 | |
dotnet-runtime-6.0-debuginfo | 6.0.28-2.el9_3 | |
dotnet-runtime-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-runtime-7.0-debuginfo | 7.0.17-1.el9_3 | |
dotnet-runtime-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-runtime-8.0-debuginfo | 8.0.3-2.el9_3 | |
dotnet-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-6.0 | 6.0.128-2.el9_3 | |
dotnet-sdk-6.0-debuginfo | 6.0.128-2.el9_3 | |
dotnet-sdk-7.0 | 7.0.117-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-7.0-debuginfo | 7.0.117-1.el9_3 | |
dotnet-sdk-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-8.0-debuginfo | 8.0.103-2.el9_3 | |
dotnet-sdk-dbg-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-targeting-pack-6.0 | 6.0.28-2.el9_3 | |
dotnet-targeting-pack-7.0 | 7.0.17-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-targeting-pack-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-templates-6.0 | 6.0.128-2.el9_3 | |
dotnet-templates-7.0 | 7.0.117-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-templates-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet6.0-debuginfo | 6.0.128-2.el9_3 | |
dotnet6.0-debugsource | 6.0.128-2.el9_3 | |
dotnet7.0-debuginfo | 7.0.117-1.el9_3 | |
dotnet7.0-debugsource | 7.0.117-1.el9_3 | |
dotnet8.0-debuginfo | 8.0.103-2.el9_3 | |
dotnet8.0-debugsource | 8.0.103-2.el9_3 | |
ipa-client | 4.10.2-8.el9_3.alma.1 | |
ipa-client-common | 4.10.2-8.el9_3.alma.1 | |
ipa-client-debuginfo | 4.10.2-8.el9_3.alma.1 | |
ipa-client-epn | 4.10.2-8.el9_3.alma.1 | |
ipa-client-samba | 4.10.2-8.el9_3.alma.1 | |
ipa-common | 4.10.2-8.el9_3.alma.1 | |
ipa-debuginfo | 4.10.2-8.el9_3.alma.1 | |
ipa-debugsource | 4.10.2-8.el9_3.alma.1 | |
ipa-selinux | 4.10.2-8.el9_3.alma.1 | |
ipa-server | 4.10.2-8.el9_3.alma.1 | |
ipa-server-common | 4.10.2-8.el9_3.alma.1 | |
ipa-server-debuginfo | 4.10.2-8.el9_3.alma.1 | |
ipa-server-dns | 4.10.2-8.el9_3.alma.1 | |
ipa-server-trust-ad | 4.10.2-8.el9_3.alma.1 | |
ipa-server-trust-ad-debuginfo | 4.10.2-8.el9_3.alma.1 | |
netstandard-targeting-pack-2.1 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
python3-ipaclient | 4.10.2-8.el9_3.alma.1 | |
python3-ipalib | 4.10.2-8.el9_3.alma.1 | |
python3-ipaserver | 4.10.2-8.el9_3.alma.1 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.128-2.el9_3 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.117-1.el9_3 | [ALSA-2024:1309](https://errata.almalinux.org/9/ALSA-2024-1309.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-8.0-source-built-artifacts | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
python3-ipatests | 4.10.2-8.el9_3.alma.1 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-runtime-dbg-8.0 | 8.0.3-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
dotnet-sdk-dbg-8.0 | 8.0.103-2.el9_3 | [ALSA-2024:1310](https://errata.almalinux.org/9/ALSA-2024-1310.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21392](https://access.redhat.com/security/cve/CVE-2024-21392))
ipa-python-compat | 4.10.2-8.el9_3.alma.1 | |

