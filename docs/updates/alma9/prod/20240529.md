## 2024-05-29

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.30-1.el9_4 | |
aspnetcore-runtime-7.0 | 7.0.19-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-runtime-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-runtime-dbg-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-targeting-pack-6.0 | 6.0.30-1.el9_4 | |
aspnetcore-targeting-pack-7.0 | 7.0.19-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-targeting-pack-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-apphost-pack-6.0 | 6.0.30-1.el9_4 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-apphost-pack-7.0 | 7.0.19-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-apphost-pack-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-apphost-pack-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-apphost-pack-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-host | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-host-debuginfo | 8.0.5-1.el9_4 | |
dotnet-hostfxr-6.0 | 6.0.30-1.el9_4 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-hostfxr-7.0 | 7.0.19-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-hostfxr-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-hostfxr-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-hostfxr-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-runtime-6.0 | 6.0.30-1.el9_4 | |
dotnet-runtime-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-runtime-7.0 | 7.0.19-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-runtime-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-runtime-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-runtime-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-runtime-dbg-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-6.0 | 6.0.130-1.el9_4 | |
dotnet-sdk-6.0-debuginfo | 6.0.130-1.el9_4 | |
dotnet-sdk-7.0 | 7.0.119-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-7.0-debuginfo | 7.0.119-1.el9_4 | |
dotnet-sdk-8.0 | 8.0.105-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-8.0-debuginfo | 8.0.105-1.el9_4 | |
dotnet-sdk-dbg-8.0 | 8.0.105-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-targeting-pack-6.0 | 6.0.30-1.el9_4 | |
dotnet-targeting-pack-7.0 | 7.0.19-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-targeting-pack-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-templates-6.0 | 6.0.130-1.el9_4 | |
dotnet-templates-7.0 | 7.0.119-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-templates-8.0 | 8.0.105-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet6.0-debuginfo | 6.0.130-1.el9_4 | |
dotnet6.0-debugsource | 6.0.130-1.el9_4 | |
dotnet7.0-debuginfo | 7.0.119-1.el9_4 | |
dotnet7.0-debugsource | 7.0.119-1.el9_4 | |
dotnet8.0-debuginfo | 8.0.105-1.el9_4 | |
dotnet8.0-debugsource | 8.0.105-1.el9_4 | |
firefox | 115.11.0-1.el9_4.alma.1 | [ALSA-2024:2883](https://errata.almalinux.org/9/ALSA-2024-2883.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4367](https://access.redhat.com/security/cve/CVE-2024-4367), [CVE-2024-4767](https://access.redhat.com/security/cve/CVE-2024-4767), [CVE-2024-4768](https://access.redhat.com/security/cve/CVE-2024-4768), [CVE-2024-4769](https://access.redhat.com/security/cve/CVE-2024-4769), [CVE-2024-4770](https://access.redhat.com/security/cve/CVE-2024-4770), [CVE-2024-4777](https://access.redhat.com/security/cve/CVE-2024-4777))
firefox-debuginfo | 115.11.0-1.el9_4.alma.1 | |
firefox-debugsource | 115.11.0-1.el9_4.alma.1 | |
firefox-x11 | 115.11.0-1.el9_4.alma.1 | [ALSA-2024:2883](https://errata.almalinux.org/9/ALSA-2024-2883.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4367](https://access.redhat.com/security/cve/CVE-2024-4367), [CVE-2024-4767](https://access.redhat.com/security/cve/CVE-2024-4767), [CVE-2024-4768](https://access.redhat.com/security/cve/CVE-2024-4768), [CVE-2024-4769](https://access.redhat.com/security/cve/CVE-2024-4769), [CVE-2024-4770](https://access.redhat.com/security/cve/CVE-2024-4770), [CVE-2024-4777](https://access.redhat.com/security/cve/CVE-2024-4777))
netstandard-targeting-pack-2.1 | 8.0.105-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
nodejs | 16.20.2-8.el9_4 | [ALSA-2024:2910](https://errata.almalinux.org/9/ALSA-2024-2910.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs | 20.12.2-2.module_el9.4.0+100+71fc9528 | [ALSA-2024:2853](https://errata.almalinux.org/9/ALSA-2024-2853.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-debuginfo | 16.20.2-8.el9_4 | |
nodejs-debuginfo | 20.12.2-2.module_el9.4.0+100+71fc9528 | |
nodejs-debugsource | 16.20.2-8.el9_4 | |
nodejs-debugsource | 20.12.2-2.module_el9.4.0+100+71fc9528 | |
nodejs-devel | 20.12.2-2.module_el9.4.0+100+71fc9528 | [ALSA-2024:2853](https://errata.almalinux.org/9/ALSA-2024-2853.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-docs | 16.20.2-8.el9_4 | [ALSA-2024:2910](https://errata.almalinux.org/9/ALSA-2024-2910.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-docs | 20.12.2-2.module_el9.4.0+100+71fc9528 | [ALSA-2024:2853](https://errata.almalinux.org/9/ALSA-2024-2853.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-full-i18n | 16.20.2-8.el9_4 | [ALSA-2024:2910](https://errata.almalinux.org/9/ALSA-2024-2910.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-full-i18n | 20.12.2-2.module_el9.4.0+100+71fc9528 | [ALSA-2024:2853](https://errata.almalinux.org/9/ALSA-2024-2853.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-libs | 16.20.2-8.el9_4 | [ALSA-2024:2910](https://errata.almalinux.org/9/ALSA-2024-2910.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-libs-debuginfo | 16.20.2-8.el9_4 | |
npm | 10.5.0-1.20.12.2.2.module_el9.4.0+100+71fc9528 | [ALSA-2024:2853](https://errata.almalinux.org/9/ALSA-2024-2853.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
npm | 8.19.4-1.16.20.2.8.el9_4 | [ALSA-2024:2910](https://errata.almalinux.org/9/ALSA-2024-2910.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
thunderbird | 115.11.0-1.el9_4.alma.1 | [ALSA-2024:2888](https://errata.almalinux.org/9/ALSA-2024-2888.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4367](https://access.redhat.com/security/cve/CVE-2024-4367), [CVE-2024-4767](https://access.redhat.com/security/cve/CVE-2024-4767), [CVE-2024-4768](https://access.redhat.com/security/cve/CVE-2024-4768), [CVE-2024-4769](https://access.redhat.com/security/cve/CVE-2024-4769), [CVE-2024-4770](https://access.redhat.com/security/cve/CVE-2024-4770), [CVE-2024-4777](https://access.redhat.com/security/cve/CVE-2024-4777))
thunderbird-debuginfo | 115.11.0-1.el9_4.alma.1 | |
thunderbird-debugsource | 115.11.0-1.el9_4.alma.1 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.130-1.el9_4 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.119-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-8.0-source-built-artifacts | 8.0.105-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-dbg-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-runtime-dbg-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-dbg-8.0 | 8.0.105-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
nodejs-devel | 16.20.2-8.el9_4 | |
v8-devel | 9.4.146.26-1.16.20.2.8.el9_4 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.30-1.el9_4 | |
aspnetcore-runtime-7.0 | 7.0.19-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-runtime-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-runtime-dbg-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-targeting-pack-6.0 | 6.0.30-1.el9_4 | |
aspnetcore-targeting-pack-7.0 | 7.0.19-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-targeting-pack-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-apphost-pack-6.0 | 6.0.30-1.el9_4 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-apphost-pack-7.0 | 7.0.19-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-apphost-pack-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-apphost-pack-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-apphost-pack-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-host | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-host-debuginfo | 8.0.5-1.el9_4 | |
dotnet-hostfxr-6.0 | 6.0.30-1.el9_4 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-hostfxr-7.0 | 7.0.19-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-hostfxr-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-hostfxr-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-hostfxr-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-runtime-6.0 | 6.0.30-1.el9_4 | |
dotnet-runtime-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-runtime-7.0 | 7.0.19-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-runtime-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-runtime-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-runtime-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-runtime-dbg-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-6.0 | 6.0.130-1.el9_4 | |
dotnet-sdk-6.0-debuginfo | 6.0.130-1.el9_4 | |
dotnet-sdk-7.0 | 7.0.119-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-7.0-debuginfo | 7.0.119-1.el9_4 | |
dotnet-sdk-8.0 | 8.0.105-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-8.0-debuginfo | 8.0.105-1.el9_4 | |
dotnet-sdk-dbg-8.0 | 8.0.105-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-targeting-pack-6.0 | 6.0.30-1.el9_4 | |
dotnet-targeting-pack-7.0 | 7.0.19-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-targeting-pack-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-templates-6.0 | 6.0.130-1.el9_4 | |
dotnet-templates-7.0 | 7.0.119-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-templates-8.0 | 8.0.105-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet6.0-debuginfo | 6.0.130-1.el9_4 | |
dotnet6.0-debugsource | 6.0.130-1.el9_4 | |
dotnet7.0-debuginfo | 7.0.119-1.el9_4 | |
dotnet7.0-debugsource | 7.0.119-1.el9_4 | |
dotnet8.0-debuginfo | 8.0.105-1.el9_4 | |
dotnet8.0-debugsource | 8.0.105-1.el9_4 | |
firefox | 115.11.0-1.el9_4.alma.1 | [ALSA-2024:2883](https://errata.almalinux.org/9/ALSA-2024-2883.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4367](https://access.redhat.com/security/cve/CVE-2024-4367), [CVE-2024-4767](https://access.redhat.com/security/cve/CVE-2024-4767), [CVE-2024-4768](https://access.redhat.com/security/cve/CVE-2024-4768), [CVE-2024-4769](https://access.redhat.com/security/cve/CVE-2024-4769), [CVE-2024-4770](https://access.redhat.com/security/cve/CVE-2024-4770), [CVE-2024-4777](https://access.redhat.com/security/cve/CVE-2024-4777))
firefox-debuginfo | 115.11.0-1.el9_4.alma.1 | |
firefox-debugsource | 115.11.0-1.el9_4.alma.1 | |
firefox-x11 | 115.11.0-1.el9_4.alma.1 | [ALSA-2024:2883](https://errata.almalinux.org/9/ALSA-2024-2883.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4367](https://access.redhat.com/security/cve/CVE-2024-4367), [CVE-2024-4767](https://access.redhat.com/security/cve/CVE-2024-4767), [CVE-2024-4768](https://access.redhat.com/security/cve/CVE-2024-4768), [CVE-2024-4769](https://access.redhat.com/security/cve/CVE-2024-4769), [CVE-2024-4770](https://access.redhat.com/security/cve/CVE-2024-4770), [CVE-2024-4777](https://access.redhat.com/security/cve/CVE-2024-4777))
netstandard-targeting-pack-2.1 | 8.0.105-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
nodejs | 16.20.2-8.el9_4 | [ALSA-2024:2910](https://errata.almalinux.org/9/ALSA-2024-2910.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs | 20.12.2-2.module_el9.4.0+100+71fc9528 | [ALSA-2024:2853](https://errata.almalinux.org/9/ALSA-2024-2853.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-debuginfo | 16.20.2-8.el9_4 | |
nodejs-debuginfo | 20.12.2-2.module_el9.4.0+100+71fc9528 | |
nodejs-debugsource | 16.20.2-8.el9_4 | |
nodejs-debugsource | 20.12.2-2.module_el9.4.0+100+71fc9528 | |
nodejs-devel | 20.12.2-2.module_el9.4.0+100+71fc9528 | [ALSA-2024:2853](https://errata.almalinux.org/9/ALSA-2024-2853.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-docs | 16.20.2-8.el9_4 | [ALSA-2024:2910](https://errata.almalinux.org/9/ALSA-2024-2910.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-docs | 20.12.2-2.module_el9.4.0+100+71fc9528 | [ALSA-2024:2853](https://errata.almalinux.org/9/ALSA-2024-2853.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-full-i18n | 16.20.2-8.el9_4 | [ALSA-2024:2910](https://errata.almalinux.org/9/ALSA-2024-2910.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-full-i18n | 20.12.2-2.module_el9.4.0+100+71fc9528 | [ALSA-2024:2853](https://errata.almalinux.org/9/ALSA-2024-2853.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-libs | 16.20.2-8.el9_4 | [ALSA-2024:2910](https://errata.almalinux.org/9/ALSA-2024-2910.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-libs-debuginfo | 16.20.2-8.el9_4 | |
npm | 10.5.0-1.20.12.2.2.module_el9.4.0+100+71fc9528 | [ALSA-2024:2853](https://errata.almalinux.org/9/ALSA-2024-2853.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
npm | 8.19.4-1.16.20.2.8.el9_4 | [ALSA-2024:2910](https://errata.almalinux.org/9/ALSA-2024-2910.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
thunderbird | 115.11.0-1.el9_4.alma.1 | [ALSA-2024:2888](https://errata.almalinux.org/9/ALSA-2024-2888.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4367](https://access.redhat.com/security/cve/CVE-2024-4367), [CVE-2024-4767](https://access.redhat.com/security/cve/CVE-2024-4767), [CVE-2024-4768](https://access.redhat.com/security/cve/CVE-2024-4768), [CVE-2024-4769](https://access.redhat.com/security/cve/CVE-2024-4769), [CVE-2024-4770](https://access.redhat.com/security/cve/CVE-2024-4770), [CVE-2024-4777](https://access.redhat.com/security/cve/CVE-2024-4777))
thunderbird-debuginfo | 115.11.0-1.el9_4.alma.1 | |
thunderbird-debugsource | 115.11.0-1.el9_4.alma.1 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.130-1.el9_4 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.119-1.el9_4 | [ALSA-2024:2843](https://errata.almalinux.org/9/ALSA-2024-2843.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-8.0-source-built-artifacts | 8.0.105-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-dbg-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-runtime-dbg-8.0 | 8.0.5-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-dbg-8.0 | 8.0.105-1.el9_4 | [ALSA-2024:2842](https://errata.almalinux.org/9/ALSA-2024-2842.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
nodejs-devel | 16.20.2-8.el9_4 | |
v8-devel | 9.4.146.26-1.16.20.2.8.el9_4 | |

