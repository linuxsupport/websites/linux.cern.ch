## 2024-11-15


Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libsoup | 2.62.2 | [RHSA-2024:9654](https://access.redhat.com/errata/RHSA-2024:9654) | <div class="adv_s">Security Advisory</div> ([CVE-2024-52530](https://access.redhat.com/security/cve/CVE-2024-52530))
libsoup-devel | 2.62.2 | [RHSA-2024:9654](https://access.redhat.com/errata/RHSA-2024:9654) | <div class="adv_s">Security Advisory</div> ([CVE-2024-52530](https://access.redhat.com/security/cve/CVE-2024-52530))
