## 2025-02-12


Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 3.10.0 | [RHSA-2025:1281](https://access.redhat.com/errata/RHSA-2025:1281) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53104](https://access.redhat.com/security/cve/CVE-2024-53104))
doxygen | 1.8.5 | [RHSA-2025:1255](https://access.redhat.com/errata/RHSA-2025:1255) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
doxygen-doxywizard | 1.8.5 | |
doxygen-latex | 1.8.5 | |
kernel | 3.10.0 | [RHSA-2025:1281](https://access.redhat.com/errata/RHSA-2025:1281) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53104](https://access.redhat.com/security/cve/CVE-2024-53104))
kernel-abi-whitelists | 3.10.0 | [RHSA-2025:1281](https://access.redhat.com/errata/RHSA-2025:1281) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53104](https://access.redhat.com/security/cve/CVE-2024-53104))
kernel-debug | 3.10.0 | [RHSA-2025:1281](https://access.redhat.com/errata/RHSA-2025:1281) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53104](https://access.redhat.com/security/cve/CVE-2024-53104))
kernel-debug-devel | 3.10.0 | [RHSA-2025:1281](https://access.redhat.com/errata/RHSA-2025:1281) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53104](https://access.redhat.com/security/cve/CVE-2024-53104))
kernel-devel | 3.10.0 | [RHSA-2025:1281](https://access.redhat.com/errata/RHSA-2025:1281) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53104](https://access.redhat.com/security/cve/CVE-2024-53104))
kernel-doc | 3.10.0 | [RHSA-2025:1281](https://access.redhat.com/errata/RHSA-2025:1281) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53104](https://access.redhat.com/security/cve/CVE-2024-53104))
kernel-headers | 3.10.0 | [RHSA-2025:1281](https://access.redhat.com/errata/RHSA-2025:1281) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53104](https://access.redhat.com/security/cve/CVE-2024-53104))
kernel-tools | 3.10.0 | [RHSA-2025:1281](https://access.redhat.com/errata/RHSA-2025:1281) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53104](https://access.redhat.com/security/cve/CVE-2024-53104))
kernel-tools-libs | 3.10.0 | [RHSA-2025:1281](https://access.redhat.com/errata/RHSA-2025:1281) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53104](https://access.redhat.com/security/cve/CVE-2024-53104))
kernel-tools-libs-devel | 3.10.0 | |
perf | 3.10.0 | [RHSA-2025:1281](https://access.redhat.com/errata/RHSA-2025:1281) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53104](https://access.redhat.com/security/cve/CVE-2024-53104))
python-jinja2 | 2.7.2 | [RHSA-2025:1250](https://access.redhat.com/errata/RHSA-2025:1250) | <div class="adv_s">Security Advisory</div> ([CVE-2024-56326](https://access.redhat.com/security/cve/CVE-2024-56326))
python-perf | 3.10.0 | [RHSA-2025:1281](https://access.redhat.com/errata/RHSA-2025:1281) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53104](https://access.redhat.com/security/cve/CVE-2024-53104))
