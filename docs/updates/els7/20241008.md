## 2024-10-08


Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libgudev1 | 219 | [RHSA-2024:7705](https://access.redhat.com/errata/RHSA-2024:7705) | <div class="adv_s">Security Advisory</div> ([CVE-2023-26604](https://access.redhat.com/security/cve/CVE-2023-26604))
libgudev1-devel | 219 | [RHSA-2024:7705](https://access.redhat.com/errata/RHSA-2024:7705) | <div class="adv_s">Security Advisory</div> ([CVE-2023-26604](https://access.redhat.com/security/cve/CVE-2023-26604))
systemd | 219 | [RHSA-2024:7705](https://access.redhat.com/errata/RHSA-2024:7705) | <div class="adv_s">Security Advisory</div> ([CVE-2023-26604](https://access.redhat.com/security/cve/CVE-2023-26604))
systemd-devel | 219 | [RHSA-2024:7705](https://access.redhat.com/errata/RHSA-2024:7705) | <div class="adv_s">Security Advisory</div> ([CVE-2023-26604](https://access.redhat.com/security/cve/CVE-2023-26604))
systemd-journal-gateway | 219 | |
systemd-libs | 219 | [RHSA-2024:7705](https://access.redhat.com/errata/RHSA-2024:7705) | <div class="adv_s">Security Advisory</div> ([CVE-2023-26604](https://access.redhat.com/security/cve/CVE-2023-26604))
systemd-networkd | 219 | |
systemd-python | 219 | [RHSA-2024:7705](https://access.redhat.com/errata/RHSA-2024:7705) | <div class="adv_s">Security Advisory</div> ([CVE-2023-26604](https://access.redhat.com/security/cve/CVE-2023-26604))
systemd-resolved | 219 | |
systemd-sysv | 219 | [RHSA-2024:7705](https://access.redhat.com/errata/RHSA-2024:7705) | <div class="adv_s">Security Advisory</div> ([CVE-2023-26604](https://access.redhat.com/security/cve/CVE-2023-26604))
