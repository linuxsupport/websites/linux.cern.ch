## 2024-12-10


Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 128.5.1 | [RHSA-2024:10881](https://access.redhat.com/errata/RHSA-2024:10881) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11692](https://access.redhat.com/security/cve/CVE-2024-11692), [CVE-2024-11694](https://access.redhat.com/security/cve/CVE-2024-11694), [CVE-2024-11695](https://access.redhat.com/security/cve/CVE-2024-11695), [CVE-2024-11696](https://access.redhat.com/security/cve/CVE-2024-11696), [CVE-2024-11697](https://access.redhat.com/security/cve/CVE-2024-11697), [CVE-2024-11699](https://access.redhat.com/security/cve/CVE-2024-11699))
postgresql | 9.2.24 | [RHSA-2024:10882](https://access.redhat.com/errata/RHSA-2024:10882) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10979](https://access.redhat.com/security/cve/CVE-2024-10979))
postgresql-contrib | 9.2.24 | [RHSA-2024:10882](https://access.redhat.com/errata/RHSA-2024:10882) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10979](https://access.redhat.com/security/cve/CVE-2024-10979))
postgresql-devel | 9.2.24 | [RHSA-2024:10882](https://access.redhat.com/errata/RHSA-2024:10882) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10979](https://access.redhat.com/security/cve/CVE-2024-10979))
postgresql-docs | 9.2.24 | [RHSA-2024:10882](https://access.redhat.com/errata/RHSA-2024:10882) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10979](https://access.redhat.com/security/cve/CVE-2024-10979))
postgresql-libs | 9.2.24 | [RHSA-2024:10882](https://access.redhat.com/errata/RHSA-2024:10882) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10979](https://access.redhat.com/security/cve/CVE-2024-10979))
postgresql-plperl | 9.2.24 | [RHSA-2024:10882](https://access.redhat.com/errata/RHSA-2024:10882) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10979](https://access.redhat.com/security/cve/CVE-2024-10979))
postgresql-plpython | 9.2.24 | [RHSA-2024:10882](https://access.redhat.com/errata/RHSA-2024:10882) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10979](https://access.redhat.com/security/cve/CVE-2024-10979))
postgresql-pltcl | 9.2.24 | [RHSA-2024:10882](https://access.redhat.com/errata/RHSA-2024:10882) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10979](https://access.redhat.com/security/cve/CVE-2024-10979))
postgresql-server | 9.2.24 | [RHSA-2024:10882](https://access.redhat.com/errata/RHSA-2024:10882) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10979](https://access.redhat.com/security/cve/CVE-2024-10979))
postgresql-static | 9.2.24 | |
postgresql-test | 9.2.24 | [RHSA-2024:10882](https://access.redhat.com/errata/RHSA-2024:10882) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10979](https://access.redhat.com/security/cve/CVE-2024-10979))
postgresql-upgrade | 9.2.24 | |
