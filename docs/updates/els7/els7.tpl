# Latest Extended Lifecycle Support (ELS) Updates

For more information about ELS, please check: [ELS7](/els7/)

**Update types:**

* <div class="adv_s">[S] - security</div>
* <div class="adv_b">[B] - bug fix</div>
* <div class="adv_e">[E] - enhancement</div>
