## 2024-11-09


Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 2.1 | [RHEA-2024:9029](https://access.redhat.com/errata/RHEA-2024:9029) | <div class="adv_e">Product Enhancement Advisory</div>
