## 2025-01-23


Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-1.8.0-openjdk | 1.8.0.442.b06 | [RHBA-2025:0417](https://access.redhat.com/errata/RHBA-2025:0417) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-accessibility | 1.8.0.442.b06 | |
java-1.8.0-openjdk-accessibility-debug | 1.8.0.442.b06 | |
java-1.8.0-openjdk-debug | 1.8.0.442.b06 | |
java-1.8.0-openjdk-demo | 1.8.0.442.b06 | |
java-1.8.0-openjdk-demo-debug | 1.8.0.442.b06 | |
java-1.8.0-openjdk-devel | 1.8.0.442.b06 | [RHBA-2025:0417](https://access.redhat.com/errata/RHBA-2025:0417) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-devel-debug | 1.8.0.442.b06 | |
java-1.8.0-openjdk-headless | 1.8.0.442.b06 | [RHBA-2025:0417](https://access.redhat.com/errata/RHBA-2025:0417) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-headless-debug | 1.8.0.442.b06 | |
java-1.8.0-openjdk-javadoc | 1.8.0.442.b06 | |
java-1.8.0-openjdk-javadoc-debug | 1.8.0.442.b06 | |
java-1.8.0-openjdk-javadoc-zip | 1.8.0.442.b06 | |
java-1.8.0-openjdk-javadoc-zip-debug | 1.8.0.442.b06 | |
java-1.8.0-openjdk-src | 1.8.0.442.b06 | |
java-1.8.0-openjdk-src-debug | 1.8.0.442.b06 | |
