## 2024-12-13


Package | Version | Advisory | Notes
------- | ------- | -------- | -----
unbound | 1.6.6 | [RHSA-2024:11003](https://access.redhat.com/errata/RHSA-2024:11003) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
unbound-devel | 1.6.6 | |
unbound-libs | 1.6.6 | [RHSA-2024:11003](https://access.redhat.com/errata/RHSA-2024:11003) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
unbound-python | 1.6.6 | |
