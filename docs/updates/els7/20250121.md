## 2025-01-21


Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aide | 0.15.1 | [RHBA-2025:0447](https://access.redhat.com/errata/RHBA-2025:0447) | <div class="adv_b">Bug Fix Advisory</div>
iperf3 | 3.1.7 | [RHSA-2025:0402](https://access.redhat.com/errata/RHSA-2025:0402) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53580](https://access.redhat.com/security/cve/CVE-2024-53580))
iperf3-devel | 3.1.7 | |
microcode_ctl | 2.1 | [RHEA-2025:0449](https://access.redhat.com/errata/RHEA-2025:0449) | <div class="adv_e">Product Enhancement Advisory</div>
