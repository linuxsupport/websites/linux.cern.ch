## 2024-09-25


Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 3.10.0 | [RHSA-2024:6994](https://access.redhat.com/errata/RHSA-2024:6994) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2201](https://access.redhat.com/security/cve/CVE-2024-2201), [CVE-2024-41071](https://access.redhat.com/security/cve/CVE-2024-41071))
kernel | 3.10.0 | [RHSA-2024:6994](https://access.redhat.com/errata/RHSA-2024:6994) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2201](https://access.redhat.com/security/cve/CVE-2024-2201), [CVE-2024-41071](https://access.redhat.com/security/cve/CVE-2024-41071))
kernel-abi-whitelists | 3.10.0 | [RHSA-2024:6994](https://access.redhat.com/errata/RHSA-2024:6994) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2201](https://access.redhat.com/security/cve/CVE-2024-2201), [CVE-2024-41071](https://access.redhat.com/security/cve/CVE-2024-41071))
kernel-debug | 3.10.0 | [RHSA-2024:6994](https://access.redhat.com/errata/RHSA-2024:6994) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2201](https://access.redhat.com/security/cve/CVE-2024-2201), [CVE-2024-41071](https://access.redhat.com/security/cve/CVE-2024-41071))
kernel-debug-devel | 3.10.0 | [RHSA-2024:6994](https://access.redhat.com/errata/RHSA-2024:6994) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2201](https://access.redhat.com/security/cve/CVE-2024-2201), [CVE-2024-41071](https://access.redhat.com/security/cve/CVE-2024-41071))
kernel-devel | 3.10.0 | [RHSA-2024:6994](https://access.redhat.com/errata/RHSA-2024:6994) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2201](https://access.redhat.com/security/cve/CVE-2024-2201), [CVE-2024-41071](https://access.redhat.com/security/cve/CVE-2024-41071))
kernel-doc | 3.10.0 | [RHSA-2024:6994](https://access.redhat.com/errata/RHSA-2024:6994) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2201](https://access.redhat.com/security/cve/CVE-2024-2201), [CVE-2024-41071](https://access.redhat.com/security/cve/CVE-2024-41071))
kernel-headers | 3.10.0 | [RHSA-2024:6994](https://access.redhat.com/errata/RHSA-2024:6994) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2201](https://access.redhat.com/security/cve/CVE-2024-2201), [CVE-2024-41071](https://access.redhat.com/security/cve/CVE-2024-41071))
kernel-tools | 3.10.0 | [RHSA-2024:6994](https://access.redhat.com/errata/RHSA-2024:6994) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2201](https://access.redhat.com/security/cve/CVE-2024-2201), [CVE-2024-41071](https://access.redhat.com/security/cve/CVE-2024-41071))
kernel-tools-libs | 3.10.0 | [RHSA-2024:6994](https://access.redhat.com/errata/RHSA-2024:6994) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2201](https://access.redhat.com/security/cve/CVE-2024-2201), [CVE-2024-41071](https://access.redhat.com/security/cve/CVE-2024-41071))
kernel-tools-libs-devel | 3.10.0 | |
perf | 3.10.0 | [RHSA-2024:6994](https://access.redhat.com/errata/RHSA-2024:6994) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2201](https://access.redhat.com/security/cve/CVE-2024-2201), [CVE-2024-41071](https://access.redhat.com/security/cve/CVE-2024-41071))
python-perf | 3.10.0 | [RHSA-2024:6994](https://access.redhat.com/errata/RHSA-2024:6994) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2201](https://access.redhat.com/security/cve/CVE-2024-2201), [CVE-2024-41071](https://access.redhat.com/security/cve/CVE-2024-41071))
