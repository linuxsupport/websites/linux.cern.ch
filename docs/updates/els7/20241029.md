## 2024-10-29


Package | Version | Advisory | Notes
------- | ------- | -------- | -----
postgresql | 9.2.24 | [RHSA-2024:8495](https://access.redhat.com/errata/RHSA-2024:8495) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-contrib | 9.2.24 | [RHSA-2024:8495](https://access.redhat.com/errata/RHSA-2024:8495) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-devel | 9.2.24 | [RHSA-2024:8495](https://access.redhat.com/errata/RHSA-2024:8495) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-docs | 9.2.24 | [RHSA-2024:8495](https://access.redhat.com/errata/RHSA-2024:8495) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-libs | 9.2.24 | [RHSA-2024:8495](https://access.redhat.com/errata/RHSA-2024:8495) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-plperl | 9.2.24 | [RHSA-2024:8495](https://access.redhat.com/errata/RHSA-2024:8495) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-plpython | 9.2.24 | [RHSA-2024:8495](https://access.redhat.com/errata/RHSA-2024:8495) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-pltcl | 9.2.24 | [RHSA-2024:8495](https://access.redhat.com/errata/RHSA-2024:8495) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-server | 9.2.24 | [RHSA-2024:8495](https://access.redhat.com/errata/RHSA-2024:8495) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-static | 9.2.24 | |
postgresql-test | 9.2.24 | [RHSA-2024:8495](https://access.redhat.com/errata/RHSA-2024:8495) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-upgrade | 9.2.24 | |
