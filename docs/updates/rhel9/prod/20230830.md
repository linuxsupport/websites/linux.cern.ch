## 2023-08-30

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libdnf-plugin-subscription-manager | 1.29.33.1-2.el9_2 | [RHSA-2023:4708](https://access.redhat.com/errata/RHSA-2023:4708) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
libdnf-plugin-subscription-manager-debuginfo | 1.29.33.1-2.el9_2 | |
python3-cloud-what | 1.29.33.1-2.el9_2 | [RHSA-2023:4708](https://access.redhat.com/errata/RHSA-2023:4708) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
python3-subscription-manager-rhsm | 1.29.33.1-2.el9_2 | [RHSA-2023:4708](https://access.redhat.com/errata/RHSA-2023:4708) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
python3-subscription-manager-rhsm-debuginfo | 1.29.33.1-2.el9_2 | |
subscription-manager | 1.29.33.1-2.el9_2 | [RHSA-2023:4708](https://access.redhat.com/errata/RHSA-2023:4708) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
subscription-manager-debuginfo | 1.29.33.1-2.el9_2 | |
subscription-manager-debugsource | 1.29.33.1-2.el9_2 | |
subscription-manager-plugin-ostree | 1.29.33.1-2.el9_2 | [RHSA-2023:4708](https://access.redhat.com/errata/RHSA-2023:4708) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libdnf-plugin-subscription-manager | 1.29.33.1-2.el9_2 | [RHSA-2023:4708](https://access.redhat.com/errata/RHSA-2023:4708) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
libdnf-plugin-subscription-manager-debuginfo | 1.29.33.1-2.el9_2 | |
python3-cloud-what | 1.29.33.1-2.el9_2 | [RHSA-2023:4708](https://access.redhat.com/errata/RHSA-2023:4708) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
python3-subscription-manager-rhsm | 1.29.33.1-2.el9_2 | [RHSA-2023:4708](https://access.redhat.com/errata/RHSA-2023:4708) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
python3-subscription-manager-rhsm-debuginfo | 1.29.33.1-2.el9_2 | |
subscription-manager | 1.29.33.1-2.el9_2 | [RHSA-2023:4708](https://access.redhat.com/errata/RHSA-2023:4708) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
subscription-manager-debuginfo | 1.29.33.1-2.el9_2 | |
subscription-manager-debugsource | 1.29.33.1-2.el9_2 | |
subscription-manager-plugin-ostree | 1.29.33.1-2.el9_2 | [RHSA-2023:4708](https://access.redhat.com/errata/RHSA-2023:4708) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))

