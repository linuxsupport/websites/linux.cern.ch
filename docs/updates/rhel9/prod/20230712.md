## 2023-07-12

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.5-1.rh9.cern | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
go-toolset | 1.19.10-1.el9_2 | [RHSA-2023:3923](https://access.redhat.com/errata/RHSA-2023:3923) | <div class="adv_s">Security Advisory</div> ([CVE-2023-29402](https://access.redhat.com/security/cve/CVE-2023-29402), [CVE-2023-29403](https://access.redhat.com/security/cve/CVE-2023-29403), [CVE-2023-29404](https://access.redhat.com/security/cve/CVE-2023-29404), [CVE-2023-29405](https://access.redhat.com/security/cve/CVE-2023-29405))
golang | 1.19.10-1.el9_2 | [RHSA-2023:3923](https://access.redhat.com/errata/RHSA-2023:3923) | <div class="adv_s">Security Advisory</div> ([CVE-2023-29402](https://access.redhat.com/security/cve/CVE-2023-29402), [CVE-2023-29403](https://access.redhat.com/security/cve/CVE-2023-29403), [CVE-2023-29404](https://access.redhat.com/security/cve/CVE-2023-29404), [CVE-2023-29405](https://access.redhat.com/security/cve/CVE-2023-29405))
golang-bin | 1.19.10-1.el9_2 | [RHSA-2023:3923](https://access.redhat.com/errata/RHSA-2023:3923) | <div class="adv_s">Security Advisory</div> ([CVE-2023-29402](https://access.redhat.com/security/cve/CVE-2023-29402), [CVE-2023-29403](https://access.redhat.com/security/cve/CVE-2023-29403), [CVE-2023-29404](https://access.redhat.com/security/cve/CVE-2023-29404), [CVE-2023-29405](https://access.redhat.com/security/cve/CVE-2023-29405))
golang-docs | 1.19.10-1.el9_2 | [RHSA-2023:3923](https://access.redhat.com/errata/RHSA-2023:3923) | <div class="adv_s">Security Advisory</div> ([CVE-2023-29402](https://access.redhat.com/security/cve/CVE-2023-29402), [CVE-2023-29403](https://access.redhat.com/security/cve/CVE-2023-29403), [CVE-2023-29404](https://access.redhat.com/security/cve/CVE-2023-29404), [CVE-2023-29405](https://access.redhat.com/security/cve/CVE-2023-29405))
golang-misc | 1.19.10-1.el9_2 | [RHSA-2023:3923](https://access.redhat.com/errata/RHSA-2023:3923) | <div class="adv_s">Security Advisory</div> ([CVE-2023-29402](https://access.redhat.com/security/cve/CVE-2023-29402), [CVE-2023-29403](https://access.redhat.com/security/cve/CVE-2023-29403), [CVE-2023-29404](https://access.redhat.com/security/cve/CVE-2023-29404), [CVE-2023-29405](https://access.redhat.com/security/cve/CVE-2023-29405))
golang-race | 1.19.10-1.el9_2 | [RHSA-2023:3923](https://access.redhat.com/errata/RHSA-2023:3923) | <div class="adv_s">Security Advisory</div> ([CVE-2023-29402](https://access.redhat.com/security/cve/CVE-2023-29402), [CVE-2023-29403](https://access.redhat.com/security/cve/CVE-2023-29403), [CVE-2023-29404](https://access.redhat.com/security/cve/CVE-2023-29404), [CVE-2023-29405](https://access.redhat.com/security/cve/CVE-2023-29405))
golang-src | 1.19.10-1.el9_2 | [RHSA-2023:3923](https://access.redhat.com/errata/RHSA-2023:3923) | <div class="adv_s">Security Advisory</div> ([CVE-2023-29402](https://access.redhat.com/security/cve/CVE-2023-29402), [CVE-2023-29403](https://access.redhat.com/security/cve/CVE-2023-29403), [CVE-2023-29404](https://access.redhat.com/security/cve/CVE-2023-29404), [CVE-2023-29405](https://access.redhat.com/security/cve/CVE-2023-29405))
golang-tests | 1.19.10-1.el9_2 | [RHSA-2023:3923](https://access.redhat.com/errata/RHSA-2023:3923) | <div class="adv_s">Security Advisory</div> ([CVE-2023-29402](https://access.redhat.com/security/cve/CVE-2023-29402), [CVE-2023-29403](https://access.redhat.com/security/cve/CVE-2023-29403), [CVE-2023-29404](https://access.redhat.com/security/cve/CVE-2023-29404), [CVE-2023-29405](https://access.redhat.com/security/cve/CVE-2023-29405))
open-vm-tools | 12.1.5-1.el9_2.1 | |
open-vm-tools-debuginfo | 12.1.5-1.el9_2.1 | |
open-vm-tools-debugsource | 12.1.5-1.el9_2.1 | |
open-vm-tools-desktop | 12.1.5-1.el9_2.1 | |
open-vm-tools-desktop-debuginfo | 12.1.5-1.el9_2.1 | |
open-vm-tools-salt-minion | 12.1.5-1.el9_2.1 | |
open-vm-tools-sdmp | 12.1.5-1.el9_2.1 | |
open-vm-tools-sdmp-debuginfo | 12.1.5-1.el9_2.1 | |
open-vm-tools-test | 12.1.5-1.el9_2.1 | |
open-vm-tools-test-debuginfo | 12.1.5-1.el9_2.1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.5-1.rh9.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
go-toolset | 1.19.10-1.el9_2 | [RHSA-2023:3923](https://access.redhat.com/errata/RHSA-2023:3923) | <div class="adv_s">Security Advisory</div> ([CVE-2023-29402](https://access.redhat.com/security/cve/CVE-2023-29402), [CVE-2023-29403](https://access.redhat.com/security/cve/CVE-2023-29403), [CVE-2023-29404](https://access.redhat.com/security/cve/CVE-2023-29404), [CVE-2023-29405](https://access.redhat.com/security/cve/CVE-2023-29405))
golang | 1.19.10-1.el9_2 | [RHSA-2023:3923](https://access.redhat.com/errata/RHSA-2023:3923) | <div class="adv_s">Security Advisory</div> ([CVE-2023-29402](https://access.redhat.com/security/cve/CVE-2023-29402), [CVE-2023-29403](https://access.redhat.com/security/cve/CVE-2023-29403), [CVE-2023-29404](https://access.redhat.com/security/cve/CVE-2023-29404), [CVE-2023-29405](https://access.redhat.com/security/cve/CVE-2023-29405))
golang-bin | 1.19.10-1.el9_2 | [RHSA-2023:3923](https://access.redhat.com/errata/RHSA-2023:3923) | <div class="adv_s">Security Advisory</div> ([CVE-2023-29402](https://access.redhat.com/security/cve/CVE-2023-29402), [CVE-2023-29403](https://access.redhat.com/security/cve/CVE-2023-29403), [CVE-2023-29404](https://access.redhat.com/security/cve/CVE-2023-29404), [CVE-2023-29405](https://access.redhat.com/security/cve/CVE-2023-29405))
golang-docs | 1.19.10-1.el9_2 | [RHSA-2023:3923](https://access.redhat.com/errata/RHSA-2023:3923) | <div class="adv_s">Security Advisory</div> ([CVE-2023-29402](https://access.redhat.com/security/cve/CVE-2023-29402), [CVE-2023-29403](https://access.redhat.com/security/cve/CVE-2023-29403), [CVE-2023-29404](https://access.redhat.com/security/cve/CVE-2023-29404), [CVE-2023-29405](https://access.redhat.com/security/cve/CVE-2023-29405))
golang-misc | 1.19.10-1.el9_2 | [RHSA-2023:3923](https://access.redhat.com/errata/RHSA-2023:3923) | <div class="adv_s">Security Advisory</div> ([CVE-2023-29402](https://access.redhat.com/security/cve/CVE-2023-29402), [CVE-2023-29403](https://access.redhat.com/security/cve/CVE-2023-29403), [CVE-2023-29404](https://access.redhat.com/security/cve/CVE-2023-29404), [CVE-2023-29405](https://access.redhat.com/security/cve/CVE-2023-29405))
golang-src | 1.19.10-1.el9_2 | [RHSA-2023:3923](https://access.redhat.com/errata/RHSA-2023:3923) | <div class="adv_s">Security Advisory</div> ([CVE-2023-29402](https://access.redhat.com/security/cve/CVE-2023-29402), [CVE-2023-29403](https://access.redhat.com/security/cve/CVE-2023-29403), [CVE-2023-29404](https://access.redhat.com/security/cve/CVE-2023-29404), [CVE-2023-29405](https://access.redhat.com/security/cve/CVE-2023-29405))
golang-tests | 1.19.10-1.el9_2 | [RHSA-2023:3923](https://access.redhat.com/errata/RHSA-2023:3923) | <div class="adv_s">Security Advisory</div> ([CVE-2023-29402](https://access.redhat.com/security/cve/CVE-2023-29402), [CVE-2023-29403](https://access.redhat.com/security/cve/CVE-2023-29403), [CVE-2023-29404](https://access.redhat.com/security/cve/CVE-2023-29404), [CVE-2023-29405](https://access.redhat.com/security/cve/CVE-2023-29405))
open-vm-tools | 12.1.5-1.el9_2.1 | |
open-vm-tools-debuginfo | 12.1.5-1.el9_2.1 | |
open-vm-tools-debugsource | 12.1.5-1.el9_2.1 | |
open-vm-tools-desktop | 12.1.5-1.el9_2.1 | |
open-vm-tools-desktop-debuginfo | 12.1.5-1.el9_2.1 | |
open-vm-tools-sdmp-debuginfo | 12.1.5-1.el9_2.1 | |
open-vm-tools-test | 12.1.5-1.el9_2.1 | |
open-vm-tools-test-debuginfo | 12.1.5-1.el9_2.1 | |

