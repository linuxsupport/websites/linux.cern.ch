## 2024-05-22

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.30-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-7.0 | 7.0.19-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-runtime-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-runtime-dbg-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-targeting-pack-6.0 | 6.0.30-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-7.0 | 7.0.19-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-targeting-pack-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-apphost-pack-6.0 | 6.0.30-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-apphost-pack-7.0 | 7.0.19-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-apphost-pack-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-apphost-pack-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-apphost-pack-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-host | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-host-debuginfo | 8.0.5-1.el9_4 | |
dotnet-hostfxr-6.0 | 6.0.30-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-hostfxr-7.0 | 7.0.19-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-hostfxr-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-hostfxr-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-hostfxr-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-runtime-6.0 | 6.0.30-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-runtime-7.0 | 7.0.19-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-runtime-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-runtime-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-runtime-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-runtime-dbg-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-6.0 | 6.0.130-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.130-1.el9_4 | |
dotnet-sdk-7.0 | 7.0.119-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-7.0-debuginfo | 7.0.119-1.el9_4 | |
dotnet-sdk-8.0 | 8.0.105-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-8.0-debuginfo | 8.0.105-1.el9_4 | |
dotnet-sdk-dbg-8.0 | 8.0.105-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-targeting-pack-6.0 | 6.0.30-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-7.0 | 7.0.19-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-targeting-pack-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-templates-6.0 | 6.0.130-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-7.0 | 7.0.119-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-templates-8.0 | 8.0.105-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet6.0-debuginfo | 6.0.130-1.el9_4 | |
dotnet6.0-debugsource | 6.0.130-1.el9_4 | |
dotnet7.0-debuginfo | 7.0.119-1.el9_4 | |
dotnet7.0-debugsource | 7.0.119-1.el9_4 | |
dotnet8.0-debuginfo | 8.0.105-1.el9_4 | |
dotnet8.0-debugsource | 8.0.105-1.el9_4 | |
netstandard-targeting-pack-2.1 | 8.0.105-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
nodejs | 18.20.2-2.module+el9.4.0+21742+692df1ea | [RHSA-2024:2779](https://access.redhat.com/errata/RHSA-2024:2779) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-debuginfo | 18.20.2-2.module+el9.4.0+21742+692df1ea | |
nodejs-debugsource | 18.20.2-2.module+el9.4.0+21742+692df1ea | |
nodejs-devel | 18.20.2-2.module+el9.4.0+21742+692df1ea | [RHSA-2024:2779](https://access.redhat.com/errata/RHSA-2024:2779) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-docs | 18.20.2-2.module+el9.4.0+21742+692df1ea | [RHSA-2024:2779](https://access.redhat.com/errata/RHSA-2024:2779) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-full-i18n | 18.20.2-2.module+el9.4.0+21742+692df1ea | [RHSA-2024:2779](https://access.redhat.com/errata/RHSA-2024:2779) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
npm | 10.5.0-1.18.20.2.2.module+el9.4.0+21742+692df1ea | [RHSA-2024:2779](https://access.redhat.com/errata/RHSA-2024:2779) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-host-debuginfo | 8.0.5-1.el9_4 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-runtime-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-runtime-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-runtime-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-sdk-6.0-debuginfo | 6.0.130-1.el9_4 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.130-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.119-1.el9_4 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.119-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-8.0-debuginfo | 8.0.105-1.el9_4 | |
dotnet-sdk-8.0-source-built-artifacts | 8.0.105-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet6.0-debuginfo | 6.0.130-1.el9_4 | |
dotnet6.0-debugsource | 6.0.130-1.el9_4 | |
dotnet7.0-debuginfo | 7.0.119-1.el9_4 | |
dotnet7.0-debugsource | 7.0.119-1.el9_4 | |
dotnet8.0-debuginfo | 8.0.105-1.el9_4 | |
dotnet8.0-debugsource | 8.0.105-1.el9_4 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.30-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-7.0 | 7.0.19-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-runtime-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-runtime-dbg-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-targeting-pack-6.0 | 6.0.30-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-7.0 | 7.0.19-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
aspnetcore-targeting-pack-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-apphost-pack-6.0 | 6.0.30-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-apphost-pack-7.0 | 7.0.19-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-apphost-pack-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-apphost-pack-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-apphost-pack-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-host | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-host-debuginfo | 8.0.5-1.el9_4 | |
dotnet-hostfxr-6.0 | 6.0.30-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-hostfxr-7.0 | 7.0.19-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-hostfxr-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-hostfxr-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-hostfxr-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-runtime-6.0 | 6.0.30-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-runtime-7.0 | 7.0.19-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-runtime-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-runtime-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-runtime-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-runtime-dbg-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-6.0 | 6.0.130-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.130-1.el9_4 | |
dotnet-sdk-7.0 | 7.0.119-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-7.0-debuginfo | 7.0.119-1.el9_4 | |
dotnet-sdk-8.0 | 8.0.105-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-8.0-debuginfo | 8.0.105-1.el9_4 | |
dotnet-sdk-dbg-8.0 | 8.0.105-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-targeting-pack-6.0 | 6.0.30-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-7.0 | 7.0.19-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-targeting-pack-8.0 | 8.0.5-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-templates-6.0 | 6.0.130-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-7.0 | 7.0.119-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-templates-8.0 | 8.0.105-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet6.0-debuginfo | 6.0.130-1.el9_4 | |
dotnet6.0-debugsource | 6.0.130-1.el9_4 | |
dotnet7.0-debuginfo | 7.0.119-1.el9_4 | |
dotnet7.0-debugsource | 7.0.119-1.el9_4 | |
dotnet8.0-debuginfo | 8.0.105-1.el9_4 | |
dotnet8.0-debugsource | 8.0.105-1.el9_4 | |
netstandard-targeting-pack-2.1 | 8.0.105-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
nodejs | 18.20.2-2.module+el9.4.0+21742+692df1ea | [RHSA-2024:2779](https://access.redhat.com/errata/RHSA-2024:2779) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-debuginfo | 18.20.2-2.module+el9.4.0+21742+692df1ea | |
nodejs-debugsource | 18.20.2-2.module+el9.4.0+21742+692df1ea | |
nodejs-devel | 18.20.2-2.module+el9.4.0+21742+692df1ea | [RHSA-2024:2779](https://access.redhat.com/errata/RHSA-2024:2779) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-docs | 18.20.2-2.module+el9.4.0+21742+692df1ea | [RHSA-2024:2779](https://access.redhat.com/errata/RHSA-2024:2779) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nodejs-full-i18n | 18.20.2-2.module+el9.4.0+21742+692df1ea | [RHSA-2024:2779](https://access.redhat.com/errata/RHSA-2024:2779) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
npm | 10.5.0-1.18.20.2.2.module+el9.4.0+21742+692df1ea | [RHSA-2024:2779](https://access.redhat.com/errata/RHSA-2024:2779) | <div class="adv_s">Security Advisory</div> ([CVE-2024-22025](https://access.redhat.com/security/cve/CVE-2024-22025), [CVE-2024-25629](https://access.redhat.com/security/cve/CVE-2024-25629), [CVE-2024-27982](https://access.redhat.com/security/cve/CVE-2024-27982), [CVE-2024-27983](https://access.redhat.com/security/cve/CVE-2024-27983), [CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-host-debuginfo | 8.0.5-1.el9_4 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-runtime-6.0-debuginfo | 6.0.30-1.el9_4 | |
dotnet-runtime-7.0-debuginfo | 7.0.19-1.el9_4 | |
dotnet-runtime-8.0-debuginfo | 8.0.5-1.el9_4 | |
dotnet-sdk-6.0-debuginfo | 6.0.130-1.el9_4 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.130-1.el9_4 | [RHBA-2024:2841](https://access.redhat.com/errata/RHBA-2024:2841) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.119-1.el9_4 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.119-1.el9_4 | [RHSA-2024:2843](https://access.redhat.com/errata/RHSA-2024:2843) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet-sdk-8.0-debuginfo | 8.0.105-1.el9_4 | |
dotnet-sdk-8.0-source-built-artifacts | 8.0.105-1.el9_4 | [RHSA-2024:2842](https://access.redhat.com/errata/RHSA-2024:2842) | <div class="adv_s">Security Advisory</div> ([CVE-2024-30045](https://access.redhat.com/security/cve/CVE-2024-30045), [CVE-2024-30046](https://access.redhat.com/security/cve/CVE-2024-30046))
dotnet6.0-debuginfo | 6.0.130-1.el9_4 | |
dotnet6.0-debugsource | 6.0.130-1.el9_4 | |
dotnet7.0-debuginfo | 7.0.119-1.el9_4 | |
dotnet7.0-debugsource | 7.0.119-1.el9_4 | |
dotnet8.0-debuginfo | 8.0.105-1.el9_4 | |
dotnet8.0-debugsource | 8.0.105-1.el9_4 | |

