## 2024-02-28

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lpadmincern | 1.4.5-1.rh9.cern | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-runtime-dbg-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-targeting-pack-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-8.0-debuginfo | 8.0.2-2.el9_3 | |
dotnet-host | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-host-debuginfo | 8.0.2-2.el9_3 | |
dotnet-hostfxr-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-hostfxr-8.0-debuginfo | 8.0.2-2.el9_3 | |
dotnet-runtime-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-8.0-debuginfo | 8.0.2-2.el9_3 | |
dotnet-runtime-dbg-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-8.0 | 8.0.102-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-8.0-debuginfo | 8.0.102-2.el9_3 | |
dotnet-sdk-dbg-8.0 | 8.0.102-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-targeting-pack-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-templates-8.0 | 8.0.102-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet8.0-debuginfo | 8.0.102-2.el9_3 | |
dotnet8.0-debugsource | 8.0.102-2.el9_3 | |
netstandard-targeting-pack-2.1 | 8.0.102-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-8.0-debuginfo | 8.0.2-2.el9_3 | |
dotnet-host-debuginfo | 8.0.2-2.el9_3 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.2-2.el9_3 | |
dotnet-runtime-8.0-debuginfo | 8.0.2-2.el9_3 | |
dotnet-sdk-8.0-debuginfo | 8.0.102-2.el9_3 | |
dotnet-sdk-8.0-source-built-artifacts | 8.0.102-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet8.0-debuginfo | 8.0.102-2.el9_3 | |
dotnet8.0-debugsource | 8.0.102-2.el9_3 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lpadmincern | 1.4.5-1.rh9.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-runtime-dbg-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-targeting-pack-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-8.0-debuginfo | 8.0.2-2.el9_3 | |
dotnet-host | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-host-debuginfo | 8.0.2-2.el9_3 | |
dotnet-hostfxr-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-hostfxr-8.0-debuginfo | 8.0.2-2.el9_3 | |
dotnet-runtime-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-8.0-debuginfo | 8.0.2-2.el9_3 | |
dotnet-runtime-dbg-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-8.0 | 8.0.102-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-8.0-debuginfo | 8.0.102-2.el9_3 | |
dotnet-sdk-dbg-8.0 | 8.0.102-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-targeting-pack-8.0 | 8.0.2-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-templates-8.0 | 8.0.102-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet8.0-debuginfo | 8.0.102-2.el9_3 | |
dotnet8.0-debugsource | 8.0.102-2.el9_3 | |
netstandard-targeting-pack-2.1 | 8.0.102-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-8.0-debuginfo | 8.0.2-2.el9_3 | |
dotnet-host-debuginfo | 8.0.2-2.el9_3 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.2-2.el9_3 | |
dotnet-runtime-8.0-debuginfo | 8.0.2-2.el9_3 | |
dotnet-sdk-8.0-debuginfo | 8.0.102-2.el9_3 | |
dotnet-sdk-8.0-source-built-artifacts | 8.0.102-2.el9_3 | [RHSA-2024:0848](https://access.redhat.com/errata/RHSA-2024:0848) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet8.0-debuginfo | 8.0.102-2.el9_3 | |
dotnet8.0-debugsource | 8.0.102-2.el9_3 | |

