## 2024-10-16

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 1.0.2-1.rh9.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20230808-2.20240531.1.el9_4 | [RHEA-2024:7620](https://access.redhat.com/errata/RHEA-2024:7620) | <div class="adv_e">Product Enhancement Advisory</div>

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 128.3.0-1.el9_4 | [RHSA-2024:7505](https://access.redhat.com/errata/RHSA-2024:7505) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9392](https://access.redhat.com/security/cve/CVE-2024-9392), [CVE-2024-9393](https://access.redhat.com/security/cve/CVE-2024-9393), [CVE-2024-9394](https://access.redhat.com/security/cve/CVE-2024-9394), [CVE-2024-9401](https://access.redhat.com/security/cve/CVE-2024-9401), [CVE-2024-9402](https://access.redhat.com/security/cve/CVE-2024-9402))
firefox-debuginfo | 128.3.0-1.el9_4 | |
firefox-debugsource | 128.3.0-1.el9_4 | |
firefox-x11 | 128.3.0-1.el9_4 | [RHSA-2024:7505](https://access.redhat.com/errata/RHSA-2024:7505) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9392](https://access.redhat.com/security/cve/CVE-2024-9392), [CVE-2024-9393](https://access.redhat.com/security/cve/CVE-2024-9393), [CVE-2024-9394](https://access.redhat.com/security/cve/CVE-2024-9394), [CVE-2024-9401](https://access.redhat.com/security/cve/CVE-2024-9401), [CVE-2024-9402](https://access.redhat.com/security/cve/CVE-2024-9402))
go-toolset | 1.21.13-4.el9_4 | [RHSA-2024:7550](https://access.redhat.com/errata/RHSA-2024:7550) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9355](https://access.redhat.com/security/cve/CVE-2024-9355))
golang | 1.21.13-4.el9_4 | [RHSA-2024:7550](https://access.redhat.com/errata/RHSA-2024:7550) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9355](https://access.redhat.com/security/cve/CVE-2024-9355))
golang-bin | 1.21.13-4.el9_4 | [RHSA-2024:7550](https://access.redhat.com/errata/RHSA-2024:7550) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9355](https://access.redhat.com/security/cve/CVE-2024-9355))
golang-docs | 1.21.13-4.el9_4 | [RHSA-2024:7550](https://access.redhat.com/errata/RHSA-2024:7550) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9355](https://access.redhat.com/security/cve/CVE-2024-9355))
golang-misc | 1.21.13-4.el9_4 | [RHSA-2024:7550](https://access.redhat.com/errata/RHSA-2024:7550) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9355](https://access.redhat.com/security/cve/CVE-2024-9355))
golang-src | 1.21.13-4.el9_4 | [RHSA-2024:7550](https://access.redhat.com/errata/RHSA-2024:7550) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9355](https://access.redhat.com/security/cve/CVE-2024-9355))
golang-tests | 1.21.13-4.el9_4 | [RHSA-2024:7550](https://access.redhat.com/errata/RHSA-2024:7550) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9355](https://access.redhat.com/security/cve/CVE-2024-9355))
thunderbird | 128.3.0-1.el9_4 | [RHSA-2024:7552](https://access.redhat.com/errata/RHSA-2024:7552) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9392](https://access.redhat.com/security/cve/CVE-2024-9392), [CVE-2024-9393](https://access.redhat.com/security/cve/CVE-2024-9393), [CVE-2024-9394](https://access.redhat.com/security/cve/CVE-2024-9394), [CVE-2024-9396](https://access.redhat.com/security/cve/CVE-2024-9396), [CVE-2024-9397](https://access.redhat.com/security/cve/CVE-2024-9397), [CVE-2024-9398](https://access.redhat.com/security/cve/CVE-2024-9398), [CVE-2024-9399](https://access.redhat.com/security/cve/CVE-2024-9399), [CVE-2024-9400](https://access.redhat.com/security/cve/CVE-2024-9400), [CVE-2024-9401](https://access.redhat.com/security/cve/CVE-2024-9401), [CVE-2024-9402](https://access.redhat.com/security/cve/CVE-2024-9402), [CVE-2024-9403](https://access.redhat.com/security/cve/CVE-2024-9403))
thunderbird-debuginfo | 128.3.0-1.el9_4 | |
thunderbird-debugsource | 128.3.0-1.el9_4 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 1.0.2-1.rh9.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 128.3.0-1.el9_4 | [RHSA-2024:7505](https://access.redhat.com/errata/RHSA-2024:7505) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9392](https://access.redhat.com/security/cve/CVE-2024-9392), [CVE-2024-9393](https://access.redhat.com/security/cve/CVE-2024-9393), [CVE-2024-9394](https://access.redhat.com/security/cve/CVE-2024-9394), [CVE-2024-9401](https://access.redhat.com/security/cve/CVE-2024-9401), [CVE-2024-9402](https://access.redhat.com/security/cve/CVE-2024-9402))
firefox-debuginfo | 128.3.0-1.el9_4 | |
firefox-debugsource | 128.3.0-1.el9_4 | |
firefox-x11 | 128.3.0-1.el9_4 | [RHSA-2024:7505](https://access.redhat.com/errata/RHSA-2024:7505) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9392](https://access.redhat.com/security/cve/CVE-2024-9392), [CVE-2024-9393](https://access.redhat.com/security/cve/CVE-2024-9393), [CVE-2024-9394](https://access.redhat.com/security/cve/CVE-2024-9394), [CVE-2024-9401](https://access.redhat.com/security/cve/CVE-2024-9401), [CVE-2024-9402](https://access.redhat.com/security/cve/CVE-2024-9402))
go-toolset | 1.21.13-4.el9_4 | [RHSA-2024:7550](https://access.redhat.com/errata/RHSA-2024:7550) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9355](https://access.redhat.com/security/cve/CVE-2024-9355))
golang | 1.21.13-4.el9_4 | [RHSA-2024:7550](https://access.redhat.com/errata/RHSA-2024:7550) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9355](https://access.redhat.com/security/cve/CVE-2024-9355))
golang-bin | 1.21.13-4.el9_4 | [RHSA-2024:7550](https://access.redhat.com/errata/RHSA-2024:7550) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9355](https://access.redhat.com/security/cve/CVE-2024-9355))
golang-docs | 1.21.13-4.el9_4 | [RHSA-2024:7550](https://access.redhat.com/errata/RHSA-2024:7550) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9355](https://access.redhat.com/security/cve/CVE-2024-9355))
golang-misc | 1.21.13-4.el9_4 | [RHSA-2024:7550](https://access.redhat.com/errata/RHSA-2024:7550) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9355](https://access.redhat.com/security/cve/CVE-2024-9355))
golang-src | 1.21.13-4.el9_4 | [RHSA-2024:7550](https://access.redhat.com/errata/RHSA-2024:7550) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9355](https://access.redhat.com/security/cve/CVE-2024-9355))
golang-tests | 1.21.13-4.el9_4 | [RHSA-2024:7550](https://access.redhat.com/errata/RHSA-2024:7550) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9355](https://access.redhat.com/security/cve/CVE-2024-9355))
thunderbird | 128.3.0-1.el9_4 | [RHSA-2024:7552](https://access.redhat.com/errata/RHSA-2024:7552) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9392](https://access.redhat.com/security/cve/CVE-2024-9392), [CVE-2024-9393](https://access.redhat.com/security/cve/CVE-2024-9393), [CVE-2024-9394](https://access.redhat.com/security/cve/CVE-2024-9394), [CVE-2024-9396](https://access.redhat.com/security/cve/CVE-2024-9396), [CVE-2024-9397](https://access.redhat.com/security/cve/CVE-2024-9397), [CVE-2024-9398](https://access.redhat.com/security/cve/CVE-2024-9398), [CVE-2024-9399](https://access.redhat.com/security/cve/CVE-2024-9399), [CVE-2024-9400](https://access.redhat.com/security/cve/CVE-2024-9400), [CVE-2024-9401](https://access.redhat.com/security/cve/CVE-2024-9401), [CVE-2024-9402](https://access.redhat.com/security/cve/CVE-2024-9402), [CVE-2024-9403](https://access.redhat.com/security/cve/CVE-2024-9403))
thunderbird-debuginfo | 128.3.0-1.el9_4 | |
thunderbird-debugsource | 128.3.0-1.el9_4 | |

