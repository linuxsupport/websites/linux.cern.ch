## 2024-07-03

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3 | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-libs | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3.9-debuginfo | 3.9.18-3.el9_4.1 | |
python3.9-debugsource | 3.9.18-3.el9_4.1 | |
sos | 4.7.1-3.el9 | [RHBA-2024:4049](https://access.redhat.com/errata/RHBA-2024:4049) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.7.1-3.el9 | [RHBA-2024:4049](https://access.redhat.com/errata/RHBA-2024:4049) | <div class="adv_b">Bug Fix Advisory</div>

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ghostscript | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-debuginfo | 9.54.0-16.el9_4 | |
ghostscript-debugsource | 9.54.0-16.el9_4 | |
ghostscript-doc | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-gtk-debuginfo | 9.54.0-16.el9_4 | |
ghostscript-tools-dvipdf | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-tools-fonts | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-tools-printing | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-x11 | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-x11-debuginfo | 9.54.0-16.el9_4 | |
git | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-all | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-core | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-core-debuginfo | 2.43.5-1.el9_4 | |
git-core-doc | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-credential-libsecret | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-credential-libsecret-debuginfo | 2.43.5-1.el9_4 | |
git-daemon | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-daemon-debuginfo | 2.43.5-1.el9_4 | |
git-debuginfo | 2.43.5-1.el9_4 | |
git-debugsource | 2.43.5-1.el9_4 | |
git-email | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-gui | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-instaweb | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-subtree | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-svn | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
gitk | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
gitweb | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
libgs | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
libgs-debuginfo | 9.54.0-16.el9_4 | |
libreswan | 4.12-2.el9_4.1 | [RHSA-2024:4050](https://access.redhat.com/errata/RHSA-2024:4050) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3652](https://access.redhat.com/security/cve/CVE-2024-3652))
libreswan-debuginfo | 4.12-2.el9_4.1 | |
libreswan-debugsource | 4.12-2.el9_4.1 | |
perl-Git | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
perl-Git-SVN | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
python-unversioned-command | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-devel | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-tkinter | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3.11 | 3.11.7-1.el9_4.1 | [RHSA-2024:4077](https://access.redhat.com/errata/RHSA-2024:4077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-debuginfo | 3.11.7-1.el9_4.1 | |
python3.11-debugsource | 3.11.7-1.el9_4.1 | |
python3.11-devel | 3.11.7-1.el9_4.1 | [RHSA-2024:4077](https://access.redhat.com/errata/RHSA-2024:4077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-libs | 3.11.7-1.el9_4.1 | [RHSA-2024:4077](https://access.redhat.com/errata/RHSA-2024:4077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-tkinter | 3.11.7-1.el9_4.1 | [RHSA-2024:4077](https://access.redhat.com/errata/RHSA-2024:4077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.9-debuginfo | 3.9.18-3.el9_4.1 | |
python3.9-debugsource | 3.9.18-3.el9_4.1 | |
qemu-guest-agent | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-guest-agent-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-img | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-img-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-audio-dbus-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-audio-pa | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-audio-pa-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-blkio | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-block-blkio-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-curl | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-block-curl-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-rbd | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-block-rbd-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-common | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-common-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-core | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-core-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-debugsource | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-display-virtio-gpu | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-device-display-virtio-gpu-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-display-virtio-gpu-pci | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-device-display-virtio-gpu-pci-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-display-virtio-vga | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-device-display-virtio-vga-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-usb-host | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-device-usb-host-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-usb-redirect | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-device-usb-redirect-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-docs | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-tests-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-tools | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-tools-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-ui-dbus-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-ui-egl-headless | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-ui-egl-headless-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-ui-opengl | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-ui-opengl-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-pr-helper | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-pr-helper-debuginfo | 8.2.0-11.el9_4.3 | |
thunderbird | 115.12.1-1.el9_4 | [RHSA-2024:4002](https://access.redhat.com/errata/RHSA-2024:4002) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5688](https://access.redhat.com/security/cve/CVE-2024-5688), [CVE-2024-5690](https://access.redhat.com/security/cve/CVE-2024-5690), [CVE-2024-5691](https://access.redhat.com/security/cve/CVE-2024-5691), [CVE-2024-5693](https://access.redhat.com/security/cve/CVE-2024-5693), [CVE-2024-5696](https://access.redhat.com/security/cve/CVE-2024-5696), [CVE-2024-5700](https://access.redhat.com/security/cve/CVE-2024-5700), [CVE-2024-5702](https://access.redhat.com/security/cve/CVE-2024-5702))
thunderbird-debuginfo | 115.12.1-1.el9_4 | |
thunderbird-debugsource | 115.12.1-1.el9_4 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ghostscript-debuginfo | 9.54.0-16.el9_4 | |
ghostscript-debugsource | 9.54.0-16.el9_4 | |
ghostscript-gtk-debuginfo | 9.54.0-16.el9_4 | |
ghostscript-x11-debuginfo | 9.54.0-16.el9_4 | |
libgs-debuginfo | 9.54.0-16.el9_4 | |
libgs-devel | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
python3-debug | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-idle | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-test | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3.11-debug | 3.11.7-1.el9_4.1 | [RHSA-2024:4077](https://access.redhat.com/errata/RHSA-2024:4077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-debuginfo | 3.11.7-1.el9_4.1 | |
python3.11-debugsource | 3.11.7-1.el9_4.1 | |
python3.11-idle | 3.11.7-1.el9_4.1 | [RHSA-2024:4077](https://access.redhat.com/errata/RHSA-2024:4077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-test | 3.11.7-1.el9_4.1 | [RHSA-2024:4077](https://access.redhat.com/errata/RHSA-2024:4077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.9-debuginfo | 3.9.18-3.el9_4.1 | |
python3.9-debugsource | 3.9.18-3.el9_4.1 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3 | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-libs | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3.9-debuginfo | 3.9.18-3.el9_4.1 | |
python3.9-debugsource | 3.9.18-3.el9_4.1 | |
sos | 4.7.1-3.el9 | [RHBA-2024:4049](https://access.redhat.com/errata/RHBA-2024:4049) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.7.1-3.el9 | [RHBA-2024:4049](https://access.redhat.com/errata/RHBA-2024:4049) | <div class="adv_b">Bug Fix Advisory</div>

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ghostscript | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-debuginfo | 9.54.0-16.el9_4 | |
ghostscript-debugsource | 9.54.0-16.el9_4 | |
ghostscript-doc | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-gtk-debuginfo | 9.54.0-16.el9_4 | |
ghostscript-tools-dvipdf | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-tools-fonts | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-tools-printing | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-x11 | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
ghostscript-x11-debuginfo | 9.54.0-16.el9_4 | |
git | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-all | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-core | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-core-debuginfo | 2.43.5-1.el9_4 | |
git-core-doc | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-credential-libsecret | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-credential-libsecret-debuginfo | 2.43.5-1.el9_4 | |
git-daemon | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-daemon-debuginfo | 2.43.5-1.el9_4 | |
git-debuginfo | 2.43.5-1.el9_4 | |
git-debugsource | 2.43.5-1.el9_4 | |
git-email | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-gui | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-instaweb | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-subtree | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-svn | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
gitk | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
gitweb | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
libgs | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
libgs-debuginfo | 9.54.0-16.el9_4 | |
libreswan | 4.12-2.el9_4.1 | [RHSA-2024:4050](https://access.redhat.com/errata/RHSA-2024:4050) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3652](https://access.redhat.com/security/cve/CVE-2024-3652))
libreswan-debuginfo | 4.12-2.el9_4.1 | |
libreswan-debugsource | 4.12-2.el9_4.1 | |
perl-Git | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
perl-Git-SVN | 2.43.5-1.el9_4 | [RHSA-2024:4083](https://access.redhat.com/errata/RHSA-2024:4083) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
python-unversioned-command | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-devel | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-tkinter | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3.11 | 3.11.7-1.el9_4.1 | [RHSA-2024:4077](https://access.redhat.com/errata/RHSA-2024:4077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-debuginfo | 3.11.7-1.el9_4.1 | |
python3.11-debugsource | 3.11.7-1.el9_4.1 | |
python3.11-devel | 3.11.7-1.el9_4.1 | [RHSA-2024:4077](https://access.redhat.com/errata/RHSA-2024:4077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-libs | 3.11.7-1.el9_4.1 | [RHSA-2024:4077](https://access.redhat.com/errata/RHSA-2024:4077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-tkinter | 3.11.7-1.el9_4.1 | [RHSA-2024:4077](https://access.redhat.com/errata/RHSA-2024:4077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.9-debuginfo | 3.9.18-3.el9_4.1 | |
python3.9-debugsource | 3.9.18-3.el9_4.1 | |
qemu-guest-agent | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-guest-agent-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-img | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-img-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-audio-dbus-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-audio-pa | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-audio-pa-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-blkio | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-block-blkio-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-curl | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-block-curl-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-block-rbd | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-block-rbd-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-common | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-common-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-core | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-core-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-debugsource | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-display-virtio-gpu | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-device-display-virtio-gpu-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-display-virtio-gpu-pci | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-device-display-virtio-gpu-pci-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-usb-host | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-device-usb-host-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-device-usb-redirect | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-device-usb-redirect-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-docs | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-tests-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-tools | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-kvm-tools-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-kvm-ui-dbus-debuginfo | 8.2.0-11.el9_4.3 | |
qemu-pr-helper | 8.2.0-11.el9_4.3 | [RHBA-2024:3995](https://access.redhat.com/errata/RHBA-2024:3995) | <div class="adv_b">Bug Fix Advisory</div>
qemu-pr-helper-debuginfo | 8.2.0-11.el9_4.3 | |
thunderbird | 115.12.1-1.el9_4 | [RHSA-2024:4002](https://access.redhat.com/errata/RHSA-2024:4002) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5688](https://access.redhat.com/security/cve/CVE-2024-5688), [CVE-2024-5690](https://access.redhat.com/security/cve/CVE-2024-5690), [CVE-2024-5691](https://access.redhat.com/security/cve/CVE-2024-5691), [CVE-2024-5693](https://access.redhat.com/security/cve/CVE-2024-5693), [CVE-2024-5696](https://access.redhat.com/security/cve/CVE-2024-5696), [CVE-2024-5700](https://access.redhat.com/security/cve/CVE-2024-5700), [CVE-2024-5702](https://access.redhat.com/security/cve/CVE-2024-5702))
thunderbird-debuginfo | 115.12.1-1.el9_4 | |
thunderbird-debugsource | 115.12.1-1.el9_4 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ghostscript-debuginfo | 9.54.0-16.el9_4 | |
ghostscript-debugsource | 9.54.0-16.el9_4 | |
ghostscript-gtk-debuginfo | 9.54.0-16.el9_4 | |
ghostscript-x11-debuginfo | 9.54.0-16.el9_4 | |
libgs-debuginfo | 9.54.0-16.el9_4 | |
libgs-devel | 9.54.0-16.el9_4 | [RHSA-2024:3999](https://access.redhat.com/errata/RHSA-2024:3999) | <div class="adv_s">Security Advisory</div> ([CVE-2024-33871](https://access.redhat.com/security/cve/CVE-2024-33871))
python3-debug | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-idle | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3-test | 3.9.18-3.el9_4.1 | [RHSA-2024:4078](https://access.redhat.com/errata/RHSA-2024:4078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597), [CVE-2024-0450](https://access.redhat.com/security/cve/CVE-2024-0450))
python3.11-debug | 3.11.7-1.el9_4.1 | [RHSA-2024:4077](https://access.redhat.com/errata/RHSA-2024:4077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-debuginfo | 3.11.7-1.el9_4.1 | |
python3.11-debugsource | 3.11.7-1.el9_4.1 | |
python3.11-idle | 3.11.7-1.el9_4.1 | [RHSA-2024:4077](https://access.redhat.com/errata/RHSA-2024:4077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.11-test | 3.11.7-1.el9_4.1 | [RHSA-2024:4077](https://access.redhat.com/errata/RHSA-2024:4077) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6597](https://access.redhat.com/security/cve/CVE-2023-6597))
python3.9-debuginfo | 3.9.18-3.el9_4.1 | |
python3.9-debugsource | 3.9.18-3.el9_4.1 | |

