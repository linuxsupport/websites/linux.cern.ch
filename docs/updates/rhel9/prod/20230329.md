## 2023-03-29

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.6-0.rh9.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.5.0-1.el9 | |
sos-audit | 4.5.0-1.el9 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.15-1.el9_1 | |
aspnetcore-runtime-7.0 | 7.0.4-1.el9_1 | |
aspnetcore-targeting-pack-6.0 | 6.0.15-1.el9_1 | |
aspnetcore-targeting-pack-7.0 | 7.0.4-1.el9_1 | |
dotnet-apphost-pack-6.0 | 6.0.15-1.el9_1 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-apphost-pack-7.0 | 7.0.4-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-host | 7.0.4-1.el9_1 | |
dotnet-host-debuginfo | 7.0.4-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.15-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-hostfxr-7.0 | 7.0.4-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.15-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-runtime-7.0 | 7.0.4-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.115-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet-sdk-7.0 | 7.0.104-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.15-1.el9_1 | |
dotnet-targeting-pack-7.0 | 7.0.4-1.el9_1 | |
dotnet-templates-6.0 | 6.0.115-1.el9_1 | |
dotnet-templates-7.0 | 7.0.104-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet6.0-debugsource | 6.0.115-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet7.0-debugsource | 7.0.104-1.el9_1 | |
firefox | 102.9.0-3.el9_1 | |
firefox-debuginfo | 102.9.0-3.el9_1 | |
firefox-debugsource | 102.9.0-3.el9_1 | |
firefox-x11 | 102.9.0-3.el9_1 | |
netstandard-targeting-pack-2.1 | 7.0.104-1.el9_1 | |
nspr | 4.34.0-17.el9_1 | |
nspr-debuginfo | 4.34.0-17.el9_1 | |
nspr-devel | 4.34.0-17.el9_1 | |
nss | 3.79.0-17.el9_1 | |
nss-debuginfo | 3.79.0-17.el9_1 | |
nss-debugsource | 3.79.0-17.el9_1 | |
nss-devel | 3.79.0-17.el9_1 | |
nss-softokn | 3.79.0-17.el9_1 | |
nss-softokn-debuginfo | 3.79.0-17.el9_1 | |
nss-softokn-devel | 3.79.0-17.el9_1 | |
nss-softokn-freebl | 3.79.0-17.el9_1 | |
nss-softokn-freebl-debuginfo | 3.79.0-17.el9_1 | |
nss-softokn-freebl-devel | 3.79.0-17.el9_1 | |
nss-sysinit | 3.79.0-17.el9_1 | |
nss-sysinit-debuginfo | 3.79.0-17.el9_1 | |
nss-tools | 3.79.0-17.el9_1 | |
nss-tools-debuginfo | 3.79.0-17.el9_1 | |
nss-util | 3.79.0-17.el9_1 | |
nss-util-debuginfo | 3.79.0-17.el9_1 | |
nss-util-devel | 3.79.0-17.el9_1 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-host-debuginfo | 7.0.4-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.115-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.104-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet6.0-debugsource | 6.0.115-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet7.0-debugsource | 7.0.104-1.el9_1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.6-0.rh9.cern | |

