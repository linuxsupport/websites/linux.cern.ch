## 2023-02-22

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.4-1.rh9.cern | |
pyphonebook | 2.1.5-1.rh9.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9.0-2.rh9.cern | |
kmod-openafs | 1.8.9.0-2.5.14.0_162.12.1.el9_1.rh9.cern | |
openafs | 1.8.9.0-2.rh9.cern | |
openafs-authlibs | 1.8.9.0-2.rh9.cern | |
openafs-authlibs-devel | 1.8.9.0-2.rh9.cern | |
openafs-client | 1.8.9.0-2.rh9.cern | |
openafs-compat | 1.8.9.0-2.rh9.cern | |
openafs-debugsource | 1.8.9.0-2.rh9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_162.12.1.el9_1-2.rh9.cern | |
openafs-devel | 1.8.9.0-2.rh9.cern | |
openafs-docs | 1.8.9.0-2.rh9.cern | |
openafs-kernel-source | 1.8.9.0-2.rh9.cern | |
openafs-krb5 | 1.8.9.0-2.rh9.cern | |
openafs-server | 1.8.9.0-2.rh9.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grub2-common | 2.06-46.el9_1.3 | |
grub2-debuginfo | 2.06-46.el9_1.3 | |
grub2-debugsource | 2.06-46.el9_1.3 | |
grub2-efi-aa64-modules | 2.06-46.el9_1.3 | |
grub2-efi-x64 | 2.06-46.el9_1.3 | |
grub2-efi-x64-cdboot | 2.06-46.el9_1.3 | |
grub2-efi-x64-modules | 2.06-46.el9_1.3 | |
grub2-emu-debuginfo | 2.06-46.el9_1.3 | |
grub2-pc | 2.06-46.el9_1.3 | |
grub2-pc-modules | 2.06-46.el9_1.3 | |
grub2-tools | 2.06-46.el9_1.3 | |
grub2-tools-debuginfo | 2.06-46.el9_1.3 | |
grub2-tools-efi | 2.06-46.el9_1.3 | |
grub2-tools-efi-debuginfo | 2.06-46.el9_1.3 | |
grub2-tools-extra | 2.06-46.el9_1.3 | |
grub2-tools-extra-debuginfo | 2.06-46.el9_1.3 | |
grub2-tools-minimal | 2.06-46.el9_1.3 | |
grub2-tools-minimal-debuginfo | 2.06-46.el9_1.3 | |
selinux-policy | 34.1.43-1.el9_1.1 | |
selinux-policy-doc | 34.1.43-1.el9_1.1 | |
selinux-policy-mls | 34.1.43-1.el9_1.1 | |
selinux-policy-sandbox | 34.1.43-1.el9_1.1 | |
selinux-policy-targeted | 34.1.43-1.el9_1.1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy-devel | 34.1.43-1.el9_1.1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.4-1.rh9.cern | |
pyphonebook | 2.1.5-1.rh9.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9.0-2.rh9.cern | |
kmod-openafs | 1.8.9.0-2.5.14.0_162.12.1.el9_1.rh9.cern | |
openafs | 1.8.9.0-2.rh9.cern | |
openafs-authlibs | 1.8.9.0-2.rh9.cern | |
openafs-authlibs-devel | 1.8.9.0-2.rh9.cern | |
openafs-client | 1.8.9.0-2.rh9.cern | |
openafs-compat | 1.8.9.0-2.rh9.cern | |
openafs-debugsource | 1.8.9.0-2.rh9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_162.12.1.el9_1-2.rh9.cern | |
openafs-devel | 1.8.9.0-2.rh9.cern | |
openafs-docs | 1.8.9.0-2.rh9.cern | |
openafs-kernel-source | 1.8.9.0-2.rh9.cern | |
openafs-krb5 | 1.8.9.0-2.rh9.cern | |
openafs-server | 1.8.9.0-2.rh9.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grub2-common | 2.06-46.el9_1.3 | |
grub2-debuginfo | 2.06-46.el9_1.3 | |
grub2-debugsource | 2.06-46.el9_1.3 | |
grub2-efi-aa64 | 2.06-46.el9_1.3 | |
grub2-efi-aa64-cdboot | 2.06-46.el9_1.3 | |
grub2-efi-aa64-modules | 2.06-46.el9_1.3 | |
grub2-efi-x64-modules | 2.06-46.el9_1.3 | |
grub2-emu-debuginfo | 2.06-46.el9_1.3 | |
grub2-tools | 2.06-46.el9_1.3 | |
grub2-tools-debuginfo | 2.06-46.el9_1.3 | |
grub2-tools-extra | 2.06-46.el9_1.3 | |
grub2-tools-extra-debuginfo | 2.06-46.el9_1.3 | |
grub2-tools-minimal | 2.06-46.el9_1.3 | |
grub2-tools-minimal-debuginfo | 2.06-46.el9_1.3 | |
selinux-policy | 34.1.43-1.el9_1.1 | |
selinux-policy-doc | 34.1.43-1.el9_1.1 | |
selinux-policy-mls | 34.1.43-1.el9_1.1 | |
selinux-policy-sandbox | 34.1.43-1.el9_1.1 | |
selinux-policy-targeted | 34.1.43-1.el9_1.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy-devel | 34.1.43-1.el9_1.1 | |

