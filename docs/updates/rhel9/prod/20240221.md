## 2024-02-21

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy | 38.1.23-1.el9_3.2 | [RHBA-2024:0770](https://access.redhat.com/errata/RHBA-2024:0770) | <div class="adv_b">Bug Fix Advisory</div>
selinux-policy-doc | 38.1.23-1.el9_3.2 | [RHBA-2024:0770](https://access.redhat.com/errata/RHBA-2024:0770) | <div class="adv_b">Bug Fix Advisory</div>
selinux-policy-mls | 38.1.23-1.el9_3.2 | [RHBA-2024:0770](https://access.redhat.com/errata/RHBA-2024:0770) | <div class="adv_b">Bug Fix Advisory</div>
selinux-policy-sandbox | 38.1.23-1.el9_3.2 | [RHBA-2024:0770](https://access.redhat.com/errata/RHBA-2024:0770) | <div class="adv_b">Bug Fix Advisory</div>
selinux-policy-targeted | 38.1.23-1.el9_3.2 | [RHBA-2024:0770](https://access.redhat.com/errata/RHBA-2024:0770) | <div class="adv_b">Bug Fix Advisory</div>
sos | 4.6.1-1.el9 | [RHBA-2024:0721](https://access.redhat.com/errata/RHBA-2024:0721) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.6.1-1.el9 | [RHBA-2024:0721](https://access.redhat.com/errata/RHBA-2024:0721) | <div class="adv_b">Bug Fix Advisory</div>
sudo | 1.9.5p2-10.el9_3 | [RHSA-2024:0811](https://access.redhat.com/errata/RHSA-2024:0811) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28486](https://access.redhat.com/security/cve/CVE-2023-28486), [CVE-2023-28487](https://access.redhat.com/security/cve/CVE-2023-28487), [CVE-2023-42465](https://access.redhat.com/security/cve/CVE-2023-42465))
sudo-debuginfo | 1.9.5p2-10.el9_3 | |
sudo-debugsource | 1.9.5p2-10.el9_3 | |
sudo-python-plugin-debuginfo | 1.9.5p2-10.el9_3 | |
tzdata | 2024a-1.el9 | [RHBA-2024:0762](https://access.redhat.com/errata/RHBA-2024:0762) | <div class="adv_b">Bug Fix Advisory</div>

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.27-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-runtime-7.0 | 7.0.16-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-targeting-pack-6.0 | 6.0.27-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-targeting-pack-7.0 | 7.0.16-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-6.0 | 6.0.27-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-apphost-pack-7.0 | 7.0.16-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-hostfxr-6.0 | 6.0.27-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-hostfxr-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-hostfxr-7.0 | 7.0.16-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-hostfxr-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-runtime-6.0 | 6.0.27-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-runtime-7.0 | 7.0.16-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-sdk-6.0 | 6.0.127-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet-sdk-7.0 | 7.0.116-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet-targeting-pack-6.0 | 6.0.27-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-targeting-pack-7.0 | 7.0.16-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-templates-6.0 | 6.0.127-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-templates-7.0 | 7.0.116-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet6.0-debugsource | 6.0.127-1.el9_3 | |
dotnet7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet7.0-debugsource | 7.0.116-1.el9_3 | |
nspr | 4.35.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nspr-debuginfo | 4.35.0-6.el9_3 | |
nspr-devel | 4.35.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-debuginfo | 3.90.0-6.el9_3 | |
nss-debugsource | 3.90.0-6.el9_3 | |
nss-devel | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-debuginfo | 3.90.0-6.el9_3 | |
nss-softokn-devel | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-freebl | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-freebl-debuginfo | 3.90.0-6.el9_3 | |
nss-softokn-freebl-devel | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-sysinit | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-sysinit-debuginfo | 3.90.0-6.el9_3 | |
nss-tools | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-tools-debuginfo | 3.90.0-6.el9_3 | |
nss-util | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-util-debuginfo | 3.90.0-6.el9_3 | |
nss-util-devel | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
osbuild | 93-1.el9_3.1 | [RHBA-2024:0718](https://access.redhat.com/errata/RHBA-2024:0718) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-luks2 | 93-1.el9_3.1 | [RHBA-2024:0718](https://access.redhat.com/errata/RHBA-2024:0718) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-lvm2 | 93-1.el9_3.1 | [RHBA-2024:0718](https://access.redhat.com/errata/RHBA-2024:0718) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-ostree | 93-1.el9_3.1 | [RHBA-2024:0718](https://access.redhat.com/errata/RHBA-2024:0718) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-selinux | 93-1.el9_3.1 | [RHBA-2024:0718](https://access.redhat.com/errata/RHBA-2024:0718) | <div class="adv_b">Bug Fix Advisory</div>
python3-osbuild | 93-1.el9_3.1 | [RHBA-2024:0718](https://access.redhat.com/errata/RHBA-2024:0718) | <div class="adv_b">Bug Fix Advisory</div>
selinux-policy-devel | 38.1.23-1.el9_3.2 | [RHBA-2024:0770](https://access.redhat.com/errata/RHBA-2024:0770) | <div class="adv_b">Bug Fix Advisory</div>
sudo-debuginfo | 1.9.5p2-10.el9_3 | |
sudo-debugsource | 1.9.5p2-10.el9_3 | |
sudo-python-plugin | 1.9.5p2-10.el9_3 | [RHSA-2024:0811](https://access.redhat.com/errata/RHSA-2024:0811) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28486](https://access.redhat.com/security/cve/CVE-2023-28486), [CVE-2023-28487](https://access.redhat.com/security/cve/CVE-2023-28487), [CVE-2023-42465](https://access.redhat.com/security/cve/CVE-2023-42465))
sudo-python-plugin-debuginfo | 1.9.5p2-10.el9_3 | |
tzdata-java | 2024a-1.el9 | [RHBA-2024:0762](https://access.redhat.com/errata/RHBA-2024:0762) | <div class="adv_b">Bug Fix Advisory</div>

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-runtime-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-runtime-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-sdk-6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.127-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.116-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet6.0-debugsource | 6.0.127-1.el9_3 | |
dotnet7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet7.0-debugsource | 7.0.116-1.el9_3 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy | 38.1.23-1.el9_3.2 | [RHBA-2024:0770](https://access.redhat.com/errata/RHBA-2024:0770) | <div class="adv_b">Bug Fix Advisory</div>
selinux-policy-doc | 38.1.23-1.el9_3.2 | [RHBA-2024:0770](https://access.redhat.com/errata/RHBA-2024:0770) | <div class="adv_b">Bug Fix Advisory</div>
selinux-policy-mls | 38.1.23-1.el9_3.2 | [RHBA-2024:0770](https://access.redhat.com/errata/RHBA-2024:0770) | <div class="adv_b">Bug Fix Advisory</div>
selinux-policy-sandbox | 38.1.23-1.el9_3.2 | [RHBA-2024:0770](https://access.redhat.com/errata/RHBA-2024:0770) | <div class="adv_b">Bug Fix Advisory</div>
selinux-policy-targeted | 38.1.23-1.el9_3.2 | [RHBA-2024:0770](https://access.redhat.com/errata/RHBA-2024:0770) | <div class="adv_b">Bug Fix Advisory</div>
sos | 4.6.1-1.el9 | [RHBA-2024:0721](https://access.redhat.com/errata/RHBA-2024:0721) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.6.1-1.el9 | [RHBA-2024:0721](https://access.redhat.com/errata/RHBA-2024:0721) | <div class="adv_b">Bug Fix Advisory</div>
sudo | 1.9.5p2-10.el9_3 | [RHSA-2024:0811](https://access.redhat.com/errata/RHSA-2024:0811) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28486](https://access.redhat.com/security/cve/CVE-2023-28486), [CVE-2023-28487](https://access.redhat.com/security/cve/CVE-2023-28487), [CVE-2023-42465](https://access.redhat.com/security/cve/CVE-2023-42465))
sudo-debuginfo | 1.9.5p2-10.el9_3 | |
sudo-debugsource | 1.9.5p2-10.el9_3 | |
sudo-python-plugin-debuginfo | 1.9.5p2-10.el9_3 | |
tzdata | 2024a-1.el9 | [RHBA-2024:0762](https://access.redhat.com/errata/RHBA-2024:0762) | <div class="adv_b">Bug Fix Advisory</div>

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.27-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-runtime-7.0 | 7.0.16-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-targeting-pack-6.0 | 6.0.27-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-targeting-pack-7.0 | 7.0.16-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-6.0 | 6.0.27-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-apphost-pack-7.0 | 7.0.16-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-hostfxr-6.0 | 6.0.27-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-hostfxr-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-hostfxr-7.0 | 7.0.16-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-hostfxr-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-runtime-6.0 | 6.0.27-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-runtime-7.0 | 7.0.16-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-sdk-6.0 | 6.0.127-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet-sdk-7.0 | 7.0.116-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet-targeting-pack-6.0 | 6.0.27-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-targeting-pack-7.0 | 7.0.16-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-templates-6.0 | 6.0.127-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-templates-7.0 | 7.0.116-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet6.0-debugsource | 6.0.127-1.el9_3 | |
dotnet7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet7.0-debugsource | 7.0.116-1.el9_3 | |
nspr | 4.35.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nspr-debuginfo | 4.35.0-6.el9_3 | |
nspr-devel | 4.35.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-debuginfo | 3.90.0-6.el9_3 | |
nss-debugsource | 3.90.0-6.el9_3 | |
nss-devel | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-debuginfo | 3.90.0-6.el9_3 | |
nss-softokn-devel | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-freebl | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-freebl-debuginfo | 3.90.0-6.el9_3 | |
nss-softokn-freebl-devel | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-sysinit | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-sysinit-debuginfo | 3.90.0-6.el9_3 | |
nss-tools | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-tools-debuginfo | 3.90.0-6.el9_3 | |
nss-util | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-util-debuginfo | 3.90.0-6.el9_3 | |
nss-util-devel | 3.90.0-6.el9_3 | [RHSA-2024:0790](https://access.redhat.com/errata/RHSA-2024:0790) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
osbuild | 93-1.el9_3.1 | [RHBA-2024:0718](https://access.redhat.com/errata/RHBA-2024:0718) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-luks2 | 93-1.el9_3.1 | [RHBA-2024:0718](https://access.redhat.com/errata/RHBA-2024:0718) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-lvm2 | 93-1.el9_3.1 | [RHBA-2024:0718](https://access.redhat.com/errata/RHBA-2024:0718) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-ostree | 93-1.el9_3.1 | [RHBA-2024:0718](https://access.redhat.com/errata/RHBA-2024:0718) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-selinux | 93-1.el9_3.1 | [RHBA-2024:0718](https://access.redhat.com/errata/RHBA-2024:0718) | <div class="adv_b">Bug Fix Advisory</div>
python3-osbuild | 93-1.el9_3.1 | [RHBA-2024:0718](https://access.redhat.com/errata/RHBA-2024:0718) | <div class="adv_b">Bug Fix Advisory</div>
selinux-policy-devel | 38.1.23-1.el9_3.2 | [RHBA-2024:0770](https://access.redhat.com/errata/RHBA-2024:0770) | <div class="adv_b">Bug Fix Advisory</div>
sudo-debuginfo | 1.9.5p2-10.el9_3 | |
sudo-debugsource | 1.9.5p2-10.el9_3 | |
sudo-python-plugin | 1.9.5p2-10.el9_3 | [RHSA-2024:0811](https://access.redhat.com/errata/RHSA-2024:0811) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28486](https://access.redhat.com/security/cve/CVE-2023-28486), [CVE-2023-28487](https://access.redhat.com/security/cve/CVE-2023-28487), [CVE-2023-42465](https://access.redhat.com/security/cve/CVE-2023-42465))
sudo-python-plugin-debuginfo | 1.9.5p2-10.el9_3 | |
tzdata-java | 2024a-1.el9 | [RHBA-2024:0762](https://access.redhat.com/errata/RHBA-2024:0762) | <div class="adv_b">Bug Fix Advisory</div>

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-runtime-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-runtime-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-sdk-6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.127-1.el9_3 | [RHSA-2024:0807](https://access.redhat.com/errata/RHSA-2024:0807) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.116-1.el9_3 | [RHSA-2024:0805](https://access.redhat.com/errata/RHSA-2024:0805) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet6.0-debugsource | 6.0.127-1.el9_3 | |
dotnet7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet7.0-debugsource | 7.0.116-1.el9_3 | |

