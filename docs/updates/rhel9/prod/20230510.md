## 2023-05-10

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs | 27.2-6.el9_1.1 | |
emacs-common | 27.2-6.el9_1.1 | |
emacs-common-debuginfo | 27.2-6.el9_1.1 | |
emacs-debuginfo | 27.2-6.el9_1.1 | |
emacs-debugsource | 27.2-6.el9_1.1 | |
emacs-filesystem | 27.2-6.el9_1.1 | |
emacs-lucid | 27.2-6.el9_1.1 | |
emacs-lucid-debuginfo | 27.2-6.el9_1.1 | |
emacs-nox | 27.2-6.el9_1.1 | |
emacs-nox-debuginfo | 27.2-6.el9_1.1 | |
libwebp | 1.2.0-6.el9_1 | |
libwebp-debuginfo | 1.2.0-6.el9_1 | |
libwebp-debugsource | 1.2.0-6.el9_1 | |
libwebp-devel | 1.2.0-6.el9_1 | |
libwebp-java-debuginfo | 1.2.0-6.el9_1 | |
libwebp-tools-debuginfo | 1.2.0-6.el9_1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs | 27.2-6.el9_1.1 | |
emacs-common | 27.2-6.el9_1.1 | |
emacs-common-debuginfo | 27.2-6.el9_1.1 | |
emacs-debuginfo | 27.2-6.el9_1.1 | |
emacs-debugsource | 27.2-6.el9_1.1 | |
emacs-filesystem | 27.2-6.el9_1.1 | |
emacs-lucid | 27.2-6.el9_1.1 | |
emacs-lucid-debuginfo | 27.2-6.el9_1.1 | |
emacs-nox | 27.2-6.el9_1.1 | |
emacs-nox-debuginfo | 27.2-6.el9_1.1 | |
libwebp | 1.2.0-6.el9_1 | |
libwebp-debuginfo | 1.2.0-6.el9_1 | |
libwebp-debugsource | 1.2.0-6.el9_1 | |
libwebp-devel | 1.2.0-6.el9_1 | |
libwebp-java-debuginfo | 1.2.0-6.el9_1 | |
libwebp-tools-debuginfo | 1.2.0-6.el9_1 | |

