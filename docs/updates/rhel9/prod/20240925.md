## 2024-09-25

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
mdadm | 4.2-14.el9_4 | [RHBA-2024:6577](https://access.redhat.com/errata/RHBA-2024:6577) | <div class="adv_b">Bug Fix Advisory</div>
mdadm-debuginfo | 4.2-14.el9_4 | |
mdadm-debugsource | 4.2-14.el9_4 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-common | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-agents-compute | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-agents-debuginfo | 4.10.0-62.el9_4.5 | |
fence-agents-debugsource | 4.10.0-62.el9_4.5 | |
fence-agents-ibm-powervs | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-agents-ibm-vpc | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-agents-kdump-debuginfo | 4.10.0-62.el9_4.5 | |
fence-agents-kubevirt | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-agents-kubevirt-debuginfo | 4.10.0-62.el9_4.5 | |
fence-agents-virsh | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-virt | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-virt-debuginfo | 4.10.0-62.el9_4.5 | |
fence-virtd | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-virtd-cpg | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-virtd-cpg-debuginfo | 4.10.0-62.el9_4.5 | |
fence-virtd-debuginfo | 4.10.0-62.el9_4.5 | |
fence-virtd-libvirt | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-virtd-libvirt-debuginfo | 4.10.0-62.el9_4.5 | |
fence-virtd-multicast | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-virtd-multicast-debuginfo | 4.10.0-62.el9_4.5 | |
fence-virtd-serial | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-virtd-serial-debuginfo | 4.10.0-62.el9_4.5 | |
fence-virtd-tcp | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-virtd-tcp-debuginfo | 4.10.0-62.el9_4.5 | |
firefox | 128.2.0-1.el9_4 | [RHSA-2024:6681](https://access.redhat.com/errata/RHSA-2024:6681) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7652](https://access.redhat.com/security/cve/CVE-2024-7652), [CVE-2024-8381](https://access.redhat.com/security/cve/CVE-2024-8381), [CVE-2024-8382](https://access.redhat.com/security/cve/CVE-2024-8382), [CVE-2024-8383](https://access.redhat.com/security/cve/CVE-2024-8383), [CVE-2024-8384](https://access.redhat.com/security/cve/CVE-2024-8384), [CVE-2024-8385](https://access.redhat.com/security/cve/CVE-2024-8385), [CVE-2024-8386](https://access.redhat.com/security/cve/CVE-2024-8386), [CVE-2024-8387](https://access.redhat.com/security/cve/CVE-2024-8387))
firefox-debuginfo | 128.2.0-1.el9_4 | |
firefox-debugsource | 128.2.0-1.el9_4 | |
firefox-x11 | 128.2.0-1.el9_4 | [RHSA-2024:6681](https://access.redhat.com/errata/RHSA-2024:6681) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7652](https://access.redhat.com/security/cve/CVE-2024-7652), [CVE-2024-8381](https://access.redhat.com/security/cve/CVE-2024-8381), [CVE-2024-8382](https://access.redhat.com/security/cve/CVE-2024-8382), [CVE-2024-8383](https://access.redhat.com/security/cve/CVE-2024-8383), [CVE-2024-8384](https://access.redhat.com/security/cve/CVE-2024-8384), [CVE-2024-8385](https://access.redhat.com/security/cve/CVE-2024-8385), [CVE-2024-8386](https://access.redhat.com/security/cve/CVE-2024-8386), [CVE-2024-8387](https://access.redhat.com/security/cve/CVE-2024-8387))
greenboot | 0.15.6-1.el9_4 | [RHBA-2024:6728](https://access.redhat.com/errata/RHBA-2024:6728) | <div class="adv_b">Bug Fix Advisory</div>
greenboot-default-health-checks | 0.15.6-1.el9_4 | [RHBA-2024:6728](https://access.redhat.com/errata/RHBA-2024:6728) | <div class="adv_b">Bug Fix Advisory</div>
ha-cloud-support-debuginfo | 4.10.0-62.el9_4.5 | |
libvirt | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-client | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-client-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-client-qemu | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-common | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-common-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-config-network | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-config-nwfilter | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-interface | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-interface-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-network | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-network-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-nodedev | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-nodedev-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-nwfilter | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-nwfilter-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-qemu | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-qemu-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-secret | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-secret-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-core | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-core-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-disk | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-disk-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-iscsi | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-iscsi-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-logical | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-logical-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-mpath | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-mpath-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-rbd | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-rbd-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-scsi | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-scsi-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-kvm | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-lock | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-lock-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-log | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-log-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-plugin-lockd | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-plugin-lockd-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-plugin-sanlock-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-proxy | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-proxy-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-debugsource | 10.0.0-6.7.el9_4 | |
libvirt-libs | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-libs-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-nss | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-nss-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-wireshark-debuginfo | 10.0.0-6.7.el9_4 | |
nspr | 4.35.0-14.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nspr-debuginfo | 4.35.0-14.el9_2 | |
nspr-devel | 4.35.0-14.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-debuginfo | 3.101.0-7.el9_2 | |
nss-debugsource | 3.101.0-7.el9_2 | |
nss-devel | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-debuginfo | 3.101.0-7.el9_2 | |
nss-softokn-devel | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-freebl | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-freebl-debuginfo | 3.101.0-7.el9_2 | |
nss-softokn-freebl-devel | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-sysinit | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-sysinit-debuginfo | 3.101.0-7.el9_2 | |
nss-tools | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-tools-debuginfo | 3.101.0-7.el9_2 | |
nss-util | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-util-debuginfo | 3.101.0-7.el9_2 | |
nss-util-devel | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
thunderbird | 128.2.0-1.el9_4 | [RHSA-2024:6683](https://access.redhat.com/errata/RHSA-2024:6683) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7652](https://access.redhat.com/security/cve/CVE-2024-7652), [CVE-2024-8381](https://access.redhat.com/security/cve/CVE-2024-8381), [CVE-2024-8382](https://access.redhat.com/security/cve/CVE-2024-8382), [CVE-2024-8384](https://access.redhat.com/security/cve/CVE-2024-8384), [CVE-2024-8385](https://access.redhat.com/security/cve/CVE-2024-8385), [CVE-2024-8386](https://access.redhat.com/security/cve/CVE-2024-8386), [CVE-2024-8387](https://access.redhat.com/security/cve/CVE-2024-8387), [CVE-2024-8394](https://access.redhat.com/security/cve/CVE-2024-8394))
thunderbird-debuginfo | 128.2.0-1.el9_4 | |
thunderbird-debugsource | 128.2.0-1.el9_4 | |

### highavailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-aliyun | 4.10.0-62.el9_4.5 | |
fence-agents-all | 4.10.0-62.el9_4.5 | |
fence-agents-amt-ws | 4.10.0-62.el9_4.5 | |
fence-agents-apc | 4.10.0-62.el9_4.5 | |
fence-agents-apc-snmp | 4.10.0-62.el9_4.5 | |
fence-agents-aws | 4.10.0-62.el9_4.5 | |
fence-agents-azure-arm | 4.10.0-62.el9_4.5 | |
fence-agents-bladecenter | 4.10.0-62.el9_4.5 | |
fence-agents-brocade | 4.10.0-62.el9_4.5 | |
fence-agents-cisco-mds | 4.10.0-62.el9_4.5 | |
fence-agents-cisco-ucs | 4.10.0-62.el9_4.5 | |
fence-agents-debuginfo | 4.10.0-62.el9_4.5 | |
fence-agents-debugsource | 4.10.0-62.el9_4.5 | |
fence-agents-drac5 | 4.10.0-62.el9_4.5 | |
fence-agents-eaton-snmp | 4.10.0-62.el9_4.5 | |
fence-agents-emerson | 4.10.0-62.el9_4.5 | |
fence-agents-eps | 4.10.0-62.el9_4.5 | |
fence-agents-gce | 4.10.0-62.el9_4.5 | |
fence-agents-heuristics-ping | 4.10.0-62.el9_4.5 | |
fence-agents-hpblade | 4.10.0-62.el9_4.5 | |
fence-agents-ibmblade | 4.10.0-62.el9_4.5 | |
fence-agents-ifmib | 4.10.0-62.el9_4.5 | |
fence-agents-ilo-moonshot | 4.10.0-62.el9_4.5 | |
fence-agents-ilo-mp | 4.10.0-62.el9_4.5 | |
fence-agents-ilo-ssh | 4.10.0-62.el9_4.5 | |
fence-agents-ilo2 | 4.10.0-62.el9_4.5 | |
fence-agents-intelmodular | 4.10.0-62.el9_4.5 | |
fence-agents-ipdu | 4.10.0-62.el9_4.5 | |
fence-agents-ipmilan | 4.10.0-62.el9_4.5 | |
fence-agents-kdump | 4.10.0-62.el9_4.5 | |
fence-agents-kdump-debuginfo | 4.10.0-62.el9_4.5 | |
fence-agents-kubevirt-debuginfo | 4.10.0-62.el9_4.5 | |
fence-agents-mpath | 4.10.0-62.el9_4.5 | |
fence-agents-openstack | 4.10.0-62.el9_4.5 | |
fence-agents-redfish | 4.10.0-62.el9_4.5 | |
fence-agents-rhevm | 4.10.0-62.el9_4.5 | |
fence-agents-rsa | 4.10.0-62.el9_4.5 | |
fence-agents-rsb | 4.10.0-62.el9_4.5 | |
fence-agents-sbd | 4.10.0-62.el9_4.5 | |
fence-agents-scsi | 4.10.0-62.el9_4.5 | |
fence-agents-vmware-rest | 4.10.0-62.el9_4.5 | |
fence-agents-vmware-soap | 4.10.0-62.el9_4.5 | |
fence-agents-wti | 4.10.0-62.el9_4.5 | |
fence-virt-debuginfo | 4.10.0-62.el9_4.5 | |
fence-virtd-cpg-debuginfo | 4.10.0-62.el9_4.5 | |
fence-virtd-debuginfo | 4.10.0-62.el9_4.5 | |
fence-virtd-libvirt-debuginfo | 4.10.0-62.el9_4.5 | |
fence-virtd-multicast-debuginfo | 4.10.0-62.el9_4.5 | |
fence-virtd-serial-debuginfo | 4.10.0-62.el9_4.5 | |
fence-virtd-tcp-debuginfo | 4.10.0-62.el9_4.5 | |
ha-cloud-support | 4.10.0-62.el9_4.5 | |
ha-cloud-support-debuginfo | 4.10.0-62.el9_4.5 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libvirt-client-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-common-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-interface-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-network-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-nodedev-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-nwfilter-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-qemu-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-secret-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-core-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-disk-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-iscsi-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-logical-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-mpath-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-rbd-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-scsi-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-lock-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-log-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-plugin-lockd-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-plugin-sanlock | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-plugin-sanlock-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-proxy-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-debugsource | 10.0.0-6.7.el9_4 | |
libvirt-devel | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-docs | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-libs-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-nss-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-wireshark-debuginfo | 10.0.0-6.7.el9_4 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
mdadm | 4.2-14.el9_4 | [RHBA-2024:6577](https://access.redhat.com/errata/RHBA-2024:6577) | <div class="adv_b">Bug Fix Advisory</div>
mdadm-debuginfo | 4.2-14.el9_4 | |
mdadm-debugsource | 4.2-14.el9_4 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-common | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-agents-debuginfo | 4.10.0-62.el9_4.5 | |
fence-agents-debugsource | 4.10.0-62.el9_4.5 | |
fence-agents-ibm-powervs | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-agents-ibm-vpc | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-agents-kdump-debuginfo | 4.10.0-62.el9_4.5 | |
fence-agents-kubevirt | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
fence-agents-kubevirt-debuginfo | 4.10.0-62.el9_4.5 | |
fence-agents-virsh | 4.10.0-62.el9_4.5 | [RHSA-2024:6726](https://access.redhat.com/errata/RHSA-2024:6726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6345](https://access.redhat.com/security/cve/CVE-2024-6345))
firefox | 128.2.0-1.el9_4 | [RHSA-2024:6681](https://access.redhat.com/errata/RHSA-2024:6681) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7652](https://access.redhat.com/security/cve/CVE-2024-7652), [CVE-2024-8381](https://access.redhat.com/security/cve/CVE-2024-8381), [CVE-2024-8382](https://access.redhat.com/security/cve/CVE-2024-8382), [CVE-2024-8383](https://access.redhat.com/security/cve/CVE-2024-8383), [CVE-2024-8384](https://access.redhat.com/security/cve/CVE-2024-8384), [CVE-2024-8385](https://access.redhat.com/security/cve/CVE-2024-8385), [CVE-2024-8386](https://access.redhat.com/security/cve/CVE-2024-8386), [CVE-2024-8387](https://access.redhat.com/security/cve/CVE-2024-8387))
firefox-debuginfo | 128.2.0-1.el9_4 | |
firefox-debugsource | 128.2.0-1.el9_4 | |
firefox-x11 | 128.2.0-1.el9_4 | [RHSA-2024:6681](https://access.redhat.com/errata/RHSA-2024:6681) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7652](https://access.redhat.com/security/cve/CVE-2024-7652), [CVE-2024-8381](https://access.redhat.com/security/cve/CVE-2024-8381), [CVE-2024-8382](https://access.redhat.com/security/cve/CVE-2024-8382), [CVE-2024-8383](https://access.redhat.com/security/cve/CVE-2024-8383), [CVE-2024-8384](https://access.redhat.com/security/cve/CVE-2024-8384), [CVE-2024-8385](https://access.redhat.com/security/cve/CVE-2024-8385), [CVE-2024-8386](https://access.redhat.com/security/cve/CVE-2024-8386), [CVE-2024-8387](https://access.redhat.com/security/cve/CVE-2024-8387))
greenboot | 0.15.6-1.el9_4 | [RHBA-2024:6728](https://access.redhat.com/errata/RHBA-2024:6728) | <div class="adv_b">Bug Fix Advisory</div>
greenboot-default-health-checks | 0.15.6-1.el9_4 | [RHBA-2024:6728](https://access.redhat.com/errata/RHBA-2024:6728) | <div class="adv_b">Bug Fix Advisory</div>
libvirt | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-client | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-client-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-client-qemu | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-common | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-common-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-config-network | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-config-nwfilter | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-interface | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-interface-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-network | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-network-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-nodedev | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-nodedev-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-nwfilter | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-nwfilter-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-qemu | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-qemu-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-secret | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-secret-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-core | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-core-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-disk | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-disk-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-iscsi | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-iscsi-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-logical | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-logical-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-mpath | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-mpath-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-rbd | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-rbd-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-scsi | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-driver-storage-scsi-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-kvm | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-lock | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-lock-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-log | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-log-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-plugin-lockd | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-plugin-lockd-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-plugin-sanlock-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-proxy | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-proxy-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-debugsource | 10.0.0-6.7.el9_4 | |
libvirt-libs | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-libs-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-nss | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-nss-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-wireshark-debuginfo | 10.0.0-6.7.el9_4 | |
nspr | 4.35.0-14.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nspr-debuginfo | 4.35.0-14.el9_2 | |
nspr-devel | 4.35.0-14.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-debuginfo | 3.101.0-7.el9_2 | |
nss-debugsource | 3.101.0-7.el9_2 | |
nss-devel | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-debuginfo | 3.101.0-7.el9_2 | |
nss-softokn-devel | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-freebl | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-freebl-debuginfo | 3.101.0-7.el9_2 | |
nss-softokn-freebl-devel | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-sysinit | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-sysinit-debuginfo | 3.101.0-7.el9_2 | |
nss-tools | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-tools-debuginfo | 3.101.0-7.el9_2 | |
nss-util | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
nss-util-debuginfo | 3.101.0-7.el9_2 | |
nss-util-devel | 3.101.0-7.el9_2 | [RHBA-2024:6679](https://access.redhat.com/errata/RHBA-2024:6679) | <div class="adv_b">Bug Fix Advisory</div>
thunderbird | 128.2.0-1.el9_4 | [RHSA-2024:6683](https://access.redhat.com/errata/RHSA-2024:6683) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7652](https://access.redhat.com/security/cve/CVE-2024-7652), [CVE-2024-8381](https://access.redhat.com/security/cve/CVE-2024-8381), [CVE-2024-8382](https://access.redhat.com/security/cve/CVE-2024-8382), [CVE-2024-8384](https://access.redhat.com/security/cve/CVE-2024-8384), [CVE-2024-8385](https://access.redhat.com/security/cve/CVE-2024-8385), [CVE-2024-8386](https://access.redhat.com/security/cve/CVE-2024-8386), [CVE-2024-8387](https://access.redhat.com/security/cve/CVE-2024-8387), [CVE-2024-8394](https://access.redhat.com/security/cve/CVE-2024-8394))
thunderbird-debuginfo | 128.2.0-1.el9_4 | |
thunderbird-debugsource | 128.2.0-1.el9_4 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libvirt-client-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-common-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-interface-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-network-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-nodedev-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-nwfilter-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-qemu-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-secret-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-core-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-disk-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-iscsi-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-logical-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-mpath-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-rbd-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-driver-storage-scsi-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-lock-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-log-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-plugin-lockd-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-plugin-sanlock | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-daemon-plugin-sanlock-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-daemon-proxy-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-debugsource | 10.0.0-6.7.el9_4 | |
libvirt-devel | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-docs | 10.0.0-6.7.el9_4 | [RHBA-2024:6669](https://access.redhat.com/errata/RHBA-2024:6669) | <div class="adv_b">Bug Fix Advisory</div>
libvirt-libs-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-nss-debuginfo | 10.0.0-6.7.el9_4 | |
libvirt-wireshark-debuginfo | 10.0.0-6.7.el9_4 | |

