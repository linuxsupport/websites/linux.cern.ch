## 2024-12-11

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.8-1.rh9.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kpatch-patch-5_14_0-503_15_1 | 0-0.el9_5 | [RHEA-2024:10600](https://access.redhat.com/errata/RHEA-2024:10600) | <div class="adv_e">Product Enhancement Advisory</div>
sos | 4.8.1-1.el9_4 | [RHBA-2024:10663](https://access.redhat.com/errata/RHBA-2024:10663) | <div class="adv_b">Bug Fix Advisory</div>
sos | 4.8.1-1.el9_5 | [RHBA-2024:10663](https://access.redhat.com/errata/RHBA-2024:10663) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.8.1-1.el9_4 | [RHBA-2024:10663](https://access.redhat.com/errata/RHBA-2024:10663) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.8.1-1.el9_5 | [RHBA-2024:10663](https://access.redhat.com/errata/RHBA-2024:10663) | <div class="adv_b">Bug Fix Advisory</div>

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 128.5.1-1.el9_5 | [RHSA-2024:10702](https://access.redhat.com/errata/RHSA-2024:10702) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11692](https://access.redhat.com/security/cve/CVE-2024-11692), [CVE-2024-11694](https://access.redhat.com/security/cve/CVE-2024-11694), [CVE-2024-11695](https://access.redhat.com/security/cve/CVE-2024-11695), [CVE-2024-11696](https://access.redhat.com/security/cve/CVE-2024-11696), [CVE-2024-11697](https://access.redhat.com/security/cve/CVE-2024-11697), [CVE-2024-11699](https://access.redhat.com/security/cve/CVE-2024-11699))
firefox-debuginfo | 128.5.1-1.el9_5 | |
firefox-debugsource | 128.5.1-1.el9_5 | |
firefox-x11 | 128.5.1-1.el9_5 | [RHSA-2024:10702](https://access.redhat.com/errata/RHSA-2024:10702) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11692](https://access.redhat.com/security/cve/CVE-2024-11692), [CVE-2024-11694](https://access.redhat.com/security/cve/CVE-2024-11694), [CVE-2024-11695](https://access.redhat.com/security/cve/CVE-2024-11695), [CVE-2024-11696](https://access.redhat.com/security/cve/CVE-2024-11696), [CVE-2024-11697](https://access.redhat.com/security/cve/CVE-2024-11697), [CVE-2024-11699](https://access.redhat.com/security/cve/CVE-2024-11699))
python-tornado-debugsource | 6.4.2-1.el9_5 | |
python3-tornado | 6.4.2-1.el9_5 | [RHSA-2024:10590](https://access.redhat.com/errata/RHSA-2024:10590) | <div class="adv_s">Security Advisory</div> ([CVE-2024-52804](https://access.redhat.com/security/cve/CVE-2024-52804))
python3-tornado-debuginfo | 6.4.2-1.el9_5 | |
rhc | 0.2.5-1.el9_5 | [RHSA-2024:10759](https://access.redhat.com/errata/RHSA-2024:10759) | <div class="adv_s">Security Advisory</div> ([CVE-2022-3064](https://access.redhat.com/security/cve/CVE-2022-3064))
rhc-debuginfo | 0.2.5-1.el9_5 | |
rhc-debugsource | 0.2.5-1.el9_5 | |
rhc-worker-playbook | 0.1.10-1.el9_5 | [RHSA-2024:10761](https://access.redhat.com/errata/RHSA-2024:10761) | <div class="adv_s">Security Advisory</div> ([CVE-2022-40898](https://access.redhat.com/security/cve/CVE-2022-40898), [CVE-2023-1428](https://access.redhat.com/security/cve/CVE-2023-1428), [CVE-2023-32731](https://access.redhat.com/security/cve/CVE-2023-32731), [CVE-2023-33953](https://access.redhat.com/security/cve/CVE-2023-33953))
rhc-worker-playbook-debuginfo | 0.1.10-1.el9_5 | |
thunderbird | 128.5.0-1.el9_5 | [RHSA-2024:10592](https://access.redhat.com/errata/RHSA-2024:10592) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11159](https://access.redhat.com/security/cve/CVE-2024-11159), [CVE-2024-11692](https://access.redhat.com/security/cve/CVE-2024-11692), [CVE-2024-11694](https://access.redhat.com/security/cve/CVE-2024-11694), [CVE-2024-11695](https://access.redhat.com/security/cve/CVE-2024-11695), [CVE-2024-11696](https://access.redhat.com/security/cve/CVE-2024-11696), [CVE-2024-11697](https://access.redhat.com/security/cve/CVE-2024-11697), [CVE-2024-11699](https://access.redhat.com/security/cve/CVE-2024-11699))
thunderbird-debuginfo | 128.5.0-1.el9_5 | |
thunderbird-debugsource | 128.5.0-1.el9_5 | |
webkit2gtk3 | 2.46.3-2.el9_5 | [RHSA-2024:10472](https://access.redhat.com/errata/RHSA-2024:10472) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-debuginfo | 2.46.3-2.el9_5 | |
webkit2gtk3-debugsource | 2.46.3-2.el9_5 | |
webkit2gtk3-devel | 2.46.3-2.el9_5 | [RHSA-2024:10472](https://access.redhat.com/errata/RHSA-2024:10472) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-devel-debuginfo | 2.46.3-2.el9_5 | |
webkit2gtk3-jsc | 2.46.3-2.el9_5 | [RHSA-2024:10472](https://access.redhat.com/errata/RHSA-2024:10472) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-jsc-debuginfo | 2.46.3-2.el9_5 | |
webkit2gtk3-jsc-devel | 2.46.3-2.el9_5 | [RHSA-2024:10472](https://access.redhat.com/errata/RHSA-2024:10472) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-jsc-devel-debuginfo | 2.46.3-2.el9_5 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rhc-debuginfo | 0.2.5-1.el9_5 | |
rhc-debugsource | 0.2.5-1.el9_5 | |
rhc-devel | 0.2.5-1.el9_5 | [RHSA-2024:10759](https://access.redhat.com/errata/RHSA-2024:10759) | <div class="adv_s">Security Advisory</div> ([CVE-2022-3064](https://access.redhat.com/security/cve/CVE-2022-3064))

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.8-1.rh9.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.8.1-1.el9_4 | [RHBA-2024:10663](https://access.redhat.com/errata/RHBA-2024:10663) | <div class="adv_b">Bug Fix Advisory</div>
sos | 4.8.1-1.el9_5 | [RHBA-2024:10663](https://access.redhat.com/errata/RHBA-2024:10663) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.8.1-1.el9_4 | [RHBA-2024:10663](https://access.redhat.com/errata/RHBA-2024:10663) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.8.1-1.el9_5 | [RHBA-2024:10663](https://access.redhat.com/errata/RHBA-2024:10663) | <div class="adv_b">Bug Fix Advisory</div>

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 128.5.1-1.el9_5 | [RHSA-2024:10702](https://access.redhat.com/errata/RHSA-2024:10702) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11692](https://access.redhat.com/security/cve/CVE-2024-11692), [CVE-2024-11694](https://access.redhat.com/security/cve/CVE-2024-11694), [CVE-2024-11695](https://access.redhat.com/security/cve/CVE-2024-11695), [CVE-2024-11696](https://access.redhat.com/security/cve/CVE-2024-11696), [CVE-2024-11697](https://access.redhat.com/security/cve/CVE-2024-11697), [CVE-2024-11699](https://access.redhat.com/security/cve/CVE-2024-11699))
firefox-debuginfo | 128.5.1-1.el9_5 | |
firefox-debugsource | 128.5.1-1.el9_5 | |
firefox-x11 | 128.5.1-1.el9_5 | [RHSA-2024:10702](https://access.redhat.com/errata/RHSA-2024:10702) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11692](https://access.redhat.com/security/cve/CVE-2024-11692), [CVE-2024-11694](https://access.redhat.com/security/cve/CVE-2024-11694), [CVE-2024-11695](https://access.redhat.com/security/cve/CVE-2024-11695), [CVE-2024-11696](https://access.redhat.com/security/cve/CVE-2024-11696), [CVE-2024-11697](https://access.redhat.com/security/cve/CVE-2024-11697), [CVE-2024-11699](https://access.redhat.com/security/cve/CVE-2024-11699))
python-tornado-debugsource | 6.4.2-1.el9_5 | |
python3-tornado | 6.4.2-1.el9_5 | [RHSA-2024:10590](https://access.redhat.com/errata/RHSA-2024:10590) | <div class="adv_s">Security Advisory</div> ([CVE-2024-52804](https://access.redhat.com/security/cve/CVE-2024-52804))
python3-tornado-debuginfo | 6.4.2-1.el9_5 | |
rhc | 0.2.5-1.el9_5 | [RHSA-2024:10759](https://access.redhat.com/errata/RHSA-2024:10759) | <div class="adv_s">Security Advisory</div> ([CVE-2022-3064](https://access.redhat.com/security/cve/CVE-2022-3064))
rhc-debuginfo | 0.2.5-1.el9_5 | |
rhc-debugsource | 0.2.5-1.el9_5 | |
rhc-worker-playbook | 0.1.10-1.el9_5 | [RHSA-2024:10761](https://access.redhat.com/errata/RHSA-2024:10761) | <div class="adv_s">Security Advisory</div> ([CVE-2022-40898](https://access.redhat.com/security/cve/CVE-2022-40898), [CVE-2023-1428](https://access.redhat.com/security/cve/CVE-2023-1428), [CVE-2023-32731](https://access.redhat.com/security/cve/CVE-2023-32731), [CVE-2023-33953](https://access.redhat.com/security/cve/CVE-2023-33953))
rhc-worker-playbook-debuginfo | 0.1.10-1.el9_5 | |
thunderbird | 128.5.0-1.el9_5 | [RHSA-2024:10592](https://access.redhat.com/errata/RHSA-2024:10592) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11159](https://access.redhat.com/security/cve/CVE-2024-11159), [CVE-2024-11692](https://access.redhat.com/security/cve/CVE-2024-11692), [CVE-2024-11694](https://access.redhat.com/security/cve/CVE-2024-11694), [CVE-2024-11695](https://access.redhat.com/security/cve/CVE-2024-11695), [CVE-2024-11696](https://access.redhat.com/security/cve/CVE-2024-11696), [CVE-2024-11697](https://access.redhat.com/security/cve/CVE-2024-11697), [CVE-2024-11699](https://access.redhat.com/security/cve/CVE-2024-11699))
thunderbird-debuginfo | 128.5.0-1.el9_5 | |
thunderbird-debugsource | 128.5.0-1.el9_5 | |
webkit2gtk3 | 2.46.3-2.el9_5 | [RHSA-2024:10472](https://access.redhat.com/errata/RHSA-2024:10472) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-debuginfo | 2.46.3-2.el9_5 | |
webkit2gtk3-debugsource | 2.46.3-2.el9_5 | |
webkit2gtk3-devel | 2.46.3-2.el9_5 | [RHSA-2024:10472](https://access.redhat.com/errata/RHSA-2024:10472) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-devel-debuginfo | 2.46.3-2.el9_5 | |
webkit2gtk3-jsc | 2.46.3-2.el9_5 | [RHSA-2024:10472](https://access.redhat.com/errata/RHSA-2024:10472) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-jsc-debuginfo | 2.46.3-2.el9_5 | |
webkit2gtk3-jsc-devel | 2.46.3-2.el9_5 | [RHSA-2024:10472](https://access.redhat.com/errata/RHSA-2024:10472) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-jsc-devel-debuginfo | 2.46.3-2.el9_5 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rhc-debuginfo | 0.2.5-1.el9_5 | |
rhc-debugsource | 0.2.5-1.el9_5 | |
rhc-devel | 0.2.5-1.el9_5 | [RHSA-2024:10759](https://access.redhat.com/errata/RHSA-2024:10759) | <div class="adv_s">Security Advisory</div> ([CVE-2022-3064](https://access.redhat.com/security/cve/CVE-2022-3064))

