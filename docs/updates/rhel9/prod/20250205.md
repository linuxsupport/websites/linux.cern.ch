## 2025-02-05

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git-lfs | 3.4.1-4.el9_5 | [RHSA-2025:0673](https://access.redhat.com/errata/RHSA-2025:0673) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53263](https://access.redhat.com/security/cve/CVE-2024-53263))
git-lfs-debuginfo | 3.4.1-4.el9_5 | |
git-lfs-debugsource | 3.4.1-4.el9_5 | |
java-1.8.0-openjdk | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-debugsource | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-demo | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-devel | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-headless | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-javadoc | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-javadoc-zip | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-src | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-17-openjdk | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-debugsource | 17.0.14.0.7-2.el9 | |
java-17-openjdk-demo | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-devel | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-devel-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-headless | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-headless-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-javadoc | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-javadoc-zip | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-jmods | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-src | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-static-libs | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-debugsource | 21.0.6.0.7-1.el9 | |
java-21-openjdk-demo | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-devel | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-devel-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-headless | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-headless-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-javadoc | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-javadoc-zip | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-jmods | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-src | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-static-libs | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
python3-jinja2 | 2.11.3-7.el9_5 | [RHSA-2025:0667](https://access.redhat.com/errata/RHSA-2025:0667) | <div class="adv_s">Security Advisory</div> ([CVE-2024-56326](https://access.redhat.com/security/cve/CVE-2024-56326))
redis | 6.2.17-1.el9_5 | [RHSA-2025:0693](https://access.redhat.com/errata/RHSA-2025:0693) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis | 7.2.7-1.module+el9.5.0+22705+3d861c1c | [RHSA-2025:0692](https://access.redhat.com/errata/RHSA-2025:0692) | <div class="adv_s">Security Advisory</div> ([CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981), [CVE-2024-51741](https://access.redhat.com/security/cve/CVE-2024-51741))
redis-debuginfo | 6.2.17-1.el9_5 | |
redis-debuginfo | 7.2.7-1.module+el9.5.0+22705+3d861c1c | |
redis-debugsource | 6.2.17-1.el9_5 | |
redis-debugsource | 7.2.7-1.module+el9.5.0+22705+3d861c1c | |
redis-devel | 6.2.17-1.el9_5 | [RHSA-2025:0693](https://access.redhat.com/errata/RHSA-2025:0693) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis-devel | 7.2.7-1.module+el9.5.0+22705+3d861c1c | [RHSA-2025:0692](https://access.redhat.com/errata/RHSA-2025:0692) | <div class="adv_s">Security Advisory</div> ([CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981), [CVE-2024-51741](https://access.redhat.com/security/cve/CVE-2024-51741))
redis-doc | 6.2.17-1.el9_5 | [RHSA-2025:0693](https://access.redhat.com/errata/RHSA-2025:0693) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis-doc | 7.2.7-1.module+el9.5.0+22705+3d861c1c | [RHSA-2025:0692](https://access.redhat.com/errata/RHSA-2025:0692) | <div class="adv_s">Security Advisory</div> ([CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981), [CVE-2024-51741](https://access.redhat.com/security/cve/CVE-2024-51741))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-1.8.0-openjdk-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-debugsource | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-demo-fastdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-demo-fastdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-demo-slowdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-devel-fastdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-devel-fastdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-devel-slowdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-fastdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-fastdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-headless-fastdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-headless-fastdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-headless-slowdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-slowdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-src-fastdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-src-slowdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-17-openjdk-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-debugsource | 17.0.14.0.7-2.el9 | |
java-17-openjdk-demo-fastdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-demo-slowdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-devel-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-devel-fastdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-devel-fastdebug-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-devel-slowdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-devel-slowdebug-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-fastdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-fastdebug-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-headless-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-headless-fastdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-headless-fastdebug-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-headless-slowdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-headless-slowdebug-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-jmods-fastdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-jmods-slowdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-slowdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-slowdebug-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-src-fastdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-src-slowdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-static-libs-fastdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-static-libs-slowdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-debugsource | 21.0.6.0.7-1.el9 | |
java-21-openjdk-demo-fastdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-demo-slowdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-devel-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-devel-fastdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-devel-fastdebug-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-devel-slowdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-devel-slowdebug-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-fastdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-fastdebug-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-headless-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-headless-fastdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-headless-fastdebug-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-headless-slowdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-headless-slowdebug-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-jmods-fastdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-jmods-slowdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-slowdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-slowdebug-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-src-fastdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-src-slowdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-static-libs-fastdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-static-libs-slowdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git-lfs | 3.4.1-4.el9_5 | [RHSA-2025:0673](https://access.redhat.com/errata/RHSA-2025:0673) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53263](https://access.redhat.com/security/cve/CVE-2024-53263))
git-lfs-debuginfo | 3.4.1-4.el9_5 | |
git-lfs-debugsource | 3.4.1-4.el9_5 | |
java-1.8.0-openjdk | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-debugsource | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-demo | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-devel | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-headless | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-javadoc | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-javadoc-zip | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-src | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-17-openjdk | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-debugsource | 17.0.14.0.7-2.el9 | |
java-17-openjdk-demo | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-devel | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-devel-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-headless | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-headless-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-javadoc | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-javadoc-zip | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-jmods | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-src | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-static-libs | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-debugsource | 21.0.6.0.7-1.el9 | |
java-21-openjdk-demo | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-devel | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-devel-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-headless | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-headless-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-javadoc | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-javadoc-zip | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-jmods | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-src | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-static-libs | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
python3-jinja2 | 2.11.3-7.el9_5 | [RHSA-2025:0667](https://access.redhat.com/errata/RHSA-2025:0667) | <div class="adv_s">Security Advisory</div> ([CVE-2024-56326](https://access.redhat.com/security/cve/CVE-2024-56326))
redis | 6.2.17-1.el9_5 | [RHSA-2025:0693](https://access.redhat.com/errata/RHSA-2025:0693) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis | 7.2.7-1.module+el9.5.0+22705+3d861c1c | [RHSA-2025:0692](https://access.redhat.com/errata/RHSA-2025:0692) | <div class="adv_s">Security Advisory</div> ([CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981), [CVE-2024-51741](https://access.redhat.com/security/cve/CVE-2024-51741))
redis-debuginfo | 6.2.17-1.el9_5 | |
redis-debuginfo | 7.2.7-1.module+el9.5.0+22705+3d861c1c | |
redis-debugsource | 6.2.17-1.el9_5 | |
redis-debugsource | 7.2.7-1.module+el9.5.0+22705+3d861c1c | |
redis-devel | 6.2.17-1.el9_5 | [RHSA-2025:0693](https://access.redhat.com/errata/RHSA-2025:0693) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis-devel | 7.2.7-1.module+el9.5.0+22705+3d861c1c | [RHSA-2025:0692](https://access.redhat.com/errata/RHSA-2025:0692) | <div class="adv_s">Security Advisory</div> ([CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981), [CVE-2024-51741](https://access.redhat.com/security/cve/CVE-2024-51741))
redis-doc | 6.2.17-1.el9_5 | [RHSA-2025:0693](https://access.redhat.com/errata/RHSA-2025:0693) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis-doc | 7.2.7-1.module+el9.5.0+22705+3d861c1c | [RHSA-2025:0692](https://access.redhat.com/errata/RHSA-2025:0692) | <div class="adv_s">Security Advisory</div> ([CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981), [CVE-2024-51741](https://access.redhat.com/security/cve/CVE-2024-51741))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-1.8.0-openjdk-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-debugsource | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-demo-fastdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-demo-fastdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-demo-slowdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-devel-fastdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-devel-fastdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-devel-slowdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-fastdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-fastdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-headless-fastdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-headless-fastdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-headless-slowdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-slowdebug-debuginfo | 1.8.0.442.b06-2.el9 | |
java-1.8.0-openjdk-src-fastdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-src-slowdebug | 1.8.0.442.b06-2.el9 | [RHBA-2025:0418](https://access.redhat.com/errata/RHBA-2025:0418) | <div class="adv_b">Bug Fix Advisory</div>
java-17-openjdk-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-debugsource | 17.0.14.0.7-2.el9 | |
java-17-openjdk-demo-fastdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-demo-slowdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-devel-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-devel-fastdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-devel-fastdebug-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-devel-slowdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-devel-slowdebug-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-fastdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-fastdebug-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-headless-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-headless-fastdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-headless-fastdebug-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-headless-slowdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-headless-slowdebug-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-jmods-fastdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-jmods-slowdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-slowdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-slowdebug-debuginfo | 17.0.14.0.7-2.el9 | |
java-17-openjdk-src-fastdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-src-slowdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-static-libs-fastdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-17-openjdk-static-libs-slowdebug | 17.0.14.0.7-2.el9 | [RHSA-2025:0422](https://access.redhat.com/errata/RHSA-2025:0422) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-debugsource | 21.0.6.0.7-1.el9 | |
java-21-openjdk-demo-fastdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-demo-slowdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-devel-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-devel-fastdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-devel-fastdebug-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-devel-slowdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-devel-slowdebug-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-fastdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-fastdebug-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-headless-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-headless-fastdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-headless-fastdebug-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-headless-slowdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-headless-slowdebug-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-jmods-fastdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-jmods-slowdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-slowdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-slowdebug-debuginfo | 21.0.6.0.7-1.el9 | |
java-21-openjdk-src-fastdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-src-slowdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-static-libs-fastdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))
java-21-openjdk-static-libs-slowdebug | 21.0.6.0.7-1.el9 | [RHSA-2025:0426](https://access.redhat.com/errata/RHSA-2025:0426) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21502](https://access.redhat.com/security/cve/CVE-2025-21502))

