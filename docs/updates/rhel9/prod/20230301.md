## 2023-03-01

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-dracut-conf | 1.1-1.rh9.cern | |
cern-dracut-conf | 1.1-2.rh9.cern | |
cern-dracut-conf | 1.2-1.rh9.cern | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.14-1.el9_1 | |
aspnetcore-runtime-7.0 | 7.0.3-1.el9_1 | |
aspnetcore-targeting-pack-6.0 | 6.0.14-1.el9_1 | |
aspnetcore-targeting-pack-7.0 | 7.0.3-1.el9_1 | |
dotnet-apphost-pack-6.0 | 6.0.14-1.el9_1 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-apphost-pack-7.0 | 7.0.3-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-host | 7.0.3-1.el9_1 | |
dotnet-host-debuginfo | 7.0.3-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.14-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-hostfxr-7.0 | 7.0.3-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.14-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-runtime-7.0 | 7.0.3-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.114-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet-sdk-7.0 | 7.0.103-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.14-1.el9_1 | |
dotnet-targeting-pack-7.0 | 7.0.3-1.el9_1 | |
dotnet-templates-6.0 | 6.0.114-1.el9_1 | |
dotnet-templates-7.0 | 7.0.103-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet6.0-debugsource | 6.0.114-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet7.0-debugsource | 7.0.103-1.el9_1 | |
firefox | 102.8.0-2.el9_1 | |
firefox-debuginfo | 102.8.0-2.el9_1 | |
firefox-debugsource | 102.8.0-2.el9_1 | |
firefox-x11 | 102.8.0-2.el9_1 | |
netstandard-targeting-pack-2.1 | 7.0.103-1.el9_1 | |
thunderbird | 102.8.0-2.el9_1 | |
thunderbird-debuginfo | 102.8.0-2.el9_1 | |
thunderbird-debugsource | 102.8.0-2.el9_1 | |
webkit2gtk3 | 2.36.7-1.el9_1.2 | |
webkit2gtk3-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-debugsource | 2.36.7-1.el9_1.2 | |
webkit2gtk3-devel | 2.36.7-1.el9_1.2 | |
webkit2gtk3-devel-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-devel | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-devel-debuginfo | 2.36.7-1.el9_1.2 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-host-debuginfo | 7.0.3-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.114-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.103-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet6.0-debugsource | 6.0.114-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet7.0-debugsource | 7.0.103-1.el9_1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-dracut-conf | 1.1-1.rh9.cern | |
cern-dracut-conf | 1.1-2.rh9.cern | |
cern-dracut-conf | 1.2-1.rh9.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.14-1.el9_1 | |
aspnetcore-runtime-7.0 | 7.0.3-1.el9_1 | |
aspnetcore-targeting-pack-6.0 | 6.0.14-1.el9_1 | |
aspnetcore-targeting-pack-7.0 | 7.0.3-1.el9_1 | |
dotnet-apphost-pack-6.0 | 6.0.14-1.el9_1 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-apphost-pack-7.0 | 7.0.3-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-host | 7.0.3-1.el9_1 | |
dotnet-host-debuginfo | 7.0.3-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.14-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-hostfxr-7.0 | 7.0.3-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.14-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-runtime-7.0 | 7.0.3-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.114-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet-sdk-7.0 | 7.0.103-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.14-1.el9_1 | |
dotnet-targeting-pack-7.0 | 7.0.3-1.el9_1 | |
dotnet-templates-6.0 | 6.0.114-1.el9_1 | |
dotnet-templates-7.0 | 7.0.103-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet6.0-debugsource | 6.0.114-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet7.0-debugsource | 7.0.103-1.el9_1 | |
firefox | 102.8.0-2.el9_1 | |
firefox-debuginfo | 102.8.0-2.el9_1 | |
firefox-debugsource | 102.8.0-2.el9_1 | |
firefox-x11 | 102.8.0-2.el9_1 | |
netstandard-targeting-pack-2.1 | 7.0.103-1.el9_1 | |
thunderbird | 102.8.0-2.el9_1 | |
thunderbird-debuginfo | 102.8.0-2.el9_1 | |
thunderbird-debugsource | 102.8.0-2.el9_1 | |
webkit2gtk3 | 2.36.7-1.el9_1.2 | |
webkit2gtk3-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-debugsource | 2.36.7-1.el9_1.2 | |
webkit2gtk3-devel | 2.36.7-1.el9_1.2 | |
webkit2gtk3-devel-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-debuginfo | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-devel | 2.36.7-1.el9_1.2 | |
webkit2gtk3-jsc-devel-debuginfo | 2.36.7-1.el9_1.2 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-host-debuginfo | 7.0.3-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.14-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.3-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.114-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.103-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.114-1.el9_1 | |
dotnet6.0-debugsource | 6.0.114-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.103-1.el9_1 | |
dotnet7.0-debugsource | 7.0.103-1.el9_1 | |

