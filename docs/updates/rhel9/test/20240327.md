## 2024-03-27

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
expat | 2.5.0-1.el9_3.1 | [RHSA-2024:1530](https://access.redhat.com/errata/RHSA-2024:1530) | <div class="adv_s">Security Advisory</div> ([CVE-2023-52425](https://access.redhat.com/security/cve/CVE-2023-52425), [CVE-2024-28757](https://access.redhat.com/security/cve/CVE-2024-28757))
expat-debuginfo | 2.5.0-1.el9_3.1 | |
expat-debugsource | 2.5.0-1.el9_3.1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
expat-debuginfo | 2.5.0-1.el9_3.1 | |
expat-debugsource | 2.5.0-1.el9_3.1 | |
expat-devel | 2.5.0-1.el9_3.1 | [RHSA-2024:1530](https://access.redhat.com/errata/RHSA-2024:1530) | <div class="adv_s">Security Advisory</div> ([CVE-2023-52425](https://access.redhat.com/security/cve/CVE-2023-52425), [CVE-2024-28757](https://access.redhat.com/security/cve/CVE-2024-28757))

