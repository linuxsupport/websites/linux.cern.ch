## 2024-02-23

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.8.0-1.el9_3 | [RHSA-2024:0952](https://access.redhat.com/errata/RHSA-2024:0952) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1546](https://access.redhat.com/security/cve/CVE-2024-1546), [CVE-2024-1547](https://access.redhat.com/security/cve/CVE-2024-1547), [CVE-2024-1548](https://access.redhat.com/security/cve/CVE-2024-1548), [CVE-2024-1549](https://access.redhat.com/security/cve/CVE-2024-1549), [CVE-2024-1550](https://access.redhat.com/security/cve/CVE-2024-1550), [CVE-2024-1551](https://access.redhat.com/security/cve/CVE-2024-1551), [CVE-2024-1552](https://access.redhat.com/security/cve/CVE-2024-1552), [CVE-2024-1553](https://access.redhat.com/security/cve/CVE-2024-1553))
firefox-debuginfo | 115.8.0-1.el9_3 | |
firefox-debugsource | 115.8.0-1.el9_3 | |
firefox-x11 | 115.8.0-1.el9_3 | [RHSA-2024:0952](https://access.redhat.com/errata/RHSA-2024:0952) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1546](https://access.redhat.com/security/cve/CVE-2024-1546), [CVE-2024-1547](https://access.redhat.com/security/cve/CVE-2024-1547), [CVE-2024-1548](https://access.redhat.com/security/cve/CVE-2024-1548), [CVE-2024-1549](https://access.redhat.com/security/cve/CVE-2024-1549), [CVE-2024-1550](https://access.redhat.com/security/cve/CVE-2024-1550), [CVE-2024-1551](https://access.redhat.com/security/cve/CVE-2024-1551), [CVE-2024-1552](https://access.redhat.com/security/cve/CVE-2024-1552), [CVE-2024-1553](https://access.redhat.com/security/cve/CVE-2024-1553))
postgresql | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-contrib | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-contrib | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-contrib-debuginfo | 13.14-1.el9_3 | |
postgresql-contrib-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-debuginfo | 13.14-1.el9_3 | |
postgresql-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-debugsource | 13.14-1.el9_3 | |
postgresql-debugsource | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-docs | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-docs-debuginfo | 13.14-1.el9_3 | |
postgresql-docs-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-plperl | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plperl | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plperl-debuginfo | 13.14-1.el9_3 | |
postgresql-plperl-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-plpython3 | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plpython3 | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plpython3-debuginfo | 13.14-1.el9_3 | |
postgresql-plpython3-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-pltcl | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-pltcl | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-pltcl-debuginfo | 13.14-1.el9_3 | |
postgresql-pltcl-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-private-devel | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-private-libs | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-private-libs | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-private-libs-debuginfo | 13.14-1.el9_3 | |
postgresql-private-libs-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-server | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server-debuginfo | 13.14-1.el9_3 | |
postgresql-server-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-server-devel | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server-devel-debuginfo | 13.14-1.el9_3 | |
postgresql-server-devel-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-static | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test-debuginfo | 13.14-1.el9_3 | |
postgresql-test-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-test-rpm-macros | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade-debuginfo | 13.14-1.el9_3 | |
postgresql-upgrade-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-upgrade-devel | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade-devel-debuginfo | 13.14-1.el9_3 | |
postgresql-upgrade-devel-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
postgresql-contrib-debuginfo | 13.14-1.el9_3 | |
postgresql-debuginfo | 13.14-1.el9_3 | |
postgresql-debugsource | 13.14-1.el9_3 | |
postgresql-docs | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-docs-debuginfo | 13.14-1.el9_3 | |
postgresql-plperl-debuginfo | 13.14-1.el9_3 | |
postgresql-plpython3-debuginfo | 13.14-1.el9_3 | |
postgresql-pltcl-debuginfo | 13.14-1.el9_3 | |
postgresql-private-devel | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-private-libs-debuginfo | 13.14-1.el9_3 | |
postgresql-server-debuginfo | 13.14-1.el9_3 | |
postgresql-server-devel | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server-devel-debuginfo | 13.14-1.el9_3 | |
postgresql-static | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test-debuginfo | 13.14-1.el9_3 | |
postgresql-upgrade-debuginfo | 13.14-1.el9_3 | |
postgresql-upgrade-devel | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade-devel-debuginfo | 13.14-1.el9_3 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.8.0-1.el9_3 | [RHSA-2024:0952](https://access.redhat.com/errata/RHSA-2024:0952) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1546](https://access.redhat.com/security/cve/CVE-2024-1546), [CVE-2024-1547](https://access.redhat.com/security/cve/CVE-2024-1547), [CVE-2024-1548](https://access.redhat.com/security/cve/CVE-2024-1548), [CVE-2024-1549](https://access.redhat.com/security/cve/CVE-2024-1549), [CVE-2024-1550](https://access.redhat.com/security/cve/CVE-2024-1550), [CVE-2024-1551](https://access.redhat.com/security/cve/CVE-2024-1551), [CVE-2024-1552](https://access.redhat.com/security/cve/CVE-2024-1552), [CVE-2024-1553](https://access.redhat.com/security/cve/CVE-2024-1553))
firefox-debuginfo | 115.8.0-1.el9_3 | |
firefox-debugsource | 115.8.0-1.el9_3 | |
firefox-x11 | 115.8.0-1.el9_3 | [RHSA-2024:0952](https://access.redhat.com/errata/RHSA-2024:0952) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1546](https://access.redhat.com/security/cve/CVE-2024-1546), [CVE-2024-1547](https://access.redhat.com/security/cve/CVE-2024-1547), [CVE-2024-1548](https://access.redhat.com/security/cve/CVE-2024-1548), [CVE-2024-1549](https://access.redhat.com/security/cve/CVE-2024-1549), [CVE-2024-1550](https://access.redhat.com/security/cve/CVE-2024-1550), [CVE-2024-1551](https://access.redhat.com/security/cve/CVE-2024-1551), [CVE-2024-1552](https://access.redhat.com/security/cve/CVE-2024-1552), [CVE-2024-1553](https://access.redhat.com/security/cve/CVE-2024-1553))
postgresql | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-contrib | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-contrib | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-contrib-debuginfo | 13.14-1.el9_3 | |
postgresql-contrib-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-debuginfo | 13.14-1.el9_3 | |
postgresql-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-debugsource | 13.14-1.el9_3 | |
postgresql-debugsource | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-docs | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-docs-debuginfo | 13.14-1.el9_3 | |
postgresql-docs-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-plperl | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plperl | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plperl-debuginfo | 13.14-1.el9_3 | |
postgresql-plperl-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-plpython3 | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plpython3 | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-plpython3-debuginfo | 13.14-1.el9_3 | |
postgresql-plpython3-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-pltcl | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-pltcl | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-pltcl-debuginfo | 13.14-1.el9_3 | |
postgresql-pltcl-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-private-devel | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-private-libs | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-private-libs | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-private-libs-debuginfo | 13.14-1.el9_3 | |
postgresql-private-libs-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-server | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server-debuginfo | 13.14-1.el9_3 | |
postgresql-server-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-server-devel | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server-devel-debuginfo | 13.14-1.el9_3 | |
postgresql-server-devel-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-static | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test-debuginfo | 13.14-1.el9_3 | |
postgresql-test-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-test-rpm-macros | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade-debuginfo | 13.14-1.el9_3 | |
postgresql-upgrade-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |
postgresql-upgrade-devel | 15.6-1.module+el9.3.0+21283+b0ea34b6 | [RHSA-2024:0950](https://access.redhat.com/errata/RHSA-2024:0950) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade-devel-debuginfo | 13.14-1.el9_3 | |
postgresql-upgrade-devel-debuginfo | 15.6-1.module+el9.3.0+21283+b0ea34b6 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
postgresql-contrib-debuginfo | 13.14-1.el9_3 | |
postgresql-debuginfo | 13.14-1.el9_3 | |
postgresql-debugsource | 13.14-1.el9_3 | |
postgresql-docs | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-docs-debuginfo | 13.14-1.el9_3 | |
postgresql-plperl-debuginfo | 13.14-1.el9_3 | |
postgresql-plpython3-debuginfo | 13.14-1.el9_3 | |
postgresql-pltcl-debuginfo | 13.14-1.el9_3 | |
postgresql-private-devel | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-private-libs-debuginfo | 13.14-1.el9_3 | |
postgresql-server-debuginfo | 13.14-1.el9_3 | |
postgresql-server-devel | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-server-devel-debuginfo | 13.14-1.el9_3 | |
postgresql-static | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-test-debuginfo | 13.14-1.el9_3 | |
postgresql-upgrade-debuginfo | 13.14-1.el9_3 | |
postgresql-upgrade-devel | 13.14-1.el9_3 | [RHSA-2024:0951](https://access.redhat.com/errata/RHSA-2024:0951) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0985](https://access.redhat.com/security/cve/CVE-2024-0985))
postgresql-upgrade-devel-debuginfo | 13.14-1.el9_3 | |

