## 2025-02-18

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-debuginfo | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-debugsource | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-devel | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-docs | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-full-i18n | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-libs | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-libs-debuginfo | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-nodemon | 3.0.1-1.module+el9.5.0+22763+17233acb | |
nodejs-packaging | 2021.06-4.module+el9.5.0+22763+17233acb | |
nodejs-packaging-bundler | 2021.06-4.module+el9.5.0+22763+17233acb | |
npm | 10.9.2-1.22.13.1.1.module+el9.5.0+22763+17233acb | |
opentelemetry-collector | 0.107.0-2.el9_5 | [RHBA-2025:1579](https://access.redhat.com/errata/RHBA-2025:1579) | <div class="adv_b">Bug Fix Advisory</div>
v8-12.4-devel | 12.4.254.21-1.22.13.1.1.module+el9.5.0+22763+17233acb | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-debuginfo | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-debugsource | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-devel | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-docs | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-full-i18n | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-libs | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-libs-debuginfo | 22.13.1-1.module+el9.5.0+22763+17233acb | |
nodejs-nodemon | 3.0.1-1.module+el9.5.0+22763+17233acb | |
nodejs-packaging | 2021.06-4.module+el9.5.0+22763+17233acb | |
nodejs-packaging-bundler | 2021.06-4.module+el9.5.0+22763+17233acb | |
npm | 10.9.2-1.22.13.1.1.module+el9.5.0+22763+17233acb | |
opentelemetry-collector | 0.107.0-2.el9_5 | [RHBA-2025:1579](https://access.redhat.com/errata/RHBA-2025:1579) | <div class="adv_b">Bug Fix Advisory</div>
v8-12.4-devel | 12.4.254.21-1.22.13.1.1.module+el9.5.0+22763+17233acb | |

