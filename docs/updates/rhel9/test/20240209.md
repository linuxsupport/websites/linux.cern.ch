## 2024-02-09

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2024a-1.el9 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2024a-1.el9 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2024a-1.el9 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2024a-1.el9 | |

