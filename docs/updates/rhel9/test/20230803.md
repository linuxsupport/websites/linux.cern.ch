## 2023-08-03

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.5.14.0_284.25.1.el9_2.rh9.cern | |

