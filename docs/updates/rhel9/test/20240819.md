## 2024-08-19

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
curl | 7.76.1-29.el9_4.1 | |
curl-debuginfo | 7.76.1-29.el9_4.1 | |
curl-debugsource | 7.76.1-29.el9_4.1 | |
curl-minimal | 7.76.1-29.el9_4.1 | |
curl-minimal-debuginfo | 7.76.1-29.el9_4.1 | |
kpatch-patch-5_14_0-427_13_1 | 1-2.el9_4 | |
kpatch-patch-5_14_0-427_13_1-debuginfo | 1-2.el9_4 | |
kpatch-patch-5_14_0-427_13_1-debugsource | 1-2.el9_4 | |
libcurl | 7.76.1-29.el9_4.1 | |
libcurl-debuginfo | 7.76.1-29.el9_4.1 | |
libcurl-minimal | 7.76.1-29.el9_4.1 | |
libcurl-minimal-debuginfo | 7.76.1-29.el9_4.1 | |
python3-setuptools | 53.0.0-12.el9_4.1 | |
python3-setuptools-wheel | 53.0.0-12.el9_4.1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
curl-debuginfo | 7.76.1-29.el9_4.1 | |
curl-debugsource | 7.76.1-29.el9_4.1 | |
curl-minimal-debuginfo | 7.76.1-29.el9_4.1 | |
libcurl-debuginfo | 7.76.1-29.el9_4.1 | |
libcurl-devel | 7.76.1-29.el9_4.1 | |
libcurl-minimal-debuginfo | 7.76.1-29.el9_4.1 | |
python3.12-setuptools | 68.2.2-3.el9_4.1 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3.12-setuptools-wheel | 68.2.2-3.el9_4.1 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
curl | 7.76.1-29.el9_4.1 | |
curl-debuginfo | 7.76.1-29.el9_4.1 | |
curl-debugsource | 7.76.1-29.el9_4.1 | |
curl-minimal | 7.76.1-29.el9_4.1 | |
curl-minimal-debuginfo | 7.76.1-29.el9_4.1 | |
libcurl | 7.76.1-29.el9_4.1 | |
libcurl-debuginfo | 7.76.1-29.el9_4.1 | |
libcurl-minimal | 7.76.1-29.el9_4.1 | |
libcurl-minimal-debuginfo | 7.76.1-29.el9_4.1 | |
python3-setuptools | 53.0.0-12.el9_4.1 | |
python3-setuptools-wheel | 53.0.0-12.el9_4.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
curl-debuginfo | 7.76.1-29.el9_4.1 | |
curl-debugsource | 7.76.1-29.el9_4.1 | |
curl-minimal-debuginfo | 7.76.1-29.el9_4.1 | |
libcurl-debuginfo | 7.76.1-29.el9_4.1 | |
libcurl-devel | 7.76.1-29.el9_4.1 | |
libcurl-minimal-debuginfo | 7.76.1-29.el9_4.1 | |
python3.12-setuptools | 68.2.2-3.el9_4.1 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3.12-setuptools-wheel | 68.2.2-3.el9_4.1 | |

