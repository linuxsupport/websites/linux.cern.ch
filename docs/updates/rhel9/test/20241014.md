## 2024-10-14

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.12-1.rh9.cern | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
OpenIPMI | 2.0.32-5.el9_4 | |
OpenIPMI-debuginfo | 2.0.32-5.el9_4 | |
OpenIPMI-debugsource | 2.0.32-5.el9_4 | |
OpenIPMI-lanserv | 2.0.32-5.el9_4 | |
OpenIPMI-lanserv-debuginfo | 2.0.32-5.el9_4 | |
OpenIPMI-libs | 2.0.32-5.el9_4 | |
OpenIPMI-libs-debuginfo | 2.0.32-5.el9_4 | |
OpenIPMI-perl-debuginfo | 2.0.32-5.el9_4 | |
podman | 4.9.4-13.el9_4 | |
podman-debuginfo | 4.9.4-13.el9_4 | |
podman-debugsource | 4.9.4-13.el9_4 | |
podman-docker | 4.9.4-13.el9_4 | |
podman-plugins | 4.9.4-13.el9_4 | |
podman-plugins-debuginfo | 4.9.4-13.el9_4 | |
podman-remote | 4.9.4-13.el9_4 | |
podman-remote-debuginfo | 4.9.4-13.el9_4 | |
podman-tests | 4.9.4-13.el9_4 | |
python3-openipmi-debuginfo | 2.0.32-5.el9_4 | |
thunderbird | 128.3.1-1.el9_4 | |
thunderbird-debuginfo | 128.3.1-1.el9_4 | |
thunderbird-debugsource | 128.3.1-1.el9_4 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
OpenIPMI-debuginfo | 2.0.32-5.el9_4 | |
OpenIPMI-debugsource | 2.0.32-5.el9_4 | |
OpenIPMI-devel | 2.0.32-5.el9_4 | |
OpenIPMI-lanserv-debuginfo | 2.0.32-5.el9_4 | |
OpenIPMI-libs-debuginfo | 2.0.32-5.el9_4 | |
OpenIPMI-perl-debuginfo | 2.0.32-5.el9_4 | |
python3-openipmi-debuginfo | 2.0.32-5.el9_4 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.12-1.rh9.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
OpenIPMI | 2.0.32-5.el9_4 | |
OpenIPMI-debuginfo | 2.0.32-5.el9_4 | |
OpenIPMI-debugsource | 2.0.32-5.el9_4 | |
OpenIPMI-lanserv | 2.0.32-5.el9_4 | |
OpenIPMI-lanserv-debuginfo | 2.0.32-5.el9_4 | |
OpenIPMI-libs | 2.0.32-5.el9_4 | |
OpenIPMI-libs-debuginfo | 2.0.32-5.el9_4 | |
OpenIPMI-perl-debuginfo | 2.0.32-5.el9_4 | |
podman | 4.9.4-13.el9_4 | |
podman-debuginfo | 4.9.4-13.el9_4 | |
podman-debugsource | 4.9.4-13.el9_4 | |
podman-docker | 4.9.4-13.el9_4 | |
podman-plugins | 4.9.4-13.el9_4 | |
podman-plugins-debuginfo | 4.9.4-13.el9_4 | |
podman-remote | 4.9.4-13.el9_4 | |
podman-remote-debuginfo | 4.9.4-13.el9_4 | |
podman-tests | 4.9.4-13.el9_4 | |
python3-openipmi-debuginfo | 2.0.32-5.el9_4 | |
thunderbird | 128.3.1-1.el9_4 | |
thunderbird-debuginfo | 128.3.1-1.el9_4 | |
thunderbird-debugsource | 128.3.1-1.el9_4 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
OpenIPMI-debuginfo | 2.0.32-5.el9_4 | |
OpenIPMI-debugsource | 2.0.32-5.el9_4 | |
OpenIPMI-devel | 2.0.32-5.el9_4 | |
OpenIPMI-lanserv-debuginfo | 2.0.32-5.el9_4 | |
OpenIPMI-libs-debuginfo | 2.0.32-5.el9_4 | |
OpenIPMI-perl-debuginfo | 2.0.32-5.el9_4 | |
python3-openipmi-debuginfo | 2.0.32-5.el9_4 | |

