## 2025-01-17

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
NetworkManager | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-adsl | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-adsl-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-bluetooth | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-bluetooth-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-cloud-setup-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-config-server | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-debugsource | 1.48.10-5.el9_5 | |
NetworkManager-initscripts-updown | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-libnm | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-libnm-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-ovs-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-ppp-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-team | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-team-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-tui | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-tui-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-wifi | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-wifi-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-wwan | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-wwan-debuginfo | 1.48.10-5.el9_5 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
NetworkManager-adsl-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-bluetooth-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-cloud-setup | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-cloud-setup-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-config-connectivity-redhat | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-debugsource | 1.48.10-5.el9_5 | |
NetworkManager-dispatcher-routing-rules | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-libnm-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-ovs | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-ovs-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-ppp | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-ppp-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-team-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-tui-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-wifi-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-wwan-debuginfo | 1.48.10-5.el9_5 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
NetworkManager-adsl-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-bluetooth-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-cloud-setup-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-debugsource | 1.48.10-5.el9_5 | |
NetworkManager-libnm-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-libnm-devel | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-ovs-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-ppp-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-team-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-tui-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-wifi-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-wwan-debuginfo | 1.48.10-5.el9_5 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
NetworkManager | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-adsl | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-adsl-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-bluetooth | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-bluetooth-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-cloud-setup-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-config-server | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-debugsource | 1.48.10-5.el9_5 | |
NetworkManager-initscripts-updown | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-libnm | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-libnm-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-ovs-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-ppp-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-team | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-team-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-tui | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-tui-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-wifi | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-wifi-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-wwan | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-wwan-debuginfo | 1.48.10-5.el9_5 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
NetworkManager-adsl-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-bluetooth-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-cloud-setup | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-cloud-setup-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-config-connectivity-redhat | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-debugsource | 1.48.10-5.el9_5 | |
NetworkManager-dispatcher-routing-rules | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-libnm-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-ovs | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-ovs-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-ppp | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-ppp-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-team-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-tui-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-wifi-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-wwan-debuginfo | 1.48.10-5.el9_5 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
NetworkManager-adsl-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-bluetooth-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-cloud-setup-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-debugsource | 1.48.10-5.el9_5 | |
NetworkManager-libnm-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-libnm-devel | 1.48.10-5.el9_5 | [RHSA-2025:0377](https://access.redhat.com/errata/RHSA-2025:0377) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3661](https://access.redhat.com/security/cve/CVE-2024-3661))
NetworkManager-ovs-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-ppp-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-team-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-tui-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-wifi-debuginfo | 1.48.10-5.el9_5 | |
NetworkManager-wwan-debuginfo | 1.48.10-5.el9_5 | |

