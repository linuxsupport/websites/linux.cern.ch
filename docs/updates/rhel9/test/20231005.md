## 2023-10-05

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.3.1-1.el9_2 | [RHSA-2023:5434](https://access.redhat.com/errata/RHSA-2023:5434) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3600](https://access.redhat.com/security/cve/CVE-2023-3600), [CVE-2023-5169](https://access.redhat.com/security/cve/CVE-2023-5169), [CVE-2023-5171](https://access.redhat.com/security/cve/CVE-2023-5171), [CVE-2023-5176](https://access.redhat.com/security/cve/CVE-2023-5176), [CVE-2023-5217](https://access.redhat.com/security/cve/CVE-2023-5217))
firefox-debuginfo | 115.3.1-1.el9_2 | |
firefox-debugsource | 115.3.1-1.el9_2 | |
firefox-x11 | 115.3.1-1.el9_2 | [RHSA-2023:5434](https://access.redhat.com/errata/RHSA-2023:5434) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3600](https://access.redhat.com/security/cve/CVE-2023-3600), [CVE-2023-5169](https://access.redhat.com/security/cve/CVE-2023-5169), [CVE-2023-5171](https://access.redhat.com/security/cve/CVE-2023-5171), [CVE-2023-5176](https://access.redhat.com/security/cve/CVE-2023-5176), [CVE-2023-5217](https://access.redhat.com/security/cve/CVE-2023-5217))
thunderbird | 115.3.1-1.el9_2 | [RHSA-2023:5435](https://access.redhat.com/errata/RHSA-2023:5435) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3600](https://access.redhat.com/security/cve/CVE-2023-3600), [CVE-2023-5169](https://access.redhat.com/security/cve/CVE-2023-5169), [CVE-2023-5171](https://access.redhat.com/security/cve/CVE-2023-5171), [CVE-2023-5176](https://access.redhat.com/security/cve/CVE-2023-5176), [CVE-2023-5217](https://access.redhat.com/security/cve/CVE-2023-5217))
thunderbird-debuginfo | 115.3.1-1.el9_2 | |
thunderbird-debugsource | 115.3.1-1.el9_2 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.3.1-1.el9_2 | [RHSA-2023:5434](https://access.redhat.com/errata/RHSA-2023:5434) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3600](https://access.redhat.com/security/cve/CVE-2023-3600), [CVE-2023-5169](https://access.redhat.com/security/cve/CVE-2023-5169), [CVE-2023-5171](https://access.redhat.com/security/cve/CVE-2023-5171), [CVE-2023-5176](https://access.redhat.com/security/cve/CVE-2023-5176), [CVE-2023-5217](https://access.redhat.com/security/cve/CVE-2023-5217))
firefox-debuginfo | 115.3.1-1.el9_2 | |
firefox-debugsource | 115.3.1-1.el9_2 | |
firefox-x11 | 115.3.1-1.el9_2 | [RHSA-2023:5434](https://access.redhat.com/errata/RHSA-2023:5434) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3600](https://access.redhat.com/security/cve/CVE-2023-3600), [CVE-2023-5169](https://access.redhat.com/security/cve/CVE-2023-5169), [CVE-2023-5171](https://access.redhat.com/security/cve/CVE-2023-5171), [CVE-2023-5176](https://access.redhat.com/security/cve/CVE-2023-5176), [CVE-2023-5217](https://access.redhat.com/security/cve/CVE-2023-5217))
thunderbird | 115.3.1-1.el9_2 | [RHSA-2023:5435](https://access.redhat.com/errata/RHSA-2023:5435) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3600](https://access.redhat.com/security/cve/CVE-2023-3600), [CVE-2023-5169](https://access.redhat.com/security/cve/CVE-2023-5169), [CVE-2023-5171](https://access.redhat.com/security/cve/CVE-2023-5171), [CVE-2023-5176](https://access.redhat.com/security/cve/CVE-2023-5176), [CVE-2023-5217](https://access.redhat.com/security/cve/CVE-2023-5217))
thunderbird-debuginfo | 115.3.1-1.el9_2 | |
thunderbird-debugsource | 115.3.1-1.el9_2 | |

