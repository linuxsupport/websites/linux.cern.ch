## 2023-04-12

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.5.14.0_162.23.1.el9_1.rh9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_162.23.1.el9_1-2.rh9.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-162.23.1.el9_1 | |
bpftool-debuginfo | 5.14.0-162.23.1.el9_1 | |
curl | 7.76.1-19.el9_1.2 | |
curl-debuginfo | 7.76.1-19.el9_1.2 | |
curl-debugsource | 7.76.1-19.el9_1.2 | |
curl-minimal | 7.76.1-19.el9_1.2 | |
curl-minimal-debuginfo | 7.76.1-19.el9_1.2 | |
grub2-common | 2.06-46.el9_1.5 | |
grub2-debuginfo | 2.06-46.el9_1.5 | |
grub2-debugsource | 2.06-46.el9_1.5 | |
grub2-efi-aa64-modules | 2.06-46.el9_1.5 | |
grub2-efi-x64 | 2.06-46.el9_1.5 | |
grub2-efi-x64-cdboot | 2.06-46.el9_1.5 | |
grub2-efi-x64-modules | 2.06-46.el9_1.5 | |
grub2-emu-debuginfo | 2.06-46.el9_1.5 | |
grub2-pc | 2.06-46.el9_1.5 | |
grub2-pc-modules | 2.06-46.el9_1.5 | |
grub2-tools | 2.06-46.el9_1.5 | |
grub2-tools-debuginfo | 2.06-46.el9_1.5 | |
grub2-tools-efi | 2.06-46.el9_1.5 | |
grub2-tools-efi-debuginfo | 2.06-46.el9_1.5 | |
grub2-tools-extra | 2.06-46.el9_1.5 | |
grub2-tools-extra-debuginfo | 2.06-46.el9_1.5 | |
grub2-tools-minimal | 2.06-46.el9_1.5 | |
grub2-tools-minimal-debuginfo | 2.06-46.el9_1.5 | |
kernel | 5.14.0-162.23.1.el9_1 | |
kernel-abi-stablelists | 5.14.0-162.23.1.el9_1 | |
kernel-core | 5.14.0-162.23.1.el9_1 | |
kernel-debug | 5.14.0-162.23.1.el9_1 | |
kernel-debug-core | 5.14.0-162.23.1.el9_1 | |
kernel-debug-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-debug-modules | 5.14.0-162.23.1.el9_1 | |
kernel-debug-modules-extra | 5.14.0-162.23.1.el9_1 | |
kernel-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-debuginfo-common-x86_64 | 5.14.0-162.23.1.el9_1 | |
kernel-modules | 5.14.0-162.23.1.el9_1 | |
kernel-modules-extra | 5.14.0-162.23.1.el9_1 | |
kernel-tools | 5.14.0-162.23.1.el9_1 | |
kernel-tools-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-tools-libs | 5.14.0-162.23.1.el9_1 | |
kpatch-patch-5_14_0-162_23_1 | 0-0.el9_1 | |
libcurl | 7.76.1-19.el9_1.2 | |
libcurl-debuginfo | 7.76.1-19.el9_1.2 | |
libcurl-minimal | 7.76.1-19.el9_1.2 | |
libcurl-minimal-debuginfo | 7.76.1-19.el9_1.2 | |
libgcrypt | 1.10.0-10.el9_1 | |
libgcrypt-debuginfo | 1.10.0-10.el9_1 | |
libgcrypt-debugsource | 1.10.0-10.el9_1 | |
libgcrypt-devel-debuginfo | 1.10.0-10.el9_1 | |
NetworkManager | 1.40.0-2.el9_1 | |
NetworkManager-adsl | 1.40.0-2.el9_1 | |
NetworkManager-adsl-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-bluetooth | 1.40.0-2.el9_1 | |
NetworkManager-bluetooth-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-cloud-setup-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-config-server | 1.40.0-2.el9_1 | |
NetworkManager-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-debugsource | 1.40.0-2.el9_1 | |
NetworkManager-initscripts-updown | 1.40.0-2.el9_1 | |
NetworkManager-libnm | 1.40.0-2.el9_1 | |
NetworkManager-libnm-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-ovs-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-ppp-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-team | 1.40.0-2.el9_1 | |
NetworkManager-team-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-tui | 1.40.0-2.el9_1 | |
NetworkManager-tui-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-wifi | 1.40.0-2.el9_1 | |
NetworkManager-wifi-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-wwan | 1.40.0-2.el9_1 | |
NetworkManager-wwan-debuginfo | 1.40.0-2.el9_1 | |
nftables | 1.0.4-10.el9_1 | |
nftables-debuginfo | 1.0.4-10.el9_1 | |
nftables-debugsource | 1.0.4-10.el9_1 | |
perf-debuginfo | 5.14.0-162.23.1.el9_1 | |
python3-nftables | 1.0.4-10.el9_1 | |
python3-perf | 5.14.0-162.23.1.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.23.1.el9_1 | |
sos | 4.5.1-3.el9_1 | |
sos-audit | 4.5.1-3.el9_1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 5.14.0-162.23.1.el9_1 | |
curl-debuginfo | 7.76.1-19.el9_1.2 | |
curl-debugsource | 7.76.1-19.el9_1.2 | |
curl-minimal-debuginfo | 7.76.1-19.el9_1.2 | |
gnome-shell | 40.10-5.el9_1 | |
gnome-shell-debuginfo | 40.10-5.el9_1 | |
gnome-shell-debugsource | 40.10-5.el9_1 | |
go-toolset | 1.18.10-1.el9_1 | |
golang | 1.18.10-1.el9_1 | |
golang-bin | 1.18.10-1.el9_1 | |
golang-docs | 1.18.10-1.el9_1 | |
golang-misc | 1.18.10-1.el9_1 | |
golang-race | 1.18.10-1.el9_1 | |
golang-src | 1.18.10-1.el9_1 | |
golang-tests | 1.18.10-1.el9_1 | |
haproxy | 2.4.17-3.el9_1.2 | |
haproxy-debuginfo | 2.4.17-3.el9_1.2 | |
haproxy-debugsource | 2.4.17-3.el9_1.2 | |
kernel-debug-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-debug-devel | 5.14.0-162.23.1.el9_1 | |
kernel-debug-devel-matched | 5.14.0-162.23.1.el9_1 | |
kernel-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-debuginfo-common-x86_64 | 5.14.0-162.23.1.el9_1 | |
kernel-devel | 5.14.0-162.23.1.el9_1 | |
kernel-devel-matched | 5.14.0-162.23.1.el9_1 | |
kernel-doc | 5.14.0-162.23.1.el9_1 | |
kernel-headers | 5.14.0-162.23.1.el9_1 | |
kernel-tools-debuginfo | 5.14.0-162.23.1.el9_1 | |
libcurl-debuginfo | 7.76.1-19.el9_1.2 | |
libcurl-devel | 7.76.1-19.el9_1.2 | |
libcurl-minimal-debuginfo | 7.76.1-19.el9_1.2 | |
libgcrypt-debuginfo | 1.10.0-10.el9_1 | |
libgcrypt-debugsource | 1.10.0-10.el9_1 | |
libgcrypt-devel | 1.10.0-10.el9_1 | |
libgcrypt-devel-debuginfo | 1.10.0-10.el9_1 | |
NetworkManager-adsl-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-bluetooth-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-cloud-setup | 1.40.0-2.el9_1 | |
NetworkManager-cloud-setup-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-config-connectivity-redhat | 1.40.0-2.el9_1 | |
NetworkManager-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-debugsource | 1.40.0-2.el9_1 | |
NetworkManager-dispatcher-routing-rules | 1.40.0-2.el9_1 | |
NetworkManager-libnm-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-ovs | 1.40.0-2.el9_1 | |
NetworkManager-ovs-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-ppp | 1.40.0-2.el9_1 | |
NetworkManager-ppp-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-team-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-tui-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-wifi-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-wwan-debuginfo | 1.40.0-2.el9_1 | |
perf | 5.14.0-162.23.1.el9_1 | |
perf-debuginfo | 5.14.0-162.23.1.el9_1 | |
postgresql | 13.10-1.el9_1 | |
postgresql-contrib | 13.10-1.el9_1 | |
postgresql-contrib-debuginfo | 13.10-1.el9_1 | |
postgresql-debuginfo | 13.10-1.el9_1 | |
postgresql-debugsource | 13.10-1.el9_1 | |
postgresql-docs-debuginfo | 13.10-1.el9_1 | |
postgresql-plperl | 13.10-1.el9_1 | |
postgresql-plperl-debuginfo | 13.10-1.el9_1 | |
postgresql-plpython3 | 13.10-1.el9_1 | |
postgresql-plpython3-debuginfo | 13.10-1.el9_1 | |
postgresql-pltcl | 13.10-1.el9_1 | |
postgresql-pltcl-debuginfo | 13.10-1.el9_1 | |
postgresql-private-libs | 13.10-1.el9_1 | |
postgresql-private-libs-debuginfo | 13.10-1.el9_1 | |
postgresql-server | 13.10-1.el9_1 | |
postgresql-server-debuginfo | 13.10-1.el9_1 | |
postgresql-server-devel-debuginfo | 13.10-1.el9_1 | |
postgresql-test-debuginfo | 13.10-1.el9_1 | |
postgresql-upgrade | 13.10-1.el9_1 | |
postgresql-upgrade-debuginfo | 13.10-1.el9_1 | |
postgresql-upgrade-devel-debuginfo | 13.10-1.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.23.1.el9_1 | |
rpm-ostree | 2022.12-3.el9_1 | |
rpm-ostree-debuginfo | 2022.12-3.el9_1 | |
rpm-ostree-debugsource | 2022.12-3.el9_1 | |
rpm-ostree-libs | 2022.12-3.el9_1 | |
rpm-ostree-libs-debuginfo | 2022.12-3.el9_1 | |

### highavailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
corosync-qdevice | 3.0.1-4.el9_1.1 | |
corosync-qdevice-debuginfo | 3.0.1-4.el9_1.1 | |
corosync-qdevice-debugsource | 3.0.1-4.el9_1.1 | |
corosync-qnetd | 3.0.1-4.el9_1.1 | |
corosync-qnetd-debuginfo | 3.0.1-4.el9_1.1 | |

### rt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-162.23.1.rt21.186.el9_1 | |
kernel-rt-core | 5.14.0-162.23.1.rt21.186.el9_1 | |
kernel-rt-debug | 5.14.0-162.23.1.rt21.186.el9_1 | |
kernel-rt-debug-core | 5.14.0-162.23.1.rt21.186.el9_1 | |
kernel-rt-debug-debuginfo | 5.14.0-162.23.1.rt21.186.el9_1 | |
kernel-rt-debug-devel | 5.14.0-162.23.1.rt21.186.el9_1 | |
kernel-rt-debug-modules | 5.14.0-162.23.1.rt21.186.el9_1 | |
kernel-rt-debug-modules-extra | 5.14.0-162.23.1.rt21.186.el9_1 | |
kernel-rt-debuginfo | 5.14.0-162.23.1.rt21.186.el9_1 | |
kernel-rt-debuginfo-common-x86_64 | 5.14.0-162.23.1.rt21.186.el9_1 | |
kernel-rt-devel | 5.14.0-162.23.1.rt21.186.el9_1 | |
kernel-rt-modules | 5.14.0-162.23.1.rt21.186.el9_1 | |
kernel-rt-modules-extra | 5.14.0-162.23.1.rt21.186.el9_1 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-cross-headers | 5.14.0-162.23.1.el9_1 | |
kernel-debug-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-debuginfo-common-x86_64 | 5.14.0-162.23.1.el9_1 | |
kernel-tools-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-tools-libs-devel | 5.14.0-162.23.1.el9_1 | |
NetworkManager-adsl-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-bluetooth-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-cloud-setup-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-debugsource | 1.40.0-2.el9_1 | |
NetworkManager-libnm-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-libnm-devel | 1.40.0-2.el9_1 | |
NetworkManager-ovs-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-ppp-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-team-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-tui-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-wifi-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-wwan-debuginfo | 1.40.0-2.el9_1 | |
nftables-debuginfo | 1.0.4-10.el9_1 | |
nftables-debugsource | 1.0.4-10.el9_1 | |
nftables-devel | 1.0.4-10.el9_1 | |
perf-debuginfo | 5.14.0-162.23.1.el9_1 | |
postgresql-contrib-debuginfo | 13.10-1.el9_1 | |
postgresql-debuginfo | 13.10-1.el9_1 | |
postgresql-debugsource | 13.10-1.el9_1 | |
postgresql-docs | 13.10-1.el9_1 | |
postgresql-docs-debuginfo | 13.10-1.el9_1 | |
postgresql-plperl-debuginfo | 13.10-1.el9_1 | |
postgresql-plpython3-debuginfo | 13.10-1.el9_1 | |
postgresql-pltcl-debuginfo | 13.10-1.el9_1 | |
postgresql-private-devel | 13.10-1.el9_1 | |
postgresql-private-libs-debuginfo | 13.10-1.el9_1 | |
postgresql-server-debuginfo | 13.10-1.el9_1 | |
postgresql-server-devel | 13.10-1.el9_1 | |
postgresql-server-devel-debuginfo | 13.10-1.el9_1 | |
postgresql-static | 13.10-1.el9_1 | |
postgresql-test | 13.10-1.el9_1 | |
postgresql-test-debuginfo | 13.10-1.el9_1 | |
postgresql-upgrade-debuginfo | 13.10-1.el9_1 | |
postgresql-upgrade-devel | 13.10-1.el9_1 | |
postgresql-upgrade-devel-debuginfo | 13.10-1.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.23.1.el9_1 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.5.14.0_162.23.1.el9_1.rh9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_162.23.1.el9_1-2.rh9.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-162.23.1.el9_1 | |
bpftool-debuginfo | 5.14.0-162.23.1.el9_1 | |
curl | 7.76.1-19.el9_1.2 | |
curl-debuginfo | 7.76.1-19.el9_1.2 | |
curl-debugsource | 7.76.1-19.el9_1.2 | |
curl-minimal | 7.76.1-19.el9_1.2 | |
curl-minimal-debuginfo | 7.76.1-19.el9_1.2 | |
grub2-common | 2.06-46.el9_1.5 | |
grub2-debuginfo | 2.06-46.el9_1.5 | |
grub2-debugsource | 2.06-46.el9_1.5 | |
grub2-efi-aa64 | 2.06-46.el9_1.5 | |
grub2-efi-aa64-cdboot | 2.06-46.el9_1.5 | |
grub2-efi-aa64-modules | 2.06-46.el9_1.5 | |
grub2-efi-x64-modules | 2.06-46.el9_1.5 | |
grub2-emu-debuginfo | 2.06-46.el9_1.5 | |
grub2-tools | 2.06-46.el9_1.5 | |
grub2-tools-debuginfo | 2.06-46.el9_1.5 | |
grub2-tools-extra | 2.06-46.el9_1.5 | |
grub2-tools-extra-debuginfo | 2.06-46.el9_1.5 | |
grub2-tools-minimal | 2.06-46.el9_1.5 | |
grub2-tools-minimal-debuginfo | 2.06-46.el9_1.5 | |
kernel | 5.14.0-162.23.1.el9_1 | |
kernel-abi-stablelists | 5.14.0-162.23.1.el9_1 | |
kernel-core | 5.14.0-162.23.1.el9_1 | |
kernel-debug | 5.14.0-162.23.1.el9_1 | |
kernel-debug-core | 5.14.0-162.23.1.el9_1 | |
kernel-debug-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-debug-modules | 5.14.0-162.23.1.el9_1 | |
kernel-debug-modules-extra | 5.14.0-162.23.1.el9_1 | |
kernel-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-debuginfo-common-aarch64 | 5.14.0-162.23.1.el9_1 | |
kernel-modules | 5.14.0-162.23.1.el9_1 | |
kernel-modules-extra | 5.14.0-162.23.1.el9_1 | |
kernel-tools | 5.14.0-162.23.1.el9_1 | |
kernel-tools-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-tools-libs | 5.14.0-162.23.1.el9_1 | |
libcurl | 7.76.1-19.el9_1.2 | |
libcurl-debuginfo | 7.76.1-19.el9_1.2 | |
libcurl-minimal | 7.76.1-19.el9_1.2 | |
libcurl-minimal-debuginfo | 7.76.1-19.el9_1.2 | |
libgcrypt | 1.10.0-10.el9_1 | |
libgcrypt-debuginfo | 1.10.0-10.el9_1 | |
libgcrypt-debugsource | 1.10.0-10.el9_1 | |
libgcrypt-devel-debuginfo | 1.10.0-10.el9_1 | |
NetworkManager | 1.40.0-2.el9_1 | |
NetworkManager-adsl | 1.40.0-2.el9_1 | |
NetworkManager-adsl-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-bluetooth | 1.40.0-2.el9_1 | |
NetworkManager-bluetooth-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-cloud-setup-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-config-server | 1.40.0-2.el9_1 | |
NetworkManager-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-debugsource | 1.40.0-2.el9_1 | |
NetworkManager-initscripts-updown | 1.40.0-2.el9_1 | |
NetworkManager-libnm | 1.40.0-2.el9_1 | |
NetworkManager-libnm-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-ovs-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-ppp-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-team | 1.40.0-2.el9_1 | |
NetworkManager-team-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-tui | 1.40.0-2.el9_1 | |
NetworkManager-tui-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-wifi | 1.40.0-2.el9_1 | |
NetworkManager-wifi-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-wwan | 1.40.0-2.el9_1 | |
NetworkManager-wwan-debuginfo | 1.40.0-2.el9_1 | |
nftables | 1.0.4-10.el9_1 | |
nftables-debuginfo | 1.0.4-10.el9_1 | |
nftables-debugsource | 1.0.4-10.el9_1 | |
perf-debuginfo | 5.14.0-162.23.1.el9_1 | |
python3-nftables | 1.0.4-10.el9_1 | |
python3-perf | 5.14.0-162.23.1.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.23.1.el9_1 | |
sos | 4.5.1-3.el9_1 | |
sos-audit | 4.5.1-3.el9_1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 5.14.0-162.23.1.el9_1 | |
curl-debuginfo | 7.76.1-19.el9_1.2 | |
curl-debugsource | 7.76.1-19.el9_1.2 | |
curl-minimal-debuginfo | 7.76.1-19.el9_1.2 | |
gnome-shell | 40.10-5.el9_1 | |
gnome-shell-debuginfo | 40.10-5.el9_1 | |
gnome-shell-debugsource | 40.10-5.el9_1 | |
go-toolset | 1.18.10-1.el9_1 | |
golang | 1.18.10-1.el9_1 | |
golang-bin | 1.18.10-1.el9_1 | |
golang-docs | 1.18.10-1.el9_1 | |
golang-misc | 1.18.10-1.el9_1 | |
golang-src | 1.18.10-1.el9_1 | |
golang-tests | 1.18.10-1.el9_1 | |
haproxy | 2.4.17-3.el9_1.2 | |
haproxy-debuginfo | 2.4.17-3.el9_1.2 | |
haproxy-debugsource | 2.4.17-3.el9_1.2 | |
kernel-debug-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-debug-devel | 5.14.0-162.23.1.el9_1 | |
kernel-debug-devel-matched | 5.14.0-162.23.1.el9_1 | |
kernel-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-debuginfo-common-aarch64 | 5.14.0-162.23.1.el9_1 | |
kernel-devel | 5.14.0-162.23.1.el9_1 | |
kernel-devel-matched | 5.14.0-162.23.1.el9_1 | |
kernel-doc | 5.14.0-162.23.1.el9_1 | |
kernel-headers | 5.14.0-162.23.1.el9_1 | |
kernel-tools-debuginfo | 5.14.0-162.23.1.el9_1 | |
libcurl-debuginfo | 7.76.1-19.el9_1.2 | |
libcurl-devel | 7.76.1-19.el9_1.2 | |
libcurl-minimal-debuginfo | 7.76.1-19.el9_1.2 | |
libgcrypt-debuginfo | 1.10.0-10.el9_1 | |
libgcrypt-debugsource | 1.10.0-10.el9_1 | |
libgcrypt-devel | 1.10.0-10.el9_1 | |
libgcrypt-devel-debuginfo | 1.10.0-10.el9_1 | |
NetworkManager-adsl-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-bluetooth-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-cloud-setup | 1.40.0-2.el9_1 | |
NetworkManager-cloud-setup-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-config-connectivity-redhat | 1.40.0-2.el9_1 | |
NetworkManager-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-debugsource | 1.40.0-2.el9_1 | |
NetworkManager-dispatcher-routing-rules | 1.40.0-2.el9_1 | |
NetworkManager-libnm-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-ovs | 1.40.0-2.el9_1 | |
NetworkManager-ovs-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-ppp | 1.40.0-2.el9_1 | |
NetworkManager-ppp-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-team-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-tui-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-wifi-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-wwan-debuginfo | 1.40.0-2.el9_1 | |
perf | 5.14.0-162.23.1.el9_1 | |
perf-debuginfo | 5.14.0-162.23.1.el9_1 | |
postgresql | 13.10-1.el9_1 | |
postgresql-contrib | 13.10-1.el9_1 | |
postgresql-contrib-debuginfo | 13.10-1.el9_1 | |
postgresql-debuginfo | 13.10-1.el9_1 | |
postgresql-debugsource | 13.10-1.el9_1 | |
postgresql-docs-debuginfo | 13.10-1.el9_1 | |
postgresql-plperl | 13.10-1.el9_1 | |
postgresql-plperl-debuginfo | 13.10-1.el9_1 | |
postgresql-plpython3 | 13.10-1.el9_1 | |
postgresql-plpython3-debuginfo | 13.10-1.el9_1 | |
postgresql-pltcl | 13.10-1.el9_1 | |
postgresql-pltcl-debuginfo | 13.10-1.el9_1 | |
postgresql-private-libs | 13.10-1.el9_1 | |
postgresql-private-libs-debuginfo | 13.10-1.el9_1 | |
postgresql-server | 13.10-1.el9_1 | |
postgresql-server-debuginfo | 13.10-1.el9_1 | |
postgresql-server-devel-debuginfo | 13.10-1.el9_1 | |
postgresql-test-debuginfo | 13.10-1.el9_1 | |
postgresql-upgrade | 13.10-1.el9_1 | |
postgresql-upgrade-debuginfo | 13.10-1.el9_1 | |
postgresql-upgrade-devel-debuginfo | 13.10-1.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.23.1.el9_1 | |
rpm-ostree | 2022.12-3.el9_1 | |
rpm-ostree-debuginfo | 2022.12-3.el9_1 | |
rpm-ostree-debugsource | 2022.12-3.el9_1 | |
rpm-ostree-libs | 2022.12-3.el9_1 | |
rpm-ostree-libs-debuginfo | 2022.12-3.el9_1 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-cross-headers | 5.14.0-162.23.1.el9_1 | |
kernel-debug-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-debuginfo-common-aarch64 | 5.14.0-162.23.1.el9_1 | |
kernel-tools-debuginfo | 5.14.0-162.23.1.el9_1 | |
kernel-tools-libs-devel | 5.14.0-162.23.1.el9_1 | |
NetworkManager-adsl-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-bluetooth-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-cloud-setup-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-debugsource | 1.40.0-2.el9_1 | |
NetworkManager-libnm-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-libnm-devel | 1.40.0-2.el9_1 | |
NetworkManager-ovs-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-ppp-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-team-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-tui-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-wifi-debuginfo | 1.40.0-2.el9_1 | |
NetworkManager-wwan-debuginfo | 1.40.0-2.el9_1 | |
nftables-debuginfo | 1.0.4-10.el9_1 | |
nftables-debugsource | 1.0.4-10.el9_1 | |
nftables-devel | 1.0.4-10.el9_1 | |
perf-debuginfo | 5.14.0-162.23.1.el9_1 | |
postgresql-contrib-debuginfo | 13.10-1.el9_1 | |
postgresql-debuginfo | 13.10-1.el9_1 | |
postgresql-debugsource | 13.10-1.el9_1 | |
postgresql-docs | 13.10-1.el9_1 | |
postgresql-docs-debuginfo | 13.10-1.el9_1 | |
postgresql-plperl-debuginfo | 13.10-1.el9_1 | |
postgresql-plpython3-debuginfo | 13.10-1.el9_1 | |
postgresql-pltcl-debuginfo | 13.10-1.el9_1 | |
postgresql-private-devel | 13.10-1.el9_1 | |
postgresql-private-libs-debuginfo | 13.10-1.el9_1 | |
postgresql-server-debuginfo | 13.10-1.el9_1 | |
postgresql-server-devel | 13.10-1.el9_1 | |
postgresql-server-devel-debuginfo | 13.10-1.el9_1 | |
postgresql-static | 13.10-1.el9_1 | |
postgresql-test | 13.10-1.el9_1 | |
postgresql-test-debuginfo | 13.10-1.el9_1 | |
postgresql-upgrade-debuginfo | 13.10-1.el9_1 | |
postgresql-upgrade-devel | 13.10-1.el9_1 | |
postgresql-upgrade-devel-debuginfo | 13.10-1.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.23.1.el9_1 | |

