## 2024-11-11

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
podman | 4.9.4-16.el9_4 | |
podman-debuginfo | 4.9.4-16.el9_4 | |
podman-debugsource | 4.9.4-16.el9_4 | |
podman-docker | 4.9.4-16.el9_4 | |
podman-plugins | 4.9.4-16.el9_4 | |
podman-plugins-debuginfo | 4.9.4-16.el9_4 | |
podman-remote | 4.9.4-16.el9_4 | |
podman-remote-debuginfo | 4.9.4-16.el9_4 | |
podman-tests | 4.9.4-16.el9_4 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
podman | 4.9.4-16.el9_4 | |
podman-debuginfo | 4.9.4-16.el9_4 | |
podman-debugsource | 4.9.4-16.el9_4 | |
podman-docker | 4.9.4-16.el9_4 | |
podman-plugins | 4.9.4-16.el9_4 | |
podman-plugins-debuginfo | 4.9.4-16.el9_4 | |
podman-remote | 4.9.4-16.el9_4 | |
podman-remote-debuginfo | 4.9.4-16.el9_4 | |
podman-tests | 4.9.4-16.el9_4 | |

