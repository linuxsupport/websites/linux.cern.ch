## 2023-03-30

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-162.22.2.el9_1 | |
bpftool-debuginfo | 5.14.0-162.22.2.el9_1 | |
gnutls | 3.7.6-18.el9_1 | |
gnutls-c++-debuginfo | 3.7.6-18.el9_1 | |
gnutls-dane-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debugsource | 3.7.6-18.el9_1 | |
gnutls-utils-debuginfo | 3.7.6-18.el9_1 | |
kernel | 5.14.0-162.22.2.el9_1 | |
kernel-abi-stablelists | 5.14.0-162.22.2.el9_1 | |
kernel-core | 5.14.0-162.22.2.el9_1 | |
kernel-debug | 5.14.0-162.22.2.el9_1 | |
kernel-debug-core | 5.14.0-162.22.2.el9_1 | |
kernel-debug-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debug-modules | 5.14.0-162.22.2.el9_1 | |
kernel-debug-modules-extra | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo-common-aarch64 | 5.14.0-162.22.2.el9_1 | |
kernel-modules | 5.14.0-162.22.2.el9_1 | |
kernel-modules-extra | 5.14.0-162.22.2.el9_1 | |
kernel-tools | 5.14.0-162.22.2.el9_1 | |
kernel-tools-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-tools-libs | 5.14.0-162.22.2.el9_1 | |
perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
python3-perf | 5.14.0-162.22.2.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
sos | 4.5.0-1.el9 | |
sos-audit | 4.5.0-1.el9 | |
tzdata | 2023b-1.el9 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 5.14.0-162.22.2.el9_1 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-host-debuginfo | 7.0.4-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.115-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.104-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet6.0-debugsource | 6.0.115-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet7.0-debugsource | 7.0.104-1.el9_1 | |
kernel-cross-headers | 5.14.0-162.22.2.el9_1 | |
kernel-debug-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo-common-aarch64 | 5.14.0-162.22.2.el9_1 | |
kernel-tools-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-tools-libs-devel | 5.14.0-162.22.2.el9_1 | |
libjpeg-turbo-debuginfo | 2.0.90-6.el9_1 | |
libjpeg-turbo-debugsource | 2.0.90-6.el9_1 | |
libjpeg-turbo-utils-debuginfo | 2.0.90-6.el9_1 | |
perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
turbojpeg | 2.0.90-6.el9_1 | |
turbojpeg-debuginfo | 2.0.90-6.el9_1 | |
turbojpeg-devel | 2.0.90-6.el9_1 | |

