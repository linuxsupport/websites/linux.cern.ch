## 2024-11-19

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libvpx | 1.9.0-8.el9_5 | [RHSA-2024:9827](https://access.redhat.com/errata/RHSA-2024:9827) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5197](https://access.redhat.com/security/cve/CVE-2024-5197))
libvpx-debuginfo | 1.9.0-8.el9_5 | |
libvpx-debugsource | 1.9.0-8.el9_5 | |
libvpx-utils-debuginfo | 1.9.0-8.el9_5 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libvpx-debuginfo | 1.9.0-8.el9_5 | |
libvpx-debugsource | 1.9.0-8.el9_5 | |
libvpx-devel | 1.9.0-8.el9_5 | [RHSA-2024:9827](https://access.redhat.com/errata/RHSA-2024:9827) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5197](https://access.redhat.com/security/cve/CVE-2024-5197))
libvpx-utils-debuginfo | 1.9.0-8.el9_5 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libvpx | 1.9.0-8.el9_5 | [RHSA-2024:9827](https://access.redhat.com/errata/RHSA-2024:9827) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5197](https://access.redhat.com/security/cve/CVE-2024-5197))
libvpx-debuginfo | 1.9.0-8.el9_5 | |
libvpx-debugsource | 1.9.0-8.el9_5 | |
libvpx-utils-debuginfo | 1.9.0-8.el9_5 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libvpx-debuginfo | 1.9.0-8.el9_5 | |
libvpx-debugsource | 1.9.0-8.el9_5 | |
libvpx-devel | 1.9.0-8.el9_5 | [RHSA-2024:9827](https://access.redhat.com/errata/RHSA-2024:9827) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5197](https://access.redhat.com/security/cve/CVE-2024-5197))
libvpx-utils-debuginfo | 1.9.0-8.el9_5 | |

