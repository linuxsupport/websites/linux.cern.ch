## 2024-07-03

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
go-toolset | 1.21.11-1.el9_4 | [RHSA-2024:4212](https://access.redhat.com/errata/RHSA-2024:4212) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24789](https://access.redhat.com/security/cve/CVE-2024-24789), [CVE-2024-24790](https://access.redhat.com/security/cve/CVE-2024-24790))
golang | 1.21.11-1.el9_4 | [RHSA-2024:4212](https://access.redhat.com/errata/RHSA-2024:4212) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24789](https://access.redhat.com/security/cve/CVE-2024-24789), [CVE-2024-24790](https://access.redhat.com/security/cve/CVE-2024-24790))
golang-bin | 1.21.11-1.el9_4 | [RHSA-2024:4212](https://access.redhat.com/errata/RHSA-2024:4212) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24789](https://access.redhat.com/security/cve/CVE-2024-24789), [CVE-2024-24790](https://access.redhat.com/security/cve/CVE-2024-24790))
golang-docs | 1.21.11-1.el9_4 | [RHSA-2024:4212](https://access.redhat.com/errata/RHSA-2024:4212) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24789](https://access.redhat.com/security/cve/CVE-2024-24789), [CVE-2024-24790](https://access.redhat.com/security/cve/CVE-2024-24790))
golang-misc | 1.21.11-1.el9_4 | [RHSA-2024:4212](https://access.redhat.com/errata/RHSA-2024:4212) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24789](https://access.redhat.com/security/cve/CVE-2024-24789), [CVE-2024-24790](https://access.redhat.com/security/cve/CVE-2024-24790))
golang-src | 1.21.11-1.el9_4 | [RHSA-2024:4212](https://access.redhat.com/errata/RHSA-2024:4212) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24789](https://access.redhat.com/security/cve/CVE-2024-24789), [CVE-2024-24790](https://access.redhat.com/security/cve/CVE-2024-24790))
golang-tests | 1.21.11-1.el9_4 | [RHSA-2024:4212](https://access.redhat.com/errata/RHSA-2024:4212) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24789](https://access.redhat.com/security/cve/CVE-2024-24789), [CVE-2024-24790](https://access.redhat.com/security/cve/CVE-2024-24790))
qemu-guest-agent | 8.2.0-11.el9_4.4 | |
qemu-guest-agent-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-img | 8.2.0-11.el9_4.4 | |
qemu-img-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm | 8.2.0-11.el9_4.4 | |
qemu-kvm-audio-dbus-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-audio-pa | 8.2.0-11.el9_4.4 | |
qemu-kvm-audio-pa-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-block-blkio | 8.2.0-11.el9_4.4 | |
qemu-kvm-block-blkio-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-block-curl | 8.2.0-11.el9_4.4 | |
qemu-kvm-block-curl-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-block-rbd | 8.2.0-11.el9_4.4 | |
qemu-kvm-block-rbd-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-common | 8.2.0-11.el9_4.4 | |
qemu-kvm-common-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-core | 8.2.0-11.el9_4.4 | |
qemu-kvm-core-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-debugsource | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-display-virtio-gpu | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-display-virtio-gpu-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-display-virtio-gpu-pci | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-display-virtio-gpu-pci-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-display-virtio-vga | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-display-virtio-vga-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-usb-host | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-usb-host-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-usb-redirect | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-usb-redirect-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-docs | 8.2.0-11.el9_4.4 | |
qemu-kvm-tests-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-tools | 8.2.0-11.el9_4.4 | |
qemu-kvm-tools-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-ui-dbus-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-ui-egl-headless | 8.2.0-11.el9_4.4 | |
qemu-kvm-ui-egl-headless-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-ui-opengl | 8.2.0-11.el9_4.4 | |
qemu-kvm-ui-opengl-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-pr-helper | 8.2.0-11.el9_4.4 | |
qemu-pr-helper-debuginfo | 8.2.0-11.el9_4.4 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
go-toolset | 1.21.11-1.el9_4 | [RHSA-2024:4212](https://access.redhat.com/errata/RHSA-2024:4212) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24789](https://access.redhat.com/security/cve/CVE-2024-24789), [CVE-2024-24790](https://access.redhat.com/security/cve/CVE-2024-24790))
golang | 1.21.11-1.el9_4 | [RHSA-2024:4212](https://access.redhat.com/errata/RHSA-2024:4212) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24789](https://access.redhat.com/security/cve/CVE-2024-24789), [CVE-2024-24790](https://access.redhat.com/security/cve/CVE-2024-24790))
golang-bin | 1.21.11-1.el9_4 | [RHSA-2024:4212](https://access.redhat.com/errata/RHSA-2024:4212) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24789](https://access.redhat.com/security/cve/CVE-2024-24789), [CVE-2024-24790](https://access.redhat.com/security/cve/CVE-2024-24790))
golang-docs | 1.21.11-1.el9_4 | [RHSA-2024:4212](https://access.redhat.com/errata/RHSA-2024:4212) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24789](https://access.redhat.com/security/cve/CVE-2024-24789), [CVE-2024-24790](https://access.redhat.com/security/cve/CVE-2024-24790))
golang-misc | 1.21.11-1.el9_4 | [RHSA-2024:4212](https://access.redhat.com/errata/RHSA-2024:4212) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24789](https://access.redhat.com/security/cve/CVE-2024-24789), [CVE-2024-24790](https://access.redhat.com/security/cve/CVE-2024-24790))
golang-src | 1.21.11-1.el9_4 | [RHSA-2024:4212](https://access.redhat.com/errata/RHSA-2024:4212) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24789](https://access.redhat.com/security/cve/CVE-2024-24789), [CVE-2024-24790](https://access.redhat.com/security/cve/CVE-2024-24790))
golang-tests | 1.21.11-1.el9_4 | [RHSA-2024:4212](https://access.redhat.com/errata/RHSA-2024:4212) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24789](https://access.redhat.com/security/cve/CVE-2024-24789), [CVE-2024-24790](https://access.redhat.com/security/cve/CVE-2024-24790))
qemu-guest-agent | 8.2.0-11.el9_4.4 | |
qemu-guest-agent-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-img | 8.2.0-11.el9_4.4 | |
qemu-img-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm | 8.2.0-11.el9_4.4 | |
qemu-kvm-audio-dbus-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-audio-pa | 8.2.0-11.el9_4.4 | |
qemu-kvm-audio-pa-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-block-blkio | 8.2.0-11.el9_4.4 | |
qemu-kvm-block-blkio-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-block-curl | 8.2.0-11.el9_4.4 | |
qemu-kvm-block-curl-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-block-rbd | 8.2.0-11.el9_4.4 | |
qemu-kvm-block-rbd-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-common | 8.2.0-11.el9_4.4 | |
qemu-kvm-common-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-core | 8.2.0-11.el9_4.4 | |
qemu-kvm-core-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-debugsource | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-display-virtio-gpu | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-display-virtio-gpu-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-display-virtio-gpu-pci | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-display-virtio-gpu-pci-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-usb-host | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-usb-host-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-usb-redirect | 8.2.0-11.el9_4.4 | |
qemu-kvm-device-usb-redirect-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-docs | 8.2.0-11.el9_4.4 | |
qemu-kvm-tests-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-tools | 8.2.0-11.el9_4.4 | |
qemu-kvm-tools-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-kvm-ui-dbus-debuginfo | 8.2.0-11.el9_4.4 | |
qemu-pr-helper | 8.2.0-11.el9_4.4 | |
qemu-pr-helper-debuginfo | 8.2.0-11.el9_4.4 | |

