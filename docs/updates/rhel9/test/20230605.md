## 2023-06-05

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20230604-1.rh9.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.5.3-1.el9 | [RHBA-2023:3414](https://access.redhat.com/errata/RHBA-2023:3414) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.5.3-1.el9 | [RHBA-2023:3414](https://access.redhat.com/errata/RHBA-2023:3414) | <div class="adv_b">Bug Fix Advisory</div>

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-filters | 1.28.7-11.el9_2.1 | |
cups-filters-debuginfo | 1.28.7-11.el9_2.1 | |
cups-filters-debugsource | 1.28.7-11.el9_2.1 | |
cups-filters-libs | 1.28.7-11.el9_2.1 | |
cups-filters-libs-debuginfo | 1.28.7-11.el9_2.1 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-filters-debuginfo | 1.28.7-11.el9_2.1 | |
cups-filters-debugsource | 1.28.7-11.el9_2.1 | |
cups-filters-devel | 1.28.7-11.el9_2.1 | |
cups-filters-libs-debuginfo | 1.28.7-11.el9_2.1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20230604-1.rh9.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.5.3-1.el9 | [RHBA-2023:3414](https://access.redhat.com/errata/RHBA-2023:3414) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.5.3-1.el9 | [RHBA-2023:3414](https://access.redhat.com/errata/RHBA-2023:3414) | <div class="adv_b">Bug Fix Advisory</div>

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-filters | 1.28.7-11.el9_2.1 | |
cups-filters-debuginfo | 1.28.7-11.el9_2.1 | |
cups-filters-debugsource | 1.28.7-11.el9_2.1 | |
cups-filters-libs | 1.28.7-11.el9_2.1 | |
cups-filters-libs-debuginfo | 1.28.7-11.el9_2.1 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-filters-debuginfo | 1.28.7-11.el9_2.1 | |
cups-filters-debugsource | 1.28.7-11.el9_2.1 | |
cups-filters-devel | 1.28.7-11.el9_2.1 | |
cups-filters-libs-debuginfo | 1.28.7-11.el9_2.1 | |

