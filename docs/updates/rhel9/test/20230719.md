## 2023-07-19

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
webkit2gtk3 | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-debugsource | 2.38.5-1.el9_2.3 | |
webkit2gtk3-devel | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-devel-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-jsc | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-jsc-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-jsc-devel | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-jsc-devel-debuginfo | 2.38.5-1.el9_2.3 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
webkit2gtk3 | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-debugsource | 2.38.5-1.el9_2.3 | |
webkit2gtk3-devel | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-devel-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-jsc | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-jsc-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-jsc-devel | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-jsc-devel-debuginfo | 2.38.5-1.el9_2.3 | |

