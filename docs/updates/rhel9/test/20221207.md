## 2022-12-07

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 18.12.1-1.module+el9.1.0.z+17326+318294bb | |
nodejs-debuginfo | 18.12.1-1.module+el9.1.0.z+17326+318294bb | |
nodejs-debugsource | 18.12.1-1.module+el9.1.0.z+17326+318294bb | |
nodejs-devel | 18.12.1-1.module+el9.1.0.z+17326+318294bb | |
nodejs-docs | 18.12.1-1.module+el9.1.0.z+17326+318294bb | |
nodejs-full-i18n | 18.12.1-1.module+el9.1.0.z+17326+318294bb | |
nodejs-nodemon | 2.0.20-1.module+el9.1.0.z+17326+318294bb | |
npm | 8.19.2-1.18.12.1.1.module+el9.1.0.z+17326+318294bb | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 18.12.1-1.module+el9.1.0.z+17326+318294bb | |
nodejs-debuginfo | 18.12.1-1.module+el9.1.0.z+17326+318294bb | |
nodejs-debugsource | 18.12.1-1.module+el9.1.0.z+17326+318294bb | |
nodejs-devel | 18.12.1-1.module+el9.1.0.z+17326+318294bb | |
nodejs-docs | 18.12.1-1.module+el9.1.0.z+17326+318294bb | |
nodejs-full-i18n | 18.12.1-1.module+el9.1.0.z+17326+318294bb | |
nodejs-nodemon | 2.0.20-1.module+el9.1.0.z+17326+318294bb | |
npm | 8.19.2-1.18.12.1.1.module+el9.1.0.z+17326+318294bb | |

