## 2023-09-27

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.6.0-2.el9 | [RHBA-2023:5354](https://access.redhat.com/errata/RHBA-2023:5354) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.6.0-2.el9 | [RHBA-2023:5354](https://access.redhat.com/errata/RHBA-2023:5354) | <div class="adv_b">Bug Fix Advisory</div>

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 18.17.1-1.module+el9.2.0.z+19753+58118bc0 | [RHSA-2023:5363](https://access.redhat.com/errata/RHSA-2023:5363) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-debuginfo | 18.17.1-1.module+el9.2.0.z+19753+58118bc0 | |
nodejs-debugsource | 18.17.1-1.module+el9.2.0.z+19753+58118bc0 | |
nodejs-devel | 18.17.1-1.module+el9.2.0.z+19753+58118bc0 | [RHSA-2023:5363](https://access.redhat.com/errata/RHSA-2023:5363) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-docs | 18.17.1-1.module+el9.2.0.z+19753+58118bc0 | [RHSA-2023:5363](https://access.redhat.com/errata/RHSA-2023:5363) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-full-i18n | 18.17.1-1.module+el9.2.0.z+19753+58118bc0 | [RHSA-2023:5363](https://access.redhat.com/errata/RHSA-2023:5363) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-nodemon | 3.0.1-1.module+el9.2.0.z+19753+58118bc0 | [RHSA-2023:5363](https://access.redhat.com/errata/RHSA-2023:5363) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
npm | 9.6.7-1.18.17.1.1.module+el9.2.0.z+19753+58118bc0 | [RHSA-2023:5363](https://access.redhat.com/errata/RHSA-2023:5363) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.6.0-2.el9 | [RHBA-2023:5354](https://access.redhat.com/errata/RHBA-2023:5354) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.6.0-2.el9 | [RHBA-2023:5354](https://access.redhat.com/errata/RHBA-2023:5354) | <div class="adv_b">Bug Fix Advisory</div>

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 18.17.1-1.module+el9.2.0.z+19753+58118bc0 | [RHSA-2023:5363](https://access.redhat.com/errata/RHSA-2023:5363) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-debuginfo | 18.17.1-1.module+el9.2.0.z+19753+58118bc0 | |
nodejs-debugsource | 18.17.1-1.module+el9.2.0.z+19753+58118bc0 | |
nodejs-devel | 18.17.1-1.module+el9.2.0.z+19753+58118bc0 | [RHSA-2023:5363](https://access.redhat.com/errata/RHSA-2023:5363) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-docs | 18.17.1-1.module+el9.2.0.z+19753+58118bc0 | [RHSA-2023:5363](https://access.redhat.com/errata/RHSA-2023:5363) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-full-i18n | 18.17.1-1.module+el9.2.0.z+19753+58118bc0 | [RHSA-2023:5363](https://access.redhat.com/errata/RHSA-2023:5363) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-nodemon | 3.0.1-1.module+el9.2.0.z+19753+58118bc0 | [RHSA-2023:5363](https://access.redhat.com/errata/RHSA-2023:5363) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
npm | 9.6.7-1.18.17.1.1.module+el9.2.0.z+19753+58118bc0 | [RHSA-2023:5363](https://access.redhat.com/errata/RHSA-2023:5363) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))

