## 2025-02-17

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
virt-v2v | 2.5.6-8.el9_5 | [RHBA-2025:1512](https://access.redhat.com/errata/RHBA-2025:1512) | <div class="adv_b">Bug Fix Advisory</div>
virt-v2v-bash-completion | 2.5.6-8.el9_5 | [RHBA-2025:1512](https://access.redhat.com/errata/RHBA-2025:1512) | <div class="adv_b">Bug Fix Advisory</div>
virt-v2v-debuginfo | 2.5.6-8.el9_5 | |
virt-v2v-debugsource | 2.5.6-8.el9_5 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
virt-v2v-man-pages-ja | 2.5.6-8.el9_5 | [RHBA-2025:1512](https://access.redhat.com/errata/RHBA-2025:1512) | <div class="adv_b">Bug Fix Advisory</div>
virt-v2v-man-pages-uk | 2.5.6-8.el9_5 | [RHBA-2025:1512](https://access.redhat.com/errata/RHBA-2025:1512) | <div class="adv_b">Bug Fix Advisory</div>

