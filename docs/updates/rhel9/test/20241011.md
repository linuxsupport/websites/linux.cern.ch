## 2024-10-11

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 128.3.1-2.el9_4 | [RHSA-2024:7958](https://access.redhat.com/errata/RHSA-2024:7958) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9680](https://access.redhat.com/security/cve/CVE-2024-9680))
firefox-debuginfo | 128.3.1-2.el9_4 | |
firefox-debugsource | 128.3.1-2.el9_4 | |
firefox-x11 | 128.3.1-2.el9_4 | [RHSA-2024:7958](https://access.redhat.com/errata/RHSA-2024:7958) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9680](https://access.redhat.com/security/cve/CVE-2024-9680))

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 128.3.1-2.el9_4 | [RHSA-2024:7958](https://access.redhat.com/errata/RHSA-2024:7958) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9680](https://access.redhat.com/security/cve/CVE-2024-9680))
firefox-debuginfo | 128.3.1-2.el9_4 | |
firefox-debugsource | 128.3.1-2.el9_4 | |
firefox-x11 | 128.3.1-2.el9_4 | [RHSA-2024:7958](https://access.redhat.com/errata/RHSA-2024:7958) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9680](https://access.redhat.com/security/cve/CVE-2024-9680))

