## 2023-06-06

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.3-1.rh9.cern | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
webkit2gtk3 | 2.38.5-1.el9_2.2 | |
webkit2gtk3-debuginfo | 2.38.5-1.el9_2.2 | |
webkit2gtk3-debugsource | 2.38.5-1.el9_2.2 | |
webkit2gtk3-devel | 2.38.5-1.el9_2.2 | |
webkit2gtk3-devel-debuginfo | 2.38.5-1.el9_2.2 | |
webkit2gtk3-jsc | 2.38.5-1.el9_2.2 | |
webkit2gtk3-jsc-debuginfo | 2.38.5-1.el9_2.2 | |
webkit2gtk3-jsc-devel | 2.38.5-1.el9_2.2 | |
webkit2gtk3-jsc-devel-debuginfo | 2.38.5-1.el9_2.2 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.3-1.rh9.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
webkit2gtk3 | 2.38.5-1.el9_2.2 | |
webkit2gtk3-debuginfo | 2.38.5-1.el9_2.2 | |
webkit2gtk3-debugsource | 2.38.5-1.el9_2.2 | |
webkit2gtk3-devel | 2.38.5-1.el9_2.2 | |
webkit2gtk3-devel-debuginfo | 2.38.5-1.el9_2.2 | |
webkit2gtk3-jsc | 2.38.5-1.el9_2.2 | |
webkit2gtk3-jsc-debuginfo | 2.38.5-1.el9_2.2 | |
webkit2gtk3-jsc-devel | 2.38.5-1.el9_2.2 | |
webkit2gtk3-jsc-devel-debuginfo | 2.38.5-1.el9_2.2 | |

