## 2023-07-18

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-chroot | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-debuginfo | 9.16.23-11.el9_2.1 | |
bind-debugsource | 9.16.23-11.el9_2.1 | |
bind-dnssec-doc | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils-debuginfo | 9.16.23-11.el9_2.1 | |
bind-libs | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-libs-debuginfo | 9.16.23-11.el9_2.1 | |
bind-license | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-utils | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-utils-debuginfo | 9.16.23-11.el9_2.1 | |
python3-bind | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-debuginfo | 9.16.23-11.el9_2.1 | |
bind-debugsource | 9.16.23-11.el9_2.1 | |
bind-devel | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils-debuginfo | 9.16.23-11.el9_2.1 | |
bind-doc | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-libs-debuginfo | 9.16.23-11.el9_2.1 | |
bind-utils-debuginfo | 9.16.23-11.el9_2.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-chroot | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-debuginfo | 9.16.23-11.el9_2.1 | |
bind-debugsource | 9.16.23-11.el9_2.1 | |
bind-dnssec-doc | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils-debuginfo | 9.16.23-11.el9_2.1 | |
bind-libs | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-libs-debuginfo | 9.16.23-11.el9_2.1 | |
bind-license | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-utils | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-utils-debuginfo | 9.16.23-11.el9_2.1 | |
python3-bind | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-debuginfo | 9.16.23-11.el9_2.1 | |
bind-debugsource | 9.16.23-11.el9_2.1 | |
bind-devel | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils-debuginfo | 9.16.23-11.el9_2.1 | |
bind-doc | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-libs-debuginfo | 9.16.23-11.el9_2.1 | |
bind-utils-debuginfo | 9.16.23-11.el9_2.1 | |

