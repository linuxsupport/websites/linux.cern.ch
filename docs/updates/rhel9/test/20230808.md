## 2023-08-08

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 18.16.1-2.module+el9.2.0.z+19546+89a27685 | [RHBA-2023:4491](https://access.redhat.com/errata/RHBA-2023:4491) | <div class="adv_b">Bug Fix Advisory</div>
nodejs-debuginfo | 18.16.1-2.module+el9.2.0.z+19546+89a27685 | |
nodejs-debugsource | 18.16.1-2.module+el9.2.0.z+19546+89a27685 | |
nodejs-devel | 18.16.1-2.module+el9.2.0.z+19546+89a27685 | [RHBA-2023:4491](https://access.redhat.com/errata/RHBA-2023:4491) | <div class="adv_b">Bug Fix Advisory</div>
nodejs-docs | 18.16.1-2.module+el9.2.0.z+19546+89a27685 | [RHBA-2023:4491](https://access.redhat.com/errata/RHBA-2023:4491) | <div class="adv_b">Bug Fix Advisory</div>
nodejs-full-i18n | 18.16.1-2.module+el9.2.0.z+19546+89a27685 | [RHBA-2023:4491](https://access.redhat.com/errata/RHBA-2023:4491) | <div class="adv_b">Bug Fix Advisory</div>
npm | 9.5.1-1.18.16.1.2.module+el9.2.0.z+19546+89a27685 | [RHBA-2023:4491](https://access.redhat.com/errata/RHBA-2023:4491) | <div class="adv_b">Bug Fix Advisory</div>
thunderbird | 102.14.0-1.el9_2 | [RHSA-2023:4499](https://access.redhat.com/errata/RHSA-2023:4499) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3417](https://access.redhat.com/security/cve/CVE-2023-3417), [CVE-2023-4045](https://access.redhat.com/security/cve/CVE-2023-4045), [CVE-2023-4046](https://access.redhat.com/security/cve/CVE-2023-4046), [CVE-2023-4047](https://access.redhat.com/security/cve/CVE-2023-4047), [CVE-2023-4048](https://access.redhat.com/security/cve/CVE-2023-4048), [CVE-2023-4049](https://access.redhat.com/security/cve/CVE-2023-4049), [CVE-2023-4050](https://access.redhat.com/security/cve/CVE-2023-4050), [CVE-2023-4055](https://access.redhat.com/security/cve/CVE-2023-4055), [CVE-2023-4056](https://access.redhat.com/security/cve/CVE-2023-4056), [CVE-2023-4057](https://access.redhat.com/security/cve/CVE-2023-4057))
thunderbird-debuginfo | 102.14.0-1.el9_2 | |
thunderbird-debugsource | 102.14.0-1.el9_2 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 18.16.1-2.module+el9.2.0.z+19546+89a27685 | [RHBA-2023:4491](https://access.redhat.com/errata/RHBA-2023:4491) | <div class="adv_b">Bug Fix Advisory</div>
nodejs-debuginfo | 18.16.1-2.module+el9.2.0.z+19546+89a27685 | |
nodejs-debugsource | 18.16.1-2.module+el9.2.0.z+19546+89a27685 | |
nodejs-devel | 18.16.1-2.module+el9.2.0.z+19546+89a27685 | [RHBA-2023:4491](https://access.redhat.com/errata/RHBA-2023:4491) | <div class="adv_b">Bug Fix Advisory</div>
nodejs-docs | 18.16.1-2.module+el9.2.0.z+19546+89a27685 | [RHBA-2023:4491](https://access.redhat.com/errata/RHBA-2023:4491) | <div class="adv_b">Bug Fix Advisory</div>
nodejs-full-i18n | 18.16.1-2.module+el9.2.0.z+19546+89a27685 | [RHBA-2023:4491](https://access.redhat.com/errata/RHBA-2023:4491) | <div class="adv_b">Bug Fix Advisory</div>
npm | 9.5.1-1.18.16.1.2.module+el9.2.0.z+19546+89a27685 | [RHBA-2023:4491](https://access.redhat.com/errata/RHBA-2023:4491) | <div class="adv_b">Bug Fix Advisory</div>
thunderbird | 102.14.0-1.el9_2 | [RHSA-2023:4499](https://access.redhat.com/errata/RHSA-2023:4499) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3417](https://access.redhat.com/security/cve/CVE-2023-3417), [CVE-2023-4045](https://access.redhat.com/security/cve/CVE-2023-4045), [CVE-2023-4046](https://access.redhat.com/security/cve/CVE-2023-4046), [CVE-2023-4047](https://access.redhat.com/security/cve/CVE-2023-4047), [CVE-2023-4048](https://access.redhat.com/security/cve/CVE-2023-4048), [CVE-2023-4049](https://access.redhat.com/security/cve/CVE-2023-4049), [CVE-2023-4050](https://access.redhat.com/security/cve/CVE-2023-4050), [CVE-2023-4055](https://access.redhat.com/security/cve/CVE-2023-4055), [CVE-2023-4056](https://access.redhat.com/security/cve/CVE-2023-4056), [CVE-2023-4057](https://access.redhat.com/security/cve/CVE-2023-4057))
thunderbird-debuginfo | 102.14.0-1.el9_2 | |
thunderbird-debugsource | 102.14.0-1.el9_2 | |

