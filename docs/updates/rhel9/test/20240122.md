## 2024-01-22

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssl | 3.0.7-25.el9_3 | |
openssl-debuginfo | 3.0.7-25.el9_3 | |
openssl-debugsource | 3.0.7-25.el9_3 | |
openssl-libs | 3.0.7-25.el9_3 | |
openssl-libs-debuginfo | 3.0.7-25.el9_3 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssl-debuginfo | 3.0.7-25.el9_3 | |
openssl-debugsource | 3.0.7-25.el9_3 | |
openssl-devel | 3.0.7-25.el9_3 | |
openssl-libs-debuginfo | 3.0.7-25.el9_3 | |
openssl-perl | 3.0.7-25.el9_3 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssl | 3.0.7-25.el9_3 | |
openssl-debuginfo | 3.0.7-25.el9_3 | |
openssl-debugsource | 3.0.7-25.el9_3 | |
openssl-libs | 3.0.7-25.el9_3 | |
openssl-libs-debuginfo | 3.0.7-25.el9_3 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssl-debuginfo | 3.0.7-25.el9_3 | |
openssl-debugsource | 3.0.7-25.el9_3 | |
openssl-devel | 3.0.7-25.el9_3 | |
openssl-libs-debuginfo | 3.0.7-25.el9_3 | |
openssl-perl | 3.0.7-25.el9_3 | |

