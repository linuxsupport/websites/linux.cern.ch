## 2024-09-09

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
glib2 | 2.68.4-14.el9_4.1 | |
glib2-debuginfo | 2.68.4-14.el9_4.1 | |
glib2-debugsource | 2.68.4-14.el9_4.1 | |
glib2-devel-debuginfo | 2.68.4-14.el9_4.1 | |
glib2-tests-debuginfo | 2.68.4-14.el9_4.1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
glib2-debuginfo | 2.68.4-14.el9_4.1 | |
glib2-debugsource | 2.68.4-14.el9_4.1 | |
glib2-devel | 2.68.4-14.el9_4.1 | |
glib2-devel-debuginfo | 2.68.4-14.el9_4.1 | |
glib2-doc | 2.68.4-14.el9_4.1 | |
glib2-tests | 2.68.4-14.el9_4.1 | |
glib2-tests-debuginfo | 2.68.4-14.el9_4.1 | |
net-snmp | 5.9.1-13.el9_4.2 | |
net-snmp-agent-libs | 5.9.1-13.el9_4.2 | |
net-snmp-agent-libs-debuginfo | 5.9.1-13.el9_4.2 | |
net-snmp-debuginfo | 5.9.1-13.el9_4.2 | |
net-snmp-debugsource | 5.9.1-13.el9_4.2 | |
net-snmp-devel | 5.9.1-13.el9_4.2 | |
net-snmp-libs | 5.9.1-13.el9_4.2 | |
net-snmp-libs-debuginfo | 5.9.1-13.el9_4.2 | |
net-snmp-perl | 5.9.1-13.el9_4.2 | |
net-snmp-perl-debuginfo | 5.9.1-13.el9_4.2 | |
net-snmp-utils | 5.9.1-13.el9_4.2 | |
net-snmp-utils-debuginfo | 5.9.1-13.el9_4.2 | |
python3-net-snmp | 5.9.1-13.el9_4.2 | |
python3-net-snmp-debuginfo | 5.9.1-13.el9_4.2 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
glib2-debuginfo | 2.68.4-14.el9_4.1 | |
glib2-debugsource | 2.68.4-14.el9_4.1 | |
glib2-devel-debuginfo | 2.68.4-14.el9_4.1 | |
glib2-static | 2.68.4-14.el9_4.1 | |
glib2-tests-debuginfo | 2.68.4-14.el9_4.1 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
glib2 | 2.68.4-14.el9_4.1 | |
glib2-debuginfo | 2.68.4-14.el9_4.1 | |
glib2-debugsource | 2.68.4-14.el9_4.1 | |
glib2-devel-debuginfo | 2.68.4-14.el9_4.1 | |
glib2-tests-debuginfo | 2.68.4-14.el9_4.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
glib2-debuginfo | 2.68.4-14.el9_4.1 | |
glib2-debugsource | 2.68.4-14.el9_4.1 | |
glib2-devel | 2.68.4-14.el9_4.1 | |
glib2-devel-debuginfo | 2.68.4-14.el9_4.1 | |
glib2-doc | 2.68.4-14.el9_4.1 | |
glib2-tests | 2.68.4-14.el9_4.1 | |
glib2-tests-debuginfo | 2.68.4-14.el9_4.1 | |
net-snmp | 5.9.1-13.el9_4.2 | |
net-snmp-agent-libs | 5.9.1-13.el9_4.2 | |
net-snmp-agent-libs-debuginfo | 5.9.1-13.el9_4.2 | |
net-snmp-debuginfo | 5.9.1-13.el9_4.2 | |
net-snmp-debugsource | 5.9.1-13.el9_4.2 | |
net-snmp-devel | 5.9.1-13.el9_4.2 | |
net-snmp-libs | 5.9.1-13.el9_4.2 | |
net-snmp-libs-debuginfo | 5.9.1-13.el9_4.2 | |
net-snmp-perl | 5.9.1-13.el9_4.2 | |
net-snmp-perl-debuginfo | 5.9.1-13.el9_4.2 | |
net-snmp-utils | 5.9.1-13.el9_4.2 | |
net-snmp-utils-debuginfo | 5.9.1-13.el9_4.2 | |
python3-net-snmp | 5.9.1-13.el9_4.2 | |
python3-net-snmp-debuginfo | 5.9.1-13.el9_4.2 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
glib2-debuginfo | 2.68.4-14.el9_4.1 | |
glib2-debugsource | 2.68.4-14.el9_4.1 | |
glib2-devel-debuginfo | 2.68.4-14.el9_4.1 | |
glib2-static | 2.68.4-14.el9_4.1 | |
glib2-tests-debuginfo | 2.68.4-14.el9_4.1 | |

