## 2024-10-10

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.35-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-runtime-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-runtime-dbg-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-targeting-pack-6.0 | 6.0.35-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-targeting-pack-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-apphost-pack-6.0 | 6.0.35-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-apphost-pack-6.0-debuginfo | 6.0.35-1.el9_4 | |
dotnet-apphost-pack-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-apphost-pack-8.0-debuginfo | 8.0.10-1.el9_4 | |
dotnet-host | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-host-debuginfo | 8.0.10-1.el9_4 | |
dotnet-hostfxr-6.0 | 6.0.35-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-hostfxr-6.0-debuginfo | 6.0.35-1.el9_4 | |
dotnet-hostfxr-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-hostfxr-8.0-debuginfo | 8.0.10-1.el9_4 | |
dotnet-runtime-6.0 | 6.0.35-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-runtime-6.0-debuginfo | 6.0.35-1.el9_4 | |
dotnet-runtime-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-runtime-8.0-debuginfo | 8.0.10-1.el9_4 | |
dotnet-runtime-dbg-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-6.0 | 6.0.135-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-6.0-debuginfo | 6.0.135-1.el9_4 | |
dotnet-sdk-8.0 | 8.0.110-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-8.0-debuginfo | 8.0.110-1.el9_4 | |
dotnet-sdk-dbg-8.0 | 8.0.110-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-targeting-pack-6.0 | 6.0.35-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-targeting-pack-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-templates-6.0 | 6.0.135-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-templates-8.0 | 8.0.110-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet6.0-debuginfo | 6.0.135-1.el9_4 | |
dotnet6.0-debugsource | 6.0.135-1.el9_4 | |
dotnet8.0-debuginfo | 8.0.110-1.el9_4 | |
dotnet8.0-debugsource | 8.0.110-1.el9_4 | |
netstandard-targeting-pack-2.1 | 8.0.110-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.35-1.el9_4 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.10-1.el9_4 | |
dotnet-host-debuginfo | 8.0.10-1.el9_4 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.35-1.el9_4 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.10-1.el9_4 | |
dotnet-runtime-6.0-debuginfo | 6.0.35-1.el9_4 | |
dotnet-runtime-8.0-debuginfo | 8.0.10-1.el9_4 | |
dotnet-sdk-6.0-debuginfo | 6.0.135-1.el9_4 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.135-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-8.0-debuginfo | 8.0.110-1.el9_4 | |
dotnet-sdk-8.0-source-built-artifacts | 8.0.110-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet6.0-debuginfo | 6.0.135-1.el9_4 | |
dotnet6.0-debugsource | 6.0.135-1.el9_4 | |
dotnet8.0-debuginfo | 8.0.110-1.el9_4 | |
dotnet8.0-debugsource | 8.0.110-1.el9_4 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.35-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-runtime-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-runtime-dbg-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-targeting-pack-6.0 | 6.0.35-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-targeting-pack-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-apphost-pack-6.0 | 6.0.35-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-apphost-pack-6.0-debuginfo | 6.0.35-1.el9_4 | |
dotnet-apphost-pack-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-apphost-pack-8.0-debuginfo | 8.0.10-1.el9_4 | |
dotnet-host | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-host-debuginfo | 8.0.10-1.el9_4 | |
dotnet-hostfxr-6.0 | 6.0.35-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-hostfxr-6.0-debuginfo | 6.0.35-1.el9_4 | |
dotnet-hostfxr-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-hostfxr-8.0-debuginfo | 8.0.10-1.el9_4 | |
dotnet-runtime-6.0 | 6.0.35-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-runtime-6.0-debuginfo | 6.0.35-1.el9_4 | |
dotnet-runtime-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-runtime-8.0-debuginfo | 8.0.10-1.el9_4 | |
dotnet-runtime-dbg-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-6.0 | 6.0.135-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-6.0-debuginfo | 6.0.135-1.el9_4 | |
dotnet-sdk-8.0 | 8.0.110-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-8.0-debuginfo | 8.0.110-1.el9_4 | |
dotnet-sdk-dbg-8.0 | 8.0.110-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-targeting-pack-6.0 | 6.0.35-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-targeting-pack-8.0 | 8.0.10-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-templates-6.0 | 6.0.135-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-templates-8.0 | 8.0.110-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet6.0-debuginfo | 6.0.135-1.el9_4 | |
dotnet6.0-debugsource | 6.0.135-1.el9_4 | |
dotnet8.0-debuginfo | 8.0.110-1.el9_4 | |
dotnet8.0-debugsource | 8.0.110-1.el9_4 | |
netstandard-targeting-pack-2.1 | 8.0.110-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.35-1.el9_4 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.10-1.el9_4 | |
dotnet-host-debuginfo | 8.0.10-1.el9_4 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.35-1.el9_4 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.10-1.el9_4 | |
dotnet-runtime-6.0-debuginfo | 6.0.35-1.el9_4 | |
dotnet-runtime-8.0-debuginfo | 8.0.10-1.el9_4 | |
dotnet-sdk-6.0-debuginfo | 6.0.135-1.el9_4 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.135-1.el9_4 | [RHSA-2024:7867](https://access.redhat.com/errata/RHSA-2024:7867) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-8.0-debuginfo | 8.0.110-1.el9_4 | |
dotnet-sdk-8.0-source-built-artifacts | 8.0.110-1.el9_4 | [RHSA-2024:7869](https://access.redhat.com/errata/RHSA-2024:7869) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet6.0-debuginfo | 6.0.135-1.el9_4 | |
dotnet6.0-debugsource | 6.0.135-1.el9_4 | |
dotnet8.0-debuginfo | 8.0.110-1.el9_4 | |
dotnet8.0-debugsource | 8.0.110-1.el9_4 | |

