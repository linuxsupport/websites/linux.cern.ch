## 2023-05-26

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
go-toolset | 1.19.9-1.el9_2 | [RHSA-2023:3318](https://access.redhat.com/errata/RHSA-2023:3318) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang | 1.19.9-2.el9_2 | [RHSA-2023:3318](https://access.redhat.com/errata/RHSA-2023:3318) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-bin | 1.19.9-2.el9_2 | [RHSA-2023:3318](https://access.redhat.com/errata/RHSA-2023:3318) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-docs | 1.19.9-2.el9_2 | [RHSA-2023:3318](https://access.redhat.com/errata/RHSA-2023:3318) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-misc | 1.19.9-2.el9_2 | [RHSA-2023:3318](https://access.redhat.com/errata/RHSA-2023:3318) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-race | 1.19.9-2.el9_2 | [RHSA-2023:3318](https://access.redhat.com/errata/RHSA-2023:3318) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-src | 1.19.9-2.el9_2 | [RHSA-2023:3318](https://access.redhat.com/errata/RHSA-2023:3318) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-tests | 1.19.9-2.el9_2 | [RHSA-2023:3318](https://access.redhat.com/errata/RHSA-2023:3318) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
go-toolset | 1.19.9-1.el9_2 | [RHSA-2023:3318](https://access.redhat.com/errata/RHSA-2023:3318) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang | 1.19.9-2.el9_2 | [RHSA-2023:3318](https://access.redhat.com/errata/RHSA-2023:3318) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-bin | 1.19.9-2.el9_2 | [RHSA-2023:3318](https://access.redhat.com/errata/RHSA-2023:3318) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-docs | 1.19.9-2.el9_2 | [RHSA-2023:3318](https://access.redhat.com/errata/RHSA-2023:3318) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-misc | 1.19.9-2.el9_2 | [RHSA-2023:3318](https://access.redhat.com/errata/RHSA-2023:3318) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-src | 1.19.9-2.el9_2 | [RHSA-2023:3318](https://access.redhat.com/errata/RHSA-2023:3318) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-tests | 1.19.9-2.el9_2 | [RHSA-2023:3318](https://access.redhat.com/errata/RHSA-2023:3318) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))

