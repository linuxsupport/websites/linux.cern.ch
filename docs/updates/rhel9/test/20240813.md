## 2024-08-13

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-chroot | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-debuginfo | 9.16.23-18.el9_4.6 | |
bind-debugsource | 9.16.23-18.el9_4.6 | |
bind-dnssec-doc | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-dnssec-utils | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-dnssec-utils-debuginfo | 9.16.23-18.el9_4.6 | |
bind-dyndb-ldap | 11.9-10.el9_4 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-dyndb-ldap-debuginfo | 11.9-10.el9_4 | |
bind-dyndb-ldap-debugsource | 11.9-10.el9_4 | |
bind-libs | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-libs-debuginfo | 9.16.23-18.el9_4.6 | |
bind-license | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-utils | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-utils-debuginfo | 9.16.23-18.el9_4.6 | |
python3-bind | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-debuginfo | 9.16.23-18.el9_4.6 | |
bind-debugsource | 9.16.23-18.el9_4.6 | |
bind-devel | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-dnssec-utils-debuginfo | 9.16.23-18.el9_4.6 | |
bind-doc | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-libs-debuginfo | 9.16.23-18.el9_4.6 | |
bind-utils-debuginfo | 9.16.23-18.el9_4.6 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-chroot | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-debuginfo | 9.16.23-18.el9_4.6 | |
bind-debugsource | 9.16.23-18.el9_4.6 | |
bind-dnssec-doc | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-dnssec-utils | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-dnssec-utils-debuginfo | 9.16.23-18.el9_4.6 | |
bind-dyndb-ldap | 11.9-10.el9_4 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-dyndb-ldap-debuginfo | 11.9-10.el9_4 | |
bind-dyndb-ldap-debugsource | 11.9-10.el9_4 | |
bind-libs | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-libs-debuginfo | 9.16.23-18.el9_4.6 | |
bind-license | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-utils | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-utils-debuginfo | 9.16.23-18.el9_4.6 | |
python3-bind | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-debuginfo | 9.16.23-18.el9_4.6 | |
bind-debugsource | 9.16.23-18.el9_4.6 | |
bind-devel | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-dnssec-utils-debuginfo | 9.16.23-18.el9_4.6 | |
bind-doc | 9.16.23-18.el9_4.6 | [RHSA-2024:5231](https://access.redhat.com/errata/RHSA-2024:5231) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind-libs-debuginfo | 9.16.23-18.el9_4.6 | |
bind-utils-debuginfo | 9.16.23-18.el9_4.6 | |

