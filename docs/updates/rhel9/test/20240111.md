## 2024-01-11

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.26-1.el9_3 | |
aspnetcore-runtime-7.0 | 7.0.15-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-runtime-8.0 | 8.0.1-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-targeting-pack-6.0 | 6.0.26-1.el9_3 | |
aspnetcore-targeting-pack-7.0 | 7.0.15-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-targeting-pack-8.0 | 8.0.1-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-apphost-pack-6.0 | 6.0.26-1.el9_3 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.26-1.el9_3 | |
dotnet-apphost-pack-7.0 | 7.0.15-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-apphost-pack-7.0-debuginfo | 7.0.15-1.el9_3 | |
dotnet-apphost-pack-8.0 | 8.0.1-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-apphost-pack-8.0-debuginfo | 8.0.1-1.el9_3 | |
dotnet-host | 8.0.1-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-host-debuginfo | 8.0.1-1.el9_3 | |
dotnet-hostfxr-6.0 | 6.0.26-1.el9_3 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.26-1.el9_3 | |
dotnet-hostfxr-7.0 | 7.0.15-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-hostfxr-7.0-debuginfo | 7.0.15-1.el9_3 | |
dotnet-hostfxr-8.0 | 8.0.1-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-hostfxr-8.0-debuginfo | 8.0.1-1.el9_3 | |
dotnet-runtime-6.0 | 6.0.26-1.el9_3 | |
dotnet-runtime-6.0-debuginfo | 6.0.26-1.el9_3 | |
dotnet-runtime-7.0 | 7.0.15-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-runtime-7.0-debuginfo | 7.0.15-1.el9_3 | |
dotnet-runtime-8.0 | 8.0.1-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-runtime-8.0-debuginfo | 8.0.1-1.el9_3 | |
dotnet-sdk-6.0 | 6.0.126-1.el9_3 | |
dotnet-sdk-6.0-debuginfo | 6.0.126-1.el9_3 | |
dotnet-sdk-7.0 | 7.0.115-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-7.0-debuginfo | 7.0.115-1.el9_3 | |
dotnet-sdk-8.0 | 8.0.101-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-8.0-debuginfo | 8.0.101-1.el9_3 | |
dotnet-targeting-pack-6.0 | 6.0.26-1.el9_3 | |
dotnet-targeting-pack-7.0 | 7.0.15-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-targeting-pack-8.0 | 8.0.1-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-templates-6.0 | 6.0.126-1.el9_3 | |
dotnet-templates-7.0 | 7.0.115-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-templates-8.0 | 8.0.101-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet6.0-debuginfo | 6.0.126-1.el9_3 | |
dotnet6.0-debugsource | 6.0.126-1.el9_3 | |
dotnet7.0-debuginfo | 7.0.115-1.el9_3 | |
dotnet7.0-debugsource | 7.0.115-1.el9_3 | |
dotnet8.0-debuginfo | 8.0.101-1.el9_3 | |
dotnet8.0-debugsource | 8.0.101-1.el9_3 | |
ipa-client | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-client-common | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-client-debuginfo | 4.10.2-5.el9_3 | |
ipa-client-epn | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-client-samba | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-common | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-debuginfo | 4.10.2-5.el9_3 | |
ipa-debugsource | 4.10.2-5.el9_3 | |
ipa-selinux | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-server | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-server-common | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-server-debuginfo | 4.10.2-5.el9_3 | |
ipa-server-dns | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-server-trust-ad | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-server-trust-ad-debuginfo | 4.10.2-5.el9_3 | |
netstandard-targeting-pack-2.1 | 8.0.101-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
nspr | 4.35.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nspr-debuginfo | 4.35.0-4.el9_3 | |
nspr-devel | 4.35.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-debuginfo | 3.90.0-4.el9_3 | |
nss-debugsource | 3.90.0-4.el9_3 | |
nss-devel | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn-debuginfo | 3.90.0-4.el9_3 | |
nss-softokn-devel | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn-freebl | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn-freebl-debuginfo | 3.90.0-4.el9_3 | |
nss-softokn-freebl-devel | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-sysinit | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-sysinit-debuginfo | 3.90.0-4.el9_3 | |
nss-tools | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-tools-debuginfo | 3.90.0-4.el9_3 | |
nss-util | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-util-debuginfo | 3.90.0-4.el9_3 | |
nss-util-devel | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
python3-ipaclient | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
python3-ipalib | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
python3-ipaserver | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.26-1.el9_3 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.15-1.el9_3 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.1-1.el9_3 | |
dotnet-host-debuginfo | 8.0.1-1.el9_3 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.26-1.el9_3 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.15-1.el9_3 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.1-1.el9_3 | |
dotnet-runtime-6.0-debuginfo | 6.0.26-1.el9_3 | |
dotnet-runtime-7.0-debuginfo | 7.0.15-1.el9_3 | |
dotnet-runtime-8.0-debuginfo | 8.0.1-1.el9_3 | |
dotnet-sdk-6.0-debuginfo | 6.0.126-1.el9_3 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.126-1.el9_3 | |
dotnet-sdk-7.0-debuginfo | 7.0.115-1.el9_3 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.115-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-8.0-debuginfo | 8.0.101-1.el9_3 | |
dotnet-sdk-8.0-source-built-artifacts | 8.0.101-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet6.0-debuginfo | 6.0.126-1.el9_3 | |
dotnet6.0-debugsource | 6.0.126-1.el9_3 | |
dotnet7.0-debuginfo | 7.0.115-1.el9_3 | |
dotnet7.0-debugsource | 7.0.115-1.el9_3 | |
dotnet8.0-debuginfo | 8.0.101-1.el9_3 | |
dotnet8.0-debugsource | 8.0.101-1.el9_3 | |
python3-ipatests | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.26-1.el9_3 | |
aspnetcore-runtime-7.0 | 7.0.15-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-runtime-8.0 | 8.0.1-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-targeting-pack-6.0 | 6.0.26-1.el9_3 | |
aspnetcore-targeting-pack-7.0 | 7.0.15-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-targeting-pack-8.0 | 8.0.1-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-apphost-pack-6.0 | 6.0.26-1.el9_3 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.26-1.el9_3 | |
dotnet-apphost-pack-7.0 | 7.0.15-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-apphost-pack-7.0-debuginfo | 7.0.15-1.el9_3 | |
dotnet-apphost-pack-8.0 | 8.0.1-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-apphost-pack-8.0-debuginfo | 8.0.1-1.el9_3 | |
dotnet-host | 8.0.1-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-host-debuginfo | 8.0.1-1.el9_3 | |
dotnet-hostfxr-6.0 | 6.0.26-1.el9_3 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.26-1.el9_3 | |
dotnet-hostfxr-7.0 | 7.0.15-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-hostfxr-7.0-debuginfo | 7.0.15-1.el9_3 | |
dotnet-hostfxr-8.0 | 8.0.1-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-hostfxr-8.0-debuginfo | 8.0.1-1.el9_3 | |
dotnet-runtime-6.0 | 6.0.26-1.el9_3 | |
dotnet-runtime-6.0-debuginfo | 6.0.26-1.el9_3 | |
dotnet-runtime-7.0 | 7.0.15-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-runtime-7.0-debuginfo | 7.0.15-1.el9_3 | |
dotnet-runtime-8.0 | 8.0.1-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-runtime-8.0-debuginfo | 8.0.1-1.el9_3 | |
dotnet-sdk-6.0 | 6.0.126-1.el9_3 | |
dotnet-sdk-6.0-debuginfo | 6.0.126-1.el9_3 | |
dotnet-sdk-7.0 | 7.0.115-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-7.0-debuginfo | 7.0.115-1.el9_3 | |
dotnet-sdk-8.0 | 8.0.101-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-8.0-debuginfo | 8.0.101-1.el9_3 | |
dotnet-targeting-pack-6.0 | 6.0.26-1.el9_3 | |
dotnet-targeting-pack-7.0 | 7.0.15-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-targeting-pack-8.0 | 8.0.1-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-templates-6.0 | 6.0.126-1.el9_3 | |
dotnet-templates-7.0 | 7.0.115-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-templates-8.0 | 8.0.101-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet6.0-debuginfo | 6.0.126-1.el9_3 | |
dotnet6.0-debugsource | 6.0.126-1.el9_3 | |
dotnet7.0-debuginfo | 7.0.115-1.el9_3 | |
dotnet7.0-debugsource | 7.0.115-1.el9_3 | |
dotnet8.0-debuginfo | 8.0.101-1.el9_3 | |
dotnet8.0-debugsource | 8.0.101-1.el9_3 | |
ipa-client | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-client-common | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-client-debuginfo | 4.10.2-5.el9_3 | |
ipa-client-epn | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-client-samba | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-common | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-debuginfo | 4.10.2-5.el9_3 | |
ipa-debugsource | 4.10.2-5.el9_3 | |
ipa-selinux | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-server | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-server-common | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-server-debuginfo | 4.10.2-5.el9_3 | |
ipa-server-dns | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-server-trust-ad | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
ipa-server-trust-ad-debuginfo | 4.10.2-5.el9_3 | |
netstandard-targeting-pack-2.1 | 8.0.101-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
nspr | 4.35.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nspr-debuginfo | 4.35.0-4.el9_3 | |
nspr-devel | 4.35.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-debuginfo | 3.90.0-4.el9_3 | |
nss-debugsource | 3.90.0-4.el9_3 | |
nss-devel | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn-debuginfo | 3.90.0-4.el9_3 | |
nss-softokn-devel | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn-freebl | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn-freebl-debuginfo | 3.90.0-4.el9_3 | |
nss-softokn-freebl-devel | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-sysinit | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-sysinit-debuginfo | 3.90.0-4.el9_3 | |
nss-tools | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-tools-debuginfo | 3.90.0-4.el9_3 | |
nss-util | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-util-debuginfo | 3.90.0-4.el9_3 | |
nss-util-devel | 3.90.0-4.el9_3 | [RHSA-2024:0108](https://access.redhat.com/errata/RHSA-2024:0108) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
python3-ipaclient | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
python3-ipalib | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))
python3-ipaserver | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.26-1.el9_3 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.15-1.el9_3 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.1-1.el9_3 | |
dotnet-host-debuginfo | 8.0.1-1.el9_3 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.26-1.el9_3 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.15-1.el9_3 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.1-1.el9_3 | |
dotnet-runtime-6.0-debuginfo | 6.0.26-1.el9_3 | |
dotnet-runtime-7.0-debuginfo | 7.0.15-1.el9_3 | |
dotnet-runtime-8.0-debuginfo | 8.0.1-1.el9_3 | |
dotnet-sdk-6.0-debuginfo | 6.0.126-1.el9_3 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.126-1.el9_3 | |
dotnet-sdk-7.0-debuginfo | 7.0.115-1.el9_3 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.115-1.el9_3 | [RHSA-2024:0151](https://access.redhat.com/errata/RHSA-2024:0151) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-8.0-debuginfo | 8.0.101-1.el9_3 | |
dotnet-sdk-8.0-source-built-artifacts | 8.0.101-1.el9_3 | [RHSA-2024:0152](https://access.redhat.com/errata/RHSA-2024:0152) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet6.0-debuginfo | 6.0.126-1.el9_3 | |
dotnet6.0-debugsource | 6.0.126-1.el9_3 | |
dotnet7.0-debuginfo | 7.0.115-1.el9_3 | |
dotnet7.0-debugsource | 7.0.115-1.el9_3 | |
dotnet8.0-debuginfo | 8.0.101-1.el9_3 | |
dotnet8.0-debugsource | 8.0.101-1.el9_3 | |
python3-ipatests | 4.10.2-5.el9_3 | [RHSA-2024:0141](https://access.redhat.com/errata/RHSA-2024:0141) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5455](https://access.redhat.com/security/cve/CVE-2023-5455))

