## 2024-04-26

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
buildah | 1.31.5-1.el9_3 | [RHSA-2024:2055](https://access.redhat.com/errata/RHSA-2024:2055) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1753](https://access.redhat.com/security/cve/CVE-2024-1753))
buildah-debuginfo | 1.31.5-1.el9_3 | |
buildah-debugsource | 1.31.5-1.el9_3 | |
buildah-tests | 1.31.5-1.el9_3 | [RHSA-2024:2055](https://access.redhat.com/errata/RHSA-2024:2055) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1753](https://access.redhat.com/security/cve/CVE-2024-1753))
buildah-tests-debuginfo | 1.31.5-1.el9_3 | |
nspr | 4.35.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nspr-debuginfo | 4.35.0-7.el9_3 | |
nspr-devel | 4.35.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-debuginfo | 3.90.0-7.el9_3 | |
nss-debugsource | 3.90.0-7.el9_3 | |
nss-devel | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-debuginfo | 3.90.0-7.el9_3 | |
nss-softokn-devel | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-freebl | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-freebl-debuginfo | 3.90.0-7.el9_3 | |
nss-softokn-freebl-devel | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-sysinit | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-sysinit-debuginfo | 3.90.0-7.el9_3 | |
nss-tools | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-tools-debuginfo | 3.90.0-7.el9_3 | |
nss-util | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-util-debuginfo | 3.90.0-7.el9_3 | |
nss-util-devel | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
openscap | 1.3.10-2.el9_3 | [RHBA-2024:2056](https://access.redhat.com/errata/RHBA-2024:2056) | <div class="adv_b">Bug Fix Advisory</div>
openscap-debuginfo | 1.3.10-2.el9_3 | |
openscap-debugsource | 1.3.10-2.el9_3 | |
openscap-devel | 1.3.10-2.el9_3 | [RHBA-2024:2056](https://access.redhat.com/errata/RHBA-2024:2056) | <div class="adv_b">Bug Fix Advisory</div>
openscap-engine-sce | 1.3.10-2.el9_3 | [RHBA-2024:2056](https://access.redhat.com/errata/RHBA-2024:2056) | <div class="adv_b">Bug Fix Advisory</div>
openscap-engine-sce-debuginfo | 1.3.10-2.el9_3 | |
openscap-python3 | 1.3.10-2.el9_3 | [RHBA-2024:2056](https://access.redhat.com/errata/RHBA-2024:2056) | <div class="adv_b">Bug Fix Advisory</div>
openscap-python3-debuginfo | 1.3.10-2.el9_3 | |
openscap-scanner | 1.3.10-2.el9_3 | [RHBA-2024:2056](https://access.redhat.com/errata/RHBA-2024:2056) | <div class="adv_b">Bug Fix Advisory</div>
openscap-scanner-debuginfo | 1.3.10-2.el9_3 | |
openscap-utils | 1.3.10-2.el9_3 | [RHBA-2024:2056](https://access.redhat.com/errata/RHBA-2024:2056) | <div class="adv_b">Bug Fix Advisory</div>

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openscap-debuginfo | 1.3.10-2.el9_3 | |
openscap-debugsource | 1.3.10-2.el9_3 | |
openscap-engine-sce-debuginfo | 1.3.10-2.el9_3 | |
openscap-engine-sce-devel | 1.3.10-2.el9_3 | [RHBA-2024:2056](https://access.redhat.com/errata/RHBA-2024:2056) | <div class="adv_b">Bug Fix Advisory</div>
openscap-python3-debuginfo | 1.3.10-2.el9_3 | |
openscap-scanner-debuginfo | 1.3.10-2.el9_3 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
buildah | 1.31.5-1.el9_3 | [RHSA-2024:2055](https://access.redhat.com/errata/RHSA-2024:2055) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1753](https://access.redhat.com/security/cve/CVE-2024-1753))
buildah-debuginfo | 1.31.5-1.el9_3 | |
buildah-debugsource | 1.31.5-1.el9_3 | |
buildah-tests | 1.31.5-1.el9_3 | [RHSA-2024:2055](https://access.redhat.com/errata/RHSA-2024:2055) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1753](https://access.redhat.com/security/cve/CVE-2024-1753))
buildah-tests-debuginfo | 1.31.5-1.el9_3 | |
nspr | 4.35.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nspr-debuginfo | 4.35.0-7.el9_3 | |
nspr-devel | 4.35.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-debuginfo | 3.90.0-7.el9_3 | |
nss-debugsource | 3.90.0-7.el9_3 | |
nss-devel | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-debuginfo | 3.90.0-7.el9_3 | |
nss-softokn-devel | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-freebl | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-freebl-debuginfo | 3.90.0-7.el9_3 | |
nss-softokn-freebl-devel | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-sysinit | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-sysinit-debuginfo | 3.90.0-7.el9_3 | |
nss-tools | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-tools-debuginfo | 3.90.0-7.el9_3 | |
nss-util | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
nss-util-debuginfo | 3.90.0-7.el9_3 | |
nss-util-devel | 3.90.0-7.el9_3 | [RHBA-2024:2058](https://access.redhat.com/errata/RHBA-2024:2058) | <div class="adv_b">Bug Fix Advisory</div>
openscap | 1.3.10-2.el9_3 | [RHBA-2024:2056](https://access.redhat.com/errata/RHBA-2024:2056) | <div class="adv_b">Bug Fix Advisory</div>
openscap-debuginfo | 1.3.10-2.el9_3 | |
openscap-debugsource | 1.3.10-2.el9_3 | |
openscap-devel | 1.3.10-2.el9_3 | [RHBA-2024:2056](https://access.redhat.com/errata/RHBA-2024:2056) | <div class="adv_b">Bug Fix Advisory</div>
openscap-engine-sce | 1.3.10-2.el9_3 | [RHBA-2024:2056](https://access.redhat.com/errata/RHBA-2024:2056) | <div class="adv_b">Bug Fix Advisory</div>
openscap-engine-sce-debuginfo | 1.3.10-2.el9_3 | |
openscap-python3 | 1.3.10-2.el9_3 | [RHBA-2024:2056](https://access.redhat.com/errata/RHBA-2024:2056) | <div class="adv_b">Bug Fix Advisory</div>
openscap-python3-debuginfo | 1.3.10-2.el9_3 | |
openscap-scanner | 1.3.10-2.el9_3 | [RHBA-2024:2056](https://access.redhat.com/errata/RHBA-2024:2056) | <div class="adv_b">Bug Fix Advisory</div>
openscap-scanner-debuginfo | 1.3.10-2.el9_3 | |
openscap-utils | 1.3.10-2.el9_3 | [RHBA-2024:2056](https://access.redhat.com/errata/RHBA-2024:2056) | <div class="adv_b">Bug Fix Advisory</div>

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openscap-debuginfo | 1.3.10-2.el9_3 | |
openscap-debugsource | 1.3.10-2.el9_3 | |
openscap-engine-sce-debuginfo | 1.3.10-2.el9_3 | |
openscap-engine-sce-devel | 1.3.10-2.el9_3 | [RHBA-2024:2056](https://access.redhat.com/errata/RHBA-2024:2056) | <div class="adv_b">Bug Fix Advisory</div>
openscap-python3-debuginfo | 1.3.10-2.el9_3 | |
openscap-scanner-debuginfo | 1.3.10-2.el9_3 | |

