## 2024-03-22

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
go-toolset | 1.20.12-2.el9_3 | [RHSA-2024:1462](https://access.redhat.com/errata/RHSA-2024:1462) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))
golang | 1.20.12-2.el9_3 | [RHSA-2024:1462](https://access.redhat.com/errata/RHSA-2024:1462) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))
golang-bin | 1.20.12-2.el9_3 | [RHSA-2024:1462](https://access.redhat.com/errata/RHSA-2024:1462) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))
golang-docs | 1.20.12-2.el9_3 | [RHSA-2024:1462](https://access.redhat.com/errata/RHSA-2024:1462) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))
golang-misc | 1.20.12-2.el9_3 | [RHSA-2024:1462](https://access.redhat.com/errata/RHSA-2024:1462) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))
golang-src | 1.20.12-2.el9_3 | [RHSA-2024:1462](https://access.redhat.com/errata/RHSA-2024:1462) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))
golang-tests | 1.20.12-2.el9_3 | [RHSA-2024:1462](https://access.redhat.com/errata/RHSA-2024:1462) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))

