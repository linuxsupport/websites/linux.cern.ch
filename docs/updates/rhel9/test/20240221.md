## 2024-02-21

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lpadmincern | 1.4.5-1.rh9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lpadmincern | 1.4.5-1.rh9.cern | |

