## 2025-03-13

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libxml2 | 2.9.13-6.el9_5.2 | [RHSA-2025:2679](https://access.redhat.com/errata/RHSA-2025:2679) | <div class="adv_s">Security Advisory</div> ([CVE-2024-56171](https://access.redhat.com/security/cve/CVE-2024-56171), [CVE-2025-24928](https://access.redhat.com/security/cve/CVE-2025-24928))
libxml2-debuginfo | 2.9.13-6.el9_5.2 | |
libxml2-debugsource | 2.9.13-6.el9_5.2 | |
python3-libxml2 | 2.9.13-6.el9_5.2 | [RHSA-2025:2679](https://access.redhat.com/errata/RHSA-2025:2679) | <div class="adv_s">Security Advisory</div> ([CVE-2024-56171](https://access.redhat.com/security/cve/CVE-2024-56171), [CVE-2025-24928](https://access.redhat.com/security/cve/CVE-2025-24928))
python3-libxml2-debuginfo | 2.9.13-6.el9_5.2 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libxml2-debuginfo | 2.9.13-6.el9_5.2 | |
libxml2-debugsource | 2.9.13-6.el9_5.2 | |
libxml2-devel | 2.9.13-6.el9_5.2 | [RHSA-2025:2679](https://access.redhat.com/errata/RHSA-2025:2679) | <div class="adv_s">Security Advisory</div> ([CVE-2024-56171](https://access.redhat.com/security/cve/CVE-2024-56171), [CVE-2025-24928](https://access.redhat.com/security/cve/CVE-2025-24928))
python3-libxml2-debuginfo | 2.9.13-6.el9_5.2 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libxml2 | 2.9.13-6.el9_5.2 | [RHSA-2025:2679](https://access.redhat.com/errata/RHSA-2025:2679) | <div class="adv_s">Security Advisory</div> ([CVE-2024-56171](https://access.redhat.com/security/cve/CVE-2024-56171), [CVE-2025-24928](https://access.redhat.com/security/cve/CVE-2025-24928))
libxml2-debuginfo | 2.9.13-6.el9_5.2 | |
libxml2-debugsource | 2.9.13-6.el9_5.2 | |
python3-libxml2 | 2.9.13-6.el9_5.2 | [RHSA-2025:2679](https://access.redhat.com/errata/RHSA-2025:2679) | <div class="adv_s">Security Advisory</div> ([CVE-2024-56171](https://access.redhat.com/security/cve/CVE-2024-56171), [CVE-2025-24928](https://access.redhat.com/security/cve/CVE-2025-24928))
python3-libxml2-debuginfo | 2.9.13-6.el9_5.2 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libxml2-debuginfo | 2.9.13-6.el9_5.2 | |
libxml2-debugsource | 2.9.13-6.el9_5.2 | |
libxml2-devel | 2.9.13-6.el9_5.2 | [RHSA-2025:2679](https://access.redhat.com/errata/RHSA-2025:2679) | <div class="adv_s">Security Advisory</div> ([CVE-2024-56171](https://access.redhat.com/security/cve/CVE-2024-56171), [CVE-2025-24928](https://access.redhat.com/security/cve/CVE-2025-24928))
python3-libxml2-debuginfo | 2.9.13-6.el9_5.2 | |

