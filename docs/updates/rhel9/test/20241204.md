## 2024-12-04

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rhc | 0.2.5-1.el9_5 | [RHSA-2024:10759](https://access.redhat.com/errata/RHSA-2024:10759) | <div class="adv_s">Security Advisory</div> ([CVE-2022-3064](https://access.redhat.com/security/cve/CVE-2022-3064))
rhc-debuginfo | 0.2.5-1.el9_5 | |
rhc-debugsource | 0.2.5-1.el9_5 | |
rhc-worker-playbook | 0.1.10-1.el9_5 | [RHSA-2024:10761](https://access.redhat.com/errata/RHSA-2024:10761) | <div class="adv_s">Security Advisory</div> ([CVE-2022-40898](https://access.redhat.com/security/cve/CVE-2022-40898), [CVE-2023-1428](https://access.redhat.com/security/cve/CVE-2023-1428), [CVE-2023-32731](https://access.redhat.com/security/cve/CVE-2023-32731), [CVE-2023-33953](https://access.redhat.com/security/cve/CVE-2023-33953))
rhc-worker-playbook-debuginfo | 0.1.10-1.el9_5 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rhc-debuginfo | 0.2.5-1.el9_5 | |
rhc-debugsource | 0.2.5-1.el9_5 | |
rhc-devel | 0.2.5-1.el9_5 | [RHSA-2024:10759](https://access.redhat.com/errata/RHSA-2024:10759) | <div class="adv_s">Security Advisory</div> ([CVE-2022-3064](https://access.redhat.com/security/cve/CVE-2022-3064))

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rhc | 0.2.5-1.el9_5 | [RHSA-2024:10759](https://access.redhat.com/errata/RHSA-2024:10759) | <div class="adv_s">Security Advisory</div> ([CVE-2022-3064](https://access.redhat.com/security/cve/CVE-2022-3064))
rhc-debuginfo | 0.2.5-1.el9_5 | |
rhc-debugsource | 0.2.5-1.el9_5 | |
rhc-worker-playbook | 0.1.10-1.el9_5 | [RHSA-2024:10761](https://access.redhat.com/errata/RHSA-2024:10761) | <div class="adv_s">Security Advisory</div> ([CVE-2022-40898](https://access.redhat.com/security/cve/CVE-2022-40898), [CVE-2023-1428](https://access.redhat.com/security/cve/CVE-2023-1428), [CVE-2023-32731](https://access.redhat.com/security/cve/CVE-2023-32731), [CVE-2023-33953](https://access.redhat.com/security/cve/CVE-2023-33953))
rhc-worker-playbook-debuginfo | 0.1.10-1.el9_5 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rhc-debuginfo | 0.2.5-1.el9_5 | |
rhc-debugsource | 0.2.5-1.el9_5 | |
rhc-devel | 0.2.5-1.el9_5 | [RHSA-2024:10759](https://access.redhat.com/errata/RHSA-2024:10759) | <div class="adv_s">Security Advisory</div> ([CVE-2022-3064](https://access.redhat.com/security/cve/CVE-2022-3064))

