## 2023-09-07

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20220809-2.20230808.2.el9_2 | [RHEA-2023:4998](https://access.redhat.com/errata/RHEA-2023:4998) | <div class="adv_e">Product Enhancement Advisory</div>

