## 2024-01-09

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.5-1.rh9.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2023d-1.el9 | [RHBA-2024:0076](https://access.redhat.com/errata/RHBA-2024:0076) | <div class="adv_b">Bug Fix Advisory</div>

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
squid | 5.5-6.el9_3.5 | [RHSA-2024:0071](https://access.redhat.com/errata/RHSA-2024:0071) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46724](https://access.redhat.com/security/cve/CVE-2023-46724), [CVE-2023-46728](https://access.redhat.com/security/cve/CVE-2023-46728), [CVE-2023-49285](https://access.redhat.com/security/cve/CVE-2023-49285), [CVE-2023-49286](https://access.redhat.com/security/cve/CVE-2023-49286))
squid-debuginfo | 5.5-6.el9_3.5 | |
squid-debugsource | 5.5-6.el9_3.5 | |
tzdata-java | 2023d-1.el9 | [RHBA-2024:0076](https://access.redhat.com/errata/RHBA-2024:0076) | <div class="adv_b">Bug Fix Advisory</div>

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.5-1.rh9.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2023d-1.el9 | [RHBA-2024:0076](https://access.redhat.com/errata/RHBA-2024:0076) | <div class="adv_b">Bug Fix Advisory</div>

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
squid | 5.5-6.el9_3.5 | [RHSA-2024:0071](https://access.redhat.com/errata/RHSA-2024:0071) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46724](https://access.redhat.com/security/cve/CVE-2023-46724), [CVE-2023-46728](https://access.redhat.com/security/cve/CVE-2023-46728), [CVE-2023-49285](https://access.redhat.com/security/cve/CVE-2023-49285), [CVE-2023-49286](https://access.redhat.com/security/cve/CVE-2023-49286))
squid-debuginfo | 5.5-6.el9_3.5 | |
squid-debugsource | 5.5-6.el9_3.5 | |
tzdata-java | 2023d-1.el9 | [RHBA-2024:0076](https://access.redhat.com/errata/RHBA-2024:0076) | <div class="adv_b">Bug Fix Advisory</div>

