## 2023-08-09

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dbus | 1.12.20-7.el9_2.1 | [RHSA-2023:4569](https://access.redhat.com/errata/RHSA-2023:4569) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34969](https://access.redhat.com/security/cve/CVE-2023-34969))
dbus-common | 1.12.20-7.el9_2.1 | [RHSA-2023:4569](https://access.redhat.com/errata/RHSA-2023:4569) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34969](https://access.redhat.com/security/cve/CVE-2023-34969))
dbus-daemon-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-debugsource | 1.12.20-7.el9_2.1 | |
dbus-libs | 1.12.20-7.el9_2.1 | [RHSA-2023:4569](https://access.redhat.com/errata/RHSA-2023:4569) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34969](https://access.redhat.com/security/cve/CVE-2023-34969))
dbus-libs-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-tests-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-tools | 1.12.20-7.el9_2.1 | [RHSA-2023:4569](https://access.redhat.com/errata/RHSA-2023:4569) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34969](https://access.redhat.com/security/cve/CVE-2023-34969))
dbus-tools-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-x11-debuginfo | 1.12.20-7.el9_2.1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dbus-daemon | 1.12.20-7.el9_2.1 | [RHSA-2023:4569](https://access.redhat.com/errata/RHSA-2023:4569) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34969](https://access.redhat.com/security/cve/CVE-2023-34969))
dbus-daemon-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-debugsource | 1.12.20-7.el9_2.1 | |
dbus-devel | 1.12.20-7.el9_2.1 | [RHSA-2023:4569](https://access.redhat.com/errata/RHSA-2023:4569) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34969](https://access.redhat.com/security/cve/CVE-2023-34969))
dbus-libs-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-tests-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-tools-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-x11 | 1.12.20-7.el9_2.1 | [RHSA-2023:4569](https://access.redhat.com/errata/RHSA-2023:4569) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34969](https://access.redhat.com/security/cve/CVE-2023-34969))
dbus-x11-debuginfo | 1.12.20-7.el9_2.1 | |
iperf3 | 3.9-10.el9_2 | [RHSA-2023:4571](https://access.redhat.com/errata/RHSA-2023:4571) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38403](https://access.redhat.com/security/cve/CVE-2023-38403))
iperf3-debuginfo | 3.9-10.el9_2 | |
iperf3-debugsource | 3.9-10.el9_2 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dbus | 1.12.20-7.el9_2.1 | [RHSA-2023:4569](https://access.redhat.com/errata/RHSA-2023:4569) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34969](https://access.redhat.com/security/cve/CVE-2023-34969))
dbus-common | 1.12.20-7.el9_2.1 | [RHSA-2023:4569](https://access.redhat.com/errata/RHSA-2023:4569) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34969](https://access.redhat.com/security/cve/CVE-2023-34969))
dbus-daemon-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-debugsource | 1.12.20-7.el9_2.1 | |
dbus-libs | 1.12.20-7.el9_2.1 | [RHSA-2023:4569](https://access.redhat.com/errata/RHSA-2023:4569) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34969](https://access.redhat.com/security/cve/CVE-2023-34969))
dbus-libs-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-tests-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-tools | 1.12.20-7.el9_2.1 | [RHSA-2023:4569](https://access.redhat.com/errata/RHSA-2023:4569) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34969](https://access.redhat.com/security/cve/CVE-2023-34969))
dbus-tools-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-x11-debuginfo | 1.12.20-7.el9_2.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dbus-daemon | 1.12.20-7.el9_2.1 | [RHSA-2023:4569](https://access.redhat.com/errata/RHSA-2023:4569) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34969](https://access.redhat.com/security/cve/CVE-2023-34969))
dbus-daemon-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-debugsource | 1.12.20-7.el9_2.1 | |
dbus-devel | 1.12.20-7.el9_2.1 | [RHSA-2023:4569](https://access.redhat.com/errata/RHSA-2023:4569) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34969](https://access.redhat.com/security/cve/CVE-2023-34969))
dbus-libs-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-tests-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-tools-debuginfo | 1.12.20-7.el9_2.1 | |
dbus-x11 | 1.12.20-7.el9_2.1 | [RHSA-2023:4569](https://access.redhat.com/errata/RHSA-2023:4569) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34969](https://access.redhat.com/security/cve/CVE-2023-34969))
dbus-x11-debuginfo | 1.12.20-7.el9_2.1 | |
iperf3 | 3.9-10.el9_2 | [RHSA-2023:4571](https://access.redhat.com/errata/RHSA-2023:4571) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38403](https://access.redhat.com/security/cve/CVE-2023-38403))
iperf3-debuginfo | 3.9-10.el9_2 | |
iperf3-debugsource | 3.9-10.el9_2 | |

