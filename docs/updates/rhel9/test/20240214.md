## 2024-02-14

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sudo | 1.9.5p2-10.el9_3 | |
sudo-debuginfo | 1.9.5p2-10.el9_3 | |
sudo-debugsource | 1.9.5p2-10.el9_3 | |
sudo-python-plugin-debuginfo | 1.9.5p2-10.el9_3 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.27-1.el9_3 | |
aspnetcore-runtime-7.0 | 7.0.16-1.el9_3 | |
aspnetcore-targeting-pack-6.0 | 6.0.27-1.el9_3 | |
aspnetcore-targeting-pack-7.0 | 7.0.16-1.el9_3 | |
dotnet-apphost-pack-6.0 | 6.0.27-1.el9_3 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-apphost-pack-7.0 | 7.0.16-1.el9_3 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-hostfxr-6.0 | 6.0.27-1.el9_3 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-hostfxr-7.0 | 7.0.16-1.el9_3 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-runtime-6.0 | 6.0.27-1.el9_3 | |
dotnet-runtime-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-runtime-7.0 | 7.0.16-1.el9_3 | |
dotnet-runtime-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-sdk-6.0 | 6.0.127-1.el9_3 | |
dotnet-sdk-6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet-sdk-7.0 | 7.0.116-1.el9_3 | |
dotnet-sdk-7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet-targeting-pack-6.0 | 6.0.27-1.el9_3 | |
dotnet-targeting-pack-7.0 | 7.0.16-1.el9_3 | |
dotnet-templates-6.0 | 6.0.127-1.el9_3 | |
dotnet-templates-7.0 | 7.0.116-1.el9_3 | |
dotnet6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet6.0-debugsource | 6.0.127-1.el9_3 | |
dotnet7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet7.0-debugsource | 7.0.116-1.el9_3 | |
sudo-debuginfo | 1.9.5p2-10.el9_3 | |
sudo-debugsource | 1.9.5p2-10.el9_3 | |
sudo-python-plugin | 1.9.5p2-10.el9_3 | |
sudo-python-plugin-debuginfo | 1.9.5p2-10.el9_3 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-runtime-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-runtime-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-sdk-6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.127-1.el9_3 | |
dotnet-sdk-7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.116-1.el9_3 | |
dotnet6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet6.0-debugsource | 6.0.127-1.el9_3 | |
dotnet7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet7.0-debugsource | 7.0.116-1.el9_3 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sudo | 1.9.5p2-10.el9_3 | |
sudo-debuginfo | 1.9.5p2-10.el9_3 | |
sudo-debugsource | 1.9.5p2-10.el9_3 | |
sudo-python-plugin-debuginfo | 1.9.5p2-10.el9_3 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.27-1.el9_3 | |
aspnetcore-runtime-7.0 | 7.0.16-1.el9_3 | |
aspnetcore-targeting-pack-6.0 | 6.0.27-1.el9_3 | |
aspnetcore-targeting-pack-7.0 | 7.0.16-1.el9_3 | |
dotnet-apphost-pack-6.0 | 6.0.27-1.el9_3 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-apphost-pack-7.0 | 7.0.16-1.el9_3 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-hostfxr-6.0 | 6.0.27-1.el9_3 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-hostfxr-7.0 | 7.0.16-1.el9_3 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-runtime-6.0 | 6.0.27-1.el9_3 | |
dotnet-runtime-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-runtime-7.0 | 7.0.16-1.el9_3 | |
dotnet-runtime-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-sdk-6.0 | 6.0.127-1.el9_3 | |
dotnet-sdk-6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet-sdk-7.0 | 7.0.116-1.el9_3 | |
dotnet-sdk-7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet-targeting-pack-6.0 | 6.0.27-1.el9_3 | |
dotnet-targeting-pack-7.0 | 7.0.16-1.el9_3 | |
dotnet-templates-6.0 | 6.0.127-1.el9_3 | |
dotnet-templates-7.0 | 7.0.116-1.el9_3 | |
dotnet6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet6.0-debugsource | 6.0.127-1.el9_3 | |
dotnet7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet7.0-debugsource | 7.0.116-1.el9_3 | |
sudo-debuginfo | 1.9.5p2-10.el9_3 | |
sudo-debugsource | 1.9.5p2-10.el9_3 | |
sudo-python-plugin | 1.9.5p2-10.el9_3 | |
sudo-python-plugin-debuginfo | 1.9.5p2-10.el9_3 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-runtime-6.0-debuginfo | 6.0.27-1.el9_3 | |
dotnet-runtime-7.0-debuginfo | 7.0.16-1.el9_3 | |
dotnet-sdk-6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.127-1.el9_3 | |
dotnet-sdk-7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.116-1.el9_3 | |
dotnet6.0-debuginfo | 6.0.127-1.el9_3 | |
dotnet6.0-debugsource | 6.0.127-1.el9_3 | |
dotnet7.0-debuginfo | 7.0.116-1.el9_3 | |
dotnet7.0-debugsource | 7.0.116-1.el9_3 | |

