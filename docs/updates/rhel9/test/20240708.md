## 2024-07-08

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.5.14.0_427.24.1.el9_4.rh9.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.3.0-427.24.1.el9_4 | |
bpftool-debuginfo | 7.3.0-427.24.1.el9_4 | |
kernel | 5.14.0-427.24.1.el9_4 | |
kernel-abi-stablelists | 5.14.0-427.24.1.el9_4 | |
kernel-core | 5.14.0-427.24.1.el9_4 | |
kernel-debug | 5.14.0-427.24.1.el9_4 | |
kernel-debug-core | 5.14.0-427.24.1.el9_4 | |
kernel-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-debug-modules | 5.14.0-427.24.1.el9_4 | |
kernel-debug-modules-core | 5.14.0-427.24.1.el9_4 | |
kernel-debug-modules-extra | 5.14.0-427.24.1.el9_4 | |
kernel-debug-uki-virt | 5.14.0-427.24.1.el9_4 | |
kernel-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-debuginfo-common-x86_64 | 5.14.0-427.24.1.el9_4 | |
kernel-modules | 5.14.0-427.24.1.el9_4 | |
kernel-modules-core | 5.14.0-427.24.1.el9_4 | |
kernel-modules-extra | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-tools | 5.14.0-427.24.1.el9_4 | |
kernel-tools-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-tools-libs | 5.14.0-427.24.1.el9_4 | |
kernel-uki-virt | 5.14.0-427.24.1.el9_4 | |
libperf-debuginfo | 5.14.0-427.24.1.el9_4 | |
perf-debuginfo | 5.14.0-427.24.1.el9_4 | |
python3-perf | 5.14.0-427.24.1.el9_4 | |
python3-perf-debuginfo | 5.14.0-427.24.1.el9_4 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 7.3.0-427.24.1.el9_4 | |
kernel-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-debug-devel | 5.14.0-427.24.1.el9_4 | |
kernel-debug-devel-matched | 5.14.0-427.24.1.el9_4 | |
kernel-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-debuginfo-common-x86_64 | 5.14.0-427.24.1.el9_4 | |
kernel-devel | 5.14.0-427.24.1.el9_4 | |
kernel-devel-matched | 5.14.0-427.24.1.el9_4 | |
kernel-doc | 5.14.0-427.24.1.el9_4 | |
kernel-headers | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-tools-debuginfo | 5.14.0-427.24.1.el9_4 | |
libperf-debuginfo | 5.14.0-427.24.1.el9_4 | |
perf | 5.14.0-427.24.1.el9_4 | |
perf-debuginfo | 5.14.0-427.24.1.el9_4 | |
python3-perf-debuginfo | 5.14.0-427.24.1.el9_4 | |
rtla | 5.14.0-427.24.1.el9_4 | |
rv | 5.14.0-427.24.1.el9_4 | |

### rt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 7.3.0-427.24.1.el9_4 | |
kernel-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-debuginfo-common-x86_64 | 5.14.0-427.24.1.el9_4 | |
kernel-rt | 5.14.0-427.24.1.el9_4 | |
kernel-rt-core | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debug | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debug-core | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debug-devel | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debug-modules | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debug-modules-core | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debug-modules-extra | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-rt-devel | 5.14.0-427.24.1.el9_4 | |
kernel-rt-modules | 5.14.0-427.24.1.el9_4 | |
kernel-rt-modules-core | 5.14.0-427.24.1.el9_4 | |
kernel-rt-modules-extra | 5.14.0-427.24.1.el9_4 | |
kernel-tools-debuginfo | 5.14.0-427.24.1.el9_4 | |
libperf-debuginfo | 5.14.0-427.24.1.el9_4 | |
perf-debuginfo | 5.14.0-427.24.1.el9_4 | |
python3-perf-debuginfo | 5.14.0-427.24.1.el9_4 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 7.3.0-427.24.1.el9_4 | |
kernel-cross-headers | 5.14.0-427.24.1.el9_4 | |
kernel-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-debuginfo-common-x86_64 | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-tools-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-tools-libs-devel | 5.14.0-427.24.1.el9_4 | |
libperf | 5.14.0-427.24.1.el9_4 | |
libperf-debuginfo | 5.14.0-427.24.1.el9_4 | |
perf-debuginfo | 5.14.0-427.24.1.el9_4 | |
python3-perf-debuginfo | 5.14.0-427.24.1.el9_4 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.5.14.0_427.24.1.el9_4.rh9.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.3.0-427.24.1.el9_4 | |
bpftool-debuginfo | 7.3.0-427.24.1.el9_4 | |
kernel | 5.14.0-427.24.1.el9_4 | |
kernel-64k | 5.14.0-427.24.1.el9_4 | |
kernel-64k-core | 5.14.0-427.24.1.el9_4 | |
kernel-64k-debug | 5.14.0-427.24.1.el9_4 | |
kernel-64k-debug-core | 5.14.0-427.24.1.el9_4 | |
kernel-64k-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-64k-debug-modules | 5.14.0-427.24.1.el9_4 | |
kernel-64k-debug-modules-core | 5.14.0-427.24.1.el9_4 | |
kernel-64k-debug-modules-extra | 5.14.0-427.24.1.el9_4 | |
kernel-64k-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-64k-modules | 5.14.0-427.24.1.el9_4 | |
kernel-64k-modules-core | 5.14.0-427.24.1.el9_4 | |
kernel-64k-modules-extra | 5.14.0-427.24.1.el9_4 | |
kernel-abi-stablelists | 5.14.0-427.24.1.el9_4 | |
kernel-core | 5.14.0-427.24.1.el9_4 | |
kernel-debug | 5.14.0-427.24.1.el9_4 | |
kernel-debug-core | 5.14.0-427.24.1.el9_4 | |
kernel-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-debug-modules | 5.14.0-427.24.1.el9_4 | |
kernel-debug-modules-core | 5.14.0-427.24.1.el9_4 | |
kernel-debug-modules-extra | 5.14.0-427.24.1.el9_4 | |
kernel-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-debuginfo-common-aarch64 | 5.14.0-427.24.1.el9_4 | |
kernel-modules | 5.14.0-427.24.1.el9_4 | |
kernel-modules-core | 5.14.0-427.24.1.el9_4 | |
kernel-modules-extra | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-tools | 5.14.0-427.24.1.el9_4 | |
kernel-tools-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-tools-libs | 5.14.0-427.24.1.el9_4 | |
libperf-debuginfo | 5.14.0-427.24.1.el9_4 | |
perf-debuginfo | 5.14.0-427.24.1.el9_4 | |
python3-perf | 5.14.0-427.24.1.el9_4 | |
python3-perf-debuginfo | 5.14.0-427.24.1.el9_4 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 7.3.0-427.24.1.el9_4 | |
kernel-64k-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-64k-debug-devel | 5.14.0-427.24.1.el9_4 | |
kernel-64k-debug-devel-matched | 5.14.0-427.24.1.el9_4 | |
kernel-64k-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-64k-devel | 5.14.0-427.24.1.el9_4 | |
kernel-64k-devel-matched | 5.14.0-427.24.1.el9_4 | |
kernel-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-debug-devel | 5.14.0-427.24.1.el9_4 | |
kernel-debug-devel-matched | 5.14.0-427.24.1.el9_4 | |
kernel-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-debuginfo-common-aarch64 | 5.14.0-427.24.1.el9_4 | |
kernel-devel | 5.14.0-427.24.1.el9_4 | |
kernel-devel-matched | 5.14.0-427.24.1.el9_4 | |
kernel-doc | 5.14.0-427.24.1.el9_4 | |
kernel-headers | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-tools-debuginfo | 5.14.0-427.24.1.el9_4 | |
libperf-debuginfo | 5.14.0-427.24.1.el9_4 | |
perf | 5.14.0-427.24.1.el9_4 | |
perf-debuginfo | 5.14.0-427.24.1.el9_4 | |
python3-perf-debuginfo | 5.14.0-427.24.1.el9_4 | |
rtla | 5.14.0-427.24.1.el9_4 | |
rv | 5.14.0-427.24.1.el9_4 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 7.3.0-427.24.1.el9_4 | |
kernel-64k-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-64k-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-cross-headers | 5.14.0-427.24.1.el9_4 | |
kernel-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-debuginfo-common-aarch64 | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debug-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-rt-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-tools-debuginfo | 5.14.0-427.24.1.el9_4 | |
kernel-tools-libs-devel | 5.14.0-427.24.1.el9_4 | |
libperf | 5.14.0-427.24.1.el9_4 | |
libperf-debuginfo | 5.14.0-427.24.1.el9_4 | |
perf-debuginfo | 5.14.0-427.24.1.el9_4 | |
python3-perf-debuginfo | 5.14.0-427.24.1.el9_4 | |

