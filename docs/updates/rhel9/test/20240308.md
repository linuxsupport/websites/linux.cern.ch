## 2024-03-08

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
opencryptoki | 3.21.0-9.el9_3 | |
opencryptoki-debuginfo | 3.21.0-9.el9_3 | |
opencryptoki-debugsource | 3.21.0-9.el9_3 | |
opencryptoki-icsftok | 3.21.0-9.el9_3 | |
opencryptoki-icsftok-debuginfo | 3.21.0-9.el9_3 | |
opencryptoki-libs | 3.21.0-9.el9_3 | |
opencryptoki-libs-debuginfo | 3.21.0-9.el9_3 | |
opencryptoki-swtok | 3.21.0-9.el9_3 | |
opencryptoki-swtok-debuginfo | 3.21.0-9.el9_3 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
opencryptoki-debuginfo | 3.21.0-9.el9_3 | |
opencryptoki-debugsource | 3.21.0-9.el9_3 | |
opencryptoki-devel | 3.21.0-9.el9_3 | |
opencryptoki-icsftok-debuginfo | 3.21.0-9.el9_3 | |
opencryptoki-libs-debuginfo | 3.21.0-9.el9_3 | |
opencryptoki-swtok-debuginfo | 3.21.0-9.el9_3 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
opencryptoki | 3.21.0-9.el9_3 | |
opencryptoki-debuginfo | 3.21.0-9.el9_3 | |
opencryptoki-debugsource | 3.21.0-9.el9_3 | |
opencryptoki-icsftok | 3.21.0-9.el9_3 | |
opencryptoki-icsftok-debuginfo | 3.21.0-9.el9_3 | |
opencryptoki-libs | 3.21.0-9.el9_3 | |
opencryptoki-libs-debuginfo | 3.21.0-9.el9_3 | |
opencryptoki-swtok | 3.21.0-9.el9_3 | |
opencryptoki-swtok-debuginfo | 3.21.0-9.el9_3 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
opencryptoki-debuginfo | 3.21.0-9.el9_3 | |
opencryptoki-debugsource | 3.21.0-9.el9_3 | |
opencryptoki-devel | 3.21.0-9.el9_3 | |
opencryptoki-icsftok-debuginfo | 3.21.0-9.el9_3 | |
opencryptoki-libs-debuginfo | 3.21.0-9.el9_3 | |
opencryptoki-swtok-debuginfo | 3.21.0-9.el9_3 | |

