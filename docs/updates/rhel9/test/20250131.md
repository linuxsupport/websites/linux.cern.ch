## 2025-01-31

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-release | 9-9.rh9.cern | |
hepix | 4.10.14-0.rh9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-release | 9-9.rh9.cern | |
hepix | 4.10.14-0.rh9.cern | |

