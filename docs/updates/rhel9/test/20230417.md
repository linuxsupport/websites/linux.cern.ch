## 2023-04-17

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.10.0-1.el9_1 | |
firefox-debuginfo | 102.10.0-1.el9_1 | |
firefox-debugsource | 102.10.0-1.el9_1 | |
firefox-x11 | 102.10.0-1.el9_1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.10.0-1.el9_1 | |
firefox-debuginfo | 102.10.0-1.el9_1 | |
firefox-debugsource | 102.10.0-1.el9_1 | |
firefox-x11 | 102.10.0-1.el9_1 | |

