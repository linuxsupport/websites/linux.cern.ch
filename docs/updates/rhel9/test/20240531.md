## 2024-05-31

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
less | 590-4.el9_4 | [RHSA-2024:3513](https://access.redhat.com/errata/RHSA-2024:3513) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32487](https://access.redhat.com/security/cve/CVE-2024-32487))
less-debuginfo | 590-4.el9_4 | |
less-debugsource | 590-4.el9_4 | |
libnghttp2 | 1.43.0-5.el9_4.3 | [RHSA-2024:3501](https://access.redhat.com/errata/RHSA-2024:3501) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
libnghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
nghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
nghttp2-debugsource | 1.43.0-5.el9_4.3 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libnghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
libnghttp2-devel | 1.43.0-5.el9_4.3 | [RHSA-2024:3501](https://access.redhat.com/errata/RHSA-2024:3501) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nghttp2 | 1.43.0-5.el9_4.3 | [RHSA-2024:3501](https://access.redhat.com/errata/RHSA-2024:3501) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
nghttp2-debugsource | 1.43.0-5.el9_4.3 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
less | 590-4.el9_4 | [RHSA-2024:3513](https://access.redhat.com/errata/RHSA-2024:3513) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32487](https://access.redhat.com/security/cve/CVE-2024-32487))
less-debuginfo | 590-4.el9_4 | |
less-debugsource | 590-4.el9_4 | |
libnghttp2 | 1.43.0-5.el9_4.3 | [RHSA-2024:3501](https://access.redhat.com/errata/RHSA-2024:3501) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
libnghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
nghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
nghttp2-debugsource | 1.43.0-5.el9_4.3 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libnghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
libnghttp2-devel | 1.43.0-5.el9_4.3 | [RHSA-2024:3501](https://access.redhat.com/errata/RHSA-2024:3501) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nghttp2 | 1.43.0-5.el9_4.3 | [RHSA-2024:3501](https://access.redhat.com/errata/RHSA-2024:3501) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28182](https://access.redhat.com/security/cve/CVE-2024-28182))
nghttp2-debuginfo | 1.43.0-5.el9_4.3 | |
nghttp2-debugsource | 1.43.0-5.el9_4.3 | |

