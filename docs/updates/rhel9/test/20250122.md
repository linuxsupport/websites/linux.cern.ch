## 2025-01-22

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.5.14.0_503.22.1.el9_5.rh9.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.4.0-503.22.1.el9_5 | |
bpftool-debuginfo | 7.4.0-503.22.1.el9_5 | |
kernel | 5.14.0-503.22.1.el9_5 | |
kernel-abi-stablelists | 5.14.0-503.22.1.el9_5 | |
kernel-core | 5.14.0-503.22.1.el9_5 | |
kernel-debug | 5.14.0-503.22.1.el9_5 | |
kernel-debug-core | 5.14.0-503.22.1.el9_5 | |
kernel-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-debug-modules | 5.14.0-503.22.1.el9_5 | |
kernel-debug-modules-core | 5.14.0-503.22.1.el9_5 | |
kernel-debug-modules-extra | 5.14.0-503.22.1.el9_5 | |
kernel-debug-uki-virt | 5.14.0-503.22.1.el9_5 | |
kernel-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-debuginfo-common-x86_64 | 5.14.0-503.22.1.el9_5 | |
kernel-modules | 5.14.0-503.22.1.el9_5 | |
kernel-modules-core | 5.14.0-503.22.1.el9_5 | |
kernel-modules-extra | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-tools | 5.14.0-503.22.1.el9_5 | |
kernel-tools-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-tools-libs | 5.14.0-503.22.1.el9_5 | |
kernel-uki-virt | 5.14.0-503.22.1.el9_5 | |
kernel-uki-virt-addons | 5.14.0-503.22.1.el9_5 | |
libperf-debuginfo | 5.14.0-503.22.1.el9_5 | |
perf-debuginfo | 5.14.0-503.22.1.el9_5 | |
python3-perf | 5.14.0-503.22.1.el9_5 | |
python3-perf-debuginfo | 5.14.0-503.22.1.el9_5 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 7.4.0-503.22.1.el9_5 | |
httpd | 2.4.62-1.el9_5.2 | |
httpd-core | 2.4.62-1.el9_5.2 | |
httpd-core-debuginfo | 2.4.62-1.el9_5.2 | |
httpd-debuginfo | 2.4.62-1.el9_5.2 | |
httpd-debugsource | 2.4.62-1.el9_5.2 | |
httpd-devel | 2.4.62-1.el9_5.2 | |
httpd-filesystem | 2.4.62-1.el9_5.2 | |
httpd-manual | 2.4.62-1.el9_5.2 | |
httpd-tools | 2.4.62-1.el9_5.2 | |
httpd-tools-debuginfo | 2.4.62-1.el9_5.2 | |
kernel-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-debug-devel | 5.14.0-503.22.1.el9_5 | |
kernel-debug-devel-matched | 5.14.0-503.22.1.el9_5 | |
kernel-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-debuginfo-common-x86_64 | 5.14.0-503.22.1.el9_5 | |
kernel-devel | 5.14.0-503.22.1.el9_5 | |
kernel-devel-matched | 5.14.0-503.22.1.el9_5 | |
kernel-doc | 5.14.0-503.22.1.el9_5 | |
kernel-headers | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-tools-debuginfo | 5.14.0-503.22.1.el9_5 | |
libisoburn | 1.5.4-5.el9_5 | [RHBA-2025:0515](https://access.redhat.com/errata/RHBA-2025:0515) | <div class="adv_b">Bug Fix Advisory</div>
libisoburn-debuginfo | 1.5.4-5.el9_5 | |
libisoburn-debugsource | 1.5.4-5.el9_5 | |
libisoburn-doc | 1.5.4-5.el9_5 | [RHBA-2025:0515](https://access.redhat.com/errata/RHBA-2025:0515) | <div class="adv_b">Bug Fix Advisory</div>
libperf-debuginfo | 5.14.0-503.22.1.el9_5 | |
mod_ldap | 2.4.62-1.el9_5.2 | |
mod_ldap-debuginfo | 2.4.62-1.el9_5.2 | |
mod_lua | 2.4.62-1.el9_5.2 | |
mod_lua-debuginfo | 2.4.62-1.el9_5.2 | |
mod_proxy_html | 2.4.62-1.el9_5.2 | |
mod_proxy_html-debuginfo | 2.4.62-1.el9_5.2 | |
mod_session | 2.4.62-1.el9_5.2 | |
mod_session-debuginfo | 2.4.62-1.el9_5.2 | |
mod_ssl | 2.4.62-1.el9_5.2 | |
mod_ssl-debuginfo | 2.4.62-1.el9_5.2 | |
perf | 5.14.0-503.22.1.el9_5 | |
perf-debuginfo | 5.14.0-503.22.1.el9_5 | |
python3-perf-debuginfo | 5.14.0-503.22.1.el9_5 | |
rtla | 5.14.0-503.22.1.el9_5 | |
rv | 5.14.0-503.22.1.el9_5 | |
xorriso | 1.5.4-5.el9_5 | [RHBA-2025:0515](https://access.redhat.com/errata/RHBA-2025:0515) | <div class="adv_b">Bug Fix Advisory</div>
xorriso-debuginfo | 1.5.4-5.el9_5 | |

### rt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 7.4.0-503.22.1.el9_5 | |
kernel-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-debuginfo-common-x86_64 | 5.14.0-503.22.1.el9_5 | |
kernel-rt | 5.14.0-503.22.1.el9_5 | |
kernel-rt-core | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debug | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debug-core | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debug-devel | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debug-modules | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debug-modules-core | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debug-modules-extra | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-rt-devel | 5.14.0-503.22.1.el9_5 | |
kernel-rt-modules | 5.14.0-503.22.1.el9_5 | |
kernel-rt-modules-core | 5.14.0-503.22.1.el9_5 | |
kernel-rt-modules-extra | 5.14.0-503.22.1.el9_5 | |
kernel-tools-debuginfo | 5.14.0-503.22.1.el9_5 | |
libperf-debuginfo | 5.14.0-503.22.1.el9_5 | |
perf-debuginfo | 5.14.0-503.22.1.el9_5 | |
python3-perf-debuginfo | 5.14.0-503.22.1.el9_5 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 7.4.0-503.22.1.el9_5 | |
kernel-cross-headers | 5.14.0-503.22.1.el9_5 | |
kernel-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-debuginfo-common-x86_64 | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-tools-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-tools-libs-devel | 5.14.0-503.22.1.el9_5 | |
libisoburn-debuginfo | 1.5.4-5.el9_5 | |
libisoburn-debugsource | 1.5.4-5.el9_5 | |
libisoburn-devel | 1.5.4-5.el9_5 | [RHBA-2025:0515](https://access.redhat.com/errata/RHBA-2025:0515) | <div class="adv_b">Bug Fix Advisory</div>
libperf | 5.14.0-503.22.1.el9_5 | |
libperf-debuginfo | 5.14.0-503.22.1.el9_5 | |
perf-debuginfo | 5.14.0-503.22.1.el9_5 | |
python3-perf-debuginfo | 5.14.0-503.22.1.el9_5 | |
xorriso-debuginfo | 1.5.4-5.el9_5 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.5.14.0_503.22.1.el9_5.rh9.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.4.0-503.22.1.el9_5 | |
bpftool-debuginfo | 7.4.0-503.22.1.el9_5 | |
kernel | 5.14.0-503.22.1.el9_5 | |
kernel-64k | 5.14.0-503.22.1.el9_5 | |
kernel-64k-core | 5.14.0-503.22.1.el9_5 | |
kernel-64k-debug | 5.14.0-503.22.1.el9_5 | |
kernel-64k-debug-core | 5.14.0-503.22.1.el9_5 | |
kernel-64k-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-64k-debug-modules | 5.14.0-503.22.1.el9_5 | |
kernel-64k-debug-modules-core | 5.14.0-503.22.1.el9_5 | |
kernel-64k-debug-modules-extra | 5.14.0-503.22.1.el9_5 | |
kernel-64k-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-64k-modules | 5.14.0-503.22.1.el9_5 | |
kernel-64k-modules-core | 5.14.0-503.22.1.el9_5 | |
kernel-64k-modules-extra | 5.14.0-503.22.1.el9_5 | |
kernel-abi-stablelists | 5.14.0-503.22.1.el9_5 | |
kernel-core | 5.14.0-503.22.1.el9_5 | |
kernel-debug | 5.14.0-503.22.1.el9_5 | |
kernel-debug-core | 5.14.0-503.22.1.el9_5 | |
kernel-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-debug-modules | 5.14.0-503.22.1.el9_5 | |
kernel-debug-modules-core | 5.14.0-503.22.1.el9_5 | |
kernel-debug-modules-extra | 5.14.0-503.22.1.el9_5 | |
kernel-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-debuginfo-common-aarch64 | 5.14.0-503.22.1.el9_5 | |
kernel-modules | 5.14.0-503.22.1.el9_5 | |
kernel-modules-core | 5.14.0-503.22.1.el9_5 | |
kernel-modules-extra | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-tools | 5.14.0-503.22.1.el9_5 | |
kernel-tools-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-tools-libs | 5.14.0-503.22.1.el9_5 | |
libperf-debuginfo | 5.14.0-503.22.1.el9_5 | |
perf-debuginfo | 5.14.0-503.22.1.el9_5 | |
python3-perf | 5.14.0-503.22.1.el9_5 | |
python3-perf-debuginfo | 5.14.0-503.22.1.el9_5 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 7.4.0-503.22.1.el9_5 | |
httpd | 2.4.62-1.el9_5.2 | |
httpd-core | 2.4.62-1.el9_5.2 | |
httpd-core-debuginfo | 2.4.62-1.el9_5.2 | |
httpd-debuginfo | 2.4.62-1.el9_5.2 | |
httpd-debugsource | 2.4.62-1.el9_5.2 | |
httpd-devel | 2.4.62-1.el9_5.2 | |
httpd-filesystem | 2.4.62-1.el9_5.2 | |
httpd-manual | 2.4.62-1.el9_5.2 | |
httpd-tools | 2.4.62-1.el9_5.2 | |
httpd-tools-debuginfo | 2.4.62-1.el9_5.2 | |
kernel-64k-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-64k-debug-devel | 5.14.0-503.22.1.el9_5 | |
kernel-64k-debug-devel-matched | 5.14.0-503.22.1.el9_5 | |
kernel-64k-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-64k-devel | 5.14.0-503.22.1.el9_5 | |
kernel-64k-devel-matched | 5.14.0-503.22.1.el9_5 | |
kernel-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-debug-devel | 5.14.0-503.22.1.el9_5 | |
kernel-debug-devel-matched | 5.14.0-503.22.1.el9_5 | |
kernel-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-debuginfo-common-aarch64 | 5.14.0-503.22.1.el9_5 | |
kernel-devel | 5.14.0-503.22.1.el9_5 | |
kernel-devel-matched | 5.14.0-503.22.1.el9_5 | |
kernel-doc | 5.14.0-503.22.1.el9_5 | |
kernel-headers | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-tools-debuginfo | 5.14.0-503.22.1.el9_5 | |
libisoburn | 1.5.4-5.el9_5 | [RHBA-2025:0515](https://access.redhat.com/errata/RHBA-2025:0515) | <div class="adv_b">Bug Fix Advisory</div>
libisoburn-debuginfo | 1.5.4-5.el9_5 | |
libisoburn-debugsource | 1.5.4-5.el9_5 | |
libisoburn-doc | 1.5.4-5.el9_5 | [RHBA-2025:0515](https://access.redhat.com/errata/RHBA-2025:0515) | <div class="adv_b">Bug Fix Advisory</div>
libperf-debuginfo | 5.14.0-503.22.1.el9_5 | |
mod_ldap | 2.4.62-1.el9_5.2 | |
mod_ldap-debuginfo | 2.4.62-1.el9_5.2 | |
mod_lua | 2.4.62-1.el9_5.2 | |
mod_lua-debuginfo | 2.4.62-1.el9_5.2 | |
mod_proxy_html | 2.4.62-1.el9_5.2 | |
mod_proxy_html-debuginfo | 2.4.62-1.el9_5.2 | |
mod_session | 2.4.62-1.el9_5.2 | |
mod_session-debuginfo | 2.4.62-1.el9_5.2 | |
mod_ssl | 2.4.62-1.el9_5.2 | |
mod_ssl-debuginfo | 2.4.62-1.el9_5.2 | |
perf | 5.14.0-503.22.1.el9_5 | |
perf-debuginfo | 5.14.0-503.22.1.el9_5 | |
python3-perf-debuginfo | 5.14.0-503.22.1.el9_5 | |
rtla | 5.14.0-503.22.1.el9_5 | |
rv | 5.14.0-503.22.1.el9_5 | |
xorriso | 1.5.4-5.el9_5 | [RHBA-2025:0515](https://access.redhat.com/errata/RHBA-2025:0515) | <div class="adv_b">Bug Fix Advisory</div>
xorriso-debuginfo | 1.5.4-5.el9_5 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 7.4.0-503.22.1.el9_5 | |
kernel-64k-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-64k-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-cross-headers | 5.14.0-503.22.1.el9_5 | |
kernel-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-debuginfo-common-aarch64 | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debug-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-rt-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-tools-debuginfo | 5.14.0-503.22.1.el9_5 | |
kernel-tools-libs-devel | 5.14.0-503.22.1.el9_5 | |
libisoburn-debuginfo | 1.5.4-5.el9_5 | |
libisoburn-debugsource | 1.5.4-5.el9_5 | |
libisoburn-devel | 1.5.4-5.el9_5 | [RHBA-2025:0515](https://access.redhat.com/errata/RHBA-2025:0515) | <div class="adv_b">Bug Fix Advisory</div>
libperf | 5.14.0-503.22.1.el9_5 | |
libperf-debuginfo | 5.14.0-503.22.1.el9_5 | |
perf-debuginfo | 5.14.0-503.22.1.el9_5 | |
python3-perf-debuginfo | 5.14.0-503.22.1.el9_5 | |
xorriso-debuginfo | 1.5.4-5.el9_5 | |

