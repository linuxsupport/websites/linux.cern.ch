## 2023-10-24

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-linuxsupport-access | 1.9-2.rh9.cern | |
hepix | 4.10.8-0.rh9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-linuxsupport-access | 1.9-2.rh9.cern | |
hepix | 4.10.8-0.rh9.cern | |

