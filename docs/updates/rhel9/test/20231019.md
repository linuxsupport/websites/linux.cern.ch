## 2023-10-19

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libnghttp2 | 1.43.0-5.el9_2.1 | [RHSA-2023:5838](https://access.redhat.com/errata/RHSA-2023:5838) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
libnghttp2-debuginfo | 1.43.0-5.el9_2.1 | |
nghttp2-debuginfo | 1.43.0-5.el9_2.1 | |
nghttp2-debugsource | 1.43.0-5.el9_2.1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grafana | 9.0.9-4.el9_2 | |
grafana-debuginfo | 9.0.9-4.el9_2 | |
grafana-debugsource | 9.0.9-4.el9_2 | |
java-1.8.0-openjdk | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-debugsource | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-demo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-devel | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-headless | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-javadoc | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-javadoc-zip | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-src | 1.8.0.392.b08-3.el9 | |
java-11-openjdk | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-debugsource | 11.0.21.0.9-2.el9 | |
java-11-openjdk-demo | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-devel | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-devel-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-headless | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-headless-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-javadoc | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-javadoc-zip | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-jmods | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-src | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-static-libs | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-debugsource | 17.0.9.0.9-2.el9 | |
java-17-openjdk-demo | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-devel | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-devel-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-headless | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-headless-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-javadoc | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-javadoc-zip | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-jmods | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-src | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-static-libs | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
nodejs | 18.18.2-2.module+el9.2.0.z+20408+7cb5fda5 | [RHSA-2023:5849](https://access.redhat.com/errata/RHSA-2023:5849) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38552](https://access.redhat.com/security/cve/CVE-2023-38552), [CVE-2023-39333](https://access.redhat.com/security/cve/CVE-2023-39333), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487), [CVE-2023-45143](https://access.redhat.com/security/cve/CVE-2023-45143))
nodejs-debuginfo | 18.18.2-2.module+el9.2.0.z+20408+7cb5fda5 | |
nodejs-debugsource | 18.18.2-2.module+el9.2.0.z+20408+7cb5fda5 | |
nodejs-devel | 18.18.2-2.module+el9.2.0.z+20408+7cb5fda5 | [RHSA-2023:5849](https://access.redhat.com/errata/RHSA-2023:5849) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38552](https://access.redhat.com/security/cve/CVE-2023-38552), [CVE-2023-39333](https://access.redhat.com/security/cve/CVE-2023-39333), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487), [CVE-2023-45143](https://access.redhat.com/security/cve/CVE-2023-45143))
nodejs-docs | 18.18.2-2.module+el9.2.0.z+20408+7cb5fda5 | [RHSA-2023:5849](https://access.redhat.com/errata/RHSA-2023:5849) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38552](https://access.redhat.com/security/cve/CVE-2023-38552), [CVE-2023-39333](https://access.redhat.com/security/cve/CVE-2023-39333), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487), [CVE-2023-45143](https://access.redhat.com/security/cve/CVE-2023-45143))
nodejs-full-i18n | 18.18.2-2.module+el9.2.0.z+20408+7cb5fda5 | [RHSA-2023:5849](https://access.redhat.com/errata/RHSA-2023:5849) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38552](https://access.redhat.com/security/cve/CVE-2023-38552), [CVE-2023-39333](https://access.redhat.com/security/cve/CVE-2023-39333), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487), [CVE-2023-45143](https://access.redhat.com/security/cve/CVE-2023-45143))
npm | 9.8.1-1.18.18.2.2.module+el9.2.0.z+20408+7cb5fda5 | [RHSA-2023:5849](https://access.redhat.com/errata/RHSA-2023:5849) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38552](https://access.redhat.com/security/cve/CVE-2023-38552), [CVE-2023-39333](https://access.redhat.com/security/cve/CVE-2023-39333), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487), [CVE-2023-45143](https://access.redhat.com/security/cve/CVE-2023-45143))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-1.8.0-openjdk-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-debugsource | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-demo-fastdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-demo-fastdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-demo-slowdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-devel-fastdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-devel-fastdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-devel-slowdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-fastdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-fastdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-headless-fastdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-headless-fastdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-headless-slowdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-slowdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-src-fastdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-src-slowdebug | 1.8.0.392.b08-3.el9 | |
java-11-openjdk-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-debugsource | 11.0.21.0.9-2.el9 | |
java-11-openjdk-demo-fastdebug | 11.0.21.0.9-2.el9 | |
java-11-openjdk-demo-slowdebug | 11.0.21.0.9-2.el9 | |
java-11-openjdk-devel-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-devel-fastdebug | 11.0.21.0.9-2.el9 | |
java-11-openjdk-devel-fastdebug-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-devel-slowdebug | 11.0.21.0.9-2.el9 | |
java-11-openjdk-devel-slowdebug-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-fastdebug | 11.0.21.0.9-2.el9 | |
java-11-openjdk-fastdebug-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-headless-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-headless-fastdebug | 11.0.21.0.9-2.el9 | |
java-11-openjdk-headless-fastdebug-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-headless-slowdebug | 11.0.21.0.9-2.el9 | |
java-11-openjdk-headless-slowdebug-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-jmods-fastdebug | 11.0.21.0.9-2.el9 | |
java-11-openjdk-jmods-slowdebug | 11.0.21.0.9-2.el9 | |
java-11-openjdk-slowdebug | 11.0.21.0.9-2.el9 | |
java-11-openjdk-slowdebug-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-src-fastdebug | 11.0.21.0.9-2.el9 | |
java-11-openjdk-src-slowdebug | 11.0.21.0.9-2.el9 | |
java-11-openjdk-static-libs-fastdebug | 11.0.21.0.9-2.el9 | |
java-11-openjdk-static-libs-slowdebug | 11.0.21.0.9-2.el9 | |
java-17-openjdk-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-debugsource | 17.0.9.0.9-2.el9 | |
java-17-openjdk-demo-fastdebug | 17.0.9.0.9-2.el9 | |
java-17-openjdk-demo-slowdebug | 17.0.9.0.9-2.el9 | |
java-17-openjdk-devel-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-devel-fastdebug | 17.0.9.0.9-2.el9 | |
java-17-openjdk-devel-fastdebug-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-devel-slowdebug | 17.0.9.0.9-2.el9 | |
java-17-openjdk-devel-slowdebug-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-fastdebug | 17.0.9.0.9-2.el9 | |
java-17-openjdk-fastdebug-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-headless-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-headless-fastdebug | 17.0.9.0.9-2.el9 | |
java-17-openjdk-headless-fastdebug-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-headless-slowdebug | 17.0.9.0.9-2.el9 | |
java-17-openjdk-headless-slowdebug-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-jmods-fastdebug | 17.0.9.0.9-2.el9 | |
java-17-openjdk-jmods-slowdebug | 17.0.9.0.9-2.el9 | |
java-17-openjdk-slowdebug | 17.0.9.0.9-2.el9 | |
java-17-openjdk-slowdebug-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-src-fastdebug | 17.0.9.0.9-2.el9 | |
java-17-openjdk-src-slowdebug | 17.0.9.0.9-2.el9 | |
java-17-openjdk-static-libs-fastdebug | 17.0.9.0.9-2.el9 | |
java-17-openjdk-static-libs-slowdebug | 17.0.9.0.9-2.el9 | |
libnghttp2-debuginfo | 1.43.0-5.el9_2.1 | |
libnghttp2-devel | 1.43.0-5.el9_2.1 | [RHSA-2023:5838](https://access.redhat.com/errata/RHSA-2023:5838) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nghttp2 | 1.43.0-5.el9_2.1 | [RHSA-2023:5838](https://access.redhat.com/errata/RHSA-2023:5838) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nghttp2-debuginfo | 1.43.0-5.el9_2.1 | |
nghttp2-debugsource | 1.43.0-5.el9_2.1 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libnghttp2 | 1.43.0-5.el9_2.1 | [RHSA-2023:5838](https://access.redhat.com/errata/RHSA-2023:5838) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
libnghttp2-debuginfo | 1.43.0-5.el9_2.1 | |
nghttp2-debuginfo | 1.43.0-5.el9_2.1 | |
nghttp2-debugsource | 1.43.0-5.el9_2.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grafana | 9.0.9-4.el9_2 | |
grafana-debuginfo | 9.0.9-4.el9_2 | |
grafana-debugsource | 9.0.9-4.el9_2 | |
java-1.8.0-openjdk | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-debugsource | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-demo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-devel | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-headless | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-javadoc | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-javadoc-zip | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-src | 1.8.0.392.b08-3.el9 | |
java-11-openjdk | 11.0.21.0.9-2.el9 | |
java-11-openjdk-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-debugsource | 11.0.21.0.9-2.el9 | |
java-11-openjdk-demo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-devel | 11.0.21.0.9-2.el9 | |
java-11-openjdk-devel-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-headless | 11.0.21.0.9-2.el9 | |
java-11-openjdk-headless-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-javadoc | 11.0.21.0.9-2.el9 | |
java-11-openjdk-javadoc-zip | 11.0.21.0.9-2.el9 | |
java-11-openjdk-jmods | 11.0.21.0.9-2.el9 | |
java-11-openjdk-src | 11.0.21.0.9-2.el9 | |
java-11-openjdk-static-libs | 11.0.21.0.9-2.el9 | |
java-17-openjdk | 17.0.9.0.9-2.el9 | |
java-17-openjdk-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-debugsource | 17.0.9.0.9-2.el9 | |
java-17-openjdk-demo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-devel | 17.0.9.0.9-2.el9 | |
java-17-openjdk-devel-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-headless | 17.0.9.0.9-2.el9 | |
java-17-openjdk-headless-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-javadoc | 17.0.9.0.9-2.el9 | |
java-17-openjdk-javadoc-zip | 17.0.9.0.9-2.el9 | |
java-17-openjdk-jmods | 17.0.9.0.9-2.el9 | |
java-17-openjdk-src | 17.0.9.0.9-2.el9 | |
java-17-openjdk-static-libs | 17.0.9.0.9-2.el9 | |
nodejs | 18.18.2-2.module+el9.2.0.z+20408+7cb5fda5 | |
nodejs-debuginfo | 18.18.2-2.module+el9.2.0.z+20408+7cb5fda5 | |
nodejs-debugsource | 18.18.2-2.module+el9.2.0.z+20408+7cb5fda5 | |
nodejs-devel | 18.18.2-2.module+el9.2.0.z+20408+7cb5fda5 | |
nodejs-docs | 18.18.2-2.module+el9.2.0.z+20408+7cb5fda5 | |
nodejs-full-i18n | 18.18.2-2.module+el9.2.0.z+20408+7cb5fda5 | |
npm | 9.8.1-1.18.18.2.2.module+el9.2.0.z+20408+7cb5fda5 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-1.8.0-openjdk-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-debugsource | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-demo-fastdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-demo-fastdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-demo-slowdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-devel-fastdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-devel-fastdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-devel-slowdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-fastdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-fastdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-headless-fastdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-headless-fastdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-headless-slowdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-slowdebug-debuginfo | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-src-fastdebug | 1.8.0.392.b08-3.el9 | |
java-1.8.0-openjdk-src-slowdebug | 1.8.0.392.b08-3.el9 | |
java-11-openjdk-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-debugsource | 11.0.21.0.9-2.el9 | |
java-11-openjdk-demo-fastdebug | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-demo-slowdebug | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-devel-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-devel-fastdebug | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-devel-fastdebug-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-devel-slowdebug | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-devel-slowdebug-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-fastdebug | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-fastdebug-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-headless-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-headless-fastdebug | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-headless-fastdebug-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-headless-slowdebug | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-headless-slowdebug-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-jmods-fastdebug | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-jmods-slowdebug | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-slowdebug | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-slowdebug-debuginfo | 11.0.21.0.9-2.el9 | |
java-11-openjdk-src-fastdebug | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-src-slowdebug | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-static-libs-fastdebug | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-11-openjdk-static-libs-slowdebug | 11.0.21.0.9-2.el9 | [RHSA-2023:5744](https://access.redhat.com/errata/RHSA-2023:5744) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-debugsource | 17.0.9.0.9-2.el9 | |
java-17-openjdk-demo-fastdebug | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-demo-slowdebug | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-devel-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-devel-fastdebug | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-devel-fastdebug-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-devel-slowdebug | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-devel-slowdebug-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-fastdebug | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-fastdebug-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-headless-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-headless-fastdebug | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-headless-fastdebug-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-headless-slowdebug | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-headless-slowdebug-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-jmods-fastdebug | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-jmods-slowdebug | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-slowdebug | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-slowdebug-debuginfo | 17.0.9.0.9-2.el9 | |
java-17-openjdk-src-fastdebug | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-src-slowdebug | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-static-libs-fastdebug | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
java-17-openjdk-static-libs-slowdebug | 17.0.9.0.9-2.el9 | [RHSA-2023:5753](https://access.redhat.com/errata/RHSA-2023:5753) | <div class="adv_s">Security Advisory</div> ([CVE-2023-22025](https://access.redhat.com/security/cve/CVE-2023-22025), [CVE-2023-22081](https://access.redhat.com/security/cve/CVE-2023-22081))
libnghttp2-debuginfo | 1.43.0-5.el9_2.1 | |
libnghttp2-devel | 1.43.0-5.el9_2.1 | [RHSA-2023:5838](https://access.redhat.com/errata/RHSA-2023:5838) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nghttp2 | 1.43.0-5.el9_2.1 | [RHSA-2023:5838](https://access.redhat.com/errata/RHSA-2023:5838) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nghttp2-debuginfo | 1.43.0-5.el9_2.1 | |
nghttp2-debugsource | 1.43.0-5.el9_2.1 | |

