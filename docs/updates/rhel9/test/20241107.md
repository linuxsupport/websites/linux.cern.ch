## 2024-11-07

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20230808-2.20240910.1.el9_4 | [RHBA-2024:8936](https://access.redhat.com/errata/RHBA-2024:8936) | <div class="adv_b">Bug Fix Advisory</div>

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-ovmf | 20231122-6.el9_4.4 | [RHSA-2024:8935](https://access.redhat.com/errata/RHSA-2024:8935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-aarch64 | 20231122-6.el9_4.4 | [RHSA-2024:8935](https://access.redhat.com/errata/RHSA-2024:8935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-debugsource | 20231122-6.el9_4.4 | |
edk2-tools | 20231122-6.el9_4.4 | [RHSA-2024:8935](https://access.redhat.com/errata/RHSA-2024:8935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-tools-debuginfo | 20231122-6.el9_4.4 | |
edk2-tools-doc | 20231122-6.el9_4.4 | [RHSA-2024:8935](https://access.redhat.com/errata/RHSA-2024:8935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-aarch64 | 20231122-6.el9_4.4 | [RHSA-2024:8935](https://access.redhat.com/errata/RHSA-2024:8935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-debugsource | 20231122-6.el9_4.4 | |
edk2-ovmf | 20231122-6.el9_4.4 | [RHSA-2024:8935](https://access.redhat.com/errata/RHSA-2024:8935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-tools | 20231122-6.el9_4.4 | [RHSA-2024:8935](https://access.redhat.com/errata/RHSA-2024:8935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))
edk2-tools-debuginfo | 20231122-6.el9_4.4 | |
edk2-tools-doc | 20231122-6.el9_4.4 | [RHSA-2024:8935](https://access.redhat.com/errata/RHSA-2024:8935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6119](https://access.redhat.com/security/cve/CVE-2024-6119))

