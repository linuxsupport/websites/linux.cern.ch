## 2024-11-04

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 128.4.0-1.el9_4 | [RHSA-2024:8726](https://access.redhat.com/errata/RHSA-2024:8726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10458](https://access.redhat.com/security/cve/CVE-2024-10458), [CVE-2024-10459](https://access.redhat.com/security/cve/CVE-2024-10459), [CVE-2024-10460](https://access.redhat.com/security/cve/CVE-2024-10460), [CVE-2024-10461](https://access.redhat.com/security/cve/CVE-2024-10461), [CVE-2024-10462](https://access.redhat.com/security/cve/CVE-2024-10462), [CVE-2024-10463](https://access.redhat.com/security/cve/CVE-2024-10463), [CVE-2024-10464](https://access.redhat.com/security/cve/CVE-2024-10464), [CVE-2024-10465](https://access.redhat.com/security/cve/CVE-2024-10465), [CVE-2024-10466](https://access.redhat.com/security/cve/CVE-2024-10466), [CVE-2024-10467](https://access.redhat.com/security/cve/CVE-2024-10467))
firefox-debuginfo | 128.4.0-1.el9_4 | |
firefox-debugsource | 128.4.0-1.el9_4 | |
firefox-x11 | 128.4.0-1.el9_4 | [RHSA-2024:8726](https://access.redhat.com/errata/RHSA-2024:8726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10458](https://access.redhat.com/security/cve/CVE-2024-10458), [CVE-2024-10459](https://access.redhat.com/security/cve/CVE-2024-10459), [CVE-2024-10460](https://access.redhat.com/security/cve/CVE-2024-10460), [CVE-2024-10461](https://access.redhat.com/security/cve/CVE-2024-10461), [CVE-2024-10462](https://access.redhat.com/security/cve/CVE-2024-10462), [CVE-2024-10463](https://access.redhat.com/security/cve/CVE-2024-10463), [CVE-2024-10464](https://access.redhat.com/security/cve/CVE-2024-10464), [CVE-2024-10465](https://access.redhat.com/security/cve/CVE-2024-10465), [CVE-2024-10466](https://access.redhat.com/security/cve/CVE-2024-10466), [CVE-2024-10467](https://access.redhat.com/security/cve/CVE-2024-10467))
thunderbird | 128.4.0-1.el9_4 | |
thunderbird-debuginfo | 128.4.0-1.el9_4 | |
thunderbird-debugsource | 128.4.0-1.el9_4 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 128.4.0-1.el9_4 | [RHSA-2024:8726](https://access.redhat.com/errata/RHSA-2024:8726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10458](https://access.redhat.com/security/cve/CVE-2024-10458), [CVE-2024-10459](https://access.redhat.com/security/cve/CVE-2024-10459), [CVE-2024-10460](https://access.redhat.com/security/cve/CVE-2024-10460), [CVE-2024-10461](https://access.redhat.com/security/cve/CVE-2024-10461), [CVE-2024-10462](https://access.redhat.com/security/cve/CVE-2024-10462), [CVE-2024-10463](https://access.redhat.com/security/cve/CVE-2024-10463), [CVE-2024-10464](https://access.redhat.com/security/cve/CVE-2024-10464), [CVE-2024-10465](https://access.redhat.com/security/cve/CVE-2024-10465), [CVE-2024-10466](https://access.redhat.com/security/cve/CVE-2024-10466), [CVE-2024-10467](https://access.redhat.com/security/cve/CVE-2024-10467))
firefox-debuginfo | 128.4.0-1.el9_4 | |
firefox-debugsource | 128.4.0-1.el9_4 | |
firefox-x11 | 128.4.0-1.el9_4 | [RHSA-2024:8726](https://access.redhat.com/errata/RHSA-2024:8726) | <div class="adv_s">Security Advisory</div> ([CVE-2024-10458](https://access.redhat.com/security/cve/CVE-2024-10458), [CVE-2024-10459](https://access.redhat.com/security/cve/CVE-2024-10459), [CVE-2024-10460](https://access.redhat.com/security/cve/CVE-2024-10460), [CVE-2024-10461](https://access.redhat.com/security/cve/CVE-2024-10461), [CVE-2024-10462](https://access.redhat.com/security/cve/CVE-2024-10462), [CVE-2024-10463](https://access.redhat.com/security/cve/CVE-2024-10463), [CVE-2024-10464](https://access.redhat.com/security/cve/CVE-2024-10464), [CVE-2024-10465](https://access.redhat.com/security/cve/CVE-2024-10465), [CVE-2024-10466](https://access.redhat.com/security/cve/CVE-2024-10466), [CVE-2024-10467](https://access.redhat.com/security/cve/CVE-2024-10467))
thunderbird | 128.4.0-1.el9_4 | |
thunderbird-debuginfo | 128.4.0-1.el9_4 | |
thunderbird-debugsource | 128.4.0-1.el9_4 | |

