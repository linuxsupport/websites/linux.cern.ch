## 2024-08-01

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
freeradius | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-debuginfo | 3.0.21-40.el9_4 | |
freeradius-debugsource | 3.0.21-40.el9_4 | |
freeradius-devel | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-doc | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-krb5 | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-krb5-debuginfo | 3.0.21-40.el9_4 | |
freeradius-ldap | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-ldap-debuginfo | 3.0.21-40.el9_4 | |
freeradius-mysql-debuginfo | 3.0.21-40.el9_4 | |
freeradius-perl-debuginfo | 3.0.21-40.el9_4 | |
freeradius-postgresql-debuginfo | 3.0.21-40.el9_4 | |
freeradius-rest-debuginfo | 3.0.21-40.el9_4 | |
freeradius-sqlite-debuginfo | 3.0.21-40.el9_4 | |
freeradius-unixODBC-debuginfo | 3.0.21-40.el9_4 | |
freeradius-utils | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-utils-debuginfo | 3.0.21-40.el9_4 | |
python3-freeradius | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
python3-freeradius-debuginfo | 3.0.21-40.el9_4 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
freeradius-debuginfo | 3.0.21-40.el9_4 | |
freeradius-debugsource | 3.0.21-40.el9_4 | |
freeradius-krb5-debuginfo | 3.0.21-40.el9_4 | |
freeradius-ldap-debuginfo | 3.0.21-40.el9_4 | |
freeradius-mysql | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-mysql-debuginfo | 3.0.21-40.el9_4 | |
freeradius-perl | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-perl-debuginfo | 3.0.21-40.el9_4 | |
freeradius-postgresql | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-postgresql-debuginfo | 3.0.21-40.el9_4 | |
freeradius-rest | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-rest-debuginfo | 3.0.21-40.el9_4 | |
freeradius-sqlite | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-sqlite-debuginfo | 3.0.21-40.el9_4 | |
freeradius-unixODBC | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-unixODBC-debuginfo | 3.0.21-40.el9_4 | |
freeradius-utils-debuginfo | 3.0.21-40.el9_4 | |
python3-freeradius-debuginfo | 3.0.21-40.el9_4 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
freeradius | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-debuginfo | 3.0.21-40.el9_4 | |
freeradius-debugsource | 3.0.21-40.el9_4 | |
freeradius-devel | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-doc | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-krb5 | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-krb5-debuginfo | 3.0.21-40.el9_4 | |
freeradius-ldap | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-ldap-debuginfo | 3.0.21-40.el9_4 | |
freeradius-mysql-debuginfo | 3.0.21-40.el9_4 | |
freeradius-perl-debuginfo | 3.0.21-40.el9_4 | |
freeradius-postgresql-debuginfo | 3.0.21-40.el9_4 | |
freeradius-rest-debuginfo | 3.0.21-40.el9_4 | |
freeradius-sqlite-debuginfo | 3.0.21-40.el9_4 | |
freeradius-unixODBC-debuginfo | 3.0.21-40.el9_4 | |
freeradius-utils | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-utils-debuginfo | 3.0.21-40.el9_4 | |
python3-freeradius | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
python3-freeradius-debuginfo | 3.0.21-40.el9_4 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
freeradius-debuginfo | 3.0.21-40.el9_4 | |
freeradius-debugsource | 3.0.21-40.el9_4 | |
freeradius-krb5-debuginfo | 3.0.21-40.el9_4 | |
freeradius-ldap-debuginfo | 3.0.21-40.el9_4 | |
freeradius-mysql | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-mysql-debuginfo | 3.0.21-40.el9_4 | |
freeradius-perl | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-perl-debuginfo | 3.0.21-40.el9_4 | |
freeradius-postgresql | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-postgresql-debuginfo | 3.0.21-40.el9_4 | |
freeradius-rest | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-rest-debuginfo | 3.0.21-40.el9_4 | |
freeradius-sqlite | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-sqlite-debuginfo | 3.0.21-40.el9_4 | |
freeradius-unixODBC | 3.0.21-40.el9_4 | [RHSA-2024:4935](https://access.redhat.com/errata/RHSA-2024:4935) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-unixODBC-debuginfo | 3.0.21-40.el9_4 | |
freeradius-utils-debuginfo | 3.0.21-40.el9_4 | |
python3-freeradius-debuginfo | 3.0.21-40.el9_4 | |

