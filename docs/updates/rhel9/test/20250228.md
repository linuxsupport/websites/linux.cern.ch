## 2025-02-28

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs | 27.2-11.el9_5.1 | [RHSA-2025:1915](https://access.redhat.com/errata/RHSA-2025:1915) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1244](https://access.redhat.com/security/cve/CVE-2025-1244))
emacs-common | 27.2-11.el9_5.1 | [RHSA-2025:1915](https://access.redhat.com/errata/RHSA-2025:1915) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1244](https://access.redhat.com/security/cve/CVE-2025-1244))
emacs-common-debuginfo | 27.2-11.el9_5.1 | |
emacs-debuginfo | 27.2-11.el9_5.1 | |
emacs-debugsource | 27.2-11.el9_5.1 | |
emacs-filesystem | 27.2-11.el9_5.1 | [RHSA-2025:1915](https://access.redhat.com/errata/RHSA-2025:1915) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1244](https://access.redhat.com/security/cve/CVE-2025-1244))
emacs-lucid | 27.2-11.el9_5.1 | [RHSA-2025:1915](https://access.redhat.com/errata/RHSA-2025:1915) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1244](https://access.redhat.com/security/cve/CVE-2025-1244))
emacs-lucid-debuginfo | 27.2-11.el9_5.1 | |
emacs-nox | 27.2-11.el9_5.1 | [RHSA-2025:1915](https://access.redhat.com/errata/RHSA-2025:1915) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1244](https://access.redhat.com/security/cve/CVE-2025-1244))
emacs-nox-debuginfo | 27.2-11.el9_5.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs | 27.2-11.el9_5.1 | [RHSA-2025:1915](https://access.redhat.com/errata/RHSA-2025:1915) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1244](https://access.redhat.com/security/cve/CVE-2025-1244))
emacs-common | 27.2-11.el9_5.1 | [RHSA-2025:1915](https://access.redhat.com/errata/RHSA-2025:1915) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1244](https://access.redhat.com/security/cve/CVE-2025-1244))
emacs-common-debuginfo | 27.2-11.el9_5.1 | |
emacs-debuginfo | 27.2-11.el9_5.1 | |
emacs-debugsource | 27.2-11.el9_5.1 | |
emacs-filesystem | 27.2-11.el9_5.1 | [RHSA-2025:1915](https://access.redhat.com/errata/RHSA-2025:1915) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1244](https://access.redhat.com/security/cve/CVE-2025-1244))
emacs-lucid | 27.2-11.el9_5.1 | [RHSA-2025:1915](https://access.redhat.com/errata/RHSA-2025:1915) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1244](https://access.redhat.com/security/cve/CVE-2025-1244))
emacs-lucid-debuginfo | 27.2-11.el9_5.1 | |
emacs-nox | 27.2-11.el9_5.1 | [RHSA-2025:1915](https://access.redhat.com/errata/RHSA-2025:1915) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1244](https://access.redhat.com/security/cve/CVE-2025-1244))
emacs-nox-debuginfo | 27.2-11.el9_5.1 | |

