## 2024-08-12

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 2.4.5-9.el9_4 | |
389-ds-base-debuginfo | 2.4.5-9.el9_4 | |
389-ds-base-debugsource | 2.4.5-9.el9_4 | |
389-ds-base-libs | 2.4.5-9.el9_4 | |
389-ds-base-libs-debuginfo | 2.4.5-9.el9_4 | |
389-ds-base-snmp-debuginfo | 2.4.5-9.el9_4 | |
python3-lib389 | 2.4.5-9.el9_4 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base-debuginfo | 2.4.5-9.el9_4 | |
389-ds-base-debugsource | 2.4.5-9.el9_4 | |
389-ds-base-devel | 2.4.5-9.el9_4 | |
389-ds-base-libs-debuginfo | 2.4.5-9.el9_4 | |
389-ds-base-snmp-debuginfo | 2.4.5-9.el9_4 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 2.4.5-9.el9_4 | |
389-ds-base-debuginfo | 2.4.5-9.el9_4 | |
389-ds-base-debugsource | 2.4.5-9.el9_4 | |
389-ds-base-libs | 2.4.5-9.el9_4 | |
389-ds-base-libs-debuginfo | 2.4.5-9.el9_4 | |
389-ds-base-snmp-debuginfo | 2.4.5-9.el9_4 | |
python3-lib389 | 2.4.5-9.el9_4 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base-debuginfo | 2.4.5-9.el9_4 | |
389-ds-base-debugsource | 2.4.5-9.el9_4 | |
389-ds-base-devel | 2.4.5-9.el9_4 | |
389-ds-base-libs-debuginfo | 2.4.5-9.el9_4 | |
389-ds-base-snmp-debuginfo | 2.4.5-9.el9_4 | |

