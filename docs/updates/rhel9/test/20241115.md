## 2024-11-15

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
evolution | 3.40.4-10.el9_5.1 | [RHBA-2024:9683](https://access.redhat.com/errata/RHBA-2024:9683) | <div class="adv_b">Bug Fix Advisory</div>
evolution-bogofilter | 3.40.4-10.el9_5.1 | [RHBA-2024:9683](https://access.redhat.com/errata/RHBA-2024:9683) | <div class="adv_b">Bug Fix Advisory</div>
evolution-help | 3.40.4-10.el9_5.1 | [RHBA-2024:9683](https://access.redhat.com/errata/RHBA-2024:9683) | <div class="adv_b">Bug Fix Advisory</div>
evolution-langpacks | 3.40.4-10.el9_5.1 | [RHBA-2024:9683](https://access.redhat.com/errata/RHBA-2024:9683) | <div class="adv_b">Bug Fix Advisory</div>
evolution-pst | 3.40.4-10.el9_5.1 | [RHBA-2024:9683](https://access.redhat.com/errata/RHBA-2024:9683) | <div class="adv_b">Bug Fix Advisory</div>
evolution-spamassassin | 3.40.4-10.el9_5.1 | [RHBA-2024:9683](https://access.redhat.com/errata/RHBA-2024:9683) | <div class="adv_b">Bug Fix Advisory</div>
squid | 5.5-14.el9_5.3 | [RHSA-2024:9625](https://access.redhat.com/errata/RHSA-2024:9625) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45802](https://access.redhat.com/security/cve/CVE-2024-45802))
squid-debuginfo | 5.5-14.el9_5.3 | |
squid-debugsource | 5.5-14.el9_5.3 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
evolution-bogofilter-debuginfo | 3.40.4-10.el9_5.1 | |
evolution-debuginfo | 3.40.4-10.el9_5.1 | |
evolution-debugsource | 3.40.4-10.el9_5.1 | |
evolution-devel | 3.40.4-10.el9_5.1 | |
evolution-pst-debuginfo | 3.40.4-10.el9_5.1 | |
evolution-spamassassin-debuginfo | 3.40.4-10.el9_5.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
evolution | 3.40.4-10.el9_5.1 | |
evolution-bogofilter | 3.40.4-10.el9_5.1 | |
evolution-bogofilter-debuginfo | 3.40.4-10.el9_5.1 | |
evolution-debuginfo | 3.40.4-10.el9_5.1 | |
evolution-debugsource | 3.40.4-10.el9_5.1 | |
evolution-help | 3.40.4-10.el9_5.1 | |
evolution-langpacks | 3.40.4-10.el9_5.1 | |
evolution-pst | 3.40.4-10.el9_5.1 | |
evolution-pst-debuginfo | 3.40.4-10.el9_5.1 | |
evolution-spamassassin | 3.40.4-10.el9_5.1 | |
evolution-spamassassin-debuginfo | 3.40.4-10.el9_5.1 | |
squid | 5.5-14.el9_5.3 | [RHSA-2024:9625](https://access.redhat.com/errata/RHSA-2024:9625) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45802](https://access.redhat.com/security/cve/CVE-2024-45802))
squid-debuginfo | 5.5-14.el9_5.3 | |
squid-debugsource | 5.5-14.el9_5.3 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
evolution-bogofilter-debuginfo | 3.40.4-10.el9_5.1 | |
evolution-debuginfo | 3.40.4-10.el9_5.1 | |
evolution-debugsource | 3.40.4-10.el9_5.1 | |
evolution-devel | 3.40.4-10.el9_5.1 | |
evolution-pst-debuginfo | 3.40.4-10.el9_5.1 | |
evolution-spamassassin-debuginfo | 3.40.4-10.el9_5.1 | |

