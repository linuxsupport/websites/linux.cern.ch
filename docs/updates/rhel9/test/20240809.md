## 2024-08-09

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
httpd | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
httpd-core | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
httpd-core-debuginfo | 2.4.57-11.el9_4.1 | |
httpd-debuginfo | 2.4.57-11.el9_4.1 | |
httpd-debugsource | 2.4.57-11.el9_4.1 | |
httpd-devel | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
httpd-filesystem | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
httpd-manual | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
httpd-tools | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
httpd-tools-debuginfo | 2.4.57-11.el9_4.1 | |
mod_ldap | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
mod_ldap-debuginfo | 2.4.57-11.el9_4.1 | |
mod_lua | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
mod_lua-debuginfo | 2.4.57-11.el9_4.1 | |
mod_proxy_html | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
mod_proxy_html-debuginfo | 2.4.57-11.el9_4.1 | |
mod_session | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
mod_session-debuginfo | 2.4.57-11.el9_4.1 | |
mod_ssl | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
mod_ssl-debuginfo | 2.4.57-11.el9_4.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
httpd | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
httpd-core | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
httpd-core-debuginfo | 2.4.57-11.el9_4.1 | |
httpd-debuginfo | 2.4.57-11.el9_4.1 | |
httpd-debugsource | 2.4.57-11.el9_4.1 | |
httpd-devel | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
httpd-filesystem | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
httpd-manual | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
httpd-tools | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
httpd-tools-debuginfo | 2.4.57-11.el9_4.1 | |
mod_ldap | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
mod_ldap-debuginfo | 2.4.57-11.el9_4.1 | |
mod_lua | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
mod_lua-debuginfo | 2.4.57-11.el9_4.1 | |
mod_proxy_html | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
mod_proxy_html-debuginfo | 2.4.57-11.el9_4.1 | |
mod_session | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
mod_session-debuginfo | 2.4.57-11.el9_4.1 | |
mod_ssl | 2.4.57-11.el9_4.1 | [RHSA-2024:5138](https://access.redhat.com/errata/RHSA-2024:5138) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38476](https://access.redhat.com/security/cve/CVE-2024-38476))
mod_ssl-debuginfo | 2.4.57-11.el9_4.1 | |

