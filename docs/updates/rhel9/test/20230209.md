## 2023-02-09

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.4-1.rh9.cern | |
pyphonebook | 2.1.5-1.rh9.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy | 34.1.43-1.el9_1.1 | |
selinux-policy-doc | 34.1.43-1.el9_1.1 | |
selinux-policy-mls | 34.1.43-1.el9_1.1 | |
selinux-policy-sandbox | 34.1.43-1.el9_1.1 | |
selinux-policy-targeted | 34.1.43-1.el9_1.1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy-devel | 34.1.43-1.el9_1.1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.4-1.rh9.cern | |
pyphonebook | 2.1.5-1.rh9.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy | 34.1.43-1.el9_1.1 | |
selinux-policy-doc | 34.1.43-1.el9_1.1 | |
selinux-policy-mls | 34.1.43-1.el9_1.1 | |
selinux-policy-sandbox | 34.1.43-1.el9_1.1 | |
selinux-policy-targeted | 34.1.43-1.el9_1.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy-devel | 34.1.43-1.el9_1.1 | |

