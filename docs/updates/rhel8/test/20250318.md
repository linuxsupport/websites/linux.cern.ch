## 2025-03-18

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 128.8.0-2.el8_10 | [RHSA-2025:2900](https://access.redhat.com/errata/RHSA-2025:2900) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1937](https://access.redhat.com/security/cve/CVE-2025-1937), [CVE-2025-1938](https://access.redhat.com/security/cve/CVE-2025-1938))
thunderbird-debuginfo | 128.8.0-2.el8_10 | |
thunderbird-debugsource | 128.8.0-2.el8_10 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 128.8.0-2.el8_10 | [RHSA-2025:2900](https://access.redhat.com/errata/RHSA-2025:2900) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1937](https://access.redhat.com/security/cve/CVE-2025-1937), [CVE-2025-1938](https://access.redhat.com/security/cve/CVE-2025-1938))
thunderbird-debuginfo | 128.8.0-2.el8_10 | |
thunderbird-debugsource | 128.8.0-2.el8_10 | |

