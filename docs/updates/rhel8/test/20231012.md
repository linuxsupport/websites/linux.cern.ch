## 2023-10-12

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.11-1.rh8.cern | |
useraddcern | 1.2-1.rh8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.11-1.rh8.cern | |
useraddcern | 1.2-1.rh8.cern | |

