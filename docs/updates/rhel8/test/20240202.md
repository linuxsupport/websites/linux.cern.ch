## 2024-02-02

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.6-1.rh8.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-rpm | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
python3-rpm-debuginfo | 4.14.3-28.el8_9 | |
rpm | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-apidocs | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-build-debuginfo | 4.14.3-28.el8_9 | |
rpm-build-libs | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-build-libs-debuginfo | 4.14.3-28.el8_9 | |
rpm-cron | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-debuginfo | 4.14.3-28.el8_9 | |
rpm-debugsource | 4.14.3-28.el8_9 | |
rpm-devel | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-devel-debuginfo | 4.14.3-28.el8_9 | |
rpm-libs | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-libs-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-fapolicyd-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-ima | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-plugin-ima-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-prioreset | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-plugin-prioreset-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-selinux | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-plugin-selinux-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-syslog | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-plugin-syslog-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-systemd-inhibit | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-plugin-systemd-inhibit-debuginfo | 4.14.3-28.el8_9 | |
rpm-sign | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-sign-debuginfo | 4.14.3-28.el8_9 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-rpm-debuginfo | 4.14.3-28.el8_9 | |
rpm-build | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-build-debuginfo | 4.14.3-28.el8_9 | |
rpm-build-libs-debuginfo | 4.14.3-28.el8_9 | |
rpm-debuginfo | 4.14.3-28.el8_9 | |
rpm-debugsource | 4.14.3-28.el8_9 | |
rpm-devel-debuginfo | 4.14.3-28.el8_9 | |
rpm-libs-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-fapolicyd | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-plugin-fapolicyd-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-ima-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-prioreset-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-selinux-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-syslog-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-systemd-inhibit-debuginfo | 4.14.3-28.el8_9 | |
rpm-sign-debuginfo | 4.14.3-28.el8_9 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.6-1.rh8.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-rpm | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
python3-rpm-debuginfo | 4.14.3-28.el8_9 | |
rpm | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-apidocs | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-build-debuginfo | 4.14.3-28.el8_9 | |
rpm-build-libs | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-build-libs-debuginfo | 4.14.3-28.el8_9 | |
rpm-cron | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-debuginfo | 4.14.3-28.el8_9 | |
rpm-debugsource | 4.14.3-28.el8_9 | |
rpm-devel | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-devel-debuginfo | 4.14.3-28.el8_9 | |
rpm-libs | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-libs-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-fapolicyd-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-ima | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-plugin-ima-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-prioreset | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-plugin-prioreset-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-selinux | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-plugin-selinux-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-syslog | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-plugin-syslog-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-systemd-inhibit | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-plugin-systemd-inhibit-debuginfo | 4.14.3-28.el8_9 | |
rpm-sign | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-sign-debuginfo | 4.14.3-28.el8_9 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-rpm-debuginfo | 4.14.3-28.el8_9 | |
rpm-build | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-build-debuginfo | 4.14.3-28.el8_9 | |
rpm-build-libs-debuginfo | 4.14.3-28.el8_9 | |
rpm-debuginfo | 4.14.3-28.el8_9 | |
rpm-debugsource | 4.14.3-28.el8_9 | |
rpm-devel-debuginfo | 4.14.3-28.el8_9 | |
rpm-libs-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-fapolicyd | 4.14.3-28.el8_9 | [RHSA-2024:0647](https://access.redhat.com/errata/RHSA-2024:0647) | <div class="adv_s">Security Advisory</div> ([CVE-2021-35937](https://access.redhat.com/security/cve/CVE-2021-35937), [CVE-2021-35938](https://access.redhat.com/security/cve/CVE-2021-35938), [CVE-2021-35939](https://access.redhat.com/security/cve/CVE-2021-35939))
rpm-plugin-fapolicyd-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-ima-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-prioreset-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-selinux-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-syslog-debuginfo | 4.14.3-28.el8_9 | |
rpm-plugin-systemd-inhibit-debuginfo | 4.14.3-28.el8_9 | |
rpm-sign-debuginfo | 4.14.3-28.el8_9 | |

