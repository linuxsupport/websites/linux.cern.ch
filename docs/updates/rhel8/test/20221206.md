## 2022-12-06

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2022g-1.el8 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2022g-1.el8 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2022g-1.el8 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2022g-1.el8 | |

