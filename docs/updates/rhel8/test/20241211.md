## 2024-12-11

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_553.32.1.el8_10.rh8.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-553.32.1.el8_10 | |
bpftool-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel | 4.18.0-553.32.1.el8_10 | |
kernel-abi-stablelists | 4.18.0-553.32.1.el8_10 | |
kernel-core | 4.18.0-553.32.1.el8_10 | |
kernel-cross-headers | 4.18.0-553.32.1.el8_10 | |
kernel-debug | 4.18.0-553.32.1.el8_10 | |
kernel-debug-core | 4.18.0-553.32.1.el8_10 | |
kernel-debug-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel-debug-devel | 4.18.0-553.32.1.el8_10 | |
kernel-debug-modules | 4.18.0-553.32.1.el8_10 | |
kernel-debug-modules-extra | 4.18.0-553.32.1.el8_10 | |
kernel-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel-debuginfo-common-x86_64 | 4.18.0-553.32.1.el8_10 | |
kernel-devel | 4.18.0-553.32.1.el8_10 | |
kernel-doc | 4.18.0-553.32.1.el8_10 | |
kernel-headers | 4.18.0-553.32.1.el8_10 | |
kernel-modules | 4.18.0-553.32.1.el8_10 | |
kernel-modules-extra | 4.18.0-553.32.1.el8_10 | |
kernel-tools | 4.18.0-553.32.1.el8_10 | |
kernel-tools-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel-tools-libs | 4.18.0-553.32.1.el8_10 | |
perf | 4.18.0-553.32.1.el8_10 | |
perf-debuginfo | 4.18.0-553.32.1.el8_10 | |
python3-perf | 4.18.0-553.32.1.el8_10 | |
python3-perf-debuginfo | 4.18.0-553.32.1.el8_10 | |

### rt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-553.32.1.rt7.373.el8_10 | |
kernel-rt-core | 4.18.0-553.32.1.rt7.373.el8_10 | |
kernel-rt-debug | 4.18.0-553.32.1.rt7.373.el8_10 | |
kernel-rt-debug-core | 4.18.0-553.32.1.rt7.373.el8_10 | |
kernel-rt-debug-debuginfo | 4.18.0-553.32.1.rt7.373.el8_10 | |
kernel-rt-debug-devel | 4.18.0-553.32.1.rt7.373.el8_10 | |
kernel-rt-debug-modules | 4.18.0-553.32.1.rt7.373.el8_10 | |
kernel-rt-debug-modules-extra | 4.18.0-553.32.1.rt7.373.el8_10 | |
kernel-rt-debuginfo | 4.18.0-553.32.1.rt7.373.el8_10 | |
kernel-rt-debuginfo-common-x86_64 | 4.18.0-553.32.1.rt7.373.el8_10 | |
kernel-rt-devel | 4.18.0-553.32.1.rt7.373.el8_10 | |
kernel-rt-modules | 4.18.0-553.32.1.rt7.373.el8_10 | |
kernel-rt-modules-extra | 4.18.0-553.32.1.rt7.373.el8_10 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel-debug-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel-debuginfo-common-x86_64 | 4.18.0-553.32.1.el8_10 | |
kernel-tools-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel-tools-libs-devel | 4.18.0-553.32.1.el8_10 | |
perf-debuginfo | 4.18.0-553.32.1.el8_10 | |
python3-perf-debuginfo | 4.18.0-553.32.1.el8_10 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_553.32.1.el8_10.rh8.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-553.32.1.el8_10 | |
bpftool-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel | 4.18.0-553.32.1.el8_10 | |
kernel-abi-stablelists | 4.18.0-553.32.1.el8_10 | |
kernel-core | 4.18.0-553.32.1.el8_10 | |
kernel-cross-headers | 4.18.0-553.32.1.el8_10 | |
kernel-debug | 4.18.0-553.32.1.el8_10 | |
kernel-debug-core | 4.18.0-553.32.1.el8_10 | |
kernel-debug-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel-debug-devel | 4.18.0-553.32.1.el8_10 | |
kernel-debug-modules | 4.18.0-553.32.1.el8_10 | |
kernel-debug-modules-extra | 4.18.0-553.32.1.el8_10 | |
kernel-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel-debuginfo-common-aarch64 | 4.18.0-553.32.1.el8_10 | |
kernel-devel | 4.18.0-553.32.1.el8_10 | |
kernel-doc | 4.18.0-553.32.1.el8_10 | |
kernel-headers | 4.18.0-553.32.1.el8_10 | |
kernel-modules | 4.18.0-553.32.1.el8_10 | |
kernel-modules-extra | 4.18.0-553.32.1.el8_10 | |
kernel-tools | 4.18.0-553.32.1.el8_10 | |
kernel-tools-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel-tools-libs | 4.18.0-553.32.1.el8_10 | |
perf | 4.18.0-553.32.1.el8_10 | |
perf-debuginfo | 4.18.0-553.32.1.el8_10 | |
python3-perf | 4.18.0-553.32.1.el8_10 | |
python3-perf-debuginfo | 4.18.0-553.32.1.el8_10 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel-debug-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel-debuginfo-common-aarch64 | 4.18.0-553.32.1.el8_10 | |
kernel-tools-debuginfo | 4.18.0-553.32.1.el8_10 | |
kernel-tools-libs-devel | 4.18.0-553.32.1.el8_10 | |
perf-debuginfo | 4.18.0-553.32.1.el8_10 | |
python3-perf-debuginfo | 4.18.0-553.32.1.el8_10 | |

