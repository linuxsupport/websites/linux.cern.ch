## 2023-05-03

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libwebp | 1.0.0-8.el8_7 | |
libwebp-debuginfo | 1.0.0-8.el8_7 | |
libwebp-debugsource | 1.0.0-8.el8_7 | |
libwebp-devel | 1.0.0-8.el8_7 | |
libwebp-java-debuginfo | 1.0.0-8.el8_7 | |
libwebp-tools-debuginfo | 1.0.0-8.el8_7 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libwebp | 1.0.0-8.el8_7 | |
libwebp-debuginfo | 1.0.0-8.el8_7 | |
libwebp-debugsource | 1.0.0-8.el8_7 | |
libwebp-devel | 1.0.0-8.el8_7 | |
libwebp-java-debuginfo | 1.0.0-8.el8_7 | |
libwebp-tools-debuginfo | 1.0.0-8.el8_7 | |

