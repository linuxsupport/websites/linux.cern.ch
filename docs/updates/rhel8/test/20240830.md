## 2024-08-30

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pgaudit | 1.4.0-7.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
pgaudit-debuginfo | 1.4.0-7.module+el8.10.0+22214+9beb89d6 | |
pgaudit-debugsource | 1.4.0-7.module+el8.10.0+22214+9beb89d6 | |
postgresql | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-contrib | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-contrib | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-contrib | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-contrib-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-contrib-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-contrib-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-debugsource | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-debugsource | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-debugsource | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-docs | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-docs | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-docs | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-docs-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-docs-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-docs-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-plperl | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-plperl | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-plperl | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-plperl-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-plperl-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-plperl-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-plpython3 | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-plpython3 | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-plpython3 | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-plpython3-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-plpython3-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-plpython3-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-pltcl | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-pltcl | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-pltcl | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-pltcl-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-pltcl-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-pltcl-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-private-devel | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-private-libs | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-private-libs-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-server | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-server | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-server | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-server-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-server-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-server-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-server-devel | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-server-devel | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-server-devel | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-server-devel-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-server-devel-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-server-devel-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-static | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-static | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-static | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-test | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-test | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-test | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-test-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-test-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-test-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-test-rpm-macros | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-test-rpm-macros | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-test-rpm-macros | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-upgrade | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-upgrade | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-upgrade | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-upgrade-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-upgrade-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-upgrade-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-upgrade-devel | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-upgrade-devel | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-upgrade-devel | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-upgrade-devel-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-upgrade-devel-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-upgrade-devel-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pgaudit | 1.4.0-7.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
pgaudit-debuginfo | 1.4.0-7.module+el8.10.0+22214+9beb89d6 | |
pgaudit-debugsource | 1.4.0-7.module+el8.10.0+22214+9beb89d6 | |
postgresql | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-contrib | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-contrib | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-contrib | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-contrib-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-contrib-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-contrib-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-debugsource | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-debugsource | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-debugsource | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-docs | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-docs | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-docs | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-docs-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-docs-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-docs-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-plperl | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-plperl | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-plperl | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-plperl-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-plperl-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-plperl-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-plpython3 | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-plpython3 | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-plpython3 | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-plpython3-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-plpython3-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-plpython3-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-pltcl | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-pltcl | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-pltcl | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-pltcl-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-pltcl-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-pltcl-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-private-devel | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-private-libs | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-private-libs-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-server | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-server | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-server | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-server-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-server-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-server-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-server-devel | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-server-devel | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-server-devel | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-server-devel-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-server-devel-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-server-devel-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-static | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-static | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-static | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-test | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-test | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-test | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-test-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-test-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-test-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-test-rpm-macros | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-test-rpm-macros | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-test-rpm-macros | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-upgrade | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-upgrade | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-upgrade | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-upgrade-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-upgrade-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-upgrade-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |
postgresql-upgrade-devel | 12.20-1.module+el8.10.0+22214+9beb89d6 | [RHSA-2024:6000](https://access.redhat.com/errata/RHSA-2024:6000) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-upgrade-devel | 13.16-1.module+el8.10.0+22213+5adc94fe | [RHSA-2024:6018](https://access.redhat.com/errata/RHSA-2024:6018) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-upgrade-devel | 15.8-1.module+el8.10.0+22212+a51fe170 | [RHSA-2024:6001](https://access.redhat.com/errata/RHSA-2024:6001) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4317](https://access.redhat.com/security/cve/CVE-2024-4317), [CVE-2024-7348](https://access.redhat.com/security/cve/CVE-2024-7348))
postgresql-upgrade-devel-debuginfo | 12.20-1.module+el8.10.0+22214+9beb89d6 | |
postgresql-upgrade-devel-debuginfo | 13.16-1.module+el8.10.0+22213+5adc94fe | |
postgresql-upgrade-devel-debuginfo | 15.8-1.module+el8.10.0+22212+a51fe170 | |

