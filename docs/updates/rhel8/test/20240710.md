## 2024-07-10

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.15-1.rh8.cern | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.32-1.el8_10 | |
aspnetcore-targeting-pack-6.0 | 6.0.32-1.el8_10 | |
dotnet-apphost-pack-6.0 | 6.0.32-1.el8_10 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.32-1.el8_10 | |
dotnet-hostfxr-6.0 | 6.0.32-1.el8_10 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.32-1.el8_10 | |
dotnet-runtime-6.0 | 6.0.32-1.el8_10 | |
dotnet-runtime-6.0-debuginfo | 6.0.32-1.el8_10 | |
dotnet-sdk-6.0 | 6.0.132-1.el8_10 | |
dotnet-sdk-6.0-debuginfo | 6.0.132-1.el8_10 | |
dotnet-targeting-pack-6.0 | 6.0.32-1.el8_10 | |
dotnet-templates-6.0 | 6.0.132-1.el8_10 | |
dotnet6.0-debuginfo | 6.0.132-1.el8_10 | |
dotnet6.0-debugsource | 6.0.132-1.el8_10 | |
qemu-guest-agent | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-guest-agent-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-img | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-img-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-block-curl | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-block-curl-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-block-gluster | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-block-gluster-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-block-iscsi | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-block-iscsi-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-block-rbd | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-block-rbd-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-block-ssh | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-block-ssh-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-common | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-common-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-core | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-core-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-debugsource | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-docs | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-hw-usbredir | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-hw-usbredir-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-ui-opengl | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-ui-opengl-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-ui-spice | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-ui-spice-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.32-1.el8_10 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.32-1.el8_10 | |
dotnet-runtime-6.0-debuginfo | 6.0.32-1.el8_10 | |
dotnet-sdk-6.0-debuginfo | 6.0.132-1.el8_10 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.132-1.el8_10 | |
dotnet6.0-debuginfo | 6.0.132-1.el8_10 | |
dotnet6.0-debugsource | 6.0.132-1.el8_10 | |
qemu-kvm-tests | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.15-1.rh8.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.32-1.el8_10 | |
aspnetcore-targeting-pack-6.0 | 6.0.32-1.el8_10 | |
dotnet-apphost-pack-6.0 | 6.0.32-1.el8_10 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.32-1.el8_10 | |
dotnet-hostfxr-6.0 | 6.0.32-1.el8_10 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.32-1.el8_10 | |
dotnet-runtime-6.0 | 6.0.32-1.el8_10 | |
dotnet-runtime-6.0-debuginfo | 6.0.32-1.el8_10 | |
dotnet-sdk-6.0 | 6.0.132-1.el8_10 | |
dotnet-sdk-6.0-debuginfo | 6.0.132-1.el8_10 | |
dotnet-targeting-pack-6.0 | 6.0.32-1.el8_10 | |
dotnet-templates-6.0 | 6.0.132-1.el8_10 | |
dotnet6.0-debuginfo | 6.0.132-1.el8_10 | |
dotnet6.0-debugsource | 6.0.132-1.el8_10 | |
qemu-guest-agent | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-guest-agent-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-img | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-img-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-block-curl | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-block-curl-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-block-iscsi | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-block-iscsi-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-block-rbd | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-block-rbd-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-block-ssh | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-block-ssh-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-common | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-common-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-core | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))
qemu-kvm-core-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-debuginfo | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-debugsource | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | |
qemu-kvm-docs | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.32-1.el8_10 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.32-1.el8_10 | |
dotnet-runtime-6.0-debuginfo | 6.0.32-1.el8_10 | |
dotnet-sdk-6.0-debuginfo | 6.0.132-1.el8_10 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.132-1.el8_10 | |
dotnet6.0-debuginfo | 6.0.132-1.el8_10 | |
dotnet6.0-debugsource | 6.0.132-1.el8_10 | |
qemu-kvm-tests | 6.2.0-50.module+el8.10.0+22027+db0a70a4 | [RHSA-2024:4420](https://access.redhat.com/errata/RHSA-2024:4420) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4467](https://access.redhat.com/security/cve/CVE-2024-4467))

