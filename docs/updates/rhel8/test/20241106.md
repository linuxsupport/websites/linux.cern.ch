## 2024-11-06

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bzip2 | 1.0.6-27.el8_10 | |
bzip2-debuginfo | 1.0.6-27.el8_10 | |
bzip2-debugsource | 1.0.6-27.el8_10 | |
bzip2-devel | 1.0.6-27.el8_10 | |
bzip2-libs | 1.0.6-27.el8_10 | |
bzip2-libs-debuginfo | 1.0.6-27.el8_10 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bzip2 | 1.0.6-27.el8_10 | |
bzip2-debuginfo | 1.0.6-27.el8_10 | |
bzip2-debugsource | 1.0.6-27.el8_10 | |
bzip2-devel | 1.0.6-27.el8_10 | |
bzip2-libs | 1.0.6-27.el8_10 | |
bzip2-libs-debuginfo | 1.0.6-27.el8_10 | |

