## 2024-11-26

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_553.30.1.el8_10.rh8.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-553.30.1.el8_10 | |
bpftool-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel | 4.18.0-553.30.1.el8_10 | |
kernel-abi-stablelists | 4.18.0-553.30.1.el8_10 | |
kernel-core | 4.18.0-553.30.1.el8_10 | |
kernel-cross-headers | 4.18.0-553.30.1.el8_10 | |
kernel-debug | 4.18.0-553.30.1.el8_10 | |
kernel-debug-core | 4.18.0-553.30.1.el8_10 | |
kernel-debug-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel-debug-devel | 4.18.0-553.30.1.el8_10 | |
kernel-debug-modules | 4.18.0-553.30.1.el8_10 | |
kernel-debug-modules-extra | 4.18.0-553.30.1.el8_10 | |
kernel-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel-debuginfo-common-x86_64 | 4.18.0-553.30.1.el8_10 | |
kernel-devel | 4.18.0-553.30.1.el8_10 | |
kernel-doc | 4.18.0-553.30.1.el8_10 | |
kernel-headers | 4.18.0-553.30.1.el8_10 | |
kernel-modules | 4.18.0-553.30.1.el8_10 | |
kernel-modules-extra | 4.18.0-553.30.1.el8_10 | |
kernel-tools | 4.18.0-553.30.1.el8_10 | |
kernel-tools-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel-tools-libs | 4.18.0-553.30.1.el8_10 | |
perf | 4.18.0-553.30.1.el8_10 | |
perf-debuginfo | 4.18.0-553.30.1.el8_10 | |
python3-perf | 4.18.0-553.30.1.el8_10 | |
python3-perf-debuginfo | 4.18.0-553.30.1.el8_10 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
perl-App-cpanminus | 1.7044-6.module+el8.10.0+22411+409a293e | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-App-cpanminus | 1.7044-6.module+el8.10.0+22411+7e3d2305 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-App-cpanminus | 1.7044-6.module+el8.10.0+22411+85254afd | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-App-cpanminus | 1.7044-6.module+el8.10.0+22411+e14e3526 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-CPAN-DistnameInfo | 0.12-13.module+el8.10.0+22411+409a293e | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-CPAN-DistnameInfo | 0.12-13.module+el8.10.0+22411+7e3d2305 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-CPAN-DistnameInfo | 0.12-13.module+el8.10.0+22411+e14e3526 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-CPAN-Meta-Check | 0.014-6.module+el8.10.0+22411+409a293e | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-CPAN-Meta-Check | 0.014-6.module+el8.10.0+22411+7e3d2305 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-CPAN-Meta-Check | 0.014-6.module+el8.10.0+22411+85254afd | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-CPAN-Meta-Check | 0.014-6.module+el8.10.0+22411+e14e3526 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-File-pushd | 1.014-6.module+el8.10.0+22411+409a293e | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-File-pushd | 1.014-6.module+el8.10.0+22411+7e3d2305 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-File-pushd | 1.014-6.module+el8.10.0+22411+85254afd | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-File-pushd | 1.014-6.module+el8.10.0+22411+e14e3526 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Module-CPANfile | 1.1002-7.module+el8.10.0+22411+409a293e | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Module-CPANfile | 1.1002-7.module+el8.10.0+22411+7e3d2305 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Module-CPANfile | 1.1002-7.module+el8.10.0+22411+85254afd | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Module-CPANfile | 1.1002-7.module+el8.10.0+22411+e14e3526 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Parse-PMFile | 0.41-7.module+el8.10.0+22411+409a293e | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Parse-PMFile | 0.41-7.module+el8.10.0+22411+7e3d2305 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Parse-PMFile | 0.41-7.module+el8.10.0+22411+85254afd | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Parse-PMFile | 0.41-7.module+el8.10.0+22411+e14e3526 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-String-ShellQuote | 1.04-24.module+el8.10.0+22411+409a293e | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-String-ShellQuote | 1.04-24.module+el8.10.0+22411+7e3d2305 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-String-ShellQuote | 1.04-24.module+el8.10.0+22411+85254afd | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-String-ShellQuote | 1.04-24.module+el8.10.0+22411+e14e3526 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))

### rt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-553.30.1.rt7.371.el8_10 | |
kernel-rt-core | 4.18.0-553.30.1.rt7.371.el8_10 | |
kernel-rt-debug | 4.18.0-553.30.1.rt7.371.el8_10 | |
kernel-rt-debug-core | 4.18.0-553.30.1.rt7.371.el8_10 | |
kernel-rt-debug-debuginfo | 4.18.0-553.30.1.rt7.371.el8_10 | |
kernel-rt-debug-devel | 4.18.0-553.30.1.rt7.371.el8_10 | |
kernel-rt-debug-modules | 4.18.0-553.30.1.rt7.371.el8_10 | |
kernel-rt-debug-modules-extra | 4.18.0-553.30.1.rt7.371.el8_10 | |
kernel-rt-debuginfo | 4.18.0-553.30.1.rt7.371.el8_10 | |
kernel-rt-debuginfo-common-x86_64 | 4.18.0-553.30.1.rt7.371.el8_10 | |
kernel-rt-devel | 4.18.0-553.30.1.rt7.371.el8_10 | |
kernel-rt-modules | 4.18.0-553.30.1.rt7.371.el8_10 | |
kernel-rt-modules-extra | 4.18.0-553.30.1.rt7.371.el8_10 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel-debug-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel-debuginfo-common-x86_64 | 4.18.0-553.30.1.el8_10 | |
kernel-tools-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel-tools-libs-devel | 4.18.0-553.30.1.el8_10 | |
perf-debuginfo | 4.18.0-553.30.1.el8_10 | |
python3-perf-debuginfo | 4.18.0-553.30.1.el8_10 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_553.30.1.el8_10.rh8.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-553.30.1.el8_10 | |
bpftool-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel | 4.18.0-553.30.1.el8_10 | |
kernel-abi-stablelists | 4.18.0-553.30.1.el8_10 | |
kernel-core | 4.18.0-553.30.1.el8_10 | |
kernel-cross-headers | 4.18.0-553.30.1.el8_10 | |
kernel-debug | 4.18.0-553.30.1.el8_10 | |
kernel-debug-core | 4.18.0-553.30.1.el8_10 | |
kernel-debug-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel-debug-devel | 4.18.0-553.30.1.el8_10 | |
kernel-debug-modules | 4.18.0-553.30.1.el8_10 | |
kernel-debug-modules-extra | 4.18.0-553.30.1.el8_10 | |
kernel-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel-debuginfo-common-aarch64 | 4.18.0-553.30.1.el8_10 | |
kernel-devel | 4.18.0-553.30.1.el8_10 | |
kernel-doc | 4.18.0-553.30.1.el8_10 | |
kernel-headers | 4.18.0-553.30.1.el8_10 | |
kernel-modules | 4.18.0-553.30.1.el8_10 | |
kernel-modules-extra | 4.18.0-553.30.1.el8_10 | |
kernel-tools | 4.18.0-553.30.1.el8_10 | |
kernel-tools-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel-tools-libs | 4.18.0-553.30.1.el8_10 | |
perf | 4.18.0-553.30.1.el8_10 | |
perf-debuginfo | 4.18.0-553.30.1.el8_10 | |
python3-perf | 4.18.0-553.30.1.el8_10 | |
python3-perf-debuginfo | 4.18.0-553.30.1.el8_10 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
perl-App-cpanminus | 1.7044-6.module+el8.10.0+22411+409a293e | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-App-cpanminus | 1.7044-6.module+el8.10.0+22411+7e3d2305 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-App-cpanminus | 1.7044-6.module+el8.10.0+22411+85254afd | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-App-cpanminus | 1.7044-6.module+el8.10.0+22411+e14e3526 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-CPAN-DistnameInfo | 0.12-13.module+el8.10.0+22411+409a293e | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-CPAN-DistnameInfo | 0.12-13.module+el8.10.0+22411+7e3d2305 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-CPAN-DistnameInfo | 0.12-13.module+el8.10.0+22411+e14e3526 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-CPAN-Meta-Check | 0.014-6.module+el8.10.0+22411+409a293e | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-CPAN-Meta-Check | 0.014-6.module+el8.10.0+22411+7e3d2305 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-CPAN-Meta-Check | 0.014-6.module+el8.10.0+22411+85254afd | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-CPAN-Meta-Check | 0.014-6.module+el8.10.0+22411+e14e3526 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-File-pushd | 1.014-6.module+el8.10.0+22411+409a293e | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-File-pushd | 1.014-6.module+el8.10.0+22411+7e3d2305 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-File-pushd | 1.014-6.module+el8.10.0+22411+85254afd | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-File-pushd | 1.014-6.module+el8.10.0+22411+e14e3526 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Module-CPANfile | 1.1002-7.module+el8.10.0+22411+409a293e | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Module-CPANfile | 1.1002-7.module+el8.10.0+22411+7e3d2305 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Module-CPANfile | 1.1002-7.module+el8.10.0+22411+85254afd | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Module-CPANfile | 1.1002-7.module+el8.10.0+22411+e14e3526 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Parse-PMFile | 0.41-7.module+el8.10.0+22411+409a293e | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Parse-PMFile | 0.41-7.module+el8.10.0+22411+7e3d2305 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Parse-PMFile | 0.41-7.module+el8.10.0+22411+85254afd | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-Parse-PMFile | 0.41-7.module+el8.10.0+22411+e14e3526 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-String-ShellQuote | 1.04-24.module+el8.10.0+22411+409a293e | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-String-ShellQuote | 1.04-24.module+el8.10.0+22411+7e3d2305 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-String-ShellQuote | 1.04-24.module+el8.10.0+22411+85254afd | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))
perl-String-ShellQuote | 1.04-24.module+el8.10.0+22411+e14e3526 | [RHSA-2024:10219](https://access.redhat.com/errata/RHSA-2024:10219) | <div class="adv_s">Security Advisory</div> ([CVE-2024-45321](https://access.redhat.com/security/cve/CVE-2024-45321))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel-debug-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel-debuginfo-common-aarch64 | 4.18.0-553.30.1.el8_10 | |
kernel-tools-debuginfo | 4.18.0-553.30.1.el8_10 | |
kernel-tools-libs-devel | 4.18.0-553.30.1.el8_10 | |
perf-debuginfo | 4.18.0-553.30.1.el8_10 | |
python3-perf-debuginfo | 4.18.0-553.30.1.el8_10 | |

