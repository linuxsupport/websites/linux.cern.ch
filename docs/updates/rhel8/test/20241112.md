## 2024-11-12

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gstreamer1-plugins-base | 1.16.1-4.el8_10 | [RHSA-2024:9056](https://access.redhat.com/errata/RHSA-2024:9056) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4453](https://access.redhat.com/security/cve/CVE-2024-4453))
gstreamer1-plugins-base-debuginfo | 1.16.1-4.el8_10 | |
gstreamer1-plugins-base-debugsource | 1.16.1-4.el8_10 | |
gstreamer1-plugins-base-devel | 1.16.1-4.el8_10 | [RHSA-2024:9056](https://access.redhat.com/errata/RHSA-2024:9056) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4453](https://access.redhat.com/security/cve/CVE-2024-4453))
gstreamer1-plugins-base-tools-debuginfo | 1.16.1-4.el8_10 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gstreamer1-plugins-base | 1.16.1-4.el8_10 | [RHSA-2024:9056](https://access.redhat.com/errata/RHSA-2024:9056) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4453](https://access.redhat.com/security/cve/CVE-2024-4453))
gstreamer1-plugins-base-debuginfo | 1.16.1-4.el8_10 | |
gstreamer1-plugins-base-debugsource | 1.16.1-4.el8_10 | |
gstreamer1-plugins-base-devel | 1.16.1-4.el8_10 | [RHSA-2024:9056](https://access.redhat.com/errata/RHSA-2024:9056) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4453](https://access.redhat.com/security/cve/CVE-2024-4453))
gstreamer1-plugins-base-tools-debuginfo | 1.16.1-4.el8_10 | |

