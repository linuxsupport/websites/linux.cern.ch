## 2024-04-18

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-17-openjdk | 17.0.11.0.9-2.el8 | |
java-17-openjdk-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-debugsource | 17.0.11.0.9-2.el8 | |
java-17-openjdk-demo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-devel | 17.0.11.0.9-2.el8 | |
java-17-openjdk-devel-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-headless | 17.0.11.0.9-2.el8 | |
java-17-openjdk-headless-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-javadoc | 17.0.11.0.9-2.el8 | |
java-17-openjdk-javadoc-zip | 17.0.11.0.9-2.el8 | |
java-17-openjdk-jmods | 17.0.11.0.9-2.el8 | |
java-17-openjdk-src | 17.0.11.0.9-2.el8 | |
java-17-openjdk-static-libs | 17.0.11.0.9-2.el8 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-17-openjdk-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-debugsource | 17.0.11.0.9-2.el8 | |
java-17-openjdk-demo-fastdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-demo-slowdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-devel-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-devel-fastdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-devel-fastdebug-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-devel-slowdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-devel-slowdebug-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-fastdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-fastdebug-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-headless-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-headless-fastdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-headless-fastdebug-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-headless-slowdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-headless-slowdebug-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-jmods-fastdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-jmods-slowdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-slowdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-slowdebug-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-src-fastdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-src-slowdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-static-libs-fastdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-static-libs-slowdebug | 17.0.11.0.9-2.el8 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-17-openjdk | 17.0.11.0.9-2.el8 | |
java-17-openjdk-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-debugsource | 17.0.11.0.9-2.el8 | |
java-17-openjdk-demo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-devel | 17.0.11.0.9-2.el8 | |
java-17-openjdk-devel-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-headless | 17.0.11.0.9-2.el8 | |
java-17-openjdk-headless-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-javadoc | 17.0.11.0.9-2.el8 | |
java-17-openjdk-javadoc-zip | 17.0.11.0.9-2.el8 | |
java-17-openjdk-jmods | 17.0.11.0.9-2.el8 | |
java-17-openjdk-src | 17.0.11.0.9-2.el8 | |
java-17-openjdk-static-libs | 17.0.11.0.9-2.el8 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-17-openjdk-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-debugsource | 17.0.11.0.9-2.el8 | |
java-17-openjdk-demo-fastdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-demo-slowdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-devel-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-devel-fastdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-devel-fastdebug-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-devel-slowdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-devel-slowdebug-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-fastdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-fastdebug-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-headless-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-headless-fastdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-headless-fastdebug-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-headless-slowdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-headless-slowdebug-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-jmods-fastdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-jmods-slowdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-slowdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-slowdebug-debuginfo | 17.0.11.0.9-2.el8 | |
java-17-openjdk-src-fastdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-src-slowdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-static-libs-fastdebug | 17.0.11.0.9-2.el8 | |
java-17-openjdk-static-libs-slowdebug | 17.0.11.0.9-2.el8 | |

