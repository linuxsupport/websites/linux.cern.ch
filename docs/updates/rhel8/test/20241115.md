## 2024-11-15

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
binutils | 2.30-125.el8_10 | |
binutils-debuginfo | 2.30-125.el8_10 | |
binutils-debugsource | 2.30-125.el8_10 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
binutils-debuginfo | 2.30-125.el8_10 | |
binutils-debugsource | 2.30-125.el8_10 | |
binutils-devel | 2.30-125.el8_10 | |
evolution | 3.28.5-27.el8_10 | |
evolution-bogofilter | 3.28.5-27.el8_10 | |
evolution-bogofilter-debuginfo | 3.28.5-27.el8_10 | |
evolution-debuginfo | 3.28.5-27.el8_10 | |
evolution-debugsource | 3.28.5-27.el8_10 | |
evolution-help | 3.28.5-27.el8_10 | |
evolution-langpacks | 3.28.5-27.el8_10 | |
evolution-pst | 3.28.5-27.el8_10 | |
evolution-pst-debuginfo | 3.28.5-27.el8_10 | |
evolution-spamassassin | 3.28.5-27.el8_10 | |
evolution-spamassassin-debuginfo | 3.28.5-27.el8_10 | |
squid | 4.15-10.module+el8.10.0+22489+b920747d.3 | [RHSA-2024:9644](https://access.redhat.com/errata/RHSA-2024:9644) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23638](https://access.redhat.com/security/cve/CVE-2024-23638), [CVE-2024-45802](https://access.redhat.com/security/cve/CVE-2024-45802))
squid-debuginfo | 4.15-10.module+el8.10.0+22489+b920747d.3 | |
squid-debugsource | 4.15-10.module+el8.10.0+22489+b920747d.3 | |
webkit2gtk3 | 2.46.3-1.el8_10 | [RHSA-2024:9636](https://access.redhat.com/errata/RHSA-2024:9636) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23271](https://access.redhat.com/security/cve/CVE-2024-23271), [CVE-2024-27820](https://access.redhat.com/security/cve/CVE-2024-27820), [CVE-2024-27838](https://access.redhat.com/security/cve/CVE-2024-27838), [CVE-2024-27851](https://access.redhat.com/security/cve/CVE-2024-27851), [CVE-2024-40779](https://access.redhat.com/security/cve/CVE-2024-40779), [CVE-2024-40780](https://access.redhat.com/security/cve/CVE-2024-40780), [CVE-2024-40782](https://access.redhat.com/security/cve/CVE-2024-40782), [CVE-2024-40789](https://access.redhat.com/security/cve/CVE-2024-40789), [CVE-2024-40866](https://access.redhat.com/security/cve/CVE-2024-40866), [CVE-2024-44185](https://access.redhat.com/security/cve/CVE-2024-44185), [CVE-2024-44187](https://access.redhat.com/security/cve/CVE-2024-44187), [CVE-2024-44244](https://access.redhat.com/security/cve/CVE-2024-44244), [CVE-2024-44296](https://access.redhat.com/security/cve/CVE-2024-44296), [CVE-2024-4558](https://access.redhat.com/security/cve/CVE-2024-4558))
webkit2gtk3-debuginfo | 2.46.3-1.el8_10 | |
webkit2gtk3-debugsource | 2.46.3-1.el8_10 | |
webkit2gtk3-devel | 2.46.3-1.el8_10 | [RHSA-2024:9636](https://access.redhat.com/errata/RHSA-2024:9636) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23271](https://access.redhat.com/security/cve/CVE-2024-23271), [CVE-2024-27820](https://access.redhat.com/security/cve/CVE-2024-27820), [CVE-2024-27838](https://access.redhat.com/security/cve/CVE-2024-27838), [CVE-2024-27851](https://access.redhat.com/security/cve/CVE-2024-27851), [CVE-2024-40779](https://access.redhat.com/security/cve/CVE-2024-40779), [CVE-2024-40780](https://access.redhat.com/security/cve/CVE-2024-40780), [CVE-2024-40782](https://access.redhat.com/security/cve/CVE-2024-40782), [CVE-2024-40789](https://access.redhat.com/security/cve/CVE-2024-40789), [CVE-2024-40866](https://access.redhat.com/security/cve/CVE-2024-40866), [CVE-2024-44185](https://access.redhat.com/security/cve/CVE-2024-44185), [CVE-2024-44187](https://access.redhat.com/security/cve/CVE-2024-44187), [CVE-2024-44244](https://access.redhat.com/security/cve/CVE-2024-44244), [CVE-2024-44296](https://access.redhat.com/security/cve/CVE-2024-44296), [CVE-2024-4558](https://access.redhat.com/security/cve/CVE-2024-4558))
webkit2gtk3-devel-debuginfo | 2.46.3-1.el8_10 | |
webkit2gtk3-jsc | 2.46.3-1.el8_10 | [RHSA-2024:9636](https://access.redhat.com/errata/RHSA-2024:9636) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23271](https://access.redhat.com/security/cve/CVE-2024-23271), [CVE-2024-27820](https://access.redhat.com/security/cve/CVE-2024-27820), [CVE-2024-27838](https://access.redhat.com/security/cve/CVE-2024-27838), [CVE-2024-27851](https://access.redhat.com/security/cve/CVE-2024-27851), [CVE-2024-40779](https://access.redhat.com/security/cve/CVE-2024-40779), [CVE-2024-40780](https://access.redhat.com/security/cve/CVE-2024-40780), [CVE-2024-40782](https://access.redhat.com/security/cve/CVE-2024-40782), [CVE-2024-40789](https://access.redhat.com/security/cve/CVE-2024-40789), [CVE-2024-40866](https://access.redhat.com/security/cve/CVE-2024-40866), [CVE-2024-44185](https://access.redhat.com/security/cve/CVE-2024-44185), [CVE-2024-44187](https://access.redhat.com/security/cve/CVE-2024-44187), [CVE-2024-44244](https://access.redhat.com/security/cve/CVE-2024-44244), [CVE-2024-44296](https://access.redhat.com/security/cve/CVE-2024-44296), [CVE-2024-4558](https://access.redhat.com/security/cve/CVE-2024-4558))
webkit2gtk3-jsc-debuginfo | 2.46.3-1.el8_10 | |
webkit2gtk3-jsc-devel | 2.46.3-1.el8_10 | [RHSA-2024:9636](https://access.redhat.com/errata/RHSA-2024:9636) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23271](https://access.redhat.com/security/cve/CVE-2024-23271), [CVE-2024-27820](https://access.redhat.com/security/cve/CVE-2024-27820), [CVE-2024-27838](https://access.redhat.com/security/cve/CVE-2024-27838), [CVE-2024-27851](https://access.redhat.com/security/cve/CVE-2024-27851), [CVE-2024-40779](https://access.redhat.com/security/cve/CVE-2024-40779), [CVE-2024-40780](https://access.redhat.com/security/cve/CVE-2024-40780), [CVE-2024-40782](https://access.redhat.com/security/cve/CVE-2024-40782), [CVE-2024-40789](https://access.redhat.com/security/cve/CVE-2024-40789), [CVE-2024-40866](https://access.redhat.com/security/cve/CVE-2024-40866), [CVE-2024-44185](https://access.redhat.com/security/cve/CVE-2024-44185), [CVE-2024-44187](https://access.redhat.com/security/cve/CVE-2024-44187), [CVE-2024-44244](https://access.redhat.com/security/cve/CVE-2024-44244), [CVE-2024-44296](https://access.redhat.com/security/cve/CVE-2024-44296), [CVE-2024-4558](https://access.redhat.com/security/cve/CVE-2024-4558))
webkit2gtk3-jsc-devel-debuginfo | 2.46.3-1.el8_10 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
evolution-bogofilter-debuginfo | 3.28.5-27.el8_10 | |
evolution-debuginfo | 3.28.5-27.el8_10 | |
evolution-debugsource | 3.28.5-27.el8_10 | |
evolution-devel | 3.28.5-27.el8_10 | |
evolution-pst-debuginfo | 3.28.5-27.el8_10 | |
evolution-spamassassin-debuginfo | 3.28.5-27.el8_10 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
binutils | 2.30-125.el8_10 | |
binutils-debuginfo | 2.30-125.el8_10 | |
binutils-debugsource | 2.30-125.el8_10 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
binutils-debuginfo | 2.30-125.el8_10 | |
binutils-debugsource | 2.30-125.el8_10 | |
binutils-devel | 2.30-125.el8_10 | |
evolution | 3.28.5-27.el8_10 | |
evolution-bogofilter | 3.28.5-27.el8_10 | |
evolution-bogofilter-debuginfo | 3.28.5-27.el8_10 | |
evolution-debuginfo | 3.28.5-27.el8_10 | |
evolution-debugsource | 3.28.5-27.el8_10 | |
evolution-help | 3.28.5-27.el8_10 | |
evolution-langpacks | 3.28.5-27.el8_10 | |
evolution-pst | 3.28.5-27.el8_10 | |
evolution-pst-debuginfo | 3.28.5-27.el8_10 | |
evolution-spamassassin | 3.28.5-27.el8_10 | |
evolution-spamassassin-debuginfo | 3.28.5-27.el8_10 | |
squid | 4.15-10.module+el8.10.0+22489+b920747d.3 | [RHSA-2024:9644](https://access.redhat.com/errata/RHSA-2024:9644) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23638](https://access.redhat.com/security/cve/CVE-2024-23638), [CVE-2024-45802](https://access.redhat.com/security/cve/CVE-2024-45802))
squid-debuginfo | 4.15-10.module+el8.10.0+22489+b920747d.3 | |
squid-debugsource | 4.15-10.module+el8.10.0+22489+b920747d.3 | |
webkit2gtk3 | 2.46.3-1.el8_10 | [RHSA-2024:9636](https://access.redhat.com/errata/RHSA-2024:9636) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23271](https://access.redhat.com/security/cve/CVE-2024-23271), [CVE-2024-27820](https://access.redhat.com/security/cve/CVE-2024-27820), [CVE-2024-27838](https://access.redhat.com/security/cve/CVE-2024-27838), [CVE-2024-27851](https://access.redhat.com/security/cve/CVE-2024-27851), [CVE-2024-40779](https://access.redhat.com/security/cve/CVE-2024-40779), [CVE-2024-40780](https://access.redhat.com/security/cve/CVE-2024-40780), [CVE-2024-40782](https://access.redhat.com/security/cve/CVE-2024-40782), [CVE-2024-40789](https://access.redhat.com/security/cve/CVE-2024-40789), [CVE-2024-40866](https://access.redhat.com/security/cve/CVE-2024-40866), [CVE-2024-44185](https://access.redhat.com/security/cve/CVE-2024-44185), [CVE-2024-44187](https://access.redhat.com/security/cve/CVE-2024-44187), [CVE-2024-44244](https://access.redhat.com/security/cve/CVE-2024-44244), [CVE-2024-44296](https://access.redhat.com/security/cve/CVE-2024-44296), [CVE-2024-4558](https://access.redhat.com/security/cve/CVE-2024-4558))
webkit2gtk3-debuginfo | 2.46.3-1.el8_10 | |
webkit2gtk3-debugsource | 2.46.3-1.el8_10 | |
webkit2gtk3-devel | 2.46.3-1.el8_10 | [RHSA-2024:9636](https://access.redhat.com/errata/RHSA-2024:9636) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23271](https://access.redhat.com/security/cve/CVE-2024-23271), [CVE-2024-27820](https://access.redhat.com/security/cve/CVE-2024-27820), [CVE-2024-27838](https://access.redhat.com/security/cve/CVE-2024-27838), [CVE-2024-27851](https://access.redhat.com/security/cve/CVE-2024-27851), [CVE-2024-40779](https://access.redhat.com/security/cve/CVE-2024-40779), [CVE-2024-40780](https://access.redhat.com/security/cve/CVE-2024-40780), [CVE-2024-40782](https://access.redhat.com/security/cve/CVE-2024-40782), [CVE-2024-40789](https://access.redhat.com/security/cve/CVE-2024-40789), [CVE-2024-40866](https://access.redhat.com/security/cve/CVE-2024-40866), [CVE-2024-44185](https://access.redhat.com/security/cve/CVE-2024-44185), [CVE-2024-44187](https://access.redhat.com/security/cve/CVE-2024-44187), [CVE-2024-44244](https://access.redhat.com/security/cve/CVE-2024-44244), [CVE-2024-44296](https://access.redhat.com/security/cve/CVE-2024-44296), [CVE-2024-4558](https://access.redhat.com/security/cve/CVE-2024-4558))
webkit2gtk3-devel-debuginfo | 2.46.3-1.el8_10 | |
webkit2gtk3-jsc | 2.46.3-1.el8_10 | [RHSA-2024:9636](https://access.redhat.com/errata/RHSA-2024:9636) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23271](https://access.redhat.com/security/cve/CVE-2024-23271), [CVE-2024-27820](https://access.redhat.com/security/cve/CVE-2024-27820), [CVE-2024-27838](https://access.redhat.com/security/cve/CVE-2024-27838), [CVE-2024-27851](https://access.redhat.com/security/cve/CVE-2024-27851), [CVE-2024-40779](https://access.redhat.com/security/cve/CVE-2024-40779), [CVE-2024-40780](https://access.redhat.com/security/cve/CVE-2024-40780), [CVE-2024-40782](https://access.redhat.com/security/cve/CVE-2024-40782), [CVE-2024-40789](https://access.redhat.com/security/cve/CVE-2024-40789), [CVE-2024-40866](https://access.redhat.com/security/cve/CVE-2024-40866), [CVE-2024-44185](https://access.redhat.com/security/cve/CVE-2024-44185), [CVE-2024-44187](https://access.redhat.com/security/cve/CVE-2024-44187), [CVE-2024-44244](https://access.redhat.com/security/cve/CVE-2024-44244), [CVE-2024-44296](https://access.redhat.com/security/cve/CVE-2024-44296), [CVE-2024-4558](https://access.redhat.com/security/cve/CVE-2024-4558))
webkit2gtk3-jsc-debuginfo | 2.46.3-1.el8_10 | |
webkit2gtk3-jsc-devel | 2.46.3-1.el8_10 | [RHSA-2024:9636](https://access.redhat.com/errata/RHSA-2024:9636) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23271](https://access.redhat.com/security/cve/CVE-2024-23271), [CVE-2024-27820](https://access.redhat.com/security/cve/CVE-2024-27820), [CVE-2024-27838](https://access.redhat.com/security/cve/CVE-2024-27838), [CVE-2024-27851](https://access.redhat.com/security/cve/CVE-2024-27851), [CVE-2024-40779](https://access.redhat.com/security/cve/CVE-2024-40779), [CVE-2024-40780](https://access.redhat.com/security/cve/CVE-2024-40780), [CVE-2024-40782](https://access.redhat.com/security/cve/CVE-2024-40782), [CVE-2024-40789](https://access.redhat.com/security/cve/CVE-2024-40789), [CVE-2024-40866](https://access.redhat.com/security/cve/CVE-2024-40866), [CVE-2024-44185](https://access.redhat.com/security/cve/CVE-2024-44185), [CVE-2024-44187](https://access.redhat.com/security/cve/CVE-2024-44187), [CVE-2024-44244](https://access.redhat.com/security/cve/CVE-2024-44244), [CVE-2024-44296](https://access.redhat.com/security/cve/CVE-2024-44296), [CVE-2024-4558](https://access.redhat.com/security/cve/CVE-2024-4558))
webkit2gtk3-jsc-devel-debuginfo | 2.46.3-1.el8_10 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
evolution-bogofilter-debuginfo | 3.28.5-27.el8_10 | |
evolution-debuginfo | 3.28.5-27.el8_10 | |
evolution-debugsource | 3.28.5-27.el8_10 | |
evolution-devel | 3.28.5-27.el8_10 | [RHBA-2024:9685](https://access.redhat.com/errata/RHBA-2024:9685) | <div class="adv_b">Bug Fix Advisory</div>
evolution-pst-debuginfo | 3.28.5-27.el8_10 | |
evolution-spamassassin-debuginfo | 3.28.5-27.el8_10 | |

