## 2024-10-09

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 1.0.2-1.rh8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 1.0.2-1.rh8.cern | |

