## 2023-10-24

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-linuxsupport-access | 1.9-2.rh8.cern | |
hepix | 4.10.8-0.rh8.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
platform-python | 3.6.8-51.el8_8.2 | [RHSA-2023:5997](https://access.redhat.com/errata/RHSA-2023:5997) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-debuginfo | 3.6.8-51.el8_8.2 | |
python3-debugsource | 3.6.8-51.el8_8.2 | |
python3-libs | 3.6.8-51.el8_8.2 | [RHSA-2023:5997](https://access.redhat.com/errata/RHSA-2023:5997) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-test | 3.6.8-51.el8_8.2 | [RHSA-2023:5997](https://access.redhat.com/errata/RHSA-2023:5997) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
platform-python-debug | 3.6.8-51.el8_8.2 | [RHSA-2023:5997](https://access.redhat.com/errata/RHSA-2023:5997) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
platform-python-devel | 3.6.8-51.el8_8.2 | [RHSA-2023:5997](https://access.redhat.com/errata/RHSA-2023:5997) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2 | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | [RHSA-2023:5994](https://access.redhat.com/errata/RHSA-2023:5994) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-debug | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | [RHSA-2023:5994](https://access.redhat.com/errata/RHSA-2023:5994) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-debuginfo | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | |
python2-debugsource | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | |
python2-devel | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | [RHSA-2023:5994](https://access.redhat.com/errata/RHSA-2023:5994) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-libs | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | [RHSA-2023:5994](https://access.redhat.com/errata/RHSA-2023:5994) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-test | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | [RHSA-2023:5994](https://access.redhat.com/errata/RHSA-2023:5994) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-tkinter | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | [RHSA-2023:5994](https://access.redhat.com/errata/RHSA-2023:5994) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-tools | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | [RHSA-2023:5994](https://access.redhat.com/errata/RHSA-2023:5994) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-debuginfo | 3.6.8-51.el8_8.2 | |
python3-debugsource | 3.6.8-51.el8_8.2 | |
python3-idle | 3.6.8-51.el8_8.2 | [RHSA-2023:5997](https://access.redhat.com/errata/RHSA-2023:5997) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-tkinter | 3.6.8-51.el8_8.2 | [RHSA-2023:5997](https://access.redhat.com/errata/RHSA-2023:5997) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39 | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-debuginfo | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | |
python39-debugsource | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | |
python39-devel | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-idle | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-libs | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-rpm-macros | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-test | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-tkinter | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
varnish | 6.0.8-3.module+el8.8.0+20455+bdc2c048.1 | [RHSA-2023:5989](https://access.redhat.com/errata/RHSA-2023:5989) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
varnish-devel | 6.0.8-3.module+el8.8.0+20455+bdc2c048.1 | [RHSA-2023:5989](https://access.redhat.com/errata/RHSA-2023:5989) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
varnish-docs | 6.0.8-3.module+el8.8.0+20455+bdc2c048.1 | [RHSA-2023:5989](https://access.redhat.com/errata/RHSA-2023:5989) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python39-debug | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-linuxsupport-access | 1.9-2.rh8.cern | |
hepix | 4.10.8-0.rh8.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
platform-python | 3.6.8-51.el8_8.2 | [RHSA-2023:5997](https://access.redhat.com/errata/RHSA-2023:5997) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-debuginfo | 3.6.8-51.el8_8.2 | |
python3-debugsource | 3.6.8-51.el8_8.2 | |
python3-libs | 3.6.8-51.el8_8.2 | [RHSA-2023:5997](https://access.redhat.com/errata/RHSA-2023:5997) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-test | 3.6.8-51.el8_8.2 | [RHSA-2023:5997](https://access.redhat.com/errata/RHSA-2023:5997) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
platform-python-debug | 3.6.8-51.el8_8.2 | [RHSA-2023:5997](https://access.redhat.com/errata/RHSA-2023:5997) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
platform-python-devel | 3.6.8-51.el8_8.2 | [RHSA-2023:5997](https://access.redhat.com/errata/RHSA-2023:5997) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2 | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | [RHSA-2023:5994](https://access.redhat.com/errata/RHSA-2023:5994) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-debug | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | [RHSA-2023:5994](https://access.redhat.com/errata/RHSA-2023:5994) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-debuginfo | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | |
python2-debugsource | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | |
python2-devel | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | [RHSA-2023:5994](https://access.redhat.com/errata/RHSA-2023:5994) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-libs | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | [RHSA-2023:5994](https://access.redhat.com/errata/RHSA-2023:5994) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-test | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | [RHSA-2023:5994](https://access.redhat.com/errata/RHSA-2023:5994) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-tkinter | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | [RHSA-2023:5994](https://access.redhat.com/errata/RHSA-2023:5994) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-tools | 2.7.18-13.module+el8.8.0+20144+beed974d.2 | [RHSA-2023:5994](https://access.redhat.com/errata/RHSA-2023:5994) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-debuginfo | 3.6.8-51.el8_8.2 | |
python3-debugsource | 3.6.8-51.el8_8.2 | |
python3-idle | 3.6.8-51.el8_8.2 | [RHSA-2023:5997](https://access.redhat.com/errata/RHSA-2023:5997) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-tkinter | 3.6.8-51.el8_8.2 | [RHSA-2023:5997](https://access.redhat.com/errata/RHSA-2023:5997) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39 | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-debuginfo | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | |
python39-debugsource | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | |
python39-devel | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-idle | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-libs | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-rpm-macros | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-test | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-tkinter | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
varnish | 6.0.8-3.module+el8.8.0+20455+bdc2c048.1 | [RHSA-2023:5989](https://access.redhat.com/errata/RHSA-2023:5989) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
varnish-devel | 6.0.8-3.module+el8.8.0+20455+bdc2c048.1 | [RHSA-2023:5989](https://access.redhat.com/errata/RHSA-2023:5989) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
varnish-docs | 6.0.8-3.module+el8.8.0+20455+bdc2c048.1 | [RHSA-2023:5989](https://access.redhat.com/errata/RHSA-2023:5989) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python39-debug | 3.9.16-1.module+el8.8.0+20025+f2100191.2 | [RHSA-2023:5998](https://access.redhat.com/errata/RHSA-2023:5998) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))

