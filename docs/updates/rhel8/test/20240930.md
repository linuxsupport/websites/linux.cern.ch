## 2024-09-30

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grafana | 9.2.10-18.el8_10 | |
grafana-debuginfo | 9.2.10-18.el8_10 | |
grafana-debugsource | 9.2.10-18.el8_10 | |
grafana-selinux | 9.2.10-18.el8_10 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grafana | 9.2.10-18.el8_10 | |
grafana-debuginfo | 9.2.10-18.el8_10 | |
grafana-debugsource | 9.2.10-18.el8_10 | |
grafana-selinux | 9.2.10-18.el8_10 | |

