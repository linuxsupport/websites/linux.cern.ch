## 2024-09-17

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 128.2.0-1.el8_10 | [RHSA-2024:6682](https://access.redhat.com/errata/RHSA-2024:6682) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7652](https://access.redhat.com/security/cve/CVE-2024-7652), [CVE-2024-8381](https://access.redhat.com/security/cve/CVE-2024-8381), [CVE-2024-8382](https://access.redhat.com/security/cve/CVE-2024-8382), [CVE-2024-8383](https://access.redhat.com/security/cve/CVE-2024-8383), [CVE-2024-8384](https://access.redhat.com/security/cve/CVE-2024-8384), [CVE-2024-8385](https://access.redhat.com/security/cve/CVE-2024-8385), [CVE-2024-8386](https://access.redhat.com/security/cve/CVE-2024-8386), [CVE-2024-8387](https://access.redhat.com/security/cve/CVE-2024-8387))
firefox-debuginfo | 128.2.0-1.el8_10 | |
firefox-debugsource | 128.2.0-1.el8_10 | |
nss | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-debuginfo | 3.101.0-7.el8_8 | |
nss-debugsource | 3.101.0-7.el8_8 | |
nss-devel | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-debuginfo | 3.101.0-7.el8_8 | |
nss-softokn-devel | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-freebl | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-freebl-debuginfo | 3.101.0-7.el8_8 | |
nss-softokn-freebl-devel | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-sysinit | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-sysinit-debuginfo | 3.101.0-7.el8_8 | |
nss-tools | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-tools-debuginfo | 3.101.0-7.el8_8 | |
nss-util | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-util-debuginfo | 3.101.0-7.el8_8 | |
nss-util-devel | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
thunderbird | 128.2.0-1.el8_10 | [RHSA-2024:6684](https://access.redhat.com/errata/RHSA-2024:6684) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7652](https://access.redhat.com/security/cve/CVE-2024-7652), [CVE-2024-8381](https://access.redhat.com/security/cve/CVE-2024-8381), [CVE-2024-8382](https://access.redhat.com/security/cve/CVE-2024-8382), [CVE-2024-8384](https://access.redhat.com/security/cve/CVE-2024-8384), [CVE-2024-8385](https://access.redhat.com/security/cve/CVE-2024-8385), [CVE-2024-8386](https://access.redhat.com/security/cve/CVE-2024-8386), [CVE-2024-8387](https://access.redhat.com/security/cve/CVE-2024-8387), [CVE-2024-8394](https://access.redhat.com/security/cve/CVE-2024-8394))
thunderbird-debuginfo | 128.2.0-1.el8_10 | |
thunderbird-debugsource | 128.2.0-1.el8_10 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 128.2.0-1.el8_10 | [RHSA-2024:6682](https://access.redhat.com/errata/RHSA-2024:6682) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7652](https://access.redhat.com/security/cve/CVE-2024-7652), [CVE-2024-8381](https://access.redhat.com/security/cve/CVE-2024-8381), [CVE-2024-8382](https://access.redhat.com/security/cve/CVE-2024-8382), [CVE-2024-8383](https://access.redhat.com/security/cve/CVE-2024-8383), [CVE-2024-8384](https://access.redhat.com/security/cve/CVE-2024-8384), [CVE-2024-8385](https://access.redhat.com/security/cve/CVE-2024-8385), [CVE-2024-8386](https://access.redhat.com/security/cve/CVE-2024-8386), [CVE-2024-8387](https://access.redhat.com/security/cve/CVE-2024-8387))
firefox-debuginfo | 128.2.0-1.el8_10 | |
firefox-debugsource | 128.2.0-1.el8_10 | |
nss | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-debuginfo | 3.101.0-7.el8_8 | |
nss-debugsource | 3.101.0-7.el8_8 | |
nss-devel | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-debuginfo | 3.101.0-7.el8_8 | |
nss-softokn-devel | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-freebl | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-softokn-freebl-debuginfo | 3.101.0-7.el8_8 | |
nss-softokn-freebl-devel | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-sysinit | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-sysinit-debuginfo | 3.101.0-7.el8_8 | |
nss-tools | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-tools-debuginfo | 3.101.0-7.el8_8 | |
nss-util | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
nss-util-debuginfo | 3.101.0-7.el8_8 | |
nss-util-devel | 3.101.0-7.el8_8 | [RHBA-2024:6680](https://access.redhat.com/errata/RHBA-2024:6680) | <div class="adv_b">Bug Fix Advisory</div>
thunderbird | 128.2.0-1.el8_10 | [RHSA-2024:6684](https://access.redhat.com/errata/RHSA-2024:6684) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7652](https://access.redhat.com/security/cve/CVE-2024-7652), [CVE-2024-8381](https://access.redhat.com/security/cve/CVE-2024-8381), [CVE-2024-8382](https://access.redhat.com/security/cve/CVE-2024-8382), [CVE-2024-8384](https://access.redhat.com/security/cve/CVE-2024-8384), [CVE-2024-8385](https://access.redhat.com/security/cve/CVE-2024-8385), [CVE-2024-8386](https://access.redhat.com/security/cve/CVE-2024-8386), [CVE-2024-8387](https://access.redhat.com/security/cve/CVE-2024-8387), [CVE-2024-8394](https://access.redhat.com/security/cve/CVE-2024-8394))
thunderbird-debuginfo | 128.2.0-1.el8_10 | |
thunderbird-debugsource | 128.2.0-1.el8_10 | |

