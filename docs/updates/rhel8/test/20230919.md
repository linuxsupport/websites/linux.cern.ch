## 2023-09-19

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.15.1-1.el8_8 | [RHSA-2023:5184](https://access.redhat.com/errata/RHSA-2023:5184) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4863](https://access.redhat.com/security/cve/CVE-2023-4863))
firefox-debuginfo | 102.15.1-1.el8_8 | |
firefox-debugsource | 102.15.1-1.el8_8 | |
thunderbird | 102.15.1-1.el8_8 | [RHSA-2023:5201](https://access.redhat.com/errata/RHSA-2023:5201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4863](https://access.redhat.com/security/cve/CVE-2023-4863))
thunderbird-debuginfo | 102.15.1-1.el8_8 | |
thunderbird-debugsource | 102.15.1-1.el8_8 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.15.1-1.el8_8 | [RHSA-2023:5184](https://access.redhat.com/errata/RHSA-2023:5184) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4863](https://access.redhat.com/security/cve/CVE-2023-4863))
firefox-debuginfo | 102.15.1-1.el8_8 | |
firefox-debugsource | 102.15.1-1.el8_8 | |
thunderbird | 102.15.1-1.el8_8 | [RHSA-2023:5201](https://access.redhat.com/errata/RHSA-2023:5201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4863](https://access.redhat.com/security/cve/CVE-2023-4863))
thunderbird-debuginfo | 102.15.1-1.el8_8 | |
thunderbird-debugsource | 102.15.1-1.el8_8 | |

