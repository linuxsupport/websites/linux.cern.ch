## 2023-08-02

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-askpass-debuginfo | 8.0p1-19.el8_8 | |
openssh-cavs | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-cavs-debuginfo | 8.0p1-19.el8_8 | |
openssh-clients | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-clients-debuginfo | 8.0p1-19.el8_8 | |
openssh-debuginfo | 8.0p1-19.el8_8 | |
openssh-debugsource | 8.0p1-19.el8_8 | |
openssh-keycat | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-keycat-debuginfo | 8.0p1-19.el8_8 | |
openssh-ldap | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-ldap-debuginfo | 8.0p1-19.el8_8 | |
openssh-server | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-server-debuginfo | 8.0p1-19.el8_8 | |
pam_ssh_agent_auth | 0.10.3-7.19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
pam_ssh_agent_auth-debuginfo | 0.10.3-7.19.el8_8 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cjose | 0.6.1-3.module+el8.8.0+19464+578f4546 | [RHSA-2023:4418](https://access.redhat.com/errata/RHSA-2023:4418) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37464](https://access.redhat.com/security/cve/CVE-2023-37464))
cjose-debuginfo | 0.6.1-3.module+el8.8.0+19464+578f4546 | |
cjose-debugsource | 0.6.1-3.module+el8.8.0+19464+578f4546 | |
cjose-devel | 0.6.1-3.module+el8.8.0+19464+578f4546 | [RHSA-2023:4418](https://access.redhat.com/errata/RHSA-2023:4418) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37464](https://access.redhat.com/security/cve/CVE-2023-37464))
openssh-askpass | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-askpass-debuginfo | 8.0p1-19.el8_8 | |
openssh-cavs-debuginfo | 8.0p1-19.el8_8 | |
openssh-clients-debuginfo | 8.0p1-19.el8_8 | |
openssh-debuginfo | 8.0p1-19.el8_8 | |
openssh-debugsource | 8.0p1-19.el8_8 | |
openssh-keycat-debuginfo | 8.0p1-19.el8_8 | |
openssh-ldap-debuginfo | 8.0p1-19.el8_8 | |
openssh-server-debuginfo | 8.0p1-19.el8_8 | |
pam_ssh_agent_auth-debuginfo | 0.10.3-7.19.el8_8 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-askpass-debuginfo | 8.0p1-19.el8_8 | |
openssh-cavs | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-cavs-debuginfo | 8.0p1-19.el8_8 | |
openssh-clients | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-clients-debuginfo | 8.0p1-19.el8_8 | |
openssh-debuginfo | 8.0p1-19.el8_8 | |
openssh-debugsource | 8.0p1-19.el8_8 | |
openssh-keycat | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-keycat-debuginfo | 8.0p1-19.el8_8 | |
openssh-ldap | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-ldap-debuginfo | 8.0p1-19.el8_8 | |
openssh-server | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-server-debuginfo | 8.0p1-19.el8_8 | |
pam_ssh_agent_auth | 0.10.3-7.19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
pam_ssh_agent_auth-debuginfo | 0.10.3-7.19.el8_8 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cjose | 0.6.1-3.module+el8.8.0+19464+578f4546 | [RHSA-2023:4418](https://access.redhat.com/errata/RHSA-2023:4418) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37464](https://access.redhat.com/security/cve/CVE-2023-37464))
cjose-debuginfo | 0.6.1-3.module+el8.8.0+19464+578f4546 | |
cjose-debugsource | 0.6.1-3.module+el8.8.0+19464+578f4546 | |
cjose-devel | 0.6.1-3.module+el8.8.0+19464+578f4546 | [RHSA-2023:4418](https://access.redhat.com/errata/RHSA-2023:4418) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37464](https://access.redhat.com/security/cve/CVE-2023-37464))
openssh-askpass | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-askpass-debuginfo | 8.0p1-19.el8_8 | |
openssh-cavs-debuginfo | 8.0p1-19.el8_8 | |
openssh-clients-debuginfo | 8.0p1-19.el8_8 | |
openssh-debuginfo | 8.0p1-19.el8_8 | |
openssh-debugsource | 8.0p1-19.el8_8 | |
openssh-keycat-debuginfo | 8.0p1-19.el8_8 | |
openssh-ldap-debuginfo | 8.0p1-19.el8_8 | |
openssh-server-debuginfo | 8.0p1-19.el8_8 | |
pam_ssh_agent_auth-debuginfo | 0.10.3-7.19.el8_8 | |

