## 2025-01-15

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
raptor2 | 2.0.15-17.el8_10 | [RHSA-2025:0314](https://access.redhat.com/errata/RHSA-2025:0314) | <div class="adv_s">Security Advisory</div> ([CVE-2024-57823](https://access.redhat.com/security/cve/CVE-2024-57823))
raptor2-debuginfo | 2.0.15-17.el8_10 | |
raptor2-debugsource | 2.0.15-17.el8_10 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
raptor2-debuginfo | 2.0.15-17.el8_10 | |
raptor2-debugsource | 2.0.15-17.el8_10 | |
raptor2-devel | 2.0.15-17.el8_10 | [RHSA-2025:0314](https://access.redhat.com/errata/RHSA-2025:0314) | <div class="adv_s">Security Advisory</div> ([CVE-2024-57823](https://access.redhat.com/security/cve/CVE-2024-57823))

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
raptor2 | 2.0.15-17.el8_10 | [RHSA-2025:0314](https://access.redhat.com/errata/RHSA-2025:0314) | <div class="adv_s">Security Advisory</div> ([CVE-2024-57823](https://access.redhat.com/security/cve/CVE-2024-57823))
raptor2-debuginfo | 2.0.15-17.el8_10 | |
raptor2-debugsource | 2.0.15-17.el8_10 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
raptor2-debuginfo | 2.0.15-17.el8_10 | |
raptor2-debugsource | 2.0.15-17.el8_10 | |
raptor2-devel | 2.0.15-17.el8_10 | [RHSA-2025:0314](https://access.redhat.com/errata/RHSA-2025:0314) | <div class="adv_s">Security Advisory</div> ([CVE-2024-57823](https://access.redhat.com/security/cve/CVE-2024-57823))

