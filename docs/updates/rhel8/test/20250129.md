## 2025-01-29

### highavailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-54.el8_10.8 | |
resource-agents-aliyun | 4.9.0-54.el8_10.8 | |
resource-agents-aliyun-debuginfo | 4.9.0-54.el8_10.8 | |
resource-agents-debuginfo | 4.9.0-54.el8_10.8 | |
resource-agents-debugsource | 4.9.0-54.el8_10.8 | |
resource-agents-gcp | 4.9.0-54.el8_10.8 | |
resource-agents-paf | 4.9.0-54.el8_10.8 | |

