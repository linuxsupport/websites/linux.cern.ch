## 2024-10-07

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 128.3.0-1.el8_10 | |
firefox-debuginfo | 128.3.0-1.el8_10 | |
firefox-debugsource | 128.3.0-1.el8_10 | |
thunderbird | 128.3.0-1.el8_10 | |
thunderbird-debuginfo | 128.3.0-1.el8_10 | |
thunderbird-debugsource | 128.3.0-1.el8_10 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 128.3.0-1.el8_10 | |
firefox-debuginfo | 128.3.0-1.el8_10 | |
firefox-debugsource | 128.3.0-1.el8_10 | |
thunderbird | 128.3.0-1.el8_10 | |
thunderbird-debuginfo | 128.3.0-1.el8_10 | |
thunderbird-debugsource | 128.3.0-1.el8_10 | |

