## 2023-08-04

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.14.0-1.el8_8 | [RHSA-2023:4468](https://access.redhat.com/errata/RHSA-2023:4468) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4045](https://access.redhat.com/security/cve/CVE-2023-4045), [CVE-2023-4046](https://access.redhat.com/security/cve/CVE-2023-4046), [CVE-2023-4047](https://access.redhat.com/security/cve/CVE-2023-4047), [CVE-2023-4048](https://access.redhat.com/security/cve/CVE-2023-4048), [CVE-2023-4049](https://access.redhat.com/security/cve/CVE-2023-4049), [CVE-2023-4050](https://access.redhat.com/security/cve/CVE-2023-4050), [CVE-2023-4055](https://access.redhat.com/security/cve/CVE-2023-4055), [CVE-2023-4056](https://access.redhat.com/security/cve/CVE-2023-4056), [CVE-2023-4057](https://access.redhat.com/security/cve/CVE-2023-4057))
firefox-debuginfo | 102.14.0-1.el8_8 | |
firefox-debugsource | 102.14.0-1.el8_8 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.14.0-1.el8_8 | [RHSA-2023:4468](https://access.redhat.com/errata/RHSA-2023:4468) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4045](https://access.redhat.com/security/cve/CVE-2023-4045), [CVE-2023-4046](https://access.redhat.com/security/cve/CVE-2023-4046), [CVE-2023-4047](https://access.redhat.com/security/cve/CVE-2023-4047), [CVE-2023-4048](https://access.redhat.com/security/cve/CVE-2023-4048), [CVE-2023-4049](https://access.redhat.com/security/cve/CVE-2023-4049), [CVE-2023-4050](https://access.redhat.com/security/cve/CVE-2023-4050), [CVE-2023-4055](https://access.redhat.com/security/cve/CVE-2023-4055), [CVE-2023-4056](https://access.redhat.com/security/cve/CVE-2023-4056), [CVE-2023-4057](https://access.redhat.com/security/cve/CVE-2023-4057))
firefox-debuginfo | 102.14.0-1.el8_8 | |
firefox-debugsource | 102.14.0-1.el8_8 | |

