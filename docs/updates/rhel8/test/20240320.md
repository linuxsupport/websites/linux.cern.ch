## 2024-03-20

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ruby | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-bundled-gems | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-bundled-gems-debuginfo | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-debuginfo | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-debugsource | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-default-gems | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-devel | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-doc | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-libs | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-libs-debuginfo | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-abrt | 0.4.0-1.module+el8.9.0+21471+7d1e4a35 | |
rubygem-abrt-doc | 0.4.0-1.module+el8.9.0+21471+7d1e4a35 | |
rubygem-bigdecimal | 3.1.1-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-bigdecimal-debuginfo | 3.1.1-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-bundler | 2.3.26-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-io-console | 0.5.11-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-io-console-debuginfo | 0.5.11-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-irb | 1.4.1-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-json | 2.6.1-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-json-debuginfo | 2.6.1-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-minitest | 5.15.0-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-mysql2 | 0.5.3-3.module+el8.9.0+21471+7d1e4a35 | |
rubygem-mysql2-debuginfo | 0.5.3-3.module+el8.9.0+21471+7d1e4a35 | |
rubygem-mysql2-debugsource | 0.5.3-3.module+el8.9.0+21471+7d1e4a35 | |
rubygem-mysql2-doc | 0.5.3-3.module+el8.9.0+21471+7d1e4a35 | |
rubygem-pg | 1.3.2-1.module+el8.9.0+21471+7d1e4a35 | |
rubygem-pg-debuginfo | 1.3.2-1.module+el8.9.0+21471+7d1e4a35 | |
rubygem-pg-debugsource | 1.3.2-1.module+el8.9.0+21471+7d1e4a35 | |
rubygem-pg-doc | 1.3.2-1.module+el8.9.0+21471+7d1e4a35 | |
rubygem-power_assert | 2.0.1-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-psych | 4.0.4-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-psych-debuginfo | 4.0.4-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-rake | 13.0.6-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-rbs | 2.7.0-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-rbs-debuginfo | 2.7.0-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-rdoc | 6.4.0-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-rexml | 3.2.5-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-rss | 0.2.9-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-test-unit | 3.5.3-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-typeprof | 0.21.3-142.module+el8.9.0+21471+7d1e4a35 | |
rubygems | 3.3.26-142.module+el8.9.0+21471+7d1e4a35 | |
rubygems-devel | 3.3.26-142.module+el8.9.0+21471+7d1e4a35 | |
squid | 4.15-7.module+el8.9.0+21530+59b09a5b.10 | [RHSA-2024:1375](https://access.redhat.com/errata/RHSA-2024:1375) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50269](https://access.redhat.com/security/cve/CVE-2023-50269), [CVE-2024-25111](https://access.redhat.com/security/cve/CVE-2024-25111), [CVE-2024-25617](https://access.redhat.com/security/cve/CVE-2024-25617))
squid-debuginfo | 4.15-7.module+el8.9.0+21530+59b09a5b.10 | |
squid-debugsource | 4.15-7.module+el8.9.0+21530+59b09a5b.10 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ruby | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-bundled-gems | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-bundled-gems-debuginfo | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-debuginfo | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-debugsource | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-default-gems | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-devel | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-doc | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-libs | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
ruby-libs-debuginfo | 3.1.4-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-abrt | 0.4.0-1.module+el8.9.0+21471+7d1e4a35 | |
rubygem-abrt-doc | 0.4.0-1.module+el8.9.0+21471+7d1e4a35 | |
rubygem-bigdecimal | 3.1.1-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-bigdecimal-debuginfo | 3.1.1-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-bundler | 2.3.26-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-io-console | 0.5.11-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-io-console-debuginfo | 0.5.11-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-irb | 1.4.1-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-json | 2.6.1-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-json-debuginfo | 2.6.1-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-minitest | 5.15.0-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-mysql2 | 0.5.3-3.module+el8.9.0+21471+7d1e4a35 | |
rubygem-mysql2-debuginfo | 0.5.3-3.module+el8.9.0+21471+7d1e4a35 | |
rubygem-mysql2-debugsource | 0.5.3-3.module+el8.9.0+21471+7d1e4a35 | |
rubygem-mysql2-doc | 0.5.3-3.module+el8.9.0+21471+7d1e4a35 | |
rubygem-pg | 1.3.2-1.module+el8.9.0+21471+7d1e4a35 | |
rubygem-pg-debuginfo | 1.3.2-1.module+el8.9.0+21471+7d1e4a35 | |
rubygem-pg-debugsource | 1.3.2-1.module+el8.9.0+21471+7d1e4a35 | |
rubygem-pg-doc | 1.3.2-1.module+el8.9.0+21471+7d1e4a35 | |
rubygem-power_assert | 2.0.1-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-psych | 4.0.4-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-psych-debuginfo | 4.0.4-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-rake | 13.0.6-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-rbs | 2.7.0-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-rbs-debuginfo | 2.7.0-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-rdoc | 6.4.0-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-rexml | 3.2.5-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-rss | 0.2.9-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-test-unit | 3.5.3-142.module+el8.9.0+21471+7d1e4a35 | |
rubygem-typeprof | 0.21.3-142.module+el8.9.0+21471+7d1e4a35 | |
rubygems | 3.3.26-142.module+el8.9.0+21471+7d1e4a35 | |
rubygems-devel | 3.3.26-142.module+el8.9.0+21471+7d1e4a35 | |
squid | 4.15-7.module+el8.9.0+21530+59b09a5b.10 | [RHSA-2024:1375](https://access.redhat.com/errata/RHSA-2024:1375) | <div class="adv_s">Security Advisory</div> ([CVE-2023-50269](https://access.redhat.com/security/cve/CVE-2023-50269), [CVE-2024-25111](https://access.redhat.com/security/cve/CVE-2024-25111), [CVE-2024-25617](https://access.redhat.com/security/cve/CVE-2024-25617))
squid-debuginfo | 4.15-7.module+el8.9.0+21530+59b09a5b.10 | |
squid-debugsource | 4.15-7.module+el8.9.0+21530+59b09a5b.10 | |

