## 2024-08-13

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aardvark-dns | 1.10.0-1.module+el8.10.0+22202+761b9a65 | |
buildah | 1.33.8-4.module+el8.10.0+22202+761b9a65 | |
buildah-debuginfo | 1.33.8-4.module+el8.10.0+22202+761b9a65 | |
buildah-debugsource | 1.33.8-4.module+el8.10.0+22202+761b9a65 | |
buildah-tests | 1.33.8-4.module+el8.10.0+22202+761b9a65 | |
buildah-tests-debuginfo | 1.33.8-4.module+el8.10.0+22202+761b9a65 | |
cockpit-podman | 84.1-1.module+el8.10.0+22202+761b9a65 | |
conmon | 2.1.10-1.module+el8.10.0+22202+761b9a65 | |
conmon-debuginfo | 2.1.10-1.module+el8.10.0+22202+761b9a65 | |
conmon-debugsource | 2.1.10-1.module+el8.10.0+22202+761b9a65 | |
container-selinux | 2.229.0-2.module+el8.10.0+22202+761b9a65 | |
containernetworking-plugins | 1.4.0-5.module+el8.10.0+22202+761b9a65 | |
containernetworking-plugins-debuginfo | 1.4.0-5.module+el8.10.0+22202+761b9a65 | |
containernetworking-plugins-debugsource | 1.4.0-5.module+el8.10.0+22202+761b9a65 | |
containers-common | 1-82.module+el8.10.0+22202+761b9a65 | |
crit | 3.18-5.module+el8.10.0+22202+761b9a65 | |
criu | 3.18-5.module+el8.10.0+22202+761b9a65 | |
criu-debuginfo | 3.18-5.module+el8.10.0+22202+761b9a65 | |
criu-debugsource | 3.18-5.module+el8.10.0+22202+761b9a65 | |
criu-devel | 3.18-5.module+el8.10.0+22202+761b9a65 | |
criu-libs | 3.18-5.module+el8.10.0+22202+761b9a65 | |
criu-libs-debuginfo | 3.18-5.module+el8.10.0+22202+761b9a65 | |
crun | 1.14.3-2.module+el8.10.0+22202+761b9a65 | |
crun-debuginfo | 1.14.3-2.module+el8.10.0+22202+761b9a65 | |
crun-debugsource | 1.14.3-2.module+el8.10.0+22202+761b9a65 | |
fuse-overlayfs | 1.13-1.module+el8.10.0+22202+761b9a65 | |
fuse-overlayfs-debuginfo | 1.13-1.module+el8.10.0+22202+761b9a65 | |
fuse-overlayfs-debugsource | 1.13-1.module+el8.10.0+22202+761b9a65 | |
libslirp | 4.4.0-2.module+el8.10.0+22202+761b9a65 | |
libslirp-debuginfo | 4.4.0-2.module+el8.10.0+22202+761b9a65 | |
libslirp-debugsource | 4.4.0-2.module+el8.10.0+22202+761b9a65 | |
libslirp-devel | 4.4.0-2.module+el8.10.0+22202+761b9a65 | |
netavark | 1.10.3-1.module+el8.10.0+22202+761b9a65 | |
oci-seccomp-bpf-hook | 1.2.10-1.module+el8.10.0+22202+761b9a65 | |
oci-seccomp-bpf-hook-debuginfo | 1.2.10-1.module+el8.10.0+22202+761b9a65 | |
oci-seccomp-bpf-hook-debugsource | 1.2.10-1.module+el8.10.0+22202+761b9a65 | |
pacemaker-cli-debuginfo | 2.1.7-5.1.el8_10 | |
pacemaker-cluster-libs | 2.1.7-5.1.el8_10 | |
pacemaker-cluster-libs-debuginfo | 2.1.7-5.1.el8_10 | |
pacemaker-debuginfo | 2.1.7-5.1.el8_10 | |
pacemaker-debugsource | 2.1.7-5.1.el8_10 | |
pacemaker-libs | 2.1.7-5.1.el8_10 | |
pacemaker-libs-debuginfo | 2.1.7-5.1.el8_10 | |
pacemaker-remote-debuginfo | 2.1.7-5.1.el8_10 | |
pacemaker-schemas | 2.1.7-5.1.el8_10 | |
podman | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-catatonit | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-catatonit-debuginfo | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-debuginfo | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-debugsource | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-docker | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-gvproxy | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-gvproxy-debuginfo | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-plugins | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-plugins-debuginfo | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-remote | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-remote-debuginfo | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-tests | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
python3-criu | 3.18-5.module+el8.10.0+22202+761b9a65 | |
python3-podman | 4.9.0-2.module+el8.10.0+22202+761b9a65 | |
runc | 1.1.12-4.module+el8.10.0+22202+761b9a65 | |
runc-debuginfo | 1.1.12-4.module+el8.10.0+22202+761b9a65 | |
runc-debugsource | 1.1.12-4.module+el8.10.0+22202+761b9a65 | |
skopeo | 1.14.5-3.module+el8.10.0+22202+761b9a65 | |
skopeo-tests | 1.14.5-3.module+el8.10.0+22202+761b9a65 | |
slirp4netns | 1.2.3-1.module+el8.10.0+22202+761b9a65 | |
slirp4netns-debuginfo | 1.2.3-1.module+el8.10.0+22202+761b9a65 | |
slirp4netns-debugsource | 1.2.3-1.module+el8.10.0+22202+761b9a65 | |
toolbox | 0.0.99.5-2.module+el8.10.0+22202+761b9a65 | |
toolbox-debuginfo | 0.0.99.5-2.module+el8.10.0+22202+761b9a65 | |
toolbox-debugsource | 0.0.99.5-2.module+el8.10.0+22202+761b9a65 | |
toolbox-tests | 0.0.99.5-2.module+el8.10.0+22202+761b9a65 | |
udica | 0.2.6-21.module+el8.10.0+22202+761b9a65 | |

### highavailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pacemaker | 2.1.7-5.1.el8_10 | |
pacemaker-cli | 2.1.7-5.1.el8_10 | |
pacemaker-cli-debuginfo | 2.1.7-5.1.el8_10 | |
pacemaker-cluster-libs-debuginfo | 2.1.7-5.1.el8_10 | |
pacemaker-cts | 2.1.7-5.1.el8_10 | |
pacemaker-debuginfo | 2.1.7-5.1.el8_10 | |
pacemaker-debugsource | 2.1.7-5.1.el8_10 | |
pacemaker-doc | 2.1.7-5.1.el8_10 | |
pacemaker-libs-debuginfo | 2.1.7-5.1.el8_10 | |
pacemaker-libs-devel | 2.1.7-5.1.el8_10 | |
pacemaker-nagios-plugins-metadata | 2.1.7-5.1.el8_10 | |
pacemaker-remote | 2.1.7-5.1.el8_10 | |
pacemaker-remote-debuginfo | 2.1.7-5.1.el8_10 | |
python3-pacemaker | 2.1.7-5.1.el8_10 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aardvark-dns | 1.10.0-1.module+el8.10.0+22202+761b9a65 | |
buildah | 1.33.8-4.module+el8.10.0+22202+761b9a65 | |
buildah-debuginfo | 1.33.8-4.module+el8.10.0+22202+761b9a65 | |
buildah-debugsource | 1.33.8-4.module+el8.10.0+22202+761b9a65 | |
buildah-tests | 1.33.8-4.module+el8.10.0+22202+761b9a65 | |
buildah-tests-debuginfo | 1.33.8-4.module+el8.10.0+22202+761b9a65 | |
cockpit-podman | 84.1-1.module+el8.10.0+22202+761b9a65 | |
conmon | 2.1.10-1.module+el8.10.0+22202+761b9a65 | |
conmon-debuginfo | 2.1.10-1.module+el8.10.0+22202+761b9a65 | |
conmon-debugsource | 2.1.10-1.module+el8.10.0+22202+761b9a65 | |
container-selinux | 2.229.0-2.module+el8.10.0+22202+761b9a65 | |
containernetworking-plugins | 1.4.0-5.module+el8.10.0+22202+761b9a65 | |
containernetworking-plugins-debuginfo | 1.4.0-5.module+el8.10.0+22202+761b9a65 | |
containernetworking-plugins-debugsource | 1.4.0-5.module+el8.10.0+22202+761b9a65 | |
containers-common | 1-82.module+el8.10.0+22202+761b9a65 | |
crit | 3.18-5.module+el8.10.0+22202+761b9a65 | |
criu | 3.18-5.module+el8.10.0+22202+761b9a65 | |
criu-debuginfo | 3.18-5.module+el8.10.0+22202+761b9a65 | |
criu-debugsource | 3.18-5.module+el8.10.0+22202+761b9a65 | |
criu-devel | 3.18-5.module+el8.10.0+22202+761b9a65 | |
criu-libs | 3.18-5.module+el8.10.0+22202+761b9a65 | |
criu-libs-debuginfo | 3.18-5.module+el8.10.0+22202+761b9a65 | |
crun | 1.14.3-2.module+el8.10.0+22202+761b9a65 | |
crun-debuginfo | 1.14.3-2.module+el8.10.0+22202+761b9a65 | |
crun-debugsource | 1.14.3-2.module+el8.10.0+22202+761b9a65 | |
fuse-overlayfs | 1.13-1.module+el8.10.0+22202+761b9a65 | |
fuse-overlayfs-debuginfo | 1.13-1.module+el8.10.0+22202+761b9a65 | |
fuse-overlayfs-debugsource | 1.13-1.module+el8.10.0+22202+761b9a65 | |
libslirp | 4.4.0-2.module+el8.10.0+22202+761b9a65 | |
libslirp-debuginfo | 4.4.0-2.module+el8.10.0+22202+761b9a65 | |
libslirp-debugsource | 4.4.0-2.module+el8.10.0+22202+761b9a65 | |
libslirp-devel | 4.4.0-2.module+el8.10.0+22202+761b9a65 | |
netavark | 1.10.3-1.module+el8.10.0+22202+761b9a65 | |
oci-seccomp-bpf-hook | 1.2.10-1.module+el8.10.0+22202+761b9a65 | |
oci-seccomp-bpf-hook-debuginfo | 1.2.10-1.module+el8.10.0+22202+761b9a65 | |
oci-seccomp-bpf-hook-debugsource | 1.2.10-1.module+el8.10.0+22202+761b9a65 | |
pacemaker-cli-debuginfo | 2.1.7-5.1.el8_10 | |
pacemaker-cluster-libs | 2.1.7-5.1.el8_10 | |
pacemaker-cluster-libs-debuginfo | 2.1.7-5.1.el8_10 | |
pacemaker-debuginfo | 2.1.7-5.1.el8_10 | |
pacemaker-debugsource | 2.1.7-5.1.el8_10 | |
pacemaker-libs | 2.1.7-5.1.el8_10 | |
pacemaker-libs-debuginfo | 2.1.7-5.1.el8_10 | |
pacemaker-remote-debuginfo | 2.1.7-5.1.el8_10 | |
pacemaker-schemas | 2.1.7-5.1.el8_10 | |
podman | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-catatonit | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-catatonit-debuginfo | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-debuginfo | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-debugsource | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-docker | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-gvproxy | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-gvproxy-debuginfo | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-plugins | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-plugins-debuginfo | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-remote | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-remote-debuginfo | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
podman-tests | 4.9.4-12.module+el8.10.0+22202+761b9a65 | |
python3-criu | 3.18-5.module+el8.10.0+22202+761b9a65 | |
python3-podman | 4.9.0-2.module+el8.10.0+22202+761b9a65 | |
runc | 1.1.12-4.module+el8.10.0+22202+761b9a65 | |
runc-debuginfo | 1.1.12-4.module+el8.10.0+22202+761b9a65 | |
runc-debugsource | 1.1.12-4.module+el8.10.0+22202+761b9a65 | |
skopeo | 1.14.5-3.module+el8.10.0+22202+761b9a65 | |
skopeo-tests | 1.14.5-3.module+el8.10.0+22202+761b9a65 | |
slirp4netns | 1.2.3-1.module+el8.10.0+22202+761b9a65 | |
slirp4netns-debuginfo | 1.2.3-1.module+el8.10.0+22202+761b9a65 | |
slirp4netns-debugsource | 1.2.3-1.module+el8.10.0+22202+761b9a65 | |
toolbox | 0.0.99.5-2.module+el8.10.0+22202+761b9a65 | |
toolbox-debuginfo | 0.0.99.5-2.module+el8.10.0+22202+761b9a65 | |
toolbox-debugsource | 0.0.99.5-2.module+el8.10.0+22202+761b9a65 | |
toolbox-tests | 0.0.99.5-2.module+el8.10.0+22202+761b9a65 | |
udica | 0.2.6-21.module+el8.10.0+22202+761b9a65 | |

