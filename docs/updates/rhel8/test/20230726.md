## 2023-07-26

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.10-0.rh8.cern | |
kmod-openafs | 1.8.10-0.4.18.0_477.15.1.el8_8.rh8.cern | |
openafs | 1.8.10-0.rh8.cern | |
openafs-authlibs | 1.8.10-0.rh8.cern | |
openafs-authlibs-devel | 1.8.10-0.rh8.cern | |
openafs-client | 1.8.10-0.rh8.cern | |
openafs-compat | 1.8.10-0.rh8.cern | |
openafs-devel | 1.8.10-0.rh8.cern | |
openafs-docs | 1.8.10-0.rh8.cern | |
openafs-kernel-source | 1.8.10-0.rh8.cern | |
openafs-krb5 | 1.8.10-0.rh8.cern | |
openafs-server | 1.8.10-0.rh8.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.10-0.rh8.cern | |
openafs | 1.8.10-0.rh8.cern | |
openafs-authlibs | 1.8.10-0.rh8.cern | |
openafs-authlibs-devel | 1.8.10-0.rh8.cern | |
openafs-client | 1.8.10-0.rh8.cern | |
openafs-compat | 1.8.10-0.rh8.cern | |
openafs-devel | 1.8.10-0.rh8.cern | |
openafs-docs | 1.8.10-0.rh8.cern | |
openafs-kernel-source | 1.8.10-0.rh8.cern | |
openafs-krb5 | 1.8.10-0.rh8.cern | |
openafs-server | 1.8.10-0.rh8.cern | |

