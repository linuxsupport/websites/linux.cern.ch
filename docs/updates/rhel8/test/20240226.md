## 2024-02-26

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
opensc | 0.20.0-8.el8_9 | |
opensc-debuginfo | 0.20.0-8.el8_9 | |
opensc-debugsource | 0.20.0-8.el8_9 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.8.0-1.el8_9 | |
firefox-debuginfo | 115.8.0-1.el8_9 | |
firefox-debugsource | 115.8.0-1.el8_9 | |
pgaudit | 1.4.0-7.module+el8.9.0+21289+7c796f9d | |
pgaudit-debuginfo | 1.4.0-7.module+el8.9.0+21289+7c796f9d | |
pgaudit-debugsource | 1.4.0-7.module+el8.9.0+21289+7c796f9d | |
postgresql | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-contrib | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-contrib | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-contrib | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-contrib | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-contrib-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-contrib-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-contrib-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-contrib-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-debugsource | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-debugsource | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-debugsource | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-debugsource | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-docs | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-docs | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-docs | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-docs | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-docs-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-docs-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-docs-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-docs-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-plperl | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-plperl | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-plperl | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-plperl | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-plperl-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-plperl-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-plperl-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-plperl-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-plpython3 | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-plpython3 | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-plpython3 | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-plpython3 | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-plpython3-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-plpython3-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-plpython3-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-plpython3-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-pltcl | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-pltcl | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-pltcl | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-pltcl | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-pltcl-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-pltcl-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-pltcl-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-pltcl-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-private-devel | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-private-libs | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-private-libs-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-server | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-server | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-server | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-server | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-server-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-server-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-server-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-server-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-server-devel | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-server-devel | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-server-devel | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-server-devel | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-server-devel-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-server-devel-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-server-devel-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-server-devel-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-static | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-static | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-static | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-static | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-test | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-test | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-test | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-test | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-test-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-test-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-test-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-test-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-test-rpm-macros | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-test-rpm-macros | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-test-rpm-macros | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-test-rpm-macros | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-upgrade | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-upgrade | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-upgrade | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-upgrade | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-upgrade-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-upgrade-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-upgrade-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-upgrade-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-upgrade-devel | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-upgrade-devel | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-upgrade-devel | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-upgrade-devel | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-upgrade-devel-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-upgrade-devel-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-upgrade-devel-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-upgrade-devel-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
python3-unbound | 1.16.2-5.el8_9.2 | |
python3-unbound-debuginfo | 1.16.2-5.el8_9.2 | |
thunderbird | 115.8.0-1.el8_9 | |
thunderbird-debuginfo | 115.8.0-1.el8_9 | |
thunderbird-debugsource | 115.8.0-1.el8_9 | |
unbound | 1.16.2-5.el8_9.2 | |
unbound-debuginfo | 1.16.2-5.el8_9.2 | |
unbound-debugsource | 1.16.2-5.el8_9.2 | |
unbound-devel | 1.16.2-5.el8_9.2 | |
unbound-libs | 1.16.2-5.el8_9.2 | |
unbound-libs-debuginfo | 1.16.2-5.el8_9.2 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
opensc | 0.20.0-8.el8_9 | |
opensc-debuginfo | 0.20.0-8.el8_9 | |
opensc-debugsource | 0.20.0-8.el8_9 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.8.0-1.el8_9 | |
firefox-debuginfo | 115.8.0-1.el8_9 | |
firefox-debugsource | 115.8.0-1.el8_9 | |
pgaudit | 1.4.0-7.module+el8.9.0+21289+7c796f9d | |
pgaudit-debuginfo | 1.4.0-7.module+el8.9.0+21289+7c796f9d | |
pgaudit-debugsource | 1.4.0-7.module+el8.9.0+21289+7c796f9d | |
postgresql | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-contrib | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-contrib | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-contrib | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-contrib | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-contrib-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-contrib-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-contrib-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-contrib-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-debugsource | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-debugsource | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-debugsource | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-debugsource | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-docs | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-docs | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-docs | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-docs | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-docs-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-docs-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-docs-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-docs-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-plperl | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-plperl | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-plperl | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-plperl | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-plperl-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-plperl-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-plperl-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-plperl-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-plpython3 | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-plpython3 | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-plpython3 | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-plpython3 | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-plpython3-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-plpython3-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-plpython3-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-plpython3-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-pltcl | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-pltcl | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-pltcl | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-pltcl | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-pltcl-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-pltcl-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-pltcl-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-pltcl-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-private-devel | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-private-libs | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-private-libs-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-server | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-server | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-server | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-server | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-server-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-server-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-server-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-server-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-server-devel | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-server-devel | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-server-devel | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-server-devel | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-server-devel-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-server-devel-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-server-devel-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-server-devel-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-static | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-static | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-static | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-static | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-test | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-test | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-test | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-test | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-test-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-test-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-test-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-test-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-test-rpm-macros | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-test-rpm-macros | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-test-rpm-macros | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-test-rpm-macros | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-upgrade | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-upgrade | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-upgrade | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-upgrade | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-upgrade-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-upgrade-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-upgrade-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-upgrade-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-upgrade-devel | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-upgrade-devel | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-upgrade-devel | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-upgrade-devel | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
postgresql-upgrade-devel-debuginfo | 10.23-4.module+el8.9.0+21317+d2c6bf8a | |
postgresql-upgrade-devel-debuginfo | 12.18-1.module+el8.9.0+21289+7c796f9d | |
postgresql-upgrade-devel-debuginfo | 13.14-1.module+el8.9.0+21288+3d364c44 | |
postgresql-upgrade-devel-debuginfo | 15.6-1.module+el8.9.0+21287+d0eecfe6 | |
python3-unbound | 1.16.2-5.el8_9.2 | |
python3-unbound-debuginfo | 1.16.2-5.el8_9.2 | |
thunderbird | 115.8.0-1.el8_9 | |
thunderbird-debuginfo | 115.8.0-1.el8_9 | |
thunderbird-debugsource | 115.8.0-1.el8_9 | |
unbound | 1.16.2-5.el8_9.2 | |
unbound-debuginfo | 1.16.2-5.el8_9.2 | |
unbound-debugsource | 1.16.2-5.el8_9.2 | |
unbound-devel | 1.16.2-5.el8_9.2 | |
unbound-libs | 1.16.2-5.el8_9.2 | |
unbound-libs-debuginfo | 1.16.2-5.el8_9.2 | |

