## 2025-02-21

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libpq | 13.20-1.el8_10 | [RHSA-2025:1737](https://access.redhat.com/errata/RHSA-2025:1737) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
libpq-debuginfo | 13.20-1.el8_10 | |
libpq-debugsource | 13.20-1.el8_10 | |
libpq-devel | 13.20-1.el8_10 | [RHSA-2025:1737](https://access.redhat.com/errata/RHSA-2025:1737) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
libpq-devel-debuginfo | 13.20-1.el8_10 | |
postgresql | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql | 15.12-1.module+el8.10.0+22871+d29fc53a | [RHSA-2025:1739](https://access.redhat.com/errata/RHSA-2025:1739) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql | 16.8-1.module+el8.10.0+22867+e4e13b1c | [RHSA-2025:1740](https://access.redhat.com/errata/RHSA-2025:1740) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-contrib | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-contrib | 15.12-1.module+el8.10.0+22871+d29fc53a | [RHSA-2025:1739](https://access.redhat.com/errata/RHSA-2025:1739) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-contrib | 16.8-1.module+el8.10.0+22867+e4e13b1c | [RHSA-2025:1740](https://access.redhat.com/errata/RHSA-2025:1740) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-contrib-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-contrib-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-contrib-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-debugsource | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-debugsource | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-debugsource | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-docs | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-docs | 15.12-1.module+el8.10.0+22871+d29fc53a | [RHSA-2025:1739](https://access.redhat.com/errata/RHSA-2025:1739) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-docs | 16.8-1.module+el8.10.0+22867+e4e13b1c | [RHSA-2025:1740](https://access.redhat.com/errata/RHSA-2025:1740) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-docs-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-docs-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-docs-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-plperl | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-plperl | 15.12-1.module+el8.10.0+22871+d29fc53a | [RHSA-2025:1739](https://access.redhat.com/errata/RHSA-2025:1739) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-plperl | 16.8-1.module+el8.10.0+22867+e4e13b1c | [RHSA-2025:1740](https://access.redhat.com/errata/RHSA-2025:1740) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-plperl-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-plperl-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-plperl-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-plpython3 | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-plpython3 | 15.12-1.module+el8.10.0+22871+d29fc53a | [RHSA-2025:1739](https://access.redhat.com/errata/RHSA-2025:1739) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-plpython3 | 16.8-1.module+el8.10.0+22867+e4e13b1c | [RHSA-2025:1740](https://access.redhat.com/errata/RHSA-2025:1740) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-plpython3-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-plpython3-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-plpython3-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-pltcl | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-pltcl | 15.12-1.module+el8.10.0+22871+d29fc53a | [RHSA-2025:1739](https://access.redhat.com/errata/RHSA-2025:1739) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-pltcl | 16.8-1.module+el8.10.0+22867+e4e13b1c | [RHSA-2025:1740](https://access.redhat.com/errata/RHSA-2025:1740) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-pltcl-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-pltcl-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-pltcl-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-private-devel | 15.12-1.module+el8.10.0+22871+d29fc53a | [RHSA-2025:1739](https://access.redhat.com/errata/RHSA-2025:1739) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-private-devel | 16.8-1.module+el8.10.0+22867+e4e13b1c | [RHSA-2025:1740](https://access.redhat.com/errata/RHSA-2025:1740) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-private-libs | 15.12-1.module+el8.10.0+22871+d29fc53a | [RHSA-2025:1739](https://access.redhat.com/errata/RHSA-2025:1739) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-private-libs | 16.8-1.module+el8.10.0+22867+e4e13b1c | [RHSA-2025:1740](https://access.redhat.com/errata/RHSA-2025:1740) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-private-libs-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-private-libs-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-server | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-server | 15.12-1.module+el8.10.0+22871+d29fc53a | [RHSA-2025:1739](https://access.redhat.com/errata/RHSA-2025:1739) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-server | 16.8-1.module+el8.10.0+22867+e4e13b1c | [RHSA-2025:1740](https://access.redhat.com/errata/RHSA-2025:1740) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-server-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-server-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-server-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-server-devel | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-server-devel | 15.12-1.module+el8.10.0+22871+d29fc53a | [RHSA-2025:1739](https://access.redhat.com/errata/RHSA-2025:1739) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-server-devel | 16.8-1.module+el8.10.0+22867+e4e13b1c | [RHSA-2025:1740](https://access.redhat.com/errata/RHSA-2025:1740) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-server-devel-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-server-devel-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-server-devel-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-static | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-static | 15.12-1.module+el8.10.0+22871+d29fc53a | [RHSA-2025:1739](https://access.redhat.com/errata/RHSA-2025:1739) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-static | 16.8-1.module+el8.10.0+22867+e4e13b1c | [RHSA-2025:1740](https://access.redhat.com/errata/RHSA-2025:1740) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-test | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-test | 15.12-1.module+el8.10.0+22871+d29fc53a | [RHSA-2025:1739](https://access.redhat.com/errata/RHSA-2025:1739) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-test | 16.8-1.module+el8.10.0+22867+e4e13b1c | [RHSA-2025:1740](https://access.redhat.com/errata/RHSA-2025:1740) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-test-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-test-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-test-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-test-rpm-macros | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-test-rpm-macros | 15.12-1.module+el8.10.0+22871+d29fc53a | [RHSA-2025:1739](https://access.redhat.com/errata/RHSA-2025:1739) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-test-rpm-macros | 16.8-1.module+el8.10.0+22867+e4e13b1c | [RHSA-2025:1740](https://access.redhat.com/errata/RHSA-2025:1740) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-upgrade | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-upgrade | 15.12-1.module+el8.10.0+22871+d29fc53a | [RHSA-2025:1739](https://access.redhat.com/errata/RHSA-2025:1739) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-upgrade | 16.8-1.module+el8.10.0+22867+e4e13b1c | [RHSA-2025:1740](https://access.redhat.com/errata/RHSA-2025:1740) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-upgrade-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-upgrade-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-upgrade-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-upgrade-devel | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-upgrade-devel | 15.12-1.module+el8.10.0+22871+d29fc53a | [RHSA-2025:1739](https://access.redhat.com/errata/RHSA-2025:1739) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-upgrade-devel | 16.8-1.module+el8.10.0+22867+e4e13b1c | [RHSA-2025:1740](https://access.redhat.com/errata/RHSA-2025:1740) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-upgrade-devel-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-upgrade-devel-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-upgrade-devel-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libpq | 13.20-1.el8_10 | [RHSA-2025:1737](https://access.redhat.com/errata/RHSA-2025:1737) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
libpq-debuginfo | 13.20-1.el8_10 | |
libpq-debugsource | 13.20-1.el8_10 | |
libpq-devel | 13.20-1.el8_10 | [RHSA-2025:1737](https://access.redhat.com/errata/RHSA-2025:1737) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
libpq-devel-debuginfo | 13.20-1.el8_10 | |
postgresql | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-contrib | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-contrib | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-contrib | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-contrib-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-contrib-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-contrib-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-debugsource | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-debugsource | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-debugsource | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-docs | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-docs | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-docs | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-docs-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-docs-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-docs-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-plperl | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-plperl | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-plperl | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-plperl-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-plperl-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-plperl-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-plpython3 | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-plpython3 | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-plpython3 | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-plpython3-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-plpython3-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-plpython3-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-pltcl | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-pltcl | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-pltcl | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-pltcl-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-pltcl-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-pltcl-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-private-devel | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-private-devel | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-private-libs | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-private-libs | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-private-libs-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-private-libs-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-server | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-server | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-server | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-server-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-server-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-server-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-server-devel | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-server-devel | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-server-devel | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-server-devel-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-server-devel-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-server-devel-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-static | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-static | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-static | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-test | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-test | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-test | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-test-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-test-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-test-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-test-rpm-macros | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-test-rpm-macros | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-test-rpm-macros | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-upgrade | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-upgrade | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-upgrade | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-upgrade-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-upgrade-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-upgrade-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-upgrade-devel | 13.20-1.module+el8.10.0+22878+46d41b73 | [RHSA-2025:1736](https://access.redhat.com/errata/RHSA-2025:1736) | <div class="adv_s">Security Advisory</div> ([CVE-2025-1094](https://access.redhat.com/security/cve/CVE-2025-1094))
postgresql-upgrade-devel | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-upgrade-devel | 16.8-1.module+el8.10.0+22867+e4e13b1c | |
postgresql-upgrade-devel-debuginfo | 13.20-1.module+el8.10.0+22878+46d41b73 | |
postgresql-upgrade-devel-debuginfo | 15.12-1.module+el8.10.0+22871+d29fc53a | |
postgresql-upgrade-devel-debuginfo | 16.8-1.module+el8.10.0+22867+e4e13b1c | |

