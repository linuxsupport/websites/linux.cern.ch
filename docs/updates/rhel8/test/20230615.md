## 2023-06-15

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
c-ares | 1.13.0-6.el8_8.2 | |
c-ares-debuginfo | 1.13.0-6.el8_8.2 | |
c-ares-debugsource | 1.13.0-6.el8_8.2 | |
c-ares-devel | 1.13.0-6.el8_8.2 | |
platform-python | 3.6.8-51.el8_8.1 | |
python3-debuginfo | 3.6.8-51.el8_8.1 | |
python3-debugsource | 3.6.8-51.el8_8.1 | |
python3-libs | 3.6.8-51.el8_8.1 | |
python3-test | 3.6.8-51.el8_8.1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.18-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
aspnetcore-runtime-7.0 | 7.0.7-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
aspnetcore-targeting-pack-6.0 | 6.0.18-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
aspnetcore-targeting-pack-7.0 | 7.0.7-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet | 7.0.107-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-apphost-pack-6.0 | 6.0.18-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-apphost-pack-6.0-debuginfo | 6.0.18-1.el8_8 | |
dotnet-apphost-pack-7.0 | 7.0.7-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-apphost-pack-7.0-debuginfo | 7.0.7-1.el8_8 | |
dotnet-host | 7.0.7-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-host-debuginfo | 7.0.7-1.el8_8 | |
dotnet-hostfxr-6.0 | 6.0.18-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-hostfxr-6.0-debuginfo | 6.0.18-1.el8_8 | |
dotnet-hostfxr-7.0 | 7.0.7-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-hostfxr-7.0-debuginfo | 7.0.7-1.el8_8 | |
dotnet-runtime-6.0 | 6.0.18-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-runtime-6.0-debuginfo | 6.0.18-1.el8_8 | |
dotnet-runtime-7.0 | 7.0.7-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-runtime-7.0-debuginfo | 7.0.7-1.el8_8 | |
dotnet-sdk-6.0 | 6.0.118-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-sdk-6.0-debuginfo | 6.0.118-1.el8_8 | |
dotnet-sdk-7.0 | 7.0.107-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-sdk-7.0-debuginfo | 7.0.107-1.el8_8 | |
dotnet-targeting-pack-6.0 | 6.0.18-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-targeting-pack-7.0 | 7.0.7-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-templates-6.0 | 6.0.118-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-templates-7.0 | 7.0.107-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet6.0-debuginfo | 6.0.118-1.el8_8 | |
dotnet6.0-debugsource | 6.0.118-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.107-1.el8_8 | |
dotnet7.0-debugsource | 7.0.107-1.el8_8 | |
firefox | 102.12.0-1.el8_8 | [RHSA-2023:3590](https://access.redhat.com/errata/RHSA-2023:3590) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34414](https://access.redhat.com/security/cve/CVE-2023-34414), [CVE-2023-34416](https://access.redhat.com/security/cve/CVE-2023-34416))
firefox-debuginfo | 102.12.0-1.el8_8 | |
firefox-debugsource | 102.12.0-1.el8_8 | |
netstandard-targeting-pack-2.1 | 7.0.107-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
platform-python-debug | 3.6.8-51.el8_8.1 | |
platform-python-devel | 3.6.8-51.el8_8.1 | |
python3-debuginfo | 3.6.8-51.el8_8.1 | |
python3-debugsource | 3.6.8-51.el8_8.1 | |
python3-idle | 3.6.8-51.el8_8.1 | |
python3-tkinter | 3.6.8-51.el8_8.1 | |
python3.11 | 3.11.2-2.el8_8.1 | |
python3.11-debuginfo | 3.11.2-2.el8_8.1 | |
python3.11-debugsource | 3.11.2-2.el8_8.1 | |
python3.11-devel | 3.11.2-2.el8_8.1 | |
python3.11-libs | 3.11.2-2.el8_8.1 | |
python3.11-rpm-macros | 3.11.2-2.el8_8.1 | |
python3.11-tkinter | 3.11.2-2.el8_8.1 | |
thunderbird | 102.12.0-1.el8_8 | [RHSA-2023:3588](https://access.redhat.com/errata/RHSA-2023:3588) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34414](https://access.redhat.com/security/cve/CVE-2023-34414), [CVE-2023-34416](https://access.redhat.com/security/cve/CVE-2023-34416))
thunderbird-debuginfo | 102.12.0-1.el8_8 | |
thunderbird-debugsource | 102.12.0-1.el8_8 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.18-1.el8_8 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.7-1.el8_8 | |
dotnet-host-debuginfo | 7.0.7-1.el8_8 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.18-1.el8_8 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.7-1.el8_8 | |
dotnet-runtime-6.0-debuginfo | 6.0.18-1.el8_8 | |
dotnet-runtime-7.0-debuginfo | 7.0.7-1.el8_8 | |
dotnet-sdk-6.0-debuginfo | 6.0.118-1.el8_8 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.118-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-sdk-7.0-debuginfo | 7.0.107-1.el8_8 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.107-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet6.0-debuginfo | 6.0.118-1.el8_8 | |
dotnet6.0-debugsource | 6.0.118-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.107-1.el8_8 | |
dotnet7.0-debugsource | 7.0.107-1.el8_8 | |
python3.11-debug | 3.11.2-2.el8_8.1 | |
python3.11-debuginfo | 3.11.2-2.el8_8.1 | |
python3.11-debugsource | 3.11.2-2.el8_8.1 | |
python3.11-idle | 3.11.2-2.el8_8.1 | |
python3.11-test | 3.11.2-2.el8_8.1 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
c-ares | 1.13.0-6.el8_8.2 | |
c-ares-debuginfo | 1.13.0-6.el8_8.2 | |
c-ares-debugsource | 1.13.0-6.el8_8.2 | |
c-ares-devel | 1.13.0-6.el8_8.2 | |
platform-python | 3.6.8-51.el8_8.1 | |
python3-debuginfo | 3.6.8-51.el8_8.1 | |
python3-debugsource | 3.6.8-51.el8_8.1 | |
python3-libs | 3.6.8-51.el8_8.1 | |
python3-test | 3.6.8-51.el8_8.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.18-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
aspnetcore-runtime-7.0 | 7.0.7-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
aspnetcore-targeting-pack-6.0 | 6.0.18-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
aspnetcore-targeting-pack-7.0 | 7.0.7-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet | 7.0.107-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-apphost-pack-6.0 | 6.0.18-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-apphost-pack-6.0-debuginfo | 6.0.18-1.el8_8 | |
dotnet-apphost-pack-7.0 | 7.0.7-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-apphost-pack-7.0-debuginfo | 7.0.7-1.el8_8 | |
dotnet-host | 7.0.7-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-host-debuginfo | 7.0.7-1.el8_8 | |
dotnet-hostfxr-6.0 | 6.0.18-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-hostfxr-6.0-debuginfo | 6.0.18-1.el8_8 | |
dotnet-hostfxr-7.0 | 7.0.7-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-hostfxr-7.0-debuginfo | 7.0.7-1.el8_8 | |
dotnet-runtime-6.0 | 6.0.18-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-runtime-6.0-debuginfo | 6.0.18-1.el8_8 | |
dotnet-runtime-7.0 | 7.0.7-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-runtime-7.0-debuginfo | 7.0.7-1.el8_8 | |
dotnet-sdk-6.0 | 6.0.118-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-sdk-6.0-debuginfo | 6.0.118-1.el8_8 | |
dotnet-sdk-7.0 | 7.0.107-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-sdk-7.0-debuginfo | 7.0.107-1.el8_8 | |
dotnet-targeting-pack-6.0 | 6.0.18-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-targeting-pack-7.0 | 7.0.7-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-templates-6.0 | 6.0.118-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-templates-7.0 | 7.0.107-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet6.0-debuginfo | 6.0.118-1.el8_8 | |
dotnet6.0-debugsource | 6.0.118-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.107-1.el8_8 | |
dotnet7.0-debugsource | 7.0.107-1.el8_8 | |
firefox | 102.12.0-1.el8_8 | [RHSA-2023:3590](https://access.redhat.com/errata/RHSA-2023:3590) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34414](https://access.redhat.com/security/cve/CVE-2023-34414), [CVE-2023-34416](https://access.redhat.com/security/cve/CVE-2023-34416))
firefox-debuginfo | 102.12.0-1.el8_8 | |
firefox-debugsource | 102.12.0-1.el8_8 | |
netstandard-targeting-pack-2.1 | 7.0.107-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
platform-python-debug | 3.6.8-51.el8_8.1 | |
platform-python-devel | 3.6.8-51.el8_8.1 | |
python3-debuginfo | 3.6.8-51.el8_8.1 | |
python3-debugsource | 3.6.8-51.el8_8.1 | |
python3-idle | 3.6.8-51.el8_8.1 | |
python3-tkinter | 3.6.8-51.el8_8.1 | |
python3.11 | 3.11.2-2.el8_8.1 | |
python3.11-debuginfo | 3.11.2-2.el8_8.1 | |
python3.11-debugsource | 3.11.2-2.el8_8.1 | |
python3.11-devel | 3.11.2-2.el8_8.1 | |
python3.11-libs | 3.11.2-2.el8_8.1 | |
python3.11-rpm-macros | 3.11.2-2.el8_8.1 | |
python3.11-tkinter | 3.11.2-2.el8_8.1 | |
thunderbird | 102.12.0-1.el8_8 | [RHSA-2023:3588](https://access.redhat.com/errata/RHSA-2023:3588) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34414](https://access.redhat.com/security/cve/CVE-2023-34414), [CVE-2023-34416](https://access.redhat.com/security/cve/CVE-2023-34416))
thunderbird-debuginfo | 102.12.0-1.el8_8 | |
thunderbird-debugsource | 102.12.0-1.el8_8 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.18-1.el8_8 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.7-1.el8_8 | |
dotnet-host-debuginfo | 7.0.7-1.el8_8 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.18-1.el8_8 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.7-1.el8_8 | |
dotnet-runtime-6.0-debuginfo | 6.0.18-1.el8_8 | |
dotnet-runtime-7.0-debuginfo | 7.0.7-1.el8_8 | |
dotnet-sdk-6.0-debuginfo | 6.0.118-1.el8_8 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.118-1.el8_8 | [RHSA-2023:3582](https://access.redhat.com/errata/RHSA-2023:3582) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-sdk-7.0-debuginfo | 7.0.107-1.el8_8 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.107-1.el8_8 | [RHSA-2023:3593](https://access.redhat.com/errata/RHSA-2023:3593) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet6.0-debuginfo | 6.0.118-1.el8_8 | |
dotnet6.0-debugsource | 6.0.118-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.107-1.el8_8 | |
dotnet7.0-debugsource | 7.0.107-1.el8_8 | |
python3.11-debug | 3.11.2-2.el8_8.1 | |
python3.11-debuginfo | 3.11.2-2.el8_8.1 | |
python3.11-debugsource | 3.11.2-2.el8_8.1 | |
python3.11-idle | 3.11.2-2.el8_8.1 | |
python3.11-test | 3.11.2-2.el8_8.1 | |

