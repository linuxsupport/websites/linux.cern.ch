## 2024-10-24

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
NetworkManager-libreswan | 1.2.10-7.el8_10 | [RHSA-2024:8353](https://access.redhat.com/errata/RHSA-2024:8353) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9050](https://access.redhat.com/security/cve/CVE-2024-9050))
NetworkManager-libreswan-debuginfo | 1.2.10-7.el8_10 | |
NetworkManager-libreswan-debugsource | 1.2.10-7.el8_10 | |
NetworkManager-libreswan-gnome | 1.2.10-7.el8_10 | [RHSA-2024:8353](https://access.redhat.com/errata/RHSA-2024:8353) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9050](https://access.redhat.com/security/cve/CVE-2024-9050))
NetworkManager-libreswan-gnome-debuginfo | 1.2.10-7.el8_10 | |
python39 | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python39-debuginfo | 3.9.20-1.module+el8.10.0+22342+478c159e | |
python39-debugsource | 3.9.20-1.module+el8.10.0+22342+478c159e | |
python39-devel | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python39-idle | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python39-libs | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python39-rpm-macros | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python39-test | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python39-tkinter | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python39-debug | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
NetworkManager-libreswan | 1.2.10-7.el8_10 | [RHSA-2024:8353](https://access.redhat.com/errata/RHSA-2024:8353) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9050](https://access.redhat.com/security/cve/CVE-2024-9050))
NetworkManager-libreswan-debuginfo | 1.2.10-7.el8_10 | |
NetworkManager-libreswan-debugsource | 1.2.10-7.el8_10 | |
NetworkManager-libreswan-gnome | 1.2.10-7.el8_10 | [RHSA-2024:8353](https://access.redhat.com/errata/RHSA-2024:8353) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9050](https://access.redhat.com/security/cve/CVE-2024-9050))
NetworkManager-libreswan-gnome-debuginfo | 1.2.10-7.el8_10 | |
python39 | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python39-debuginfo | 3.9.20-1.module+el8.10.0+22342+478c159e | |
python39-debugsource | 3.9.20-1.module+el8.10.0+22342+478c159e | |
python39-devel | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python39-idle | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python39-libs | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python39-rpm-macros | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python39-test | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))
python39-tkinter | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python39-debug | 3.9.20-1.module+el8.10.0+22342+478c159e | [RHSA-2024:8359](https://access.redhat.com/errata/RHSA-2024:8359) | <div class="adv_s">Security Advisory</div> ([CVE-2024-6232](https://access.redhat.com/security/cve/CVE-2024-6232))

