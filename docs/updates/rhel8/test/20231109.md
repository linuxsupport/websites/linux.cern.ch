## 2023-11-09

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
net-snmp-agent-libs-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-debugsource | 5.8-27.el8_8.1 | |
net-snmp-libs | 5.8-27.el8_8.1 | [RHBA-2023:6792](https://access.redhat.com/errata/RHBA-2023:6792) | <div class="adv_b">Bug Fix Advisory</div>
net-snmp-libs-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-perl-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-utils-debuginfo | 5.8-27.el8_8.1 | |
xfsdump | 3.1.8-5.el8_8 | [RHBA-2023:6797](https://access.redhat.com/errata/RHBA-2023:6797) | <div class="adv_b">Bug Fix Advisory</div>
xfsdump-debuginfo | 3.1.8-5.el8_8 | |
xfsdump-debugsource | 3.1.8-5.el8_8 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
net-snmp | 5.8-27.el8_8.1 | [RHBA-2023:6792](https://access.redhat.com/errata/RHBA-2023:6792) | <div class="adv_b">Bug Fix Advisory</div>
net-snmp-agent-libs | 5.8-27.el8_8.1 | [RHBA-2023:6792](https://access.redhat.com/errata/RHBA-2023:6792) | <div class="adv_b">Bug Fix Advisory</div>
net-snmp-agent-libs-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-debugsource | 5.8-27.el8_8.1 | |
net-snmp-devel | 5.8-27.el8_8.1 | [RHBA-2023:6792](https://access.redhat.com/errata/RHBA-2023:6792) | <div class="adv_b">Bug Fix Advisory</div>
net-snmp-libs-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-perl | 5.8-27.el8_8.1 | [RHBA-2023:6792](https://access.redhat.com/errata/RHBA-2023:6792) | <div class="adv_b">Bug Fix Advisory</div>
net-snmp-perl-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-utils | 5.8-27.el8_8.1 | [RHBA-2023:6792](https://access.redhat.com/errata/RHBA-2023:6792) | <div class="adv_b">Bug Fix Advisory</div>
net-snmp-utils-debuginfo | 5.8-27.el8_8.1 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
net-snmp-agent-libs-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-debugsource | 5.8-27.el8_8.1 | |
net-snmp-libs | 5.8-27.el8_8.1 | [RHBA-2023:6792](https://access.redhat.com/errata/RHBA-2023:6792) | <div class="adv_b">Bug Fix Advisory</div>
net-snmp-libs-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-perl-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-utils-debuginfo | 5.8-27.el8_8.1 | |
xfsdump | 3.1.8-5.el8_8 | [RHBA-2023:6797](https://access.redhat.com/errata/RHBA-2023:6797) | <div class="adv_b">Bug Fix Advisory</div>
xfsdump-debuginfo | 3.1.8-5.el8_8 | |
xfsdump-debugsource | 3.1.8-5.el8_8 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
net-snmp | 5.8-27.el8_8.1 | [RHBA-2023:6792](https://access.redhat.com/errata/RHBA-2023:6792) | <div class="adv_b">Bug Fix Advisory</div>
net-snmp-agent-libs | 5.8-27.el8_8.1 | [RHBA-2023:6792](https://access.redhat.com/errata/RHBA-2023:6792) | <div class="adv_b">Bug Fix Advisory</div>
net-snmp-agent-libs-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-debugsource | 5.8-27.el8_8.1 | |
net-snmp-devel | 5.8-27.el8_8.1 | [RHBA-2023:6792](https://access.redhat.com/errata/RHBA-2023:6792) | <div class="adv_b">Bug Fix Advisory</div>
net-snmp-libs-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-perl | 5.8-27.el8_8.1 | [RHBA-2023:6792](https://access.redhat.com/errata/RHBA-2023:6792) | <div class="adv_b">Bug Fix Advisory</div>
net-snmp-perl-debuginfo | 5.8-27.el8_8.1 | |
net-snmp-utils | 5.8-27.el8_8.1 | [RHBA-2023:6792](https://access.redhat.com/errata/RHBA-2023:6792) | <div class="adv_b">Bug Fix Advisory</div>
net-snmp-utils-debuginfo | 5.8-27.el8_8.1 | |

