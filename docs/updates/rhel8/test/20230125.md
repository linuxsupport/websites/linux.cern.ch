## 2023-01-25

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
oracle-release | 1.5-2.rh8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
oracle-release | 1.5-2.rh8.cern | |

