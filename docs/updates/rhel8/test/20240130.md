## 2024-01-30

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tomcat | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-admin-webapps | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-docs-webapp | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-el-3.0-api | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-jsp-2.3-api | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-lib | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-servlet-4.0-api | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-webapps | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tomcat | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-admin-webapps | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-docs-webapp | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-el-3.0-api | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-jsp-2.3-api | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-lib | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-servlet-4.0-api | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-webapps | 9.0.62-27.el8_9.3 | [RHSA-2024:0539](https://access.redhat.com/errata/RHSA-2024:0539) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))

