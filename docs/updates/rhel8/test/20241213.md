## 2024-12-13

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3.11 | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.11-debuginfo | 3.11.11-1.el8_10 | |
python3.11-debugsource | 3.11.11-1.el8_10 | |
python3.11-devel | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.11-libs | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.11-rpm-macros | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.11-tkinter | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12 | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12-debuginfo | 3.12.8-1.el8_10 | |
python3.12-debugsource | 3.12.8-1.el8_10 | |
python3.12-devel | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12-libs | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12-rpm-macros | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12-tkinter | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))

### highavailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pcs | 0.10.18-2.el8_10.3 | [RHSA-2024:10987](https://access.redhat.com/errata/RHSA-2024:10987) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21510](https://access.redhat.com/security/cve/CVE-2024-21510))
pcs-snmp | 0.10.18-2.el8_10.3 | [RHSA-2024:10987](https://access.redhat.com/errata/RHSA-2024:10987) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21510](https://access.redhat.com/security/cve/CVE-2024-21510))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3.11-debug | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.11-debuginfo | 3.11.11-1.el8_10 | |
python3.11-debugsource | 3.11.11-1.el8_10 | |
python3.11-idle | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.11-test | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12-debug | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12-debuginfo | 3.12.8-1.el8_10 | |
python3.12-debugsource | 3.12.8-1.el8_10 | |
python3.12-idle | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12-test | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3.11 | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.11-debuginfo | 3.11.11-1.el8_10 | |
python3.11-debugsource | 3.11.11-1.el8_10 | |
python3.11-devel | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.11-libs | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.11-rpm-macros | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.11-tkinter | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12 | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12-debuginfo | 3.12.8-1.el8_10 | |
python3.12-debugsource | 3.12.8-1.el8_10 | |
python3.12-devel | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12-libs | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12-rpm-macros | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12-tkinter | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3.11-debug | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.11-debuginfo | 3.11.11-1.el8_10 | |
python3.11-debugsource | 3.11.11-1.el8_10 | |
python3.11-idle | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.11-test | 3.11.11-1.el8_10 | [RHSA-2024:10979](https://access.redhat.com/errata/RHSA-2024:10979) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12-debug | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12-debuginfo | 3.12.8-1.el8_10 | |
python3.12-debugsource | 3.12.8-1.el8_10 | |
python3.12-idle | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))
python3.12-test | 3.12.8-1.el8_10 | [RHSA-2024:10980](https://access.redhat.com/errata/RHSA-2024:10980) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12254](https://access.redhat.com/security/cve/CVE-2024-12254), [CVE-2024-9287](https://access.redhat.com/security/cve/CVE-2024-9287))

