## 2024-07-02

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
httpd | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
httpd-debuginfo | 2.4.37-65.module+el8.10.0+21982+14717793 | |
httpd-debugsource | 2.4.37-65.module+el8.10.0+21982+14717793 | |
httpd-devel | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
httpd-filesystem | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
httpd-manual | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
httpd-tools | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
httpd-tools-debuginfo | 2.4.37-65.module+el8.10.0+21982+14717793 | |
mod_ldap | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
mod_ldap-debuginfo | 2.4.37-65.module+el8.10.0+21982+14717793 | |
mod_proxy_html | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
mod_proxy_html-debuginfo | 2.4.37-65.module+el8.10.0+21982+14717793 | |
mod_session | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
mod_session-debuginfo | 2.4.37-65.module+el8.10.0+21982+14717793 | |
mod_ssl | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
mod_ssl-debuginfo | 2.4.37-65.module+el8.10.0+21982+14717793 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
httpd | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
httpd-debuginfo | 2.4.37-65.module+el8.10.0+21982+14717793 | |
httpd-debugsource | 2.4.37-65.module+el8.10.0+21982+14717793 | |
httpd-devel | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
httpd-filesystem | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
httpd-manual | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
httpd-tools | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
httpd-tools-debuginfo | 2.4.37-65.module+el8.10.0+21982+14717793 | |
mod_ldap | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
mod_ldap-debuginfo | 2.4.37-65.module+el8.10.0+21982+14717793 | |
mod_proxy_html | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
mod_proxy_html-debuginfo | 2.4.37-65.module+el8.10.0+21982+14717793 | |
mod_session | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
mod_session-debuginfo | 2.4.37-65.module+el8.10.0+21982+14717793 | |
mod_ssl | 2.4.37-65.module+el8.10.0+21982+14717793 | [RHSA-2024:4197](https://access.redhat.com/errata/RHSA-2024:4197) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38709](https://access.redhat.com/security/cve/CVE-2023-38709))
mod_ssl-debuginfo | 2.4.37-65.module+el8.10.0+21982+14717793 | |

