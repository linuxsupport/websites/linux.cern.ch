## 2024-06-26

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-all | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-core | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-core-debuginfo | 2.43.5-1.el8_10 | |
git-core-doc | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-credential-libsecret | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-credential-libsecret-debuginfo | 2.43.5-1.el8_10 | |
git-daemon | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-daemon-debuginfo | 2.43.5-1.el8_10 | |
git-debuginfo | 2.43.5-1.el8_10 | |
git-debugsource | 2.43.5-1.el8_10 | |
git-email | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-gui | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-instaweb | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-subtree | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-svn | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
gitk | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
gitweb | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
perl-Git | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
perl-Git-SVN | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-all | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-core | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-core-debuginfo | 2.43.5-1.el8_10 | |
git-core-doc | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-credential-libsecret | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-credential-libsecret-debuginfo | 2.43.5-1.el8_10 | |
git-daemon | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-daemon-debuginfo | 2.43.5-1.el8_10 | |
git-debuginfo | 2.43.5-1.el8_10 | |
git-debugsource | 2.43.5-1.el8_10 | |
git-email | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-gui | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-instaweb | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-subtree | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
git-svn | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
gitk | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
gitweb | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
perl-Git | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))
perl-Git-SVN | 2.43.5-1.el8_10 | [RHSA-2024:4084](https://access.redhat.com/errata/RHSA-2024:4084) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32002](https://access.redhat.com/security/cve/CVE-2024-32002), [CVE-2024-32004](https://access.redhat.com/security/cve/CVE-2024-32004), [CVE-2024-32020](https://access.redhat.com/security/cve/CVE-2024-32020), [CVE-2024-32021](https://access.redhat.com/security/cve/CVE-2024-32021), [CVE-2024-32465](https://access.redhat.com/security/cve/CVE-2024-32465))

