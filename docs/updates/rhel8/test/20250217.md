## 2025-02-17

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libxml2 | 2.9.7-18.el8_10.2 | |
libxml2-debuginfo | 2.9.7-18.el8_10.2 | |
libxml2-debugsource | 2.9.7-18.el8_10.2 | |
python3-libxml2 | 2.9.7-18.el8_10.2 | |
python3-libxml2-debuginfo | 2.9.7-18.el8_10.2 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libxml2-debuginfo | 2.9.7-18.el8_10.2 | |
libxml2-debugsource | 2.9.7-18.el8_10.2 | |
libxml2-devel | 2.9.7-18.el8_10.2 | |
python3-libxml2-debuginfo | 2.9.7-18.el8_10.2 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libxml2 | 2.9.7-18.el8_10.2 | |
libxml2-debuginfo | 2.9.7-18.el8_10.2 | |
libxml2-debugsource | 2.9.7-18.el8_10.2 | |
python3-libxml2 | 2.9.7-18.el8_10.2 | |
python3-libxml2-debuginfo | 2.9.7-18.el8_10.2 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libxml2-debuginfo | 2.9.7-18.el8_10.2 | |
libxml2-debugsource | 2.9.7-18.el8_10.2 | |
libxml2-devel | 2.9.7-18.el8_10.2 | |
python3-libxml2-debuginfo | 2.9.7-18.el8_10.2 | |

