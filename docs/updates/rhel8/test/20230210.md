## 2023-02-10

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-redhat-oracleasm-kernel_4_18_0_425_10_1 | 2.0.8-15.1.el8_7 | |
kmod-redhat-oracleasm-kernel_4_18_0_425_3_1 | 2.0.8-15.1.el8_7 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rhc | 0.2.1-12.el8_7 | |
rhc-debuginfo | 0.2.1-12.el8_7 | |
rhc-debugsource | 0.2.1-12.el8_7 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rhc | 0.2.1-12.el8_7 | |
rhc-debuginfo | 0.2.1-12.el8_7 | |
rhc-debugsource | 0.2.1-12.el8_7 | |

