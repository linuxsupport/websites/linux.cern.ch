## 2024-03-26

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.9.1-1.el8_9 | |
firefox-debuginfo | 115.9.1-1.el8_9 | |
firefox-debugsource | 115.9.1-1.el8_9 | |
thunderbird | 115.9.0-1.el8_9 | |
thunderbird-debuginfo | 115.9.0-1.el8_9 | |
thunderbird-debugsource | 115.9.0-1.el8_9 | |

