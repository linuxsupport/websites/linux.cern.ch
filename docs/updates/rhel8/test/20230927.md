## 2023-09-27

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.6.0-2.el8 | [RHBA-2023:5354](https://access.redhat.com/errata/RHBA-2023:5354) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.6.0-2.el8 | [RHBA-2023:5354](https://access.redhat.com/errata/RHBA-2023:5354) | <div class="adv_b">Bug Fix Advisory</div>

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libtiff | 4.0.9-29.el8_8 | [RHSA-2023:5353](https://access.redhat.com/errata/RHSA-2023:5353) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0800](https://access.redhat.com/security/cve/CVE-2023-0800), [CVE-2023-0801](https://access.redhat.com/security/cve/CVE-2023-0801), [CVE-2023-0802](https://access.redhat.com/security/cve/CVE-2023-0802), [CVE-2023-0803](https://access.redhat.com/security/cve/CVE-2023-0803), [CVE-2023-0804](https://access.redhat.com/security/cve/CVE-2023-0804))
libtiff-debuginfo | 4.0.9-29.el8_8 | |
libtiff-debugsource | 4.0.9-29.el8_8 | |
libtiff-devel | 4.0.9-29.el8_8 | [RHSA-2023:5353](https://access.redhat.com/errata/RHSA-2023:5353) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0800](https://access.redhat.com/security/cve/CVE-2023-0800), [CVE-2023-0801](https://access.redhat.com/security/cve/CVE-2023-0801), [CVE-2023-0802](https://access.redhat.com/security/cve/CVE-2023-0802), [CVE-2023-0803](https://access.redhat.com/security/cve/CVE-2023-0803), [CVE-2023-0804](https://access.redhat.com/security/cve/CVE-2023-0804))
libtiff-tools-debuginfo | 4.0.9-29.el8_8 | |
nodejs | 16.20.2-2.module+el8.8.0+19898+ab99ba34 | [RHSA-2023:5360](https://access.redhat.com/errata/RHSA-2023:5360) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs | 18.17.1-1.module+el8.8.0+19757+8ca87034 | [RHSA-2023:5362](https://access.redhat.com/errata/RHSA-2023:5362) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-debuginfo | 16.20.2-2.module+el8.8.0+19898+ab99ba34 | |
nodejs-debuginfo | 18.17.1-1.module+el8.8.0+19757+8ca87034 | |
nodejs-debugsource | 16.20.2-2.module+el8.8.0+19898+ab99ba34 | |
nodejs-debugsource | 18.17.1-1.module+el8.8.0+19757+8ca87034 | |
nodejs-devel | 16.20.2-2.module+el8.8.0+19898+ab99ba34 | [RHSA-2023:5360](https://access.redhat.com/errata/RHSA-2023:5360) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-devel | 18.17.1-1.module+el8.8.0+19757+8ca87034 | [RHSA-2023:5362](https://access.redhat.com/errata/RHSA-2023:5362) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-docs | 16.20.2-2.module+el8.8.0+19898+ab99ba34 | [RHSA-2023:5360](https://access.redhat.com/errata/RHSA-2023:5360) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-docs | 18.17.1-1.module+el8.8.0+19757+8ca87034 | [RHSA-2023:5362](https://access.redhat.com/errata/RHSA-2023:5362) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-full-i18n | 16.20.2-2.module+el8.8.0+19898+ab99ba34 | [RHSA-2023:5360](https://access.redhat.com/errata/RHSA-2023:5360) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-full-i18n | 18.17.1-1.module+el8.8.0+19757+8ca87034 | [RHSA-2023:5362](https://access.redhat.com/errata/RHSA-2023:5362) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-nodemon | 3.0.1-1.module+el8.8.0+19757+8ca87034 | [RHSA-2023:5362](https://access.redhat.com/errata/RHSA-2023:5362) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-nodemon | 3.0.1-1.module+el8.8.0+19764+7eed1ca3 | [RHSA-2023:5360](https://access.redhat.com/errata/RHSA-2023:5360) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-packaging | 26-1.module+el8.8.0+19857+6d2a104d | [RHSA-2023:5360](https://access.redhat.com/errata/RHSA-2023:5360) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
npm | 8.19.4-1.16.20.2.2.module+el8.8.0+19898+ab99ba34 | [RHSA-2023:5360](https://access.redhat.com/errata/RHSA-2023:5360) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
npm | 9.6.7-1.18.17.1.1.module+el8.8.0+19757+8ca87034 | [RHSA-2023:5362](https://access.redhat.com/errata/RHSA-2023:5362) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libtiff-debuginfo | 4.0.9-29.el8_8 | |
libtiff-debugsource | 4.0.9-29.el8_8 | |
libtiff-tools | 4.0.9-29.el8_8 | [RHSA-2023:5353](https://access.redhat.com/errata/RHSA-2023:5353) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0800](https://access.redhat.com/security/cve/CVE-2023-0800), [CVE-2023-0801](https://access.redhat.com/security/cve/CVE-2023-0801), [CVE-2023-0802](https://access.redhat.com/security/cve/CVE-2023-0802), [CVE-2023-0803](https://access.redhat.com/security/cve/CVE-2023-0803), [CVE-2023-0804](https://access.redhat.com/security/cve/CVE-2023-0804))
libtiff-tools-debuginfo | 4.0.9-29.el8_8 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.6.0-2.el8 | [RHBA-2023:5354](https://access.redhat.com/errata/RHBA-2023:5354) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.6.0-2.el8 | [RHBA-2023:5354](https://access.redhat.com/errata/RHBA-2023:5354) | <div class="adv_b">Bug Fix Advisory</div>

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libtiff | 4.0.9-29.el8_8 | [RHSA-2023:5353](https://access.redhat.com/errata/RHSA-2023:5353) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0800](https://access.redhat.com/security/cve/CVE-2023-0800), [CVE-2023-0801](https://access.redhat.com/security/cve/CVE-2023-0801), [CVE-2023-0802](https://access.redhat.com/security/cve/CVE-2023-0802), [CVE-2023-0803](https://access.redhat.com/security/cve/CVE-2023-0803), [CVE-2023-0804](https://access.redhat.com/security/cve/CVE-2023-0804))
libtiff-debuginfo | 4.0.9-29.el8_8 | |
libtiff-debugsource | 4.0.9-29.el8_8 | |
libtiff-devel | 4.0.9-29.el8_8 | [RHSA-2023:5353](https://access.redhat.com/errata/RHSA-2023:5353) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0800](https://access.redhat.com/security/cve/CVE-2023-0800), [CVE-2023-0801](https://access.redhat.com/security/cve/CVE-2023-0801), [CVE-2023-0802](https://access.redhat.com/security/cve/CVE-2023-0802), [CVE-2023-0803](https://access.redhat.com/security/cve/CVE-2023-0803), [CVE-2023-0804](https://access.redhat.com/security/cve/CVE-2023-0804))
libtiff-tools-debuginfo | 4.0.9-29.el8_8 | |
nodejs | 16.20.2-2.module+el8.8.0+19898+ab99ba34 | [RHSA-2023:5360](https://access.redhat.com/errata/RHSA-2023:5360) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs | 18.17.1-1.module+el8.8.0+19757+8ca87034 | [RHSA-2023:5362](https://access.redhat.com/errata/RHSA-2023:5362) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-debuginfo | 16.20.2-2.module+el8.8.0+19898+ab99ba34 | |
nodejs-debuginfo | 18.17.1-1.module+el8.8.0+19757+8ca87034 | |
nodejs-debugsource | 16.20.2-2.module+el8.8.0+19898+ab99ba34 | |
nodejs-debugsource | 18.17.1-1.module+el8.8.0+19757+8ca87034 | |
nodejs-devel | 16.20.2-2.module+el8.8.0+19898+ab99ba34 | [RHSA-2023:5360](https://access.redhat.com/errata/RHSA-2023:5360) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-devel | 18.17.1-1.module+el8.8.0+19757+8ca87034 | [RHSA-2023:5362](https://access.redhat.com/errata/RHSA-2023:5362) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-docs | 16.20.2-2.module+el8.8.0+19898+ab99ba34 | [RHSA-2023:5360](https://access.redhat.com/errata/RHSA-2023:5360) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-docs | 18.17.1-1.module+el8.8.0+19757+8ca87034 | [RHSA-2023:5362](https://access.redhat.com/errata/RHSA-2023:5362) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-full-i18n | 16.20.2-2.module+el8.8.0+19898+ab99ba34 | [RHSA-2023:5360](https://access.redhat.com/errata/RHSA-2023:5360) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-full-i18n | 18.17.1-1.module+el8.8.0+19757+8ca87034 | [RHSA-2023:5362](https://access.redhat.com/errata/RHSA-2023:5362) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-nodemon | 3.0.1-1.module+el8.8.0+19757+8ca87034 | [RHSA-2023:5362](https://access.redhat.com/errata/RHSA-2023:5362) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-nodemon | 3.0.1-1.module+el8.8.0+19764+7eed1ca3 | [RHSA-2023:5360](https://access.redhat.com/errata/RHSA-2023:5360) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
nodejs-packaging | 26-1.module+el8.8.0+19857+6d2a104d | [RHSA-2023:5360](https://access.redhat.com/errata/RHSA-2023:5360) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
npm | 8.19.4-1.16.20.2.2.module+el8.8.0+19898+ab99ba34 | [RHSA-2023:5360](https://access.redhat.com/errata/RHSA-2023:5360) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))
npm | 9.6.7-1.18.17.1.1.module+el8.8.0+19757+8ca87034 | [RHSA-2023:5362](https://access.redhat.com/errata/RHSA-2023:5362) | <div class="adv_s">Security Advisory</div> ([CVE-2022-25883](https://access.redhat.com/security/cve/CVE-2022-25883), [CVE-2023-32002](https://access.redhat.com/security/cve/CVE-2023-32002), [CVE-2023-32006](https://access.redhat.com/security/cve/CVE-2023-32006), [CVE-2023-32559](https://access.redhat.com/security/cve/CVE-2023-32559))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libtiff-debuginfo | 4.0.9-29.el8_8 | |
libtiff-debugsource | 4.0.9-29.el8_8 | |
libtiff-tools | 4.0.9-29.el8_8 | [RHSA-2023:5353](https://access.redhat.com/errata/RHSA-2023:5353) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0800](https://access.redhat.com/security/cve/CVE-2023-0800), [CVE-2023-0801](https://access.redhat.com/security/cve/CVE-2023-0801), [CVE-2023-0802](https://access.redhat.com/security/cve/CVE-2023-0802), [CVE-2023-0803](https://access.redhat.com/security/cve/CVE-2023-0803), [CVE-2023-0804](https://access.redhat.com/security/cve/CVE-2023-0804))
libtiff-tools-debuginfo | 4.0.9-29.el8_8 | |

