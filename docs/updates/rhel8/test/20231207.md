## 2023-12-07

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
squid | 4.15-7.module+el8.9.0+20806+014d88aa.3 | [RHSA-2023:7668](https://access.redhat.com/errata/RHSA-2023:7668) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5824](https://access.redhat.com/security/cve/CVE-2023-5824))
squid-debuginfo | 4.15-7.module+el8.9.0+20806+014d88aa.3 | |
squid-debugsource | 4.15-7.module+el8.9.0+20806+014d88aa.3 | |

