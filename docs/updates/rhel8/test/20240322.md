## 2024-03-22

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
golang | 1.20.12-3.module+el8.9.0+21528+703c3aa2 | [RHSA-2024:1472](https://access.redhat.com/errata/RHSA-2024:1472) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))
golang-bin | 1.20.12-3.module+el8.9.0+21528+703c3aa2 | [RHSA-2024:1472](https://access.redhat.com/errata/RHSA-2024:1472) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))
golang-docs | 1.20.12-3.module+el8.9.0+21528+703c3aa2 | [RHSA-2024:1472](https://access.redhat.com/errata/RHSA-2024:1472) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))
golang-misc | 1.20.12-3.module+el8.9.0+21528+703c3aa2 | [RHSA-2024:1472](https://access.redhat.com/errata/RHSA-2024:1472) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))
golang-src | 1.20.12-3.module+el8.9.0+21528+703c3aa2 | [RHSA-2024:1472](https://access.redhat.com/errata/RHSA-2024:1472) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))
golang-tests | 1.20.12-3.module+el8.9.0+21528+703c3aa2 | [RHSA-2024:1472](https://access.redhat.com/errata/RHSA-2024:1472) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1394](https://access.redhat.com/security/cve/CVE-2024-1394))

