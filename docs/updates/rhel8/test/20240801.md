## 2024-08-01

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
freeradius | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-debugsource | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-devel | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-doc | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-krb5 | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-krb5-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-ldap | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-ldap-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-mysql | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-mysql-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-perl | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-perl-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-postgresql | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-postgresql-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-rest | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-rest-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-sqlite | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-sqlite-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-unixODBC | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-unixODBC-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-utils | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-utils-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
python3-freeradius | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
python3-freeradius-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
freeradius | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-debugsource | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-devel | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-doc | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-krb5 | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-krb5-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-ldap | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-ldap-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-mysql | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-mysql-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-perl | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-perl-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-postgresql | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-postgresql-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-rest | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-rest-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-sqlite | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-sqlite-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-unixODBC | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-unixODBC-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
freeradius-utils | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-utils-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |
python3-freeradius | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | [RHSA-2024:4936](https://access.redhat.com/errata/RHSA-2024:4936) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
python3-freeradius-debuginfo | 3.0.20-15.module+el8.10.0+22065+aa3d48ca | |

