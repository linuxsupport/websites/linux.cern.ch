## 2024-08-15

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.33-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.33-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
bind9.16 | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-chroot | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-debuginfo | 9.16.23-0.22.el8_10 | |
bind9.16-debugsource | 9.16.23-0.22.el8_10 | |
bind9.16-dnssec-utils | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-dnssec-utils-debuginfo | 9.16.23-0.22.el8_10 | |
bind9.16-libs | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-libs-debuginfo | 9.16.23-0.22.el8_10 | |
bind9.16-license | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-utils | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-utils-debuginfo | 9.16.23-0.22.el8_10 | |
dotnet-apphost-pack-6.0 | 6.0.33-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.33-1.el8_10 | |
dotnet-hostfxr-6.0 | 6.0.33-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.33-1.el8_10 | |
dotnet-runtime-6.0 | 6.0.33-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.33-1.el8_10 | |
dotnet-sdk-6.0 | 6.0.133-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.133-1.el8_10 | |
dotnet-targeting-pack-6.0 | 6.0.33-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.133-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.133-1.el8_10 | |
dotnet6.0-debugsource | 6.0.133-1.el8_10 | |
firefox | 115.14.0-2.el8_10 | [RHSA-2024:5391](https://access.redhat.com/errata/RHSA-2024:5391) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7518](https://access.redhat.com/security/cve/CVE-2024-7518), [CVE-2024-7519](https://access.redhat.com/security/cve/CVE-2024-7519), [CVE-2024-7520](https://access.redhat.com/security/cve/CVE-2024-7520), [CVE-2024-7521](https://access.redhat.com/security/cve/CVE-2024-7521), [CVE-2024-7522](https://access.redhat.com/security/cve/CVE-2024-7522), [CVE-2024-7524](https://access.redhat.com/security/cve/CVE-2024-7524), [CVE-2024-7525](https://access.redhat.com/security/cve/CVE-2024-7525), [CVE-2024-7526](https://access.redhat.com/security/cve/CVE-2024-7526), [CVE-2024-7527](https://access.redhat.com/security/cve/CVE-2024-7527), [CVE-2024-7528](https://access.redhat.com/security/cve/CVE-2024-7528), [CVE-2024-7529](https://access.redhat.com/security/cve/CVE-2024-7529))
firefox-debuginfo | 115.14.0-2.el8_10 | |
firefox-debugsource | 115.14.0-2.el8_10 | |
python3-bind9.16 | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
thunderbird | 115.14.0-1.el8_10 | [RHSA-2024:5402](https://access.redhat.com/errata/RHSA-2024:5402) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7518](https://access.redhat.com/security/cve/CVE-2024-7518), [CVE-2024-7519](https://access.redhat.com/security/cve/CVE-2024-7519), [CVE-2024-7520](https://access.redhat.com/security/cve/CVE-2024-7520), [CVE-2024-7521](https://access.redhat.com/security/cve/CVE-2024-7521), [CVE-2024-7522](https://access.redhat.com/security/cve/CVE-2024-7522), [CVE-2024-7525](https://access.redhat.com/security/cve/CVE-2024-7525), [CVE-2024-7526](https://access.redhat.com/security/cve/CVE-2024-7526), [CVE-2024-7527](https://access.redhat.com/security/cve/CVE-2024-7527), [CVE-2024-7528](https://access.redhat.com/security/cve/CVE-2024-7528), [CVE-2024-7529](https://access.redhat.com/security/cve/CVE-2024-7529))
thunderbird-debuginfo | 115.14.0-1.el8_10 | |
thunderbird-debugsource | 115.14.0-1.el8_10 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind9.16-debuginfo | 9.16.23-0.22.el8_10 | |
bind9.16-debugsource | 9.16.23-0.22.el8_10 | |
bind9.16-devel | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-dnssec-utils-debuginfo | 9.16.23-0.22.el8_10 | |
bind9.16-doc | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-libs-debuginfo | 9.16.23-0.22.el8_10 | |
bind9.16-utils-debuginfo | 9.16.23-0.22.el8_10 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.33-1.el8_10 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.33-1.el8_10 | |
dotnet-runtime-6.0-debuginfo | 6.0.33-1.el8_10 | |
dotnet-sdk-6.0-debuginfo | 6.0.133-1.el8_10 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.133-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.133-1.el8_10 | |
dotnet6.0-debugsource | 6.0.133-1.el8_10 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.33-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.33-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
bind9.16 | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-chroot | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-debuginfo | 9.16.23-0.22.el8_10 | |
bind9.16-debugsource | 9.16.23-0.22.el8_10 | |
bind9.16-dnssec-utils | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-dnssec-utils-debuginfo | 9.16.23-0.22.el8_10 | |
bind9.16-libs | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-libs-debuginfo | 9.16.23-0.22.el8_10 | |
bind9.16-license | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-utils | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-utils-debuginfo | 9.16.23-0.22.el8_10 | |
dotnet-apphost-pack-6.0 | 6.0.33-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.33-1.el8_10 | |
dotnet-hostfxr-6.0 | 6.0.33-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.33-1.el8_10 | |
dotnet-runtime-6.0 | 6.0.33-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.33-1.el8_10 | |
dotnet-sdk-6.0 | 6.0.133-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.133-1.el8_10 | |
dotnet-targeting-pack-6.0 | 6.0.33-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.133-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.133-1.el8_10 | |
dotnet6.0-debugsource | 6.0.133-1.el8_10 | |
firefox | 115.14.0-2.el8_10 | [RHSA-2024:5391](https://access.redhat.com/errata/RHSA-2024:5391) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7518](https://access.redhat.com/security/cve/CVE-2024-7518), [CVE-2024-7519](https://access.redhat.com/security/cve/CVE-2024-7519), [CVE-2024-7520](https://access.redhat.com/security/cve/CVE-2024-7520), [CVE-2024-7521](https://access.redhat.com/security/cve/CVE-2024-7521), [CVE-2024-7522](https://access.redhat.com/security/cve/CVE-2024-7522), [CVE-2024-7524](https://access.redhat.com/security/cve/CVE-2024-7524), [CVE-2024-7525](https://access.redhat.com/security/cve/CVE-2024-7525), [CVE-2024-7526](https://access.redhat.com/security/cve/CVE-2024-7526), [CVE-2024-7527](https://access.redhat.com/security/cve/CVE-2024-7527), [CVE-2024-7528](https://access.redhat.com/security/cve/CVE-2024-7528), [CVE-2024-7529](https://access.redhat.com/security/cve/CVE-2024-7529))
firefox-debuginfo | 115.14.0-2.el8_10 | |
firefox-debugsource | 115.14.0-2.el8_10 | |
python3-bind9.16 | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
thunderbird | 115.14.0-1.el8_10 | [RHSA-2024:5402](https://access.redhat.com/errata/RHSA-2024:5402) | <div class="adv_s">Security Advisory</div> ([CVE-2024-7518](https://access.redhat.com/security/cve/CVE-2024-7518), [CVE-2024-7519](https://access.redhat.com/security/cve/CVE-2024-7519), [CVE-2024-7520](https://access.redhat.com/security/cve/CVE-2024-7520), [CVE-2024-7521](https://access.redhat.com/security/cve/CVE-2024-7521), [CVE-2024-7522](https://access.redhat.com/security/cve/CVE-2024-7522), [CVE-2024-7525](https://access.redhat.com/security/cve/CVE-2024-7525), [CVE-2024-7526](https://access.redhat.com/security/cve/CVE-2024-7526), [CVE-2024-7527](https://access.redhat.com/security/cve/CVE-2024-7527), [CVE-2024-7528](https://access.redhat.com/security/cve/CVE-2024-7528), [CVE-2024-7529](https://access.redhat.com/security/cve/CVE-2024-7529))
thunderbird-debuginfo | 115.14.0-1.el8_10 | |
thunderbird-debugsource | 115.14.0-1.el8_10 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind9.16-debuginfo | 9.16.23-0.22.el8_10 | |
bind9.16-debugsource | 9.16.23-0.22.el8_10 | |
bind9.16-devel | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-dnssec-utils-debuginfo | 9.16.23-0.22.el8_10 | |
bind9.16-doc | 9.16.23-0.22.el8_10 | [RHSA-2024:5390](https://access.redhat.com/errata/RHSA-2024:5390) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1737](https://access.redhat.com/security/cve/CVE-2024-1737), [CVE-2024-1975](https://access.redhat.com/security/cve/CVE-2024-1975), [CVE-2024-4076](https://access.redhat.com/security/cve/CVE-2024-4076))
bind9.16-libs-debuginfo | 9.16.23-0.22.el8_10 | |
bind9.16-utils-debuginfo | 9.16.23-0.22.el8_10 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.33-1.el8_10 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.33-1.el8_10 | |
dotnet-runtime-6.0-debuginfo | 6.0.33-1.el8_10 | |
dotnet-sdk-6.0-debuginfo | 6.0.133-1.el8_10 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.133-1.el8_10 | [RHBA-2024:5401](https://access.redhat.com/errata/RHBA-2024:5401) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.133-1.el8_10 | |
dotnet6.0-debugsource | 6.0.133-1.el8_10 | |

