## 2023-08-23

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dnf-plugin-subscription-manager | 1.28.36-3.el8_8 | |
dnf-plugin-subscription-manager-debuginfo | 1.28.36-3.el8_8 | |
python3-cloud-what | 1.28.36-3.el8_8 | |
python3-subscription-manager-rhsm | 1.28.36-3.el8_8 | |
python3-subscription-manager-rhsm-debuginfo | 1.28.36-3.el8_8 | |
python3-syspurpose | 1.28.36-3.el8_8 | |
rhsm-icons | 1.28.36-3.el8_8 | |
subscription-manager | 1.28.36-3.el8_8 | |
subscription-manager-cockpit | 1.28.36-3.el8_8 | |
subscription-manager-debuginfo | 1.28.36-3.el8_8 | |
subscription-manager-debugsource | 1.28.36-3.el8_8 | |
subscription-manager-plugin-ostree | 1.28.36-3.el8_8 | |
subscription-manager-rhsm-certificates | 1.28.36-3.el8_8 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dnf-plugin-subscription-manager-debuginfo | 1.28.36-3.el8_8 | |
python3-subscription-manager-rhsm-debuginfo | 1.28.36-3.el8_8 | |
rhsm-gtk | 1.28.36-3.el8_8 | |
subscription-manager-debuginfo | 1.28.36-3.el8_8 | |
subscription-manager-debugsource | 1.28.36-3.el8_8 | |
subscription-manager-initial-setup-addon | 1.28.36-3.el8_8 | |
subscription-manager-migration | 1.28.36-3.el8_8 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dnf-plugin-subscription-manager | 1.28.36-3.el8_8 | [RHSA-2023:4706](https://access.redhat.com/errata/RHSA-2023:4706) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
dnf-plugin-subscription-manager-debuginfo | 1.28.36-3.el8_8 | |
python3-cloud-what | 1.28.36-3.el8_8 | [RHSA-2023:4706](https://access.redhat.com/errata/RHSA-2023:4706) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
python3-subscription-manager-rhsm | 1.28.36-3.el8_8 | [RHSA-2023:4706](https://access.redhat.com/errata/RHSA-2023:4706) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
python3-subscription-manager-rhsm-debuginfo | 1.28.36-3.el8_8 | |
python3-syspurpose | 1.28.36-3.el8_8 | [RHSA-2023:4706](https://access.redhat.com/errata/RHSA-2023:4706) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
rhsm-icons | 1.28.36-3.el8_8 | [RHSA-2023:4706](https://access.redhat.com/errata/RHSA-2023:4706) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
subscription-manager | 1.28.36-3.el8_8 | [RHSA-2023:4706](https://access.redhat.com/errata/RHSA-2023:4706) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
subscription-manager-cockpit | 1.28.36-3.el8_8 | [RHSA-2023:4706](https://access.redhat.com/errata/RHSA-2023:4706) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
subscription-manager-debuginfo | 1.28.36-3.el8_8 | |
subscription-manager-debugsource | 1.28.36-3.el8_8 | |
subscription-manager-plugin-ostree | 1.28.36-3.el8_8 | [RHSA-2023:4706](https://access.redhat.com/errata/RHSA-2023:4706) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))
subscription-manager-rhsm-certificates | 1.28.36-3.el8_8 | [RHSA-2023:4706](https://access.redhat.com/errata/RHSA-2023:4706) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3899](https://access.redhat.com/security/cve/CVE-2023-3899))

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dnf-plugin-subscription-manager-debuginfo | 1.28.36-3.el8_8 | |
python3-subscription-manager-rhsm-debuginfo | 1.28.36-3.el8_8 | |
rhsm-gtk | 1.28.36-3.el8_8 | |
subscription-manager-debuginfo | 1.28.36-3.el8_8 | |
subscription-manager-debugsource | 1.28.36-3.el8_8 | |
subscription-manager-initial-setup-addon | 1.28.36-3.el8_8 | |
subscription-manager-migration | 1.28.36-3.el8_8 | |

