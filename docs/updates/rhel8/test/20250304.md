## 2025-03-04

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
webkit2gtk3 | 2.46.6-1.el8_10 | [RHSA-2025:2034](https://access.redhat.com/errata/RHSA-2025:2034) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54543](https://access.redhat.com/security/cve/CVE-2024-54543), [CVE-2025-24143](https://access.redhat.com/security/cve/CVE-2025-24143), [CVE-2025-24150](https://access.redhat.com/security/cve/CVE-2025-24150), [CVE-2025-24158](https://access.redhat.com/security/cve/CVE-2025-24158), [CVE-2025-24162](https://access.redhat.com/security/cve/CVE-2025-24162))
webkit2gtk3-debuginfo | 2.46.6-1.el8_10 | |
webkit2gtk3-debugsource | 2.46.6-1.el8_10 | |
webkit2gtk3-devel | 2.46.6-1.el8_10 | [RHSA-2025:2034](https://access.redhat.com/errata/RHSA-2025:2034) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54543](https://access.redhat.com/security/cve/CVE-2024-54543), [CVE-2025-24143](https://access.redhat.com/security/cve/CVE-2025-24143), [CVE-2025-24150](https://access.redhat.com/security/cve/CVE-2025-24150), [CVE-2025-24158](https://access.redhat.com/security/cve/CVE-2025-24158), [CVE-2025-24162](https://access.redhat.com/security/cve/CVE-2025-24162))
webkit2gtk3-devel-debuginfo | 2.46.6-1.el8_10 | |
webkit2gtk3-jsc | 2.46.6-1.el8_10 | [RHSA-2025:2034](https://access.redhat.com/errata/RHSA-2025:2034) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54543](https://access.redhat.com/security/cve/CVE-2024-54543), [CVE-2025-24143](https://access.redhat.com/security/cve/CVE-2025-24143), [CVE-2025-24150](https://access.redhat.com/security/cve/CVE-2025-24150), [CVE-2025-24158](https://access.redhat.com/security/cve/CVE-2025-24158), [CVE-2025-24162](https://access.redhat.com/security/cve/CVE-2025-24162))
webkit2gtk3-jsc-debuginfo | 2.46.6-1.el8_10 | |
webkit2gtk3-jsc-devel | 2.46.6-1.el8_10 | [RHSA-2025:2034](https://access.redhat.com/errata/RHSA-2025:2034) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54543](https://access.redhat.com/security/cve/CVE-2024-54543), [CVE-2025-24143](https://access.redhat.com/security/cve/CVE-2025-24143), [CVE-2025-24150](https://access.redhat.com/security/cve/CVE-2025-24150), [CVE-2025-24158](https://access.redhat.com/security/cve/CVE-2025-24158), [CVE-2025-24162](https://access.redhat.com/security/cve/CVE-2025-24162))
webkit2gtk3-jsc-devel-debuginfo | 2.46.6-1.el8_10 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
webkit2gtk3 | 2.46.6-1.el8_10 | [RHSA-2025:2034](https://access.redhat.com/errata/RHSA-2025:2034) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54543](https://access.redhat.com/security/cve/CVE-2024-54543), [CVE-2025-24143](https://access.redhat.com/security/cve/CVE-2025-24143), [CVE-2025-24150](https://access.redhat.com/security/cve/CVE-2025-24150), [CVE-2025-24158](https://access.redhat.com/security/cve/CVE-2025-24158), [CVE-2025-24162](https://access.redhat.com/security/cve/CVE-2025-24162))
webkit2gtk3-debuginfo | 2.46.6-1.el8_10 | |
webkit2gtk3-debugsource | 2.46.6-1.el8_10 | |
webkit2gtk3-devel | 2.46.6-1.el8_10 | [RHSA-2025:2034](https://access.redhat.com/errata/RHSA-2025:2034) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54543](https://access.redhat.com/security/cve/CVE-2024-54543), [CVE-2025-24143](https://access.redhat.com/security/cve/CVE-2025-24143), [CVE-2025-24150](https://access.redhat.com/security/cve/CVE-2025-24150), [CVE-2025-24158](https://access.redhat.com/security/cve/CVE-2025-24158), [CVE-2025-24162](https://access.redhat.com/security/cve/CVE-2025-24162))
webkit2gtk3-devel-debuginfo | 2.46.6-1.el8_10 | |
webkit2gtk3-jsc | 2.46.6-1.el8_10 | [RHSA-2025:2034](https://access.redhat.com/errata/RHSA-2025:2034) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54543](https://access.redhat.com/security/cve/CVE-2024-54543), [CVE-2025-24143](https://access.redhat.com/security/cve/CVE-2025-24143), [CVE-2025-24150](https://access.redhat.com/security/cve/CVE-2025-24150), [CVE-2025-24158](https://access.redhat.com/security/cve/CVE-2025-24158), [CVE-2025-24162](https://access.redhat.com/security/cve/CVE-2025-24162))
webkit2gtk3-jsc-debuginfo | 2.46.6-1.el8_10 | |
webkit2gtk3-jsc-devel | 2.46.6-1.el8_10 | [RHSA-2025:2034](https://access.redhat.com/errata/RHSA-2025:2034) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54543](https://access.redhat.com/security/cve/CVE-2024-54543), [CVE-2025-24143](https://access.redhat.com/security/cve/CVE-2025-24143), [CVE-2025-24150](https://access.redhat.com/security/cve/CVE-2025-24150), [CVE-2025-24158](https://access.redhat.com/security/cve/CVE-2025-24158), [CVE-2025-24162](https://access.redhat.com/security/cve/CVE-2025-24162))
webkit2gtk3-jsc-devel-debuginfo | 2.46.6-1.el8_10 | |

