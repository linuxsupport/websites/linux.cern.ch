## 2023-11-23

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb | 4.18.6-2.el8_9 | |
ctdb-debuginfo | 4.18.6-2.el8_9 | |
libnetapi | 4.18.6-2.el8_9 | |
libnetapi-debuginfo | 4.18.6-2.el8_9 | |
libsmbclient | 4.18.6-2.el8_9 | |
libsmbclient-debuginfo | 4.18.6-2.el8_9 | |
libwbclient | 4.18.6-2.el8_9 | |
libwbclient-debuginfo | 4.18.6-2.el8_9 | |
python3-samba | 4.18.6-2.el8_9 | |
python3-samba-dc | 4.18.6-2.el8_9 | |
python3-samba-dc-debuginfo | 4.18.6-2.el8_9 | |
python3-samba-debuginfo | 4.18.6-2.el8_9 | |
python3-samba-test | 4.18.6-2.el8_9 | |
samba | 4.18.6-2.el8_9 | |
samba-client | 4.18.6-2.el8_9 | |
samba-client-debuginfo | 4.18.6-2.el8_9 | |
samba-client-libs | 4.18.6-2.el8_9 | |
samba-client-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-common | 4.18.6-2.el8_9 | |
samba-common-libs | 4.18.6-2.el8_9 | |
samba-common-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-common-tools | 4.18.6-2.el8_9 | |
samba-common-tools-debuginfo | 4.18.6-2.el8_9 | |
samba-dc-libs | 4.18.6-2.el8_9 | |
samba-dc-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-dcerpc | 4.18.6-2.el8_9 | |
samba-dcerpc-debuginfo | 4.18.6-2.el8_9 | |
samba-debuginfo | 4.18.6-2.el8_9 | |
samba-debugsource | 4.18.6-2.el8_9 | |
samba-krb5-printing | 4.18.6-2.el8_9 | |
samba-krb5-printing-debuginfo | 4.18.6-2.el8_9 | |
samba-ldb-ldap-modules | 4.18.6-2.el8_9 | |
samba-ldb-ldap-modules-debuginfo | 4.18.6-2.el8_9 | |
samba-libs | 4.18.6-2.el8_9 | |
samba-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-pidl | 4.18.6-2.el8_9 | |
samba-test | 4.18.6-2.el8_9 | |
samba-test-debuginfo | 4.18.6-2.el8_9 | |
samba-test-libs | 4.18.6-2.el8_9 | |
samba-test-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-tools | 4.18.6-2.el8_9 | |
samba-usershares | 4.18.6-2.el8_9 | |
samba-vfs-iouring-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind | 4.18.6-2.el8_9 | |
samba-winbind-clients | 4.18.6-2.el8_9 | |
samba-winbind-clients-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-krb5-locator | 4.18.6-2.el8_9 | |
samba-winbind-krb5-locator-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-modules | 4.18.6-2.el8_9 | |
samba-winbind-modules-debuginfo | 4.18.6-2.el8_9 | |
samba-winexe | 4.18.6-2.el8_9 | |
samba-winexe-debuginfo | 4.18.6-2.el8_9 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.18.6-2.el8_9 | |
libnetapi-debuginfo | 4.18.6-2.el8_9 | |
libsmbclient-debuginfo | 4.18.6-2.el8_9 | |
libwbclient-debuginfo | 4.18.6-2.el8_9 | |
python3-samba-dc-debuginfo | 4.18.6-2.el8_9 | |
python3-samba-debuginfo | 4.18.6-2.el8_9 | |
samba-client-debuginfo | 4.18.6-2.el8_9 | |
samba-client-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-common-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-common-tools-debuginfo | 4.18.6-2.el8_9 | |
samba-dc-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-dcerpc-debuginfo | 4.18.6-2.el8_9 | |
samba-debuginfo | 4.18.6-2.el8_9 | |
samba-debugsource | 4.18.6-2.el8_9 | |
samba-krb5-printing-debuginfo | 4.18.6-2.el8_9 | |
samba-ldb-ldap-modules-debuginfo | 4.18.6-2.el8_9 | |
samba-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-test-debuginfo | 4.18.6-2.el8_9 | |
samba-test-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-vfs-iouring | 4.18.6-2.el8_9 | |
samba-vfs-iouring-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-clients-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-krb5-locator-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-modules-debuginfo | 4.18.6-2.el8_9 | |
samba-winexe-debuginfo | 4.18.6-2.el8_9 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.18.6-2.el8_9 | |
libnetapi-debuginfo | 4.18.6-2.el8_9 | |
libnetapi-devel | 4.18.6-2.el8_9 | |
libsmbclient-debuginfo | 4.18.6-2.el8_9 | |
libsmbclient-devel | 4.18.6-2.el8_9 | |
libwbclient-debuginfo | 4.18.6-2.el8_9 | |
libwbclient-devel | 4.18.6-2.el8_9 | |
python3-samba-dc-debuginfo | 4.18.6-2.el8_9 | |
python3-samba-debuginfo | 4.18.6-2.el8_9 | |
python3-samba-devel | 4.18.6-2.el8_9 | |
samba-client-debuginfo | 4.18.6-2.el8_9 | |
samba-client-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-common-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-common-tools-debuginfo | 4.18.6-2.el8_9 | |
samba-dc-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-dcerpc-debuginfo | 4.18.6-2.el8_9 | |
samba-debuginfo | 4.18.6-2.el8_9 | |
samba-debugsource | 4.18.6-2.el8_9 | |
samba-devel | 4.18.6-2.el8_9 | |
samba-krb5-printing-debuginfo | 4.18.6-2.el8_9 | |
samba-ldb-ldap-modules-debuginfo | 4.18.6-2.el8_9 | |
samba-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-test-debuginfo | 4.18.6-2.el8_9 | |
samba-test-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-vfs-iouring-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-clients-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-krb5-locator-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-modules-debuginfo | 4.18.6-2.el8_9 | |
samba-winexe-debuginfo | 4.18.6-2.el8_9 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
ctdb-debuginfo | 4.18.6-2.el8_9 | |
libnetapi | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
libnetapi-debuginfo | 4.18.6-2.el8_9 | |
libsmbclient | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
libsmbclient-debuginfo | 4.18.6-2.el8_9 | |
libwbclient | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
libwbclient-debuginfo | 4.18.6-2.el8_9 | |
python3-samba | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
python3-samba-dc | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
python3-samba-dc-debuginfo | 4.18.6-2.el8_9 | |
python3-samba-debuginfo | 4.18.6-2.el8_9 | |
python3-samba-test | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-client | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-client-debuginfo | 4.18.6-2.el8_9 | |
samba-client-libs | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-client-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-common | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-common-libs | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-common-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-common-tools | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-common-tools-debuginfo | 4.18.6-2.el8_9 | |
samba-dc-libs | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-dc-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-dcerpc | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-dcerpc-debuginfo | 4.18.6-2.el8_9 | |
samba-debuginfo | 4.18.6-2.el8_9 | |
samba-debugsource | 4.18.6-2.el8_9 | |
samba-krb5-printing | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-krb5-printing-debuginfo | 4.18.6-2.el8_9 | |
samba-ldb-ldap-modules | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-ldb-ldap-modules-debuginfo | 4.18.6-2.el8_9 | |
samba-libs | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-pidl | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-test | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-test-debuginfo | 4.18.6-2.el8_9 | |
samba-test-libs | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-test-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-tools | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-usershares | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-vfs-iouring-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-winbind-clients | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-winbind-clients-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-krb5-locator | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-winbind-krb5-locator-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-modules | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-winbind-modules-debuginfo | 4.18.6-2.el8_9 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.18.6-2.el8_9 | |
libnetapi-debuginfo | 4.18.6-2.el8_9 | |
libsmbclient-debuginfo | 4.18.6-2.el8_9 | |
libwbclient-debuginfo | 4.18.6-2.el8_9 | |
python3-samba-dc-debuginfo | 4.18.6-2.el8_9 | |
python3-samba-debuginfo | 4.18.6-2.el8_9 | |
samba-client-debuginfo | 4.18.6-2.el8_9 | |
samba-client-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-common-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-common-tools-debuginfo | 4.18.6-2.el8_9 | |
samba-dc-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-dcerpc-debuginfo | 4.18.6-2.el8_9 | |
samba-debuginfo | 4.18.6-2.el8_9 | |
samba-debugsource | 4.18.6-2.el8_9 | |
samba-krb5-printing-debuginfo | 4.18.6-2.el8_9 | |
samba-ldb-ldap-modules-debuginfo | 4.18.6-2.el8_9 | |
samba-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-test-debuginfo | 4.18.6-2.el8_9 | |
samba-test-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-vfs-iouring | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-vfs-iouring-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-clients-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-krb5-locator-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-modules-debuginfo | 4.18.6-2.el8_9 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.18.6-2.el8_9 | |
libnetapi-debuginfo | 4.18.6-2.el8_9 | |
libnetapi-devel | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
libsmbclient-debuginfo | 4.18.6-2.el8_9 | |
libsmbclient-devel | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
libwbclient-debuginfo | 4.18.6-2.el8_9 | |
libwbclient-devel | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
python3-samba-dc-debuginfo | 4.18.6-2.el8_9 | |
python3-samba-debuginfo | 4.18.6-2.el8_9 | |
python3-samba-devel | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-client-debuginfo | 4.18.6-2.el8_9 | |
samba-client-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-common-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-common-tools-debuginfo | 4.18.6-2.el8_9 | |
samba-dc-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-dcerpc-debuginfo | 4.18.6-2.el8_9 | |
samba-debuginfo | 4.18.6-2.el8_9 | |
samba-debugsource | 4.18.6-2.el8_9 | |
samba-devel | 4.18.6-2.el8_9 | [RHSA-2023:7467](https://access.redhat.com/errata/RHSA-2023:7467) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3961](https://access.redhat.com/security/cve/CVE-2023-3961), [CVE-2023-4091](https://access.redhat.com/security/cve/CVE-2023-4091), [CVE-2023-42669](https://access.redhat.com/security/cve/CVE-2023-42669))
samba-krb5-printing-debuginfo | 4.18.6-2.el8_9 | |
samba-ldb-ldap-modules-debuginfo | 4.18.6-2.el8_9 | |
samba-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-test-debuginfo | 4.18.6-2.el8_9 | |
samba-test-libs-debuginfo | 4.18.6-2.el8_9 | |
samba-vfs-iouring-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-clients-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-krb5-locator-debuginfo | 4.18.6-2.el8_9 | |
samba-winbind-modules-debuginfo | 4.18.6-2.el8_9 | |

