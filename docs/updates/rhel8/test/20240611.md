## 2024-06-11

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.11.0-1.el8_10 | |
firefox-debuginfo | 115.11.0-1.el8_10 | |
firefox-debugsource | 115.11.0-1.el8_10 | |
ipa-client | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-client-common | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-client-debuginfo | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-client-epn | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-client-samba | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-common | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-debuginfo | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-debugsource | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-python-compat | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-selinux | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-server | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-server-common | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-server-debuginfo | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-server-dns | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-server-trust-ad | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-server-trust-ad-debuginfo | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
python3-ipaclient | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
python3-ipalib | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
python3-ipaserver | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
python3-ipatests | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
thunderbird | 115.11.0-1.el8_10 | |
thunderbird-debuginfo | 115.11.0-1.el8_10 | |
thunderbird-debugsource | 115.11.0-1.el8_10 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.11.0-1.el8_10 | |
firefox-debuginfo | 115.11.0-1.el8_10 | |
firefox-debugsource | 115.11.0-1.el8_10 | |
ipa-client | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-client-common | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-client-debuginfo | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-client-epn | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-client-samba | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-common | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-debuginfo | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-debugsource | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-python-compat | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-selinux | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-server | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-server-common | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-server-debuginfo | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-server-dns | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-server-trust-ad | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
ipa-server-trust-ad-debuginfo | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
python3-ipaclient | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
python3-ipalib | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
python3-ipaserver | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
python3-ipatests | 4.9.13-10.module+el8.10.0+21944+3943ad77 | |
thunderbird | 115.11.0-1.el8_10 | |
thunderbird-debuginfo | 115.11.0-1.el8_10 | |
thunderbird-debugsource | 115.11.0-1.el8_10 | |

