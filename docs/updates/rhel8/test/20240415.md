## 2024-04-15

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-dyndb-ldap | 11.6-5.module+el8.9.0+21623+25375490 | [RHBA-2024:1798](https://access.redhat.com/errata/RHBA-2024:1798) | <div class="adv_b">Bug Fix Advisory</div>
bind-dyndb-ldap-debuginfo | 11.6-5.module+el8.9.0+21623+25375490 | |
bind-dyndb-ldap-debugsource | 11.6-5.module+el8.9.0+21623+25375490 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-dyndb-ldap | 11.6-5.module+el8.9.0+21623+25375490 | [RHBA-2024:1798](https://access.redhat.com/errata/RHBA-2024:1798) | <div class="adv_b">Bug Fix Advisory</div>
bind-dyndb-ldap-debuginfo | 11.6-5.module+el8.9.0+21623+25375490 | |
bind-dyndb-ldap-debugsource | 11.6-5.module+el8.9.0+21623+25375490 | |

