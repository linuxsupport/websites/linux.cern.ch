## 2023-11-28

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.5.0-1.el8_9 | [RHSA-2023:7508](https://access.redhat.com/errata/RHSA-2023:7508) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6204](https://access.redhat.com/security/cve/CVE-2023-6204), [CVE-2023-6205](https://access.redhat.com/security/cve/CVE-2023-6205), [CVE-2023-6206](https://access.redhat.com/security/cve/CVE-2023-6206), [CVE-2023-6207](https://access.redhat.com/security/cve/CVE-2023-6207), [CVE-2023-6208](https://access.redhat.com/security/cve/CVE-2023-6208), [CVE-2023-6209](https://access.redhat.com/security/cve/CVE-2023-6209), [CVE-2023-6212](https://access.redhat.com/security/cve/CVE-2023-6212))
firefox-debuginfo | 115.5.0-1.el8_9 | |
firefox-debugsource | 115.5.0-1.el8_9 | |
thunderbird | 115.5.0-1.el8_9 | [RHSA-2023:7500](https://access.redhat.com/errata/RHSA-2023:7500) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6204](https://access.redhat.com/security/cve/CVE-2023-6204), [CVE-2023-6205](https://access.redhat.com/security/cve/CVE-2023-6205), [CVE-2023-6206](https://access.redhat.com/security/cve/CVE-2023-6206), [CVE-2023-6207](https://access.redhat.com/security/cve/CVE-2023-6207), [CVE-2023-6208](https://access.redhat.com/security/cve/CVE-2023-6208), [CVE-2023-6209](https://access.redhat.com/security/cve/CVE-2023-6209), [CVE-2023-6212](https://access.redhat.com/security/cve/CVE-2023-6212))
thunderbird-debuginfo | 115.5.0-1.el8_9 | |
thunderbird-debugsource | 115.5.0-1.el8_9 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.5.0-1.el8_9 | [RHSA-2023:7508](https://access.redhat.com/errata/RHSA-2023:7508) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6204](https://access.redhat.com/security/cve/CVE-2023-6204), [CVE-2023-6205](https://access.redhat.com/security/cve/CVE-2023-6205), [CVE-2023-6206](https://access.redhat.com/security/cve/CVE-2023-6206), [CVE-2023-6207](https://access.redhat.com/security/cve/CVE-2023-6207), [CVE-2023-6208](https://access.redhat.com/security/cve/CVE-2023-6208), [CVE-2023-6209](https://access.redhat.com/security/cve/CVE-2023-6209), [CVE-2023-6212](https://access.redhat.com/security/cve/CVE-2023-6212))
firefox-debuginfo | 115.5.0-1.el8_9 | |
firefox-debugsource | 115.5.0-1.el8_9 | |
thunderbird | 115.5.0-1.el8_9 | [RHSA-2023:7500](https://access.redhat.com/errata/RHSA-2023:7500) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6204](https://access.redhat.com/security/cve/CVE-2023-6204), [CVE-2023-6205](https://access.redhat.com/security/cve/CVE-2023-6205), [CVE-2023-6206](https://access.redhat.com/security/cve/CVE-2023-6206), [CVE-2023-6207](https://access.redhat.com/security/cve/CVE-2023-6207), [CVE-2023-6208](https://access.redhat.com/security/cve/CVE-2023-6208), [CVE-2023-6209](https://access.redhat.com/security/cve/CVE-2023-6209), [CVE-2023-6212](https://access.redhat.com/security/cve/CVE-2023-6212))
thunderbird-debuginfo | 115.5.0-1.el8_9 | |
thunderbird-debugsource | 115.5.0-1.el8_9 | |

