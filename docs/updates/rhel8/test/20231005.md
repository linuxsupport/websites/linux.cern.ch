## 2023-10-05

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aardvark-dns | 1.5.0-2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
buildah | 1.29.1-2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
buildah-debuginfo | 1.29.1-2.module+el8.8.0+19993+47c8ef84 | |
buildah-debugsource | 1.29.1-2.module+el8.8.0+19993+47c8ef84 | |
buildah-tests | 1.29.1-2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
buildah-tests-debuginfo | 1.29.1-2.module+el8.8.0+19993+47c8ef84 | |
cockpit-podman | 63.1-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
conmon | 2.1.6-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
conmon-debuginfo | 2.1.6-1.module+el8.8.0+19993+47c8ef84 | |
conmon-debugsource | 2.1.6-1.module+el8.8.0+19993+47c8ef84 | |
container-selinux | 2.205.0-2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
containernetworking-plugins | 1.2.0-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
containernetworking-plugins-debuginfo | 1.2.0-1.module+el8.8.0+19993+47c8ef84 | |
containernetworking-plugins-debugsource | 1.2.0-1.module+el8.8.0+19993+47c8ef84 | |
containers-common | 1-64.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
crit | 3.15-4.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
criu | 3.15-4.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
criu-debuginfo | 3.15-4.module+el8.8.0+19993+47c8ef84 | |
criu-debugsource | 3.15-4.module+el8.8.0+19993+47c8ef84 | |
criu-devel | 3.15-4.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
criu-libs | 3.15-4.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
criu-libs-debuginfo | 3.15-4.module+el8.8.0+19993+47c8ef84 | |
crun | 1.8.4-2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
crun-debuginfo | 1.8.4-2.module+el8.8.0+19993+47c8ef84 | |
crun-debugsource | 1.8.4-2.module+el8.8.0+19993+47c8ef84 | |
firefox | 115.3.1-1.el8_8 | [RHSA-2023:5433](https://access.redhat.com/errata/RHSA-2023:5433) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3600](https://access.redhat.com/security/cve/CVE-2023-3600), [CVE-2023-5169](https://access.redhat.com/security/cve/CVE-2023-5169), [CVE-2023-5171](https://access.redhat.com/security/cve/CVE-2023-5171), [CVE-2023-5176](https://access.redhat.com/security/cve/CVE-2023-5176), [CVE-2023-5217](https://access.redhat.com/security/cve/CVE-2023-5217))
firefox-debuginfo | 115.3.1-1.el8_8 | |
firefox-debugsource | 115.3.1-1.el8_8 | |
fuse-overlayfs | 1.11-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
fuse-overlayfs-debuginfo | 1.11-1.module+el8.8.0+19993+47c8ef84 | |
fuse-overlayfs-debugsource | 1.11-1.module+el8.8.0+19993+47c8ef84 | |
libslirp | 4.4.0-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
libslirp-debuginfo | 4.4.0-1.module+el8.8.0+19993+47c8ef84 | |
libslirp-debugsource | 4.4.0-1.module+el8.8.0+19993+47c8ef84 | |
libslirp-devel | 4.4.0-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
netavark | 1.5.1-2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
oci-seccomp-bpf-hook | 1.2.8-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
oci-seccomp-bpf-hook-debuginfo | 1.2.8-1.module+el8.8.0+19993+47c8ef84 | |
oci-seccomp-bpf-hook-debugsource | 1.2.8-1.module+el8.8.0+19993+47c8ef84 | |
podman | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
podman-catatonit | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
podman-catatonit-debuginfo | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | |
podman-debuginfo | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | |
podman-debugsource | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | |
podman-docker | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
podman-gvproxy | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
podman-gvproxy-debuginfo | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | |
podman-plugins | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
podman-plugins-debuginfo | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | |
podman-remote | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
podman-remote-debuginfo | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | |
podman-tests | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
python3-criu | 3.15-4.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
python3-podman | 4.4.1-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
runc | 1.1.4-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
runc-debuginfo | 1.1.4-1.module+el8.8.0+19993+47c8ef84 | |
runc-debugsource | 1.1.4-1.module+el8.8.0+19993+47c8ef84 | |
skopeo | 1.11.2-0.2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
skopeo-tests | 1.11.2-0.2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
slirp4netns | 1.2.0-2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
slirp4netns-debuginfo | 1.2.0-2.module+el8.8.0+19993+47c8ef84 | |
slirp4netns-debugsource | 1.2.0-2.module+el8.8.0+19993+47c8ef84 | |
thunderbird | 115.3.1-1.el8_8 | [RHSA-2023:5428](https://access.redhat.com/errata/RHSA-2023:5428) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3600](https://access.redhat.com/security/cve/CVE-2023-3600), [CVE-2023-5169](https://access.redhat.com/security/cve/CVE-2023-5169), [CVE-2023-5171](https://access.redhat.com/security/cve/CVE-2023-5171), [CVE-2023-5176](https://access.redhat.com/security/cve/CVE-2023-5176), [CVE-2023-5217](https://access.redhat.com/security/cve/CVE-2023-5217))
thunderbird-debuginfo | 115.3.1-1.el8_8 | |
thunderbird-debugsource | 115.3.1-1.el8_8 | |
toolbox | 0.0.99.3-7.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
toolbox-debuginfo | 0.0.99.3-7.module+el8.8.0+19993+47c8ef84 | |
toolbox-debugsource | 0.0.99.3-7.module+el8.8.0+19993+47c8ef84 | |
toolbox-tests | 0.0.99.3-7.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
udica | 0.2.6-20.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aardvark-dns | 1.5.0-2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
buildah | 1.29.1-2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
buildah-debuginfo | 1.29.1-2.module+el8.8.0+19993+47c8ef84 | |
buildah-debugsource | 1.29.1-2.module+el8.8.0+19993+47c8ef84 | |
buildah-tests | 1.29.1-2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
buildah-tests-debuginfo | 1.29.1-2.module+el8.8.0+19993+47c8ef84 | |
cockpit-podman | 63.1-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
conmon | 2.1.6-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
conmon-debuginfo | 2.1.6-1.module+el8.8.0+19993+47c8ef84 | |
conmon-debugsource | 2.1.6-1.module+el8.8.0+19993+47c8ef84 | |
container-selinux | 2.205.0-2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
containernetworking-plugins | 1.2.0-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
containernetworking-plugins-debuginfo | 1.2.0-1.module+el8.8.0+19993+47c8ef84 | |
containernetworking-plugins-debugsource | 1.2.0-1.module+el8.8.0+19993+47c8ef84 | |
containers-common | 1-64.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
crit | 3.15-4.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
criu | 3.15-4.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
criu-debuginfo | 3.15-4.module+el8.8.0+19993+47c8ef84 | |
criu-debugsource | 3.15-4.module+el8.8.0+19993+47c8ef84 | |
criu-devel | 3.15-4.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
criu-libs | 3.15-4.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
criu-libs-debuginfo | 3.15-4.module+el8.8.0+19993+47c8ef84 | |
crun | 1.8.4-2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
crun-debuginfo | 1.8.4-2.module+el8.8.0+19993+47c8ef84 | |
crun-debugsource | 1.8.4-2.module+el8.8.0+19993+47c8ef84 | |
firefox | 115.3.1-1.el8_8 | [RHSA-2023:5433](https://access.redhat.com/errata/RHSA-2023:5433) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3600](https://access.redhat.com/security/cve/CVE-2023-3600), [CVE-2023-5169](https://access.redhat.com/security/cve/CVE-2023-5169), [CVE-2023-5171](https://access.redhat.com/security/cve/CVE-2023-5171), [CVE-2023-5176](https://access.redhat.com/security/cve/CVE-2023-5176), [CVE-2023-5217](https://access.redhat.com/security/cve/CVE-2023-5217))
firefox-debuginfo | 115.3.1-1.el8_8 | |
firefox-debugsource | 115.3.1-1.el8_8 | |
fuse-overlayfs | 1.11-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
fuse-overlayfs-debuginfo | 1.11-1.module+el8.8.0+19993+47c8ef84 | |
fuse-overlayfs-debugsource | 1.11-1.module+el8.8.0+19993+47c8ef84 | |
libslirp | 4.4.0-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
libslirp-debuginfo | 4.4.0-1.module+el8.8.0+19993+47c8ef84 | |
libslirp-debugsource | 4.4.0-1.module+el8.8.0+19993+47c8ef84 | |
libslirp-devel | 4.4.0-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
netavark | 1.5.1-2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
oci-seccomp-bpf-hook | 1.2.8-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
oci-seccomp-bpf-hook-debuginfo | 1.2.8-1.module+el8.8.0+19993+47c8ef84 | |
oci-seccomp-bpf-hook-debugsource | 1.2.8-1.module+el8.8.0+19993+47c8ef84 | |
podman | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
podman-catatonit | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
podman-catatonit-debuginfo | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | |
podman-debuginfo | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | |
podman-debugsource | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | |
podman-docker | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
podman-gvproxy | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
podman-gvproxy-debuginfo | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | |
podman-plugins | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
podman-plugins-debuginfo | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | |
podman-remote | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
podman-remote-debuginfo | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | |
podman-tests | 4.4.1-16.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
python3-criu | 3.15-4.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
python3-podman | 4.4.1-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
runc | 1.1.4-1.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
runc-debuginfo | 1.1.4-1.module+el8.8.0+19993+47c8ef84 | |
runc-debugsource | 1.1.4-1.module+el8.8.0+19993+47c8ef84 | |
skopeo | 1.11.2-0.2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
skopeo-tests | 1.11.2-0.2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
slirp4netns | 1.2.0-2.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
slirp4netns-debuginfo | 1.2.0-2.module+el8.8.0+19993+47c8ef84 | |
slirp4netns-debugsource | 1.2.0-2.module+el8.8.0+19993+47c8ef84 | |
thunderbird | 115.3.1-1.el8_8 | [RHSA-2023:5428](https://access.redhat.com/errata/RHSA-2023:5428) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3600](https://access.redhat.com/security/cve/CVE-2023-3600), [CVE-2023-5169](https://access.redhat.com/security/cve/CVE-2023-5169), [CVE-2023-5171](https://access.redhat.com/security/cve/CVE-2023-5171), [CVE-2023-5176](https://access.redhat.com/security/cve/CVE-2023-5176), [CVE-2023-5217](https://access.redhat.com/security/cve/CVE-2023-5217))
thunderbird-debuginfo | 115.3.1-1.el8_8 | |
thunderbird-debugsource | 115.3.1-1.el8_8 | |
toolbox | 0.0.99.3-7.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
toolbox-debuginfo | 0.0.99.3-7.module+el8.8.0+19993+47c8ef84 | |
toolbox-debugsource | 0.0.99.3-7.module+el8.8.0+19993+47c8ef84 | |
toolbox-tests | 0.0.99.3-7.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>
udica | 0.2.6-20.module+el8.8.0+19993+47c8ef84 | [RHBA-2023:5431](https://access.redhat.com/errata/RHBA-2023:5431) | <div class="adv_b">Bug Fix Advisory</div>

