## 2024-06-18

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aardvark-dns | 1.10.0-1.module+el8.10.0+21962+8143777b | |
buildah | 1.33.7-2.module+el8.10.0+21962+8143777b | |
buildah-debuginfo | 1.33.7-2.module+el8.10.0+21962+8143777b | |
buildah-debugsource | 1.33.7-2.module+el8.10.0+21962+8143777b | |
buildah-tests | 1.33.7-2.module+el8.10.0+21962+8143777b | |
buildah-tests-debuginfo | 1.33.7-2.module+el8.10.0+21962+8143777b | |
cockpit-podman | 84.1-1.module+el8.10.0+21962+8143777b | |
conmon | 2.1.10-1.module+el8.10.0+21962+8143777b | |
conmon-debuginfo | 2.1.10-1.module+el8.10.0+21962+8143777b | |
conmon-debugsource | 2.1.10-1.module+el8.10.0+21962+8143777b | |
container-selinux | 2.229.0-2.module+el8.10.0+21962+8143777b | |
containernetworking-plugins | 1.4.0-2.module+el8.10.0+21962+8143777b | |
containernetworking-plugins-debuginfo | 1.4.0-2.module+el8.10.0+21962+8143777b | |
containernetworking-plugins-debugsource | 1.4.0-2.module+el8.10.0+21962+8143777b | |
containers-common | 1-81.module+el8.10.0+21962+8143777b | |
crit | 3.18-5.module+el8.10.0+21962+8143777b | |
criu | 3.18-5.module+el8.10.0+21962+8143777b | |
criu-debuginfo | 3.18-5.module+el8.10.0+21962+8143777b | |
criu-debugsource | 3.18-5.module+el8.10.0+21962+8143777b | |
criu-devel | 3.18-5.module+el8.10.0+21962+8143777b | |
criu-libs | 3.18-5.module+el8.10.0+21962+8143777b | |
criu-libs-debuginfo | 3.18-5.module+el8.10.0+21962+8143777b | |
crun | 1.14.3-2.module+el8.10.0+21962+8143777b | |
crun-debuginfo | 1.14.3-2.module+el8.10.0+21962+8143777b | |
crun-debugsource | 1.14.3-2.module+el8.10.0+21962+8143777b | |
firefox | 115.12.0-1.el8_10 | [RHSA-2024:3954](https://access.redhat.com/errata/RHSA-2024:3954) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5688](https://access.redhat.com/security/cve/CVE-2024-5688), [CVE-2024-5690](https://access.redhat.com/security/cve/CVE-2024-5690), [CVE-2024-5691](https://access.redhat.com/security/cve/CVE-2024-5691), [CVE-2024-5693](https://access.redhat.com/security/cve/CVE-2024-5693), [CVE-2024-5696](https://access.redhat.com/security/cve/CVE-2024-5696), [CVE-2024-5700](https://access.redhat.com/security/cve/CVE-2024-5700), [CVE-2024-5702](https://access.redhat.com/security/cve/CVE-2024-5702))
firefox-debuginfo | 115.12.0-1.el8_10 | |
firefox-debugsource | 115.12.0-1.el8_10 | |
flatpak | 1.12.9-1.el8_10 | [RHSA-2024:3961](https://access.redhat.com/errata/RHSA-2024:3961) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32462](https://access.redhat.com/security/cve/CVE-2024-32462))
flatpak-debuginfo | 1.12.9-1.el8_10 | |
flatpak-debugsource | 1.12.9-1.el8_10 | |
flatpak-libs | 1.12.9-1.el8_10 | [RHSA-2024:3961](https://access.redhat.com/errata/RHSA-2024:3961) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32462](https://access.redhat.com/security/cve/CVE-2024-32462))
flatpak-libs-debuginfo | 1.12.9-1.el8_10 | |
flatpak-selinux | 1.12.9-1.el8_10 | [RHSA-2024:3961](https://access.redhat.com/errata/RHSA-2024:3961) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32462](https://access.redhat.com/security/cve/CVE-2024-32462))
flatpak-session-helper | 1.12.9-1.el8_10 | [RHSA-2024:3961](https://access.redhat.com/errata/RHSA-2024:3961) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32462](https://access.redhat.com/security/cve/CVE-2024-32462))
flatpak-session-helper-debuginfo | 1.12.9-1.el8_10 | |
flatpak-tests-debuginfo | 1.12.9-1.el8_10 | |
fuse-overlayfs | 1.13-1.module+el8.10.0+21962+8143777b | |
fuse-overlayfs-debuginfo | 1.13-1.module+el8.10.0+21962+8143777b | |
fuse-overlayfs-debugsource | 1.13-1.module+el8.10.0+21962+8143777b | |
libslirp | 4.4.0-2.module+el8.10.0+21962+8143777b | |
libslirp-debuginfo | 4.4.0-2.module+el8.10.0+21962+8143777b | |
libslirp-debugsource | 4.4.0-2.module+el8.10.0+21962+8143777b | |
libslirp-devel | 4.4.0-2.module+el8.10.0+21962+8143777b | |
netavark | 1.10.3-1.module+el8.10.0+21962+8143777b | |
oci-seccomp-bpf-hook | 1.2.10-1.module+el8.10.0+21962+8143777b | |
oci-seccomp-bpf-hook-debuginfo | 1.2.10-1.module+el8.10.0+21962+8143777b | |
oci-seccomp-bpf-hook-debugsource | 1.2.10-1.module+el8.10.0+21962+8143777b | |
podman | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-catatonit | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-catatonit-debuginfo | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-debuginfo | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-debugsource | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-docker | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-gvproxy | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-gvproxy-debuginfo | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-plugins | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-plugins-debuginfo | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-remote | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-remote-debuginfo | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-tests | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
python3-criu | 3.18-5.module+el8.10.0+21962+8143777b | |
python3-podman | 4.9.0-1.module+el8.10.0+21962+8143777b | |
runc | 1.1.12-1.module+el8.10.0+21974+acd2159c | |
runc-debuginfo | 1.1.12-1.module+el8.10.0+21974+acd2159c | |
runc-debugsource | 1.1.12-1.module+el8.10.0+21974+acd2159c | |
skopeo | 1.14.3-2.module+el8.10.0+21962+8143777b | |
skopeo-tests | 1.14.3-2.module+el8.10.0+21962+8143777b | |
slirp4netns | 1.2.3-1.module+el8.10.0+21962+8143777b | |
slirp4netns-debuginfo | 1.2.3-1.module+el8.10.0+21962+8143777b | |
slirp4netns-debugsource | 1.2.3-1.module+el8.10.0+21962+8143777b | |
toolbox | 0.0.99.5-2.module+el8.10.0+21962+8143777b | |
toolbox-debuginfo | 0.0.99.5-2.module+el8.10.0+21962+8143777b | |
toolbox-debugsource | 0.0.99.5-2.module+el8.10.0+21962+8143777b | |
toolbox-tests | 0.0.99.5-2.module+el8.10.0+21962+8143777b | |
udica | 0.2.6-21.module+el8.10.0+21962+8143777b | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
flatpak-debuginfo | 1.12.9-1.el8_10 | |
flatpak-debugsource | 1.12.9-1.el8_10 | |
flatpak-devel | 1.12.9-1.el8_10 | [RHSA-2024:3961](https://access.redhat.com/errata/RHSA-2024:3961) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32462](https://access.redhat.com/security/cve/CVE-2024-32462))
flatpak-libs-debuginfo | 1.12.9-1.el8_10 | |
flatpak-session-helper-debuginfo | 1.12.9-1.el8_10 | |
flatpak-tests-debuginfo | 1.12.9-1.el8_10 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aardvark-dns | 1.10.0-1.module+el8.10.0+21962+8143777b | |
buildah | 1.33.7-2.module+el8.10.0+21962+8143777b | |
buildah-debuginfo | 1.33.7-2.module+el8.10.0+21962+8143777b | |
buildah-debugsource | 1.33.7-2.module+el8.10.0+21962+8143777b | |
buildah-tests | 1.33.7-2.module+el8.10.0+21962+8143777b | |
buildah-tests-debuginfo | 1.33.7-2.module+el8.10.0+21962+8143777b | |
cockpit-podman | 84.1-1.module+el8.10.0+21962+8143777b | |
conmon | 2.1.10-1.module+el8.10.0+21962+8143777b | |
conmon-debuginfo | 2.1.10-1.module+el8.10.0+21962+8143777b | |
conmon-debugsource | 2.1.10-1.module+el8.10.0+21962+8143777b | |
container-selinux | 2.229.0-2.module+el8.10.0+21962+8143777b | |
containernetworking-plugins | 1.4.0-2.module+el8.10.0+21962+8143777b | |
containernetworking-plugins-debuginfo | 1.4.0-2.module+el8.10.0+21962+8143777b | |
containernetworking-plugins-debugsource | 1.4.0-2.module+el8.10.0+21962+8143777b | |
containers-common | 1-81.module+el8.10.0+21962+8143777b | |
crit | 3.18-5.module+el8.10.0+21962+8143777b | |
criu | 3.18-5.module+el8.10.0+21962+8143777b | |
criu-debuginfo | 3.18-5.module+el8.10.0+21962+8143777b | |
criu-debugsource | 3.18-5.module+el8.10.0+21962+8143777b | |
criu-devel | 3.18-5.module+el8.10.0+21962+8143777b | |
criu-libs | 3.18-5.module+el8.10.0+21962+8143777b | |
criu-libs-debuginfo | 3.18-5.module+el8.10.0+21962+8143777b | |
crun | 1.14.3-2.module+el8.10.0+21962+8143777b | |
crun-debuginfo | 1.14.3-2.module+el8.10.0+21962+8143777b | |
crun-debugsource | 1.14.3-2.module+el8.10.0+21962+8143777b | |
firefox | 115.12.0-1.el8_10 | [RHSA-2024:3954](https://access.redhat.com/errata/RHSA-2024:3954) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5688](https://access.redhat.com/security/cve/CVE-2024-5688), [CVE-2024-5690](https://access.redhat.com/security/cve/CVE-2024-5690), [CVE-2024-5691](https://access.redhat.com/security/cve/CVE-2024-5691), [CVE-2024-5693](https://access.redhat.com/security/cve/CVE-2024-5693), [CVE-2024-5696](https://access.redhat.com/security/cve/CVE-2024-5696), [CVE-2024-5700](https://access.redhat.com/security/cve/CVE-2024-5700), [CVE-2024-5702](https://access.redhat.com/security/cve/CVE-2024-5702))
firefox-debuginfo | 115.12.0-1.el8_10 | |
firefox-debugsource | 115.12.0-1.el8_10 | |
flatpak | 1.12.9-1.el8_10 | [RHSA-2024:3961](https://access.redhat.com/errata/RHSA-2024:3961) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32462](https://access.redhat.com/security/cve/CVE-2024-32462))
flatpak-debuginfo | 1.12.9-1.el8_10 | |
flatpak-debugsource | 1.12.9-1.el8_10 | |
flatpak-libs | 1.12.9-1.el8_10 | [RHSA-2024:3961](https://access.redhat.com/errata/RHSA-2024:3961) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32462](https://access.redhat.com/security/cve/CVE-2024-32462))
flatpak-libs-debuginfo | 1.12.9-1.el8_10 | |
flatpak-selinux | 1.12.9-1.el8_10 | [RHSA-2024:3961](https://access.redhat.com/errata/RHSA-2024:3961) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32462](https://access.redhat.com/security/cve/CVE-2024-32462))
flatpak-session-helper | 1.12.9-1.el8_10 | [RHSA-2024:3961](https://access.redhat.com/errata/RHSA-2024:3961) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32462](https://access.redhat.com/security/cve/CVE-2024-32462))
flatpak-session-helper-debuginfo | 1.12.9-1.el8_10 | |
flatpak-tests-debuginfo | 1.12.9-1.el8_10 | |
fuse-overlayfs | 1.13-1.module+el8.10.0+21962+8143777b | |
fuse-overlayfs-debuginfo | 1.13-1.module+el8.10.0+21962+8143777b | |
fuse-overlayfs-debugsource | 1.13-1.module+el8.10.0+21962+8143777b | |
libslirp | 4.4.0-2.module+el8.10.0+21962+8143777b | |
libslirp-debuginfo | 4.4.0-2.module+el8.10.0+21962+8143777b | |
libslirp-debugsource | 4.4.0-2.module+el8.10.0+21962+8143777b | |
libslirp-devel | 4.4.0-2.module+el8.10.0+21962+8143777b | |
netavark | 1.10.3-1.module+el8.10.0+21962+8143777b | |
oci-seccomp-bpf-hook | 1.2.10-1.module+el8.10.0+21962+8143777b | |
oci-seccomp-bpf-hook-debuginfo | 1.2.10-1.module+el8.10.0+21962+8143777b | |
oci-seccomp-bpf-hook-debugsource | 1.2.10-1.module+el8.10.0+21962+8143777b | |
podman | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-catatonit | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-catatonit-debuginfo | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-debuginfo | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-debugsource | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-docker | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-gvproxy | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-gvproxy-debuginfo | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-plugins | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-plugins-debuginfo | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-remote | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-remote-debuginfo | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
podman-tests | 4.9.4-3.module+el8.10.0+21974+acd2159c | |
python3-criu | 3.18-5.module+el8.10.0+21962+8143777b | |
python3-podman | 4.9.0-1.module+el8.10.0+21962+8143777b | |
runc | 1.1.12-1.module+el8.10.0+21974+acd2159c | |
runc-debuginfo | 1.1.12-1.module+el8.10.0+21974+acd2159c | |
runc-debugsource | 1.1.12-1.module+el8.10.0+21974+acd2159c | |
skopeo | 1.14.3-2.module+el8.10.0+21962+8143777b | |
skopeo-tests | 1.14.3-2.module+el8.10.0+21962+8143777b | |
slirp4netns | 1.2.3-1.module+el8.10.0+21962+8143777b | |
slirp4netns-debuginfo | 1.2.3-1.module+el8.10.0+21962+8143777b | |
slirp4netns-debugsource | 1.2.3-1.module+el8.10.0+21962+8143777b | |
toolbox | 0.0.99.5-2.module+el8.10.0+21962+8143777b | |
toolbox-debuginfo | 0.0.99.5-2.module+el8.10.0+21962+8143777b | |
toolbox-debugsource | 0.0.99.5-2.module+el8.10.0+21962+8143777b | |
toolbox-tests | 0.0.99.5-2.module+el8.10.0+21962+8143777b | |
udica | 0.2.6-21.module+el8.10.0+21962+8143777b | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
flatpak-debuginfo | 1.12.9-1.el8_10 | |
flatpak-debugsource | 1.12.9-1.el8_10 | |
flatpak-devel | 1.12.9-1.el8_10 | [RHSA-2024:3961](https://access.redhat.com/errata/RHSA-2024:3961) | <div class="adv_s">Security Advisory</div> ([CVE-2024-32462](https://access.redhat.com/security/cve/CVE-2024-32462))
flatpak-libs-debuginfo | 1.12.9-1.el8_10 | |
flatpak-session-helper-debuginfo | 1.12.9-1.el8_10 | |
flatpak-tests-debuginfo | 1.12.9-1.el8_10 | |

