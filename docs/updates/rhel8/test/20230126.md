## 2023-01-26

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.10-1.rh8.cern | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
go-toolset | 1.18.9-1.module+el8.7.0+17845+708ebe87 | |
golang | 1.18.9-1.module+el8.7.0+17640+84246675 | |
golang-bin | 1.18.9-1.module+el8.7.0+17640+84246675 | |
golang-docs | 1.18.9-1.module+el8.7.0+17640+84246675 | |
golang-misc | 1.18.9-1.module+el8.7.0+17640+84246675 | |
golang-race | 1.18.9-1.module+el8.7.0+17640+84246675 | |
golang-src | 1.18.9-1.module+el8.7.0+17640+84246675 | |
golang-tests | 1.18.9-1.module+el8.7.0+17640+84246675 | |
thunderbird | 102.7.1-1.el8_7 | |
thunderbird-debuginfo | 102.7.1-1.el8_7 | |
thunderbird-debugsource | 102.7.1-1.el8_7 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.10-1.rh8.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
go-toolset | 1.18.9-1.module+el8.7.0+17845+708ebe87 | |
golang | 1.18.9-1.module+el8.7.0+17640+84246675 | |
golang-bin | 1.18.9-1.module+el8.7.0+17640+84246675 | |
golang-docs | 1.18.9-1.module+el8.7.0+17640+84246675 | |
golang-misc | 1.18.9-1.module+el8.7.0+17640+84246675 | |
golang-src | 1.18.9-1.module+el8.7.0+17640+84246675 | |
golang-tests | 1.18.9-1.module+el8.7.0+17640+84246675 | |
thunderbird | 102.7.1-1.el8_7 | |
thunderbird-debuginfo | 102.7.1-1.el8_7 | |
thunderbird-debugsource | 102.7.1-1.el8_7 | |

