## 2024-05-29

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
polkit | 0.115-15.el8_10.2 | |
polkit-debuginfo | 0.115-15.el8_10.2 | |
polkit-debugsource | 0.115-15.el8_10.2 | |
polkit-devel | 0.115-15.el8_10.2 | |
polkit-docs | 0.115-15.el8_10.2 | |
polkit-libs | 0.115-15.el8_10.2 | |
polkit-libs-debuginfo | 0.115-15.el8_10.2 | |
tuned | 2.22.1-4.el8_10.1 | |
tuned-profiles-atomic | 2.22.1-4.el8_10.1 | |
tuned-profiles-compat | 2.22.1-4.el8_10.1 | |
tuned-profiles-cpu-partitioning | 2.22.1-4.el8_10.1 | |
tuned-profiles-mssql | 2.22.1-4.el8_10.1 | |
tuned-profiles-oracle | 2.22.1-4.el8_10.1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tuned-gtk | 2.22.1-4.el8_10.1 | |
tuned-profiles-postgresql | 2.22.1-4.el8_10.1 | |
tuned-utils | 2.22.1-4.el8_10.1 | |
tuned-utils-systemtap | 2.22.1-4.el8_10.1 | |

### rt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tuned-profiles-realtime | 2.22.1-4.el8_10.1 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
polkit | 0.115-15.el8_10.2 | |
polkit-debuginfo | 0.115-15.el8_10.2 | |
polkit-debugsource | 0.115-15.el8_10.2 | |
polkit-devel | 0.115-15.el8_10.2 | |
polkit-docs | 0.115-15.el8_10.2 | |
polkit-libs | 0.115-15.el8_10.2 | |
polkit-libs-debuginfo | 0.115-15.el8_10.2 | |
tuned | 2.22.1-4.el8_10.1 | |
tuned-profiles-atomic | 2.22.1-4.el8_10.1 | |
tuned-profiles-compat | 2.22.1-4.el8_10.1 | |
tuned-profiles-cpu-partitioning | 2.22.1-4.el8_10.1 | |
tuned-profiles-mssql | 2.22.1-4.el8_10.1 | |
tuned-profiles-oracle | 2.22.1-4.el8_10.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tuned-gtk | 2.22.1-4.el8_10.1 | |
tuned-profiles-postgresql | 2.22.1-4.el8_10.1 | |
tuned-utils | 2.22.1-4.el8_10.1 | |
tuned-utils-systemtap | 2.22.1-4.el8_10.1 | |

