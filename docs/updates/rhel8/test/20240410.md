## 2024-04-10

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.7.0-1.el8 | |
sos-audit | 4.7.0-1.el8 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.29-1.el8_9 | |
aspnetcore-runtime-7.0 | 7.0.18-1.el8_9 | |
aspnetcore-runtime-8.0 | 8.0.4-1.el8_9 | |
aspnetcore-runtime-dbg-8.0 | 8.0.4-1.el8_9 | |
aspnetcore-targeting-pack-6.0 | 6.0.29-1.el8_9 | |
aspnetcore-targeting-pack-7.0 | 7.0.18-1.el8_9 | |
aspnetcore-targeting-pack-8.0 | 8.0.4-1.el8_9 | |
dotnet | 8.0.104-1.el8_9 | |
dotnet-apphost-pack-6.0 | 6.0.29-1.el8_9 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.29-1.el8_9 | |
dotnet-apphost-pack-7.0 | 7.0.18-1.el8_9 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.18-1.el8_9 | |
dotnet-apphost-pack-8.0 | 8.0.4-1.el8_9 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.4-1.el8_9 | |
dotnet-host | 8.0.4-1.el8_9 | |
dotnet-host-debuginfo | 8.0.4-1.el8_9 | |
dotnet-hostfxr-6.0 | 6.0.29-1.el8_9 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.29-1.el8_9 | |
dotnet-hostfxr-7.0 | 7.0.18-1.el8_9 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.18-1.el8_9 | |
dotnet-hostfxr-8.0 | 8.0.4-1.el8_9 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.4-1.el8_9 | |
dotnet-runtime-6.0 | 6.0.29-1.el8_9 | |
dotnet-runtime-6.0-debuginfo | 6.0.29-1.el8_9 | |
dotnet-runtime-7.0 | 7.0.18-1.el8_9 | |
dotnet-runtime-7.0-debuginfo | 7.0.18-1.el8_9 | |
dotnet-runtime-8.0 | 8.0.4-1.el8_9 | |
dotnet-runtime-8.0-debuginfo | 8.0.4-1.el8_9 | |
dotnet-runtime-dbg-8.0 | 8.0.4-1.el8_9 | |
dotnet-sdk-6.0 | 6.0.129-1.el8_9 | |
dotnet-sdk-6.0-debuginfo | 6.0.129-1.el8_9 | |
dotnet-sdk-7.0 | 7.0.118-1.el8_9 | |
dotnet-sdk-7.0-debuginfo | 7.0.118-1.el8_9 | |
dotnet-sdk-8.0 | 8.0.104-1.el8_9 | |
dotnet-sdk-8.0-debuginfo | 8.0.104-1.el8_9 | |
dotnet-sdk-dbg-8.0 | 8.0.104-1.el8_9 | |
dotnet-targeting-pack-6.0 | 6.0.29-1.el8_9 | |
dotnet-targeting-pack-7.0 | 7.0.18-1.el8_9 | |
dotnet-targeting-pack-8.0 | 8.0.4-1.el8_9 | |
dotnet-templates-6.0 | 6.0.129-1.el8_9 | |
dotnet-templates-7.0 | 7.0.118-1.el8_9 | |
dotnet-templates-8.0 | 8.0.104-1.el8_9 | |
dotnet6.0-debuginfo | 6.0.129-1.el8_9 | |
dotnet6.0-debugsource | 6.0.129-1.el8_9 | |
dotnet7.0-debuginfo | 7.0.118-1.el8_9 | |
dotnet7.0-debugsource | 7.0.118-1.el8_9 | |
dotnet8.0-debuginfo | 8.0.104-1.el8_9 | |
dotnet8.0-debugsource | 8.0.104-1.el8_9 | |
netstandard-targeting-pack-2.1 | 8.0.104-1.el8_9 | |
rear | 2.6-11.el8_9 | [RHSA-2024:1719](https://access.redhat.com/errata/RHSA-2024:1719) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23301](https://access.redhat.com/security/cve/CVE-2024-23301))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.29-1.el8_9 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.18-1.el8_9 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.4-1.el8_9 | |
dotnet-host-debuginfo | 8.0.4-1.el8_9 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.29-1.el8_9 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.18-1.el8_9 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.4-1.el8_9 | |
dotnet-runtime-6.0-debuginfo | 6.0.29-1.el8_9 | |
dotnet-runtime-7.0-debuginfo | 7.0.18-1.el8_9 | |
dotnet-runtime-8.0-debuginfo | 8.0.4-1.el8_9 | |
dotnet-sdk-6.0-debuginfo | 6.0.129-1.el8_9 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.129-1.el8_9 | |
dotnet-sdk-7.0-debuginfo | 7.0.118-1.el8_9 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.118-1.el8_9 | |
dotnet-sdk-8.0-debuginfo | 8.0.104-1.el8_9 | |
dotnet-sdk-8.0-source-built-artifacts | 8.0.104-1.el8_9 | |
dotnet6.0-debuginfo | 6.0.129-1.el8_9 | |
dotnet6.0-debugsource | 6.0.129-1.el8_9 | |
dotnet7.0-debuginfo | 7.0.118-1.el8_9 | |
dotnet7.0-debugsource | 7.0.118-1.el8_9 | |
dotnet8.0-debuginfo | 8.0.104-1.el8_9 | |
dotnet8.0-debugsource | 8.0.104-1.el8_9 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.7.0-1.el8 | |
sos-audit | 4.7.0-1.el8 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.29-1.el8_9 | |
aspnetcore-runtime-7.0 | 7.0.18-1.el8_9 | |
aspnetcore-runtime-8.0 | 8.0.4-1.el8_9 | |
aspnetcore-runtime-dbg-8.0 | 8.0.4-1.el8_9 | |
aspnetcore-targeting-pack-6.0 | 6.0.29-1.el8_9 | |
aspnetcore-targeting-pack-7.0 | 7.0.18-1.el8_9 | |
aspnetcore-targeting-pack-8.0 | 8.0.4-1.el8_9 | |
dotnet | 8.0.104-1.el8_9 | |
dotnet-apphost-pack-6.0 | 6.0.29-1.el8_9 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.29-1.el8_9 | |
dotnet-apphost-pack-7.0 | 7.0.18-1.el8_9 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.18-1.el8_9 | |
dotnet-apphost-pack-8.0 | 8.0.4-1.el8_9 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.4-1.el8_9 | |
dotnet-host | 8.0.4-1.el8_9 | |
dotnet-host-debuginfo | 8.0.4-1.el8_9 | |
dotnet-hostfxr-6.0 | 6.0.29-1.el8_9 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.29-1.el8_9 | |
dotnet-hostfxr-7.0 | 7.0.18-1.el8_9 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.18-1.el8_9 | |
dotnet-hostfxr-8.0 | 8.0.4-1.el8_9 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.4-1.el8_9 | |
dotnet-runtime-6.0 | 6.0.29-1.el8_9 | |
dotnet-runtime-6.0-debuginfo | 6.0.29-1.el8_9 | |
dotnet-runtime-7.0 | 7.0.18-1.el8_9 | |
dotnet-runtime-7.0-debuginfo | 7.0.18-1.el8_9 | |
dotnet-runtime-8.0 | 8.0.4-1.el8_9 | |
dotnet-runtime-8.0-debuginfo | 8.0.4-1.el8_9 | |
dotnet-runtime-dbg-8.0 | 8.0.4-1.el8_9 | |
dotnet-sdk-6.0 | 6.0.129-1.el8_9 | |
dotnet-sdk-6.0-debuginfo | 6.0.129-1.el8_9 | |
dotnet-sdk-7.0 | 7.0.118-1.el8_9 | |
dotnet-sdk-7.0-debuginfo | 7.0.118-1.el8_9 | |
dotnet-sdk-8.0 | 8.0.104-1.el8_9 | |
dotnet-sdk-8.0-debuginfo | 8.0.104-1.el8_9 | |
dotnet-sdk-dbg-8.0 | 8.0.104-1.el8_9 | |
dotnet-targeting-pack-6.0 | 6.0.29-1.el8_9 | |
dotnet-targeting-pack-7.0 | 7.0.18-1.el8_9 | |
dotnet-targeting-pack-8.0 | 8.0.4-1.el8_9 | |
dotnet-templates-6.0 | 6.0.129-1.el8_9 | |
dotnet-templates-7.0 | 7.0.118-1.el8_9 | |
dotnet-templates-8.0 | 8.0.104-1.el8_9 | |
dotnet6.0-debuginfo | 6.0.129-1.el8_9 | |
dotnet6.0-debugsource | 6.0.129-1.el8_9 | |
dotnet7.0-debuginfo | 7.0.118-1.el8_9 | |
dotnet7.0-debugsource | 7.0.118-1.el8_9 | |
dotnet8.0-debuginfo | 8.0.104-1.el8_9 | |
dotnet8.0-debugsource | 8.0.104-1.el8_9 | |
netstandard-targeting-pack-2.1 | 8.0.104-1.el8_9 | |
rear | 2.6-11.el8_9 | [RHSA-2024:1719](https://access.redhat.com/errata/RHSA-2024:1719) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23301](https://access.redhat.com/security/cve/CVE-2024-23301))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.29-1.el8_9 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.18-1.el8_9 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.4-1.el8_9 | |
dotnet-host-debuginfo | 8.0.4-1.el8_9 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.29-1.el8_9 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.18-1.el8_9 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.4-1.el8_9 | |
dotnet-runtime-6.0-debuginfo | 6.0.29-1.el8_9 | |
dotnet-runtime-7.0-debuginfo | 7.0.18-1.el8_9 | |
dotnet-runtime-8.0-debuginfo | 8.0.4-1.el8_9 | |
dotnet-sdk-6.0-debuginfo | 6.0.129-1.el8_9 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.129-1.el8_9 | |
dotnet-sdk-7.0-debuginfo | 7.0.118-1.el8_9 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.118-1.el8_9 | |
dotnet-sdk-8.0-debuginfo | 8.0.104-1.el8_9 | |
dotnet-sdk-8.0-source-built-artifacts | 8.0.104-1.el8_9 | |
dotnet6.0-debuginfo | 6.0.129-1.el8_9 | |
dotnet6.0-debugsource | 6.0.129-1.el8_9 | |
dotnet7.0-debuginfo | 7.0.118-1.el8_9 | |
dotnet7.0-debugsource | 7.0.118-1.el8_9 | |
dotnet8.0-debuginfo | 8.0.104-1.el8_9 | |
dotnet8.0-debugsource | 8.0.104-1.el8_9 | |

