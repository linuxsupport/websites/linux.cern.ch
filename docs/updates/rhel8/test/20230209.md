## 2023-02-09

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.4-1.rh8.cern | |
pyphonebook | 2.1.5-1.rh8.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-redhat-oracleasm | 2.0.8-15.1.el8_7 | |
kmod-redhat-oracleasm-debugsource | 2.0.8-15.1.el8_7 | |
kmod-redhat-oracleasm-kernel_4_18_0_425_10_1-debuginfo | 2.0.8-15.1.el8_7 | |
kmod-redhat-oracleasm-kernel_4_18_0_425_3_1-debuginfo | 2.0.8-15.1.el8_7 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tigervnc | 1.12.0-9.el8_7.1 | |
tigervnc-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-debugsource | 1.12.0-9.el8_7.1 | |
tigervnc-icons | 1.12.0-9.el8_7.1 | |
tigervnc-license | 1.12.0-9.el8_7.1 | |
tigervnc-selinux | 1.12.0-9.el8_7.1 | |
tigervnc-server | 1.12.0-9.el8_7.1 | |
tigervnc-server-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-module | 1.12.0-9.el8_7.1 | |
tigervnc-server-module-debuginfo | 1.12.0-9.el8_7.1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.4-1.rh8.cern | |
pyphonebook | 2.1.5-1.rh8.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tigervnc | 1.12.0-9.el8_7.1 | |
tigervnc-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-debugsource | 1.12.0-9.el8_7.1 | |
tigervnc-icons | 1.12.0-9.el8_7.1 | |
tigervnc-license | 1.12.0-9.el8_7.1 | |
tigervnc-selinux | 1.12.0-9.el8_7.1 | |
tigervnc-server | 1.12.0-9.el8_7.1 | |
tigervnc-server-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-module | 1.12.0-9.el8_7.1 | |
tigervnc-server-module-debuginfo | 1.12.0-9.el8_7.1 | |

