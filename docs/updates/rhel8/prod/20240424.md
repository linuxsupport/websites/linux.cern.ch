## 2024-04-24

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-debuginfo | 9.11.36-11.el8_9.1 | |
bind-debugsource | 9.11.36-11.el8_9.1 | |
bind-export-devel | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-export-libs | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-export-libs-debuginfo | 9.11.36-11.el8_9.1 | |
bind-libs-debuginfo | 9.11.36-11.el8_9.1 | |
bind-libs-lite-debuginfo | 9.11.36-11.el8_9.1 | |
bind-pkcs11-debuginfo | 9.11.36-11.el8_9.1 | |
bind-pkcs11-libs-debuginfo | 9.11.36-11.el8_9.1 | |
bind-pkcs11-utils-debuginfo | 9.11.36-11.el8_9.1 | |
bind-sdb-debuginfo | 9.11.36-11.el8_9.1 | |
bind-utils-debuginfo | 9.11.36-11.el8_9.1 | |
dhcp-client | 4.3.6-49.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
dhcp-client-debuginfo | 4.3.6-49.el8_9.1 | |
dhcp-common | 4.3.6-49.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
dhcp-debuginfo | 4.3.6-49.el8_9.1 | |
dhcp-debugsource | 4.3.6-49.el8_9.1 | |
dhcp-libs | 4.3.6-49.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
dhcp-libs-debuginfo | 4.3.6-49.el8_9.1 | |
dhcp-relay | 4.3.6-49.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
dhcp-relay-debuginfo | 4.3.6-49.el8_9.1 | |
dhcp-server | 4.3.6-49.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
dhcp-server-debuginfo | 4.3.6-49.el8_9.1 | |
gnutls | 3.6.16-8.el8_9.3 | [RHSA-2024:1784](https://access.redhat.com/errata/RHSA-2024:1784) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28834](https://access.redhat.com/security/cve/CVE-2024-28834))
gnutls-c++-debuginfo | 3.6.16-8.el8_9.3 | |
gnutls-dane-debuginfo | 3.6.16-8.el8_9.3 | |
gnutls-debuginfo | 3.6.16-8.el8_9.3 | |
gnutls-debugsource | 3.6.16-8.el8_9.3 | |
gnutls-utils-debuginfo | 3.6.16-8.el8_9.3 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-chroot | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-debuginfo | 9.11.36-11.el8_9.1 | |
bind-debugsource | 9.11.36-11.el8_9.1 | |
bind-devel | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-dyndb-ldap | 11.6-5.module+el8.9.0+21623+25375490 | [RHBA-2024:1798](https://access.redhat.com/errata/RHBA-2024:1798) | <div class="adv_b">Bug Fix Advisory</div>
bind-dyndb-ldap-debuginfo | 11.6-5.module+el8.9.0+21623+25375490 | |
bind-dyndb-ldap-debugsource | 11.6-5.module+el8.9.0+21623+25375490 | |
bind-export-libs-debuginfo | 9.11.36-11.el8_9.1 | |
bind-libs | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-libs-debuginfo | 9.11.36-11.el8_9.1 | |
bind-libs-lite | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-libs-lite-debuginfo | 9.11.36-11.el8_9.1 | |
bind-license | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-lite-devel | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-pkcs11 | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-pkcs11-debuginfo | 9.11.36-11.el8_9.1 | |
bind-pkcs11-devel | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-pkcs11-libs | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-pkcs11-libs-debuginfo | 9.11.36-11.el8_9.1 | |
bind-pkcs11-utils | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-pkcs11-utils-debuginfo | 9.11.36-11.el8_9.1 | |
bind-sdb | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-sdb-chroot | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-sdb-debuginfo | 9.11.36-11.el8_9.1 | |
bind-utils | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-utils-debuginfo | 9.11.36-11.el8_9.1 | |
bind9.16 | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-chroot | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-debuginfo | 9.16.23-0.16.el8_9.2 | |
bind9.16-debugsource | 9.16.23-0.16.el8_9.2 | |
bind9.16-dnssec-utils | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-dnssec-utils-debuginfo | 9.16.23-0.16.el8_9.2 | |
bind9.16-libs | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-libs-debuginfo | 9.16.23-0.16.el8_9.2 | |
bind9.16-license | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-utils | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-utils-debuginfo | 9.16.23-0.16.el8_9.2 | |
firefox | 115.9.1-2.el8_9 | [RHBA-2024:1757](https://access.redhat.com/errata/RHBA-2024:1757) | <div class="adv_b">Bug Fix Advisory</div>
firefox-debuginfo | 115.9.1-2.el8_9 | |
firefox-debugsource | 115.9.1-2.el8_9 | |
gnutls-c++ | 3.6.16-8.el8_9.3 | [RHSA-2024:1784](https://access.redhat.com/errata/RHSA-2024:1784) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28834](https://access.redhat.com/security/cve/CVE-2024-28834))
gnutls-c++-debuginfo | 3.6.16-8.el8_9.3 | |
gnutls-dane | 3.6.16-8.el8_9.3 | [RHSA-2024:1784](https://access.redhat.com/errata/RHSA-2024:1784) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28834](https://access.redhat.com/security/cve/CVE-2024-28834))
gnutls-dane-debuginfo | 3.6.16-8.el8_9.3 | |
gnutls-debuginfo | 3.6.16-8.el8_9.3 | |
gnutls-debugsource | 3.6.16-8.el8_9.3 | |
gnutls-devel | 3.6.16-8.el8_9.3 | [RHSA-2024:1784](https://access.redhat.com/errata/RHSA-2024:1784) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28834](https://access.redhat.com/security/cve/CVE-2024-28834))
gnutls-utils | 3.6.16-8.el8_9.3 | [RHSA-2024:1784](https://access.redhat.com/errata/RHSA-2024:1784) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28834](https://access.redhat.com/security/cve/CVE-2024-28834))
gnutls-utils-debuginfo | 3.6.16-8.el8_9.3 | |
mod_http2 | 1.15.7-8.module+el8.9.0+21652+2dd1200b.5 | [RHSA-2024:1786](https://access.redhat.com/errata/RHSA-2024:1786) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27316](https://access.redhat.com/security/cve/CVE-2024-27316))
mod_http2-debuginfo | 1.15.7-8.module+el8.9.0+21652+2dd1200b.5 | |
mod_http2-debugsource | 1.15.7-8.module+el8.9.0+21652+2dd1200b.5 | |
python3-bind | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
python3-bind9.16 | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
python3-unbound | 1.16.2-5.el8_9.6 | [RHSA-2024:1751](https://access.redhat.com/errata/RHSA-2024:1751) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1488](https://access.redhat.com/security/cve/CVE-2024-1488))
python3-unbound-debuginfo | 1.16.2-5.el8_9.6 | |
unbound | 1.16.2-5.el8_9.6 | [RHSA-2024:1751](https://access.redhat.com/errata/RHSA-2024:1751) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1488](https://access.redhat.com/security/cve/CVE-2024-1488))
unbound-debuginfo | 1.16.2-5.el8_9.6 | |
unbound-debugsource | 1.16.2-5.el8_9.6 | |
unbound-devel | 1.16.2-5.el8_9.6 | [RHSA-2024:1751](https://access.redhat.com/errata/RHSA-2024:1751) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1488](https://access.redhat.com/security/cve/CVE-2024-1488))
unbound-libs | 1.16.2-5.el8_9.6 | [RHSA-2024:1751](https://access.redhat.com/errata/RHSA-2024:1751) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1488](https://access.redhat.com/security/cve/CVE-2024-1488))
unbound-libs-debuginfo | 1.16.2-5.el8_9.6 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind9.16-debuginfo | 9.16.23-0.16.el8_9.2 | |
bind9.16-debugsource | 9.16.23-0.16.el8_9.2 | |
bind9.16-devel | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-dnssec-utils-debuginfo | 9.16.23-0.16.el8_9.2 | |
bind9.16-doc | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-libs-debuginfo | 9.16.23-0.16.el8_9.2 | |
bind9.16-utils-debuginfo | 9.16.23-0.16.el8_9.2 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-debuginfo | 9.11.36-11.el8_9.1 | |
bind-debugsource | 9.11.36-11.el8_9.1 | |
bind-export-devel | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-export-libs | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-export-libs-debuginfo | 9.11.36-11.el8_9.1 | |
bind-libs-debuginfo | 9.11.36-11.el8_9.1 | |
bind-libs-lite-debuginfo | 9.11.36-11.el8_9.1 | |
bind-pkcs11-debuginfo | 9.11.36-11.el8_9.1 | |
bind-pkcs11-libs-debuginfo | 9.11.36-11.el8_9.1 | |
bind-pkcs11-utils-debuginfo | 9.11.36-11.el8_9.1 | |
bind-sdb-debuginfo | 9.11.36-11.el8_9.1 | |
bind-utils-debuginfo | 9.11.36-11.el8_9.1 | |
dhcp-client | 4.3.6-49.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
dhcp-client-debuginfo | 4.3.6-49.el8_9.1 | |
dhcp-common | 4.3.6-49.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
dhcp-debuginfo | 4.3.6-49.el8_9.1 | |
dhcp-debugsource | 4.3.6-49.el8_9.1 | |
dhcp-libs | 4.3.6-49.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
dhcp-libs-debuginfo | 4.3.6-49.el8_9.1 | |
dhcp-relay | 4.3.6-49.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
dhcp-relay-debuginfo | 4.3.6-49.el8_9.1 | |
dhcp-server | 4.3.6-49.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
dhcp-server-debuginfo | 4.3.6-49.el8_9.1 | |
gnutls | 3.6.16-8.el8_9.3 | [RHSA-2024:1784](https://access.redhat.com/errata/RHSA-2024:1784) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28834](https://access.redhat.com/security/cve/CVE-2024-28834))
gnutls-c++-debuginfo | 3.6.16-8.el8_9.3 | |
gnutls-dane-debuginfo | 3.6.16-8.el8_9.3 | |
gnutls-debuginfo | 3.6.16-8.el8_9.3 | |
gnutls-debugsource | 3.6.16-8.el8_9.3 | |
gnutls-utils-debuginfo | 3.6.16-8.el8_9.3 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-chroot | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-debuginfo | 9.11.36-11.el8_9.1 | |
bind-debugsource | 9.11.36-11.el8_9.1 | |
bind-devel | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-dyndb-ldap | 11.6-5.module+el8.9.0+21623+25375490 | [RHBA-2024:1798](https://access.redhat.com/errata/RHBA-2024:1798) | <div class="adv_b">Bug Fix Advisory</div>
bind-dyndb-ldap-debuginfo | 11.6-5.module+el8.9.0+21623+25375490 | |
bind-dyndb-ldap-debugsource | 11.6-5.module+el8.9.0+21623+25375490 | |
bind-export-libs-debuginfo | 9.11.36-11.el8_9.1 | |
bind-libs | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-libs-debuginfo | 9.11.36-11.el8_9.1 | |
bind-libs-lite | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-libs-lite-debuginfo | 9.11.36-11.el8_9.1 | |
bind-license | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-lite-devel | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-pkcs11 | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-pkcs11-debuginfo | 9.11.36-11.el8_9.1 | |
bind-pkcs11-devel | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-pkcs11-libs | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-pkcs11-libs-debuginfo | 9.11.36-11.el8_9.1 | |
bind-pkcs11-utils | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-pkcs11-utils-debuginfo | 9.11.36-11.el8_9.1 | |
bind-sdb | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-sdb-chroot | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-sdb-debuginfo | 9.11.36-11.el8_9.1 | |
bind-utils | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
bind-utils-debuginfo | 9.11.36-11.el8_9.1 | |
bind9.16 | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-chroot | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-debuginfo | 9.16.23-0.16.el8_9.2 | |
bind9.16-debugsource | 9.16.23-0.16.el8_9.2 | |
bind9.16-dnssec-utils | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-dnssec-utils-debuginfo | 9.16.23-0.16.el8_9.2 | |
bind9.16-libs | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-libs-debuginfo | 9.16.23-0.16.el8_9.2 | |
bind9.16-license | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-utils | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-utils-debuginfo | 9.16.23-0.16.el8_9.2 | |
firefox | 115.9.1-2.el8_9 | [RHBA-2024:1757](https://access.redhat.com/errata/RHBA-2024:1757) | <div class="adv_b">Bug Fix Advisory</div>
firefox-debuginfo | 115.9.1-2.el8_9 | |
firefox-debugsource | 115.9.1-2.el8_9 | |
gnutls-c++ | 3.6.16-8.el8_9.3 | [RHSA-2024:1784](https://access.redhat.com/errata/RHSA-2024:1784) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28834](https://access.redhat.com/security/cve/CVE-2024-28834))
gnutls-c++-debuginfo | 3.6.16-8.el8_9.3 | |
gnutls-dane | 3.6.16-8.el8_9.3 | [RHSA-2024:1784](https://access.redhat.com/errata/RHSA-2024:1784) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28834](https://access.redhat.com/security/cve/CVE-2024-28834))
gnutls-dane-debuginfo | 3.6.16-8.el8_9.3 | |
gnutls-debuginfo | 3.6.16-8.el8_9.3 | |
gnutls-debugsource | 3.6.16-8.el8_9.3 | |
gnutls-devel | 3.6.16-8.el8_9.3 | [RHSA-2024:1784](https://access.redhat.com/errata/RHSA-2024:1784) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28834](https://access.redhat.com/security/cve/CVE-2024-28834))
gnutls-utils | 3.6.16-8.el8_9.3 | [RHSA-2024:1784](https://access.redhat.com/errata/RHSA-2024:1784) | <div class="adv_s">Security Advisory</div> ([CVE-2024-28834](https://access.redhat.com/security/cve/CVE-2024-28834))
gnutls-utils-debuginfo | 3.6.16-8.el8_9.3 | |
mod_http2 | 1.15.7-8.module+el8.9.0+21652+2dd1200b.5 | [RHSA-2024:1786](https://access.redhat.com/errata/RHSA-2024:1786) | <div class="adv_s">Security Advisory</div> ([CVE-2024-27316](https://access.redhat.com/security/cve/CVE-2024-27316))
mod_http2-debuginfo | 1.15.7-8.module+el8.9.0+21652+2dd1200b.5 | |
mod_http2-debugsource | 1.15.7-8.module+el8.9.0+21652+2dd1200b.5 | |
python3-bind | 9.11.36-11.el8_9.1 | [RHSA-2024:1782](https://access.redhat.com/errata/RHSA-2024:1782) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868))
python3-bind9.16 | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
python3-unbound | 1.16.2-5.el8_9.6 | [RHSA-2024:1751](https://access.redhat.com/errata/RHSA-2024:1751) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1488](https://access.redhat.com/security/cve/CVE-2024-1488))
python3-unbound-debuginfo | 1.16.2-5.el8_9.6 | |
unbound | 1.16.2-5.el8_9.6 | [RHSA-2024:1751](https://access.redhat.com/errata/RHSA-2024:1751) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1488](https://access.redhat.com/security/cve/CVE-2024-1488))
unbound-debuginfo | 1.16.2-5.el8_9.6 | |
unbound-debugsource | 1.16.2-5.el8_9.6 | |
unbound-devel | 1.16.2-5.el8_9.6 | [RHSA-2024:1751](https://access.redhat.com/errata/RHSA-2024:1751) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1488](https://access.redhat.com/security/cve/CVE-2024-1488))
unbound-libs | 1.16.2-5.el8_9.6 | [RHSA-2024:1751](https://access.redhat.com/errata/RHSA-2024:1751) | <div class="adv_s">Security Advisory</div> ([CVE-2024-1488](https://access.redhat.com/security/cve/CVE-2024-1488))
unbound-libs-debuginfo | 1.16.2-5.el8_9.6 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind9.16-debuginfo | 9.16.23-0.16.el8_9.2 | |
bind9.16-debugsource | 9.16.23-0.16.el8_9.2 | |
bind9.16-devel | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-dnssec-utils-debuginfo | 9.16.23-0.16.el8_9.2 | |
bind9.16-doc | 9.16.23-0.16.el8_9.2 | [RHSA-2024:1781](https://access.redhat.com/errata/RHSA-2024:1781) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4408](https://access.redhat.com/security/cve/CVE-2023-4408), [CVE-2023-50387](https://access.redhat.com/security/cve/CVE-2023-50387), [CVE-2023-50868](https://access.redhat.com/security/cve/CVE-2023-50868), [CVE-2023-5517](https://access.redhat.com/security/cve/CVE-2023-5517), [CVE-2023-5679](https://access.redhat.com/security/cve/CVE-2023-5679), [CVE-2023-6516](https://access.redhat.com/security/cve/CVE-2023-6516))
bind9.16-libs-debuginfo | 9.16.23-0.16.el8_9.2 | |
bind9.16-utils-debuginfo | 9.16.23-0.16.el8_9.2 | |

