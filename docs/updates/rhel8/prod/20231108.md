## 2023-11-08

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.12-1.rh8.cern | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.4.0-1.el8_8 | [RHSA-2023:6187](https://access.redhat.com/errata/RHSA-2023:6187) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
firefox-debuginfo | 115.4.0-1.el8_8 | |
firefox-debugsource | 115.4.0-1.el8_8 | |
thunderbird | 115.4.1-1.el8_8 | [RHSA-2023:6194](https://access.redhat.com/errata/RHSA-2023:6194) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
thunderbird-debuginfo | 115.4.1-1.el8_8 | |
thunderbird-debugsource | 115.4.1-1.el8_8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.12-1.rh8.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.4.0-1.el8_8 | [RHSA-2023:6187](https://access.redhat.com/errata/RHSA-2023:6187) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
firefox-debuginfo | 115.4.0-1.el8_8 | |
firefox-debugsource | 115.4.0-1.el8_8 | |
thunderbird | 115.4.1-1.el8_8 | [RHSA-2023:6194](https://access.redhat.com/errata/RHSA-2023:6194) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
thunderbird-debuginfo | 115.4.1-1.el8_8 | |
thunderbird-debugsource | 115.4.1-1.el8_8 | |

