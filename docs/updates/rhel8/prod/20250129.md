## 2025-01-29

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.17-1.rh8.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_553.36.1.el8_10.rh8.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
bpftool-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-abi-stablelists | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-core | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-cross-headers | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-debug | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-debug-core | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-debug-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-debug-devel | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-debug-modules | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-debug-modules-extra | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-debuginfo-common-x86_64 | 4.18.0-553.36.1.el8_10 | |
kernel-devel | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-doc | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-headers | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-modules | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-modules-extra | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-tools | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-tools-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-tools-libs | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
perf | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
perf-debuginfo | 4.18.0-553.36.1.el8_10 | |
python3-perf | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
python3-perf-debuginfo | 4.18.0-553.36.1.el8_10 | |
rsync | 3.1.3-20.el8_10 | [RHSA-2025:0325](https://access.redhat.com/errata/RHSA-2025:0325) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12085](https://access.redhat.com/security/cve/CVE-2024-12085))
rsync-daemon | 3.1.3-20.el8_10 | [RHSA-2025:0325](https://access.redhat.com/errata/RHSA-2025:0325) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12085](https://access.redhat.com/security/cve/CVE-2024-12085))
rsync-debuginfo | 3.1.3-20.el8_10 | |
rsync-debugsource | 3.1.3-20.el8_10 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
aspnetcore-runtime-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
aspnetcore-runtime-dbg-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
aspnetcore-runtime-dbg-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
aspnetcore-targeting-pack-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
aspnetcore-targeting-pack-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet | 9.0.102-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-apphost-pack-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-apphost-pack-8.0-debuginfo | 8.0.12-1.el8_10 | |
dotnet-apphost-pack-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-apphost-pack-9.0-debuginfo | 9.0.1-1.el8_10 | |
dotnet-host | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-host-debuginfo | 9.0.1-1.el8_10 | |
dotnet-hostfxr-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-hostfxr-8.0-debuginfo | 8.0.12-1.el8_10 | |
dotnet-hostfxr-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-hostfxr-9.0-debuginfo | 9.0.1-1.el8_10 | |
dotnet-runtime-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-runtime-8.0-debuginfo | 8.0.12-1.el8_10 | |
dotnet-runtime-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-runtime-9.0-debuginfo | 9.0.1-1.el8_10 | |
dotnet-runtime-dbg-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-runtime-dbg-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-sdk-8.0 | 8.0.112-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-sdk-8.0-debuginfo | 8.0.112-1.el8_10 | |
dotnet-sdk-9.0 | 9.0.102-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-sdk-9.0-debuginfo | 9.0.102-1.el8_10 | |
dotnet-sdk-aot-9.0 | 9.0.102-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-sdk-aot-9.0-debuginfo | 9.0.102-1.el8_10 | |
dotnet-sdk-dbg-8.0 | 8.0.112-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-sdk-dbg-9.0 | 9.0.102-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-targeting-pack-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-targeting-pack-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-templates-8.0 | 8.0.112-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-templates-9.0 | 9.0.102-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet8.0-debuginfo | 8.0.112-1.el8_10 | |
dotnet8.0-debugsource | 8.0.112-1.el8_10 | |
dotnet9.0-debuginfo | 9.0.102-1.el8_10 | |
dotnet9.0-debugsource | 9.0.102-1.el8_10 | |
grafana | 9.2.10-21.el8_10 | [RHSA-2025:0401](https://access.redhat.com/errata/RHSA-2025:0401) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21613](https://access.redhat.com/security/cve/CVE-2025-21613), [CVE-2025-21614](https://access.redhat.com/security/cve/CVE-2025-21614))
grafana-debuginfo | 9.2.10-21.el8_10 | |
grafana-debugsource | 9.2.10-21.el8_10 | |
grafana-selinux | 9.2.10-21.el8_10 | [RHSA-2025:0401](https://access.redhat.com/errata/RHSA-2025:0401) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21613](https://access.redhat.com/security/cve/CVE-2025-21613), [CVE-2025-21614](https://access.redhat.com/security/cve/CVE-2025-21614))
netstandard-targeting-pack-2.1 | 9.0.102-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))

### rt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-553.36.1.rt7.377.el8_10 | [RHBA-2025:0573](https://access.redhat.com/errata/RHBA-2025:0573) | <div class="adv_b">Bug Fix Advisory</div>
kernel-rt-core | 4.18.0-553.36.1.rt7.377.el8_10 | [RHBA-2025:0573](https://access.redhat.com/errata/RHBA-2025:0573) | <div class="adv_b">Bug Fix Advisory</div>
kernel-rt-debug | 4.18.0-553.36.1.rt7.377.el8_10 | [RHBA-2025:0573](https://access.redhat.com/errata/RHBA-2025:0573) | <div class="adv_b">Bug Fix Advisory</div>
kernel-rt-debug-core | 4.18.0-553.36.1.rt7.377.el8_10 | [RHBA-2025:0573](https://access.redhat.com/errata/RHBA-2025:0573) | <div class="adv_b">Bug Fix Advisory</div>
kernel-rt-debug-debuginfo | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debug-devel | 4.18.0-553.36.1.rt7.377.el8_10 | [RHBA-2025:0573](https://access.redhat.com/errata/RHBA-2025:0573) | <div class="adv_b">Bug Fix Advisory</div>
kernel-rt-debug-modules | 4.18.0-553.36.1.rt7.377.el8_10 | [RHBA-2025:0573](https://access.redhat.com/errata/RHBA-2025:0573) | <div class="adv_b">Bug Fix Advisory</div>
kernel-rt-debug-modules-extra | 4.18.0-553.36.1.rt7.377.el8_10 | [RHBA-2025:0573](https://access.redhat.com/errata/RHBA-2025:0573) | <div class="adv_b">Bug Fix Advisory</div>
kernel-rt-debuginfo | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debuginfo-common-x86_64 | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-devel | 4.18.0-553.36.1.rt7.377.el8_10 | [RHBA-2025:0573](https://access.redhat.com/errata/RHBA-2025:0573) | <div class="adv_b">Bug Fix Advisory</div>
kernel-rt-modules | 4.18.0-553.36.1.rt7.377.el8_10 | [RHBA-2025:0573](https://access.redhat.com/errata/RHBA-2025:0573) | <div class="adv_b">Bug Fix Advisory</div>
kernel-rt-modules-extra | 4.18.0-553.36.1.rt7.377.el8_10 | [RHBA-2025:0573](https://access.redhat.com/errata/RHBA-2025:0573) | <div class="adv_b">Bug Fix Advisory</div>

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-553.36.1.el8_10 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.12-1.el8_10 | |
dotnet-apphost-pack-9.0-debuginfo | 9.0.1-1.el8_10 | |
dotnet-host-debuginfo | 9.0.1-1.el8_10 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.12-1.el8_10 | |
dotnet-hostfxr-9.0-debuginfo | 9.0.1-1.el8_10 | |
dotnet-runtime-8.0-debuginfo | 8.0.12-1.el8_10 | |
dotnet-runtime-9.0-debuginfo | 9.0.1-1.el8_10 | |
dotnet-sdk-8.0-debuginfo | 8.0.112-1.el8_10 | |
dotnet-sdk-8.0-source-built-artifacts | 8.0.112-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-sdk-9.0-debuginfo | 9.0.102-1.el8_10 | |
dotnet-sdk-9.0-source-built-artifacts | 9.0.102-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-sdk-aot-9.0-debuginfo | 9.0.102-1.el8_10 | |
dotnet8.0-debuginfo | 8.0.112-1.el8_10 | |
dotnet8.0-debugsource | 8.0.112-1.el8_10 | |
dotnet9.0-debuginfo | 9.0.102-1.el8_10 | |
dotnet9.0-debugsource | 9.0.102-1.el8_10 | |
kernel-debug-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-debuginfo-common-x86_64 | 4.18.0-553.36.1.el8_10 | |
kernel-tools-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-tools-libs-devel | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
perf-debuginfo | 4.18.0-553.36.1.el8_10 | |
python3-perf-debuginfo | 4.18.0-553.36.1.el8_10 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.17-1.rh8.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_553.36.1.el8_10.rh8.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
bpftool-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-abi-stablelists | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-core | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-cross-headers | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-debug | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-debug-core | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-debug-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-debug-devel | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-debug-modules | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-debug-modules-extra | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-debuginfo-common-aarch64 | 4.18.0-553.36.1.el8_10 | |
kernel-devel | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-doc | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-headers | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-modules | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-modules-extra | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-tools | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
kernel-tools-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-tools-libs | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
perf | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
perf-debuginfo | 4.18.0-553.36.1.el8_10 | |
python3-perf | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
python3-perf-debuginfo | 4.18.0-553.36.1.el8_10 | |
rsync | 3.1.3-20.el8_10 | [RHSA-2025:0325](https://access.redhat.com/errata/RHSA-2025:0325) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12085](https://access.redhat.com/security/cve/CVE-2024-12085))
rsync-daemon | 3.1.3-20.el8_10 | [RHSA-2025:0325](https://access.redhat.com/errata/RHSA-2025:0325) | <div class="adv_s">Security Advisory</div> ([CVE-2024-12085](https://access.redhat.com/security/cve/CVE-2024-12085))
rsync-debuginfo | 3.1.3-20.el8_10 | |
rsync-debugsource | 3.1.3-20.el8_10 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
aspnetcore-runtime-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
aspnetcore-runtime-dbg-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
aspnetcore-runtime-dbg-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
aspnetcore-targeting-pack-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
aspnetcore-targeting-pack-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet | 9.0.102-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-apphost-pack-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-apphost-pack-8.0-debuginfo | 8.0.12-1.el8_10 | |
dotnet-apphost-pack-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-apphost-pack-9.0-debuginfo | 9.0.1-1.el8_10 | |
dotnet-host | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-host-debuginfo | 9.0.1-1.el8_10 | |
dotnet-hostfxr-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-hostfxr-8.0-debuginfo | 8.0.12-1.el8_10 | |
dotnet-hostfxr-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-hostfxr-9.0-debuginfo | 9.0.1-1.el8_10 | |
dotnet-runtime-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-runtime-8.0-debuginfo | 8.0.12-1.el8_10 | |
dotnet-runtime-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-runtime-9.0-debuginfo | 9.0.1-1.el8_10 | |
dotnet-runtime-dbg-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-runtime-dbg-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-sdk-8.0 | 8.0.112-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-sdk-8.0-debuginfo | 8.0.112-1.el8_10 | |
dotnet-sdk-9.0 | 9.0.102-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-sdk-9.0-debuginfo | 9.0.102-1.el8_10 | |
dotnet-sdk-aot-9.0 | 9.0.102-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-sdk-aot-9.0-debuginfo | 9.0.102-1.el8_10 | |
dotnet-sdk-dbg-8.0 | 8.0.112-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-sdk-dbg-9.0 | 9.0.102-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-targeting-pack-8.0 | 8.0.12-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-targeting-pack-9.0 | 9.0.1-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-templates-8.0 | 8.0.112-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-templates-9.0 | 9.0.102-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet8.0-debuginfo | 8.0.112-1.el8_10 | |
dotnet8.0-debugsource | 8.0.112-1.el8_10 | |
dotnet9.0-debuginfo | 9.0.102-1.el8_10 | |
dotnet9.0-debugsource | 9.0.102-1.el8_10 | |
grafana | 9.2.10-21.el8_10 | [RHSA-2025:0401](https://access.redhat.com/errata/RHSA-2025:0401) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21613](https://access.redhat.com/security/cve/CVE-2025-21613), [CVE-2025-21614](https://access.redhat.com/security/cve/CVE-2025-21614))
grafana-debuginfo | 9.2.10-21.el8_10 | |
grafana-debugsource | 9.2.10-21.el8_10 | |
grafana-selinux | 9.2.10-21.el8_10 | [RHSA-2025:0401](https://access.redhat.com/errata/RHSA-2025:0401) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21613](https://access.redhat.com/security/cve/CVE-2025-21613), [CVE-2025-21614](https://access.redhat.com/security/cve/CVE-2025-21614))
netstandard-targeting-pack-2.1 | 9.0.102-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-553.36.1.el8_10 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.12-1.el8_10 | |
dotnet-apphost-pack-9.0-debuginfo | 9.0.1-1.el8_10 | |
dotnet-host-debuginfo | 9.0.1-1.el8_10 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.12-1.el8_10 | |
dotnet-hostfxr-9.0-debuginfo | 9.0.1-1.el8_10 | |
dotnet-runtime-8.0-debuginfo | 8.0.12-1.el8_10 | |
dotnet-runtime-9.0-debuginfo | 9.0.1-1.el8_10 | |
dotnet-sdk-8.0-debuginfo | 8.0.112-1.el8_10 | |
dotnet-sdk-8.0-source-built-artifacts | 8.0.112-1.el8_10 | [RHSA-2025:0381](https://access.redhat.com/errata/RHSA-2025:0381) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-sdk-9.0-debuginfo | 9.0.102-1.el8_10 | |
dotnet-sdk-9.0-source-built-artifacts | 9.0.102-1.el8_10 | [RHSA-2025:0382](https://access.redhat.com/errata/RHSA-2025:0382) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21171](https://access.redhat.com/security/cve/CVE-2025-21171), [CVE-2025-21172](https://access.redhat.com/security/cve/CVE-2025-21172), [CVE-2025-21173](https://access.redhat.com/security/cve/CVE-2025-21173), [CVE-2025-21176](https://access.redhat.com/security/cve/CVE-2025-21176))
dotnet-sdk-aot-9.0-debuginfo | 9.0.102-1.el8_10 | |
dotnet8.0-debuginfo | 8.0.112-1.el8_10 | |
dotnet8.0-debugsource | 8.0.112-1.el8_10 | |
dotnet9.0-debuginfo | 9.0.102-1.el8_10 | |
dotnet9.0-debugsource | 9.0.102-1.el8_10 | |
kernel-debug-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-debuginfo-common-aarch64 | 4.18.0-553.36.1.el8_10 | |
kernel-tools-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-tools-libs-devel | 4.18.0-553.36.1.el8_10 | [RHBA-2025:0572](https://access.redhat.com/errata/RHBA-2025:0572) | <div class="adv_b">Bug Fix Advisory</div>
perf-debuginfo | 4.18.0-553.36.1.el8_10 | |
python3-perf-debuginfo | 4.18.0-553.36.1.el8_10 | |

