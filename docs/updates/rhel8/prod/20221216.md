## 2022-12-16

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.0-2.rh8.cern | |
cern-sssd-conf | 1.5-1.rh8.cern | |
cern-sssd-conf-domain-cernch | 1.5-1.rh8.cern | |
cern-sssd-conf-global | 1.5-1.rh8.cern | |
cern-sssd-conf-global-cernch | 1.5-1.rh8.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.5-1.rh8.cern | |
perl-Authen-Krb5 | 1.9-5.rh8.cern | |
perl-Authen-Krb5-debugsource | 1.9-5.rh8.cern | |
pyphonebook | 2.1.4-2.rh8.cern | |
useraddcern | 1.0-1.rh8.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afs_tools | 2.3-0.rh8.cern | |
afs_tools_standalone | 2.3-0.rh8.cern | |
arc | 48-1.3.rh8.cern | |
cern-aklog-systemd-user | 1.4-1.rh8.cern | |
dkms-openafs | 1.8.9pre2-0.rh8.cern | |
dkms-openafs | 1.8.9pre2-3.rh8.cern | |
kmod-openafs | 1.8.9pre2-3.4.18.0_425.3.1.el8.rh8.cern | |
openafs | 1.8.9pre2-0.rh8.cern | |
openafs | 1.8.9pre2-3.rh8.cern | |
openafs-authlibs | 1.8.9pre2-0.rh8.cern | |
openafs-authlibs | 1.8.9pre2-3.rh8.cern | |
openafs-authlibs-devel | 1.8.9pre2-0.rh8.cern | |
openafs-authlibs-devel | 1.8.9pre2-3.rh8.cern | |
openafs-client | 1.8.9pre2-0.rh8.cern | |
openafs-client | 1.8.9pre2-3.rh8.cern | |
openafs-compat | 1.8.9pre2-0.rh8.cern | |
openafs-compat | 1.8.9pre2-3.rh8.cern | |
openafs-debugsource | 1.8.9pre2-0.rh8.cern | |
openafs-debugsource | 1.8.9pre2-3.rh8.cern | |
openafs-debugsource | 1.8.9_4.18.0_425.3.1.el8pre2-3.rh8.cern | |
openafs-devel | 1.8.9pre2-0.rh8.cern | |
openafs-devel | 1.8.9pre2-3.rh8.cern | |
openafs-docs | 1.8.9pre2-0.rh8.cern | |
openafs-docs | 1.8.9pre2-3.rh8.cern | |
openafs-kernel-source | 1.8.9pre2-0.rh8.cern | |
openafs-kernel-source | 1.8.9pre2-3.rh8.cern | |
openafs-krb5 | 1.8.9pre2-0.rh8.cern | |
openafs-krb5 | 1.8.9pre2-3.rh8.cern | |
openafs-server | 1.8.9pre2-0.rh8.cern | |
openafs-server | 1.8.9pre2-3.rh8.cern | |
pam_afs_session | 2.6-3.rh8.cern | |
pam_afs_session-debugsource | 2.6-3.rh8.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libsolv | 0.7.20-4.el8_7 | |
libsolv-debuginfo | 0.7.20-4.el8_7 | |
libsolv-debugsource | 0.7.20-4.el8_7 | |
libsolv-demo-debuginfo | 0.7.20-4.el8_7 | |
libsolv-tools-debuginfo | 0.7.20-4.el8_7 | |
perl-solv-debuginfo | 0.7.20-4.el8_7 | |
python3-solv | 0.7.20-4.el8_7 | |
python3-solv-debuginfo | 0.7.20-4.el8_7 | |
ruby-solv-debuginfo | 0.7.20-4.el8_7 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-3.1 | 3.1.32-1.el8_7 | |
aspnetcore-runtime-6.0 | 6.0.12-1.el8_7 | |
aspnetcore-runtime-7.0 | 7.0.1-1.el8_7 | |
aspnetcore-targeting-pack-3.1 | 3.1.32-1.el8_7 | |
aspnetcore-targeting-pack-6.0 | 6.0.12-1.el8_7 | |
aspnetcore-targeting-pack-7.0 | 7.0.1-1.el8_7 | |
dotnet | 7.0.101-1.el8_7 | |
dotnet-apphost-pack-3.1 | 3.1.32-1.el8_7 | |
dotnet-apphost-pack-3.1-debuginfo | 3.1.32-1.el8_7 | |
dotnet-apphost-pack-6.0 | 6.0.12-1.el8_7 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-apphost-pack-7.0 | 7.0.1-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-host | 7.0.1-1.el8_7 | |
dotnet-host-debuginfo | 7.0.1-1.el8_7 | |
dotnet-hostfxr-3.1 | 3.1.32-1.el8_7 | |
dotnet-hostfxr-3.1-debuginfo | 3.1.32-1.el8_7 | |
dotnet-hostfxr-6.0 | 6.0.12-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-hostfxr-7.0 | 7.0.1-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-runtime-3.1 | 3.1.32-1.el8_7 | |
dotnet-runtime-3.1-debuginfo | 3.1.32-1.el8_7 | |
dotnet-runtime-6.0 | 6.0.12-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-runtime-7.0 | 7.0.1-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-sdk-3.1 | 3.1.426-1.el8_7 | |
dotnet-sdk-3.1-debuginfo | 3.1.426-1.el8_7 | |
dotnet-sdk-6.0 | 6.0.112-1.el8_7 | |
dotnet-sdk-6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet-sdk-7.0 | 7.0.101-1.el8_7 | |
dotnet-sdk-7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet-targeting-pack-3.1 | 3.1.32-1.el8_7 | |
dotnet-targeting-pack-6.0 | 6.0.12-1.el8_7 | |
dotnet-targeting-pack-7.0 | 7.0.1-1.el8_7 | |
dotnet-templates-3.1 | 3.1.426-1.el8_7 | |
dotnet-templates-6.0 | 6.0.112-1.el8_7 | |
dotnet-templates-7.0 | 7.0.101-1.el8_7 | |
dotnet3.1-debuginfo | 3.1.426-1.el8_7 | |
dotnet3.1-debugsource | 3.1.426-1.el8_7 | |
dotnet6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet6.0-debugsource | 6.0.112-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet7.0-debugsource | 7.0.101-1.el8_7 | |
firefox | 102.6.0-1.el8_7 | |
firefox-debuginfo | 102.6.0-1.el8_7 | |
firefox-debugsource | 102.6.0-1.el8_7 | |
netstandard-targeting-pack-2.1 | 7.0.101-1.el8_7 | |
nodejs | 16.18.1-3.module+el8.7.0+17465+1a1abd74 | |
nodejs-debuginfo | 16.18.1-3.module+el8.7.0+17465+1a1abd74 | |
nodejs-debugsource | 16.18.1-3.module+el8.7.0+17465+1a1abd74 | |
nodejs-devel | 16.18.1-3.module+el8.7.0+17465+1a1abd74 | |
nodejs-docs | 16.18.1-3.module+el8.7.0+17465+1a1abd74 | |
nodejs-full-i18n | 16.18.1-3.module+el8.7.0+17465+1a1abd74 | |
nodejs-nodemon | 2.0.20-2.module+el8.7.0+17412+bb0e4a6b | |
npm | 8.19.2-1.16.18.1.3.module+el8.7.0+17465+1a1abd74 | |
prometheus-jmx-exporter | 0.12.0-9.el8_7 | |
prometheus-jmx-exporter-openjdk11 | 0.12.0-9.el8_7 | |
prometheus-jmx-exporter-openjdk17 | 0.12.0-9.el8_7 | |
prometheus-jmx-exporter-openjdk8 | 0.12.0-9.el8_7 | |
thunderbird | 102.6.0-2.el8_7 | |
thunderbird-debuginfo | 102.6.0-2.el8_7 | |
thunderbird-debugsource | 102.6.0-2.el8_7 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-3.1-debuginfo | 3.1.32-1.el8_7 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-host-debuginfo | 7.0.1-1.el8_7 | |
dotnet-hostfxr-3.1-debuginfo | 3.1.32-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-runtime-3.1-debuginfo | 3.1.32-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-sdk-3.1-debuginfo | 3.1.426-1.el8_7 | |
dotnet-sdk-3.1-source-built-artifacts | 3.1.426-1.el8_7 | |
dotnet-sdk-6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.112-1.el8_7 | |
dotnet-sdk-7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.101-1.el8_7 | |
dotnet3.1-debuginfo | 3.1.426-1.el8_7 | |
dotnet3.1-debugsource | 3.1.426-1.el8_7 | |
dotnet6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet6.0-debugsource | 6.0.112-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet7.0-debugsource | 7.0.101-1.el8_7 | |
libsolv-debuginfo | 0.7.20-4.el8_7 | |
libsolv-debugsource | 0.7.20-4.el8_7 | |
libsolv-demo-debuginfo | 0.7.20-4.el8_7 | |
libsolv-devel | 0.7.20-4.el8_7 | |
libsolv-tools | 0.7.20-4.el8_7 | |
libsolv-tools-debuginfo | 0.7.20-4.el8_7 | |
perl-solv-debuginfo | 0.7.20-4.el8_7 | |
python3-solv-debuginfo | 0.7.20-4.el8_7 | |
ruby-solv-debuginfo | 0.7.20-4.el8_7 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.0-2.rh8.cern | |
cern-sssd-conf | 1.5-1.rh8.cern | |
cern-sssd-conf-domain-cernch | 1.5-1.rh8.cern | |
cern-sssd-conf-global | 1.5-1.rh8.cern | |
cern-sssd-conf-global-cernch | 1.5-1.rh8.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.5-1.rh8.cern | |
perl-Authen-Krb5 | 1.9-5.rh8.cern | |
perl-Authen-Krb5-debugsource | 1.9-5.rh8.cern | |
pyphonebook | 2.1.4-2.rh8.cern | |
useraddcern | 1.0-1.rh8.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afs_tools | 2.3-0.rh8.cern | |
afs_tools_standalone | 2.3-0.rh8.cern | |
arc | 48-1.3.rh8.cern | |
cern-aklog-systemd-user | 1.4-1.rh8.cern | |
dkms-openafs | 1.8.9pre2-0.rh8.cern | |
dkms-openafs | 1.8.9pre2-3.rh8.cern | |
kmod-openafs | 1.8.9pre2-3.4.18.0_425.3.1.el8.rh8.cern | |
openafs | 1.8.9pre2-0.rh8.cern | |
openafs | 1.8.9pre2-3.rh8.cern | |
openafs-authlibs | 1.8.9pre2-0.rh8.cern | |
openafs-authlibs | 1.8.9pre2-3.rh8.cern | |
openafs-authlibs-devel | 1.8.9pre2-0.rh8.cern | |
openafs-authlibs-devel | 1.8.9pre2-3.rh8.cern | |
openafs-client | 1.8.9pre2-0.rh8.cern | |
openafs-client | 1.8.9pre2-3.rh8.cern | |
openafs-compat | 1.8.9pre2-0.rh8.cern | |
openafs-compat | 1.8.9pre2-3.rh8.cern | |
openafs-debugsource | 1.8.9pre2-0.rh8.cern | |
openafs-debugsource | 1.8.9pre2-3.rh8.cern | |
openafs-debugsource | 1.8.9_4.18.0_425.3.1.el8pre2-3.rh8.cern | |
openafs-devel | 1.8.9pre2-0.rh8.cern | |
openafs-devel | 1.8.9pre2-3.rh8.cern | |
openafs-docs | 1.8.9pre2-0.rh8.cern | |
openafs-docs | 1.8.9pre2-3.rh8.cern | |
openafs-kernel-source | 1.8.9pre2-0.rh8.cern | |
openafs-kernel-source | 1.8.9pre2-3.rh8.cern | |
openafs-krb5 | 1.8.9pre2-0.rh8.cern | |
openafs-krb5 | 1.8.9pre2-3.rh8.cern | |
openafs-server | 1.8.9pre2-0.rh8.cern | |
openafs-server | 1.8.9pre2-3.rh8.cern | |
pam_afs_session | 2.6-3.rh8.cern | |
pam_afs_session-debugsource | 2.6-3.rh8.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libsolv | 0.7.20-4.el8_7 | |
libsolv-debuginfo | 0.7.20-4.el8_7 | |
libsolv-debugsource | 0.7.20-4.el8_7 | |
libsolv-demo-debuginfo | 0.7.20-4.el8_7 | |
libsolv-tools-debuginfo | 0.7.20-4.el8_7 | |
perl-solv-debuginfo | 0.7.20-4.el8_7 | |
python3-solv | 0.7.20-4.el8_7 | |
python3-solv-debuginfo | 0.7.20-4.el8_7 | |
ruby-solv-debuginfo | 0.7.20-4.el8_7 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.12-1.el8_7 | |
aspnetcore-runtime-7.0 | 7.0.1-1.el8_7 | |
aspnetcore-targeting-pack-6.0 | 6.0.12-1.el8_7 | |
aspnetcore-targeting-pack-7.0 | 7.0.1-1.el8_7 | |
dotnet | 7.0.101-1.el8_7 | |
dotnet-apphost-pack-6.0 | 6.0.12-1.el8_7 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-apphost-pack-7.0 | 7.0.1-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-host | 7.0.1-1.el8_7 | |
dotnet-host-debuginfo | 7.0.1-1.el8_7 | |
dotnet-hostfxr-6.0 | 6.0.12-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-hostfxr-7.0 | 7.0.1-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-runtime-6.0 | 6.0.12-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-runtime-7.0 | 7.0.1-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-sdk-6.0 | 6.0.112-1.el8_7 | |
dotnet-sdk-6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet-sdk-7.0 | 7.0.101-1.el8_7 | |
dotnet-sdk-7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet-targeting-pack-6.0 | 6.0.12-1.el8_7 | |
dotnet-targeting-pack-7.0 | 7.0.1-1.el8_7 | |
dotnet-templates-6.0 | 6.0.112-1.el8_7 | |
dotnet-templates-7.0 | 7.0.101-1.el8_7 | |
dotnet6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet6.0-debugsource | 6.0.112-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet7.0-debugsource | 7.0.101-1.el8_7 | |
firefox | 102.6.0-1.el8_7 | |
firefox-debuginfo | 102.6.0-1.el8_7 | |
firefox-debugsource | 102.6.0-1.el8_7 | |
netstandard-targeting-pack-2.1 | 7.0.101-1.el8_7 | |
nodejs | 16.18.1-3.module+el8.7.0+17465+1a1abd74 | |
nodejs-debuginfo | 16.18.1-3.module+el8.7.0+17465+1a1abd74 | |
nodejs-debugsource | 16.18.1-3.module+el8.7.0+17465+1a1abd74 | |
nodejs-devel | 16.18.1-3.module+el8.7.0+17465+1a1abd74 | |
nodejs-docs | 16.18.1-3.module+el8.7.0+17465+1a1abd74 | |
nodejs-full-i18n | 16.18.1-3.module+el8.7.0+17465+1a1abd74 | |
nodejs-nodemon | 2.0.20-2.module+el8.7.0+17412+bb0e4a6b | |
npm | 8.19.2-1.16.18.1.3.module+el8.7.0+17465+1a1abd74 | |
prometheus-jmx-exporter | 0.12.0-9.el8_7 | |
prometheus-jmx-exporter-openjdk11 | 0.12.0-9.el8_7 | |
prometheus-jmx-exporter-openjdk17 | 0.12.0-9.el8_7 | |
prometheus-jmx-exporter-openjdk8 | 0.12.0-9.el8_7 | |
thunderbird | 102.6.0-2.el8_7 | |
thunderbird-debuginfo | 102.6.0-2.el8_7 | |
thunderbird-debugsource | 102.6.0-2.el8_7 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-host-debuginfo | 7.0.1-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.12-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.1-1.el8_7 | |
dotnet-sdk-6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.112-1.el8_7 | |
dotnet-sdk-7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.101-1.el8_7 | |
dotnet6.0-debuginfo | 6.0.112-1.el8_7 | |
dotnet6.0-debugsource | 6.0.112-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.101-1.el8_7 | |
dotnet7.0-debugsource | 7.0.101-1.el8_7 | |
libsolv-debuginfo | 0.7.20-4.el8_7 | |
libsolv-debugsource | 0.7.20-4.el8_7 | |
libsolv-demo-debuginfo | 0.7.20-4.el8_7 | |
libsolv-devel | 0.7.20-4.el8_7 | |
libsolv-tools | 0.7.20-4.el8_7 | |
libsolv-tools-debuginfo | 0.7.20-4.el8_7 | |
perl-solv-debuginfo | 0.7.20-4.el8_7 | |
python3-solv-debuginfo | 0.7.20-4.el8_7 | |
ruby-solv-debuginfo | 0.7.20-4.el8_7 | |

