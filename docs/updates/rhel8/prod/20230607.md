## 2023-06-07

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.4.18.0_477.13.1.el8_8.rh8.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
bpftool-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-abi-stablelists | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-core | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-cross-headers | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-debug | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-debug-core | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-debug-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debug-devel | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-debug-modules | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-debug-modules-extra | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debuginfo-common-x86_64 | 4.18.0-477.13.1.el8_8 | |
kernel-devel | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-doc | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-headers | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-modules | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-modules-extra | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-tools | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-tools-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-tools-libs | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kpatch-patch-4_18_0-477_10_1 | 1-1.el8_8 | [RHSA-2023:3351](https://access.redhat.com/errata/RHSA-2023:3351) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kpatch-patch-4_18_0-477_10_1-debuginfo | 1-1.el8_8 | |
kpatch-patch-4_18_0-477_10_1-debugsource | 1-1.el8_8 | |
kpatch-patch-4_18_0-477_13_1 | 0-0.el8_8 | [RHEA-2023:3359](https://access.redhat.com/errata/RHEA-2023:3359) | <div class="adv_e">Product Enhancement Advisory</div>
perf | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
perf-debuginfo | 4.18.0-477.13.1.el8_8 | |
python3-perf | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
python3-perf-debuginfo | 4.18.0-477.13.1.el8_8 | |
sos | 4.5.3-1.el8 | [RHBA-2023:3414](https://access.redhat.com/errata/RHBA-2023:3414) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.5.3-1.el8 | [RHBA-2023:3414](https://access.redhat.com/errata/RHBA-2023:3414) | <div class="adv_b">Bug Fix Advisory</div>

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-filters | 1.20.0-29.el8_8.2 | |
cups-filters-debuginfo | 1.20.0-29.el8_8.2 | |
cups-filters-debugsource | 1.20.0-29.el8_8.2 | |
cups-filters-libs | 1.20.0-29.el8_8.2 | |
cups-filters-libs-debuginfo | 1.20.0-29.el8_8.2 | |
go-toolset | 1.19.9-1.module+el8.8.0+18857+fca43658 | [RHSA-2023:3319](https://access.redhat.com/errata/RHSA-2023:3319) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang | 1.19.9-1.module+el8.8.0+18857+fca43658 | [RHSA-2023:3319](https://access.redhat.com/errata/RHSA-2023:3319) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-bin | 1.19.9-1.module+el8.8.0+18857+fca43658 | [RHSA-2023:3319](https://access.redhat.com/errata/RHSA-2023:3319) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-docs | 1.19.9-1.module+el8.8.0+18857+fca43658 | [RHSA-2023:3319](https://access.redhat.com/errata/RHSA-2023:3319) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-misc | 1.19.9-1.module+el8.8.0+18857+fca43658 | [RHSA-2023:3319](https://access.redhat.com/errata/RHSA-2023:3319) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-race | 1.19.9-1.module+el8.8.0+18857+fca43658 | [RHSA-2023:3319](https://access.redhat.com/errata/RHSA-2023:3319) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-src | 1.19.9-1.module+el8.8.0+18857+fca43658 | [RHSA-2023:3319](https://access.redhat.com/errata/RHSA-2023:3319) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-tests | 1.19.9-1.module+el8.8.0+18857+fca43658 | [RHSA-2023:3319](https://access.redhat.com/errata/RHSA-2023:3319) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))

### rt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-477.13.1.rt7.276.el8_8 | [RHSA-2023:3350](https://access.redhat.com/errata/RHSA-2023:3350) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-rt-core | 4.18.0-477.13.1.rt7.276.el8_8 | [RHSA-2023:3350](https://access.redhat.com/errata/RHSA-2023:3350) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-rt-debug | 4.18.0-477.13.1.rt7.276.el8_8 | [RHSA-2023:3350](https://access.redhat.com/errata/RHSA-2023:3350) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-rt-debug-core | 4.18.0-477.13.1.rt7.276.el8_8 | [RHSA-2023:3350](https://access.redhat.com/errata/RHSA-2023:3350) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-rt-debug-debuginfo | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debug-devel | 4.18.0-477.13.1.rt7.276.el8_8 | [RHSA-2023:3350](https://access.redhat.com/errata/RHSA-2023:3350) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-rt-debug-modules | 4.18.0-477.13.1.rt7.276.el8_8 | [RHSA-2023:3350](https://access.redhat.com/errata/RHSA-2023:3350) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-rt-debug-modules-extra | 4.18.0-477.13.1.rt7.276.el8_8 | [RHSA-2023:3350](https://access.redhat.com/errata/RHSA-2023:3350) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-rt-debuginfo | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debuginfo-common-x86_64 | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-devel | 4.18.0-477.13.1.rt7.276.el8_8 | [RHSA-2023:3350](https://access.redhat.com/errata/RHSA-2023:3350) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-rt-modules | 4.18.0-477.13.1.rt7.276.el8_8 | [RHSA-2023:3350](https://access.redhat.com/errata/RHSA-2023:3350) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-rt-modules-extra | 4.18.0-477.13.1.rt7.276.el8_8 | [RHSA-2023:3350](https://access.redhat.com/errata/RHSA-2023:3350) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-477.13.1.el8_8 | |
cups-filters-debuginfo | 1.20.0-29.el8_8.2 | |
cups-filters-debugsource | 1.20.0-29.el8_8.2 | |
cups-filters-devel | 1.20.0-29.el8_8.2 | |
cups-filters-libs-debuginfo | 1.20.0-29.el8_8.2 | |
kernel-debug-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debuginfo-common-x86_64 | 4.18.0-477.13.1.el8_8 | |
kernel-tools-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-tools-libs-devel | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
perf-debuginfo | 4.18.0-477.13.1.el8_8 | |
python3-perf-debuginfo | 4.18.0-477.13.1.el8_8 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.4.18.0_477.13.1.el8_8.rh8.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
bpftool-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-abi-stablelists | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-core | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-cross-headers | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-debug | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-debug-core | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-debug-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debug-devel | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-debug-modules | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-debug-modules-extra | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debuginfo-common-aarch64 | 4.18.0-477.13.1.el8_8 | |
kernel-devel | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-doc | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-headers | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-modules | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-modules-extra | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-tools | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
kernel-tools-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-tools-libs | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
perf | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
perf-debuginfo | 4.18.0-477.13.1.el8_8 | |
python3-perf | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
python3-perf-debuginfo | 4.18.0-477.13.1.el8_8 | |
sos | 4.5.3-1.el8 | [RHBA-2023:3414](https://access.redhat.com/errata/RHBA-2023:3414) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.5.3-1.el8 | [RHBA-2023:3414](https://access.redhat.com/errata/RHBA-2023:3414) | <div class="adv_b">Bug Fix Advisory</div>

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-filters | 1.20.0-29.el8_8.2 | |
cups-filters-debuginfo | 1.20.0-29.el8_8.2 | |
cups-filters-debugsource | 1.20.0-29.el8_8.2 | |
cups-filters-libs | 1.20.0-29.el8_8.2 | |
cups-filters-libs-debuginfo | 1.20.0-29.el8_8.2 | |
go-toolset | 1.19.9-1.module+el8.8.0+18857+fca43658 | [RHSA-2023:3319](https://access.redhat.com/errata/RHSA-2023:3319) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang | 1.19.9-1.module+el8.8.0+18857+fca43658 | [RHSA-2023:3319](https://access.redhat.com/errata/RHSA-2023:3319) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-bin | 1.19.9-1.module+el8.8.0+18857+fca43658 | [RHSA-2023:3319](https://access.redhat.com/errata/RHSA-2023:3319) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-docs | 1.19.9-1.module+el8.8.0+18857+fca43658 | [RHSA-2023:3319](https://access.redhat.com/errata/RHSA-2023:3319) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-misc | 1.19.9-1.module+el8.8.0+18857+fca43658 | [RHSA-2023:3319](https://access.redhat.com/errata/RHSA-2023:3319) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-src | 1.19.9-1.module+el8.8.0+18857+fca43658 | [RHSA-2023:3319](https://access.redhat.com/errata/RHSA-2023:3319) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))
golang-tests | 1.19.9-1.module+el8.8.0+18857+fca43658 | [RHSA-2023:3319](https://access.redhat.com/errata/RHSA-2023:3319) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24540](https://access.redhat.com/security/cve/CVE-2023-24540))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-477.13.1.el8_8 | |
cups-filters-debuginfo | 1.20.0-29.el8_8.2 | |
cups-filters-debugsource | 1.20.0-29.el8_8.2 | |
cups-filters-devel | 1.20.0-29.el8_8.2 | |
cups-filters-libs-debuginfo | 1.20.0-29.el8_8.2 | |
kernel-debug-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debuginfo-common-aarch64 | 4.18.0-477.13.1.el8_8 | |
kernel-tools-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-tools-libs-devel | 4.18.0-477.13.1.el8_8 | [RHSA-2023:3349](https://access.redhat.com/errata/RHSA-2023:3349) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32233](https://access.redhat.com/security/cve/CVE-2023-32233))
perf-debuginfo | 4.18.0-477.13.1.el8_8 | |
python3-perf-debuginfo | 4.18.0-477.13.1.el8_8 | |

