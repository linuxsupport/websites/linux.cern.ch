## 2023-08-09

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
ctdb-debuginfo | 4.17.5-3.el8_8 | |
libnetapi | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libnetapi-debuginfo | 4.17.5-3.el8_8 | |
libsmbclient | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libsmbclient-debuginfo | 4.17.5-3.el8_8 | |
libwbclient | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libwbclient-debuginfo | 4.17.5-3.el8_8 | |
openssh | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-askpass-debuginfo | 8.0p1-19.el8_8 | |
openssh-cavs | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-cavs-debuginfo | 8.0p1-19.el8_8 | |
openssh-clients | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-clients-debuginfo | 8.0p1-19.el8_8 | |
openssh-debuginfo | 8.0p1-19.el8_8 | |
openssh-debugsource | 8.0p1-19.el8_8 | |
openssh-keycat | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-keycat-debuginfo | 8.0p1-19.el8_8 | |
openssh-ldap | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-ldap-debuginfo | 8.0p1-19.el8_8 | |
openssh-server | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-server-debuginfo | 8.0p1-19.el8_8 | |
pam_ssh_agent_auth | 0.10.3-7.19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
pam_ssh_agent_auth-debuginfo | 0.10.3-7.19.el8_8 | |
python3-samba | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
python3-samba-dc | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
python3-samba-dc-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-test | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client-debuginfo | 4.17.5-3.el8_8 | |
samba-client-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-common-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-common-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-tools | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-common-tools-debuginfo | 4.17.5-3.el8_8 | |
samba-dc-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-dc-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-dcerpc | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-dcerpc-debuginfo | 4.17.5-3.el8_8 | |
samba-debuginfo | 4.17.5-3.el8_8 | |
samba-debugsource | 4.17.5-3.el8_8 | |
samba-krb5-printing | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-krb5-printing-debuginfo | 4.17.5-3.el8_8 | |
samba-ldb-ldap-modules | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-ldb-ldap-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-pidl | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-test | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-test-debuginfo | 4.17.5-3.el8_8 | |
samba-test-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-test-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-tools | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-usershares | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-vfs-iouring-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-clients | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-clients-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-krb5-locator | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-krb5-locator-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-modules | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-winexe | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winexe-debuginfo | 4.17.5-3.el8_8 | |
sos | 4.5.5-2.el8 | [RHBA-2023:4279](https://access.redhat.com/errata/RHBA-2023:4279) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.5.5-2.el8 | [RHBA-2023:4279](https://access.redhat.com/errata/RHBA-2023:4279) | <div class="adv_b">Bug Fix Advisory</div>

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cjose | 0.6.1-3.module+el8.8.0+19464+578f4546 | [RHSA-2023:4418](https://access.redhat.com/errata/RHSA-2023:4418) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37464](https://access.redhat.com/security/cve/CVE-2023-37464))
cjose-debuginfo | 0.6.1-3.module+el8.8.0+19464+578f4546 | |
cjose-debugsource | 0.6.1-3.module+el8.8.0+19464+578f4546 | |
cjose-devel | 0.6.1-3.module+el8.8.0+19464+578f4546 | [RHSA-2023:4418](https://access.redhat.com/errata/RHSA-2023:4418) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37464](https://access.redhat.com/security/cve/CVE-2023-37464))
ctdb-debuginfo | 4.17.5-3.el8_8 | |
libnetapi-debuginfo | 4.17.5-3.el8_8 | |
libsmbclient-debuginfo | 4.17.5-3.el8_8 | |
libwbclient-debuginfo | 4.17.5-3.el8_8 | |
openssh-askpass | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-askpass-debuginfo | 8.0p1-19.el8_8 | |
openssh-cavs-debuginfo | 8.0p1-19.el8_8 | |
openssh-clients-debuginfo | 8.0p1-19.el8_8 | |
openssh-debuginfo | 8.0p1-19.el8_8 | |
openssh-debugsource | 8.0p1-19.el8_8 | |
openssh-keycat-debuginfo | 8.0p1-19.el8_8 | |
openssh-ldap-debuginfo | 8.0p1-19.el8_8 | |
openssh-server-debuginfo | 8.0p1-19.el8_8 | |
pam_ssh_agent_auth-debuginfo | 0.10.3-7.19.el8_8 | |
python3-samba-dc-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-debuginfo | 4.17.5-3.el8_8 | |
samba-client-debuginfo | 4.17.5-3.el8_8 | |
samba-client-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-tools-debuginfo | 4.17.5-3.el8_8 | |
samba-dc-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-dcerpc-debuginfo | 4.17.5-3.el8_8 | |
samba-debuginfo | 4.17.5-3.el8_8 | |
samba-debugsource | 4.17.5-3.el8_8 | |
samba-krb5-printing-debuginfo | 4.17.5-3.el8_8 | |
samba-ldb-ldap-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-test-debuginfo | 4.17.5-3.el8_8 | |
samba-test-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-vfs-iouring | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-vfs-iouring-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-clients-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-krb5-locator-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-winexe-debuginfo | 4.17.5-3.el8_8 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.17.5-3.el8_8 | |
libnetapi-debuginfo | 4.17.5-3.el8_8 | |
libnetapi-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libsmbclient-debuginfo | 4.17.5-3.el8_8 | |
libsmbclient-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libwbclient-debuginfo | 4.17.5-3.el8_8 | |
libwbclient-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
python3-samba-dc-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client-debuginfo | 4.17.5-3.el8_8 | |
samba-client-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-tools-debuginfo | 4.17.5-3.el8_8 | |
samba-dc-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-dcerpc-debuginfo | 4.17.5-3.el8_8 | |
samba-debuginfo | 4.17.5-3.el8_8 | |
samba-debugsource | 4.17.5-3.el8_8 | |
samba-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-krb5-printing-debuginfo | 4.17.5-3.el8_8 | |
samba-ldb-ldap-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-test-debuginfo | 4.17.5-3.el8_8 | |
samba-test-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-vfs-iouring-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-clients-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-krb5-locator-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-winexe-debuginfo | 4.17.5-3.el8_8 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_477.15.1.el8_8.rh8.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
ctdb-debuginfo | 4.17.5-3.el8_8 | |
libnetapi | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libnetapi-debuginfo | 4.17.5-3.el8_8 | |
libsmbclient | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libsmbclient-debuginfo | 4.17.5-3.el8_8 | |
libwbclient | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libwbclient-debuginfo | 4.17.5-3.el8_8 | |
openssh | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-askpass-debuginfo | 8.0p1-19.el8_8 | |
openssh-cavs | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-cavs-debuginfo | 8.0p1-19.el8_8 | |
openssh-clients | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-clients-debuginfo | 8.0p1-19.el8_8 | |
openssh-debuginfo | 8.0p1-19.el8_8 | |
openssh-debugsource | 8.0p1-19.el8_8 | |
openssh-keycat | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-keycat-debuginfo | 8.0p1-19.el8_8 | |
openssh-ldap | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-ldap-debuginfo | 8.0p1-19.el8_8 | |
openssh-server | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-server-debuginfo | 8.0p1-19.el8_8 | |
pam_ssh_agent_auth | 0.10.3-7.19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
pam_ssh_agent_auth-debuginfo | 0.10.3-7.19.el8_8 | |
python3-samba | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
python3-samba-dc | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
python3-samba-dc-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-test | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client-debuginfo | 4.17.5-3.el8_8 | |
samba-client-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-common-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-common-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-tools | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-common-tools-debuginfo | 4.17.5-3.el8_8 | |
samba-dc-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-dc-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-dcerpc | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-dcerpc-debuginfo | 4.17.5-3.el8_8 | |
samba-debuginfo | 4.17.5-3.el8_8 | |
samba-debugsource | 4.17.5-3.el8_8 | |
samba-krb5-printing | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-krb5-printing-debuginfo | 4.17.5-3.el8_8 | |
samba-ldb-ldap-modules | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-ldb-ldap-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-pidl | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-test | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-test-debuginfo | 4.17.5-3.el8_8 | |
samba-test-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-test-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-tools | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-usershares | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-vfs-iouring-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-clients | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-clients-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-krb5-locator | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-krb5-locator-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-modules | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-modules-debuginfo | 4.17.5-3.el8_8 | |
sos | 4.5.5-2.el8 | [RHBA-2023:4279](https://access.redhat.com/errata/RHBA-2023:4279) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.5.5-2.el8 | [RHBA-2023:4279](https://access.redhat.com/errata/RHBA-2023:4279) | <div class="adv_b">Bug Fix Advisory</div>

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cjose | 0.6.1-3.module+el8.8.0+19464+578f4546 | [RHSA-2023:4418](https://access.redhat.com/errata/RHSA-2023:4418) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37464](https://access.redhat.com/security/cve/CVE-2023-37464))
cjose-debuginfo | 0.6.1-3.module+el8.8.0+19464+578f4546 | |
cjose-debugsource | 0.6.1-3.module+el8.8.0+19464+578f4546 | |
cjose-devel | 0.6.1-3.module+el8.8.0+19464+578f4546 | [RHSA-2023:4418](https://access.redhat.com/errata/RHSA-2023:4418) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37464](https://access.redhat.com/security/cve/CVE-2023-37464))
ctdb-debuginfo | 4.17.5-3.el8_8 | |
libnetapi-debuginfo | 4.17.5-3.el8_8 | |
libsmbclient-debuginfo | 4.17.5-3.el8_8 | |
libwbclient-debuginfo | 4.17.5-3.el8_8 | |
openssh-askpass | 8.0p1-19.el8_8 | [RHSA-2023:4419](https://access.redhat.com/errata/RHSA-2023:4419) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38408](https://access.redhat.com/security/cve/CVE-2023-38408))
openssh-askpass-debuginfo | 8.0p1-19.el8_8 | |
openssh-cavs-debuginfo | 8.0p1-19.el8_8 | |
openssh-clients-debuginfo | 8.0p1-19.el8_8 | |
openssh-debuginfo | 8.0p1-19.el8_8 | |
openssh-debugsource | 8.0p1-19.el8_8 | |
openssh-keycat-debuginfo | 8.0p1-19.el8_8 | |
openssh-ldap-debuginfo | 8.0p1-19.el8_8 | |
openssh-server-debuginfo | 8.0p1-19.el8_8 | |
pam_ssh_agent_auth-debuginfo | 0.10.3-7.19.el8_8 | |
python3-samba-dc-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-debuginfo | 4.17.5-3.el8_8 | |
samba-client-debuginfo | 4.17.5-3.el8_8 | |
samba-client-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-tools-debuginfo | 4.17.5-3.el8_8 | |
samba-dc-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-dcerpc-debuginfo | 4.17.5-3.el8_8 | |
samba-debuginfo | 4.17.5-3.el8_8 | |
samba-debugsource | 4.17.5-3.el8_8 | |
samba-krb5-printing-debuginfo | 4.17.5-3.el8_8 | |
samba-ldb-ldap-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-test-debuginfo | 4.17.5-3.el8_8 | |
samba-test-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-vfs-iouring | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-vfs-iouring-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-clients-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-krb5-locator-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-modules-debuginfo | 4.17.5-3.el8_8 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.17.5-3.el8_8 | |
libnetapi-debuginfo | 4.17.5-3.el8_8 | |
libnetapi-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libsmbclient-debuginfo | 4.17.5-3.el8_8 | |
libsmbclient-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libwbclient-debuginfo | 4.17.5-3.el8_8 | |
libwbclient-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
python3-samba-dc-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client-debuginfo | 4.17.5-3.el8_8 | |
samba-client-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-tools-debuginfo | 4.17.5-3.el8_8 | |
samba-dc-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-dcerpc-debuginfo | 4.17.5-3.el8_8 | |
samba-debuginfo | 4.17.5-3.el8_8 | |
samba-debugsource | 4.17.5-3.el8_8 | |
samba-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-krb5-printing-debuginfo | 4.17.5-3.el8_8 | |
samba-ldb-ldap-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-test-debuginfo | 4.17.5-3.el8_8 | |
samba-test-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-vfs-iouring-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-clients-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-krb5-locator-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-modules-debuginfo | 4.17.5-3.el8_8 | |

