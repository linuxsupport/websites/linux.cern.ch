## 2024-03-20

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.28-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.28-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0 | 6.0.28-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-hostfxr-6.0 | 6.0.28-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-runtime-6.0 | 6.0.28-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-sdk-6.0 | 6.0.128-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.128-1.el8_9 | |
dotnet-targeting-pack-6.0 | 6.0.28-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.128-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.128-1.el8_9 | |
dotnet6.0-debugsource | 6.0.128-1.el8_9 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-runtime-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-sdk-6.0-debuginfo | 6.0.128-1.el8_9 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.128-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.128-1.el8_9 | |
dotnet6.0-debugsource | 6.0.128-1.el8_9 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.28-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.28-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0 | 6.0.28-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-hostfxr-6.0 | 6.0.28-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-runtime-6.0 | 6.0.28-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-sdk-6.0 | 6.0.128-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.128-1.el8_9 | |
dotnet-targeting-pack-6.0 | 6.0.28-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.128-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.128-1.el8_9 | |
dotnet6.0-debugsource | 6.0.128-1.el8_9 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-runtime-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-sdk-6.0-debuginfo | 6.0.128-1.el8_9 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.128-1.el8_9 | [RHBA-2024:1298](https://access.redhat.com/errata/RHBA-2024:1298) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.128-1.el8_9 | |
dotnet6.0-debugsource | 6.0.128-1.el8_9 | |

