## 2023-11-15

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-linuxsupport-access | 1.10-1.rh8.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
binutils | 2.30-119.el8_8.2 | [RHSA-2023:6236](https://access.redhat.com/errata/RHSA-2023:6236) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4285](https://access.redhat.com/security/cve/CVE-2022-4285))
binutils-debuginfo | 2.30-119.el8_8.2 | |
binutils-debugsource | 2.30-119.el8_8.2 | |
sos | 4.6.0-5.el8 | [RHBA-2023:6293](https://access.redhat.com/errata/RHBA-2023:6293) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.6.0-5.el8 | [RHBA-2023:6293](https://access.redhat.com/errata/RHBA-2023:6293) | <div class="adv_b">Bug Fix Advisory</div>

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.24-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
aspnetcore-runtime-7.0 | 7.0.13-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
aspnetcore-targeting-pack-6.0 | 6.0.24-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
aspnetcore-targeting-pack-7.0 | 7.0.13-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
binutils-debuginfo | 2.30-119.el8_8.2 | |
binutils-debugsource | 2.30-119.el8_8.2 | |
binutils-devel | 2.30-119.el8_8.2 | [RHSA-2023:6236](https://access.redhat.com/errata/RHSA-2023:6236) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4285](https://access.redhat.com/security/cve/CVE-2022-4285))
dotnet | 7.0.113-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-apphost-pack-6.0 | 6.0.24-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-apphost-pack-6.0-debuginfo | 6.0.24-1.el8_8 | |
dotnet-apphost-pack-7.0 | 7.0.13-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-apphost-pack-7.0-debuginfo | 7.0.13-1.el8_8 | |
dotnet-host | 7.0.13-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-host-debuginfo | 7.0.13-1.el8_8 | |
dotnet-hostfxr-6.0 | 6.0.24-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-hostfxr-6.0-debuginfo | 6.0.24-1.el8_8 | |
dotnet-hostfxr-7.0 | 7.0.13-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-hostfxr-7.0-debuginfo | 7.0.13-1.el8_8 | |
dotnet-runtime-6.0 | 6.0.24-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-runtime-6.0-debuginfo | 6.0.24-1.el8_8 | |
dotnet-runtime-7.0 | 7.0.13-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-runtime-7.0-debuginfo | 7.0.13-1.el8_8 | |
dotnet-sdk-6.0 | 6.0.124-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-sdk-6.0-debuginfo | 6.0.124-1.el8_8 | |
dotnet-sdk-7.0 | 7.0.113-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-sdk-7.0-debuginfo | 7.0.113-1.el8_8 | |
dotnet-targeting-pack-6.0 | 6.0.24-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-targeting-pack-7.0 | 7.0.13-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-templates-6.0 | 6.0.124-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-templates-7.0 | 7.0.113-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet6.0-debuginfo | 6.0.124-1.el8_8 | |
dotnet6.0-debugsource | 6.0.124-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.113-1.el8_8 | |
dotnet7.0-debugsource | 7.0.113-1.el8_8 | |
insights-client | 3.2.2-1.el8_8 | [RHSA-2023:6283](https://access.redhat.com/errata/RHSA-2023:6283) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3972](https://access.redhat.com/security/cve/CVE-2023-3972))
netstandard-targeting-pack-2.1 | 7.0.113-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
squid | 4.15-6.module+el8.8.0+20569+26050b0f.1 | [RHSA-2023:6267](https://access.redhat.com/errata/RHSA-2023:6267) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46846](https://access.redhat.com/security/cve/CVE-2023-46846), [CVE-2023-46847](https://access.redhat.com/security/cve/CVE-2023-46847))
squid-debuginfo | 4.15-6.module+el8.8.0+20569+26050b0f.1 | |
squid-debugsource | 4.15-6.module+el8.8.0+20569+26050b0f.1 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.24-1.el8_8 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.13-1.el8_8 | |
dotnet-host-debuginfo | 7.0.13-1.el8_8 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.24-1.el8_8 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.13-1.el8_8 | |
dotnet-runtime-6.0-debuginfo | 6.0.24-1.el8_8 | |
dotnet-runtime-7.0-debuginfo | 7.0.13-1.el8_8 | |
dotnet-sdk-6.0-debuginfo | 6.0.124-1.el8_8 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.124-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-sdk-7.0-debuginfo | 7.0.113-1.el8_8 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.113-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet6.0-debuginfo | 6.0.124-1.el8_8 | |
dotnet6.0-debugsource | 6.0.124-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.113-1.el8_8 | |
dotnet7.0-debugsource | 7.0.113-1.el8_8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-linuxsupport-access | 1.10-1.rh8.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
binutils | 2.30-119.el8_8.2 | [RHSA-2023:6236](https://access.redhat.com/errata/RHSA-2023:6236) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4285](https://access.redhat.com/security/cve/CVE-2022-4285))
binutils-debuginfo | 2.30-119.el8_8.2 | |
binutils-debugsource | 2.30-119.el8_8.2 | |
sos | 4.6.0-5.el8 | [RHBA-2023:6293](https://access.redhat.com/errata/RHBA-2023:6293) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.6.0-5.el8 | [RHBA-2023:6293](https://access.redhat.com/errata/RHBA-2023:6293) | <div class="adv_b">Bug Fix Advisory</div>

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.24-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
aspnetcore-runtime-7.0 | 7.0.13-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
aspnetcore-targeting-pack-6.0 | 6.0.24-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
aspnetcore-targeting-pack-7.0 | 7.0.13-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
binutils-debuginfo | 2.30-119.el8_8.2 | |
binutils-debugsource | 2.30-119.el8_8.2 | |
binutils-devel | 2.30-119.el8_8.2 | [RHSA-2023:6236](https://access.redhat.com/errata/RHSA-2023:6236) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4285](https://access.redhat.com/security/cve/CVE-2022-4285))
dotnet | 7.0.113-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-apphost-pack-6.0 | 6.0.24-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-apphost-pack-6.0-debuginfo | 6.0.24-1.el8_8 | |
dotnet-apphost-pack-7.0 | 7.0.13-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-apphost-pack-7.0-debuginfo | 7.0.13-1.el8_8 | |
dotnet-host | 7.0.13-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-host-debuginfo | 7.0.13-1.el8_8 | |
dotnet-hostfxr-6.0 | 6.0.24-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-hostfxr-6.0-debuginfo | 6.0.24-1.el8_8 | |
dotnet-hostfxr-7.0 | 7.0.13-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-hostfxr-7.0-debuginfo | 7.0.13-1.el8_8 | |
dotnet-runtime-6.0 | 6.0.24-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-runtime-6.0-debuginfo | 6.0.24-1.el8_8 | |
dotnet-runtime-7.0 | 7.0.13-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-runtime-7.0-debuginfo | 7.0.13-1.el8_8 | |
dotnet-sdk-6.0 | 6.0.124-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-sdk-6.0-debuginfo | 6.0.124-1.el8_8 | |
dotnet-sdk-7.0 | 7.0.113-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-sdk-7.0-debuginfo | 7.0.113-1.el8_8 | |
dotnet-targeting-pack-6.0 | 6.0.24-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-targeting-pack-7.0 | 7.0.13-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-templates-6.0 | 6.0.124-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-templates-7.0 | 7.0.113-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet6.0-debuginfo | 6.0.124-1.el8_8 | |
dotnet6.0-debugsource | 6.0.124-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.113-1.el8_8 | |
dotnet7.0-debugsource | 7.0.113-1.el8_8 | |
insights-client | 3.2.2-1.el8_8 | [RHSA-2023:6283](https://access.redhat.com/errata/RHSA-2023:6283) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3972](https://access.redhat.com/security/cve/CVE-2023-3972))
netstandard-targeting-pack-2.1 | 7.0.113-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
squid | 4.15-6.module+el8.8.0+20569+26050b0f.1 | [RHSA-2023:6267](https://access.redhat.com/errata/RHSA-2023:6267) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46846](https://access.redhat.com/security/cve/CVE-2023-46846), [CVE-2023-46847](https://access.redhat.com/security/cve/CVE-2023-46847))
squid-debuginfo | 4.15-6.module+el8.8.0+20569+26050b0f.1 | |
squid-debugsource | 4.15-6.module+el8.8.0+20569+26050b0f.1 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.24-1.el8_8 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.13-1.el8_8 | |
dotnet-host-debuginfo | 7.0.13-1.el8_8 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.24-1.el8_8 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.13-1.el8_8 | |
dotnet-runtime-6.0-debuginfo | 6.0.24-1.el8_8 | |
dotnet-runtime-7.0-debuginfo | 7.0.13-1.el8_8 | |
dotnet-sdk-6.0-debuginfo | 6.0.124-1.el8_8 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.124-1.el8_8 | [RHSA-2023:6245](https://access.redhat.com/errata/RHSA-2023:6245) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-sdk-7.0-debuginfo | 7.0.113-1.el8_8 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.113-1.el8_8 | [RHSA-2023:6247](https://access.redhat.com/errata/RHSA-2023:6247) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet6.0-debuginfo | 6.0.124-1.el8_8 | |
dotnet6.0-debugsource | 6.0.124-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.113-1.el8_8 | |
dotnet7.0-debugsource | 7.0.113-1.el8_8 | |

