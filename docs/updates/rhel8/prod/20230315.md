## 2023-03-15

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.5-2.rh8.cern | |
cern-sssd-conf-domain-cernch | 1.5-2.rh8.cern | |
cern-sssd-conf-global | 1.5-2.rh8.cern | |
cern-sssd-conf-global-cernch | 1.5-2.rh8.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.5-2.rh8.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
curl | 7.61.1-25.el8_7.3 | |
curl-debuginfo | 7.61.1-25.el8_7.3 | |
curl-debugsource | 7.61.1-25.el8_7.3 | |
curl-minimal-debuginfo | 7.61.1-25.el8_7.3 | |
libcurl | 7.61.1-25.el8_7.3 | |
libcurl-debuginfo | 7.61.1-25.el8_7.3 | |
libcurl-devel | 7.61.1-25.el8_7.3 | |
libcurl-minimal | 7.61.1-25.el8_7.3 | |
libcurl-minimal-debuginfo | 7.61.1-25.el8_7.3 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.5-2.rh8.cern | |
cern-sssd-conf-domain-cernch | 1.5-2.rh8.cern | |
cern-sssd-conf-global | 1.5-2.rh8.cern | |
cern-sssd-conf-global-cernch | 1.5-2.rh8.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.5-2.rh8.cern | |

