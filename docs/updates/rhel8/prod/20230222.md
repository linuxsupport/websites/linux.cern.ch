## 2023-02-22

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.4-1.rh8.cern | |
pyphonebook | 2.1.5-1.rh8.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9.0-2.rh8.cern | |
kmod-openafs | 1.8.9.0-2.4.18.0_425.10.1.el8_7.rh8.cern | |
openafs | 1.8.9.0-2.rh8.cern | |
openafs-authlibs | 1.8.9.0-2.rh8.cern | |
openafs-authlibs-devel | 1.8.9.0-2.rh8.cern | |
openafs-client | 1.8.9.0-2.rh8.cern | |
openafs-compat | 1.8.9.0-2.rh8.cern | |
openafs-debugsource | 1.8.9.0-2.rh8.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_425.10.1.el8_7-2.rh8.cern | |
openafs-devel | 1.8.9.0-2.rh8.cern | |
openafs-docs | 1.8.9.0-2.rh8.cern | |
openafs-kernel-source | 1.8.9.0-2.rh8.cern | |
openafs-krb5 | 1.8.9.0-2.rh8.cern | |
openafs-server | 1.8.9.0-2.rh8.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-redhat-oracleasm | 2.0.8-15.1.el8_7 | |
kmod-redhat-oracleasm-debugsource | 2.0.8-15.1.el8_7 | |
kmod-redhat-oracleasm-kernel_4_18_0_425_10_1 | 2.0.8-15.1.el8_7 | |
kmod-redhat-oracleasm-kernel_4_18_0_425_10_1-debuginfo | 2.0.8-15.1.el8_7 | |
kmod-redhat-oracleasm-kernel_4_18_0_425_3_1 | 2.0.8-15.1.el8_7 | |
kmod-redhat-oracleasm-kernel_4_18_0_425_3_1-debuginfo | 2.0.8-15.1.el8_7 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rhc | 0.2.1-12.el8_7 | |
rhc-debuginfo | 0.2.1-12.el8_7 | |
rhc-debugsource | 0.2.1-12.el8_7 | |
tigervnc | 1.12.0-9.el8_7.1 | |
tigervnc-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-debugsource | 1.12.0-9.el8_7.1 | |
tigervnc-icons | 1.12.0-9.el8_7.1 | |
tigervnc-license | 1.12.0-9.el8_7.1 | |
tigervnc-selinux | 1.12.0-9.el8_7.1 | |
tigervnc-server | 1.12.0-9.el8_7.1 | |
tigervnc-server-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-module | 1.12.0-9.el8_7.1 | |
tigervnc-server-module-debuginfo | 1.12.0-9.el8_7.1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.4-1.rh8.cern | |
pyphonebook | 2.1.5-1.rh8.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9.0-2.rh8.cern | |
kmod-openafs | 1.8.9.0-2.4.18.0_425.10.1.el8_7.rh8.cern | |
openafs | 1.8.9.0-2.rh8.cern | |
openafs-authlibs | 1.8.9.0-2.rh8.cern | |
openafs-authlibs-devel | 1.8.9.0-2.rh8.cern | |
openafs-client | 1.8.9.0-2.rh8.cern | |
openafs-compat | 1.8.9.0-2.rh8.cern | |
openafs-debugsource | 1.8.9.0-2.rh8.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_425.10.1.el8_7-2.rh8.cern | |
openafs-devel | 1.8.9.0-2.rh8.cern | |
openafs-docs | 1.8.9.0-2.rh8.cern | |
openafs-kernel-source | 1.8.9.0-2.rh8.cern | |
openafs-krb5 | 1.8.9.0-2.rh8.cern | |
openafs-server | 1.8.9.0-2.rh8.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rhc | 0.2.1-12.el8_7 | |
rhc-debuginfo | 0.2.1-12.el8_7 | |
rhc-debugsource | 0.2.1-12.el8_7 | |
tigervnc | 1.12.0-9.el8_7.1 | |
tigervnc-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-debugsource | 1.12.0-9.el8_7.1 | |
tigervnc-icons | 1.12.0-9.el8_7.1 | |
tigervnc-license | 1.12.0-9.el8_7.1 | |
tigervnc-selinux | 1.12.0-9.el8_7.1 | |
tigervnc-server | 1.12.0-9.el8_7.1 | |
tigervnc-server-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-module | 1.12.0-9.el8_7.1 | |
tigervnc-server-module-debuginfo | 1.12.0-9.el8_7.1 | |

