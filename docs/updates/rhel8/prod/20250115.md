## 2025-01-15

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_553.34.1.el8_10.rh8.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
bpftool-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-abi-stablelists | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-core | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-cross-headers | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-debug | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-debug-core | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-debug-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel-debug-devel | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-debug-modules | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-debug-modules-extra | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel-debuginfo-common-x86_64 | 4.18.0-553.34.1.el8_10 | |
kernel-devel | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-doc | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-headers | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-modules | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-modules-extra | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-tools | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-tools-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel-tools-libs | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
perf | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
perf-debuginfo | 4.18.0-553.34.1.el8_10 | |
python3-perf | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
python3-perf-debuginfo | 4.18.0-553.34.1.el8_10 | |
python3-requests | 2.20.0-5.el8_10 | [RHSA-2025:0012](https://access.redhat.com/errata/RHSA-2025:0012) | <div class="adv_s">Security Advisory</div> ([CVE-2024-35195](https://access.redhat.com/security/cve/CVE-2024-35195))

### rt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-553.34.1.rt7.375.el8_10 | [RHSA-2025:0066](https://access.redhat.com/errata/RHSA-2025:0066) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-rt-core | 4.18.0-553.34.1.rt7.375.el8_10 | [RHSA-2025:0066](https://access.redhat.com/errata/RHSA-2025:0066) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-rt-debug | 4.18.0-553.34.1.rt7.375.el8_10 | [RHSA-2025:0066](https://access.redhat.com/errata/RHSA-2025:0066) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-rt-debug-core | 4.18.0-553.34.1.rt7.375.el8_10 | [RHSA-2025:0066](https://access.redhat.com/errata/RHSA-2025:0066) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-rt-debug-debuginfo | 4.18.0-553.34.1.rt7.375.el8_10 | |
kernel-rt-debug-devel | 4.18.0-553.34.1.rt7.375.el8_10 | [RHSA-2025:0066](https://access.redhat.com/errata/RHSA-2025:0066) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-rt-debug-modules | 4.18.0-553.34.1.rt7.375.el8_10 | [RHSA-2025:0066](https://access.redhat.com/errata/RHSA-2025:0066) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-rt-debug-modules-extra | 4.18.0-553.34.1.rt7.375.el8_10 | [RHSA-2025:0066](https://access.redhat.com/errata/RHSA-2025:0066) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-rt-debuginfo | 4.18.0-553.34.1.rt7.375.el8_10 | |
kernel-rt-debuginfo-common-x86_64 | 4.18.0-553.34.1.rt7.375.el8_10 | |
kernel-rt-devel | 4.18.0-553.34.1.rt7.375.el8_10 | [RHSA-2025:0066](https://access.redhat.com/errata/RHSA-2025:0066) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-rt-modules | 4.18.0-553.34.1.rt7.375.el8_10 | [RHSA-2025:0066](https://access.redhat.com/errata/RHSA-2025:0066) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-rt-modules-extra | 4.18.0-553.34.1.rt7.375.el8_10 | [RHSA-2025:0066](https://access.redhat.com/errata/RHSA-2025:0066) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel-debug-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel-debuginfo-common-x86_64 | 4.18.0-553.34.1.el8_10 | |
kernel-tools-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel-tools-libs-devel | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
perf-debuginfo | 4.18.0-553.34.1.el8_10 | |
python3-perf-debuginfo | 4.18.0-553.34.1.el8_10 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_553.34.1.el8_10.rh8.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
bpftool-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-abi-stablelists | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-core | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-cross-headers | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-debug | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-debug-core | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-debug-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel-debug-devel | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-debug-modules | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-debug-modules-extra | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel-debuginfo-common-aarch64 | 4.18.0-553.34.1.el8_10 | |
kernel-devel | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-doc | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-headers | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-modules | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-modules-extra | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-tools | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
kernel-tools-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel-tools-libs | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
perf | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
perf-debuginfo | 4.18.0-553.34.1.el8_10 | |
python3-perf | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
python3-perf-debuginfo | 4.18.0-553.34.1.el8_10 | |
python3-requests | 2.20.0-5.el8_10 | [RHSA-2025:0012](https://access.redhat.com/errata/RHSA-2025:0012) | <div class="adv_s">Security Advisory</div> ([CVE-2024-35195](https://access.redhat.com/security/cve/CVE-2024-35195))

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel-debug-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel-debuginfo-common-aarch64 | 4.18.0-553.34.1.el8_10 | |
kernel-tools-debuginfo | 4.18.0-553.34.1.el8_10 | |
kernel-tools-libs-devel | 4.18.0-553.34.1.el8_10 | [RHSA-2025:0065](https://access.redhat.com/errata/RHSA-2025:0065) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53088](https://access.redhat.com/security/cve/CVE-2024-53088), [CVE-2024-53122](https://access.redhat.com/security/cve/CVE-2024-53122))
perf-debuginfo | 4.18.0-553.34.1.el8_10 | |
python3-perf-debuginfo | 4.18.0-553.34.1.el8_10 | |

