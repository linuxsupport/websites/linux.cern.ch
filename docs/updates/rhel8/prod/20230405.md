## 2023-04-05

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssl | 1.1.1k-9.el8_7 | |
openssl-debuginfo | 1.1.1k-9.el8_7 | |
openssl-debugsource | 1.1.1k-9.el8_7 | |
openssl-devel | 1.1.1k-9.el8_7 | |
openssl-libs | 1.1.1k-9.el8_7 | |
openssl-libs-debuginfo | 1.1.1k-9.el8_7 | |
openssl-perl | 1.1.1k-9.el8_7 | |
tzdata | 2023b-1.el8 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.9.0-1.el8_7 | |
thunderbird-debuginfo | 102.9.0-1.el8_7 | |
thunderbird-debugsource | 102.9.0-1.el8_7 | |
tzdata-java | 2023b-1.el8 | |

