## 2023-05-18

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 1.0.0-1.al8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 1.0.0-1.al8.cern | |

