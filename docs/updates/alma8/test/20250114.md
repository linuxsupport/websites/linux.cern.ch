## 2025-01-14

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
NetworkManager | 1.40.16-18.el8_10 | |
NetworkManager-adsl | 1.40.16-18.el8_10 | |
NetworkManager-adsl-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-bluetooth | 1.40.16-18.el8_10 | |
NetworkManager-bluetooth-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-cloud-setup-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-config-connectivity-redhat | 1.40.16-18.el8_10 | |
NetworkManager-config-server | 1.40.16-18.el8_10 | |
NetworkManager-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-debugsource | 1.40.16-18.el8_10 | |
NetworkManager-dispatcher-routing-rules | 1.40.16-18.el8_10 | |
NetworkManager-initscripts-updown | 1.40.16-18.el8_10 | |
NetworkManager-libnm | 1.40.16-18.el8_10 | |
NetworkManager-libnm-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-ovs | 1.40.16-18.el8_10 | |
NetworkManager-ovs-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-ppp | 1.40.16-18.el8_10 | |
NetworkManager-ppp-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-team | 1.40.16-18.el8_10 | |
NetworkManager-team-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-tui | 1.40.16-18.el8_10 | |
NetworkManager-tui-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-wifi | 1.40.16-18.el8_10 | |
NetworkManager-wifi-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-wwan | 1.40.16-18.el8_10 | |
NetworkManager-wwan-debuginfo | 1.40.16-18.el8_10 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
NetworkManager-cloud-setup | 1.40.16-18.el8_10 | |
thunderbird | 128.6.0-3.el8_10.alma.1 | |
thunderbird-debuginfo | 128.6.0-3.el8_10.alma.1 | |
thunderbird-debugsource | 128.6.0-3.el8_10.alma.1 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
NetworkManager-libnm-devel | 1.40.16-18.el8_10 | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
elrepo-release | 8.4-2.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
NetworkManager | 1.40.16-18.el8_10 | |
NetworkManager-adsl | 1.40.16-18.el8_10 | |
NetworkManager-adsl-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-bluetooth | 1.40.16-18.el8_10 | |
NetworkManager-bluetooth-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-cloud-setup-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-config-connectivity-redhat | 1.40.16-18.el8_10 | |
NetworkManager-config-server | 1.40.16-18.el8_10 | |
NetworkManager-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-debugsource | 1.40.16-18.el8_10 | |
NetworkManager-dispatcher-routing-rules | 1.40.16-18.el8_10 | |
NetworkManager-initscripts-updown | 1.40.16-18.el8_10 | |
NetworkManager-libnm | 1.40.16-18.el8_10 | |
NetworkManager-libnm-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-ovs | 1.40.16-18.el8_10 | |
NetworkManager-ovs-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-ppp | 1.40.16-18.el8_10 | |
NetworkManager-ppp-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-team | 1.40.16-18.el8_10 | |
NetworkManager-team-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-tui | 1.40.16-18.el8_10 | |
NetworkManager-tui-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-wifi | 1.40.16-18.el8_10 | |
NetworkManager-wifi-debuginfo | 1.40.16-18.el8_10 | |
NetworkManager-wwan | 1.40.16-18.el8_10 | |
NetworkManager-wwan-debuginfo | 1.40.16-18.el8_10 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dpdk | 23.11-2.el8_10 | |
dpdk-devel | 23.11-2.el8_10 | [ALSA-2025:0222](https://errata.almalinux.org/8/ALSA-2025-0222.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11614](https://access.redhat.com/security/cve/CVE-2024-11614))
dpdk-doc | 23.11-2.el8_10 | |
dpdk-tools | 23.11-2.el8_10 | |
firefox | 128.6.0-1.el8_10 | |
iperf3 | 3.5-11.el8_10 | |
NetworkManager-cloud-setup | 1.40.16-18.el8_10 | |
thunderbird | 128.6.0-3.el8_10.alma.1 | |
thunderbird-debuginfo | 128.6.0-3.el8_10.alma.1 | |
thunderbird-debugsource | 128.6.0-3.el8_10.alma.1 | |
webkit2gtk3 | 2.46.5-1.el8_10 | |
webkit2gtk3-devel | 2.46.5-1.el8_10 | |
webkit2gtk3-jsc | 2.46.5-1.el8_10 | |
webkit2gtk3-jsc-devel | 2.46.5-1.el8_10 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
NetworkManager-libnm-devel | 1.40.16-18.el8_10 | |

