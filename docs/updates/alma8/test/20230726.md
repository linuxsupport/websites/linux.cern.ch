## 2023-07-26

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.10-0.al8.cern | |
kmod-openafs | 1.8.10-0.4.18.0_477.15.1.el8_8.al8.cern | |
openafs | 1.8.10-0.al8.cern | |
openafs-authlibs | 1.8.10-0.al8.cern | |
openafs-authlibs-devel | 1.8.10-0.al8.cern | |
openafs-client | 1.8.10-0.al8.cern | |
openafs-compat | 1.8.10-0.al8.cern | |
openafs-devel | 1.8.10-0.al8.cern | |
openafs-docs | 1.8.10-0.al8.cern | |
openafs-kernel-source | 1.8.10-0.al8.cern | |
openafs-krb5 | 1.8.10-0.al8.cern | |
openafs-server | 1.8.10-0.al8.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
bpftool-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-abi-stablelists | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-core | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-cross-headers | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-debug | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-debug-core | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-debug-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel-debug-devel | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-debug-modules | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-debug-modules-extra | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel-debuginfo-common-x86_64 | 4.18.0-477.15.1.el8_8 | |
kernel-devel | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-doc | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-headers | 4.18.0-477.15.1.el8_8 | |
kernel-modules | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-modules-extra | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-core | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-tools | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-tools-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel-tools-libs | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
perf | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
perf-debuginfo | 4.18.0-477.15.1.el8_8 | |
python3-perf | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
python3-perf-debuginfo | 4.18.0-477.15.1.el8_8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-appstream-debuginfo | 286.2-1.el8_8.alma | |
cockpit-appstream-debugsource | 286.2-1.el8_8.alma | |
cockpit-machines | 286.2-1.el8_8.alma | |
cockpit-packagekit | 286.2-1.el8_8.alma | |
cockpit-pcp | 286.2-1.el8_8.alma | |
cockpit-storaged | 286.2-1.el8_8.alma | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-debug | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-debug-core | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-debug-debuginfo | 4.18.0-477.15.1.rt7.278.el8_8 | |
kernel-rt-debug-devel | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-debug-modules | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-debug-modules-extra | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-debuginfo | 4.18.0-477.15.1.rt7.278.el8_8 | |
kernel-rt-debuginfo-common-x86_64 | 4.18.0-477.15.1.rt7.278.el8_8 | |
kernel-rt-devel | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-modules | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-modules-extra | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel-debug-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel-debuginfo-common-x86_64 | 4.18.0-477.15.1.el8_8 | |
kernel-tools-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel-tools-libs-devel | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
perf-debuginfo | 4.18.0-477.15.1.el8_8 | |
python3-perf-debuginfo | 4.18.0-477.15.1.el8_8 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-debug | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-debug-core | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-debug-debuginfo | 4.18.0-477.15.1.rt7.278.el8_8 | |
kernel-rt-debug-devel | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-debug-kvm | 4.18.0-477.15.1.rt7.278.el8_8 | |
kernel-rt-debug-modules | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-debug-modules-extra | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-debuginfo | 4.18.0-477.15.1.rt7.278.el8_8 | |
kernel-rt-debuginfo-common-x86_64 | 4.18.0-477.15.1.rt7.278.el8_8 | |
kernel-rt-devel | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-kvm | 4.18.0-477.15.1.rt7.278.el8_8 | |
kernel-rt-modules | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-rt-modules-extra | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-tests | 286.2-1.el8_8.alma | |
kernel-debug-modules-internal | 4.18.0-477.15.1.el8_8 | |
kernel-ipaclones-internal | 4.18.0-477.15.1.el8_8 | |
kernel-modules-internal | 4.18.0-477.15.1.el8_8 | |
kernel-rt-debug-modules-internal | 4.18.0-477.15.1.rt7.278.el8_8 | |
kernel-rt-modules-internal | 4.18.0-477.15.1.rt7.278.el8_8 | |
kernel-rt-selftests-internal | 4.18.0-477.15.1.rt7.278.el8_8 | |
kernel-selftests-internal | 4.18.0-477.15.1.el8_8 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.10-0.al8.cern | |
openafs | 1.8.10-0.al8.cern | |
openafs-authlibs | 1.8.10-0.al8.cern | |
openafs-authlibs-devel | 1.8.10-0.al8.cern | |
openafs-client | 1.8.10-0.al8.cern | |
openafs-compat | 1.8.10-0.al8.cern | |
openafs-devel | 1.8.10-0.al8.cern | |
openafs-docs | 1.8.10-0.al8.cern | |
openafs-kernel-source | 1.8.10-0.al8.cern | |
openafs-krb5 | 1.8.10-0.al8.cern | |
openafs-server | 1.8.10-0.al8.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
bpftool-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-abi-stablelists | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-core | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-cross-headers | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-debug | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-debug-core | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-debug-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel-debug-devel | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-debug-modules | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-debug-modules-extra | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel-debuginfo-common-aarch64 | 4.18.0-477.15.1.el8_8 | |
kernel-devel | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-doc | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-headers | 4.18.0-477.15.1.el8_8 | |
kernel-modules | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-modules-extra | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-tools | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
kernel-tools-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel-tools-libs | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
perf | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
perf-debuginfo | 4.18.0-477.15.1.el8_8 | |
python3-perf | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
python3-perf-debuginfo | 4.18.0-477.15.1.el8_8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-appstream-debuginfo | 286.2-1.el8_8.alma | |
cockpit-appstream-debugsource | 286.2-1.el8_8.alma | |
cockpit-machines | 286.2-1.el8_8.alma | |
cockpit-packagekit | 286.2-1.el8_8.alma | |
cockpit-pcp | 286.2-1.el8_8.alma | |
cockpit-storaged | 286.2-1.el8_8.alma | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel-debug-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel-debuginfo-common-aarch64 | 4.18.0-477.15.1.el8_8 | |
kernel-tools-debuginfo | 4.18.0-477.15.1.el8_8 | |
kernel-tools-libs-devel | 4.18.0-477.15.1.el8_8 | [ALSA-2023:3847](https://errata.almalinux.org/8/ALSA-2023-3847.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))
perf-debuginfo | 4.18.0-477.15.1.el8_8 | |
python3-perf-debuginfo | 4.18.0-477.15.1.el8_8 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-tests | 286.2-1.el8_8.alma | |
kernel-debug-modules-internal | 4.18.0-477.15.1.el8_8 | |
kernel-modules-internal | 4.18.0-477.15.1.el8_8 | |
kernel-selftests-internal | 4.18.0-477.15.1.el8_8 | |

