## 2023-01-12

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-sso-cookie | 0.6-3.al8.cern | |
perl-WWW-CERNSSO-Auth | 0.6-3.al8.cern | |
perl-WWW-Curl | 4.17-20.al8.cern | |
perl-WWW-Curl-debugsource | 4.17-20.al8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-sso-cookie | 0.6-3.al8.cern | |
perl-WWW-CERNSSO-Auth | 0.6-3.al8.cern | |
perl-WWW-Curl | 4.17-20.al8.cern | |
perl-WWW-Curl-debugsource | 4.17-20.al8.cern | |

