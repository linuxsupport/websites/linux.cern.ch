## 2022-12-14

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9pre2-3.al8.cern | |
kmod-openafs | 1.8.9pre2-3.4.18.0_425.3.1.el8.al8.cern | |
openafs | 1.8.9pre2-3.al8.cern | |
openafs-authlibs | 1.8.9pre2-3.al8.cern | |
openafs-authlibs-devel | 1.8.9pre2-3.al8.cern | |
openafs-client | 1.8.9pre2-3.al8.cern | |
openafs-compat | 1.8.9pre2-3.al8.cern | |
openafs-debugsource | 1.8.9pre2-3.al8.cern | |
openafs-debugsource | 1.8.9_4.18.0_425.3.1.el8pre2-3.al8.cern | |
openafs-devel | 1.8.9pre2-3.al8.cern | |
openafs-docs | 1.8.9pre2-3.al8.cern | |
openafs-kernel-source | 1.8.9pre2-3.al8.cern | |
openafs-krb5 | 1.8.9pre2-3.al8.cern | |
openafs-server | 1.8.9pre2-3.al8.cern | |
pam_afs_session | 2.6-3.al8.cern | |
pam_afs_session-debugsource | 2.6-3.al8.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9pre2-3.al8.cern | |
kmod-openafs | 1.8.9pre2-3.4.18.0_425.3.1.el8.al8.cern | |
openafs | 1.8.9pre2-3.al8.cern | |
openafs-authlibs | 1.8.9pre2-3.al8.cern | |
openafs-authlibs-devel | 1.8.9pre2-3.al8.cern | |
openafs-client | 1.8.9pre2-3.al8.cern | |
openafs-compat | 1.8.9pre2-3.al8.cern | |
openafs-debugsource | 1.8.9pre2-3.al8.cern | |
openafs-debugsource | 1.8.9_4.18.0_425.3.1.el8pre2-3.al8.cern | |
openafs-devel | 1.8.9pre2-3.al8.cern | |
openafs-docs | 1.8.9pre2-3.al8.cern | |
openafs-kernel-source | 1.8.9pre2-3.al8.cern | |
openafs-krb5 | 1.8.9pre2-3.al8.cern | |
openafs-server | 1.8.9pre2-3.al8.cern | |
pam_afs_session | 2.6-3.al8.cern | |
pam_afs_session-debugsource | 2.6-3.al8.cern | |

