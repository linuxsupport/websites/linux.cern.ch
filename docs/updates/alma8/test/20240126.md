## 2024-01-26

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.9-0.al8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.9-0.al8.cern | |

