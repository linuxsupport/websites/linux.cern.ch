## 2025-03-06

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh | 8.0p1-25.el8_10.alma.1 | |
openssh-askpass-debuginfo | 8.0p1-25.el8_10.alma.1 | |
openssh-cavs | 8.0p1-25.el8_10.alma.1 | |
openssh-cavs-debuginfo | 8.0p1-25.el8_10.alma.1 | |
openssh-clients | 8.0p1-25.el8_10.alma.1 | |
openssh-clients-debuginfo | 8.0p1-25.el8_10.alma.1 | |
openssh-debuginfo | 8.0p1-25.el8_10.alma.1 | |
openssh-debugsource | 8.0p1-25.el8_10.alma.1 | |
openssh-keycat | 8.0p1-25.el8_10.alma.1 | |
openssh-keycat-debuginfo | 8.0p1-25.el8_10.alma.1 | |
openssh-ldap | 8.0p1-25.el8_10.alma.1 | |
openssh-ldap-debuginfo | 8.0p1-25.el8_10.alma.1 | |
openssh-server | 8.0p1-25.el8_10.alma.1 | |
openssh-server-debuginfo | 8.0p1-25.el8_10.alma.1 | |
pam_ssh_agent_auth | 0.10.3-7.25.el8_10.alma.1 | |
pam_ssh_agent_auth-debuginfo | 0.10.3-7.25.el8_10.alma.1 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh-askpass | 8.0p1-25.el8_10.alma.1 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh | 8.0p1-25.el8_10.alma.1 | |
openssh-askpass-debuginfo | 8.0p1-25.el8_10.alma.1 | |
openssh-cavs | 8.0p1-25.el8_10.alma.1 | |
openssh-cavs-debuginfo | 8.0p1-25.el8_10.alma.1 | |
openssh-clients | 8.0p1-25.el8_10.alma.1 | |
openssh-clients-debuginfo | 8.0p1-25.el8_10.alma.1 | |
openssh-debuginfo | 8.0p1-25.el8_10.alma.1 | |
openssh-debugsource | 8.0p1-25.el8_10.alma.1 | |
openssh-keycat | 8.0p1-25.el8_10.alma.1 | |
openssh-keycat-debuginfo | 8.0p1-25.el8_10.alma.1 | |
openssh-ldap | 8.0p1-25.el8_10.alma.1 | |
openssh-ldap-debuginfo | 8.0p1-25.el8_10.alma.1 | |
openssh-server | 8.0p1-25.el8_10.alma.1 | |
openssh-server-debuginfo | 8.0p1-25.el8_10.alma.1 | |
pam_ssh_agent_auth | 0.10.3-7.25.el8_10.alma.1 | |
pam_ssh_agent_auth-debuginfo | 0.10.3-7.25.el8_10.alma.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh-askpass | 8.0p1-25.el8_10.alma.1 | |

