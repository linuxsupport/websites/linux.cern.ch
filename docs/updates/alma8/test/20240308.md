## 2024-03-08

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cronie | 1.5.2-8.el8.alma.1 | |
cronie-anacron | 1.5.2-8.el8.alma.1 | |
cronie-anacron-debuginfo | 1.5.2-8.el8.alma.1 | |
cronie-debuginfo | 1.5.2-8.el8.alma.1 | |
cronie-debugsource | 1.5.2-8.el8.alma.1 | |
cronie-noanacron | 1.5.2-8.el8.alma.1 | |
fwupd | 1.7.8-2.el8.alma | |
fwupd-debuginfo | 1.7.8-2.el8.alma | |
fwupd-debugsource | 1.7.8-2.el8.alma | |
fwupd-tests-debuginfo | 1.7.8-2.el8.alma | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
squid | 4.15-7.module_el8.9.0+3742+967e5cef.5.alma.1 | |
squid-debuginfo | 4.15-7.module_el8.9.0+3742+967e5cef.5.alma.1 | |
squid-debugsource | 4.15-7.module_el8.9.0+3742+967e5cef.5.alma.1 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fwupd-devel | 1.7.8-2.el8.alma | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fwupd-tests | 1.7.8-2.el8.alma | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cronie | 1.5.2-8.el8.alma.1 | |
cronie-anacron | 1.5.2-8.el8.alma.1 | |
cronie-anacron-debuginfo | 1.5.2-8.el8.alma.1 | |
cronie-debuginfo | 1.5.2-8.el8.alma.1 | |
cronie-debugsource | 1.5.2-8.el8.alma.1 | |
cronie-noanacron | 1.5.2-8.el8.alma.1 | |
fwupd | 1.7.8-2.el8.alma | |
fwupd-debuginfo | 1.7.8-2.el8.alma | |
fwupd-debugsource | 1.7.8-2.el8.alma | |
fwupd-tests-debuginfo | 1.7.8-2.el8.alma | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
squid | 4.15-7.module_el8.9.0+3742+967e5cef.5.alma.1 | |
squid-debuginfo | 4.15-7.module_el8.9.0+3742+967e5cef.5.alma.1 | |
squid-debugsource | 4.15-7.module_el8.9.0+3742+967e5cef.5.alma.1 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fwupd-devel | 1.7.8-2.el8.alma | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fwupd-tests | 1.7.8-2.el8.alma | |

