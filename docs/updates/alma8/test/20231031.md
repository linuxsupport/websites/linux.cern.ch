## 2023-10-31

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.12-1.al8.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
platform-python | 3.6.8-51.el8_8.2.alma.1 | [ALSA-2023:5997](https://errata.almalinux.org/8/ALSA-2023-5997.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-debuginfo | 3.6.8-51.el8_8.2.alma.1 | |
python3-debugsource | 3.6.8-51.el8_8.2.alma.1 | |
python3-libs | 3.6.8-51.el8_8.2.alma.1 | [ALSA-2023:5997](https://errata.almalinux.org/8/ALSA-2023-5997.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-test | 3.6.8-51.el8_8.2.alma.1 | [ALSA-2023:5997](https://errata.almalinux.org/8/ALSA-2023-5997.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
platform-python-debug | 3.6.8-51.el8_8.2.alma.1 | [ALSA-2023:5997](https://errata.almalinux.org/8/ALSA-2023-5997.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
platform-python-devel | 3.6.8-51.el8_8.2.alma.1 | [ALSA-2023:5997](https://errata.almalinux.org/8/ALSA-2023-5997.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2 | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | [ALSA-2023:5994](https://errata.almalinux.org/8/ALSA-2023-5994.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-debug | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | [ALSA-2023:5994](https://errata.almalinux.org/8/ALSA-2023-5994.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-debuginfo | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | |
python2-debugsource | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | |
python2-devel | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | [ALSA-2023:5994](https://errata.almalinux.org/8/ALSA-2023-5994.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-libs | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | [ALSA-2023:5994](https://errata.almalinux.org/8/ALSA-2023-5994.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-test | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | [ALSA-2023:5994](https://errata.almalinux.org/8/ALSA-2023-5994.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-tkinter | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | [ALSA-2023:5994](https://errata.almalinux.org/8/ALSA-2023-5994.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-tools | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | [ALSA-2023:5994](https://errata.almalinux.org/8/ALSA-2023-5994.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-debuginfo | 3.6.8-51.el8_8.2.alma.1 | |
python3-debugsource | 3.6.8-51.el8_8.2.alma.1 | |
python3-idle | 3.6.8-51.el8_8.2.alma.1 | [ALSA-2023:5997](https://errata.almalinux.org/8/ALSA-2023-5997.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-tkinter | 3.6.8-51.el8_8.2.alma.1 | [ALSA-2023:5997](https://errata.almalinux.org/8/ALSA-2023-5997.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39 | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-debuginfo | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | |
python39-debugsource | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | |
python39-devel | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-idle | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-libs | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-rpm-macros | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-test | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-tkinter | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
varnish | 6.0.8-3.module_el8.8.0+3665+0b8dabc5.1.alma.1 | [ALSA-2023:5989](https://errata.almalinux.org/8/ALSA-2023-5989.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
varnish-devel | 6.0.8-3.module_el8.8.0+3665+0b8dabc5.1.alma.1 | [ALSA-2023:5989](https://errata.almalinux.org/8/ALSA-2023-5989.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
varnish-docs | 6.0.8-3.module_el8.8.0+3665+0b8dabc5.1.alma.1 | [ALSA-2023:5989](https://errata.almalinux.org/8/ALSA-2023-5989.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python39-debug | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-devel | 3.6.8-51.el8_8.2.alma.1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.12-1.al8.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
platform-python | 3.6.8-51.el8_8.2.alma.1 | [ALSA-2023:5997](https://errata.almalinux.org/8/ALSA-2023-5997.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-debuginfo | 3.6.8-51.el8_8.2.alma.1 | |
python3-debugsource | 3.6.8-51.el8_8.2.alma.1 | |
python3-libs | 3.6.8-51.el8_8.2.alma.1 | [ALSA-2023:5997](https://errata.almalinux.org/8/ALSA-2023-5997.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-test | 3.6.8-51.el8_8.2.alma.1 | [ALSA-2023:5997](https://errata.almalinux.org/8/ALSA-2023-5997.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
platform-python-debug | 3.6.8-51.el8_8.2.alma.1 | [ALSA-2023:5997](https://errata.almalinux.org/8/ALSA-2023-5997.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
platform-python-devel | 3.6.8-51.el8_8.2.alma.1 | [ALSA-2023:5997](https://errata.almalinux.org/8/ALSA-2023-5997.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2 | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | [ALSA-2023:5994](https://errata.almalinux.org/8/ALSA-2023-5994.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-debug | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | [ALSA-2023:5994](https://errata.almalinux.org/8/ALSA-2023-5994.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-debuginfo | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | |
python2-debugsource | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | |
python2-devel | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | [ALSA-2023:5994](https://errata.almalinux.org/8/ALSA-2023-5994.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-libs | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | [ALSA-2023:5994](https://errata.almalinux.org/8/ALSA-2023-5994.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-test | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | [ALSA-2023:5994](https://errata.almalinux.org/8/ALSA-2023-5994.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-tkinter | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | [ALSA-2023:5994](https://errata.almalinux.org/8/ALSA-2023-5994.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python2-tools | 2.7.18-13.module_el8.8.0+3663+627ce7bb.2.alma.1 | [ALSA-2023:5994](https://errata.almalinux.org/8/ALSA-2023-5994.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-debuginfo | 3.6.8-51.el8_8.2.alma.1 | |
python3-debugsource | 3.6.8-51.el8_8.2.alma.1 | |
python3-idle | 3.6.8-51.el8_8.2.alma.1 | [ALSA-2023:5997](https://errata.almalinux.org/8/ALSA-2023-5997.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python3-tkinter | 3.6.8-51.el8_8.2.alma.1 | [ALSA-2023:5997](https://errata.almalinux.org/8/ALSA-2023-5997.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39 | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-debuginfo | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | |
python39-debugsource | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | |
python39-devel | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-idle | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-libs | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-rpm-macros | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-test | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
python39-tkinter | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))
varnish | 6.0.8-3.module_el8.8.0+3665+0b8dabc5.1.alma.1 | [ALSA-2023:5989](https://errata.almalinux.org/8/ALSA-2023-5989.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
varnish-devel | 6.0.8-3.module_el8.8.0+3665+0b8dabc5.1.alma.1 | [ALSA-2023:5989](https://errata.almalinux.org/8/ALSA-2023-5989.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
varnish-docs | 6.0.8-3.module_el8.8.0+3665+0b8dabc5.1.alma.1 | [ALSA-2023:5989](https://errata.almalinux.org/8/ALSA-2023-5989.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python39-debug | 3.9.16-1.module_el8.8.0+3664+2b1e4686.2 | [ALSA-2023:5998](https://errata.almalinux.org/8/ALSA-2023-5998.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-40217](https://access.redhat.com/security/cve/CVE-2023-40217))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-devel | 3.6.8-51.el8_8.2.alma.1 | |

