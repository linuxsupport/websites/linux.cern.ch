## 2023-07-11

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-debugsource | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-devel | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-legacy-tools | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-legacy-tools-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-libs | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-libs-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-snmp | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-snmp-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
python3-lib389 | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-389-ds | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-debugsource | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-devel | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-legacy-tools | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-legacy-tools-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-libs | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-libs-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-snmp | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-snmp-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
python3-lib389 | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-389-ds | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |

