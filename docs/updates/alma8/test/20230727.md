## 2023-07-27

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.5.5-2.el8.alma | |
sos-audit | 4.5.5-2.el8.alma | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_477.15.1.el8_8.al8.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.5.5-2.el8.alma | |
sos-audit | 4.5.5-2.el8.alma | |

