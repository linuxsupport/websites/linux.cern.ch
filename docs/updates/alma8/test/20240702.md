## 2024-07-02

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fuse-overlayfs | 1.13-1.module_el8.10.0+3859+6ae70a0e | |
fuse-overlayfs-debuginfo | 1.13-1.module_el8.10.0+3859+6ae70a0e | |
fuse-overlayfs-debugsource | 1.13-1.module_el8.10.0+3859+6ae70a0e | |
oci-seccomp-bpf-hook | 1.2.10-1.module_el8.10.0+3859+6ae70a0e | |
oci-seccomp-bpf-hook-debuginfo | 1.2.10-1.module_el8.10.0+3859+6ae70a0e | |
oci-seccomp-bpf-hook-debugsource | 1.2.10-1.module_el8.10.0+3859+6ae70a0e | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fuse-overlayfs | 1.13-1.module_el8.10.0+3859+6ae70a0e | |
fuse-overlayfs-debuginfo | 1.13-1.module_el8.10.0+3859+6ae70a0e | |
fuse-overlayfs-debugsource | 1.13-1.module_el8.10.0+3859+6ae70a0e | |
oci-seccomp-bpf-hook | 1.2.10-1.module_el8.10.0+3859+6ae70a0e | |
oci-seccomp-bpf-hook-debuginfo | 1.2.10-1.module_el8.10.0+3859+6ae70a0e | |
oci-seccomp-bpf-hook-debugsource | 1.2.10-1.module_el8.10.0+3859+6ae70a0e | |

