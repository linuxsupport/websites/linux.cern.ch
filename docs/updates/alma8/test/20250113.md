## 2025-01-13

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dpdk | 23.11-2.el8_10 | [ALSA-2025:0222](https://errata.almalinux.org/8/ALSA-2025-0222.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11614](https://access.redhat.com/security/cve/CVE-2024-11614))
dpdk-debuginfo | 23.11-2.el8_10 | |
dpdk-debugsource | 23.11-2.el8_10 | |
dpdk-devel | 23.11-2.el8_10 | [ALSA-2025:0222](https://errata.almalinux.org/8/ALSA-2025-0222.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11614](https://access.redhat.com/security/cve/CVE-2024-11614))
dpdk-doc | 23.11-2.el8_10 | [ALSA-2025:0222](https://errata.almalinux.org/8/ALSA-2025-0222.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11614](https://access.redhat.com/security/cve/CVE-2024-11614))
dpdk-tools | 23.11-2.el8_10 | [ALSA-2025:0222](https://errata.almalinux.org/8/ALSA-2025-0222.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11614](https://access.redhat.com/security/cve/CVE-2024-11614))
firefox | 128.6.0-1.el8_10 | [ALSA-2025:0144](https://errata.almalinux.org/8/ALSA-2025-0144.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-0237](https://access.redhat.com/security/cve/CVE-2025-0237), [CVE-2025-0238](https://access.redhat.com/security/cve/CVE-2025-0238), [CVE-2025-0239](https://access.redhat.com/security/cve/CVE-2025-0239), [CVE-2025-0240](https://access.redhat.com/security/cve/CVE-2025-0240), [CVE-2025-0241](https://access.redhat.com/security/cve/CVE-2025-0241), [CVE-2025-0242](https://access.redhat.com/security/cve/CVE-2025-0242), [CVE-2025-0243](https://access.redhat.com/security/cve/CVE-2025-0243))
firefox-debuginfo | 128.6.0-1.el8_10 | |
firefox-debugsource | 128.6.0-1.el8_10 | |
iperf3 | 3.5-11.el8_10 | [ALSA-2025:0168](https://errata.almalinux.org/8/ALSA-2025-0168.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-53580](https://access.redhat.com/security/cve/CVE-2024-53580))
iperf3-debuginfo | 3.5-11.el8_10 | |
iperf3-debugsource | 3.5-11.el8_10 | |
webkit2gtk3 | 2.46.5-1.el8_10 | [ALSA-2025:0145](https://errata.almalinux.org/8/ALSA-2025-0145.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54479](https://access.redhat.com/security/cve/CVE-2024-54479), [CVE-2024-54502](https://access.redhat.com/security/cve/CVE-2024-54502), [CVE-2024-54505](https://access.redhat.com/security/cve/CVE-2024-54505), [CVE-2024-54508](https://access.redhat.com/security/cve/CVE-2024-54508))
webkit2gtk3-debuginfo | 2.46.5-1.el8_10 | |
webkit2gtk3-debugsource | 2.46.5-1.el8_10 | |
webkit2gtk3-devel | 2.46.5-1.el8_10 | [ALSA-2025:0145](https://errata.almalinux.org/8/ALSA-2025-0145.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54479](https://access.redhat.com/security/cve/CVE-2024-54479), [CVE-2024-54502](https://access.redhat.com/security/cve/CVE-2024-54502), [CVE-2024-54505](https://access.redhat.com/security/cve/CVE-2024-54505), [CVE-2024-54508](https://access.redhat.com/security/cve/CVE-2024-54508))
webkit2gtk3-devel-debuginfo | 2.46.5-1.el8_10 | |
webkit2gtk3-jsc | 2.46.5-1.el8_10 | [ALSA-2025:0145](https://errata.almalinux.org/8/ALSA-2025-0145.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54479](https://access.redhat.com/security/cve/CVE-2024-54479), [CVE-2024-54502](https://access.redhat.com/security/cve/CVE-2024-54502), [CVE-2024-54505](https://access.redhat.com/security/cve/CVE-2024-54505), [CVE-2024-54508](https://access.redhat.com/security/cve/CVE-2024-54508))
webkit2gtk3-jsc-debuginfo | 2.46.5-1.el8_10 | |
webkit2gtk3-jsc-devel | 2.46.5-1.el8_10 | [ALSA-2025:0145](https://errata.almalinux.org/8/ALSA-2025-0145.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-54479](https://access.redhat.com/security/cve/CVE-2024-54479), [CVE-2024-54502](https://access.redhat.com/security/cve/CVE-2024-54502), [CVE-2024-54505](https://access.redhat.com/security/cve/CVE-2024-54505), [CVE-2024-54508](https://access.redhat.com/security/cve/CVE-2024-54508))
webkit2gtk3-jsc-devel-debuginfo | 2.46.5-1.el8_10 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dpdk-devel | 23.11-2.el8_10 | [ALSA-2025:0222](https://errata.almalinux.org/8/ALSA-2025-0222.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11614](https://access.redhat.com/security/cve/CVE-2024-11614))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iperf3-devel | 3.5-11.el8_10 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dpdk-debuginfo | 23.11-2.el8_10 | |
dpdk-debugsource | 23.11-2.el8_10 | |
firefox-debuginfo | 128.6.0-1.el8_10 | |
firefox-debugsource | 128.6.0-1.el8_10 | |
iperf3-debuginfo | 3.5-11.el8_10 | |
iperf3-debugsource | 3.5-11.el8_10 | |
webkit2gtk3-debuginfo | 2.46.5-1.el8_10 | |
webkit2gtk3-debugsource | 2.46.5-1.el8_10 | |
webkit2gtk3-devel-debuginfo | 2.46.5-1.el8_10 | |
webkit2gtk3-jsc-debuginfo | 2.46.5-1.el8_10 | |
webkit2gtk3-jsc-devel-debuginfo | 2.46.5-1.el8_10 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dpdk-devel | 23.11-2.el8_10 | [ALSA-2025:0222](https://errata.almalinux.org/8/ALSA-2025-0222.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-11614](https://access.redhat.com/security/cve/CVE-2024-11614))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iperf3-devel | 3.5-11.el8_10 | |

