## 2023-06-22

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-linuxsupport-registry-conf | 1.0-1.al8.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.12.0-1.el8_8.alma | |
firefox-debuginfo | 102.12.0-1.el8_8.alma | |
firefox-debugsource | 102.12.0-1.el8_8.alma | |
thunderbird | 102.12.0-1.el8_8.alma | |
thunderbird-debuginfo | 102.12.0-1.el8_8.alma | |
thunderbird-debugsource | 102.12.0-1.el8_8.alma | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.12.0-1.el8_8.alma.plus | |
thunderbird-debuginfo | 102.12.0-1.el8_8.alma.plus | |
thunderbird-debugsource | 102.12.0-1.el8_8.alma.plus | |
thunderbird-librnp-rnp | 102.12.0-1.el8_8.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.12.0-1.el8_8.alma.plus | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-linuxsupport-registry-conf | 1.0-1.al8.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.12.0-1.el8_8.alma | |
firefox-debuginfo | 102.12.0-1.el8_8.alma | |
firefox-debugsource | 102.12.0-1.el8_8.alma | |
thunderbird | 102.12.0-1.el8_8.alma | |
thunderbird-debuginfo | 102.12.0-1.el8_8.alma | |
thunderbird-debugsource | 102.12.0-1.el8_8.alma | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.12.0-1.el8_8.alma.plus | |
thunderbird-debuginfo | 102.12.0-1.el8_8.alma.plus | |
thunderbird-debugsource | 102.12.0-1.el8_8.alma.plus | |
thunderbird-librnp-rnp | 102.12.0-1.el8_8.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.12.0-1.el8_8.alma.plus | |

