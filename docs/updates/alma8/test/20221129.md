## 2022-11-29

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
krb5-debuginfo | 1.18.2-22.el8_7 | |
krb5-debugsource | 1.18.2-22.el8_7 | |
krb5-devel | 1.18.2-22.el8_7 | [RHSA-2022:8638](https://access.redhat.com/errata/RHSA-2022:8638) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42898](https://access.redhat.com/security/cve/CVE-2022-42898))
krb5-devel-debuginfo | 1.18.2-22.el8_7 | |
krb5-libs | 1.18.2-22.el8_7 | [RHSA-2022:8638](https://access.redhat.com/errata/RHSA-2022:8638) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42898](https://access.redhat.com/security/cve/CVE-2022-42898))
krb5-libs-debuginfo | 1.18.2-22.el8_7 | |
krb5-pkinit | 1.18.2-22.el8_7 | [RHSA-2022:8638](https://access.redhat.com/errata/RHSA-2022:8638) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42898](https://access.redhat.com/security/cve/CVE-2022-42898))
krb5-pkinit-debuginfo | 1.18.2-22.el8_7 | |
krb5-server | 1.18.2-22.el8_7 | [RHSA-2022:8638](https://access.redhat.com/errata/RHSA-2022:8638) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42898](https://access.redhat.com/security/cve/CVE-2022-42898))
krb5-server-debuginfo | 1.18.2-22.el8_7 | |
krb5-server-ldap | 1.18.2-22.el8_7 | [RHSA-2022:8638](https://access.redhat.com/errata/RHSA-2022:8638) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42898](https://access.redhat.com/security/cve/CVE-2022-42898))
krb5-server-ldap-debuginfo | 1.18.2-22.el8_7 | |
krb5-workstation | 1.18.2-22.el8_7 | [RHSA-2022:8638](https://access.redhat.com/errata/RHSA-2022:8638) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42898](https://access.redhat.com/security/cve/CVE-2022-42898))
krb5-workstation-debuginfo | 1.18.2-22.el8_7 | |
libkadm5 | 1.18.2-22.el8_7 | [RHSA-2022:8638](https://access.redhat.com/errata/RHSA-2022:8638) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42898](https://access.redhat.com/security/cve/CVE-2022-42898))
libkadm5-debuginfo | 1.18.2-22.el8_7 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
varnish | 6.0.8-2.module_el8.7.0+3353+b11d8f2f.1 | |
varnish-devel | 6.0.8-2.module_el8.7.0+3353+b11d8f2f.1 | |
varnish-docs | 6.0.8-2.module_el8.7.0+3353+b11d8f2f.1 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
krb5-debuginfo | 1.18.2-22.el8_7 | |
krb5-debugsource | 1.18.2-22.el8_7 | |
krb5-devel | 1.18.2-22.el8_7 | [RHSA-2022:8638](https://access.redhat.com/errata/RHSA-2022:8638) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42898](https://access.redhat.com/security/cve/CVE-2022-42898))
krb5-devel-debuginfo | 1.18.2-22.el8_7 | |
krb5-libs | 1.18.2-22.el8_7 | [RHSA-2022:8638](https://access.redhat.com/errata/RHSA-2022:8638) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42898](https://access.redhat.com/security/cve/CVE-2022-42898))
krb5-libs-debuginfo | 1.18.2-22.el8_7 | |
krb5-pkinit | 1.18.2-22.el8_7 | [RHSA-2022:8638](https://access.redhat.com/errata/RHSA-2022:8638) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42898](https://access.redhat.com/security/cve/CVE-2022-42898))
krb5-pkinit-debuginfo | 1.18.2-22.el8_7 | |
krb5-server | 1.18.2-22.el8_7 | [RHSA-2022:8638](https://access.redhat.com/errata/RHSA-2022:8638) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42898](https://access.redhat.com/security/cve/CVE-2022-42898))
krb5-server-debuginfo | 1.18.2-22.el8_7 | |
krb5-server-ldap | 1.18.2-22.el8_7 | [RHSA-2022:8638](https://access.redhat.com/errata/RHSA-2022:8638) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42898](https://access.redhat.com/security/cve/CVE-2022-42898))
krb5-server-ldap-debuginfo | 1.18.2-22.el8_7 | |
krb5-workstation | 1.18.2-22.el8_7 | [RHSA-2022:8638](https://access.redhat.com/errata/RHSA-2022:8638) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42898](https://access.redhat.com/security/cve/CVE-2022-42898))
krb5-workstation-debuginfo | 1.18.2-22.el8_7 | |
libkadm5 | 1.18.2-22.el8_7 | [RHSA-2022:8638](https://access.redhat.com/errata/RHSA-2022:8638) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42898](https://access.redhat.com/security/cve/CVE-2022-42898))
libkadm5-debuginfo | 1.18.2-22.el8_7 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
varnish | 6.0.8-2.module_el8.7.0+3353+b11d8f2f.1 | |
varnish-devel | 6.0.8-2.module_el8.7.0+3353+b11d8f2f.1 | |
varnish-docs | 6.0.8-2.module_el8.7.0+3353+b11d8f2f.1 | |

