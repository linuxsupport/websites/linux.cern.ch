## 2023-12-04

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_513.9.1.el8_9.al8.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-513.9.1.el8_9 | |
bpftool-debuginfo | 4.18.0-513.9.1.el8_9 | |
kernel | 4.18.0-513.9.1.el8_9 | |
kernel-abi-stablelists | 4.18.0-513.9.1.el8_9 | |
kernel-core | 4.18.0-513.9.1.el8_9 | |
kernel-cross-headers | 4.18.0-513.9.1.el8_9 | |
kernel-debug | 4.18.0-513.9.1.el8_9 | |
kernel-debug-core | 4.18.0-513.9.1.el8_9 | |
kernel-debug-debuginfo | 4.18.0-513.9.1.el8_9 | |
kernel-debug-devel | 4.18.0-513.9.1.el8_9 | |
kernel-debug-modules | 4.18.0-513.9.1.el8_9 | |
kernel-debug-modules-extra | 4.18.0-513.9.1.el8_9 | |
kernel-debuginfo | 4.18.0-513.9.1.el8_9 | |
kernel-debuginfo-common-x86_64 | 4.18.0-513.9.1.el8_9 | |
kernel-devel | 4.18.0-513.9.1.el8_9 | |
kernel-doc | 4.18.0-513.9.1.el8_9 | |
kernel-headers | 4.18.0-513.9.1.el8_9 | |
kernel-modules | 4.18.0-513.9.1.el8_9 | |
kernel-modules-extra | 4.18.0-513.9.1.el8_9 | |
kernel-tools | 4.18.0-513.9.1.el8_9 | |
kernel-tools-debuginfo | 4.18.0-513.9.1.el8_9 | |
kernel-tools-libs | 4.18.0-513.9.1.el8_9 | |
microcode_ctl | 20230808-2.20231009.1.el8_9 | |
perf | 4.18.0-513.9.1.el8_9 | |
perf-debuginfo | 4.18.0-513.9.1.el8_9 | |
python3-perf | 4.18.0-513.9.1.el8_9 | |
python3-perf-debuginfo | 4.18.0-513.9.1.el8_9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.5.0-1.el8_9.alma.1 | |
firefox-debuginfo | 115.5.0-1.el8_9.alma.1 | |
firefox-debugsource | 115.5.0-1.el8_9.alma.1 | |
thunderbird | 115.5.0-1.el8_9.alma.1 | |
thunderbird-debuginfo | 115.5.0-1.el8_9.alma.1 | |
thunderbird-debugsource | 115.5.0-1.el8_9.alma.1 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-513.9.1.el8_9 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-debug-modules-internal | 4.18.0-513.9.1.el8_9 | |
kernel-ipaclones-internal | 4.18.0-513.9.1.el8_9 | |
kernel-modules-internal | 4.18.0-513.9.1.el8_9 | |
kernel-selftests-internal | 4.18.0-513.9.1.el8_9 | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 115.5.0-1.el8_9.alma.plus | |
thunderbird-debuginfo | 115.5.0-1.el8_9.alma.plus | |
thunderbird-debugsource | 115.5.0-1.el8_9.alma.plus | |
thunderbird-librnp-rnp | 115.5.0-1.el8_9.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 115.5.0-1.el8_9.alma.plus | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-513.9.1.el8_9 | |
bpftool-debuginfo | 4.18.0-513.9.1.el8_9 | |
kernel | 4.18.0-513.9.1.el8_9 | |
kernel-abi-stablelists | 4.18.0-513.9.1.el8_9 | |
kernel-core | 4.18.0-513.9.1.el8_9 | |
kernel-cross-headers | 4.18.0-513.9.1.el8_9 | |
kernel-debug | 4.18.0-513.9.1.el8_9 | |
kernel-debug-core | 4.18.0-513.9.1.el8_9 | |
kernel-debug-debuginfo | 4.18.0-513.9.1.el8_9 | |
kernel-debug-devel | 4.18.0-513.9.1.el8_9 | |
kernel-debug-modules | 4.18.0-513.9.1.el8_9 | |
kernel-debug-modules-extra | 4.18.0-513.9.1.el8_9 | |
kernel-debuginfo | 4.18.0-513.9.1.el8_9 | |
kernel-debuginfo-common-aarch64 | 4.18.0-513.9.1.el8_9 | |
kernel-devel | 4.18.0-513.9.1.el8_9 | |
kernel-doc | 4.18.0-513.9.1.el8_9 | |
kernel-headers | 4.18.0-513.9.1.el8_9 | |
kernel-modules | 4.18.0-513.9.1.el8_9 | |
kernel-modules-extra | 4.18.0-513.9.1.el8_9 | |
kernel-tools | 4.18.0-513.9.1.el8_9 | |
kernel-tools-debuginfo | 4.18.0-513.9.1.el8_9 | |
kernel-tools-libs | 4.18.0-513.9.1.el8_9 | |
perf | 4.18.0-513.9.1.el8_9 | |
perf-debuginfo | 4.18.0-513.9.1.el8_9 | |
python3-perf | 4.18.0-513.9.1.el8_9 | |
python3-perf-debuginfo | 4.18.0-513.9.1.el8_9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.5.0-1.el8_9.alma.1 | |
firefox-debuginfo | 115.5.0-1.el8_9.alma.1 | |
firefox-debugsource | 115.5.0-1.el8_9.alma.1 | |
thunderbird | 115.5.0-1.el8_9.alma.1 | |
thunderbird-debuginfo | 115.5.0-1.el8_9.alma.1 | |
thunderbird-debugsource | 115.5.0-1.el8_9.alma.1 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-513.9.1.el8_9 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-debug-modules-internal | 4.18.0-513.9.1.el8_9 | |
kernel-modules-internal | 4.18.0-513.9.1.el8_9 | |
kernel-selftests-internal | 4.18.0-513.9.1.el8_9 | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 115.5.0-1.el8_9.alma.plus | |
thunderbird-debuginfo | 115.5.0-1.el8_9.alma.plus | |
thunderbird-debugsource | 115.5.0-1.el8_9.alma.plus | |
thunderbird-librnp-rnp | 115.5.0-1.el8_9.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 115.5.0-1.el8_9.alma.plus | |

