## 2024-03-14

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.28-1.el8_9 | |
aspnetcore-targeting-pack-6.0 | 6.0.28-1.el8_9 | |
dotnet-apphost-pack-6.0 | 6.0.28-1.el8_9 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-hostfxr-6.0 | 6.0.28-1.el8_9 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-runtime-6.0 | 6.0.28-1.el8_9 | |
dotnet-runtime-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-sdk-6.0 | 6.0.128-1.el8_9 | |
dotnet-sdk-6.0-debuginfo | 6.0.128-1.el8_9 | |
dotnet-targeting-pack-6.0 | 6.0.28-1.el8_9 | |
dotnet-templates-6.0 | 6.0.128-1.el8_9 | |
dotnet6.0-debuginfo | 6.0.128-1.el8_9 | |
dotnet6.0-debugsource | 6.0.128-1.el8_9 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.128-1.el8_9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.28-1.el8_9 | |
aspnetcore-targeting-pack-6.0 | 6.0.28-1.el8_9 | |
dotnet-apphost-pack-6.0 | 6.0.28-1.el8_9 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-hostfxr-6.0 | 6.0.28-1.el8_9 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-runtime-6.0 | 6.0.28-1.el8_9 | |
dotnet-runtime-6.0-debuginfo | 6.0.28-1.el8_9 | |
dotnet-sdk-6.0 | 6.0.128-1.el8_9 | |
dotnet-sdk-6.0-debuginfo | 6.0.128-1.el8_9 | |
dotnet-targeting-pack-6.0 | 6.0.28-1.el8_9 | |
dotnet-templates-6.0 | 6.0.128-1.el8_9 | |
dotnet6.0-debuginfo | 6.0.128-1.el8_9 | |
dotnet6.0-debugsource | 6.0.128-1.el8_9 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.128-1.el8_9 | |

