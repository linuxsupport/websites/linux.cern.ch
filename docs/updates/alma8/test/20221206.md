## 2022-12-06

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2022g-1.el8 | [RHBA-2022:8785](https://access.redhat.com/errata/RHBA-2022:8785) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2022g-1.el8 | [RHBA-2022:8785](https://access.redhat.com/errata/RHBA-2022:8785) | <div class="adv_b">Bug Fix Advisory</div>

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2022g-1.el8 | [RHBA-2022:8785](https://access.redhat.com/errata/RHBA-2022:8785) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2022g-1.el8 | [RHBA-2022:8785](https://access.redhat.com/errata/RHBA-2022:8785) | <div class="adv_b">Bug Fix Advisory</div>

