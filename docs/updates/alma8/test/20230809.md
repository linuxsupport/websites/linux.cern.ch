## 2023-08-09

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.14.0-1.el8_8.alma | [ALSA-2023:4497](https://errata.almalinux.org/8/ALSA-2023-4497.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3417](https://access.redhat.com/security/cve/CVE-2023-3417), [CVE-2023-4045](https://access.redhat.com/security/cve/CVE-2023-4045), [CVE-2023-4046](https://access.redhat.com/security/cve/CVE-2023-4046), [CVE-2023-4047](https://access.redhat.com/security/cve/CVE-2023-4047), [CVE-2023-4048](https://access.redhat.com/security/cve/CVE-2023-4048), [CVE-2023-4049](https://access.redhat.com/security/cve/CVE-2023-4049), [CVE-2023-4050](https://access.redhat.com/security/cve/CVE-2023-4050), [CVE-2023-4055](https://access.redhat.com/security/cve/CVE-2023-4055), [CVE-2023-4056](https://access.redhat.com/security/cve/CVE-2023-4056), [CVE-2023-4057](https://access.redhat.com/security/cve/CVE-2023-4057))
thunderbird-debuginfo | 102.14.0-1.el8_8.alma | |
thunderbird-debugsource | 102.14.0-1.el8_8.alma | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.14.0-1.el8_8.alma.plus | [ALSA-2023:4497](https://errata.almalinux.org/8/ALSA-2023-4497.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3417](https://access.redhat.com/security/cve/CVE-2023-3417), [CVE-2023-4045](https://access.redhat.com/security/cve/CVE-2023-4045), [CVE-2023-4046](https://access.redhat.com/security/cve/CVE-2023-4046), [CVE-2023-4047](https://access.redhat.com/security/cve/CVE-2023-4047), [CVE-2023-4048](https://access.redhat.com/security/cve/CVE-2023-4048), [CVE-2023-4049](https://access.redhat.com/security/cve/CVE-2023-4049), [CVE-2023-4050](https://access.redhat.com/security/cve/CVE-2023-4050), [CVE-2023-4055](https://access.redhat.com/security/cve/CVE-2023-4055), [CVE-2023-4056](https://access.redhat.com/security/cve/CVE-2023-4056), [CVE-2023-4057](https://access.redhat.com/security/cve/CVE-2023-4057))
thunderbird-debuginfo | 102.14.0-1.el8_8.alma.plus | |
thunderbird-debugsource | 102.14.0-1.el8_8.alma.plus | |
thunderbird-librnp-rnp | 102.14.0-1.el8_8.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.14.0-1.el8_8.alma.plus | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.14.0-1.el8_8.alma | [ALSA-2023:4497](https://errata.almalinux.org/8/ALSA-2023-4497.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3417](https://access.redhat.com/security/cve/CVE-2023-3417), [CVE-2023-4045](https://access.redhat.com/security/cve/CVE-2023-4045), [CVE-2023-4046](https://access.redhat.com/security/cve/CVE-2023-4046), [CVE-2023-4047](https://access.redhat.com/security/cve/CVE-2023-4047), [CVE-2023-4048](https://access.redhat.com/security/cve/CVE-2023-4048), [CVE-2023-4049](https://access.redhat.com/security/cve/CVE-2023-4049), [CVE-2023-4050](https://access.redhat.com/security/cve/CVE-2023-4050), [CVE-2023-4055](https://access.redhat.com/security/cve/CVE-2023-4055), [CVE-2023-4056](https://access.redhat.com/security/cve/CVE-2023-4056), [CVE-2023-4057](https://access.redhat.com/security/cve/CVE-2023-4057))
thunderbird-debuginfo | 102.14.0-1.el8_8.alma | |
thunderbird-debugsource | 102.14.0-1.el8_8.alma | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.14.0-1.el8_8.alma.plus | [ALSA-2023:4497](https://errata.almalinux.org/8/ALSA-2023-4497.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3417](https://access.redhat.com/security/cve/CVE-2023-3417), [CVE-2023-4045](https://access.redhat.com/security/cve/CVE-2023-4045), [CVE-2023-4046](https://access.redhat.com/security/cve/CVE-2023-4046), [CVE-2023-4047](https://access.redhat.com/security/cve/CVE-2023-4047), [CVE-2023-4048](https://access.redhat.com/security/cve/CVE-2023-4048), [CVE-2023-4049](https://access.redhat.com/security/cve/CVE-2023-4049), [CVE-2023-4050](https://access.redhat.com/security/cve/CVE-2023-4050), [CVE-2023-4055](https://access.redhat.com/security/cve/CVE-2023-4055), [CVE-2023-4056](https://access.redhat.com/security/cve/CVE-2023-4056), [CVE-2023-4057](https://access.redhat.com/security/cve/CVE-2023-4057))
thunderbird-debuginfo | 102.14.0-1.el8_8.alma.plus | |
thunderbird-debugsource | 102.14.0-1.el8_8.alma.plus | |
thunderbird-librnp-rnp | 102.14.0-1.el8_8.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.14.0-1.el8_8.alma.plus | |

