## 2024-12-13

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
apcu-panel | 5.1.18-1.module_el8.10.0+3935+28808425 | |
libzip | 1.6.1-1.module_el8.10.0+3935+28808425 | |
libzip-debuginfo | 1.6.1-1.module_el8.10.0+3935+28808425 | |
libzip-debugsource | 1.6.1-1.module_el8.10.0+3935+28808425 | |
libzip-devel | 1.6.1-1.module_el8.10.0+3935+28808425 | |
libzip-tools | 1.6.1-1.module_el8.10.0+3935+28808425 | |
libzip-tools-debuginfo | 1.6.1-1.module_el8.10.0+3935+28808425 | |
php | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-bcmath | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-bcmath | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-bcmath-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-bcmath-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-cli | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-cli | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-cli-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-cli-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-common | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-common | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-common-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-common-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-dba | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-dba | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-dba-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-dba-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-dbg | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-dbg | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-dbg-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-dbg-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-debugsource | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-debugsource | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-devel | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-devel | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-embedded | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-embedded | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-embedded-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-embedded-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-enchant | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-enchant | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-enchant-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-enchant-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-ffi | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-ffi | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-ffi-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-ffi-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-fpm | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-fpm | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-fpm-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-fpm-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-gd | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-gd | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-gd-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-gd-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-gmp | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-gmp | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-gmp-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-gmp-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-intl | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-intl | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-intl-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-intl-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-json | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-json-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-ldap | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-ldap | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-ldap-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-ldap-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-mbstring | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-mbstring | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-mbstring-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-mbstring-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-mysqlnd | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-mysqlnd | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-mysqlnd-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-mysqlnd-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-odbc | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-odbc | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-odbc-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-odbc-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-opcache | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-opcache | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-opcache-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-opcache-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-pdo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-pdo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-pdo-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-pdo-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-pear | 1.10.13-1.module_el8.10.0+3935+28808425 | |
php-pecl-apcu | 5.1.18-1.module_el8.10.0+3935+28808425 | |
php-pecl-apcu-debuginfo | 5.1.18-1.module_el8.10.0+3935+28808425 | |
php-pecl-apcu-debugsource | 5.1.18-1.module_el8.10.0+3935+28808425 | |
php-pecl-apcu-devel | 5.1.18-1.module_el8.10.0+3935+28808425 | |
php-pecl-rrd | 2.0.1-1.module_el8.10.0+3935+28808425 | |
php-pecl-rrd-debuginfo | 2.0.1-1.module_el8.10.0+3935+28808425 | |
php-pecl-rrd-debugsource | 2.0.1-1.module_el8.10.0+3935+28808425 | |
php-pecl-xdebug | 2.9.5-1.module_el8.10.0+3935+28808425 | |
php-pecl-xdebug-debuginfo | 2.9.5-1.module_el8.10.0+3935+28808425 | |
php-pecl-xdebug-debugsource | 2.9.5-1.module_el8.10.0+3935+28808425 | |
php-pecl-zip | 1.18.2-1.module_el8.10.0+3935+28808425 | |
php-pecl-zip-debuginfo | 1.18.2-1.module_el8.10.0+3935+28808425 | |
php-pecl-zip-debugsource | 1.18.2-1.module_el8.10.0+3935+28808425 | |
php-pgsql | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-pgsql | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-pgsql-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-pgsql-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-process | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-process | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-process-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-process-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-snmp | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-snmp | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-snmp-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-snmp-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-soap | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-soap | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-soap-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-soap-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-xml | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-xml | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-xml-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-xml-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-xmlrpc | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-xmlrpc-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
python-virtualenv-doc | 15.1.0-23.module_el8.10.0+3937+b6a3652f | |
python3-virtualenv | 15.1.0-23.module_el8.10.0+3937+b6a3652f | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
apcu-panel | 5.1.18-1.module_el8.10.0+3935+28808425 | |
libzip | 1.6.1-1.module_el8.10.0+3935+28808425 | |
libzip-debuginfo | 1.6.1-1.module_el8.10.0+3935+28808425 | |
libzip-debugsource | 1.6.1-1.module_el8.10.0+3935+28808425 | |
libzip-devel | 1.6.1-1.module_el8.10.0+3935+28808425 | |
libzip-tools | 1.6.1-1.module_el8.10.0+3935+28808425 | |
libzip-tools-debuginfo | 1.6.1-1.module_el8.10.0+3935+28808425 | |
php | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-bcmath | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-bcmath | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-bcmath-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-bcmath-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-cli | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-cli | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-cli-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-cli-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-common | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-common | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-common-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-common-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-dba | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-dba | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-dba-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-dba-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-dbg | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-dbg | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-dbg-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-dbg-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-debugsource | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-debugsource | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-devel | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-devel | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-embedded | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-embedded | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-embedded-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-embedded-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-enchant | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-enchant | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-enchant-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-enchant-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-ffi | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-ffi | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-ffi-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-ffi-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-fpm | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-fpm | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-fpm-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-fpm-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-gd | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-gd | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-gd-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-gd-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-gmp | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-gmp | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-gmp-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-gmp-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-intl | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-intl | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-intl-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-intl-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-json | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-json-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-ldap | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-ldap | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-ldap-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-ldap-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-mbstring | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-mbstring | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-mbstring-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-mbstring-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-mysqlnd | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-mysqlnd | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-mysqlnd-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-mysqlnd-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-odbc | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-odbc | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-odbc-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-odbc-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-opcache | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-opcache | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-opcache-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-opcache-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-pdo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-pdo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-pdo-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-pdo-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-pear | 1.10.13-1.module_el8.10.0+3935+28808425 | |
php-pecl-apcu | 5.1.18-1.module_el8.10.0+3935+28808425 | |
php-pecl-apcu-debuginfo | 5.1.18-1.module_el8.10.0+3935+28808425 | |
php-pecl-apcu-debugsource | 5.1.18-1.module_el8.10.0+3935+28808425 | |
php-pecl-apcu-devel | 5.1.18-1.module_el8.10.0+3935+28808425 | |
php-pecl-rrd | 2.0.1-1.module_el8.10.0+3935+28808425 | |
php-pecl-rrd-debuginfo | 2.0.1-1.module_el8.10.0+3935+28808425 | |
php-pecl-rrd-debugsource | 2.0.1-1.module_el8.10.0+3935+28808425 | |
php-pecl-xdebug | 2.9.5-1.module_el8.10.0+3935+28808425 | |
php-pecl-xdebug-debuginfo | 2.9.5-1.module_el8.10.0+3935+28808425 | |
php-pecl-xdebug-debugsource | 2.9.5-1.module_el8.10.0+3935+28808425 | |
php-pecl-zip | 1.18.2-1.module_el8.10.0+3935+28808425 | |
php-pecl-zip-debuginfo | 1.18.2-1.module_el8.10.0+3935+28808425 | |
php-pecl-zip-debugsource | 1.18.2-1.module_el8.10.0+3935+28808425 | |
php-pgsql | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-pgsql | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-pgsql-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-pgsql-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-process | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-process | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-process-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-process-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-snmp | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-snmp | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-snmp-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-snmp-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-soap | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-soap | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-soap-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-soap-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-xml | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-xml | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-xml-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-xml-debuginfo | 8.2.25-1.module_el8.10.0+3936+14b35499 | |
php-xmlrpc | 7.4.33-2.module_el8.10.0+3935+28808425 | |
php-xmlrpc-debuginfo | 7.4.33-2.module_el8.10.0+3935+28808425 | |
python-virtualenv-doc | 15.1.0-23.module_el8.10.0+3937+b6a3652f | |
python3-virtualenv | 15.1.0-23.module_el8.10.0+3937+b6a3652f | |

