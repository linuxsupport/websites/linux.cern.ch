## 2024-10-02

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grafana | 9.2.10-18.el8_10 | [ALSA-2024:7349](https://errata.almalinux.org/8/ALSA-2024-7349.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24791](https://access.redhat.com/security/cve/CVE-2024-24791))
grafana-debuginfo | 9.2.10-18.el8_10 | |
grafana-debugsource | 9.2.10-18.el8_10 | |
grafana-selinux | 9.2.10-18.el8_10 | [ALSA-2024:7349](https://errata.almalinux.org/8/ALSA-2024-7349.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24791](https://access.redhat.com/security/cve/CVE-2024-24791))

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grafana | 9.2.10-18.el8_10 | [ALSA-2024:7349](https://errata.almalinux.org/8/ALSA-2024-7349.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24791](https://access.redhat.com/security/cve/CVE-2024-24791))
grafana-debuginfo | 9.2.10-18.el8_10 | |
grafana-debugsource | 9.2.10-18.el8_10 | |
grafana-selinux | 9.2.10-18.el8_10 | [ALSA-2024:7349](https://errata.almalinux.org/8/ALSA-2024-7349.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-24791](https://access.redhat.com/security/cve/CVE-2024-24791))

