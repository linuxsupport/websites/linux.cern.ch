## 2025-01-24

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_553.36.1.el8_10.al8.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-553.36.1.el8_10 | |
bpftool-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel | 4.18.0-553.36.1.el8_10 | |
kernel-abi-stablelists | 4.18.0-553.36.1.el8_10 | |
kernel-core | 4.18.0-553.36.1.el8_10 | |
kernel-cross-headers | 4.18.0-553.36.1.el8_10 | |
kernel-debug | 4.18.0-553.36.1.el8_10 | |
kernel-debug-core | 4.18.0-553.36.1.el8_10 | |
kernel-debug-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-debug-devel | 4.18.0-553.36.1.el8_10 | |
kernel-debug-modules | 4.18.0-553.36.1.el8_10 | |
kernel-debug-modules-extra | 4.18.0-553.36.1.el8_10 | |
kernel-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-debuginfo-common-x86_64 | 4.18.0-553.36.1.el8_10 | |
kernel-devel | 4.18.0-553.36.1.el8_10 | |
kernel-doc | 4.18.0-553.36.1.el8_10 | |
kernel-headers | 4.18.0-553.36.1.el8_10 | |
kernel-modules | 4.18.0-553.36.1.el8_10 | |
kernel-modules-extra | 4.18.0-553.36.1.el8_10 | |
kernel-tools | 4.18.0-553.36.1.el8_10 | |
kernel-tools-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-tools-libs | 4.18.0-553.36.1.el8_10 | |
perf | 4.18.0-553.36.1.el8_10 | |
perf-debuginfo | 4.18.0-553.36.1.el8_10 | |
python3-perf | 4.18.0-553.36.1.el8_10 | |
python3-perf-debuginfo | 4.18.0-553.36.1.el8_10 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
redis | 6.2.17-1.module_el8.10.0+3946+3de613d5 | [ALSA-2025:0595](https://errata.almalinux.org/8/ALSA-2025-0595.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2022-35977](https://access.redhat.com/security/cve/CVE-2022-35977), [CVE-2022-36021](https://access.redhat.com/security/cve/CVE-2022-36021), [CVE-2023-22458](https://access.redhat.com/security/cve/CVE-2023-22458), [CVE-2023-25155](https://access.redhat.com/security/cve/CVE-2023-25155), [CVE-2023-28856](https://access.redhat.com/security/cve/CVE-2023-28856), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis-debuginfo | 6.2.17-1.module_el8.10.0+3946+3de613d5 | |
redis-debugsource | 6.2.17-1.module_el8.10.0+3946+3de613d5 | |
redis-devel | 6.2.17-1.module_el8.10.0+3946+3de613d5 | [ALSA-2025:0595](https://errata.almalinux.org/8/ALSA-2025-0595.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2022-35977](https://access.redhat.com/security/cve/CVE-2022-35977), [CVE-2022-36021](https://access.redhat.com/security/cve/CVE-2022-36021), [CVE-2023-22458](https://access.redhat.com/security/cve/CVE-2023-22458), [CVE-2023-25155](https://access.redhat.com/security/cve/CVE-2023-25155), [CVE-2023-28856](https://access.redhat.com/security/cve/CVE-2023-28856), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis-doc | 6.2.17-1.module_el8.10.0+3946+3de613d5 | [ALSA-2025:0595](https://errata.almalinux.org/8/ALSA-2025-0595.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2022-35977](https://access.redhat.com/security/cve/CVE-2022-35977), [CVE-2022-36021](https://access.redhat.com/security/cve/CVE-2022-36021), [CVE-2023-22458](https://access.redhat.com/security/cve/CVE-2023-22458), [CVE-2023-25155](https://access.redhat.com/security/cve/CVE-2023-25155), [CVE-2023-28856](https://access.redhat.com/security/cve/CVE-2023-28856), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-core | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debug | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debug-core | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debug-debuginfo | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debug-devel | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debug-modules | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debug-modules-extra | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debuginfo | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debuginfo-common-x86_64 | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-devel | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-modules | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-modules-extra | 4.18.0-553.36.1.rt7.377.el8_10 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-553.36.1.el8_10 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-core | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debug | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debug-core | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debug-debuginfo | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debug-devel | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debug-kvm | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debug-modules | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debug-modules-extra | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debuginfo | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-debuginfo-common-x86_64 | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-devel | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-kvm | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-modules | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-modules-extra | 4.18.0-553.36.1.rt7.377.el8_10 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-debug-modules-internal | 4.18.0-553.36.1.el8_10 | |
kernel-ipaclones-internal | 4.18.0-553.36.1.el8_10 | |
kernel-modules-internal | 4.18.0-553.36.1.el8_10 | |
kernel-rt-debug-modules-internal | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-modules-internal | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-rt-selftests-internal | 4.18.0-553.36.1.rt7.377.el8_10 | |
kernel-selftests-internal | 4.18.0-553.36.1.el8_10 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_553.36.1.el8_10.al8.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-553.36.1.el8_10 | |
bpftool-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel | 4.18.0-553.36.1.el8_10 | |
kernel-abi-stablelists | 4.18.0-553.36.1.el8_10 | |
kernel-core | 4.18.0-553.36.1.el8_10 | |
kernel-cross-headers | 4.18.0-553.36.1.el8_10 | |
kernel-debug | 4.18.0-553.36.1.el8_10 | |
kernel-debug-core | 4.18.0-553.36.1.el8_10 | |
kernel-debug-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-debug-devel | 4.18.0-553.36.1.el8_10 | |
kernel-debug-modules | 4.18.0-553.36.1.el8_10 | |
kernel-debug-modules-extra | 4.18.0-553.36.1.el8_10 | |
kernel-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-debuginfo-common-aarch64 | 4.18.0-553.36.1.el8_10 | |
kernel-devel | 4.18.0-553.36.1.el8_10 | |
kernel-doc | 4.18.0-553.36.1.el8_10 | |
kernel-headers | 4.18.0-553.36.1.el8_10 | |
kernel-modules | 4.18.0-553.36.1.el8_10 | |
kernel-modules-extra | 4.18.0-553.36.1.el8_10 | |
kernel-tools | 4.18.0-553.36.1.el8_10 | |
kernel-tools-debuginfo | 4.18.0-553.36.1.el8_10 | |
kernel-tools-libs | 4.18.0-553.36.1.el8_10 | |
perf | 4.18.0-553.36.1.el8_10 | |
perf-debuginfo | 4.18.0-553.36.1.el8_10 | |
python3-perf | 4.18.0-553.36.1.el8_10 | |
python3-perf-debuginfo | 4.18.0-553.36.1.el8_10 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
redis | 6.2.17-1.module_el8.10.0+3946+3de613d5 | [ALSA-2025:0595](https://errata.almalinux.org/8/ALSA-2025-0595.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2022-35977](https://access.redhat.com/security/cve/CVE-2022-35977), [CVE-2022-36021](https://access.redhat.com/security/cve/CVE-2022-36021), [CVE-2023-22458](https://access.redhat.com/security/cve/CVE-2023-22458), [CVE-2023-25155](https://access.redhat.com/security/cve/CVE-2023-25155), [CVE-2023-28856](https://access.redhat.com/security/cve/CVE-2023-28856), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis-debuginfo | 6.2.17-1.module_el8.10.0+3946+3de613d5 | |
redis-debugsource | 6.2.17-1.module_el8.10.0+3946+3de613d5 | |
redis-devel | 6.2.17-1.module_el8.10.0+3946+3de613d5 | [ALSA-2025:0595](https://errata.almalinux.org/8/ALSA-2025-0595.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2022-35977](https://access.redhat.com/security/cve/CVE-2022-35977), [CVE-2022-36021](https://access.redhat.com/security/cve/CVE-2022-36021), [CVE-2023-22458](https://access.redhat.com/security/cve/CVE-2023-22458), [CVE-2023-25155](https://access.redhat.com/security/cve/CVE-2023-25155), [CVE-2023-28856](https://access.redhat.com/security/cve/CVE-2023-28856), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))
redis-doc | 6.2.17-1.module_el8.10.0+3946+3de613d5 | [ALSA-2025:0595](https://errata.almalinux.org/8/ALSA-2025-0595.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-24834](https://access.redhat.com/security/cve/CVE-2022-24834), [CVE-2022-35977](https://access.redhat.com/security/cve/CVE-2022-35977), [CVE-2022-36021](https://access.redhat.com/security/cve/CVE-2022-36021), [CVE-2023-22458](https://access.redhat.com/security/cve/CVE-2023-22458), [CVE-2023-25155](https://access.redhat.com/security/cve/CVE-2023-25155), [CVE-2023-28856](https://access.redhat.com/security/cve/CVE-2023-28856), [CVE-2023-45145](https://access.redhat.com/security/cve/CVE-2023-45145), [CVE-2024-31228](https://access.redhat.com/security/cve/CVE-2024-31228), [CVE-2024-31449](https://access.redhat.com/security/cve/CVE-2024-31449), [CVE-2024-46981](https://access.redhat.com/security/cve/CVE-2024-46981))

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-553.36.1.el8_10 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-debug-modules-internal | 4.18.0-553.36.1.el8_10 | |
kernel-modules-internal | 4.18.0-553.36.1.el8_10 | |
kernel-selftests-internal | 4.18.0-553.36.1.el8_10 | |

