## 2024-02-07

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.6-2.al8.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.6-2.al8.cern | |

