## 2025-01-21

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grafana | 9.2.10-21.el8_10 | [ALSA-2025:0401](https://errata.almalinux.org/8/ALSA-2025-0401.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21613](https://access.redhat.com/security/cve/CVE-2025-21613), [CVE-2025-21614](https://access.redhat.com/security/cve/CVE-2025-21614))
grafana-debuginfo | 9.2.10-21.el8_10 | |
grafana-debugsource | 9.2.10-21.el8_10 | |
grafana-selinux | 9.2.10-21.el8_10 | [ALSA-2025:0401](https://errata.almalinux.org/8/ALSA-2025-0401.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21613](https://access.redhat.com/security/cve/CVE-2025-21613), [CVE-2025-21614](https://access.redhat.com/security/cve/CVE-2025-21614))

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grafana | 9.2.10-21.el8_10 | [ALSA-2025:0401](https://errata.almalinux.org/8/ALSA-2025-0401.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21613](https://access.redhat.com/security/cve/CVE-2025-21613), [CVE-2025-21614](https://access.redhat.com/security/cve/CVE-2025-21614))
grafana-debuginfo | 9.2.10-21.el8_10 | |
grafana-debugsource | 9.2.10-21.el8_10 | |
grafana-selinux | 9.2.10-21.el8_10 | [ALSA-2025:0401](https://errata.almalinux.org/8/ALSA-2025-0401.html) | <div class="adv_s">Security Advisory</div> ([CVE-2025-21613](https://access.redhat.com/security/cve/CVE-2025-21613), [CVE-2025-21614](https://access.redhat.com/security/cve/CVE-2025-21614))

