## 2023-07-17

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.20-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-runtime-7.0 | 7.0.9-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-targeting-pack-6.0 | 6.0.20-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-targeting-pack-7.0 | 7.0.9-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet | 7.0.109-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-6.0 | 6.0.20-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-6.0-debuginfo | 6.0.20-1.el8_8 | |
dotnet-apphost-pack-7.0 | 7.0.9-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-7.0-debuginfo | 7.0.9-1.el8_8 | |
dotnet-host | 7.0.9-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-host-debuginfo | 7.0.9-1.el8_8 | |
dotnet-hostfxr-6.0 | 6.0.20-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-hostfxr-6.0-debuginfo | 6.0.20-1.el8_8 | |
dotnet-hostfxr-7.0 | 7.0.9-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-hostfxr-7.0-debuginfo | 7.0.9-1.el8_8 | |
dotnet-runtime-6.0 | 6.0.20-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-runtime-6.0-debuginfo | 6.0.20-1.el8_8 | |
dotnet-runtime-7.0 | 7.0.9-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-runtime-7.0-debuginfo | 7.0.9-1.el8_8 | |
dotnet-sdk-6.0 | 6.0.120-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-6.0-debuginfo | 6.0.120-1.el8_8 | |
dotnet-sdk-7.0 | 7.0.109-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-7.0-debuginfo | 7.0.109-1.el8_8 | |
dotnet-targeting-pack-6.0 | 6.0.20-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-targeting-pack-7.0 | 7.0.9-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-templates-6.0 | 6.0.120-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-templates-7.0 | 7.0.109-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet6.0-debuginfo | 6.0.120-1.el8_8 | |
dotnet6.0-debugsource | 6.0.120-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.109-1.el8_8 | |
dotnet7.0-debugsource | 7.0.109-1.el8_8 | |
firefox | 102.13.0-2.el8_8.alma | |
firefox-debuginfo | 102.13.0-2.el8_8.alma | |
firefox-debugsource | 102.13.0-2.el8_8.alma | |
libvirt | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-client | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-client-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-config-network | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-config-nwfilter | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-interface | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-interface-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-network | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-network-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-nodedev | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-nodedev-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-nwfilter | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-nwfilter-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-qemu | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-qemu-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-secret | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-secret-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-core | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-core-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-disk | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-disk-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-gluster | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-gluster-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-iscsi | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-iscsi-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-iscsi-direct | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-iscsi-direct-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-logical | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-logical-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-mpath | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-mpath-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-rbd | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-rbd-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-scsi | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-scsi-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-kvm | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-debugsource | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-devel | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-docs | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-libs | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-libs-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-lock-sanlock | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-lock-sanlock-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-nss | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-nss-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-wireshark | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-wireshark-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
netstandard-targeting-pack-2.1 | 7.0.109-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
open-vm-tools | 12.1.5-2.el8_8.alma | |
open-vm-tools-debuginfo | 12.1.5-2.el8_8.alma | |
open-vm-tools-debugsource | 12.1.5-2.el8_8.alma | |
open-vm-tools-desktop | 12.1.5-2.el8_8.alma | |
open-vm-tools-desktop-debuginfo | 12.1.5-2.el8_8.alma | |
open-vm-tools-salt-minion | 12.1.5-2.el8_8.alma | |
open-vm-tools-sdmp | 12.1.5-2.el8_8.alma | |
open-vm-tools-sdmp-debuginfo | 12.1.5-2.el8_8.alma | |
thunderbird | 102.13.0-2.el8_8.alma | |
thunderbird-debuginfo | 102.13.0-2.el8_8.alma | |
thunderbird-debugsource | 102.13.0-2.el8_8.alma | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
open-vm-tools-devel | 12.1.5-2.el8_8.alma | |
open-vm-tools-test | 12.1.5-2.el8_8.alma | |
open-vm-tools-test-debuginfo | 12.1.5-2.el8_8.alma | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.13.0-2.el8_8.alma.plus | |
thunderbird-debuginfo | 102.13.0-2.el8_8.alma.plus | |
thunderbird-debugsource | 102.13.0-2.el8_8.alma.plus | |
thunderbird-librnp-rnp | 102.13.0-2.el8_8.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.13.0-2.el8_8.alma.plus | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.20-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-runtime-7.0 | 7.0.9-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-targeting-pack-6.0 | 6.0.20-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-targeting-pack-7.0 | 7.0.9-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet | 7.0.109-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-6.0 | 6.0.20-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-6.0-debuginfo | 6.0.20-1.el8_8 | |
dotnet-apphost-pack-7.0 | 7.0.9-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-7.0-debuginfo | 7.0.9-1.el8_8 | |
dotnet-host | 7.0.9-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-host-debuginfo | 7.0.9-1.el8_8 | |
dotnet-hostfxr-6.0 | 6.0.20-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-hostfxr-6.0-debuginfo | 6.0.20-1.el8_8 | |
dotnet-hostfxr-7.0 | 7.0.9-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-hostfxr-7.0-debuginfo | 7.0.9-1.el8_8 | |
dotnet-runtime-6.0 | 6.0.20-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-runtime-6.0-debuginfo | 6.0.20-1.el8_8 | |
dotnet-runtime-7.0 | 7.0.9-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-runtime-7.0-debuginfo | 7.0.9-1.el8_8 | |
dotnet-sdk-6.0 | 6.0.120-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-6.0-debuginfo | 6.0.120-1.el8_8 | |
dotnet-sdk-7.0 | 7.0.109-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-7.0-debuginfo | 7.0.109-1.el8_8 | |
dotnet-targeting-pack-6.0 | 6.0.20-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-targeting-pack-7.0 | 7.0.9-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-templates-6.0 | 6.0.120-1.el8_8 | [ALSA-2023:4059](https://errata.almalinux.org/8/ALSA-2023-4059.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-templates-7.0 | 7.0.109-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet6.0-debuginfo | 6.0.120-1.el8_8 | |
dotnet6.0-debugsource | 6.0.120-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.109-1.el8_8 | |
dotnet7.0-debugsource | 7.0.109-1.el8_8 | |
firefox | 102.13.0-2.el8_8.alma | |
firefox-debuginfo | 102.13.0-2.el8_8.alma | |
firefox-debugsource | 102.13.0-2.el8_8.alma | |
libvirt | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-client | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-client-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-config-network | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-config-nwfilter | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-interface | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-interface-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-network | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-network-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-nodedev | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-nodedev-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-nwfilter | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-nwfilter-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-qemu | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-qemu-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-secret | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-secret-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-core | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-core-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-disk | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-disk-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-gluster | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-gluster-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-iscsi | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-iscsi-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-iscsi-direct | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-iscsi-direct-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-logical | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-logical-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-mpath | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-mpath-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-rbd | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-rbd-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-scsi | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-driver-storage-scsi-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-daemon-kvm | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-debugsource | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-devel | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-docs | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-libs | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-libs-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-lock-sanlock | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-lock-sanlock-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-nss | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-nss-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-wireshark | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
libvirt-wireshark-debuginfo | 8.0.0-19.2.module_el8.8.0+3585+76b9c397.alma | |
netstandard-targeting-pack-2.1 | 7.0.109-1.el8_8 | [ALSA-2023:4058](https://errata.almalinux.org/8/ALSA-2023-4058.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
thunderbird | 102.13.0-2.el8_8.alma | |
thunderbird-debuginfo | 102.13.0-2.el8_8.alma | |
thunderbird-debugsource | 102.13.0-2.el8_8.alma | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.13.0-2.el8_8.alma.plus | |
thunderbird-debuginfo | 102.13.0-2.el8_8.alma.plus | |
thunderbird-debugsource | 102.13.0-2.el8_8.alma.plus | |
thunderbird-librnp-rnp | 102.13.0-2.el8_8.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.13.0-2.el8_8.alma.plus | |

