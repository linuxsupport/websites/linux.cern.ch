## 2024-11-14

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
expat | 2.2.5-16.el8_10 | [ALSA-2024:9502](https://errata.almalinux.org/8/ALSA-2024-9502.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-50602](https://access.redhat.com/security/cve/CVE-2024-50602))
expat-debuginfo | 2.2.5-16.el8_10 | |
expat-debugsource | 2.2.5-16.el8_10 | |
expat-devel | 2.2.5-16.el8_10 | [ALSA-2024:9502](https://errata.almalinux.org/8/ALSA-2024-9502.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-50602](https://access.redhat.com/security/cve/CVE-2024-50602))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
expat-static | 2.2.5-16.el8_10 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
expat | 2.2.5-16.el8_10 | [ALSA-2024:9502](https://errata.almalinux.org/8/ALSA-2024-9502.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-50602](https://access.redhat.com/security/cve/CVE-2024-50602))
expat-debuginfo | 2.2.5-16.el8_10 | |
expat-debugsource | 2.2.5-16.el8_10 | |
expat-devel | 2.2.5-16.el8_10 | [ALSA-2024:9502](https://errata.almalinux.org/8/ALSA-2024-9502.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-50602](https://access.redhat.com/security/cve/CVE-2024-50602))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
expat-static | 2.2.5-16.el8_10 | |

