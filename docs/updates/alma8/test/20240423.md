## 2024-04-23

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.10.0-1.el8_9.alma.1 | [ALSA-2024:1912](https://errata.almalinux.org/8/ALSA-2024-1912.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2609](https://access.redhat.com/security/cve/CVE-2024-2609), [CVE-2024-3852](https://access.redhat.com/security/cve/CVE-2024-3852), [CVE-2024-3854](https://access.redhat.com/security/cve/CVE-2024-3854), [CVE-2024-3857](https://access.redhat.com/security/cve/CVE-2024-3857), [CVE-2024-3859](https://access.redhat.com/security/cve/CVE-2024-3859), [CVE-2024-3861](https://access.redhat.com/security/cve/CVE-2024-3861), [CVE-2024-3864](https://access.redhat.com/security/cve/CVE-2024-3864))
firefox-debuginfo | 115.10.0-1.el8_9.alma.1 | |
firefox-debugsource | 115.10.0-1.el8_9.alma.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.10.0-1.el8_9.alma.1 | [ALSA-2024:1912](https://errata.almalinux.org/8/ALSA-2024-1912.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2609](https://access.redhat.com/security/cve/CVE-2024-2609), [CVE-2024-3852](https://access.redhat.com/security/cve/CVE-2024-3852), [CVE-2024-3854](https://access.redhat.com/security/cve/CVE-2024-3854), [CVE-2024-3857](https://access.redhat.com/security/cve/CVE-2024-3857), [CVE-2024-3859](https://access.redhat.com/security/cve/CVE-2024-3859), [CVE-2024-3861](https://access.redhat.com/security/cve/CVE-2024-3861), [CVE-2024-3864](https://access.redhat.com/security/cve/CVE-2024-3864))
firefox-debuginfo | 115.10.0-1.el8_9.alma.1 | |
firefox-debugsource | 115.10.0-1.el8_9.alma.1 | |

