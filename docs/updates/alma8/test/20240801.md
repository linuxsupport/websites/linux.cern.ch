## 2024-08-01

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
freeradius | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-debugsource | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-devel | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-doc | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-krb5 | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-krb5-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-ldap | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-ldap-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-mysql | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-mysql-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-perl | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-perl-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-postgresql | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-postgresql-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-rest | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-rest-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-sqlite | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-sqlite-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-unixODBC | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-unixODBC-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-utils | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-utils-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
python3-freeradius | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
python3-freeradius-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
shim-unsigned-aarch64-debuginfo | 15.8-2.el8.alma.1 | |
shim-unsigned-aarch64-debugsource | 15.8-2.el8.alma.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
freeradius | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-debugsource | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-devel | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-doc | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-krb5 | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-krb5-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-ldap | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-ldap-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-mysql | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-mysql-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-perl | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-perl-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-postgresql | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-postgresql-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-rest | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-rest-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-sqlite | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-sqlite-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-unixODBC | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-unixODBC-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-utils | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-utils-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
python3-freeradius | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
python3-freeradius-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
shim-unsigned-aarch64 | 15.8-2.el8.alma.1 | |
shim-unsigned-aarch64-debuginfo | 15.8-2.el8.alma.1 | |
shim-unsigned-aarch64-debugsource | 15.8-2.el8.alma.1 | |

