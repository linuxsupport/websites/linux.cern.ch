## 2023-02-13

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-redhat-oracleasm | 2.0.8-15.1.el8_7 | [RHBA-2023:0658](https://access.redhat.com/errata/RHBA-2023:0658) | <div class="adv_b">Bug Fix Advisory</div>
kmod-redhat-oracleasm-debugsource | 2.0.8-15.1.el8_7 | |
kmod-redhat-oracleasm-kernel_4_18_0_425_10_1 | 2.0.8-15.1.el8_7 | [RHBA-2023:0658](https://access.redhat.com/errata/RHBA-2023:0658) | <div class="adv_b">Bug Fix Advisory</div>
kmod-redhat-oracleasm-kernel_4_18_0_425_10_1-debuginfo | 2.0.8-15.1.el8_7 | |
kmod-redhat-oracleasm-kernel_4_18_0_425_3_1 | 2.0.8-15.1.el8_7 | [RHBA-2023:0658](https://access.redhat.com/errata/RHBA-2023:0658) | <div class="adv_b">Bug Fix Advisory</div>
kmod-redhat-oracleasm-kernel_4_18_0_425_3_1-debuginfo | 2.0.8-15.1.el8_7 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tigervnc | 1.12.0-9.el8_7.1 | |
tigervnc-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-debugsource | 1.12.0-9.el8_7.1 | |
tigervnc-icons | 1.12.0-9.el8_7.1 | |
tigervnc-license | 1.12.0-9.el8_7.1 | |
tigervnc-selinux | 1.12.0-9.el8_7.1 | |
tigervnc-server | 1.12.0-9.el8_7.1 | |
tigervnc-server-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-module | 1.12.0-9.el8_7.1 | |
tigervnc-server-module-debuginfo | 1.12.0-9.el8_7.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tigervnc | 1.12.0-9.el8_7.1 | |
tigervnc-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-debugsource | 1.12.0-9.el8_7.1 | |
tigervnc-icons | 1.12.0-9.el8_7.1 | |
tigervnc-license | 1.12.0-9.el8_7.1 | |
tigervnc-selinux | 1.12.0-9.el8_7.1 | |
tigervnc-server | 1.12.0-9.el8_7.1 | |
tigervnc-server-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal | 1.12.0-9.el8_7.1 | |
tigervnc-server-minimal-debuginfo | 1.12.0-9.el8_7.1 | |
tigervnc-server-module | 1.12.0-9.el8_7.1 | |
tigervnc-server-module-debuginfo | 1.12.0-9.el8_7.1 | |

