## 2023-09-01

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.5.6-1.el8.alma | |
sos-audit | 4.5.6-1.el8.alma | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.5.6-1.el8.alma | |
sos-audit | 4.5.6-1.el8.alma | |

