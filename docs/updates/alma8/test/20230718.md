## 2023-07-18

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kexec-tools | 2.0.25-5.el8_8.1.alma | |
kexec-tools-debuginfo | 2.0.25-5.el8_8.1.alma | |
kexec-tools-debugsource | 2.0.25-5.el8_8.1.alma | |
microcode_ctl | 20220809-2.20230214.1.el8_8.alma | |
NetworkManager | 1.40.16-3.el8_8.alma | |
NetworkManager-adsl | 1.40.16-3.el8_8.alma | |
NetworkManager-adsl-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-bluetooth | 1.40.16-3.el8_8.alma | |
NetworkManager-bluetooth-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-cloud-setup-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-config-connectivity-redhat | 1.40.16-3.el8_8.alma | |
NetworkManager-config-server | 1.40.16-3.el8_8.alma | |
NetworkManager-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-debugsource | 1.40.16-3.el8_8.alma | |
NetworkManager-dispatcher-routing-rules | 1.40.16-3.el8_8.alma | |
NetworkManager-initscripts-updown | 1.40.16-3.el8_8.alma | |
NetworkManager-libnm | 1.40.16-3.el8_8.alma | |
NetworkManager-libnm-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-ovs | 1.40.16-3.el8_8.alma | |
NetworkManager-ovs-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-ppp | 1.40.16-3.el8_8.alma | |
NetworkManager-ppp-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-team | 1.40.16-3.el8_8.alma | |
NetworkManager-team-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-tui | 1.40.16-3.el8_8.alma | |
NetworkManager-tui-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-wifi | 1.40.16-3.el8_8.alma | |
NetworkManager-wifi-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-wwan | 1.40.16-3.el8_8.alma | |
NetworkManager-wwan-debuginfo | 1.40.16-3.el8_8.alma | |
xfsprogs | 5.0.0-11.el8_8.alma | |
xfsprogs-debuginfo | 5.0.0-11.el8_8.alma | |
xfsprogs-debugsource | 5.0.0-11.el8_8.alma | |
xfsprogs-devel | 5.0.0-11.el8_8.alma | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-core | 2.14.2-4.el8_8.alma | |
ansible-test | 2.14.2-4.el8_8.alma | |
bind9.16 | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-chroot | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-debuginfo | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-debugsource | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-dnssec-utils | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-dnssec-utils-debuginfo | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-libs | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-libs-debuginfo | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-license | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-utils | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-utils-debuginfo | 9.16.23-0.14.el8_8.1.alma | |
gnome-desktop3 | 3.32.2-1.el8_8.2.alma | |
gnome-desktop3-debuginfo | 3.32.2-1.el8_8.2.alma | |
gnome-desktop3-debugsource | 3.32.2-1.el8_8.2.alma | |
gnome-desktop3-devel | 3.32.2-1.el8_8.2.alma | |
inkscape1 | 1.0.2-2.el8_8.1.alma | |
inkscape1-debuginfo | 1.0.2-2.el8_8.1.alma | |
inkscape1-debugsource | 1.0.2-2.el8_8.1.alma | |
inkscape1-docs | 1.0.2-2.el8_8.1.alma | |
inkscape1-view | 1.0.2-2.el8_8.1.alma | |
inkscape1-view-debuginfo | 1.0.2-2.el8_8.1.alma | |
ipa-client | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-client-common | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-client-debuginfo | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-client-epn | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-client-samba | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-common | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-debuginfo | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-debugsource | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-python-compat | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-selinux | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-server | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-server-common | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-server-debuginfo | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-server-dns | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-server-trust-ad | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-server-trust-ad-debuginfo | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
leapp-upgrade-el8toel9 | 0.18.0-1.el8_8.2.alma | |
leapp-upgrade-el8toel9-deps | 0.18.0-1.el8_8.2.alma | |
NetworkManager-cloud-setup | 1.40.16-3.el8_8.alma | |
nmstate | 1.4.4-2.el8_8 | |
nmstate-debuginfo | 1.4.4-2.el8_8 | |
nmstate-debugsource | 1.4.4-2.el8_8 | |
nmstate-libs | 1.4.4-2.el8_8 | |
nmstate-libs-debuginfo | 1.4.4-2.el8_8 | |
nmstate-plugin-ovsdb | 1.4.4-2.el8_8 | |
python3-bind9.16 | 9.16.23-0.14.el8_8.1.alma | |
python3-ipaclient | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
python3-ipalib | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
python3-ipaserver | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
python3-ipatests | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
python3-kdcproxy | 0.4-5.module_el8.8.0+3588+9db6b15f | |
python3-libnmstate | 1.4.4-2.el8_8 | |
rhel-system-roles | 1.21.2-1.el8_8.alma | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind9.16-devel | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-dnssec-utils | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-doc | 9.16.23-0.14.el8_8.1.alma | |
NetworkManager-libnm-devel | 1.40.16-3.el8_8.alma | |
nmstate-debuginfo | 1.4.4-2.el8_8 | |
nmstate-debugsource | 1.4.4-2.el8_8 | |
nmstate-devel | 1.4.4-2.el8_8 | |
nmstate-libs-debuginfo | 1.4.4-2.el8_8 | |
python3-bind9.16 | 9.16.23-0.14.el8_8.1.alma | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gnome-desktop3-tests | 3.32.2-1.el8_8.2.alma | |
gnome-desktop3-tests-debuginfo | 3.32.2-1.el8_8.2.alma | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kexec-tools | 2.0.25-5.el8_8.1.alma | |
kexec-tools-debuginfo | 2.0.25-5.el8_8.1.alma | |
kexec-tools-debugsource | 2.0.25-5.el8_8.1.alma | |
NetworkManager | 1.40.16-3.el8_8.alma | |
NetworkManager-adsl | 1.40.16-3.el8_8.alma | |
NetworkManager-adsl-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-bluetooth | 1.40.16-3.el8_8.alma | |
NetworkManager-bluetooth-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-cloud-setup-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-config-connectivity-redhat | 1.40.16-3.el8_8.alma | |
NetworkManager-config-server | 1.40.16-3.el8_8.alma | |
NetworkManager-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-debugsource | 1.40.16-3.el8_8.alma | |
NetworkManager-dispatcher-routing-rules | 1.40.16-3.el8_8.alma | |
NetworkManager-initscripts-updown | 1.40.16-3.el8_8.alma | |
NetworkManager-libnm | 1.40.16-3.el8_8.alma | |
NetworkManager-libnm-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-ovs | 1.40.16-3.el8_8.alma | |
NetworkManager-ovs-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-ppp | 1.40.16-3.el8_8.alma | |
NetworkManager-ppp-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-team | 1.40.16-3.el8_8.alma | |
NetworkManager-team-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-tui | 1.40.16-3.el8_8.alma | |
NetworkManager-tui-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-wifi | 1.40.16-3.el8_8.alma | |
NetworkManager-wifi-debuginfo | 1.40.16-3.el8_8.alma | |
NetworkManager-wwan | 1.40.16-3.el8_8.alma | |
NetworkManager-wwan-debuginfo | 1.40.16-3.el8_8.alma | |
xfsprogs | 5.0.0-11.el8_8.alma | |
xfsprogs-debuginfo | 5.0.0-11.el8_8.alma | |
xfsprogs-debugsource | 5.0.0-11.el8_8.alma | |
xfsprogs-devel | 5.0.0-11.el8_8.alma | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-core | 2.14.2-4.el8_8.alma | |
ansible-test | 2.14.2-4.el8_8.alma | |
bind9.16 | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-chroot | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-debuginfo | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-debugsource | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-dnssec-utils | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-dnssec-utils-debuginfo | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-libs | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-libs-debuginfo | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-license | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-utils | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-utils-debuginfo | 9.16.23-0.14.el8_8.1.alma | |
gnome-desktop3 | 3.32.2-1.el8_8.2.alma | |
gnome-desktop3-debuginfo | 3.32.2-1.el8_8.2.alma | |
gnome-desktop3-debugsource | 3.32.2-1.el8_8.2.alma | |
gnome-desktop3-devel | 3.32.2-1.el8_8.2.alma | |
inkscape1 | 1.0.2-2.el8_8.1.alma | |
inkscape1-debuginfo | 1.0.2-2.el8_8.1.alma | |
inkscape1-debugsource | 1.0.2-2.el8_8.1.alma | |
inkscape1-docs | 1.0.2-2.el8_8.1.alma | |
inkscape1-view | 1.0.2-2.el8_8.1.alma | |
inkscape1-view-debuginfo | 1.0.2-2.el8_8.1.alma | |
ipa-client | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-client-common | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-client-debuginfo | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-client-epn | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-client-samba | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-common | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-debuginfo | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-debugsource | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-python-compat | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-selinux | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-server | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-server-common | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-server-debuginfo | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-server-dns | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-server-trust-ad | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
ipa-server-trust-ad-debuginfo | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
leapp-upgrade-el8toel9 | 0.18.0-1.el8_8.2.alma | |
leapp-upgrade-el8toel9-deps | 0.18.0-1.el8_8.2.alma | |
NetworkManager-cloud-setup | 1.40.16-3.el8_8.alma | |
nmstate | 1.4.4-2.el8_8 | |
nmstate-debuginfo | 1.4.4-2.el8_8 | |
nmstate-debugsource | 1.4.4-2.el8_8 | |
nmstate-libs | 1.4.4-2.el8_8 | |
nmstate-libs-debuginfo | 1.4.4-2.el8_8 | |
nmstate-plugin-ovsdb | 1.4.4-2.el8_8 | |
python3-bind9.16 | 9.16.23-0.14.el8_8.1.alma | |
python3-ipaclient | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
python3-ipalib | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
python3-ipaserver | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
python3-ipatests | 4.9.11-6.module_el8.8.0+3588+9db6b15f.alma | |
python3-kdcproxy | 0.4-5.module_el8.8.0+3588+9db6b15f | |
python3-libnmstate | 1.4.4-2.el8_8 | |
rhel-system-roles | 1.21.2-1.el8_8.alma | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind9.16-devel | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-dnssec-utils | 9.16.23-0.14.el8_8.1.alma | |
bind9.16-doc | 9.16.23-0.14.el8_8.1.alma | |
NetworkManager-libnm-devel | 1.40.16-3.el8_8.alma | |
nmstate-debuginfo | 1.4.4-2.el8_8 | |
nmstate-debugsource | 1.4.4-2.el8_8 | |
nmstate-devel | 1.4.4-2.el8_8 | |
nmstate-libs-debuginfo | 1.4.4-2.el8_8 | |
python3-bind9.16 | 9.16.23-0.14.el8_8.1.alma | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gnome-desktop3-tests | 3.32.2-1.el8_8.2.alma | |
gnome-desktop3-tests-debuginfo | 3.32.2-1.el8_8.2.alma | |

