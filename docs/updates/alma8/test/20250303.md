## 2025-03-03

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs-filesystem | 26.1-13.el8_10 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs | 26.1-13.el8_10 | |
emacs-common | 26.1-13.el8_10 | |
emacs-common-debuginfo | 26.1-13.el8_10 | |
emacs-debuginfo | 26.1-13.el8_10 | |
emacs-debugsource | 26.1-13.el8_10 | |
emacs-lucid | 26.1-13.el8_10 | |
emacs-lucid-debuginfo | 26.1-13.el8_10 | |
emacs-nox | 26.1-13.el8_10 | |
emacs-nox-debuginfo | 26.1-13.el8_10 | |
emacs-terminal | 26.1-13.el8_10 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs-filesystem | 26.1-13.el8_10 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs | 26.1-13.el8_10 | |
emacs-common | 26.1-13.el8_10 | |
emacs-common-debuginfo | 26.1-13.el8_10 | |
emacs-debuginfo | 26.1-13.el8_10 | |
emacs-debugsource | 26.1-13.el8_10 | |
emacs-lucid | 26.1-13.el8_10 | |
emacs-lucid-debuginfo | 26.1-13.el8_10 | |
emacs-nox | 26.1-13.el8_10 | |
emacs-nox-debuginfo | 26.1-13.el8_10 | |
emacs-terminal | 26.1-13.el8_10 | |

