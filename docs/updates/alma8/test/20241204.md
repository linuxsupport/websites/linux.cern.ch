## 2024-12-04

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.8.1-1.el8_10 | |
sos-audit | 4.8.1-1.el8_10 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
webkit2gtk3 | 2.46.3-2.el8_10 | [ALSA-2024:10481](https://errata.almalinux.org/8/ALSA-2024-10481.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-debuginfo | 2.46.3-2.el8_10 | |
webkit2gtk3-debugsource | 2.46.3-2.el8_10 | |
webkit2gtk3-devel | 2.46.3-2.el8_10 | [ALSA-2024:10481](https://errata.almalinux.org/8/ALSA-2024-10481.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-devel-debuginfo | 2.46.3-2.el8_10 | |
webkit2gtk3-jsc | 2.46.3-2.el8_10 | [ALSA-2024:10481](https://errata.almalinux.org/8/ALSA-2024-10481.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-jsc-debuginfo | 2.46.3-2.el8_10 | |
webkit2gtk3-jsc-devel | 2.46.3-2.el8_10 | [ALSA-2024:10481](https://errata.almalinux.org/8/ALSA-2024-10481.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-jsc-devel-debuginfo | 2.46.3-2.el8_10 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.8.1-1.el8_10 | |
sos-audit | 4.8.1-1.el8_10 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
webkit2gtk3 | 2.46.3-2.el8_10 | [ALSA-2024:10481](https://errata.almalinux.org/8/ALSA-2024-10481.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-debuginfo | 2.46.3-2.el8_10 | |
webkit2gtk3-debugsource | 2.46.3-2.el8_10 | |
webkit2gtk3-devel | 2.46.3-2.el8_10 | [ALSA-2024:10481](https://errata.almalinux.org/8/ALSA-2024-10481.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-devel-debuginfo | 2.46.3-2.el8_10 | |
webkit2gtk3-jsc | 2.46.3-2.el8_10 | [ALSA-2024:10481](https://errata.almalinux.org/8/ALSA-2024-10481.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-jsc-debuginfo | 2.46.3-2.el8_10 | |
webkit2gtk3-jsc-devel | 2.46.3-2.el8_10 | [ALSA-2024:10481](https://errata.almalinux.org/8/ALSA-2024-10481.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-44309](https://access.redhat.com/security/cve/CVE-2024-44309))
webkit2gtk3-jsc-devel-debuginfo | 2.46.3-2.el8_10 | |

