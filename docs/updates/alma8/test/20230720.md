## 2023-07-20

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
webkit2gtk3 | 2.38.5-1.el8_8.5.alma | [ALSA-2023:4202](https://errata.almalinux.org/8/ALSA-2023-4202.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-debuginfo | 2.38.5-1.el8_8.5.alma | |
webkit2gtk3-debugsource | 2.38.5-1.el8_8.5.alma | |
webkit2gtk3-devel | 2.38.5-1.el8_8.5.alma | [ALSA-2023:4202](https://errata.almalinux.org/8/ALSA-2023-4202.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-devel-debuginfo | 2.38.5-1.el8_8.5.alma | |
webkit2gtk3-jsc | 2.38.5-1.el8_8.5.alma | [ALSA-2023:4202](https://errata.almalinux.org/8/ALSA-2023-4202.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-jsc-debuginfo | 2.38.5-1.el8_8.5.alma | |
webkit2gtk3-jsc-devel | 2.38.5-1.el8_8.5.alma | [ALSA-2023:4202](https://errata.almalinux.org/8/ALSA-2023-4202.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-jsc-devel-debuginfo | 2.38.5-1.el8_8.5.alma | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
webkit2gtk3 | 2.38.5-1.el8_8.5.alma | [ALSA-2023:4202](https://errata.almalinux.org/8/ALSA-2023-4202.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-debuginfo | 2.38.5-1.el8_8.5.alma | |
webkit2gtk3-debugsource | 2.38.5-1.el8_8.5.alma | |
webkit2gtk3-devel | 2.38.5-1.el8_8.5.alma | [ALSA-2023:4202](https://errata.almalinux.org/8/ALSA-2023-4202.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-devel-debuginfo | 2.38.5-1.el8_8.5.alma | |
webkit2gtk3-jsc | 2.38.5-1.el8_8.5.alma | [ALSA-2023:4202](https://errata.almalinux.org/8/ALSA-2023-4202.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-jsc-debuginfo | 2.38.5-1.el8_8.5.alma | |
webkit2gtk3-jsc-devel | 2.38.5-1.el8_8.5.alma | [ALSA-2023:4202](https://errata.almalinux.org/8/ALSA-2023-4202.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439))
webkit2gtk3-jsc-devel-debuginfo | 2.38.5-1.el8_8.5.alma | |

