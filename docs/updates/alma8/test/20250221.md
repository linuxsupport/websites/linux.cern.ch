## 2025-02-21

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-debuginfo | 9.11.36-16.el8_10.4 | |
bind-debugsource | 9.11.36-16.el8_10.4 | |
bind-export-devel | 9.11.36-16.el8_10.4 | |
bind-export-libs | 9.11.36-16.el8_10.4 | |
bind-export-libs-debuginfo | 9.11.36-16.el8_10.4 | |
bind-libs-debuginfo | 9.11.36-16.el8_10.4 | |
bind-libs-lite-debuginfo | 9.11.36-16.el8_10.4 | |
bind-pkcs11-debuginfo | 9.11.36-16.el8_10.4 | |
bind-pkcs11-libs-debuginfo | 9.11.36-16.el8_10.4 | |
bind-pkcs11-utils-debuginfo | 9.11.36-16.el8_10.4 | |
bind-sdb-debuginfo | 9.11.36-16.el8_10.4 | |
bind-utils-debuginfo | 9.11.36-16.el8_10.4 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind | 9.11.36-16.el8_10.4 | |
bind-chroot | 9.11.36-16.el8_10.4 | |
bind-devel | 9.11.36-16.el8_10.4 | |
bind-libs | 9.11.36-16.el8_10.4 | |
bind-libs-lite | 9.11.36-16.el8_10.4 | |
bind-license | 9.11.36-16.el8_10.4 | |
bind-lite-devel | 9.11.36-16.el8_10.4 | |
bind-pkcs11 | 9.11.36-16.el8_10.4 | |
bind-pkcs11-devel | 9.11.36-16.el8_10.4 | |
bind-pkcs11-libs | 9.11.36-16.el8_10.4 | |
bind-pkcs11-utils | 9.11.36-16.el8_10.4 | |
bind-sdb | 9.11.36-16.el8_10.4 | |
bind-sdb-chroot | 9.11.36-16.el8_10.4 | |
bind-utils | 9.11.36-16.el8_10.4 | |
bind9.16 | 9.16.23-0.22.el8_10.2 | |
bind9.16-chroot | 9.16.23-0.22.el8_10.2 | |
bind9.16-debuginfo | 9.16.23-0.22.el8_10.2 | |
bind9.16-debugsource | 9.16.23-0.22.el8_10.2 | |
bind9.16-dnssec-utils | 9.16.23-0.22.el8_10.2 | |
bind9.16-dnssec-utils-debuginfo | 9.16.23-0.22.el8_10.2 | |
bind9.16-libs | 9.16.23-0.22.el8_10.2 | |
bind9.16-libs-debuginfo | 9.16.23-0.22.el8_10.2 | |
bind9.16-license | 9.16.23-0.22.el8_10.2 | |
bind9.16-utils | 9.16.23-0.22.el8_10.2 | |
bind9.16-utils-debuginfo | 9.16.23-0.22.el8_10.2 | |
httpd | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
httpd-debuginfo | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
httpd-debugsource | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
httpd-devel | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
httpd-filesystem | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
httpd-manual | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
httpd-tools | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
httpd-tools-debuginfo | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mecab | 0.996-2.module_el8.10.0+3965+b415b607 | |
mecab-debuginfo | 0.996-2.module_el8.10.0+3965+b415b607 | |
mecab-debugsource | 0.996-2.module_el8.10.0+3965+b415b607 | |
mecab-devel | 0.996-2.module_el8.10.0+3965+b415b607 | |
mecab-ipadic | 2.7.0.20070801-17.module_el8.10.0+3965+b415b607 | |
mecab-ipadic-EUCJP | 2.7.0.20070801-17.module_el8.10.0+3965+b415b607 | |
mod_http2 | 1.15.7-10.module_el8.10.0+3966+b9483c84.3 | |
mod_http2-debuginfo | 1.15.7-10.module_el8.10.0+3966+b9483c84.3 | |
mod_http2-debugsource | 1.15.7-10.module_el8.10.0+3966+b9483c84.3 | |
mod_ldap | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mod_ldap-debuginfo | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mod_proxy_html | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mod_proxy_html-debuginfo | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mod_session | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mod_session-debuginfo | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mod_ssl | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mod_ssl-debuginfo | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mysql | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-common | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-debuginfo | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-debugsource | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-devel | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-devel-debuginfo | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-errmsg | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-libs | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-libs-debuginfo | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-server | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-server-debuginfo | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-test | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-test-debuginfo | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
python3-bind | 9.11.36-16.el8_10.4 | |
python3-bind9.16 | 9.16.23-0.22.el8_10.2 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind9.16-devel | 9.16.23-0.22.el8_10.2 | |
bind9.16-doc | 9.16.23-0.22.el8_10.2 | |

### testing x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh | 8.0p1-25.el8_10.alma.1 | |
openssh-askpass | 8.0p1-25.el8_10.alma.1 | |
openssh-cavs | 8.0p1-25.el8_10.alma.1 | |
openssh-clients | 8.0p1-25.el8_10.alma.1 | |
openssh-keycat | 8.0p1-25.el8_10.alma.1 | |
openssh-ldap | 8.0p1-25.el8_10.alma.1 | |
openssh-server | 8.0p1-25.el8_10.alma.1 | |
pam_ssh_agent_auth | 0.10.3-7.25.el8_10.alma.1 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-debuginfo | 9.11.36-16.el8_10.4 | |
bind-debugsource | 9.11.36-16.el8_10.4 | |
bind-export-devel | 9.11.36-16.el8_10.4 | |
bind-export-libs | 9.11.36-16.el8_10.4 | |
bind-export-libs-debuginfo | 9.11.36-16.el8_10.4 | |
bind-libs-debuginfo | 9.11.36-16.el8_10.4 | |
bind-libs-lite-debuginfo | 9.11.36-16.el8_10.4 | |
bind-pkcs11-debuginfo | 9.11.36-16.el8_10.4 | |
bind-pkcs11-libs-debuginfo | 9.11.36-16.el8_10.4 | |
bind-pkcs11-utils-debuginfo | 9.11.36-16.el8_10.4 | |
bind-sdb-debuginfo | 9.11.36-16.el8_10.4 | |
bind-utils-debuginfo | 9.11.36-16.el8_10.4 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind | 9.11.36-16.el8_10.4 | |
bind-chroot | 9.11.36-16.el8_10.4 | |
bind-devel | 9.11.36-16.el8_10.4 | |
bind-libs | 9.11.36-16.el8_10.4 | |
bind-libs-lite | 9.11.36-16.el8_10.4 | |
bind-license | 9.11.36-16.el8_10.4 | |
bind-lite-devel | 9.11.36-16.el8_10.4 | |
bind-pkcs11 | 9.11.36-16.el8_10.4 | |
bind-pkcs11-devel | 9.11.36-16.el8_10.4 | |
bind-pkcs11-libs | 9.11.36-16.el8_10.4 | |
bind-pkcs11-utils | 9.11.36-16.el8_10.4 | |
bind-sdb | 9.11.36-16.el8_10.4 | |
bind-sdb-chroot | 9.11.36-16.el8_10.4 | |
bind-utils | 9.11.36-16.el8_10.4 | |
bind9.16 | 9.16.23-0.22.el8_10.2 | |
bind9.16-chroot | 9.16.23-0.22.el8_10.2 | |
bind9.16-debuginfo | 9.16.23-0.22.el8_10.2 | |
bind9.16-debugsource | 9.16.23-0.22.el8_10.2 | |
bind9.16-dnssec-utils | 9.16.23-0.22.el8_10.2 | |
bind9.16-dnssec-utils-debuginfo | 9.16.23-0.22.el8_10.2 | |
bind9.16-libs | 9.16.23-0.22.el8_10.2 | |
bind9.16-libs-debuginfo | 9.16.23-0.22.el8_10.2 | |
bind9.16-license | 9.16.23-0.22.el8_10.2 | |
bind9.16-utils | 9.16.23-0.22.el8_10.2 | |
bind9.16-utils-debuginfo | 9.16.23-0.22.el8_10.2 | |
httpd | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
httpd-debuginfo | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
httpd-debugsource | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
httpd-devel | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
httpd-filesystem | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
httpd-manual | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
httpd-tools | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
httpd-tools-debuginfo | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mecab | 0.996-2.module_el8.10.0+3965+b415b607 | |
mecab-debuginfo | 0.996-2.module_el8.10.0+3965+b415b607 | |
mecab-debugsource | 0.996-2.module_el8.10.0+3965+b415b607 | |
mecab-devel | 0.996-2.module_el8.10.0+3965+b415b607 | |
mecab-ipadic | 2.7.0.20070801-17.module_el8.10.0+3965+b415b607 | |
mecab-ipadic-EUCJP | 2.7.0.20070801-17.module_el8.10.0+3965+b415b607 | |
mod_http2 | 1.15.7-10.module_el8.10.0+3966+b9483c84.3 | |
mod_http2-debuginfo | 1.15.7-10.module_el8.10.0+3966+b9483c84.3 | |
mod_http2-debugsource | 1.15.7-10.module_el8.10.0+3966+b9483c84.3 | |
mod_ldap | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mod_ldap-debuginfo | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mod_proxy_html | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mod_proxy_html-debuginfo | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mod_session | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mod_session-debuginfo | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mod_ssl | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mod_ssl-debuginfo | 2.4.37-65.module_el8.10.0+3966+b9483c84.3 | |
mysql | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-common | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-debuginfo | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-debugsource | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-devel | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-devel-debuginfo | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-errmsg | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-libs | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-libs-debuginfo | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-server | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-server-debuginfo | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-test | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
mysql-test-debuginfo | 8.0.41-1.module_el8.10.0+3965+b415b607 | |
python3-bind | 9.11.36-16.el8_10.4 | |
python3-bind9.16 | 9.16.23-0.22.el8_10.2 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind9.16-devel | 9.16.23-0.22.el8_10.2 | |
bind9.16-doc | 9.16.23-0.22.el8_10.2 | |

### testing aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssh | 8.0p1-25.el8_10.alma.1 | |
openssh-askpass | 8.0p1-25.el8_10.alma.1 | |
openssh-cavs | 8.0p1-25.el8_10.alma.1 | |
openssh-clients | 8.0p1-25.el8_10.alma.1 | |
openssh-keycat | 8.0p1-25.el8_10.alma.1 | |
openssh-ldap | 8.0p1-25.el8_10.alma.1 | |
openssh-server | 8.0p1-25.el8_10.alma.1 | |
pam_ssh_agent_auth | 0.10.3-7.25.el8_10.alma.1 | |

