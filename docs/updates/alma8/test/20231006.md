## 2023-10-06

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aardvark-dns | 1.5.0-2.module_el8.8.0+3615+3543c705 | |
buildah | 1.29.1-2.module_el8.8.0+3615+3543c705 | |
buildah-debuginfo | 1.29.1-2.module_el8.8.0+3615+3543c705 | |
buildah-debugsource | 1.29.1-2.module_el8.8.0+3615+3543c705 | |
buildah-tests | 1.29.1-2.module_el8.8.0+3615+3543c705 | |
buildah-tests-debuginfo | 1.29.1-2.module_el8.8.0+3615+3543c705 | |
cockpit-podman | 63.1-1.module_el8.8.0+3615+3543c705 | |
conmon | 2.1.6-1.module_el8.8.0+3615+3543c705 | |
conmon-debuginfo | 2.1.6-1.module_el8.8.0+3615+3543c705 | |
conmon-debugsource | 2.1.6-1.module_el8.8.0+3615+3543c705 | |
container-selinux | 2.205.0-2.module_el8.8.0+3615+3543c705 | |
containernetworking-plugins | 1.2.0-1.module_el8.8.0+3615+3543c705 | |
containernetworking-plugins-debuginfo | 1.2.0-1.module_el8.8.0+3615+3543c705 | |
containernetworking-plugins-debugsource | 1.2.0-1.module_el8.8.0+3615+3543c705 | |
containers-common | 1-64.module_el8.8.0+3615+3543c705 | |
crit | 3.15-4.module_el8.8.0+3615+3543c705 | |
criu | 3.15-4.module_el8.8.0+3615+3543c705 | |
criu-debuginfo | 3.15-4.module_el8.8.0+3615+3543c705 | |
criu-debugsource | 3.15-4.module_el8.8.0+3615+3543c705 | |
criu-devel | 3.15-4.module_el8.8.0+3615+3543c705 | |
criu-libs | 3.15-4.module_el8.8.0+3615+3543c705 | |
criu-libs-debuginfo | 3.15-4.module_el8.8.0+3615+3543c705 | |
crun | 1.8.4-2.module_el8.8.0+3615+3543c705 | |
crun-debuginfo | 1.8.4-2.module_el8.8.0+3615+3543c705 | |
crun-debugsource | 1.8.4-2.module_el8.8.0+3615+3543c705 | |
fuse-overlayfs | 1.11-1.module_el8.8.0+3615+3543c705 | |
fuse-overlayfs-debuginfo | 1.11-1.module_el8.8.0+3615+3543c705 | |
fuse-overlayfs-debugsource | 1.11-1.module_el8.8.0+3615+3543c705 | |
libslirp | 4.4.0-1.module_el8.8.0+3615+3543c705 | |
libslirp-debuginfo | 4.4.0-1.module_el8.8.0+3615+3543c705 | |
libslirp-debugsource | 4.4.0-1.module_el8.8.0+3615+3543c705 | |
libslirp-devel | 4.4.0-1.module_el8.8.0+3615+3543c705 | |
netavark | 1.5.1-2.module_el8.8.0+3615+3543c705 | |
oci-seccomp-bpf-hook | 1.2.8-1.module_el8.8.0+3615+3543c705 | |
oci-seccomp-bpf-hook-debuginfo | 1.2.8-1.module_el8.8.0+3615+3543c705 | |
oci-seccomp-bpf-hook-debugsource | 1.2.8-1.module_el8.8.0+3615+3543c705 | |
podman | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-catatonit | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-catatonit-debuginfo | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-debuginfo | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-debugsource | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-docker | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-gvproxy | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-gvproxy-debuginfo | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-plugins | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-plugins-debuginfo | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-remote | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-remote-debuginfo | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-tests | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
python3-criu | 3.15-4.module_el8.8.0+3615+3543c705 | |
python3-podman | 4.4.1-1.module_el8.8.0+3615+3543c705 | |
runc | 1.1.4-1.module_el8.8.0+3615+3543c705 | |
runc-debuginfo | 1.1.4-1.module_el8.8.0+3615+3543c705 | |
runc-debugsource | 1.1.4-1.module_el8.8.0+3615+3543c705 | |
skopeo | 1.11.2-0.2.module_el8.8.0+3615+3543c705 | |
skopeo-tests | 1.11.2-0.2.module_el8.8.0+3615+3543c705 | |
slirp4netns | 1.2.0-2.module_el8.8.0+3615+3543c705 | |
slirp4netns-debuginfo | 1.2.0-2.module_el8.8.0+3615+3543c705 | |
slirp4netns-debugsource | 1.2.0-2.module_el8.8.0+3615+3543c705 | |
toolbox | 0.0.99.3-7.module_el8.8.0+3615+3543c705 | |
toolbox-debuginfo | 0.0.99.3-7.module_el8.8.0+3615+3543c705 | |
toolbox-debugsource | 0.0.99.3-7.module_el8.8.0+3615+3543c705 | |
toolbox-tests | 0.0.99.3-7.module_el8.8.0+3615+3543c705 | |
udica | 0.2.6-20.module_el8.8.0+3615+3543c705 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_477.27.2.el8_8.al8.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aardvark-dns | 1.5.0-2.module_el8.8.0+3615+3543c705 | |
buildah | 1.29.1-2.module_el8.8.0+3615+3543c705 | |
buildah-debuginfo | 1.29.1-2.module_el8.8.0+3615+3543c705 | |
buildah-debugsource | 1.29.1-2.module_el8.8.0+3615+3543c705 | |
buildah-tests | 1.29.1-2.module_el8.8.0+3615+3543c705 | |
buildah-tests-debuginfo | 1.29.1-2.module_el8.8.0+3615+3543c705 | |
cockpit-podman | 63.1-1.module_el8.8.0+3615+3543c705 | |
conmon | 2.1.6-1.module_el8.8.0+3615+3543c705 | |
conmon-debuginfo | 2.1.6-1.module_el8.8.0+3615+3543c705 | |
conmon-debugsource | 2.1.6-1.module_el8.8.0+3615+3543c705 | |
container-selinux | 2.205.0-2.module_el8.8.0+3615+3543c705 | |
containernetworking-plugins | 1.2.0-1.module_el8.8.0+3615+3543c705 | |
containernetworking-plugins-debuginfo | 1.2.0-1.module_el8.8.0+3615+3543c705 | |
containernetworking-plugins-debugsource | 1.2.0-1.module_el8.8.0+3615+3543c705 | |
containers-common | 1-64.module_el8.8.0+3615+3543c705 | |
crit | 3.15-4.module_el8.8.0+3615+3543c705 | |
criu | 3.15-4.module_el8.8.0+3615+3543c705 | |
criu-debuginfo | 3.15-4.module_el8.8.0+3615+3543c705 | |
criu-debugsource | 3.15-4.module_el8.8.0+3615+3543c705 | |
criu-devel | 3.15-4.module_el8.8.0+3615+3543c705 | |
criu-libs | 3.15-4.module_el8.8.0+3615+3543c705 | |
criu-libs-debuginfo | 3.15-4.module_el8.8.0+3615+3543c705 | |
crun | 1.8.4-2.module_el8.8.0+3615+3543c705 | |
crun-debuginfo | 1.8.4-2.module_el8.8.0+3615+3543c705 | |
crun-debugsource | 1.8.4-2.module_el8.8.0+3615+3543c705 | |
fuse-overlayfs | 1.11-1.module_el8.8.0+3615+3543c705 | |
fuse-overlayfs-debuginfo | 1.11-1.module_el8.8.0+3615+3543c705 | |
fuse-overlayfs-debugsource | 1.11-1.module_el8.8.0+3615+3543c705 | |
libslirp | 4.4.0-1.module_el8.8.0+3615+3543c705 | |
libslirp-debuginfo | 4.4.0-1.module_el8.8.0+3615+3543c705 | |
libslirp-debugsource | 4.4.0-1.module_el8.8.0+3615+3543c705 | |
libslirp-devel | 4.4.0-1.module_el8.8.0+3615+3543c705 | |
netavark | 1.5.1-2.module_el8.8.0+3615+3543c705 | |
oci-seccomp-bpf-hook | 1.2.8-1.module_el8.8.0+3615+3543c705 | |
oci-seccomp-bpf-hook-debuginfo | 1.2.8-1.module_el8.8.0+3615+3543c705 | |
oci-seccomp-bpf-hook-debugsource | 1.2.8-1.module_el8.8.0+3615+3543c705 | |
podman | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-catatonit | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-catatonit-debuginfo | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-debuginfo | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-debugsource | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-docker | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-gvproxy | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-gvproxy-debuginfo | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-plugins | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-plugins-debuginfo | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-remote | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-remote-debuginfo | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
podman-tests | 4.4.1-16.module_el8.8.0+3615+3543c705 | |
python3-criu | 3.15-4.module_el8.8.0+3615+3543c705 | |
python3-podman | 4.4.1-1.module_el8.8.0+3615+3543c705 | |
runc | 1.1.4-1.module_el8.8.0+3615+3543c705 | |
runc-debuginfo | 1.1.4-1.module_el8.8.0+3615+3543c705 | |
runc-debugsource | 1.1.4-1.module_el8.8.0+3615+3543c705 | |
skopeo | 1.11.2-0.2.module_el8.8.0+3615+3543c705 | |
skopeo-tests | 1.11.2-0.2.module_el8.8.0+3615+3543c705 | |
slirp4netns | 1.2.0-2.module_el8.8.0+3615+3543c705 | |
slirp4netns-debuginfo | 1.2.0-2.module_el8.8.0+3615+3543c705 | |
slirp4netns-debugsource | 1.2.0-2.module_el8.8.0+3615+3543c705 | |
toolbox | 0.0.99.3-7.module_el8.8.0+3615+3543c705 | |
toolbox-debuginfo | 0.0.99.3-7.module_el8.8.0+3615+3543c705 | |
toolbox-debugsource | 0.0.99.3-7.module_el8.8.0+3615+3543c705 | |
toolbox-tests | 0.0.99.3-7.module_el8.8.0+3615+3543c705 | |
udica | 0.2.6-20.module_el8.8.0+3615+3543c705 | |

