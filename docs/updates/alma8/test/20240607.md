## 2024-06-07

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit | 310.4-1.el8_10 | [ALSA-2024:3667](https://errata.almalinux.org/8/ALSA-2024-3667.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2947](https://access.redhat.com/security/cve/CVE-2024-2947))
cockpit-bridge | 310.4-1.el8_10 | [ALSA-2024:3667](https://errata.almalinux.org/8/ALSA-2024-3667.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2947](https://access.redhat.com/security/cve/CVE-2024-2947))
cockpit-debuginfo | 310.4-1.el8_10 | |
cockpit-debugsource | 310.4-1.el8_10 | |
cockpit-doc | 310.4-1.el8_10 | [ALSA-2024:3667](https://errata.almalinux.org/8/ALSA-2024-3667.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2947](https://access.redhat.com/security/cve/CVE-2024-2947))
cockpit-system | 310.4-1.el8_10 | [ALSA-2024:3667](https://errata.almalinux.org/8/ALSA-2024-3667.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2947](https://access.redhat.com/security/cve/CVE-2024-2947))
cockpit-ws | 310.4-1.el8_10 | [ALSA-2024:3667](https://errata.almalinux.org/8/ALSA-2024-3667.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2947](https://access.redhat.com/security/cve/CVE-2024-2947))

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tomcat | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))
tomcat-admin-webapps | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))
tomcat-docs-webapp | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))
tomcat-el-3.0-api | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))
tomcat-jsp-2.3-api | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))
tomcat-lib | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))
tomcat-servlet-4.0-api | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))
tomcat-webapps | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit | 310.4-1.el8_10 | [ALSA-2024:3667](https://errata.almalinux.org/8/ALSA-2024-3667.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2947](https://access.redhat.com/security/cve/CVE-2024-2947))
cockpit-bridge | 310.4-1.el8_10 | [ALSA-2024:3667](https://errata.almalinux.org/8/ALSA-2024-3667.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2947](https://access.redhat.com/security/cve/CVE-2024-2947))
cockpit-debuginfo | 310.4-1.el8_10 | |
cockpit-debugsource | 310.4-1.el8_10 | |
cockpit-doc | 310.4-1.el8_10 | [ALSA-2024:3667](https://errata.almalinux.org/8/ALSA-2024-3667.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2947](https://access.redhat.com/security/cve/CVE-2024-2947))
cockpit-system | 310.4-1.el8_10 | [ALSA-2024:3667](https://errata.almalinux.org/8/ALSA-2024-3667.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2947](https://access.redhat.com/security/cve/CVE-2024-2947))
cockpit-ws | 310.4-1.el8_10 | [ALSA-2024:3667](https://errata.almalinux.org/8/ALSA-2024-3667.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-2947](https://access.redhat.com/security/cve/CVE-2024-2947))

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tomcat | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))
tomcat-admin-webapps | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))
tomcat-docs-webapp | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))
tomcat-el-3.0-api | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))
tomcat-jsp-2.3-api | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))
tomcat-lib | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))
tomcat-servlet-4.0-api | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))
tomcat-webapps | 9.0.87-1.el8_10.1.alma.1 | [ALSA-2024:3666](https://errata.almalinux.org/8/ALSA-2024-3666.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-23672](https://access.redhat.com/security/cve/CVE-2024-23672), [CVE-2024-24549](https://access.redhat.com/security/cve/CVE-2024-24549))

