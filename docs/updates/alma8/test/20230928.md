## 2023-09-28

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 16.20.2-2.module_el8.8.0+3614+204d6f43 | |
nodejs | 18.17.1-1.module_el8.8.0+3613+1ed8c91d | |
nodejs-debuginfo | 16.20.2-2.module_el8.8.0+3614+204d6f43 | |
nodejs-debuginfo | 18.17.1-1.module_el8.8.0+3613+1ed8c91d | |
nodejs-debugsource | 16.20.2-2.module_el8.8.0+3614+204d6f43 | |
nodejs-debugsource | 18.17.1-1.module_el8.8.0+3613+1ed8c91d | |
nodejs-devel | 16.20.2-2.module_el8.8.0+3614+204d6f43 | |
nodejs-devel | 18.17.1-1.module_el8.8.0+3613+1ed8c91d | |
nodejs-docs | 16.20.2-2.module_el8.8.0+3614+204d6f43 | |
nodejs-docs | 18.17.1-1.module_el8.8.0+3613+1ed8c91d | |
nodejs-full-i18n | 16.20.2-2.module_el8.8.0+3614+204d6f43 | |
nodejs-full-i18n | 18.17.1-1.module_el8.8.0+3613+1ed8c91d | |
nodejs-nodemon | 3.0.1-1.module_el8.8.0+3613+1ed8c91d | |
nodejs-nodemon | 3.0.1-1.module_el8.8.0+3614+204d6f43 | |
nodejs-packaging | 26-1.module_el8.8.0+3614+204d6f43 | |
npm | 8.19.4-1.16.20.2.2.module_el8.8.0+3614+204d6f43 | |
npm | 9.6.7-1.18.17.1.1.module_el8.8.0+3613+1ed8c91d | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 16.20.2-2.module_el8.8.0+3614+204d6f43 | |
nodejs | 18.17.1-1.module_el8.8.0+3613+1ed8c91d | |
nodejs-debuginfo | 16.20.2-2.module_el8.8.0+3614+204d6f43 | |
nodejs-debuginfo | 18.17.1-1.module_el8.8.0+3613+1ed8c91d | |
nodejs-debugsource | 16.20.2-2.module_el8.8.0+3614+204d6f43 | |
nodejs-debugsource | 18.17.1-1.module_el8.8.0+3613+1ed8c91d | |
nodejs-devel | 16.20.2-2.module_el8.8.0+3614+204d6f43 | |
nodejs-devel | 18.17.1-1.module_el8.8.0+3613+1ed8c91d | |
nodejs-docs | 16.20.2-2.module_el8.8.0+3614+204d6f43 | |
nodejs-docs | 18.17.1-1.module_el8.8.0+3613+1ed8c91d | |
nodejs-full-i18n | 16.20.2-2.module_el8.8.0+3614+204d6f43 | |
nodejs-full-i18n | 18.17.1-1.module_el8.8.0+3613+1ed8c91d | |
nodejs-nodemon | 3.0.1-1.module_el8.8.0+3613+1ed8c91d | |
nodejs-nodemon | 3.0.1-1.module_el8.8.0+3614+204d6f43 | |
nodejs-packaging | 26-1.module_el8.8.0+3614+204d6f43 | |
npm | 8.19.4-1.16.20.2.2.module_el8.8.0+3614+204d6f43 | |
npm | 9.6.7-1.18.17.1.1.module_el8.8.0+3613+1ed8c91d | |

