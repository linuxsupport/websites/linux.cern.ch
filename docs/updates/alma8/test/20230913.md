## 2023-09-13

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
flac-debuginfo | 1.3.2-9.el8_8.1 | |
flac-debugsource | 1.3.2-9.el8_8.1 | |
flac-libs | 1.3.2-9.el8_8.1 | [ALSA-2023:5046](https://errata.almalinux.org/8/ALSA-2023-5046.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-22219](https://access.redhat.com/security/cve/CVE-2020-22219))
flac-libs-debuginfo | 1.3.2-9.el8_8.1 | |
httpd | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
httpd-debuginfo | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | |
httpd-debugsource | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | |
httpd-devel | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
httpd-filesystem | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
httpd-manual | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
httpd-tools | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
httpd-tools-debuginfo | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | |
mod_ldap | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
mod_ldap-debuginfo | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | |
mod_proxy_html | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
mod_proxy_html-debuginfo | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | |
mod_session | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
mod_session-debuginfo | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | |
mod_ssl | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
mod_ssl-debuginfo | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
flac | 1.3.2-9.el8_8.1 | [ALSA-2023:5046](https://errata.almalinux.org/8/ALSA-2023-5046.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-22219](https://access.redhat.com/security/cve/CVE-2020-22219))
flac-devel | 1.3.2-9.el8_8.1 | [ALSA-2023:5046](https://errata.almalinux.org/8/ALSA-2023-5046.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-22219](https://access.redhat.com/security/cve/CVE-2020-22219))

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
flac-debuginfo | 1.3.2-9.el8_8.1 | |
flac-debugsource | 1.3.2-9.el8_8.1 | |
flac-libs | 1.3.2-9.el8_8.1 | [ALSA-2023:5046](https://errata.almalinux.org/8/ALSA-2023-5046.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-22219](https://access.redhat.com/security/cve/CVE-2020-22219))
flac-libs-debuginfo | 1.3.2-9.el8_8.1 | |
httpd | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
httpd-debuginfo | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | |
httpd-debugsource | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | |
httpd-devel | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
httpd-filesystem | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
httpd-manual | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
httpd-tools | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
httpd-tools-debuginfo | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | |
mod_ldap | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
mod_ldap-debuginfo | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | |
mod_proxy_html | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
mod_proxy_html-debuginfo | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | |
mod_session | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
mod_session-debuginfo | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | |
mod_ssl | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | [ALSA-2023:5050](https://errata.almalinux.org/8/ALSA-2023-5050.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-27522](https://access.redhat.com/security/cve/CVE-2023-27522))
mod_ssl-debuginfo | 2.4.37-56.module_el8.8.0+3605+a3cf1030.7 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
flac | 1.3.2-9.el8_8.1 | [ALSA-2023:5046](https://errata.almalinux.org/8/ALSA-2023-5046.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-22219](https://access.redhat.com/security/cve/CVE-2020-22219))
flac-devel | 1.3.2-9.el8_8.1 | [ALSA-2023:5046](https://errata.almalinux.org/8/ALSA-2023-5046.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-22219](https://access.redhat.com/security/cve/CVE-2020-22219))

