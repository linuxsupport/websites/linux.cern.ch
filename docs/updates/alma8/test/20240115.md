## 2024-01-15

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gnutls | 3.6.16-8.el8_9 | [ALSA-2024:0155](https://errata.almalinux.org/8/ALSA-2024-0155.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981))
gnutls-c++-debuginfo | 3.6.16-8.el8_9 | |
gnutls-dane-debuginfo | 3.6.16-8.el8_9 | |
gnutls-debuginfo | 3.6.16-8.el8_9 | |
gnutls-debugsource | 3.6.16-8.el8_9 | |
gnutls-utils-debuginfo | 3.6.16-8.el8_9 | |
libcap | 2.48-6.el8_9 | |
libcap-debuginfo | 2.48-6.el8_9 | |
libcap-debugsource | 2.48-6.el8_9 | |
libcap-devel | 2.48-6.el8_9 | |
libxml2 | 2.9.7-18.el8_9 | [ALSA-2024:0119](https://errata.almalinux.org/8/ALSA-2024-0119.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39615](https://access.redhat.com/security/cve/CVE-2023-39615))
libxml2-debuginfo | 2.9.7-18.el8_9 | |
libxml2-debugsource | 2.9.7-18.el8_9 | |
python3-libxml2 | 2.9.7-18.el8_9 | [ALSA-2024:0119](https://errata.almalinux.org/8/ALSA-2024-0119.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39615](https://access.redhat.com/security/cve/CVE-2023-39615))
python3-libxml2-debuginfo | 2.9.7-18.el8_9 | |
selinux-policy | 3.14.3-128.el8_9.1 | |
selinux-policy-devel | 3.14.3-128.el8_9.1 | |
selinux-policy-doc | 3.14.3-128.el8_9.1 | |
selinux-policy-minimum | 3.14.3-128.el8_9.1 | |
selinux-policy-mls | 3.14.3-128.el8_9.1 | |
selinux-policy-sandbox | 3.14.3-128.el8_9.1 | |
selinux-policy-targeted | 3.14.3-128.el8_9.1 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.26-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-runtime-7.0 | 7.0.15-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-runtime-8.0 | 8.0.1-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-targeting-pack-6.0 | 6.0.26-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-targeting-pack-7.0 | 7.0.15-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-targeting-pack-8.0 | 8.0.1-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
cmake | 3.26.5-1.el8_9 | |
cmake-data | 3.26.5-1.el8_9 | |
cmake-debuginfo | 3.26.5-1.el8_9 | |
cmake-debugsource | 3.26.5-1.el8_9 | |
cmake-doc | 3.26.5-1.el8_9 | |
cmake-filesystem | 3.26.5-1.el8_9 | |
cmake-gui | 3.26.5-1.el8_9 | |
cmake-gui-debuginfo | 3.26.5-1.el8_9 | |
cmake-rpm-macros | 3.26.5-1.el8_9 | |
dotnet | 8.0.101-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-apphost-pack-6.0 | 6.0.26-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-apphost-pack-6.0-debuginfo | 6.0.26-1.el8_9 | |
dotnet-apphost-pack-7.0 | 7.0.15-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-apphost-pack-7.0-debuginfo | 7.0.15-1.el8_9 | |
dotnet-apphost-pack-8.0 | 8.0.1-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-apphost-pack-8.0-debuginfo | 8.0.1-1.el8_9 | |
dotnet-host | 8.0.1-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-host-debuginfo | 8.0.1-1.el8_9 | |
dotnet-hostfxr-6.0 | 6.0.26-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-hostfxr-6.0-debuginfo | 6.0.26-1.el8_9 | |
dotnet-hostfxr-7.0 | 7.0.15-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-hostfxr-7.0-debuginfo | 7.0.15-1.el8_9 | |
dotnet-hostfxr-8.0 | 8.0.1-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-hostfxr-8.0-debuginfo | 8.0.1-1.el8_9 | |
dotnet-runtime-6.0 | 6.0.26-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-runtime-6.0-debuginfo | 6.0.26-1.el8_9 | |
dotnet-runtime-7.0 | 7.0.15-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-runtime-7.0-debuginfo | 7.0.15-1.el8_9 | |
dotnet-runtime-8.0 | 8.0.1-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-runtime-8.0-debuginfo | 8.0.1-1.el8_9 | |
dotnet-sdk-6.0 | 6.0.126-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-6.0-debuginfo | 6.0.126-1.el8_9 | |
dotnet-sdk-7.0 | 7.0.115-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-7.0-debuginfo | 7.0.115-1.el8_9 | |
dotnet-sdk-8.0 | 8.0.101-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-8.0-debuginfo | 8.0.101-1.el8_9 | |
dotnet-targeting-pack-6.0 | 6.0.26-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-targeting-pack-7.0 | 7.0.15-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-targeting-pack-8.0 | 8.0.1-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-templates-6.0 | 6.0.126-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-templates-7.0 | 7.0.115-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-templates-8.0 | 8.0.101-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet6.0-debuginfo | 6.0.126-1.el8_9 | |
dotnet6.0-debugsource | 6.0.126-1.el8_9 | |
dotnet7.0-debuginfo | 7.0.115-1.el8_9 | |
dotnet7.0-debugsource | 7.0.115-1.el8_9 | |
dotnet8.0-debuginfo | 8.0.101-1.el8_9 | |
dotnet8.0-debugsource | 8.0.101-1.el8_9 | |
fontawesome-fonts | 4.7.0-5.el8_9 | |
fontawesome-fonts-web | 4.7.0-5.el8_9 | |
gnutls-c++ | 3.6.16-8.el8_9 | [ALSA-2024:0155](https://errata.almalinux.org/8/ALSA-2024-0155.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981))
gnutls-dane | 3.6.16-8.el8_9 | [ALSA-2024:0155](https://errata.almalinux.org/8/ALSA-2024-0155.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981))
gnutls-devel | 3.6.16-8.el8_9 | [ALSA-2024:0155](https://errata.almalinux.org/8/ALSA-2024-0155.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981))
gnutls-utils | 3.6.16-8.el8_9 | [ALSA-2024:0155](https://errata.almalinux.org/8/ALSA-2024-0155.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981))
libxml2-devel | 2.9.7-18.el8_9 | [ALSA-2024:0119](https://errata.almalinux.org/8/ALSA-2024-0119.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39615](https://access.redhat.com/security/cve/CVE-2023-39615))
netstandard-targeting-pack-2.1 | 8.0.101-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
nss | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-debuginfo | 3.90.0-4.el8_9 | |
nss-debugsource | 3.90.0-4.el8_9 | |
nss-devel | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn-debuginfo | 3.90.0-4.el8_9 | |
nss-softokn-devel | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn-freebl | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn-freebl-debuginfo | 3.90.0-4.el8_9 | |
nss-softokn-freebl-devel | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-sysinit | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-sysinit-debuginfo | 3.90.0-4.el8_9 | |
nss-tools | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-tools-debuginfo | 3.90.0-4.el8_9 | |
nss-util | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-util-debuginfo | 3.90.0-4.el8_9 | |
nss-util-devel | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
pixman | 0.38.4-3.el8_9 | [ALSA-2024:0131](https://errata.almalinux.org/8/ALSA-2024-0131.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-44638](https://access.redhat.com/security/cve/CVE-2022-44638))
pixman-debuginfo | 0.38.4-3.el8_9 | |
pixman-debugsource | 0.38.4-3.el8_9 | |
pixman-devel | 0.38.4-3.el8_9 | [ALSA-2024:0131](https://errata.almalinux.org/8/ALSA-2024-0131.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-44638](https://access.redhat.com/security/cve/CVE-2022-44638))

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.126-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-7.0-source-built-artifacts | 7.0.115-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-8.0-source-built-artifacts | 8.0.101-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libcap-static | 2.48-6.el8_9 | |
libxml2-static | 2.9.7-18.el8_9 | |
nss-pkcs11-devel | 3.90.0-4.el8_9 | |
ttembed | 1.1-9.el8 | |
ttembed-debuginfo | 1.1-9.el8 | |
ttembed-debugsource | 1.1-9.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gnutls | 3.6.16-8.el8_9 | [ALSA-2024:0155](https://errata.almalinux.org/8/ALSA-2024-0155.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981))
gnutls-c++-debuginfo | 3.6.16-8.el8_9 | |
gnutls-dane-debuginfo | 3.6.16-8.el8_9 | |
gnutls-debuginfo | 3.6.16-8.el8_9 | |
gnutls-debugsource | 3.6.16-8.el8_9 | |
gnutls-utils-debuginfo | 3.6.16-8.el8_9 | |
libcap | 2.48-6.el8_9 | |
libcap-debuginfo | 2.48-6.el8_9 | |
libcap-debugsource | 2.48-6.el8_9 | |
libcap-devel | 2.48-6.el8_9 | |
libxml2 | 2.9.7-18.el8_9 | [ALSA-2024:0119](https://errata.almalinux.org/8/ALSA-2024-0119.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39615](https://access.redhat.com/security/cve/CVE-2023-39615))
libxml2-debuginfo | 2.9.7-18.el8_9 | |
libxml2-debugsource | 2.9.7-18.el8_9 | |
python3-libxml2 | 2.9.7-18.el8_9 | [ALSA-2024:0119](https://errata.almalinux.org/8/ALSA-2024-0119.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39615](https://access.redhat.com/security/cve/CVE-2023-39615))
python3-libxml2-debuginfo | 2.9.7-18.el8_9 | |
selinux-policy | 3.14.3-128.el8_9.1 | |
selinux-policy-devel | 3.14.3-128.el8_9.1 | |
selinux-policy-doc | 3.14.3-128.el8_9.1 | |
selinux-policy-minimum | 3.14.3-128.el8_9.1 | |
selinux-policy-mls | 3.14.3-128.el8_9.1 | |
selinux-policy-sandbox | 3.14.3-128.el8_9.1 | |
selinux-policy-targeted | 3.14.3-128.el8_9.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.26-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-runtime-7.0 | 7.0.15-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-runtime-8.0 | 8.0.1-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-targeting-pack-6.0 | 6.0.26-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-targeting-pack-7.0 | 7.0.15-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
aspnetcore-targeting-pack-8.0 | 8.0.1-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
cmake | 3.26.5-1.el8_9 | |
cmake-data | 3.26.5-1.el8_9 | |
cmake-debuginfo | 3.26.5-1.el8_9 | |
cmake-debugsource | 3.26.5-1.el8_9 | |
cmake-doc | 3.26.5-1.el8_9 | |
cmake-filesystem | 3.26.5-1.el8_9 | |
cmake-gui | 3.26.5-1.el8_9 | |
cmake-gui-debuginfo | 3.26.5-1.el8_9 | |
cmake-rpm-macros | 3.26.5-1.el8_9 | |
dotnet | 8.0.101-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-apphost-pack-6.0 | 6.0.26-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-apphost-pack-6.0-debuginfo | 6.0.26-1.el8_9 | |
dotnet-apphost-pack-7.0 | 7.0.15-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-apphost-pack-7.0-debuginfo | 7.0.15-1.el8_9 | |
dotnet-apphost-pack-8.0 | 8.0.1-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-apphost-pack-8.0-debuginfo | 8.0.1-1.el8_9 | |
dotnet-host | 8.0.1-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-host-debuginfo | 8.0.1-1.el8_9 | |
dotnet-hostfxr-6.0 | 6.0.26-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-hostfxr-6.0-debuginfo | 6.0.26-1.el8_9 | |
dotnet-hostfxr-7.0 | 7.0.15-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-hostfxr-7.0-debuginfo | 7.0.15-1.el8_9 | |
dotnet-hostfxr-8.0 | 8.0.1-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-hostfxr-8.0-debuginfo | 8.0.1-1.el8_9 | |
dotnet-runtime-6.0 | 6.0.26-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-runtime-6.0-debuginfo | 6.0.26-1.el8_9 | |
dotnet-runtime-7.0 | 7.0.15-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-runtime-7.0-debuginfo | 7.0.15-1.el8_9 | |
dotnet-runtime-8.0 | 8.0.1-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-runtime-8.0-debuginfo | 8.0.1-1.el8_9 | |
dotnet-sdk-6.0 | 6.0.126-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-6.0-debuginfo | 6.0.126-1.el8_9 | |
dotnet-sdk-7.0 | 7.0.115-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-7.0-debuginfo | 7.0.115-1.el8_9 | |
dotnet-sdk-8.0 | 8.0.101-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-8.0-debuginfo | 8.0.101-1.el8_9 | |
dotnet-targeting-pack-6.0 | 6.0.26-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-targeting-pack-7.0 | 7.0.15-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-targeting-pack-8.0 | 8.0.1-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-templates-6.0 | 6.0.126-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-templates-7.0 | 7.0.115-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-templates-8.0 | 8.0.101-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet6.0-debuginfo | 6.0.126-1.el8_9 | |
dotnet6.0-debugsource | 6.0.126-1.el8_9 | |
dotnet7.0-debuginfo | 7.0.115-1.el8_9 | |
dotnet7.0-debugsource | 7.0.115-1.el8_9 | |
dotnet8.0-debuginfo | 8.0.101-1.el8_9 | |
dotnet8.0-debugsource | 8.0.101-1.el8_9 | |
fontawesome-fonts | 4.7.0-5.el8_9 | |
fontawesome-fonts-web | 4.7.0-5.el8_9 | |
gnutls-c++ | 3.6.16-8.el8_9 | [ALSA-2024:0155](https://errata.almalinux.org/8/ALSA-2024-0155.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981))
gnutls-dane | 3.6.16-8.el8_9 | [ALSA-2024:0155](https://errata.almalinux.org/8/ALSA-2024-0155.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981))
gnutls-devel | 3.6.16-8.el8_9 | [ALSA-2024:0155](https://errata.almalinux.org/8/ALSA-2024-0155.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981))
gnutls-utils | 3.6.16-8.el8_9 | [ALSA-2024:0155](https://errata.almalinux.org/8/ALSA-2024-0155.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5981](https://access.redhat.com/security/cve/CVE-2023-5981))
libxml2-devel | 2.9.7-18.el8_9 | [ALSA-2024:0119](https://errata.almalinux.org/8/ALSA-2024-0119.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39615](https://access.redhat.com/security/cve/CVE-2023-39615))
netstandard-targeting-pack-2.1 | 8.0.101-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
nss | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-debuginfo | 3.90.0-4.el8_9 | |
nss-debugsource | 3.90.0-4.el8_9 | |
nss-devel | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn-debuginfo | 3.90.0-4.el8_9 | |
nss-softokn-devel | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn-freebl | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-softokn-freebl-debuginfo | 3.90.0-4.el8_9 | |
nss-softokn-freebl-devel | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-sysinit | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-sysinit-debuginfo | 3.90.0-4.el8_9 | |
nss-tools | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-tools-debuginfo | 3.90.0-4.el8_9 | |
nss-util | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
nss-util-debuginfo | 3.90.0-4.el8_9 | |
nss-util-devel | 3.90.0-4.el8_9 | [ALSA-2024:0105](https://errata.almalinux.org/8/ALSA-2024-0105.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-5388](https://access.redhat.com/security/cve/CVE-2023-5388))
pixman | 0.38.4-3.el8_9 | [ALSA-2024:0131](https://errata.almalinux.org/8/ALSA-2024-0131.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-44638](https://access.redhat.com/security/cve/CVE-2022-44638))
pixman-debuginfo | 0.38.4-3.el8_9 | |
pixman-debugsource | 0.38.4-3.el8_9 | |
pixman-devel | 0.38.4-3.el8_9 | [ALSA-2024:0131](https://errata.almalinux.org/8/ALSA-2024-0131.html) | <div class="adv_s">Security Advisory</div> ([CVE-2022-44638](https://access.redhat.com/security/cve/CVE-2022-44638))

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.126-1.el8_9 | [ALSA-2024:0158](https://errata.almalinux.org/8/ALSA-2024-0158.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-7.0-source-built-artifacts | 7.0.115-1.el8_9 | [ALSA-2024:0157](https://errata.almalinux.org/8/ALSA-2024-0157.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))
dotnet-sdk-8.0-source-built-artifacts | 8.0.101-1.el8_9 | [ALSA-2024:0150](https://errata.almalinux.org/8/ALSA-2024-0150.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0056](https://access.redhat.com/security/cve/CVE-2024-0056), [CVE-2024-0057](https://access.redhat.com/security/cve/CVE-2024-0057), [CVE-2024-21319](https://access.redhat.com/security/cve/CVE-2024-21319))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libcap-static | 2.48-6.el8_9 | |
libxml2-static | 2.9.7-18.el8_9 | |
nss-pkcs11-devel | 3.90.0-4.el8_9 | |

