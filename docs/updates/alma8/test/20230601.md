## 2023-06-01

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.4.18.0_477.13.1.el8_8.al8.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-477.13.1.el8_8 | |
bpftool-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel | 4.18.0-477.13.1.el8_8 | |
kernel-abi-stablelists | 4.18.0-477.13.1.el8_8 | |
kernel-core | 4.18.0-477.13.1.el8_8 | |
kernel-cross-headers | 4.18.0-477.13.1.el8_8 | |
kernel-debug | 4.18.0-477.13.1.el8_8 | |
kernel-debug-core | 4.18.0-477.13.1.el8_8 | |
kernel-debug-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debug-devel | 4.18.0-477.13.1.el8_8 | |
kernel-debug-modules | 4.18.0-477.13.1.el8_8 | |
kernel-debug-modules-extra | 4.18.0-477.13.1.el8_8 | |
kernel-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debuginfo-common-x86_64 | 4.18.0-477.13.1.el8_8 | |
kernel-devel | 4.18.0-477.13.1.el8_8 | |
kernel-doc | 4.18.0-477.13.1.el8_8 | |
kernel-headers | 4.18.0-477.13.1.el8_8 | |
kernel-modules | 4.18.0-477.13.1.el8_8 | |
kernel-modules-extra | 4.18.0-477.13.1.el8_8 | |
kernel-tools | 4.18.0-477.13.1.el8_8 | |
kernel-tools-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-tools-libs | 4.18.0-477.13.1.el8_8 | |
perf | 4.18.0-477.13.1.el8_8 | |
perf-debuginfo | 4.18.0-477.13.1.el8_8 | |
python3-perf | 4.18.0-477.13.1.el8_8 | |
python3-perf-debuginfo | 4.18.0-477.13.1.el8_8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-core | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debug | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debug-core | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debug-debuginfo | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debug-devel | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debug-modules | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debug-modules-extra | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debuginfo | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debuginfo-common-x86_64 | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-devel | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-modules | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-modules-extra | 4.18.0-477.13.1.rt7.276.el8_8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debug-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debuginfo-common-x86_64 | 4.18.0-477.13.1.el8_8 | |
kernel-tools-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-tools-libs-devel | 4.18.0-477.13.1.el8_8 | |
perf-debuginfo | 4.18.0-477.13.1.el8_8 | |
python3-perf-debuginfo | 4.18.0-477.13.1.el8_8 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-core | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debug | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debug-core | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debug-debuginfo | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debug-devel | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debug-kvm | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debug-modules | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debug-modules-extra | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debuginfo | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-debuginfo-common-x86_64 | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-devel | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-kvm | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-modules | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-modules-extra | 4.18.0-477.13.1.rt7.276.el8_8 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-debug-modules-internal | 4.18.0-477.13.1.el8_8 | |
kernel-ipaclones-internal | 4.18.0-477.13.1.el8_8 | |
kernel-modules-internal | 4.18.0-477.13.1.el8_8 | |
kernel-rt-debug-modules-internal | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-modules-internal | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-rt-selftests-internal | 4.18.0-477.13.1.rt7.276.el8_8 | |
kernel-selftests-internal | 4.18.0-477.13.1.el8_8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-477.13.1.el8_8 | |
bpftool-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel | 4.18.0-477.13.1.el8_8 | |
kernel-abi-stablelists | 4.18.0-477.13.1.el8_8 | |
kernel-core | 4.18.0-477.13.1.el8_8 | |
kernel-cross-headers | 4.18.0-477.13.1.el8_8 | |
kernel-debug | 4.18.0-477.13.1.el8_8 | |
kernel-debug-core | 4.18.0-477.13.1.el8_8 | |
kernel-debug-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debug-devel | 4.18.0-477.13.1.el8_8 | |
kernel-debug-modules | 4.18.0-477.13.1.el8_8 | |
kernel-debug-modules-extra | 4.18.0-477.13.1.el8_8 | |
kernel-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debuginfo-common-aarch64 | 4.18.0-477.13.1.el8_8 | |
kernel-devel | 4.18.0-477.13.1.el8_8 | |
kernel-doc | 4.18.0-477.13.1.el8_8 | |
kernel-headers | 4.18.0-477.13.1.el8_8 | |
kernel-modules | 4.18.0-477.13.1.el8_8 | |
kernel-modules-extra | 4.18.0-477.13.1.el8_8 | |
kernel-tools | 4.18.0-477.13.1.el8_8 | |
kernel-tools-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-tools-libs | 4.18.0-477.13.1.el8_8 | |
perf | 4.18.0-477.13.1.el8_8 | |
perf-debuginfo | 4.18.0-477.13.1.el8_8 | |
python3-perf | 4.18.0-477.13.1.el8_8 | |
python3-perf-debuginfo | 4.18.0-477.13.1.el8_8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debug-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-debuginfo-common-aarch64 | 4.18.0-477.13.1.el8_8 | |
kernel-tools-debuginfo | 4.18.0-477.13.1.el8_8 | |
kernel-tools-libs-devel | 4.18.0-477.13.1.el8_8 | |
perf-debuginfo | 4.18.0-477.13.1.el8_8 | |
python3-perf-debuginfo | 4.18.0-477.13.1.el8_8 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-debug-modules-internal | 4.18.0-477.13.1.el8_8 | |
kernel-modules-internal | 4.18.0-477.13.1.el8_8 | |
kernel-selftests-internal | 4.18.0-477.13.1.el8_8 | |

