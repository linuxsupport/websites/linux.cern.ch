## 2025-02-14

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libasan8 | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
libtsan2 | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-8.0 | 8.0.13-1.el8_10 | |
aspnetcore-runtime-dbg-8.0 | 8.0.13-1.el8_10 | |
aspnetcore-targeting-pack-8.0 | 8.0.13-1.el8_10 | |
dotnet-apphost-pack-8.0 | 8.0.13-1.el8_10 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.13-1.el8_10 | |
dotnet-hostfxr-8.0 | 8.0.13-1.el8_10 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.13-1.el8_10 | |
dotnet-runtime-8.0 | 8.0.13-1.el8_10 | |
dotnet-runtime-8.0-debuginfo | 8.0.13-1.el8_10 | |
dotnet-runtime-dbg-8.0 | 8.0.13-1.el8_10 | |
dotnet-sdk-8.0 | 8.0.113-1.el8_10 | |
dotnet-sdk-8.0-debuginfo | 8.0.113-1.el8_10 | |
dotnet-sdk-dbg-8.0 | 8.0.113-1.el8_10 | |
dotnet-targeting-pack-8.0 | 8.0.13-1.el8_10 | |
dotnet-templates-8.0 | 8.0.113-1.el8_10 | |
dotnet8.0-debuginfo | 8.0.113-1.el8_10 | |
dotnet8.0-debugsource | 8.0.113-1.el8_10 | |
gcc-toolset-13-gcc | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-gcc-c++ | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-gcc-c++-debuginfo | 13.3.1-2.2.el8_10 | |
gcc-toolset-13-gcc-debuginfo | 13.3.1-2.2.el8_10 | |
gcc-toolset-13-gcc-gfortran | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-gcc-gfortran-debuginfo | 13.3.1-2.2.el8_10 | |
gcc-toolset-13-gcc-plugin-annobin | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-gcc-plugin-annobin-debuginfo | 13.3.1-2.2.el8_10 | |
gcc-toolset-13-gcc-plugin-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-gcc-plugin-devel-debuginfo | 13.3.1-2.2.el8_10 | |
gcc-toolset-13-libasan-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libatomic-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libgccjit | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libgccjit-debuginfo | 13.3.1-2.2.el8_10 | |
gcc-toolset-13-libgccjit-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libitm-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-liblsan-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libquadmath-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libstdc++-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libstdc++-docs | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libtsan-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libubsan-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-offload-nvptx | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-offload-nvptx-debuginfo | 13.3.1-2.2.el8_10 | |
gcc-toolset-14-annobin-annocheck | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-annocheck-debuginfo | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-debuginfo | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-docs | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-plugin-gcc | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-plugin-gcc-debuginfo | 12.88-1.el8_10 | |
libasan8 | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
libasan8-debuginfo | 13.3.1-2.2.el8_10 | |
libhwasan-debuginfo | 13.3.1-2.2.el8_10 | |
libtsan2 | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
libtsan2-debuginfo | 13.3.1-2.2.el8_10 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-8.0-source-built-artifacts | 8.0.113-1.el8_10 | |
doxygen | 1.8.14-13.el8_10 | [ALSA-2025:1314](https://errata.almalinux.org/8/ALSA-2025-1314.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
doxygen-debuginfo | 1.8.14-13.el8_10 | |
doxygen-debugsource | 1.8.14-13.el8_10 | |
doxygen-doxywizard | 1.8.14-13.el8_10 | [ALSA-2025:1314](https://errata.almalinux.org/8/ALSA-2025-1314.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
doxygen-doxywizard-debuginfo | 1.8.14-13.el8_10 | |
doxygen-latex | 1.8.14-13.el8_10 | [ALSA-2025:1314](https://errata.almalinux.org/8/ALSA-2025-1314.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gcc-toolset-13-libhwasan-devel | 13.3.1-2.2.el8_10 | |
gcc-toolset-14-annobin-libannocheck | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-libannocheck-debuginfo | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-plugin-clang | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-plugin-clang-debuginfo | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-plugin-llvm | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-plugin-llvm-debuginfo | 12.88-1.el8_10 | |
libhwasan | 13.3.1-2.2.el8_10 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-8.0 | 8.0.13-1.el8_10 | |
aspnetcore-runtime-dbg-8.0 | 8.0.13-1.el8_10 | |
aspnetcore-targeting-pack-8.0 | 8.0.13-1.el8_10 | |
dotnet-apphost-pack-8.0 | 8.0.13-1.el8_10 | |
dotnet-apphost-pack-8.0-debuginfo | 8.0.13-1.el8_10 | |
dotnet-hostfxr-8.0 | 8.0.13-1.el8_10 | |
dotnet-hostfxr-8.0-debuginfo | 8.0.13-1.el8_10 | |
dotnet-runtime-8.0 | 8.0.13-1.el8_10 | |
dotnet-runtime-8.0-debuginfo | 8.0.13-1.el8_10 | |
dotnet-runtime-dbg-8.0 | 8.0.13-1.el8_10 | |
dotnet-sdk-8.0 | 8.0.113-1.el8_10 | |
dotnet-sdk-8.0-debuginfo | 8.0.113-1.el8_10 | |
dotnet-sdk-dbg-8.0 | 8.0.113-1.el8_10 | |
dotnet-targeting-pack-8.0 | 8.0.13-1.el8_10 | |
dotnet-templates-8.0 | 8.0.113-1.el8_10 | |
dotnet8.0-debuginfo | 8.0.113-1.el8_10 | |
dotnet8.0-debugsource | 8.0.113-1.el8_10 | |
gcc-toolset-13-gcc | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-gcc-c++ | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-gcc-c++-debuginfo | 13.3.1-2.2.el8_10 | |
gcc-toolset-13-gcc-debuginfo | 13.3.1-2.2.el8_10 | |
gcc-toolset-13-gcc-gfortran | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-gcc-gfortran-debuginfo | 13.3.1-2.2.el8_10 | |
gcc-toolset-13-gcc-plugin-annobin | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-gcc-plugin-annobin-debuginfo | 13.3.1-2.2.el8_10 | |
gcc-toolset-13-gcc-plugin-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-gcc-plugin-devel-debuginfo | 13.3.1-2.2.el8_10 | |
gcc-toolset-13-libasan-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libatomic-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libgccjit | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libgccjit-debuginfo | 13.3.1-2.2.el8_10 | |
gcc-toolset-13-libgccjit-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libitm-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-liblsan-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libstdc++-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libstdc++-docs | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libtsan-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-13-libubsan-devel | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
gcc-toolset-14-annobin-annocheck | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-annocheck-debuginfo | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-debuginfo | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-docs | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-plugin-gcc | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-plugin-gcc-debuginfo | 12.88-1.el8_10 | |
libasan8 | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
libasan8-debuginfo | 13.3.1-2.2.el8_10 | |
libhwasan-debuginfo | 13.3.1-2.2.el8_10 | |
libtsan2 | 13.3.1-2.2.el8_10 | [ALSA-2025:1306](https://errata.almalinux.org/8/ALSA-2025-1306.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
libtsan2-debuginfo | 13.3.1-2.2.el8_10 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-8.0-source-built-artifacts | 8.0.113-1.el8_10 | |
doxygen | 1.8.14-13.el8_10 | [ALSA-2025:1314](https://errata.almalinux.org/8/ALSA-2025-1314.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
doxygen-debuginfo | 1.8.14-13.el8_10 | |
doxygen-debugsource | 1.8.14-13.el8_10 | |
doxygen-doxywizard | 1.8.14-13.el8_10 | [ALSA-2025:1314](https://errata.almalinux.org/8/ALSA-2025-1314.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))
doxygen-doxywizard-debuginfo | 1.8.14-13.el8_10 | |
doxygen-latex | 1.8.14-13.el8_10 | [ALSA-2025:1314](https://errata.almalinux.org/8/ALSA-2025-1314.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-11023](https://access.redhat.com/security/cve/CVE-2020-11023))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gcc-toolset-13-libhwasan-devel | 13.3.1-2.2.el8_10 | |
gcc-toolset-14-annobin-libannocheck | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-libannocheck-debuginfo | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-plugin-clang | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-plugin-clang-debuginfo | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-plugin-llvm | 12.88-1.el8_10 | |
gcc-toolset-14-annobin-plugin-llvm-debuginfo | 12.88-1.el8_10 | |
libhwasan | 13.3.1-2.2.el8_10 | |

