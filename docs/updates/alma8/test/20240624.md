## 2024-06-24

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.11.0-1.el8_10.alma.1 | [ALSA-2024:3783](https://errata.almalinux.org/8/ALSA-2024-3783.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4367](https://access.redhat.com/security/cve/CVE-2024-4367), [CVE-2024-4767](https://access.redhat.com/security/cve/CVE-2024-4767), [CVE-2024-4768](https://access.redhat.com/security/cve/CVE-2024-4768), [CVE-2024-4769](https://access.redhat.com/security/cve/CVE-2024-4769), [CVE-2024-4770](https://access.redhat.com/security/cve/CVE-2024-4770), [CVE-2024-4777](https://access.redhat.com/security/cve/CVE-2024-4777))
firefox-debuginfo | 115.11.0-1.el8_10.alma.1 | |
firefox-debugsource | 115.11.0-1.el8_10.alma.1 | |
thunderbird | 115.11.0-1.el8_10.alma.1 | [ALSA-2024:3784](https://errata.almalinux.org/8/ALSA-2024-3784.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4367](https://access.redhat.com/security/cve/CVE-2024-4367), [CVE-2024-4767](https://access.redhat.com/security/cve/CVE-2024-4767), [CVE-2024-4768](https://access.redhat.com/security/cve/CVE-2024-4768), [CVE-2024-4769](https://access.redhat.com/security/cve/CVE-2024-4769), [CVE-2024-4770](https://access.redhat.com/security/cve/CVE-2024-4770), [CVE-2024-4777](https://access.redhat.com/security/cve/CVE-2024-4777))
thunderbird | 115.12.1-1.el8_10.alma.1 | [ALSA-2024:4036](https://errata.almalinux.org/8/ALSA-2024-4036.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5688](https://access.redhat.com/security/cve/CVE-2024-5688), [CVE-2024-5690](https://access.redhat.com/security/cve/CVE-2024-5690), [CVE-2024-5691](https://access.redhat.com/security/cve/CVE-2024-5691), [CVE-2024-5693](https://access.redhat.com/security/cve/CVE-2024-5693), [CVE-2024-5696](https://access.redhat.com/security/cve/CVE-2024-5696), [CVE-2024-5700](https://access.redhat.com/security/cve/CVE-2024-5700), [CVE-2024-5702](https://access.redhat.com/security/cve/CVE-2024-5702))
thunderbird-debuginfo | 115.11.0-1.el8_10.alma.1 | |
thunderbird-debuginfo | 115.12.1-1.el8_10.alma.1 | |
thunderbird-debugsource | 115.11.0-1.el8_10.alma.1 | |
thunderbird-debugsource | 115.12.1-1.el8_10.alma.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 115.11.0-1.el8_10.alma.1 | [ALSA-2024:3783](https://errata.almalinux.org/8/ALSA-2024-3783.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4367](https://access.redhat.com/security/cve/CVE-2024-4367), [CVE-2024-4767](https://access.redhat.com/security/cve/CVE-2024-4767), [CVE-2024-4768](https://access.redhat.com/security/cve/CVE-2024-4768), [CVE-2024-4769](https://access.redhat.com/security/cve/CVE-2024-4769), [CVE-2024-4770](https://access.redhat.com/security/cve/CVE-2024-4770), [CVE-2024-4777](https://access.redhat.com/security/cve/CVE-2024-4777))
firefox-debuginfo | 115.11.0-1.el8_10.alma.1 | |
firefox-debugsource | 115.11.0-1.el8_10.alma.1 | |
thunderbird | 115.11.0-1.el8_10.alma.1 | [ALSA-2024:3784](https://errata.almalinux.org/8/ALSA-2024-3784.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-4367](https://access.redhat.com/security/cve/CVE-2024-4367), [CVE-2024-4767](https://access.redhat.com/security/cve/CVE-2024-4767), [CVE-2024-4768](https://access.redhat.com/security/cve/CVE-2024-4768), [CVE-2024-4769](https://access.redhat.com/security/cve/CVE-2024-4769), [CVE-2024-4770](https://access.redhat.com/security/cve/CVE-2024-4770), [CVE-2024-4777](https://access.redhat.com/security/cve/CVE-2024-4777))
thunderbird | 115.12.1-1.el8_10.alma.1 | [ALSA-2024:4036](https://errata.almalinux.org/8/ALSA-2024-4036.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5688](https://access.redhat.com/security/cve/CVE-2024-5688), [CVE-2024-5690](https://access.redhat.com/security/cve/CVE-2024-5690), [CVE-2024-5691](https://access.redhat.com/security/cve/CVE-2024-5691), [CVE-2024-5693](https://access.redhat.com/security/cve/CVE-2024-5693), [CVE-2024-5696](https://access.redhat.com/security/cve/CVE-2024-5696), [CVE-2024-5700](https://access.redhat.com/security/cve/CVE-2024-5700), [CVE-2024-5702](https://access.redhat.com/security/cve/CVE-2024-5702))
thunderbird-debuginfo | 115.11.0-1.el8_10.alma.1 | |
thunderbird-debuginfo | 115.12.1-1.el8_10.alma.1 | |
thunderbird-debugsource | 115.11.0-1.el8_10.alma.1 | |
thunderbird-debugsource | 115.12.1-1.el8_10.alma.1 | |

