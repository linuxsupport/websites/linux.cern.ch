## 2022-12-02

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.0-1.al8.cern | |
hepix | 4.10.3-0.al8.cern | |
lpadmincern | 1.4.4-1.al8.cern | |
sicgsfilter | 2.0.6-4.al8.cern | |
yum-autoupdate | 4.6.1-3.al8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.0-1.al8.cern | |
hepix | 4.10.3-0.al8.cern | |
lpadmincern | 1.4.4-1.al8.cern | |
sicgsfilter | 2.0.6-4.al8.cern | |
yum-autoupdate | 4.6.1-3.al8.cern | |

