## 2024-02-12

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iwl100-firmware | 39.31.5.1-120.el8_9.1 | |
iwl1000-firmware | 39.31.5.1-120.el8_9.1 | |
iwl105-firmware | 18.168.6.1-120.el8_9.1 | |
iwl135-firmware | 18.168.6.1-120.el8_9.1 | |
iwl2000-firmware | 18.168.6.1-120.el8_9.1 | |
iwl2030-firmware | 18.168.6.1-120.el8_9.1 | |
iwl3160-firmware | 25.30.13.0-120.el8_9.1 | |
iwl3945-firmware | 15.32.2.9-120.el8_9.1 | |
iwl4965-firmware | 228.61.2.24-120.el8_9.1 | |
iwl5000-firmware | 8.83.5.1_1-120.el8_9.1 | |
iwl5150-firmware | 8.24.2.2-120.el8_9.1 | |
iwl6000-firmware | 9.221.4.1-120.el8_9.1 | |
iwl6000g2a-firmware | 18.168.6.1-120.el8_9.1 | |
iwl6000g2b-firmware | 18.168.6.1-120.el8_9.1 | |
iwl6050-firmware | 41.28.5.1-120.el8_9.1 | |
iwl7260-firmware | 25.30.13.0-120.el8_9.1 | |
libertas-sd8686-firmware | 20230824-120.git0e048b06.el8_9 | |
libertas-sd8787-firmware | 20230824-120.git0e048b06.el8_9 | |
libertas-usb8388-firmware | 20230824-120.git0e048b06.el8_9 | |
libertas-usb8388-olpc-firmware | 20230824-120.git0e048b06.el8_9 | |
linux-firmware | 20230824-120.git0e048b06.el8_9 | |
sos | 4.6.1-1.el8.alma.1 | |
sos-audit | 4.6.1-1.el8.alma.1 | |
tzdata | 2024a-1.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
osbuild-composer | 88-1.el8.alma.3 | |
osbuild-composer-core | 88-1.el8.alma.3 | |
osbuild-composer-core-debuginfo | 88-1.el8.alma.3 | |
osbuild-composer-debuginfo | 88-1.el8.alma.3 | |
osbuild-composer-debugsource | 88-1.el8.alma.3 | |
osbuild-composer-dnf-json | 88-1.el8.alma.3 | |
osbuild-composer-tests-debuginfo | 88-1.el8.alma.3 | |
osbuild-composer-worker | 88-1.el8.alma.3 | |
osbuild-composer-worker-debuginfo | 88-1.el8.alma.3 | |
tzdata-java | 2024a-1.el8 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
osbuild-composer-tests | 88-1.el8.alma.3 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iwl100-firmware | 39.31.5.1-120.el8_9.1 | |
iwl1000-firmware | 39.31.5.1-120.el8_9.1 | |
iwl105-firmware | 18.168.6.1-120.el8_9.1 | |
iwl135-firmware | 18.168.6.1-120.el8_9.1 | |
iwl2000-firmware | 18.168.6.1-120.el8_9.1 | |
iwl2030-firmware | 18.168.6.1-120.el8_9.1 | |
iwl3160-firmware | 25.30.13.0-120.el8_9.1 | |
iwl3945-firmware | 15.32.2.9-120.el8_9.1 | |
iwl4965-firmware | 228.61.2.24-120.el8_9.1 | |
iwl5000-firmware | 8.83.5.1_1-120.el8_9.1 | |
iwl5150-firmware | 8.24.2.2-120.el8_9.1 | |
iwl6000-firmware | 9.221.4.1-120.el8_9.1 | |
iwl6000g2a-firmware | 18.168.6.1-120.el8_9.1 | |
iwl6000g2b-firmware | 18.168.6.1-120.el8_9.1 | |
iwl6050-firmware | 41.28.5.1-120.el8_9.1 | |
iwl7260-firmware | 25.30.13.0-120.el8_9.1 | |
libertas-sd8686-firmware | 20230824-120.git0e048b06.el8_9 | |
libertas-sd8787-firmware | 20230824-120.git0e048b06.el8_9 | |
libertas-usb8388-firmware | 20230824-120.git0e048b06.el8_9 | |
libertas-usb8388-olpc-firmware | 20230824-120.git0e048b06.el8_9 | |
linux-firmware | 20230824-120.git0e048b06.el8_9 | |
sos | 4.6.1-1.el8.alma.1 | |
sos-audit | 4.6.1-1.el8.alma.1 | |
tzdata | 2024a-1.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
osbuild-composer | 88-1.el8.alma.3 | |
osbuild-composer-core | 88-1.el8.alma.3 | |
osbuild-composer-core-debuginfo | 88-1.el8.alma.3 | |
osbuild-composer-debuginfo | 88-1.el8.alma.3 | |
osbuild-composer-debugsource | 88-1.el8.alma.3 | |
osbuild-composer-dnf-json | 88-1.el8.alma.3 | |
osbuild-composer-tests-debuginfo | 88-1.el8.alma.3 | |
osbuild-composer-worker | 88-1.el8.alma.3 | |
osbuild-composer-worker-debuginfo | 88-1.el8.alma.3 | |
tzdata-java | 2024a-1.el8 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
osbuild-composer-tests | 88-1.el8.alma.3 | |

