# ALMA8 software repositories


<h2>AlmaLinux 8 software repositories</h2>


 <h3>System software repository</h3>

Access: Internal CERN and External Network.<br>

Support: <em>SUPPORTED</em> by <a  href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a><br>

Location:<br>

<a  href="http://linuxsoft.cern.ch/cern/alma/8/">http://linuxsoft.cern.ch/cern/alma/8/</a>

<br>



Configuration:

<pre>
/etc/yum.repos.d/almalinux-appsteam.repo
/etc/yum.repos.d/almalinux-baseo.repo
...
</pre>

<em>DO NOT DISABLE</em> in your configuration. (system updates are coming from this repository).
Configured in /etc/yum.repos.d/almalinux-*.repo to take precedence over additional
repositories listed below.

<hr>


<h3>Testing repository</h3>

Access: Internal CERN and External Network.<br>

Support: <em>SUPPORTED</em> by <a  href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a><br>

Installation: <i>dnf install cern-yum-tool && cern-yum-tool --testing</i>

Location:<br>

<a  href="http://linuxsoft.cern.ch/cern/alma/8-testing/">http://linuxsoft.cern.ch/cern/alma/8-testing/</a>

<br>

Configuration:

<pre>
/etc/yum.repos.d/almalinux-appsteam.repo
/etc/yum.repos.d/almalinux-baseo.repo
...
</pre>

Not enabled in default configuration. <em>NOT RECOMMENDED</em> for production systems. These repositories contain
packages that are expected to be released as updates in the next few days. Your help in
validating that these function properly and do not break your environment is appreciated. Please subscribe to
<a  href="https://mmm.cern.ch/public/archive-list/l/linux-announce-test/">linux-announce-test@cern.ch</a>
if you use this repository.</br>

<b>Warning:</b> Packages from this repository may occasionally be broken and may
require manual intervention.

<hr>

<h3>Additional software repository: EPEL</h3>

Access: Internal CERN and External Network.<br>

Support: <em>SUPPORTED</em> by <a  href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a><br>

Installation: <i>dnf install epel-release</i>

Location:<br>

<a href="http://linuxsoft.cern.ch/epel/8/Everything/">http://linuxsoft.cern.ch/epel/8/Everything/</a><br>

Configuration:

<pre>
/etc/yum.repos.d/epel.repo
/etc/yum.repos.d/epel-testing.repo
</pre>

This repository is:

Enabled in default configuration. This is a mirror of Extra Packages for Enterprise Linux (EPEL) repository

(see: <a  href="http://fedoraproject.org/wiki/EPEL">EPEL</a>)

<hr>
