## 2023-04-26

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libqb-debuginfo | 1.0.3-13.el8_7 | |
libqb-debugsource | 1.0.3-13.el8_7 | |
vim-debugsource | 8.0.1763-19.el8_6.4 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-apphost-pack-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-host | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host-debuginfo | 7.0.5-1.el8_7 | |
dotnet-hostfxr-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-hostfxr-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-runtime-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-runtime-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-sdk-6.0 | 6.0.116-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet-sdk-7.0 | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet-targeting-pack-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.116-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-7.0 | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet6.0-debugsource | 6.0.116-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet7.0-debugsource | 7.0.105-1.el8_7 | |
firefox | 102.10.0-1.el8_7.alma | |
firefox-debuginfo | 102.10.0-1.el8_7.alma | |
firefox-debugsource | 102.10.0-1.el8_7.alma | |
mod_wsgi-debugsource | 4.6.4-5.el8 | |
netstandard-targeting-pack-2.1 | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
nodejs | 14.21.3-1.module_el8.7.0+3551+53700ee8 | |
nodejs-debuginfo | 14.21.3-1.module_el8.7.0+3551+53700ee8 | |
nodejs-debugsource | 14.21.3-1.module_el8.7.0+3551+53700ee8 | |
nodejs-devel | 14.21.3-1.module_el8.7.0+3551+53700ee8 | |
nodejs-docs | 14.21.3-1.module_el8.7.0+3551+53700ee8 | |
nodejs-full-i18n | 14.21.3-1.module_el8.7.0+3551+53700ee8 | |
nodejs-nodemon | 2.0.20-3.module_el8.7.0+3551+53700ee8 | |
npm | 6.14.18-1.14.21.3.1.module_el8.7.0+3551+53700ee8 | |
thunderbird | 102.10.0-2.el8_7.alma | |
thunderbird-debuginfo | 102.10.0-2.el8_7.alma | |
thunderbird-debugsource | 102.10.0-2.el8_7.alma | |
vim-debugsource | 8.0.1763-19.el8_6.4 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-core | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-debug | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-debug-core | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-debug-debuginfo | 4.18.0-425.19.2.rt7.230.el8_7 | |
kernel-rt-debug-devel | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-debug-modules | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-debug-modules-extra | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-debuginfo | 4.18.0-425.19.2.rt7.230.el8_7 | |
kernel-rt-debuginfo-common-x86_64 | 4.18.0-425.19.2.rt7.230.el8_7 | |
kernel-rt-devel | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-modules | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-modules-extra | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-host-debuginfo | 7.0.5-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-sdk-6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.116-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet6.0-debugsource | 6.0.116-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet7.0-debugsource | 7.0.105-1.el8_7 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-core | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-debug | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-debug-core | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-debug-debuginfo | 4.18.0-425.19.2.rt7.230.el8_7 | |
kernel-rt-debug-devel | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-debug-kvm | 4.18.0-425.19.2.rt7.230.el8_7 | |
kernel-rt-debug-modules | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-debug-modules-extra | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-debuginfo | 4.18.0-425.19.2.rt7.230.el8_7 | |
kernel-rt-debuginfo-common-x86_64 | 4.18.0-425.19.2.rt7.230.el8_7 | |
kernel-rt-devel | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-kvm | 4.18.0-425.19.2.rt7.230.el8_7 | |
kernel-rt-modules | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))
kernel-rt-modules-extra | 4.18.0-425.19.2.rt7.230.el8_7 | [RHSA-2023:1584](https://access.redhat.com/errata/RHSA-2023:1584) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4269](https://access.redhat.com/security/cve/CVE-2022-4269), [CVE-2022-4378](https://access.redhat.com/security/cve/CVE-2022-4378), [CVE-2023-0266](https://access.redhat.com/security/cve/CVE-2023-0266), [CVE-2023-0386](https://access.redhat.com/security/cve/CVE-2023-0386))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt-debug-modules-internal | 4.18.0-425.19.2.rt7.230.el8_7 | |
kernel-rt-modules-internal | 4.18.0-425.19.2.rt7.230.el8_7 | |
kernel-rt-selftests-internal | 4.18.0-425.19.2.rt7.230.el8_7 | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.10.0-2.el8_7.alma.plus | |
thunderbird-debuginfo | 102.10.0-2.el8_7.alma.plus | |
thunderbird-debugsource | 102.10.0-2.el8_7.alma.plus | |
thunderbird-librnp-rnp | 102.10.0-2.el8_7.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.10.0-2.el8_7.alma.plus | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
syslinux-tftpboot | 6.04-6.el8 | [RHBA-2022:7806](https://access.redhat.com/errata/RHBA-2022:7806) | <div class="adv_b">Bug Fix Advisory</div>
vim-debugsource | 8.0.1763-19.el8_6.4 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-apphost-pack-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-host | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host-debuginfo | 7.0.5-1.el8_7 | |
dotnet-hostfxr-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-hostfxr-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-runtime-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-runtime-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-sdk-6.0 | 6.0.116-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet-sdk-7.0 | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet-targeting-pack-6.0 | 6.0.16-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-7.0 | 7.0.5-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-6.0 | 6.0.116-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-7.0 | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet6.0-debugsource | 6.0.116-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet7.0-debugsource | 7.0.105-1.el8_7 | |
firefox | 102.10.0-1.el8_7.alma | |
firefox-debuginfo | 102.10.0-1.el8_7.alma | |
firefox-debugsource | 102.10.0-1.el8_7.alma | |
mod_wsgi-debugsource | 4.6.4-5.el8 | |
netstandard-targeting-pack-2.1 | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
nodejs | 14.21.3-1.module_el8.7.0+3551+53700ee8 | |
nodejs-debuginfo | 14.21.3-1.module_el8.7.0+3551+53700ee8 | |
nodejs-debugsource | 14.21.3-1.module_el8.7.0+3551+53700ee8 | |
nodejs-devel | 14.21.3-1.module_el8.7.0+3551+53700ee8 | |
nodejs-docs | 14.21.3-1.module_el8.7.0+3551+53700ee8 | |
nodejs-full-i18n | 14.21.3-1.module_el8.7.0+3551+53700ee8 | |
nodejs-nodemon | 2.0.20-3.module_el8.7.0+3551+53700ee8 | |
npm | 6.14.18-1.14.21.3.1.module_el8.7.0+3551+53700ee8 | |
seabios-bin | 1.16.0-3.module_el8.7.0+3346+68867adb | |
stalld-debuginfo | 1.17.1-1.el8_7 | |
stalld-debugsource | 1.17.1-1.el8_7 | |
thunderbird | 102.10.0-2.el8_7.alma | |
thunderbird-debuginfo | 102.10.0-2.el8_7.alma | |
thunderbird-debugsource | 102.10.0-2.el8_7.alma | |
vim-debugsource | 8.0.1763-19.el8_6.4 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-host-debuginfo | 7.0.5-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-sdk-6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.116-1.el8_7 | [RHBA-2023:1755](https://access.redhat.com/errata/RHBA-2023:1755) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.105-1.el8_7 | [RHBA-2023:1754](https://access.redhat.com/errata/RHBA-2023:1754) | <div class="adv_b">Bug Fix Advisory</div>
dotnet6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet6.0-debugsource | 6.0.116-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet7.0-debugsource | 7.0.105-1.el8_7 | |
mingw32-winpthreads-debuginfo | 5.0.2-2.el8 | |
mingw64-winpthreads-debuginfo | 5.0.2-2.el8 | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.10.0-2.el8_7.alma.plus | |
thunderbird-debuginfo | 102.10.0-2.el8_7.alma.plus | |
thunderbird-debugsource | 102.10.0-2.el8_7.alma.plus | |
thunderbird-librnp-rnp | 102.10.0-2.el8_7.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.10.0-2.el8_7.alma.plus | |

