## 2024-02-07

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.9-0.al8.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tomcat | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-admin-webapps | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-docs-webapp | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-el-3.0-api | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-jsp-2.3-api | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-lib | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-servlet-4.0-api | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-webapps | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.9-0.al8.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tomcat | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-admin-webapps | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-docs-webapp | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-el-3.0-api | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-jsp-2.3-api | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-lib | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-servlet-4.0-api | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))
tomcat-webapps | 9.0.62-27.el8_9.3 | [ALSA-2024:0539](https://errata.almalinux.org/8/ALSA-2024-0539.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46589](https://access.redhat.com/security/cve/CVE-2023-46589))

