## 2024-02-28

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lpadmincern | 1.4.5-1.al8.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sudo | 1.9.5p2-1.el8_9 | |
sudo-debuginfo | 1.9.5p2-1.el8_9 | |
sudo-debugsource | 1.9.5p2-1.el8_9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.27-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-runtime-7.0 | 7.0.16-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-runtime-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-targeting-pack-6.0 | 6.0.27-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-targeting-pack-7.0 | 7.0.16-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-targeting-pack-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet | 8.0.102-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-6.0 | 6.0.27-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-6.0-debuginfo | 6.0.27-1.el8_9 | |
dotnet-apphost-pack-7.0 | 7.0.16-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-7.0-debuginfo | 7.0.16-1.el8_9 | |
dotnet-apphost-pack-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-8.0-debuginfo | 8.0.2-2.el8_9 | |
dotnet-host | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-host-debuginfo | 8.0.2-2.el8_9 | |
dotnet-hostfxr-6.0 | 6.0.27-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-hostfxr-6.0-debuginfo | 6.0.27-1.el8_9 | |
dotnet-hostfxr-7.0 | 7.0.16-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-hostfxr-7.0-debuginfo | 7.0.16-1.el8_9 | |
dotnet-hostfxr-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-hostfxr-8.0-debuginfo | 8.0.2-2.el8_9 | |
dotnet-runtime-6.0 | 6.0.27-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-6.0-debuginfo | 6.0.27-1.el8_9 | |
dotnet-runtime-7.0 | 7.0.16-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-7.0-debuginfo | 7.0.16-1.el8_9 | |
dotnet-runtime-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-8.0-debuginfo | 8.0.2-2.el8_9 | |
dotnet-sdk-6.0 | 6.0.127-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-6.0-debuginfo | 6.0.127-1.el8_9 | |
dotnet-sdk-7.0 | 7.0.116-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-7.0-debuginfo | 7.0.116-1.el8_9 | |
dotnet-sdk-8.0 | 8.0.102-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-8.0-debuginfo | 8.0.102-2.el8_9 | |
dotnet-targeting-pack-6.0 | 6.0.27-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-targeting-pack-7.0 | 7.0.16-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-targeting-pack-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-templates-6.0 | 6.0.127-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-templates-7.0 | 7.0.116-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-templates-8.0 | 8.0.102-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet6.0-debuginfo | 6.0.127-1.el8_9 | |
dotnet6.0-debugsource | 6.0.127-1.el8_9 | |
dotnet7.0-debuginfo | 7.0.116-1.el8_9 | |
dotnet7.0-debugsource | 7.0.116-1.el8_9 | |
dotnet8.0-debuginfo | 8.0.102-2.el8_9 | |
dotnet8.0-debugsource | 8.0.102-2.el8_9 | |
netstandard-targeting-pack-2.1 | 8.0.102-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
nss | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-debuginfo | 3.90.0-6.el8_9 | |
nss-debugsource | 3.90.0-6.el8_9 | |
nss-devel | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-debuginfo | 3.90.0-6.el8_9 | |
nss-softokn-devel | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-freebl | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-freebl-debuginfo | 3.90.0-6.el8_9 | |
nss-softokn-freebl-devel | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-sysinit | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-sysinit-debuginfo | 3.90.0-6.el8_9 | |
nss-tools | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-tools-debuginfo | 3.90.0-6.el8_9 | |
nss-util | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-util-debuginfo | 3.90.0-6.el8_9 | |
nss-util-devel | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
osbuild | 93-1.el8_9.1.alma.1 | |
osbuild-luks2 | 93-1.el8_9.1.alma.1 | |
osbuild-lvm2 | 93-1.el8_9.1.alma.1 | |
osbuild-ostree | 93-1.el8_9.1.alma.1 | |
osbuild-selinux | 93-1.el8_9.1.alma.1 | |
python3-osbuild | 93-1.el8_9.1.alma.1 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.127-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-7.0-source-built-artifacts | 7.0.116-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-8.0-source-built-artifacts | 8.0.102-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-dbg-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-dbg-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-dbg-8.0 | 8.0.102-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
nss-pkcs11-devel | 3.90.0-6.el8_9 | |
osbuild-tools | 93-1.el8_9.1.alma.1 | |
sudo-devel | 1.9.5p2-1.el8_9 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lpadmincern | 1.4.5-1.al8.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sudo | 1.9.5p2-1.el8_9 | |
sudo-debuginfo | 1.9.5p2-1.el8_9 | |
sudo-debugsource | 1.9.5p2-1.el8_9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.27-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-runtime-7.0 | 7.0.16-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-runtime-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-targeting-pack-6.0 | 6.0.27-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-targeting-pack-7.0 | 7.0.16-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
aspnetcore-targeting-pack-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet | 8.0.102-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-6.0 | 6.0.27-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-6.0-debuginfo | 6.0.27-1.el8_9 | |
dotnet-apphost-pack-7.0 | 7.0.16-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-7.0-debuginfo | 7.0.16-1.el8_9 | |
dotnet-apphost-pack-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-apphost-pack-8.0-debuginfo | 8.0.2-2.el8_9 | |
dotnet-host | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-host-debuginfo | 8.0.2-2.el8_9 | |
dotnet-hostfxr-6.0 | 6.0.27-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-hostfxr-6.0-debuginfo | 6.0.27-1.el8_9 | |
dotnet-hostfxr-7.0 | 7.0.16-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-hostfxr-7.0-debuginfo | 7.0.16-1.el8_9 | |
dotnet-hostfxr-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-hostfxr-8.0-debuginfo | 8.0.2-2.el8_9 | |
dotnet-runtime-6.0 | 6.0.27-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-6.0-debuginfo | 6.0.27-1.el8_9 | |
dotnet-runtime-7.0 | 7.0.16-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-7.0-debuginfo | 7.0.16-1.el8_9 | |
dotnet-runtime-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-8.0-debuginfo | 8.0.2-2.el8_9 | |
dotnet-sdk-6.0 | 6.0.127-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-6.0-debuginfo | 6.0.127-1.el8_9 | |
dotnet-sdk-7.0 | 7.0.116-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-7.0-debuginfo | 7.0.116-1.el8_9 | |
dotnet-sdk-8.0 | 8.0.102-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-8.0-debuginfo | 8.0.102-2.el8_9 | |
dotnet-targeting-pack-6.0 | 6.0.27-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-targeting-pack-7.0 | 7.0.16-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-targeting-pack-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-templates-6.0 | 6.0.127-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-templates-7.0 | 7.0.116-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-templates-8.0 | 8.0.102-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet6.0-debuginfo | 6.0.127-1.el8_9 | |
dotnet6.0-debugsource | 6.0.127-1.el8_9 | |
dotnet7.0-debuginfo | 7.0.116-1.el8_9 | |
dotnet7.0-debugsource | 7.0.116-1.el8_9 | |
dotnet8.0-debuginfo | 8.0.102-2.el8_9 | |
dotnet8.0-debugsource | 8.0.102-2.el8_9 | |
netstandard-targeting-pack-2.1 | 8.0.102-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
nss | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-debuginfo | 3.90.0-6.el8_9 | |
nss-debugsource | 3.90.0-6.el8_9 | |
nss-devel | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-debuginfo | 3.90.0-6.el8_9 | |
nss-softokn-devel | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-freebl | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-freebl-debuginfo | 3.90.0-6.el8_9 | |
nss-softokn-freebl-devel | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-sysinit | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-sysinit-debuginfo | 3.90.0-6.el8_9 | |
nss-tools | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-tools-debuginfo | 3.90.0-6.el8_9 | |
nss-util | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-util-debuginfo | 3.90.0-6.el8_9 | |
nss-util-devel | 3.90.0-6.el8_9 | [ALSA-2024:0786](https://errata.almalinux.org/8/ALSA-2024-0786.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
osbuild | 93-1.el8_9.1.alma.1 | |
osbuild-luks2 | 93-1.el8_9.1.alma.1 | |
osbuild-lvm2 | 93-1.el8_9.1.alma.1 | |
osbuild-ostree | 93-1.el8_9.1.alma.1 | |
osbuild-selinux | 93-1.el8_9.1.alma.1 | |
python3-osbuild | 93-1.el8_9.1.alma.1 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.127-1.el8_9 | [ALSA-2024:0808](https://errata.almalinux.org/8/ALSA-2024-0808.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-7.0-source-built-artifacts | 7.0.116-1.el8_9 | [ALSA-2024:0806](https://errata.almalinux.org/8/ALSA-2024-0806.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-8.0-source-built-artifacts | 8.0.102-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-dbg-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-runtime-dbg-8.0 | 8.0.2-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
dotnet-sdk-dbg-8.0 | 8.0.102-2.el8_9 | [ALSA-2024:0827](https://errata.almalinux.org/8/ALSA-2024-0827.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21386](https://access.redhat.com/security/cve/CVE-2024-21386), [CVE-2024-21404](https://access.redhat.com/security/cve/CVE-2024-21404))
nss-pkcs11-devel | 3.90.0-6.el8_9 | |
osbuild-tools | 93-1.el8_9.1.alma.1 | |
sudo-devel | 1.9.5p2-1.el8_9 | |

