## 2024-08-14

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
freeradius | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-debugsource | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-devel | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-doc | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-krb5 | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-krb5-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-ldap | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-ldap-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-mysql | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-mysql-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-perl | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-perl-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-postgresql | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-postgresql-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-rest | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-rest-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-sqlite | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-sqlite-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-unixODBC | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-unixODBC-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-utils | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-utils-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
python3-freeradius | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
python3-freeradius-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
shim-unsigned-aarch64-debuginfo | 15.8-2.el8.alma.1 | |
shim-unsigned-aarch64-debugsource | 15.8-2.el8.alma.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
freeradius | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-debugsource | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-devel | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-doc | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-krb5 | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-krb5-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-ldap | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-ldap-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-mysql | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-mysql-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-perl | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-perl-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-postgresql | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-postgresql-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-rest | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-rest-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-sqlite | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-sqlite-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-unixODBC | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-unixODBC-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
freeradius-utils | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
freeradius-utils-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |
python3-freeradius | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | [ALSA-2024:4936](https://errata.almalinux.org/8/ALSA-2024-4936.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-3596](https://access.redhat.com/security/cve/CVE-2024-3596))
python3-freeradius-debuginfo | 3.0.20-15.module_el8.10.0+3873+5b7fed0f | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
shim-unsigned-aarch64 | 15.8-2.el8.alma.1 | |
shim-unsigned-aarch64-debuginfo | 15.8-2.el8.alma.1 | |
shim-unsigned-aarch64-debugsource | 15.8-2.el8.alma.1 | |

