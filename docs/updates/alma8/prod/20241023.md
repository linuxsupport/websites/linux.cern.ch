## 2024-10-23

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.12-1.al8.cern | |
hepix | 4.10.13-0.al8.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssl | 1.1.1k-14.el8_6 | [ALSA-2024:7848](https://errata.almalinux.org/8/ALSA-2024-7848.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5535](https://access.redhat.com/security/cve/CVE-2024-5535))
openssl-debuginfo | 1.1.1k-14.el8_6 | |
openssl-debugsource | 1.1.1k-14.el8_6 | |
openssl-devel | 1.1.1k-14.el8_6 | [ALSA-2024:7848](https://errata.almalinux.org/8/ALSA-2024-7848.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5535](https://access.redhat.com/security/cve/CVE-2024-5535))
openssl-libs | 1.1.1k-14.el8_6 | [ALSA-2024:7848](https://errata.almalinux.org/8/ALSA-2024-7848.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5535](https://access.redhat.com/security/cve/CVE-2024-5535))
openssl-libs-debuginfo | 1.1.1k-14.el8_6 | |
openssl-perl | 1.1.1k-14.el8_6 | [ALSA-2024:7848](https://errata.almalinux.org/8/ALSA-2024-7848.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5535](https://access.redhat.com/security/cve/CVE-2024-5535))

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.35-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-runtime-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-runtime-dbg-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-targeting-pack-6.0 | 6.0.35-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-targeting-pack-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet | 8.0.110-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-apphost-pack-6.0 | 6.0.35-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-apphost-pack-6.0-debuginfo | 6.0.35-1.el8_10 | |
dotnet-apphost-pack-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-apphost-pack-8.0-debuginfo | 8.0.10-1.el8_10 | |
dotnet-host | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-host-debuginfo | 8.0.10-1.el8_10 | |
dotnet-hostfxr-6.0 | 6.0.35-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-hostfxr-6.0-debuginfo | 6.0.35-1.el8_10 | |
dotnet-hostfxr-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-hostfxr-8.0-debuginfo | 8.0.10-1.el8_10 | |
dotnet-runtime-6.0 | 6.0.35-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-runtime-6.0-debuginfo | 6.0.35-1.el8_10 | |
dotnet-runtime-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-runtime-8.0-debuginfo | 8.0.10-1.el8_10 | |
dotnet-runtime-dbg-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-6.0 | 6.0.135-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-6.0-debuginfo | 6.0.135-1.el8_10 | |
dotnet-sdk-8.0 | 8.0.110-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-8.0-debuginfo | 8.0.110-1.el8_10 | |
dotnet-sdk-dbg-8.0 | 8.0.110-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-targeting-pack-6.0 | 6.0.35-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-targeting-pack-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-templates-6.0 | 6.0.135-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-templates-8.0 | 8.0.110-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet6.0-debuginfo | 6.0.135-1.el8_10 | |
dotnet6.0-debugsource | 6.0.135-1.el8_10 | |
dotnet8.0-debuginfo | 8.0.110-1.el8_10 | |
dotnet8.0-debugsource | 8.0.110-1.el8_10 | |
firefox | 128.3.0-1.el8_10.alma.1 | [ALSA-2024:7700](https://errata.almalinux.org/8/ALSA-2024-7700.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-8900](https://access.redhat.com/security/cve/CVE-2024-8900), [CVE-2024-9392](https://access.redhat.com/security/cve/CVE-2024-9392), [CVE-2024-9393](https://access.redhat.com/security/cve/CVE-2024-9393), [CVE-2024-9394](https://access.redhat.com/security/cve/CVE-2024-9394), [CVE-2024-9396](https://access.redhat.com/security/cve/CVE-2024-9396), [CVE-2024-9397](https://access.redhat.com/security/cve/CVE-2024-9397), [CVE-2024-9398](https://access.redhat.com/security/cve/CVE-2024-9398), [CVE-2024-9399](https://access.redhat.com/security/cve/CVE-2024-9399), [CVE-2024-9400](https://access.redhat.com/security/cve/CVE-2024-9400), [CVE-2024-9401](https://access.redhat.com/security/cve/CVE-2024-9401), [CVE-2024-9402](https://access.redhat.com/security/cve/CVE-2024-9402))
firefox-debuginfo | 128.3.0-1.el8_10.alma.1 | |
firefox-debugsource | 128.3.0-1.el8_10.alma.1 | |
netstandard-targeting-pack-2.1 | 8.0.110-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
thunderbird | 128.3.0-1.el8_10.alma.1 | [ALSA-2024:7699](https://errata.almalinux.org/8/ALSA-2024-7699.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9392](https://access.redhat.com/security/cve/CVE-2024-9392), [CVE-2024-9393](https://access.redhat.com/security/cve/CVE-2024-9393), [CVE-2024-9394](https://access.redhat.com/security/cve/CVE-2024-9394), [CVE-2024-9396](https://access.redhat.com/security/cve/CVE-2024-9396), [CVE-2024-9397](https://access.redhat.com/security/cve/CVE-2024-9397), [CVE-2024-9398](https://access.redhat.com/security/cve/CVE-2024-9398), [CVE-2024-9399](https://access.redhat.com/security/cve/CVE-2024-9399), [CVE-2024-9400](https://access.redhat.com/security/cve/CVE-2024-9400), [CVE-2024-9401](https://access.redhat.com/security/cve/CVE-2024-9401), [CVE-2024-9402](https://access.redhat.com/security/cve/CVE-2024-9402), [CVE-2024-9403](https://access.redhat.com/security/cve/CVE-2024-9403))
thunderbird-debuginfo | 128.3.0-1.el8_10.alma.1 | |
thunderbird-debugsource | 128.3.0-1.el8_10.alma.1 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.135-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-8.0-source-built-artifacts | 8.0.110-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssl-static | 1.1.1k-14.el8_6 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.12-1.al8.cern | |
hepix | 4.10.13-0.al8.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssl | 1.1.1k-14.el8_6 | [ALSA-2024:7848](https://errata.almalinux.org/8/ALSA-2024-7848.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5535](https://access.redhat.com/security/cve/CVE-2024-5535))
openssl-debuginfo | 1.1.1k-14.el8_6 | |
openssl-debugsource | 1.1.1k-14.el8_6 | |
openssl-devel | 1.1.1k-14.el8_6 | [ALSA-2024:7848](https://errata.almalinux.org/8/ALSA-2024-7848.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5535](https://access.redhat.com/security/cve/CVE-2024-5535))
openssl-libs | 1.1.1k-14.el8_6 | [ALSA-2024:7848](https://errata.almalinux.org/8/ALSA-2024-7848.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5535](https://access.redhat.com/security/cve/CVE-2024-5535))
openssl-libs-debuginfo | 1.1.1k-14.el8_6 | |
openssl-perl | 1.1.1k-14.el8_6 | [ALSA-2024:7848](https://errata.almalinux.org/8/ALSA-2024-7848.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-5535](https://access.redhat.com/security/cve/CVE-2024-5535))

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.35-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-runtime-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-runtime-dbg-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-targeting-pack-6.0 | 6.0.35-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
aspnetcore-targeting-pack-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet | 8.0.110-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-apphost-pack-6.0 | 6.0.35-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-apphost-pack-6.0-debuginfo | 6.0.35-1.el8_10 | |
dotnet-apphost-pack-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-apphost-pack-8.0-debuginfo | 8.0.10-1.el8_10 | |
dotnet-host | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-host-debuginfo | 8.0.10-1.el8_10 | |
dotnet-hostfxr-6.0 | 6.0.35-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-hostfxr-6.0-debuginfo | 6.0.35-1.el8_10 | |
dotnet-hostfxr-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-hostfxr-8.0-debuginfo | 8.0.10-1.el8_10 | |
dotnet-runtime-6.0 | 6.0.35-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-runtime-6.0-debuginfo | 6.0.35-1.el8_10 | |
dotnet-runtime-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-runtime-8.0-debuginfo | 8.0.10-1.el8_10 | |
dotnet-runtime-dbg-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-6.0 | 6.0.135-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-6.0-debuginfo | 6.0.135-1.el8_10 | |
dotnet-sdk-8.0 | 8.0.110-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-8.0-debuginfo | 8.0.110-1.el8_10 | |
dotnet-sdk-dbg-8.0 | 8.0.110-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-targeting-pack-6.0 | 6.0.35-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-targeting-pack-8.0 | 8.0.10-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-templates-6.0 | 6.0.135-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-templates-8.0 | 8.0.110-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet6.0-debuginfo | 6.0.135-1.el8_10 | |
dotnet6.0-debugsource | 6.0.135-1.el8_10 | |
dotnet8.0-debuginfo | 8.0.110-1.el8_10 | |
dotnet8.0-debugsource | 8.0.110-1.el8_10 | |
firefox | 128.3.0-1.el8_10.alma.1 | [ALSA-2024:7700](https://errata.almalinux.org/8/ALSA-2024-7700.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-8900](https://access.redhat.com/security/cve/CVE-2024-8900), [CVE-2024-9392](https://access.redhat.com/security/cve/CVE-2024-9392), [CVE-2024-9393](https://access.redhat.com/security/cve/CVE-2024-9393), [CVE-2024-9394](https://access.redhat.com/security/cve/CVE-2024-9394), [CVE-2024-9396](https://access.redhat.com/security/cve/CVE-2024-9396), [CVE-2024-9397](https://access.redhat.com/security/cve/CVE-2024-9397), [CVE-2024-9398](https://access.redhat.com/security/cve/CVE-2024-9398), [CVE-2024-9399](https://access.redhat.com/security/cve/CVE-2024-9399), [CVE-2024-9400](https://access.redhat.com/security/cve/CVE-2024-9400), [CVE-2024-9401](https://access.redhat.com/security/cve/CVE-2024-9401), [CVE-2024-9402](https://access.redhat.com/security/cve/CVE-2024-9402))
firefox-debuginfo | 128.3.0-1.el8_10.alma.1 | |
firefox-debugsource | 128.3.0-1.el8_10.alma.1 | |
netstandard-targeting-pack-2.1 | 8.0.110-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
thunderbird | 128.3.0-1.el8_10.alma.1 | [ALSA-2024:7699](https://errata.almalinux.org/8/ALSA-2024-7699.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-9392](https://access.redhat.com/security/cve/CVE-2024-9392), [CVE-2024-9393](https://access.redhat.com/security/cve/CVE-2024-9393), [CVE-2024-9394](https://access.redhat.com/security/cve/CVE-2024-9394), [CVE-2024-9396](https://access.redhat.com/security/cve/CVE-2024-9396), [CVE-2024-9397](https://access.redhat.com/security/cve/CVE-2024-9397), [CVE-2024-9398](https://access.redhat.com/security/cve/CVE-2024-9398), [CVE-2024-9399](https://access.redhat.com/security/cve/CVE-2024-9399), [CVE-2024-9400](https://access.redhat.com/security/cve/CVE-2024-9400), [CVE-2024-9401](https://access.redhat.com/security/cve/CVE-2024-9401), [CVE-2024-9402](https://access.redhat.com/security/cve/CVE-2024-9402), [CVE-2024-9403](https://access.redhat.com/security/cve/CVE-2024-9403))
thunderbird-debuginfo | 128.3.0-1.el8_10.alma.1 | |
thunderbird-debugsource | 128.3.0-1.el8_10.alma.1 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.135-1.el8_10 | [ALSA-2024:7851](https://errata.almalinux.org/8/ALSA-2024-7851.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))
dotnet-sdk-8.0-source-built-artifacts | 8.0.110-1.el8_10 | [ALSA-2024:7868](https://errata.almalinux.org/8/ALSA-2024-7868.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-38229](https://access.redhat.com/security/cve/CVE-2024-38229), [CVE-2024-43483](https://access.redhat.com/security/cve/CVE-2024-43483), [CVE-2024-43484](https://access.redhat.com/security/cve/CVE-2024-43484), [CVE-2024-43485](https://access.redhat.com/security/cve/CVE-2024-43485))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssl-static | 1.1.1k-14.el8_6 | |

