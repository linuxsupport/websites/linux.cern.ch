## 2023-06-28

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lorax-templates-almalinux | 8.7-1.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lorax-templates-almalinux | 8.7-1.el8 | |

