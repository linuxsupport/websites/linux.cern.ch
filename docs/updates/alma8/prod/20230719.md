## 2023-07-19

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.6-1.al8.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
autofs | 5.1.4-102.el8_8.2 | |
autofs-debuginfo | 5.1.4-102.el8_8.2 | |
autofs-debugsource | 5.1.4-102.el8_8.2 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-debugsource | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-devel | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-legacy-tools | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-legacy-tools-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-libs | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-libs-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-snmp | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-snmp-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
firefox | 102.12.0-1.el8_8.alma.1 | |
firefox-debuginfo | 102.12.0-1.el8_8.alma.1 | |
firefox-debugsource | 102.12.0-1.el8_8.alma.1 | |
python3-lib389 | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-389-ds | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.6-1.al8.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
autofs | 5.1.4-102.el8_8.2 | |
autofs-debuginfo | 5.1.4-102.el8_8.2 | |
autofs-debugsource | 5.1.4-102.el8_8.2 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-debugsource | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-devel | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-legacy-tools | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-legacy-tools-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-libs | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-libs-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-snmp | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
389-ds-base-snmp-debuginfo | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |
firefox | 102.12.0-1.el8_8.alma.1 | |
firefox-debuginfo | 102.12.0-1.el8_8.alma.1 | |
firefox-debugsource | 102.12.0-1.el8_8.alma.1 | |
python3-lib389 | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-389-ds | 1.4.3.35-1.module_el8.8.0+3584+33666a53 | |

