## 2023-08-30

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.21-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
aspnetcore-runtime-7.0 | 7.0.10-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
aspnetcore-targeting-pack-6.0 | 6.0.21-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
aspnetcore-targeting-pack-7.0 | 7.0.10-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
cargo | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
cargo-debuginfo | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | |
clippy | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
clippy-debuginfo | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | |
dotnet | 7.0.110-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-apphost-pack-6.0 | 6.0.21-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-apphost-pack-6.0-debuginfo | 6.0.21-1.el8_8 | |
dotnet-apphost-pack-7.0 | 7.0.10-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-apphost-pack-7.0-debuginfo | 7.0.10-1.el8_8 | |
dotnet-host | 7.0.10-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-host-debuginfo | 7.0.10-1.el8_8 | |
dotnet-hostfxr-6.0 | 6.0.21-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-hostfxr-6.0-debuginfo | 6.0.21-1.el8_8 | |
dotnet-hostfxr-7.0 | 7.0.10-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-hostfxr-7.0-debuginfo | 7.0.10-1.el8_8 | |
dotnet-runtime-6.0 | 6.0.21-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-runtime-6.0-debuginfo | 6.0.21-1.el8_8 | |
dotnet-runtime-7.0 | 7.0.10-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-runtime-7.0-debuginfo | 7.0.10-1.el8_8 | |
dotnet-sdk-6.0 | 6.0.121-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-sdk-6.0-debuginfo | 6.0.121-1.el8_8 | |
dotnet-sdk-7.0 | 7.0.110-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-sdk-7.0-debuginfo | 7.0.110-1.el8_8 | |
dotnet-targeting-pack-6.0 | 6.0.21-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-targeting-pack-7.0 | 7.0.10-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-templates-6.0 | 6.0.121-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-templates-7.0 | 7.0.110-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet6.0-debuginfo | 6.0.121-1.el8_8 | |
dotnet6.0-debugsource | 6.0.121-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.110-1.el8_8 | |
dotnet7.0-debugsource | 7.0.110-1.el8_8 | |
netstandard-targeting-pack-2.1 | 7.0.110-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
rust | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-analysis | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-analyzer | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-analyzer-debuginfo | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | |
rust-debugger-common | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-debuginfo | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | |
rust-debugsource | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | |
rust-doc | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-gdb | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-lldb | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-src | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-std-static | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-std-static-wasm32-unknown-unknown | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-std-static-wasm32-wasi | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-toolset | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rustfmt | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rustfmt-debuginfo | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.121-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-sdk-7.0-source-built-artifacts | 7.0.110-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
almalinux-release-devel | 8-1.el8 | |
almalinux-release-synergy | 8-1.el8 | |
centos-release-advanced-virtualization | 1.0-4.el8 | |
centos-release-ceph-nautilus | 1.3-2.el8 | |
centos-release-ceph-octopus | 1.1-2.el8 | |
centos-release-ceph-pacific | 1.0-2.el8 | |
centos-release-ceph-quincy | 1.0-3.el8 | |
centos-release-gluster10 | 1.0-1.el8 | |
centos-release-gluster8 | 1.0-2.el8 | |
centos-release-gluster9 | 1.0-2.el8 | |
centos-release-messaging | 1-3.el8 | |
centos-release-nfs-ganesha30 | 1.0-3.el8 | |
centos-release-nfv-common | 1-3.el8 | |
centos-release-nfv-extras | 1-3.el8 | |
centos-release-nfv-openvswitch | 1-3.el8 | |
centos-release-openstack-ussuri | 1-6.el8 | |
centos-release-openstack-victoria | 1-3.el8 | |
centos-release-openstack-wallaby | 1-1.el8 | |
centos-release-openstack-xena | 1-1.el8 | |
centos-release-opstools | 1-12.el8 | |
centos-release-ovirt45 | 8.6-1.el8 | |
centos-release-qpid-proton | 1-3.el8 | |
centos-release-rabbitmq-38 | 1-3.el8 | |
centos-release-samba413 | 1.0-2.el8 | |
centos-release-samba414 | 1.0-2.el8 | |
centos-release-samba415 | 1.0-1.1.el8 | |
centos-release-samba416 | 1.0-1.1.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.21-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
aspnetcore-runtime-7.0 | 7.0.10-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
aspnetcore-targeting-pack-6.0 | 6.0.21-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
aspnetcore-targeting-pack-7.0 | 7.0.10-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
cargo | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
cargo-debuginfo | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | |
clippy | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
clippy-debuginfo | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | |
dotnet | 7.0.110-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-apphost-pack-6.0 | 6.0.21-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-apphost-pack-6.0-debuginfo | 6.0.21-1.el8_8 | |
dotnet-apphost-pack-7.0 | 7.0.10-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-apphost-pack-7.0-debuginfo | 7.0.10-1.el8_8 | |
dotnet-host | 7.0.10-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-host-debuginfo | 7.0.10-1.el8_8 | |
dotnet-hostfxr-6.0 | 6.0.21-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-hostfxr-6.0-debuginfo | 6.0.21-1.el8_8 | |
dotnet-hostfxr-7.0 | 7.0.10-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-hostfxr-7.0-debuginfo | 7.0.10-1.el8_8 | |
dotnet-runtime-6.0 | 6.0.21-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-runtime-6.0-debuginfo | 6.0.21-1.el8_8 | |
dotnet-runtime-7.0 | 7.0.10-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-runtime-7.0-debuginfo | 7.0.10-1.el8_8 | |
dotnet-sdk-6.0 | 6.0.121-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-sdk-6.0-debuginfo | 6.0.121-1.el8_8 | |
dotnet-sdk-7.0 | 7.0.110-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-sdk-7.0-debuginfo | 7.0.110-1.el8_8 | |
dotnet-targeting-pack-6.0 | 6.0.21-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-targeting-pack-7.0 | 7.0.10-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-templates-6.0 | 6.0.121-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-templates-7.0 | 7.0.110-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet6.0-debuginfo | 6.0.121-1.el8_8 | |
dotnet6.0-debugsource | 6.0.121-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.110-1.el8_8 | |
dotnet7.0-debugsource | 7.0.110-1.el8_8 | |
netstandard-targeting-pack-2.1 | 7.0.110-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
rust | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-analysis | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-analyzer | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-analyzer-debuginfo | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | |
rust-debugger-common | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-debuginfo | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | |
rust-debugsource | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | |
rust-doc | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-gdb | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-lldb | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-src | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-std-static | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-std-static-wasm32-unknown-unknown | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-std-static-wasm32-wasi | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-toolset | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rustfmt | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | [ALSA-2023:4635](https://errata.almalinux.org/8/ALSA-2023-4635.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rustfmt-debuginfo | 1.66.1-2.module_el8.8.0+3604+b9bee1fc | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.121-1.el8_8 | [ALSA-2023:4645](https://errata.almalinux.org/8/ALSA-2023-4645.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-sdk-7.0-source-built-artifacts | 7.0.110-1.el8_8 | [ALSA-2023:4643](https://errata.almalinux.org/8/ALSA-2023-4643.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
almalinux-release-devel | 8-1.el8 | |
almalinux-release-synergy | 8-1.el8 | |
centos-release-advanced-virtualization | 1.0-4.el8 | |
centos-release-ceph-nautilus | 1.3-2.el8 | |
centos-release-ceph-octopus | 1.1-2.el8 | |
centos-release-ceph-pacific | 1.0-2.el8 | |
centos-release-ceph-quincy | 1.0-3.el8 | |
centos-release-gluster10 | 1.0-1.el8 | |
centos-release-gluster8 | 1.0-2.el8 | |
centos-release-gluster9 | 1.0-2.el8 | |
centos-release-messaging | 1-3.el8 | |
centos-release-nfs-ganesha30 | 1.0-3.el8 | |
centos-release-nfv-common | 1-3.el8 | |
centos-release-nfv-extras | 1-3.el8 | |
centos-release-nfv-openvswitch | 1-3.el8 | |
centos-release-openstack-ussuri | 1-6.el8 | |
centos-release-openstack-victoria | 1-3.el8 | |
centos-release-openstack-wallaby | 1-1.el8 | |
centos-release-openstack-xena | 1-1.el8 | |
centos-release-opstools | 1-12.el8 | |
centos-release-ovirt45 | 8.6-1.el8 | |
centos-release-qpid-proton | 1-3.el8 | |
centos-release-rabbitmq-38 | 1-3.el8 | |
centos-release-samba413 | 1.0-2.el8 | |
centos-release-samba414 | 1.0-2.el8 | |
centos-release-samba415 | 1.0-1.1.el8 | |
centos-release-samba416 | 1.0-1.1.el8 | |

