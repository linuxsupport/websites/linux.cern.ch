## 2023-04-12

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.1-1.al8.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2023c-1.el8 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2023c-1.el8 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.1-1.al8.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2023c-1.el8 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2023c-1.el8 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>

