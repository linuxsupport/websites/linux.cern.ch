## 2025-01-15

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-requests | 2.20.0-5.el8_10 | [ALSA-2025:0012](https://errata.almalinux.org/8/ALSA-2025-0012.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-35195](https://access.redhat.com/security/cve/CVE-2024-35195))

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-debuginfo | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-debugsource | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-devel | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-legacy-tools | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-legacy-tools-debuginfo | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-libs | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-libs-debuginfo | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-snmp | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-snmp-debuginfo | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
nss | 3.101.0-11.el8_8 | |
nss-debuginfo | 3.101.0-11.el8_8 | |
nss-debugsource | 3.101.0-11.el8_8 | |
nss-devel | 3.101.0-11.el8_8 | |
nss-softokn | 3.101.0-11.el8_8 | |
nss-softokn-debuginfo | 3.101.0-11.el8_8 | |
nss-softokn-devel | 3.101.0-11.el8_8 | |
nss-softokn-freebl | 3.101.0-11.el8_8 | |
nss-softokn-freebl-debuginfo | 3.101.0-11.el8_8 | |
nss-softokn-freebl-devel | 3.101.0-11.el8_8 | |
nss-sysinit | 3.101.0-11.el8_8 | |
nss-sysinit-debuginfo | 3.101.0-11.el8_8 | |
nss-tools | 3.101.0-11.el8_8 | |
nss-tools-debuginfo | 3.101.0-11.el8_8 | |
nss-util | 3.101.0-11.el8_8 | |
nss-util-debuginfo | 3.101.0-11.el8_8 | |
nss-util-devel | 3.101.0-11.el8_8 | |
python3-lib389 | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-389-ds | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
nss-pkcs11-devel | 3.101.0-11.el8_8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-requests | 2.20.0-5.el8_10 | [ALSA-2025:0012](https://errata.almalinux.org/8/ALSA-2025-0012.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-35195](https://access.redhat.com/security/cve/CVE-2024-35195))

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-debuginfo | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-debugsource | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-devel | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-legacy-tools | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-legacy-tools-debuginfo | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-libs | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-libs-debuginfo | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-snmp | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
389-ds-base-snmp-debuginfo | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
nss | 3.101.0-11.el8_8 | |
nss-debuginfo | 3.101.0-11.el8_8 | |
nss-debugsource | 3.101.0-11.el8_8 | |
nss-devel | 3.101.0-11.el8_8 | |
nss-softokn | 3.101.0-11.el8_8 | |
nss-softokn-debuginfo | 3.101.0-11.el8_8 | |
nss-softokn-devel | 3.101.0-11.el8_8 | |
nss-softokn-freebl | 3.101.0-11.el8_8 | |
nss-softokn-freebl-debuginfo | 3.101.0-11.el8_8 | |
nss-softokn-freebl-devel | 3.101.0-11.el8_8 | |
nss-sysinit | 3.101.0-11.el8_8 | |
nss-sysinit-debuginfo | 3.101.0-11.el8_8 | |
nss-tools | 3.101.0-11.el8_8 | |
nss-tools-debuginfo | 3.101.0-11.el8_8 | |
nss-util | 3.101.0-11.el8_8 | |
nss-util-debuginfo | 3.101.0-11.el8_8 | |
nss-util-devel | 3.101.0-11.el8_8 | |
python3-lib389 | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-389-ds | 1.4.3.39-9.module_el8.10.0+3944+272edf81 | |
nss-pkcs11-devel | 3.101.0-11.el8_8 | |

