## 2023-08-09

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iwl100-firmware | 39.31.5.1-114.el8_8.1.alma | |
iwl1000-firmware | 39.31.5.1-114.el8_8.1.alma | |
iwl105-firmware | 18.168.6.1-114.el8_8.1.alma | |
iwl135-firmware | 18.168.6.1-114.el8_8.1.alma | |
iwl2000-firmware | 18.168.6.1-114.el8_8.1.alma | |
iwl2030-firmware | 18.168.6.1-114.el8_8.1.alma | |
iwl3160-firmware | 25.30.13.0-114.el8_8.1.alma | |
iwl3945-firmware | 15.32.2.9-114.el8_8.1.alma | |
iwl4965-firmware | 228.61.2.24-114.el8_8.1.alma | |
iwl5000-firmware | 8.83.5.1_1-114.el8_8.1.alma | |
iwl5150-firmware | 8.24.2.2-114.el8_8.1.alma | |
iwl6000-firmware | 9.221.4.1-114.el8_8.1.alma | |
iwl6000g2a-firmware | 18.168.6.1-114.el8_8.1.alma | |
iwl6000g2b-firmware | 18.168.6.1-114.el8_8.1.alma | |
iwl6050-firmware | 41.28.5.1-114.el8_8.1.alma | |
iwl7260-firmware | 25.30.13.0-114.el8_8.1.alma | |
libertas-sd8686-firmware | 20230404-114.git2e92a49f.el8_8.alma | |
libertas-sd8787-firmware | 20230404-114.git2e92a49f.el8_8.alma | |
libertas-usb8388-firmware | 20230404-114.git2e92a49f.el8_8.alma | |
libertas-usb8388-olpc-firmware | 20230404-114.git2e92a49f.el8_8.alma | |
linux-firmware | 20230404-114.git2e92a49f.el8_8.alma | |
sos | 4.5.5-2.el8.alma | |
sos-audit | 4.5.5-2.el8.alma | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt-core | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt-core | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_477.15.1.el8_8.al8.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iwl100-firmware | 39.31.5.1-114.el8_8.1.alma | |
iwl1000-firmware | 39.31.5.1-114.el8_8.1.alma | |
iwl105-firmware | 18.168.6.1-114.el8_8.1.alma | |
iwl135-firmware | 18.168.6.1-114.el8_8.1.alma | |
iwl2000-firmware | 18.168.6.1-114.el8_8.1.alma | |
iwl2030-firmware | 18.168.6.1-114.el8_8.1.alma | |
iwl3160-firmware | 25.30.13.0-114.el8_8.1.alma | |
iwl3945-firmware | 15.32.2.9-114.el8_8.1.alma | |
iwl4965-firmware | 228.61.2.24-114.el8_8.1.alma | |
iwl5000-firmware | 8.83.5.1_1-114.el8_8.1.alma | |
iwl5150-firmware | 8.24.2.2-114.el8_8.1.alma | |
iwl6000-firmware | 9.221.4.1-114.el8_8.1.alma | |
iwl6000g2a-firmware | 18.168.6.1-114.el8_8.1.alma | |
iwl6000g2b-firmware | 18.168.6.1-114.el8_8.1.alma | |
iwl6050-firmware | 41.28.5.1-114.el8_8.1.alma | |
iwl7260-firmware | 25.30.13.0-114.el8_8.1.alma | |
libertas-sd8686-firmware | 20230404-114.git2e92a49f.el8_8.alma | |
libertas-sd8787-firmware | 20230404-114.git2e92a49f.el8_8.alma | |
libertas-usb8388-firmware | 20230404-114.git2e92a49f.el8_8.alma | |
libertas-usb8388-olpc-firmware | 20230404-114.git2e92a49f.el8_8.alma | |
linux-firmware | 20230404-114.git2e92a49f.el8_8.alma | |
sos | 4.5.5-2.el8.alma | |
sos-audit | 4.5.5-2.el8.alma | |

