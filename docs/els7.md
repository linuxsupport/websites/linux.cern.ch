# CERN CentOS 7 / RHEL 7 - Extended Life-cycle Support

!!! danger "Using ELS7 is no substitute for upgrading to a supported operating system. ELS7 *may* reduce *some* of the risk of running on an unsupported OS, but it does not eliminate it. This risk is ultimately entirely your responsibility. CERN IT does not offer any support beyond what is explicitly stated on this page."

## What is ELS7?

Following the end-of-life of CC7 (30.06.2024) and through a [special agreement with the Accelerator and Technology Sector](#what-is-the-atsit-cc7-agreement), CERN IT will rebuild Red Hat's Extended Lifecycle Support (ELS) product to be consumable at CERN, for a limited duration of time (until **01.07.2026**). ELS7 provides updates for some security vulnerabilities beyond the end-of-life of CC7 and may, in some cases, reduce the risk of running on an unsupported operating system.

**Please read this page carefully to understand the conditions and consequences of using ELS7.**

## How do I use ELS7?

All ELS7 content is served from [linuxsoft](https://linuxsoft.cern.ch/internal/repos/els7-stable/x86_64/os/) and is available from **within the CERN network only**.

As a convenience for users, we have also created a release file which configures the ELS7 repository once installed.

The ELS7 release file can be installed as simply as:

```
yum install cern-els7-release
```

Once the `cern-els7-release` package is installed, any new updates will be installed as per your system's yum configuration.

If you don't want to install this package and you'd rather manage the repository configuration manually, download [cern-els7.repo](https://gitlab.cern.ch/linuxsupport/rpms/releases/cern-els7-release/-/raw/master/sources/cern-els7.repo?ref_type=heads) and place it in `/etc/yum.repos.d/`.

Puppet-managed nodes can configure the repository as follows:

```
if $facts['os']['release']['major'] == '7' {
    yumrepo { 'cern-els7':
        descr    => 'CERN Extended Life-cycle Support (ELS) 7',
        baseurl  => "http://linuxsoft.cern.ch/internal/repos/els7-stable/${facts[os][architecture]}/os",
        enabled  => 1,
        gpgcheck => true,
        gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2',
        protect  => 1,
        priority => 5,
    }
}
```

## Frequently Asked Questions

### What is the ATS/IT CC7 agreement?

The specifics of this arrangement are as follows:

* ELS updates "may" be provided for packages that have a security vulnerability for what Red Hat defines as "Critical". This is completely at the discretion of Red Hat, includes only the 'base' operating system and only in the context of security. Bugfixes and new features are not in scope.
* CERN IT will rebuild the ELS7 product from 01.07.2024, and **only until 01.07.2026**.
* These ELS security updates are only suitable for Front-End Computers, as others will likely rely on other software and external infrastructure
* No other repositories are in scope of this arrangement. This means that EPEL7, and CC7 Koji repositories will not be supported after 30.06.2024
* CERN software for CC7 (eg: locmap, cern-get-keytab, eos, afs, etc) will not supported after 30.06.2024
* Docker, Cloud, AIMS images, configuration management, monitoring for CC7 systems will not be supported after 30.06.2024

### Can my experiment / department use the ELS packages for CC7?

Yes, providing the system:

* Is isolated on the TN network
* Is physically difficult to access, which makes the upgrade to a more recent operating system impractical (eg: the system resides in cavern or pit)
* Does not rely on other CC7 software or infrastructure (Isolated in terms of software)

Note that running your systems like this will be under your responsibility and support will be on a best-effort basis.

### Can I build / use CC7 based docker containers after CC7 end-of-Life?

Our advice is to stop using CC7, even in a container environment.

* CC7 will be a deprecated operating system, no longer supported by upstream nor by CERN
* Whilst CC7 content will **not** be removed from `linuxsoft.cern.ch` after CC7 end-of-Life, other key repositories such as EPEL7 and Koji CC7 repositories will no longer be available
* CERN IT understands that CC7 container-based workflows are typically short-lived, do not usually expose services, and often require running on a specific version for reproducibility. As such, CERN IT will not actively break existing container deployments
* The managed [docker image](https://gitlab.cern.ch/linuxsupport/cc7-base) will continue to exist, however, it will not be updated after 30.06.2024. It's the responsibility of the service owner/user to ensure that CC7 containers running after 30.06.2024 continue to be secure. For this reason, new or updated CC7 containers should add the ELS7 repository to ensure that critical security updates can be included in the image
* The likelihood of existing CC7 containers continuing to work is directly proportional to the level of container self-sufficiency. CC7 containers should be isolated as much as possible, not install additional software at runtime and not rely on external third-party systems
* Users of CC7 containers post end-of-life assume complete liability and usage is therefore at the risk of the user

### Can I use ELS7 content within CC7 containers?

Once ELS7 is available at CERN, there will not be any restrictions on its usage if the container is running on the CERN network.

Anyone will be able to add the ELS7 repository to their existing CC7 containers, but [we do not recommend it](#can-i-build-use-cc7-based-docker-containers-after-cc7-end-of-life).

### Where can I see what updates are released as part of ELS7?

A record of ELS7 updates can be found on the [ELS7 updates](/updates/cc7/els7/latest_updates) page.

### What will happen after June 2026?

After June 2026, CERN IT will stop rebuilding ELS packages for CC7. The ELS7
repositories will no longer receive any updates, not even critical security updates.
Any CC7 machines still in existence will be completely unmaintained and at the full
responsibility of their owners.

### I get yum errors relating to EPEL, is this expected?

Yes, it is expected.

EPEL7 is deprecated as of 30.06.2024. If you are still relying on EPEL, you will need to use an archive mirror.

You can update your EPEL repository with the following to use an archive copy hosted by CERN:

```
sed -i 's#http://linuxsoft.cern.ch/epel/7/#http://linuxsoft.cern.ch/internal/archive/epel/7/#g' /etc/yum.repos.d/epel.repo
```
