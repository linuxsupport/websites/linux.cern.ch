# CERN Linux Survival Kit training course

We do provide an online training course for those who need it, you can find it on our [Training catalogue](https://lms.cern.ch/ekp/servlet/ekp?PX=N&TEACHREVIEW=N&CID=EKP000043411&TX=FORMAT1&LANGUAGE_TAG=en&DECORATEPAGE=N).

This online course covers various aspects:

* Installation
* CERN Linux distributions
* Using LOCMAP at CERN
* Building Software for Linux
* How to access/administrate AFS files
* How to access/administrate EOS files
* How to access CVMFS
* Why we don't update CentOS package
* How to open a support case in a good way
* CERN tools
* RHEL at CERN and licensing
* How to use journalctl and new systemd tools
* How to use systemd for daily tasks

!!! note
    Please note that this is NOT a Linux System Administration course.

**Format:** Online: theory & hands-on

**Target Audience:** Personnel who are running services in the CERN Data Center interested in the operating systems supported at CERN

**Objectives:** To provide a set of recipes commonly needed by people managing Linux systems outside the computer centre

**Contact:** <technical.training@cern.ch>

**Course Price (CHF):** 0

**Number of Hours:** 4

**Competencies:** [Compute systems](https://aisdb.cern.ch/pls/htmldb_aisdb_prod/f?p=158:1:212972988511920::NO::P1_SUBDOMAIN_ID:213B)

**Additional Requirements:**

Ideally, you should have completed some of the following trainings available on UDEMY:

* For absolute beginners:
    * [Complete Linux Training Course to Get Your Dream IT Job 2023](https://www.udemy.com/course/complete-linux-training-course-to-get-your-dream-it-job/)

* For more experienced users:
    * [CentOS 7 Linux Server: Alternative to Red Hat Enterprise](https://cern.udemy.com/course/centos-7-linux-server-administration-alternative-to-red-hat-enterprise/)
    * Linux Academy Red Hat Certified Systems Administrator Prep ([SA1](https://cern.udemy.com/course/redhat-linux-administration-l/) and [SA2](https://cern.udemy.com/course/linux-administration-ll/))

* Please note that UDEMY licence is needed to take the courses. It can be found in LMS and has a cost.