# (Old) News

Current [News](index/#latest-news)

<div id="news">

<table>

<tr>
<td><a name="SLC66"></a><h3>03.11.2014: Scientific Linux CERN 6.6 becomes default production SLC6 release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 6 (SLC6) version:

          ******************************************************
          SLC 6.6 becomes default SLC6 production version
          ******************************************************

                as of Monday 03.11.2014

* What is Scientific Linux CERN 6.6 (SLC66)  ?

  - A minor release of SLC6X series with all the
    updates since release of 6.5 (and up to 19.10.2014)
    integrated in the base installation tree.

* Upgrading from previous SLC6X releases:

  - fully updated SLC 6X systems do not need any upgrade to
    6.6 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

* Installation media images for SLC66 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc66/i386/images/">http://linuxsoft.cern.ch/cern/slc66/i386/images/</a>
        (install path: linuxsoft:/cern/slc66/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc66/x86_64/images/">http://linuxsoft.cern.ch/cern/slc66/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc66/x86_64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific6/docs/install">http://cern.ch/linux/scientific6/docs/install</a>


* ISO DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc66/iso/">http://linuxsoft.cern.ch/cern/slc66/iso/</a>


* Certification of the SLC 6.6:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>


<tr>
<td><a name="slc66test"></a><h3>22.10.2014 Scientific Linux CERN 6.6 TEST available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users.

We have a pleasure to announce that:

     TEST version of SLC 6.6 for i386/x86_64
     ********************************************

is available as of today.

* What is Scientific Linux CERN 6 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 6 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 6.6 (SLC66) TEST ?

 - TEST minor release of SLC6X series with all the
   updates (up to 19.10.2014) integrated in the base
   installation tree.

* Installation

- via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

- non-PXE installation:

Boot media can be found at:

<a href="http://linuxsoft.cern.ch/cern/slc66/i386/images/">http://linuxsoft.cern.ch/cern/slc66/i386/images/</a>
<a href="http://linuxsoft.cern.ch/cern/slc66/x86_64/images/">http://linuxsoft.cern.ch/cern/slc66/x86_64/images/</a>

and corresponding installation paths are:

http://linuxsoft.cern.ch/cern/slc66/i386/
http://linuxsoft.cern.ch/cern/slc66/x86_64/

- Automated PXE installations using AIMS2

 Boot targets are:

 SLC66_I386
 SLC66_X86_64

* Upgrading from previous SLC6X releases:

  - fully updated SLC 6X systems do not need any upgrade to
    6.6 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

Please note:

* SLC66 will become the default SLC6 production version as of:

                    Monday 03.11.2014
                  *************************

  providing that no show-stopper problems are found.

* SLC66 (yum) updates will be released to production on:

                    Wednesday 29.10.2014
                   ****************************

* Certification of the SLC66:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

* Problem reporting:

 Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="rhel66""></a><h3>22.10.2014 RHEL 6.6 - Red Hat Enterprise Linux 6 Server Update 6 available</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Red Hat Enterprise Linux 6 Server Update 6 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

RHEL6_U6_I386
RHEL6_U6_X86_64

,installation paths are:

http://linuxsoft.cern.ch/enterprise/6Server_U6/en/os/i386/
http://linuxsoft.cern.ch/enterprise/6Server_U6/en/os/x86_64/

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel6">http://cern.ch/linux/rhel/#rhel6</a>


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="SLC511"></a><h3>06.10.2014: Scientific Linux CERN 5.11 becomes default production SLC5 release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 5 (SLC5) version:

          ***********************************************************
          SLC 5.11 becomes default SLC5 production version
          ***********************************************************

                as of Monday 06.10.2014

* What is Scientific Linux CERN 5.11 (SLC511)  ?

  - A minor release of SLC5X series with all the
    updates since release of 5.10 (and up to 11.09.2014)
    integrated in the base installation tree.

* Upgrading from previous SLC5X releases:

  - fully updated SLC 5X systems do not need any upgrade to
    5.11 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific5/docs/quickupdate">http://cern.ch/linux/scientific5/docs/quickupdate</a>

* Installation media images for SLC511 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc511/i386/images/">http://linuxsoft.cern.ch/cern/slc511/i386/images/</a>
        (install path: linuxsoft.cern.ch:/cern/slc511/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc511/x86_64/images/">http://linuxsoft.cern.ch/cern/slc511/x86_64/images/</a>
        (install path: linuxsoft.cern.ch:/cern/slc511/x86_64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

* ISO DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc511/iso/">http://linuxsoft.cern.ch/cern/slc511/iso/</a>

* Certification of the SLC 5.11:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>


<tr>
<td><a name="slc511test"></a><h3>18.09.2014: Scientific Linux CERN 5.11 TEST available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users,

We would like to announce that:

   ***********************************************
   Scientific Linux CERN 5.11 TEST (SLC511)
   ***********************************************

   is available for tests as of Thursday 18.09.2014.

* What is Scientific Linux CERN 5 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 5 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 5.11 (SLC511) TEST ?

 - TEST minor release of SLC5X series with all the
   updates (up to 17.09.2014) integrated in the base
   installation tree.

* Installation media images for SLC511 can be found at:

   For 32-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc511/i386/images/">http://linuxsoft.cern.ch/cern/slc511/i386/images/</a>
       (install path: linuxsoft:/cern/slc511/i386/)

   For 64-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc511/x86_64/images/">http://linuxsoft.cern.ch/cern/slc511/x86_64/images/</a>
       (install path: linuxsoft:/cern/slc511/x86_64/)

   To install please follow the procedure at:

       <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>


* More information:

SLC5 web pages:

  <a href="http://cern.ch/linux/scientific5/">http://cern.ch/linux/scientific5/</a>

SLC5 installation:

  <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

SLC5 recent updates:

  <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>



* SLC511 version will become the default SLC5 production
   version as of:

                    Monday 06.10.2014
                  **************************

  providing that no show-stopper problems are found.

* Certification of the SLC511:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

* Problem reporting:

 Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="rhel511""></a><h3>18.09.2014 RHEL5.10 - Red Hat Enterprise Linux 5 Server Update 11 available</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Red Hat Enterprise Linux 5 Server Update 11 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

RHES_5_U11_I386
RHES_5_U11_X86_64

,installation paths are:

http://linuxsoft.cern.ch/enterprise/5Server_U11/en/os/i386/
http://linuxsoft.cern.ch/enterprise/5Server_U11/en/os/x86_64/

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel5">http://cern.ch/linux/rhel/#rhel5</a>


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="cc7TEST"></a><h3>04.08.2014 CC7 TEST - CERN CentOS 7 TEST available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We would like to announce immediate availability of
a test version of next CERN Linux release:

         CC7: CERN CentOS 7 TEST

For information about CC7, including installation
instructions please visit:

         <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

Please note: CC7 is a TEST release - not suitable
for production use - and not all CERN specific
software has been ported to it yet.

The certification process for CC7 will start in
September 2014.

What happened to Scientific Linux CERN (SLC) ?

 - SLC 6 and SLC5 remain supported Linux versions at
   CERN
 - Next major release: 7 is based on CentOS

please see: <a href="http://cern.ch/linux/nextversion">http://cern.ch/linux/nextversion</a>
for more information.

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>



<tr>
<td><a name="rhel70"></a><h3>13.06.2014 RHEL7 - Red Hat Enterprise Linux 7 Server available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Red Hat Enterprise Linux 7 Server for x86_64
platform is available for installation at CERN,
starting today.

AIMS2 PXE boot target:

RHEL7_U0_X86_64

,installation path:

http://linuxsoft.cern.ch/enterprise/7Server_U0/en/os/x86_64/

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel7">http://cern.ch/linux/rhel/#rhel7</a>


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="rhel7beta"></a><h3>16.12.2013 RHEL7 BETA - Red Hat Enterprise Linux 7 BETA available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Red Hat Enterprise Linux 7 BETA for x86_64
platform is available for installation at CERN,
starting today.

AIMS2 PXE boot target is:

RHEL7_BETA_X86_64

Installation path is:

http://linuxsoft.cern.ch/enterprise/7BETA/en/os/x86_64/

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that despite being called 'Red Hat Enterprise
Linux 7 Public BETA' this product requires a valid
Red Hat subscription (license). Therefore if you would
like to test it please request license assignment from
linux support.

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="SLC65"></a><h3>09.12.2013: Scientific Linux CERN 6.5 becomes default production SLC6 release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 6 (SLC6) version:

          ******************************************************
          SLC 6.5 becomes default SLC6 production version
          ******************************************************

                as of Monday 09.12.2013

* What is Scientific Linux CERN 6.5 (SLC65)  ?

  - A minor release of SLC6X series with all the
    updates since release of 6.4 (and up to 25.11.2013)
    integrated in the base installation tree.

* Upgrading from previous SLC6X releases:

  - fully updated SLC 6X systems do not need any upgrade to
    6.5 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

* Installation media images for SLC65 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc65/i386/images/">http://linuxsoft.cern.ch/cern/slc65/i386/images/</a>
        (install path: linuxsoft:/cern/slc65/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc65/x86_64/images/">http://linuxsoft.cern.ch/cern/slc65/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc65/x86_64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific6/docs/install">http://cern.ch/linux/scientific6/docs/install</a>


* ISO DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc65/iso/">http://linuxsoft.cern.ch/cern/slc65/iso/</a>


* Certification of the SLC 6.5:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="slc65beta"></a><h3>29.11.2013 Scientific Linux CERN 6.5 BETA available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users.

We have a pleasure to announce that:

     BETA version of SLC 6.5 for i386/x86_64
     ********************************************

is available as of today.

* What is Scientific Linux CERN 6 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 6 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 6.5 (SLC65) BETA ?

 - TEST minor release of SLC6X series with all the
   updates (up to 22.11.2013) integrated in the base
   installation tree.

* Installation

- via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

- non-PXE installation:

Boot media can be found at:

<a href="http://linuxsoft.cern.ch/cern/slc65/i386/images/">http://linuxsoft.cern.ch/cern/slc65/i386/images/</a>
<a href="http://linuxsoft.cern.ch/cern/slc65/x86_64/images/">http://linuxsoft.cern.ch/cern/slc65/x86_64/images/</a>

and corresponding installation paths are:

http://linuxsoft.cern.ch/cern/slc65/i386/
http://linuxsoft.cern.ch/cern/slc65/x86_64/

- Automated PXE installations using AIMS2

 Boot targets are:

 SLC65_I386
 SLC65_X86_64

* Upgrading from previous SLC6X releases:

  - fully updated SLC 6X systems do not need any upgrade to
    6.5 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

Please note:

* SLC65 will become the default SLC6 production version as of:

                    Monday 09.12.2013
                  *************************

  providing that no show-stopper problems are found.

* SLC65 (yum) updates will be released to production on:

                    Thursday 05.12.2013
                   *************************

* Certification of the SLC65:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

* Problem reporting:

 Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="rhel65""></a><h3>25.11.2013 RHEL 6.5 - Red Hat Enterprise Linux 6 Server Update 5 available</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Red Hat Enterprise Linux 6 Server Update 5 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

RHEL6_U5_I386
RHEL6_U5_X86_64

,installation paths are:

http://linuxsoft.cern.ch/enterprise/6Server_U5/en/os/i386/
http://linuxsoft.cern.ch/enterprise/6Server_U5/en/os/x86_64/

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel6">http://cern.ch/linux/rhel/#rhel6</a>


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="devtoolset-2.0"></a><h3>04.11.2013: Developer Toolset 2.0 (gcc/g++/gfortran - 4.8.1) available for SLC 5 and 6.</h3></td>
</tr>
<tr>
<td>
<pre>
Developer Toolset is an offering for developers on the Scientific Linux CERN 5 and 6 platforms.
Using a framework called Software Collections, an additional set of tools is installed into the
/opt directory, as recommended by the UNIX Filesystem Hierarchy Standard. These tools are
enabled by the user on demand using the supplied scl utility.

Developer Toolset 2.0 provides following tools:

    gcc/g++/gfortran - GNU Compiler Collection - version 4.8.1

    gdb - GNU Debugger - version 7.6.34

    binutils - A GNU collection of binary utilities - version 2.23.52

    elfutils - A collection of utilities and DSOs to handle compiled objects - version 0.155

    dwz - DWARF optimization and duplicate removal tool - version 0.11

    systemtap - Programmable system-wide instrumentation system - version 2.1

    valgrind - Tool for finding memory management bugs in programs - version 3.8.1

    oprofile - System wide profiler - version 0.9.8

    eclipse - An integrated Development Environment - version 4.3 (Kepler)
              (only available on SLC6).

For more information and installation instructions, please visit:

<a href="http://cern.ch/linux/devtoolset/">http://cern.ch/linux/devtoolset/</a>

<a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="scl10"></a><h3>21.10.2013 Software Collections 1.0 for Scientific Linux CERN 6</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux Users,

CERN Software Collections is an offering for developers on
the Scientific Linux CERN 6 platform.

Using a framework called Software Collections, an additional
set of tools is installed into the /opt directory,
as recommended by the UNIX Filesystem Hierarchy Standard.

These tools are enabled by the user on demand using the
supplied scl utility.

CERN Software Collections provides following tools:

   Mysql version 5.5, which offers performance,
                      scalability, and usability
                      enhancements.

   MariaDB version 5.5, which introduces an easy-to-adopt
                        alternative for MySQL, binary
                        compatibility allows MySQL users to
                        drop-in MariaDB without
                        converting data files.

   PostgreSQL version 9.2, which includes native JSON support,
                           covering indexes, and significant
                           improvements in replication, high
                           availability and performance.

   Python version 2.7, which includes new unit test
                       features, faster I/O, and tools
                       and back-ported features from
                       Python 3 to make future migration
                       easier.

   Python version 3.3, which offers significant improvements
                       in language consistency, Unicode
                       performance, imports, and distribution
                       of packages.

   PHP version 5.4, which includes new language syntax,
                    improved performance and reduced memory
                    consumption, and a built-in web server
                    in CLI mode to simplify development
                    workflows and testing.

   Perl version 5.16.3, which includes improved unicode
                        support, performance enhancements,
                        new debugging options, enhanced
                        security, and a number of new
                        and updated modules.

   Technology Preview of node.js version 0.10, which delivers
                         an easy to use module for handling
                         streams, better error handling with
                         domains, and performance improvements
                         for web application development.


For more information and installation instructions, please visit:

<a href="http://cern.ch/linux/scl/">http://cern.ch/linux/scl/</a>

<a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

</pre>
</td>
</tr>

<tr>
<td><a name="SLC510"></a><h3>17.10.2013: Scientific Linux CERN 5.10 becomes default production SLC5 release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 5 (SLC5) version:

          ***********************************************************
          SLC 5.10 becomes default SLC5 production version
          ***********************************************************

                as of Thursday 17.10.2013

* What is Scientific Linux CERN 5.10 (SLC510)  ?

  - A minor release of SLC5X series with all the
    updates since release of 5.9 (and up to 7.10.2013)
    integrated in the base installation tree.

* Upgrading from previous SLC5X releases:

  - fully updated SLC 5X systems do not need any upgrade to
    5.10 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific5/docs/quickupdate">http://cern.ch/linux/scientific5/docs/quickupdate</a>

* Installation media images for SLC59 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc510/i386/images/">http://linuxsoft.cern.ch/cern/slc510/i386/images/</a>
        (install path: linuxsoft.cern.ch:/cern/slc510/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc510/x86_64/images/">http://linuxsoft.cern.ch/cern/slc510/x86_64/images/</a>
        (install path: linuxsoft.cern.ch:/cern/slc510/x86_64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

* ISO DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc510/iso/">http://linuxsoft.cern.ch/cern/slc510/iso/</a>

* Certification of the SLC 5.10:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="slc510test"></a><h3>10.10.2013: Scientific Linux CERN 5.10 TEST available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users,

We would like to announce that:

   ***********************************************
   Scientific Linux CERN 5.10 TEST (SLC510)
   ***********************************************

   is available for tests as of Thursday 10.10.2013.

* What is Scientific Linux CERN 5 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 5 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 5.10 (SLC510) TEST ?

 - TEST minor release of SLC5X series with all the
   updates (up to 07.10.2013) integrated in the base
   installation tree.

* Upgrading from previous SLC4X releases:

 - Upgrade is not supported and has not been tested.

* Installation media images for SLC510 can be found at:

   For 32-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc510/i386/images/">http://linuxsoft.cern.ch/cern/slc510/i386/images/</a>
       (install path: linuxsoft:/cern/slc510/i386/)

   For 64-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc510/x86_64/images/">http://linuxsoft.cern.ch/cern/slc510/x86_64/images/</a>
       (install path: linuxsoft:/cern/slc510/x86_64/)

   To install please follow the procedure at:

       <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>


* More information:

SLC5 web pages:

  <a href="http://cern.ch/linux/scientific5/">http://cern.ch/linux/scientific5/</a>

SLC5 installation:

  <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

SLC5 recent updates:

  <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>



* SLC510 version will become the default SLC5 production
   version as of:

                    Thursday 17.10.2013
                  **************************

  providing that no show-stopper problems are found.

* Certification of the SLC510:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

* Problem reporting:

 Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="rhel510""></a><h3>08.10.2013 RHEL5.10 - Red Hat Enterprise Linux 5 Server Update 10 available</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Red Hat Enterprise Linux 5 Server Update 10 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

RHES_5_U10_I386
RHES_5_U10_X86_64

,installation paths are:

http://linuxsoft.cern.ch/enterprise/5Server_U10/en/os/i386/
http://linuxsoft.cern.ch/enterprise/5Server_U10/en/os/x86_64/

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel5">http://cern.ch/linux/rhel/#rhel5</a>


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="javasigning"></a><h3>18.04.2013  </h3></td>
</tr>
<tr>
<td>
<pre>
Oracle has significantly changed how Java runs with this version. Java
now requires code signing, and will pop up brightly coloured dialogue
boxes if your code is not signed.  They now alert on unsigned,
signed-but-expired and self-signed certificates.

Full details on the new run policy can be found here ==>
<a href="https://www.java.com/en/download/help/appsecuritydialogs.xml">https://www.java.com/en/download/help/appsecuritydialogs.xml</a>

And more information can be found here ==>
<a href="http://www.oracle.com/technetwork/java/javase/tech/java-code-signing-1915323.html">http://www.oracle.com/technetwork/java/javase/tech/java-code-signing-1915323.html</a>

</pre>
</td>
</tr>


<tr>
<td><a name="krb5change"></a><h3>SLC5/6: Kerberos configuration related changes on 21.03.2013</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Scientific Linux CERN 5 and 6 users.

System update on Thursday, 21st of March 2013 will include following
Kerberos related changes:

- cern-config-keytab will be obsoleted and replaced by cern-get-keytab

- kerberos configuration (/etc/krb5.conf) will be adjusted to point to
  kerberos key distribution center (KDC) for the given network:

   atlasvdc.cern.ch for ATLAS network
   cmsvdc.cern.ch for CMS network
   tndc.cern.ch for TECHNICAL NETWORK

  for GENERAL PURPOSE NETWORK (or systems on a network not belonging
  to any of above three):

   cerndc.cern.ch (this is the current setup)

If you would like to test the change on your system, please run
as root following commands:

# yum --enablerepo=slc*-testing clean all
# yum --enablerepo=slc*-testing install lcm-profile arc cern-get-keytab cern-config-keytab

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>


<tr>
<td><a name="SLC64"></a><h3>11.03.2013: Scientific Linux CERN 6.4 becomes default production SLC6 release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 6 (SLC6) version:

          ***********************************************************
          SLC 6.4 becomes default SLC6 production version
          ***********************************************************

                as of Monday 11.03.2013

* What is Scientific Linux CERN 6.4 (SLC64)  ?

  - A minor release of SLC6X series with all the
    updates since release of 6.3 (and up to 22.02.2013)
    integrated in the base installation tree.

* Upgrading from previous SLC6X releases:

  - fully updated SLC 6X systems do not need any upgrade to
    6.4 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

* Installation media images for SLC64 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc64/i386/images/">http://linuxsoft.cern.ch/cern/slc64/i386/images/</a>
        (install path: linuxsoft:/cern/slc64/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc64/x86_64/images/">http://linuxsoft.cern.ch/cern/slc64/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc64/x86_64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific6/docs/install">http://cern.ch/linux/scientific6/docs/install</a>


* ISO DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc64/iso/">http://linuxsoft.cern.ch/cern/slc64/iso/</a>


* Certification of the SLC 6.4:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="slc64beta"></a><h3>28.02.2013 Scientific Linux CERN 6.4 BETA available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users.

We have a pleasure to announce that:

     BETA version of SLC 6.4 for i386/x86_64
     ********************************************

is available as of today.

* What is Scientific Linux CERN 6 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 6 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 6.4 (SLC64) BETA ?

 - TEST minor release of SLC6X series with all the
   updates (up to 22.02.2013) integrated in the base
   installation tree.

* Installation

- via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

- non-PXE installation:

Boot media can be found at:

<a href="http://linuxsoft.cern.ch/cern/slc64/i386/images/">http://linuxsoft.cern.ch/cern/slc64/i386/images/</a>
<a href="http://linuxsoft.cern.ch/cern/slc64/x86_64/images/">http://linuxsoft.cern.ch/cern/slc64/x86_64/images/</a>

and corresponding installation paths are:

http://linuxsoft.cern.ch/cern/slc64/i386/
http://linuxsoft.cern.ch/cern/slc64/x86_64/

- Automated PXE installations using AIMS2

 Boot targets are:

 SLC64_I386
 SLC64_X86_64

* Upgrading from previous SLC6X releases:

  - fully updated SLC 6X systems do not need any upgrade to
    6.4 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

Please note:

* SLC64 will become the default SLC6 production version as of:

                    Monday 11.03.2013
                  *************************

  providing that no show-stopper problems are found.

* SLC64 (yum) updates will be released to production on:

                    Thursday 07.03.2013
                   *************************

* Certification of the SLC64:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

* Problem reporting:

 Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>


<tr>
<td><a name="rhel64""></a><h3>25.02.2013 RHEL 6.4 - Red Hat Enterprise Linux 6 Server Update 4 available</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Red Hat Enterprise Linux 6 Server Update 4 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

RHEL6_U4_I386
RHEL6_U4_X86_64

,installation paths are:

http://linuxsoft.cern.ch/enterprise/6Server_U4/en/os/i386/
http://linuxsoft.cern.ch/enterprise/6Server_U4/en/os/x86_64/

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel.shml#rhel6">http://cern.ch/linux/rhel.shml#rhel6</a>


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="firefox17thunderbird17"></a><h3>21.02.2013 SLC5 / SLC6: Major Firefox and Thunderbird update. </h3></td>
</tr>
<tr>
<td>
<pre>
Dear Scientific Linux CERN 5/6 Users.

Tonight SLC5 and SLC6 updates:

<a href="http://cern.ch/linux/updates/updates-slc6#20120221">http://cern.ch/linux/updates/updates-slc6#20120221</a>
<a href="http://cern.ch/linux/updates/updates-slc5#20120221">http://cern.ch/linux/updates/updates-slc5#20120221</a>

include major version update for Mozilla Firefox and
Mozilla Thunderbird products.

Firefox is updated to version 17 ESR (from 10 ESR)
Thunderbird is updated to version 17 ESR (from 10 ESR)

Please note that some of your privately installed
application plugins - if any - will need to be updated
to versions compatible with new release - this is
performed automatically on application restart.

Please also note that SLC5/SLC6 Firefox and
Thunderbird updates follow Mozilla's
'Extended Support Release' schemas:

<a href="http://www.mozilla.org/en-US/firefox/organizations/">http://www.mozilla.org/en-US/firefox/organizations/</a>
<a href="http://www.mozilla.org/en-US/thunderbird/organizations/">http://www.mozilla.org/en-US/thunderbird/organizations/</a>

and NOT 'end user' releases: therefore next Firefox and
Thunderbird major version upgrade will be made available
in December 2013 or January 2014, until then all minor upgrades
will be based on versions 17 ESR.


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="devtoolset-1.1"></a><h3>07.02.2013: Developer Toolset (gcc/g++/gfortran - 4.7.2) available for SLC 5 and 6.</h3></td>
</tr>
<tr>
<td>
<pre>
Developer Toolset is an offering for developers on the Scientific Linux CERN 5 and 6 platforms.
Using a framework called Software Collections, an additional set of tools is installed into the
/opt directory, as recommended by the UNIX Filesystem Hierarchy Standard. These tools are
enabled by the user on demand using the supplied scl utility.

Developer Toolset 1.1 provides following tools:

    gcc/g++/gfortran - GNU Compiler Collection - version 4.7.2

    gdb - GNU Debugger - version 7.5

    binutils - A GNU collection of binary utilities - version 2.23.51

    elfutils - A collection of utilities and DSOs to handle compiled objects - version 0.154

    dwz - DWARF optimization and duplicate removal tool - version 0.7

    systemtap - Programmable system-wide instrumentation system - version 1.8

    valgrind - Tool for finding memory management bugs in programs - version 3.8.1

    oprofile - System wide profiler - version 0.9.7

For more information and installation instructions, please visit:

<a href="http://cern.ch/linux/devtoolset/">http://cern.ch/linux/devtoolset/</a>

<a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="SLC59"></a><h3>29.01.2013: Scientific Linux CERN 5.9 becomes default production SLC5 release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 5 (SLC5) version:

          ***********************************************************
          SLC 5.9 becomes default SLC5 production version
          ***********************************************************

                as of Tuesday 29.01.2013

* What is Scientific Linux CERN 5.9 (SLC59)  ?

  - A minor release of SLC5X series with all the
    updates since release of 5.9 (and up to 11.01.2013)
    integrated in the base installation tree.

* Upgrading from previous SLC5X releases:

  - fully updated SLC 5X systems do not need any upgrade to
    5.9 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific5/docs/quickupdate">http://cern.ch/linux/scientific5/docs/quickupdate</a>

* Installation media images for SLC59 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc59/i386/images/">http://linuxsoft.cern.ch/cern/slc59/i386/images/</a>
        (install path: linuxsoft.cern.ch:/cern/slc59/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc59/x86_64/images/">http://linuxsoft.cern.ch/cern/slc59/x86_64/images/</a>
        (install path: linuxsoft.cern.ch:/cern/slc59/x86_64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

* ISO DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc59/iso/">http://linuxsoft.cern.ch/cern/slc59/iso/</a>

* Certification of the SLC 5.9:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="cern-get-keytab"></a><h3>SLC5/6: Change of host Kerberos identity (keytab) generation system - 29.01.2013</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users,

On

              Tuesday 29 January 2013

the current host Kerberos identity generation system
(cern-config-keytab) used to generate
"/etc/krb5.keytab" files on CERN linux systems will
be replaced by a new mechanism: cern-get-keytab.

New system provides similar functionality but it has
been designed to allow better integration with CERN
Active Directory which is used for Kerberos
authentication of Linux systems at CERN since more
than a year.

If you would like to test this new system before the
deployment, please follow the procedure:

On an SLC system run as root:

SLC6:
# yum --enablerepo=slc6-testing install ncm-srvtab

SLC5:
# yum --enablerepo=slc5-testing install ncm-srvtab

Next as root execute:

# lcm --configure srvtab

(or to use new client tool directly:

# cern-get-keytab (--verbose)

wait few seconds, then verify the resulting new keytab
file by running as root:

# klist -kt
# kinit -k
# klist

You should see valid Kerberos credentials for
your system as:

Ticket cache: FILE:/tmp/krb5cc_0_XXXXXXXX
Default principal: host/XXXXXX.cern.ch@CERN.CH

[...]


Please note:

- Generating new host keytab invalidates the current
  one (if you try to contact your host using
  kerberized ssh, please run kinit -R first on source
  host to avoid permission denied message)

- Keytabs generated using new system contain
  additional Kerberos keys in form:

  (yyyy/)XXXXX$@CERN.CH

  (where XXXXX matches the hostname)

- Existing host keytabs remain valid: regeneration
  using new system happens only when one of following
  is executed by root:

    lcm --configure --all (or --configure srvtab)
    cern-get-keytab (--verbose)

- Old system for generating keytabs (cern-config-keytab)
  remains in service but will be deprecated as of
  30 of January 2013 and stopped in June 2013.

- SLC4: Scientific Linux CERN 4 support for experiments
  will end in June 2013 (3 months after LHC Long
  Shutdown 1 begins): until then the old system can be
  used. New system will not be backported to SLC4.

- For more information about new cern-get-keytab tool
  use: cern-get-keytab --help or man cern-get-keytab
  after installing it.

Jaroslaw Polok for Linux.Support@cern.ch

</pre>
</td>
</tr>
<tr>
<td><a name="java6eol"></a><h3>Oracle (Sun) Java 6 (1.6) End of Life notice: February 2013</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users,

Following the Oracle support policy / end of life announcement we would like to
inform you that the Oracle (formerly Sun) Java 6 (1.6.X) will no more receive
security and functional updates after February 2013.

We would like therefore to encourage all SLC Sun Java 6 users to migrate to already
provided Oracle Java 7 (1.7.X) or OpenJDK Java 6/7 before the end of February 2013.

In order to install Oracle Java 7 on your SLC system please run as root:

# /usr/bin/yum install java-1.7.0-oracle

In order to install OpenJDK Java 6 on your SLC system please run as root:

# /usr/bin/yum install java-1.6.0-openjdk

In order to install OpenJDK Java 7 on your SLC system please run as root:

# /usr/bin/yum install java-1.7.0-openjdk

All above Java versions (and the current java-1.6.0-sun) can coexist on an
SLC system, newest version becoming the default one and Oracle version taking
precedence over OpenJDK version.

In order to select the default Java version on your system you may run as
root:

# /usr/sbin/alternatives --config java

To see the Oracle Java Support / End of Life policy, please visit
"Java SE 6 End of Public Updates Notice" at:

<a href="http://www.oracle.com/technetwork/java/eol-135779.html">http://www.oracle.com/technetwork/java/eol-135779.html</a>

Jaroslaw Polok for Linux.Support@cern.ch

</pre>
</td>
</tr>

<tr>
<td><a name="slc59test"></a><h3>15.01.2013: Scientific Linux CERN 5.9 TEST available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users,

We would like to announce that:

   ********************************************
   Scientific Linux CERN 5.9 TEST (SLC59)
   ********************************************

   is available for tests as of Tuesday 15.01.2013.

* What is Scientific Linux CERN 5 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 5 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 5.9 (SLC59) TEST ?

 - TEST minor release of SLC5X series with all the
   updates (up to 11.01.2013) integrated in the base
   installation tree.

* Upgrading from previous SLC4X releases:

 - Upgrade is not supported and has not been tested.

* Installation media images for SLC59 can be found at:

   For 32-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc59/i386/images/">http://linuxsoft.cern.ch/cern/slc59/i386/images/</a>
       (install path: linuxsoft:/cern/slc59/i386/)

   For 64-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc59/x86_64/images/">http://linuxsoft.cern.ch/cern/slc59/x86_64/images/</a>
       (install path: linuxsoft:/cern/slc59/x86_64/)

   To install please follow the procedure at:

       <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>


* More information:

SLC5 web pages:

  <a href="http://cern.ch/linux/scientific5/">http://cern.ch/linux/scientific5/</a>

SLC5 installation:

  <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

SLC5 recent updates:

  <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>



* SLC59 version will become the default SLC5 production
   version as of:

                    Tuesday 29.01.2013
                  *************************

  providing that no show-stopper problems are found.

* Certification of the SLC59:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

* Problem reporting:

 Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="rhel59""></a><h3>11.01.2013 RHEL5.9 - Red Hat Enterprise Linux 5 Server Update 9 available</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Red Hat Enterprise Linux 5 Server Update 9 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

RHES_5_U9_I386
RHES_5_U9_X86_64

,installation paths are:

http://linuxsoft.cern.ch/enterprise/5Server_U9/en/os/i386/
http://linuxsoft.cern.ch/enterprise/5Server_U9/en/os/x86_64/

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel5">http://cern.ch/linux/rhel/#rhel5</a>


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="lxdist"></a><h3>linuxsoft.cern.ch installation service change on Monday 14th of January 2013</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users.

On Monday 14th of January 2013 we are going to perform an hardware upgrade of linuxsoft.cern.ch installation / updates
servers. After the change the NFS system installation method will be no more available.

Please note that the NFS installation method has been used in the past as an alternative method, recommended one being
http (URL) one. The last operating system requiring NFS installation was Red Hat Linux 7.3 for which support has been
stopped at the end of 2005.

Except for the above change the intervention should be transparent.

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="oracleinstantclient11"></a><h3>20.11.2012 SLC5: Oracle Instant Client upgrade to version 11 on Monday 10th of December 2012</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Scientific Linux CERN 5 Users.

Following CERN Oracle Support announcement, the Scientific Linux CERN 5 Oracle Instant Client
will be upgraded from version 10 to version 11 on Monday 10th of December 2012.

Since this upgrade is a major one, please test your applications for compatibility
with new version, following the documentation at:

<a href="/linux/scientific5/docs/oracle-instantclient">http://cern.ch/linux/scientific5/docs/oracle-instantclient</a>.

If problems are found, please report these to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>.

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="slc6resolvconf"></a><h3>31.07.2012 SLC6:  Name resolution issue / possible other problems on SLC6 systems (workaround)</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Scientific Linux CERN 6 Users.

Recent update of <b>glibc</b> package on your system may have introduced problems with
dns lookup with some daemons and tools (nscd, etc...).

In order to fix the problem please follow these instructions:

Delete "options rotate" line in your /etc/resolv.conf

Proper fix (updated glibc package) will be supplied when available,
please see:

https://bugzilla.redhat.com/show_bug.cgi?id=841787
https://access.redhat.com/knowledge/solutions/171483

for more information.

Our apologies for problems caused on your SLC6 systems.

Thomas Oulevey for Linux.Support@cern.ch

</pre>
</td>
</tr>
<tr>
<td><a name="slc5nsswitchconf"></a><h3>25.07.2012 SLC5: authentication / possible other problems on SLC5 systems (workaround)</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Scientific Linux CERN 5 Users.

Recent update of <b>sudo</b> package on your system may have introduced problems with
user authentication and unsuccessful restarts of some of system services.

In order to fix the problem please execute as root following workaround:

# /sbin/restorecon /etc/nsswitch.conf && /sbin/service nscd condrestart

Proper fix (updated sudo package) will be supplied when available,
please see:

https://bugzilla.redhat.com/show_bug.cgi?id=818585

for more information.

Our apologies for problems caused on your SLC5 systems.

Jaroslaw Polok for Linux.Support@cern.ch

</pre>
</td>
</tr>
<tr>
<td><a name="cernssocookie"></a><h3>24.07.2012  CERN Single Sign On (SSO) authentication using scripts/programs</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Scientific Linux CERN users.

Following today <a href="http://itssb.web.cern.ch/planned-intervention/single-sign-update-24th-july/24-07-2012">update of CERN Single Sign On system (SSO)</a> we would like to announce the possibility
of accessing web pages protected by CERN SSO using scripts/programs. For detailed information please read documentation
at:

<a href="http://cern.ch/linux/docs/cernssocookie">http://cern.ch/linux/docs/cernssocookie</a>

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="smartcardspilot"></a><h3>23.07.2012 CERN SmartCards <em>PILOT</em> on Scientific Linux CERN 6</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Scientific Linux CERN 6 users.

In 2012 new <a href="https://smartcards.web.cern.ch/smartcards/">CERN SmartCards <em>PILOT</em></a> started.

Today we would like to announce the possibility of using CERN SmartCards on Scientific Linux CERN 6, for system authentication, web services authentication, e-mail signing / encryption and other purposes.

Please read:

<a href="http://cern.ch/linux/docs/smartcards">http://cern.ch/linux/docs/smartcards</a>

for information about required setup.

Since the procedure is <em>EXPERIMENTAL</em> and needs YOUR
testing, if you would like to participate please
subscribe to linux-users@cern.ch at:

<a href="http://listboxservices.web.cern.ch/listboxservices/simba2/free/subscription.aspx?list=linux-users">http://listboxservices.web.cern.ch/listboxservices/simba2/free/subscription.aspx?list=linux-users</a>

and send your feedback there.

Best Regards

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="SLC63"></a><h3>09.07.2012: Scientific Linux CERN 6.3 becomes default production SLC6 release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 6 (SLC6) version:

          ***********************************************************
          SLC 6.3 becomes default SLC6 production version
          ***********************************************************

                as of Monday 09.07.2012

* What is Scientific Linux CERN 6.3 (SLC63)  ?

  - A minor release of SLC6X series with all the
    updates since release of 6.2 (and up to 25.06.2012)
    integrated in the base installation tree.

* Upgrading from previous SLC6X releases:

  - fully updated SLC 6X systems do not need any upgrade to
    6.3 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

* Installation media images for SLC63 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc63/i386/images/">http://linuxsoft.cern.ch/cern/slc63/i386/images/</a>
        (install path: linuxsoft:/cern/slc63/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc63/x86_64/images/">http://linuxsoft.cern.ch/cern/slc63/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc63/x86_64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific6/docs/install">http://cern.ch/linux/scientific6/docs/install</a>

<!--
* ISO DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc63/iso/">http://linuxsoft.cern.ch/cern/slc63/iso/</a>
-->

* Certification of the SLC 6.3:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="leapsecondbug""></a><h3>5.07.2012 Scientific Linux CERN 6 systems affected by 'leap second' bug</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Scientific Linux CERN 6 users.

All SLC6 systems not rebooted after midnight, June 30th 2012 are potentially affected by
a 'leap second' bug causing following symptoms:

- high system CPU load.

- failures in interprocess communications in some subsystems (Java, MySQL and others).

Please note that in some cases symptoms may be not immediately noticeable, but
the system may still be affected.

In order to fix your system please run following command as root:

# /bin/date -s "`/bin/date`"

We would like to apologize for the late notice of the problem, unfortunately it
was discovered only after the leap second insertion, therefore too late for any
preventive measure.

For more information, please see:

- Leap Seconds: http://tycho.usno.navy.mil/leapsec.html
- Bug description: http://marc.info/?l=linux-kernel&m=134113577921904&w=2

Jaroslaw Polok for Linux Support.

</pre>
</td>
</tr>

<tr>
<td><a name="slc63beta"></a><h3>26.06.2012 Scientific Linux CERN 6.3 BETA available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users.

We have a pleasure to announce that:

     BETA version of SLC 6.3 for i386/x86_64
     ***************************************

is available as of today.

* What is Scientific Linux CERN 6 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 6 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 6.3 (SLC63) TEST ?

 - TEST minor release of SLC6X series with all the
   updates (up to 21.06.2012) integrated in the base
   installation tree.

* Installation

- via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

- non-PXE installation:

Boot media can be found at:

<a href="http://linuxsoft.cern.ch/cern/slc63/i386/images/">http://linuxsoft.cern.ch/cern/slc63/i386/images/</a>
<a href="http://linuxsoft.cern.ch/cern/slc63/x86_64/images/">http://linuxsoft.cern.ch/cern/slc63/x86_64/images/</a>

and corresponding installation paths are:

http://linuxsoft.cern.ch/cern/slc63/i386/
http://linuxsoft.cern.ch/cern/slc63/x86_64/

- Automated PXE installations using AIMS2

 Boot targets are:

 SLC63_BETA_I386
 SLC63_BETA_X86_64

* Upgrading from previous SLC6X releases:

  - fully updated SLC 6X systems do not need any upgrade to
    6.3 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

Please note:

* SLC63 will become the default SLC6 production version as of:

                    Monday 09.07.2012
                  *************************

  providing that no show-stopper problems are found.

* Certification of the SLC63:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

* Problem reporting:

 Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="rhel63""></a><h3>21.06.2012 RHEL 6.3 - Red Hat Enterprise Linux 6 Server Update 3 available</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Red Hat Enterprise Linux 6 Server Update 3 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

RHEL6_U3_I386
RHEL6_U3_X86_64

,installation paths are:

http://linuxsoft.cern.ch/enterprise/6Server_U3/en/os/i386/
http://linuxsoft.cern.ch/enterprise/6Server_U3/en/os/x86_64/

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel.shml#rhel6">http://cern.ch/linux/rhel.shml#rhel6</a>


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="SLC58"></a><h3>27.03.2012: Scientific Linux CERN 5.8 becomes default production SLC5 release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 5 (SLC5) version:

          ***********************************************************
          SLC 5.8 becomes default SLC5 production version
          ***********************************************************

                as of Tuesday 27.03.2012

* What is Scientific Linux CERN 5.8 (SLC58)  ?

  - A minor release of SLC5X series with all the
    updates since release of 5.7 (and up to 10.03.2012)
    integrated in the base installation tree.

* Upgrading from previous SLC5X releases:

  - fully updated SLC 5X systems do not need any upgrade to
    5.8 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific5/docs/quickupdate">http://cern.ch/linux/scientific5/docs/quickupdate</a>

* Installation media images for SLC58 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc58/i386/images/">http://linuxsoft.cern.ch/cern/slc58/i386/images/</a>
        (install path: linuxsoft:/cern/slc58/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc58/x86_64/images/">http://linuxsoft.cern.ch/cern/slc58/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc58/x86_64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

<!--
* ISO DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc56/iso/">http://linuxsoft.cern.ch/cern/slc58/iso/</a>
-->

* Certification of the SLC 5.8:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="slc58test"></a><h3>20.03.2012: Scientific Linux CERN 5.8 TEST available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users,

We would like to announce that:

   ********************************************
   Scientific Linux CERN 5.8 TEST (SLC58)
   ********************************************

   is available for tests as of Tuesday 20.03.2012.

* What is Scientific Linux CERN 5 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 5 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 5.8 (SLC58) TEST ?

 - TEST minor release of SLC5X series with all the
   updates (up to 10.03.2012) integrated in the base
   installation tree.

* Upgrading from previous SLC4X releases:

 - Upgrade is not supported and has not been tested.

* Installation media images for SLC58 can be found at:

   For 32-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc58/i386/images/">http://linuxsoft.cern.ch/cern/slc58/i386/images/</a>
       (install path: linuxsoft:/cern/slc58/i386/)

   For 64-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc58/x86_64/images/">http://linuxsoft.cern.ch/cern/slc58/x86_64/images/</a>
       (install path: linuxsoft:/cern/slc58/x86_64/)

   To install please follow the procedure at:

       <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>


* More information:

SLC5 web pages:

  <a href="http://cern.ch/linux/scientific5/">http://cern.ch/linux/scientific5/</a>

SLC5 installation:

  <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

SLC5 recent updates:

  <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>



* SLC58 version will become the default SLC5 production
   version as of:

                    Tuesday 27.03.2012
                  *************************

  providing that no show-stopper problems are found.

* Certification of the SLC58:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

* Problem reporting:

 Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="firefox10thunderbird10"></a><h3>01.03.2012 SLC5 / SLC6: Major Firefox and Thunderbird update. </h3></td>
</tr>
<tr>
<td>
<pre>
Dear Scientific Linux CERN 5/6 Users.

Tonight SLC5 and SLC6 updates:

<a href="http://cern.ch/linux/updates/updates-slc6#20120301">http://cern.ch/linux/updates/updates-slc6#20120301</a>
<a href="http://cern.ch/linux/updates/updates-slc5#20120301">http://cern.ch/linux/updates/updates-slc5#20120301</a>

include major version update for Mozilla Firefox and
Mozilla Thunderbird products.

Firefox is updated to version 10 ESR (from 3.6)
Thunderbird is updated to version 10 ESR (from 3.1)

Please note that some of your privately installed
application plugins - if any - will need to be updated
to versions compatible with new release - this is
performed automatically on application restart.

Please also note that future SLC5/SLC6 Firefox and
Thunderbird updates will follow Mozilla's
'Extended Support Release' schemas:

<a href="http://www.mozilla.org/en-US/firefox/organizations/">http://www.mozilla.org/en-US/firefox/organizations/</a>
<a href="http://www.mozilla.org/en-US/thunderbird/organizations/">http://www.mozilla.org/en-US/thunderbird/organizations/</a>

and NOT 'end user' releases: therefore next Firefox and
Thunderbird major version upgrade will be made available
in February 2013, until then all minor upgrades will be
based on version 10.


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="rhel58""></a><h3>27.02.2012 RHEL5.8 - Red Hat Enterprise Linux 5 Server Update 8 available</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Red Hat Enterprise Linux 5 Server Update 8 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

RHES_5_U8_I386
RHES_5_U8_X86_64

,installation paths are:

http://linuxsoft.cern.ch/enterprise/5Server_U8/en/os/i386/
http://linuxsoft.cern.ch/enterprise/5Server_U8/en/os/x86_64/

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel5">http://cern.ch/linux/rhel/#rhel5</a>


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="slc6cert"></a><h3>01.02.2012: Scientific Linux CERN 6 certified</h3></td>
</tr>
<tr>
<td>
<pre>
Dear readers.

We would like to announce that:

            ******************************

            Scientific Linux CERN 6 (SLC6)

            ******************************

                  is now certified

	    ******************************

* This Linux version becomes as of 1st of February 2012
  the default supported CERN Linux version.

* Not all software environments have been made available
  for SLC6 yet, please see <a href="http://cern.ch/linux/scientific6/cert/">certification</a> pages for details.

* The previous Scientific Linux CERN 5 (SLC5) version
  is still supported but we encourage all users and
  administrators to migrate to SLC6 as soon as possible.

* What is Scientific Linux CERN 6 ?
  It is a CERN customized Linux distribution built on
  top of common base platform - Scientific Linux -
  which was in turn built from freely available Red Hat
  Enterprise Linux 6 sources by a joint Fermi and CERN
  effort.

* More information:

  <a href="http://cern.ch/linux/scientific6/">http://cern.ch/linux/scientific6/</a>

  Installation instructions:

  <a href="http://cern.ch/linux/scientific6/docs/install">http://cern.ch/linux/scientific6/docs/install</a>

  Certification overview:

  <a href="http://cern.ch/linux/scientific6/cert/">http://cern.ch/linux/scientific6/cert/</a>

* SLC6 is available for network installs at CERN and DVD images are available
  at <a href="http://linuxsoft.cern.ch/cern/slc62/iso/">http://linuxsoft.cern.ch/cern/slc62/iso/</a>.

Our thanks go to all participants, especially to
the members of the Linux certification coordination
body.

  <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="slc62beta"></a><h3>13.12.2011 Scientific Linux CERN 6.2 BETA available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users.

We have a pleasure to announce that:

     BETA version of SLC 6.2 for i386/x86_64
     ***************************************

is available as of today.

* What is Scientific Linux CERN 6 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 6 Server sources by a joint Fermi and CERN
effort.

* Installation

- via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

- non-PXE installation:

Boot media can be found at:

<a href="http://linuxsoft.cern.ch/cern/slc62/i386/images/">http://linuxsoft.cern.ch/cern/slc62/i386/images/</a>
<a href="http://linuxsoft.cern.ch/cern/slc62/x86_64/images/">http://linuxsoft.cern.ch/cern/slc62/x86_64/images/</a>

and corresponding installation paths are:

http://linuxsoft.cern.ch/cern/slc62/i386/
http://linuxsoft.cern.ch/cern/slc62/x86_64/

- Automated PXE installations using AIMS2

 Boot targets are:

 SLC62_BETA_I386
 SLC62_BETA_X86_64

Please note:

- this is a pre-production version of future CERN
  certified and recommended Linux version.

- some of CERN specific software, has not been made
  available for this platform yet.

- this version will become a certified 'general purpose'
  CERN Linux version at the end of January 2012.

* More information

For more information about Scientific Linux CERN 6,
please see: <a href="http://cern.ch/linux/scientific6/">http://cern.ch/linux/scientific6/</a>


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>


<tr>
<td><a name="rhel62""></a><h3>06.12.2011 RHEL6.2 - Red Hat Enterprise Linux 6 Server Update 2 available</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Red Hat Enterprise Linux 6 Server Update 2 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

RHEL6_U2_I386
RHEL6_U2_X86_64

,installation paths are:

http://linuxsoft.cern.ch/enterprise/6Server_U2/en/os/i386/
http://linuxsoft.cern.ch/enterprise/6Server_U2/en/os/x86_64/

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel.shml#rhel6">http://cern.ch/linux/rhel.shml#rhel6</a>


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>


<tr>
<td><a name="SLC57"></a><h3>16.08.2011: Scientific Linux CERN 5.7 becomes default production SLC5 release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 5 (SLC5) version:

          ***********************************************************
          SLC 5.7 becomes default SLC5 production version
          ***********************************************************

                as of Tuesday 16.08.2011

* What is Scientific Linux CERN 5.7 (SLC57)  ?

  - A minor release of SLC5X series with all the
    updates since release of 5.7 (and up to 25.07.2011)
    integrated in the base installation tree.

* Upgrading from previous SLC5X releases:

  - fully updated SLC 5X systems do not need any upgrade to
    5.7 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific5/docs/quickupdate">http://cern.ch/linux/scientific5/docs/quickupdate</a>

* Installation media images for SLC57 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc57/i386/images/">http://linuxsoft.cern.ch/cern/slc57/i386/images/</a>
        (install path: linuxsoft:/cern/slc57/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc57/x86_64/images/">http://linuxsoft.cern.ch/cern/slc57/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc57/x86_64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

* ISO DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc56/iso/">http://linuxsoft.cern.ch/cern/slc57/iso/</a>


* Certification of the SLC 5.7:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="slc57test"></a><h3>27.07.2011: Scientific Linux CERN 5.7 TEST available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users,

We would like to announce that:

   ********************************************
   Scientific Linux CERN 5.7 TEST (SLC57)
   ********************************************

   is available for tests as of Wednesday 27.07.2011.

* What is Scientific Linux CERN 5 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 5 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 5.7 (SLC57) TEST ?

 - TEST minor release of SLC5X series with all the
   updates (up to 25.07.2011) integrated in the base
   installation tree.

* Upgrading from previous SLC4X releases:

 - Upgrade is not supported and has not been tested.

* Installation media images for SLC57 can be found at:

   For 32-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc57/i386/images/">http://linuxsoft.cern.ch/cern/slc57/i386/images/</a>
       (install path: linuxsoft:/cern/slc57/i386/)

   For 64-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc57/x86_64/images/">http://linuxsoft.cern.ch/cern/slc57/x86_64/images/</a>
       (install path: linuxsoft:/cern/slc57/x86_64/)

   To install please follow the procedure at:

       <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>


* More information:

SLC5 web pages:

  <a href="http://cern.ch/linux/scientific5/">http://cern.ch/linux/scientific5/</a>

SLC5 installation:

  <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

SLC5 recent updates:

  <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>



* SLC57 version will become the default SLC5 production
   version as of:

                    Tuesday 16.08.2011
                  *************************

  providing that no show-stopper problems are found.

* Certification of the SLC57:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

* Problem reporting:

 Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="rhel57"></a><h3>25.07.2011 Red Hat Enterprise Linux 5 Update 7 available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Red Hat Enterprise Linux 5 Update 7 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

RHES_5_U7_I386
RHES_5_U7_I386_XEN
RHES_5_U7_X86_64
RHES_5_U7_X86_64_XEN

,installation paths are:

http://linuxsoft.cern.ch/enterprise/5Server_U7/en/os/i386/
http://linuxsoft.cern.ch/enterprise/5Server_U7/en/os/x86_64/

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux
is a licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: http://cern.ch/linux/rhel.shml#rhel5


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="slc61beta"></a><h3>09.06.2011 Scientific Linux CERN 6.1 BETA available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users.

We have a pleasure to announce that:

     BETA version of SLC 6.1 for i386/x86_64
     ***************************************

is available as of today.

* What is Scientific Linux CERN 6 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 6 Server sources by a joint Fermi and CERN
effort.

* Installation

- via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

- non-PXE installation:

Boot media can be found at:

<a href="http://linuxsoft.cern.ch/cern/slc61/i386/images/">http://linuxsoft.cern.ch/cern/slc61/i386/images/</a>
<a href="http://linuxsoft.cern.ch/cern/slc61/x86_64/images/">http://linuxsoft.cern.ch/cern/slc61/x86_64/images/</a>

and corresponding installation paths are:

http://linuxsoft.cern.ch/cern/slc61/i386/
http://linuxsoft.cern.ch/cern/slc61/x86_64/

- Automated PXE installations using AIMS2

 Boot targets are:

 SLC61_BETA_I386
 SLC61_BETA_X86_64

Please note:

- this is a pre-production version of future CERN
  certified and recommended Linux version.

- most of CERN specific software, has not been made
  available for this platform yet.

- this version will become a certified 'general purpose'
  CERN Linux version before September 2011.

- Full certification, including all experiments environments
  and third-party software is expected to finish before end of 2011.

* More information

For more information about Scientific Linux CERN 6,
please see: <a href="http://cern.ch/linux/scientific6/">http://cern.ch/linux/scientific6/</a>


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="rhel61""></a><h3>20.05.2011 RHEL6.1 - Red Hat Enterprise Linux 6 Server Update 1 available</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Red Hat Enterprise Linux 6 Server Update 1 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

RHEL6_U1_I386
RHEL6_U1_X86_64

,installation paths are:

http://linuxsoft.cern.ch/enterprise/6Server_U1/en/os/i386/
http://linuxsoft.cern.ch/enterprise/6Server_U1/en/os/x86_64/

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: http://cern.ch/linux/rhel.shml#rhel6


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="SLC56"></a><h3>21.02.2011: Scientific Linux CERN 5.6 becomes default production SLC5 release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 5 (SLC5) version:

          ***********************************************************
          SLC 5.6 becomes default SLC5 production version
          ***********************************************************

                as of Monday 21.02.2011

* What is Scientific Linux CERN 5.6 (SLC56)  ?

  - A minor release of SLC5X series with all the
    updates since release of 5.5 (and up to 01.02.2011)
    integrated in the base installation tree.

* Upgrading from previous SLC5X releases:

  - fully updated SLC 5X systems do not need any upgrade to
    5.6 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific5/docs/quickupdate">http://cern.ch/linux/scientific5/docs/quickupdate</a>

* Installation media images for SLC56 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc56/i386/images/">http://linuxsoft.cern.ch/cern/slc56/i386/images/</a>
        (install path: linuxsoft:/cern/slc56/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc56/x86_64/images/">http://linuxsoft.cern.ch/cern/slc56/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc56/x86_64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

* ISO DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc56/iso/">http://linuxsoft.cern.ch/cern/slc56/iso/</a>
        (will be available before 25.02.2011)

* Certification of the SLC 5.6:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="slc56test"></a><h3>14.02.2011: Scientific Linux CERN 5.6 TEST available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users,

We would like to announce that:

   ********************************************
   Scientific Linux CERN 5.6 TEST (SLC56)
   ********************************************

   is available for tests as of Monday 14.02.2011.

* What is Scientific Linux CERN 5 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 5 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 5.6 (SLC56) TEST ?

 - TEST minor release of SLC5X series with all the
   updates (up to 14.02.2011) integrated in the base
   installation tree.

* Upgrading from previous SLC4X releases:

 - Upgrade is not supported and has not been tested.

* Installation media images for SLC56 can be found at:

   For 32-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc56/i386/images/">http://linuxsoft.cern.ch/cern/slc56/i386/images/</a>
       (install path: linuxsoft:/cern/slc56/i386/)

   For 64-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc56/x86_64/images/">http://linuxsoft.cern.ch/cern/slc56/x86_64/images/</a>
       (install path: linuxsoft:/cern/slc56/x86_64/)

   To install please follow the procedure at:

       <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>


* More information:

SLC5 web pages:

  <a href="http://cern.ch/linux/scientific5/">http://cern.ch/linux/scientific5/</a>

SLC5 installation:

  <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

SLC5 recent updates:

  <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>



* SLC56 version will become the default SLC5 production
   version as of:

                    Monday 21.02.2011
                  *************************

  providing that no show-stopper problems are found.

* Certification of the SLC56:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

* Problem reporting:

 Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="slc60beta"></a><h3>14.02.2011 Scientific Linux CERN 6.0 BETA available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users.

We have a pleasure to announce that:

     BETA version of SLC 6.0 for i386/x86_64
     ***************************************

is available as of today.

* What is Scientific Linux CERN 6 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 6 Server sources by a joint Fermi and CERN
effort.

* Installation

- via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

- non-PXE installation:

Boot media can be faound at:

<a href="http://linuxsoft.cern.ch/cern/slc60beta/i386/images/">http://linuxsoft.cern.ch/cern/slc60beta/i386/images/</a>
<a href="http://linuxsoft.cern.ch/cern/slc60beta/x86_64/images/">http://linuxsoft.cern.ch/cern/slc60beta/x86_64/images/</a>

and corresponding installation paths are:

http://linuxsoft.cern.ch/cern/slc60beta/i386/
http://linuxsoft.cern.ch/cern/slc60beta/x86_64/

- Automated PXE installations using AIMS2

 Boot targets are:

 SLC60_BETA_I386
 SLC60_BETA_X86_64

Please note:

- this is a pre-production version of future CERN
  certified and recommended Linux version.

- as such it is not meant to be used on production
  systems.

- no CERN specific software, except basic CERN
  environment tools has been made available for
  this platform yet.

- no formal support is provided for this pre-release.

* More information

For more information about Scientific Linux CERN 6,
please see: <a href="http://cern.ch/linux/scientific6/">http://cern.ch/linux/scientific6/</a>


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>


<tr>
<td><a name="krbmigr"></a><h3>01.02.2011 Migration of Kerberos infrastructure (1st of February 2011)</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux Users

Tonight (1st of February 2011) SLC Linux systems at CERN
will start migration to the unified Kerberos
infrastructure:

http://cern.ch/ssb/ServiceChangeArchive/110120-Kerberos.htm

It is possible to migrate your SLC system to the new
infrastructure now, by running on your system as root:

# yum clean all && yum update

For more information about unified Kerberos
infrastructure please see:

http://indico.cern.ch/conferenceDisplay.py?confId=73735
(Plans for a single Kerberos realm)

For troubleshooting information please see:

https://twiki.cern.ch/twiki/bin/view/AFSService/MigrationFAQ


Jaroslaw Polok for Linux.Support@cern.ch

</pre>
</td>
</tr>


<tr>
<td><a name="cernalerter"></a><h3>26.01.2011 CERN Alerter for Linux available (SLC5/SLC6) </h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users

As of today we make the test version of

          CERN Alerter for Linux

available for SLC5 and SLC6

What is CERN Alerter for Linux ?

It is a Linux implementation of well known CERN Alerter
application for Windows platform:

https://cern.ch/winservices/Help/?kbid=60810

For more information about Linux version, please see:

https://cern.ch/twiki/bin/view/LinuxSupport/CERNAlerterForLinux


How to install CERN Alerter for Linux ?

As root on your system run:

# yum install cern-alerter

Next, restart your Gnome/KDE session and the new icon
will appear in your panel along audio mixer and
other icons.

(or press Alt-F2 - Run Application - type in:
cern-alerter)

In the future SLC versions (starting with SLC5.6 and
SLC6) CERN Alerter for Linux will be pre-installed
on the system within the default package set.

I found a bug/problem - what next ?

Please mind: this is a first test version, some
problems may appear: please report to
linux.support@cern.ch

Jaroslaw Polok for Linux.Support@cern.ch

</pre>
</td>
</tr>



<tr>
<td><a name="krbtest"></a><h3>01.02.2011 Migration of Kerberos infrastructure on 1st of February (tests possible now) </h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux Users

On the 1st of February 2011 SLC Linux systems at CERN
will start migration to the unified Kerberos
infrastructure:

http://cern.ch/ssb/ServiceChangeArchive/110120-Kerberos.htm

For TEST purposes as of today it is possible to
migrate your SLC system to the new infrastructure
by running on your system as root:

SLC4:

# yum --enablerepo=slc4-test install lcm-profile

SLC5:

# yum --enablerepo=slc5-testing install lcm-profile

(note: SLC6 TEST1 systems are already preconfigured for
unified Kerberos infrastructure)

For more information about unified Kerberos
infrastructure please see:

http://indico.cern.ch/conferenceDisplay.py?confId=73735
(Plans for a single Kerberos realm)

For troubleshooting information please see:

https://twiki.cern.ch/twiki/bin/view/AFSService/MigrationFAQ


Jaroslaw Polok for Linux.Support@cern.ch

</pre>
</td>
</tr>

<tr>
<td><a name="rhel56"></a><h3>20.01.2011 Red Hat Enterprise Linux 5 Update 6 available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Red Hat Enterprise Linux 5 Update 6 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

RHES_5_U6_I386
RHES_5_U6_I386_XEN
RHES_5_U6_X86_64
RHES_5_U6_X86_64_XEN

,installation paths are:

http://linuxsoft.cern.ch/enterprise/5Server_U6/en/os/i386/
http://linuxsoft.cern.ch/enterprise/5Server_U6/en/os/x86_64/

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that this is a release version and as such
it is a licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: http://cern.ch/linux/rhel.shml#rhel5


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="slc60test1"></a><h3>30.11.2010 Scientific Linux CERN 6.0 TEST1 available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users.

We have a pleasure to announce that:


     PREVIEW/TEST1 version of SLC 6.0 for i386/x86_64
     ************************************************

is available for installation via PXE network boot menu
as of today. (Expert Menu -> SLC 6 PREVIEW/TEST Menu)

For non-PXE installations boot media can be found at

http://linuxsoft.cern.ch/cern/slc6X/i386/images/
http://linuxsoft.cern.ch/cern/slc6X/x86_64/images/

and corresponding installation paths are:

http://linuxsoft.cern.ch/cern/slc6X/i386/
http://linuxsoft.cern.ch/cern/slc6X/x86_64/

For automated PXE installations AIMS2 targets are:

SLC60_I386
SLC60_X86_64


Please note that this is an early PREVIEW version of
what will become the future CERN certified Linux platform.

As such:

    - it is not meant to be used on a production systems.

    - we do not guarantee timely updates including
      SECURITY updates.

    - there is no formal support. DO NOT REPORT PROBLEMS
      via CERN helpdesk.

    - no CERN software (except basic CERN environment
      tools, ie: hepix, addusercern, lpadmincern, openafs)
      has been made available for the platform.

    - the platform is almost completely untested.

    - there is no documentation, except a 'known
      problems' page:
      <a href="http://cern.ch/linux/scientific6/test1">http://cern.ch/linux/scientific6/test1</a>

However:

    - we invite you to give this TEST version a try.

    - we invite you to discuss problems/bugs/possible
      enhancements on linux-certification@cern.ch

Enjoy the testing !

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="slc4EoGUS""></a><h3>30.11.2010 <em>End of General User Support for SLC4 - December 2010</em></h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

As announced on June 2010 and October 2010 IT Technical Users meetings,
(see: <a href="http://indico.cern.ch/categoryDisplay.py?categId=2958">IT Technical Users Meeting</a> , CERN login required)
general user support for Scientific Linux CERN 4 (SLC4) will end on:


                   <em>31st of December 2010</em>


After that date user requests for SLC4 support will be <em>rejected</em> by
CERN service desk.

Dedicated support for experiments and accelerator controls will be
maintained for <em>three months</em> after 2011 LHC shutdown beginning but
no longer than February 2012.

At the end of the dedicated support phase SLC4 will reach
the End of Life phase when support will not be provided anymore
and no more software updates will be made available.


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="rhel60""></a><h3>12.11.2010 RHEL6 - Red Hat Enterprise Linux 6 Server available</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Red Hat Enterprise Linux 6 Server for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

RHEL6_U0_I386
RHEL6_U0_X86_64

,installation paths are:

http://linuxsoft.cern.ch/enterprise/6Server_U0/en/os/i386/
http://linuxsoft.cern.ch/enterprise/6Server_U0/en/os/x86_64/

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that this is a release version and as such
it is a licensed product installable only on authorized
systems (unlike the RHEL 6 beta versions which were
freely available). Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: http://cern.ch/linux/rhel.shml#rhel6


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="keytabsec2010"></a><h3>08.11.2010 <em>[SECURITY]</em> SLC5/SLC4 -  Security Advisory - Critical: Kerberos krb5.keytab exposure </h3></td>
</tr>
<tr>
<td>
<pre>
====================================================================
Security Advisory

Synopsis:          Critical: Kerberos krb5.keytab exposure
Product:           CERN Scientific Linux: SLC4 and SCL5
Issue date:        2010-11-8
=====================================================================


1. Summary:

A configuration error has exposed the Kerberos /etc/krb5.keytab file such that any local
user reading this file can get hold of its contents. Due to the nature of Kerberos, this
vulnerability allows any user with login access to impersonate the corresponding PC or
server. A RPM fixing this problem is available in the CERN SLC software repository.


2. Relevant releases/architectures:

Affected are all CERN Scientific Linux 4 and 5 versions which have been updated or
installed after September 15th 2010. However, we believe that his vulnerability is
particular to CERN and affects only PCs and servers which are connected to the CERN
Kerberos realm.

3. Description:

A configuration error has exposed the Kerberos /etc/krb5.keytab file such that any local
user reading this file can get hold of its contents. The key stored in this file is used
to authenticate a PC or server to other services, and for password-less authentication to
that PC or server. With the knowledge of the key, this vulnerability allows users who have
login access to impersonate the corresponding PC or server, and to perform actions under
the scope of the affected PC or server.  Technically experienced users with knowledge in
Kerberos might use this key to obtain root privileges on the corresponding PC or server.

However, we are not aware of any exploitation of this vulnerability so far.



4. Solution:

A RPM fixing this problem is available in the CERN SLC software repository. In order to
install the fix, please either wait for the next yum auto-update or run "yum update"
manually.

Users of affected PCs and servers with exposed Kerberos keys will receive an error
message when using SSH password-less authentication the next time and need to renew that
key using "kinit -R".

5. Contact:

Kerberos-Support@cern.ch
</pre>
</td>
</tr>



<tr>
<td><a name="cve20103081-Sept22"></a><h3>22.09.2010: SLC5X <em>[SECURITY]</em>: Production release of kernel addressing an exploitable kernel vulnerability </h3></td>
</tr>
<tr>
<td>
<pre>
Dear SLC5 x86_64 (64 bit) platform users.

We have released in production a new SLC5 kernel addressing the locally
exploitable security issue CVE-2010-3081. This kernel 2.6.18-194.11.4.el5
supersedes the "hotfix" kernel 2.6.18-194.11.3.el5.cve20103081 <a href="http://linux.web.cern.ch/linux/news#cve20103081">released last Thursday</a>.

In order to protect your system please apply
urgently following update by running as root:

# yum install kernel

and if your system is an Xen virtual machine or
hypervisor also run:

# yum install kernel-xen

and reboot your system for the update to take effect.

Best Regards

Jan van Eldik for Linux.Support@cern.ch

</pre>
</td>
</tr>

<tr>
<td><a name="cve20103081"></a><h3>16.09.2010: SLC5X - x86_64  <em>[SECURITY]</em>: urgent patch for an exploitable kernel vulnerability </h3></td>
</tr>
<tr>
<td>
<pre>
Dear SLC5 x86_64 (64 bit) platform users.

An exploit allowing local user privilege escalation has
been released recently, and has been already seen in the
wild. This exploit allows obtaining root access in an
unauthorized manner due to a vulnerability in the


                SLC5 x86_64 (64 bit) kernel.
                --------------------

SLC5 i386 (32 bit) and SLC4 (both x86_64 and i386
platforms) are not affected.

In order to protect your system please apply
urgently following update by running as root:

# yum --enablerepo=slc5-testing install kernel

and if your system is an Xen virtual machine or
hypervisor also run:

# yum --enablerepo=slc5-testing install kernel-xen

and reboot your system for the update to take effect.

Please note this is a temporary patched kernel and as
such it has not been thoroughly tested. Therefore,
it will not be released in regular SLC5 updates.

We expect an fully tested kernel update to arrive in
regular update repositories in coming days.

Best Regards

Jaroslaw Polok for Linux.Support@cern.ch

</pre>
</td>
</tr>

<tr>
<td><a name="ff364"></a><h3>24.06.2010:  Linux SLC4 and SLC5 Firefox and Flash plugin security updates.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Tonight an update containing updated Mozilla Firefox
and Adobe Flash player will be deployed on SLC4 and SLC5
systems.

The Firefox browser is updated to version 3.6.4 (from
3.0.19) which is a major version change:

due to this fact some privately installed firefox
plugins and/or extensions may stop to function on
your system after this upgrade is applied if these
are not compatible with 3.6 release.


The Adobe Flash player plugin is updated to version
9.0.277.0 on SLC4 and 10.1 on SLC5

On SLC5:

- unfortunately due to the Adobe decision to stop
providing 64bit native plugin on Linux (see:
http://labs.adobe.com/technologies/flashplayer10/releasenotes_64bit.html)
with this update we need to remove native flash player
support on 64bit (x86_64) SLC5 systems.

As a workaround SLC5 / x86_64 users may use 32bit
flash plugin within 64bit firefox browser after
installing the following:

# yum install flash-plugin.i386 nspluginwrapper

and restarting the browser.

On SLC4:

- Updated Firefox may display a warning about an
obsolete and potentially unsafe flash version
installed on your system: If your flash version is
9.0.277.0 please disregard this warning and do not
try to update to 10.1 release from Adobe, 10.1 does
not function on SLC4.

Please note that if you use the flash player provided
with SLC4 the 9.0.277.0 legacy version we ship is
secure. To check the version you use, please run:

# rpm -qi flash-plugin


We are sorry for late notice but as these are
security updates labeled 'critical' we cannot delay
the deployment without exposing all CERN users to
a security threat.

Jaroslaw Polok for Linux.Support@cern.ch

</pre>
</td>
</tr>

<tr>
<td><a name="SLC55"></a><h3>03.05.2010: Scientific Linux CERN 5.5 becomes default production SLC5 release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 5 (SLC5) version:

          ***********************************************************
          SLC 5.5 becomes default SLC5 production version
          ***********************************************************

                as of Monday 03.05.2010

* What is Scientific Linux CERN 5.5 (SLC55)  ?

  - A minor release of SLC5X series with all the
    updates since release of 5.4 (and up to 01.04.2010)
    integrated in the base installation tree.

* Upgrading from previous SLC5X releases:

  - fully updated SLC 5X systems do not need any upgrade to
    5.5 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific5/docs/quickupdate">http://cern.ch/linux/scientific5/docs/quickupdate</a>

* Installation media images for SLC55 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc55/i386/images/">http://linuxsoft.cern.ch/cern/slc55/i386/images/</a>
        (install path: linuxsoft:/cern/slc55/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc55/x86_64/images/">http://linuxsoft.cern.ch/cern/slc55/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc55/x86_64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

* ISO DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc55/iso/">http://linuxsoft.cern.ch/cern/slc55/iso/</a>

* Certification of the SLC 5.5:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="SLC55test"></a><h3>19.04.2010: Scientific Linux CERN 5.5 TEST available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users,

We would like to announce that:

   ********************************************
   Scientific Linux CERN 5.5 TEST (SLC55)
   ********************************************

   is available for tests as of Monday 19.04.2010.

* What is Scientific Linux CERN 5 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 5 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 5.5 (SLC55) TEST ?

 - TEST minor release of SLC5X series with all the
   updates (up to 07.04.2010) integrated in the base
   installation tree.

* Upgrading from previous SLC4X releases:

 - Upgrade is not supported and has not been tested.

* Installation media images for SLC55 can be found at:

   For 32-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc55/i386/images/">http://linuxsoft.cern.ch/cern/slc55/i386/images/</a>
       (install path: linuxsoft:/cern/slc55/i386/)

   For 64-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc55/x86_64/images/">http://linuxsoft.cern.ch/cern/slc55/x86_64/images/</a>
       (install path: linuxsoft:/cern/slc55/x86_64/)

   To install please follow the procedure at:

       <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>


* More information:

SLC5 web pages:

  <a href="http://cern.ch/linux/scientific5/">http://cern.ch/linux/scientific5/</a>

SLC5 installation:

  <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

SLC5 recent updates:

  <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>



* SLC55 version will become the default SLC5 production
   version as of:

                    Monday 03.05.2010
                  *************************

  providing that no show-stopper problems are found.

* Certification of the SLC55:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

* Problem reporting:

 Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="slcia64eol"></a><h3>25.01.2010: End of support for ia64 (Itanium II) on 31st of March 2010</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Scientific Linux CERN ia64 users.

 Due to a low demand and very marginal usage of
 ia64 architecture in Scientific Linux CERN users
 community we would like to announce that we will stop
 the support for it on the 31st of March 2010.

 - No further Scientific Linux CERN 4 (SLC4) and
   Scientific Linux CERN 5 (SLC5) updates will be
   provided for ia64 platform after that date.

 - No future versions of Scientific Linux CERN for
   ia64 will be released.

 - Current versions of SLC for ia64 will be removed
   from our installation service.

   Existing ia64 SLC4/SLC5 CERN users are encouraged to
   migrate to Red Hat Enterprise Linux 4/5 systems.
   Please contact linux.support@cern.ch if
   assistance with this migration process is needed.

   We would like to thank all ia64 users, especially
   the CERN Openlab members for the support, feedback
   and hardware supplied through the past years.

                         linux.support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="slc5cert"></a><h3>11.01.2010: Scientific Linux CERN 5 certified</h3></td>
</tr>
<tr>
<td>
<pre>
Dear readers.

We would like to announce that:

            ******************************

            Scientific Linux CERN 5 (SLC5)

            ******************************

                  is now certified

	    ******************************

* This Linux version becomes as of 11th of January 2010
  the default supported CERN Linux version.

* The previous Scientific Linux CERN 4 (SLC4) version
  is still supported but we encourage all users and
  administrators to migrate to SLC5 as soon as possible.

* What is Scientific Linux CERN 5 ?
  It is a CERN customized Linux distribution built on
  top of common base platform - Scientific Linux -
  which was in turn built from freely available Red Hat
  Enterprise Linux 5 sources by a joint Fermi and CERN
  effort.

* More information:

  <a href="http://cern.ch/linux/scientific5/">http://cern.ch/linux/scientific5/</a>

  Installation instructions:

  <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

  Certification overview:

  <a href="http://cern.ch/linux/scientific5/cert/">http://cern.ch/linux/scientific5/cert/</a>

* SLC5 is available for network installs and soon
  CD images will be provided.

Our thanks go to all participants, especially to
the members of the Linux certification coordination
body.

  <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="phoneobsolete"></a><h3>11.01.2010: REMINDER: 'phone' command obsolete.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux Users.

We would like to remind you that 'phone' command
will be obsoleted and deactivated on the 11th of
January 2010. From that date on please use
'phonebook' instead.

Please see details <a href="http://cern.ch/it-support-servicestatus/ServiceChangeArchive/091130-Linux-Phonebook.htm">here</a>.

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="SLC54test"></a><h3>28.09.2009: Scientific Linux CERN 5.4 TEST available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users,

We would like to announce that:

   ********************************************
   Scientific Linux CERN 5.4 TEST (SLC54)
   ********************************************

   is available for tests as of Monday 28.09.2009.

* What is Scientific Linux CERN 5 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 5 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 5.4 (SLC54) TEST ?

 - TEST minor release of SLC5X series with all the
   updates (up to 10.09.2009)integrated in the base
   installation tree.

* Upgrading from previous SLC4X releases:

 - Upgrade is not supported and has not been tested.

* Installation media images for SLC54 can be found at:

   For 32-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc54/i386/images/">http://linuxsoft.cern.ch/cern/slc54/i386/images/</a>
       (install path: linuxsoft:/cern/slc54/i386/)

   For 64-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc54/x86_64/images/">http://linuxsoft.cern.ch/cern/slc54/x86_64/images/</a>
       (install path: linuxsoft:/cern/slc54/x86_64/)

   For 64-bit Itanium 2 systems:

       <a href="http://linuxsoft.cern.ch/cern/slc54/ia64/images/">http://linuxsoft.cern.ch/cern/slc54/ia64/images/</a>
       (install path: linuxsoft:/cern/slc54/ia64/)


   To install please follow the procedure at:

       <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>


* More information:

SLC5 web pages:

  <a href="http://cern.ch/linux/scientific5/">http://cern.ch/linux/scientific5/</a>

SLC5 installation:

  <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

SLC5 recent updates:

  <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>

* Problem reporting:

 Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>



Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="SLC48"></a><h3>12.08.2009: Scientific Linux CERN 4.8 becomes default production SLC4 release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 4 (SLC4) version:

          ***********************************************************
          SLC 4.8 becomes default SLC4 production version
          ***********************************************************

                as of Wednesday 12.08.2009

* What is Scientific Linux CERN 4.8 (SLC48)  ?

  - A minor release of SLC4X series with all the
    updates since release of 4.7 (and up to 01.07.2009)
    integrated in the base installation tree.

* Upgrading from previous SLC4X releases:

  - fully updated SLC 4X systems do not need any upgrade to
    4.8 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific4/docs/quickupdate">http://cern.ch/linux/scientific4/docs/quickupdate</a>

* Installation media images for SLC48 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc48/i386/images/SL/">http://linuxsoft.cern.ch/cern/slc48/i386/images/SL/</a>
        (install path: linuxsoft:/cern/slc48/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc48/x86_64/images/SL/">http://linuxsoft.cern.ch/cern/slc47/x86_64/images/SL/</a>
        (install path: linuxsoft:/cern/slc48/x86_64/)

    For 64 bit Itanium 2 systems:

        <a href="http://linuxsoft.cern.ch/cern/slc48/ia64/images/SL/">http://linuxsoft.cern.ch/cern/slc48/ia64/images/SL/</a>
        (install path: linuxsoft:/cern/slc48/ia64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>

* ISO CD/DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc48/iso/">http://linuxsoft.cern.ch/cern/slc48/iso/</a>

* Certification of the SLC 4.8:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="SLC48test"></a><h3>27.07.2009: Scientific Linux CERN 4.8 test release available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN (SLC4) version:

           ************************************
            SLC 4.8 is available for tests.
           ************************************

               as of Monday 27.07.2009

* What is Scientific Linux CERN 4.8 (SLC48)  ?

   - A minor release of SLC4X series with all the
     updates since release of 4.7 (and up to 1.07.2009)
     integrated in the base installation tree.

* Upgrading from previous SLC4X releases:

   - fully updated SLC 4X systems do not need any upgrade to
     4.8 version, otherwise standard system upgrade procedure
     will bring systems up to date:
     http://cern.ch/linux/scientific4/docs/quickupdate

* Installation media images for SLC48 can be found at:

     For 32 bit systems:

         <a href="http://linuxsoft.cern.ch/cern/slc48/i386/images/SL/">http://linuxsoft.cern.ch/cern/slc48/i386/images/SL/</a>
         (install path: linuxsoft:/cern/slc48/i386/)

     For 64 bit systems:

         <a href="http://linuxsoft.cern.ch/cern/slc48/x86_64/images/SL/">http://linuxsoft.cern.ch/cern/slc48/x86_64/images/SL/</a>
         (install path: linuxsoft:/cern/slc48/x86_64/)

     For Itanium 2 systems:

         <a href="http://linuxsoft.cern.ch/cern/slc48/ia64/images/SL/">http://linuxsoft.cern.ch/cern/slc48/ia64/images/SL/</a>
         (install path: linuxsoft:/cern/slc48/ia64/)


    After downloading media images from above location
    standard installation procedure can be followed:

         <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>

* SLC 48 version will become the default SLC4 production
   version as of:

                    Monday 10.08.2009
                  *************************

  providing that no show-stopper problems are found.

* Certification of the SLC 48:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="SLC53test"></a><h3>25.02.2009: Scientific Linux CERN 5.3 TEST available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users,

We would like to announce that:

   **************************************
   Scientific Linux CERN 5.3 TEST (SLC53)
   **************************************

   is available for tests as of Wednesday 25.02.2009.

* What is Scientific Linux CERN 5 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 5 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 5.3 (SLC53) TEST ?

 - First TEST minor release of SLC5X series with all the
   updates (up to 15.02.2009)integrated in the base
   installation tree.

* Upgrading from previous SLC4X releases:

 - Upgrade is not supported and has not been tested.

* Installation media images for SLC53 can be found at:

   For 32-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc53/i386/images/">http://linuxsoft.cern.ch/cern/slc53/i386/images/</a>
       (install path: linuxsoft:/cern/slc53/i386/)

   For 64-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc53/x86_64/images/">http://linuxsoft.cern.ch/cern/slc53/x86_64/images/</a>
       (install path: linuxsoft:/cern/slc53/x86_64/)

   For 64-bit Itanium 2 systems:

       <a href="http://linuxsoft.cern.ch/cern/slc53/ia64/images/">http://linuxsoft.cern.ch/cern/slc53/ia64/images/</a>
       (install path: linuxsoft:/cern/slc53/ia64/)


   To install please follow the procedure at:

       <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>


* More information:

SLC5 web pages:

  <a href="http://cern.ch/linux/scientific5/">http://cern.ch/linux/scientific5/</a>

SLC5 installation:

  <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

SLC5 recent updates:

  <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>

* Problem reporting:

 Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>



Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<td><a name="SLC52test"></a><h3>08.09.2008: Scientific Linux CERN 5.2 TEST available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users,

We would like to announce that:

   **************************************
   Scientific Linux CERN 5.2 TEST (SLC52)
   **************************************

   is available for tests as of Monday 08.09.2008.

* What is Scientific Linux CERN 5 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 5 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 5.2 (SLC52) TEST ?

 - First TEST minor release of SLC5X series with all the
   updates (up to 03.09.2008)integrated in the base
   installation tree.

* Upgrading from previous SLC4X releases:

 - Upgrade is not supported and has not been tested yet.

* Installation media images for SLC52 can be found at:

   For 32-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc52test/i386/images/">http://linuxsoft.cern.ch/cern/slc52test/i386/images/</a>
       (install path: linuxsoft:/cern/slc52test/i386/)

   For 64-bit systems:

       <a href="http://linuxsoft.cern.ch/cern/slc52test/x86_64/images/">http://linuxsoft.cern.ch/cern/slc52test/x86_64/images/</a>
       (install path: linuxsoft:/cern/slc52test/x86_64/)

   For 64-bit Itanium 2 systems:

       <a href="http://linuxsoft.cern.ch/cern/slc52test/ia64/images/">http://linuxsoft.cern.ch/cern/slc52test/ia64/images/</a>
       (install path: linuxsoft:/cern/slc52test/ia64/)


   To install please follow the procedure at:

       <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>


* More information:

SLC5 web pages:

  <a href="http://cern.ch/linux/scientific5/">http://cern.ch/linux/scientific5/</a>

SLC5 installation:

  <a href="http://cern.ch/linux/scientific5/docs/install">http://cern.ch/linux/scientific5/docs/install</a>

SLC5 recent updates:

  <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>

* Problem reporting:

 Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>



Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="SLC47"></a><h3>01.09.2008: Scientific Linux CERN 4.7 becomes default production SLC4 release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 4 (SLC4) version:

          ***********************************************
          SLC 4.7 becomes default SLC4 production version
          ***********************************************

                as of Monday 01.09.2008

* What is Scientific Linux CERN 4.7 (SLC47)  ?

  - A minor release of SLC4X series with all the
    updates since release of 4.6 (and up to 13.08.2008)
    integrated in the base installation tree.

* Upgrading from previous SLC4X releases:

  - fully updated SLC 4X systems do not need any upgrade to
    4.7 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific4/docs/quickupdate">http://cern.ch/linux/scientific4/docs/quickupdate</a>

* Installation media images for SLC47 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc47/i386/images/SL/">http://linuxsoft.cern.ch/cern/slc47/i386/images/SL/</a>
        (install path: linuxsoft:/cern/slc47/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc47/x86_64/images/">http://linuxsoft.cern.ch/cern/slc47/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc47/x86_64/)

    For 64 bit Itanium 2 systems:

        <a href="http://linuxsoft.cern.ch/cern/slc47/ia64/images/">http://linuxsoft.cern.ch/cern/slc47/ia64/images/</a>
        (install path: linuxsoft:/cern/slc47/ia64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>


* ISO CD/DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc47/iso/">http://linuxsoft.cern.ch/cern/slc47/iso/</a>

* Certification of the SLC 4.7:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="SLC47test"></a><h3>19.08.2008: Scientific Linux CERN 4.7 test release available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN (SLC4) version:

           *********************************
            SLC 4.7 is available for tests.
           *********************************

               as of Tuesday 19.08.2008

* What is Scientific Linux CERN 4.7 (SLC47)  ?

   - A minor release of SLC4X series with all the
     updates since release of 4.6 (and up to 13.08.2008)
     integrated in the base installation tree.

* Upgrading from previous SLC4X releases:

   - fully updated SLC 4X systems do not need any upgrade to
     4.7 version, otherwise standard system upgrade procedure
     will bring systems up to date:
     http://cern.ch/linux/scientific4/docs/quickupdate

* Installation media images for SLC47 can be found at:

     For 32 bit systems:

         <a href="http://linuxsoft.cern.ch/cern/slc47/i386/images/SL/">http://linuxsoft.cern.ch/cern/slc47/i386/images/SL/</a>
         (install path: linuxsoft:/cern/slc47/i386/)

     For 64 bit systems:

         <a href="http://linuxsoft.cern.ch/cern/slc47/x86_64/images/SL/">http://linuxsoft.cern.ch/cern/slc47/x86_64/images/SL/</a>
         (install path: linuxsoft:/cern/slc47/x86_64/)

     For Itanium 2 systems:

         <a href="http://linuxsoft.cern.ch/cern/slc47/ia64/images/SL/">http://linuxsoft.cern.ch/cern/slc47/ia64/images/SL/</a>
         (install path: linuxsoft:/cern/slc47/ia64/)


    After downloading media images from above location
    standard installation procedure can be followed:

         <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>

* SLC 47 version will become the default SLC4 production
   version as of:

                    Monday 01.09.2008
                  **********************

  providing that no show-stopper problems are found.

* ISO CD/DVD images will be provided on 01.09.2008 at:

        <a href="http://linuxsoft.cern.ch/cern/slc47/iso/">http://linuxsoft.cern.ch/cern/slc47/iso/</a>

* Certification of the SLC 47:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="SLC46"></a><h3>10.01.2008: Scientific Linux CERN 4.6 becomes default production SLC4 release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 4 (SLC4) version:

          ***********************************************
          SLC 4.6 becomes default SLC4 production version
          ***********************************************

                as of Thursday 10.01.2008

* What is Scientific Linux CERN 4.6 (SLC46)  ?

  - A minor release of SLC4X series with all the
    updates since release of 4.6 (and up to 15.12.2007)
    integrated in the base installation tree.

* Upgrading from previous SLC4X releases:

  - fully updated SLC 4X systems do not need any upgrade to
    4.6 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific4/docs/quickupdate">http://cern.ch/linux/scientific4/docs/quickupdate</a>

* Installation media images for SLC46 can be found at:

    For Pentium/Athlon systems:

        <a href="http://linuxsoft.cern.ch/cern/slc46/i386/images/SL/">http://linuxsoft.cern.ch/cern/slc46/i386/images/SL/</a>
        (install path: linuxsoft:/cern/slc46/i386/)

    For Itanium 2 systems:

        <a href="http://linuxsoft.cern.ch/cern/slc46/ia64/images/">http://linuxsoft.cern.ch/cern/slc46/ia64/images/</a>
        (install path: linuxsoft:/cern/slc46/ia64/)

    For Opteron/Athlon 64/Intel EM64T systems:

        <a href="http://linuxsoft.cern.ch/cern/slc46/x86_64/images/">http://linuxsoft.cern.ch/cern/slc46/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc46/x86_64/)

    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>


* ISO CD/DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc46/iso/">http://linuxsoft.cern.ch/cern/slc46/iso/</a>

* Certification of the SLC 4.6:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="SLC46test"></a>
<h3>06.12.2007: Scientific Linux CERN 4.6 test release available.</h3>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN (SLC4) version:

           *********************************
            SLC 4.6 is available for tests.
           *********************************

               as of Thursday 6.12.2007

* What is Scientific Linux CERN 4.6 (SLC46)  ?

   - A minor release of SLC4X series with all the
     updates since release of 4.5 (and up to 1.12.2007)
     integrated in the base installation tree.

* Upgrading from previous SLC4X releases:

   - fully updated SLC 4X systems do not need any upgrade to
     4.6 version, otherwise standard system upgrade procedure
     will bring systems up to date:
     http://cern.ch/linux/scientific4/docs/quickupdate

* Installation media images for SLC46 can be found at:

     For Pentium/Athlon systems:

         <a href="http://linuxsoft.cern.ch/cern/slc46/i386/images/SL/">http://linuxsoft.cern.ch/cern/slc46/i386/images/SL/</a>
         (install path: linuxsoft:/cern/slc46/i386/)

     For Itanium 2 systems:

         <a href="http://linuxsoft.cern.ch/cern/slc46/ia64/images/SL/">http://linuxsoft.cern.ch/cern/slc46/ia64/images/SL/</a>
         (install path: linuxsoft:/cern/slc46/ia64/)

     For Opteron/Athlon 64/Xeon EM64T systems:

         <a href="http://linuxsoft.cern.ch/cern/slc46/x86_64/images/SL/">http://linuxsoft.cern.ch/cern/slc46/x86_64/images/SL/</a>
         (install path: linuxsoft:/cern/slc46/x86_64/)

    After downloading media images from above location
    standard installation procedure can be followed:

         <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>

* SLC 46 version will become the default SLC4 production
   version as of:

                   Monday 17.12.2007
                  **********************

  providing that no show-stopper problems are found.

* ISO CD/DVD images will be provided on 17.12.2007 at:

        <a href="http://linuxsoft.cern.ch/cern/slc46/iso/">http://linuxsoft.cern.ch/cern/slc46/iso/</a>

* Certification of the SLC 46:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="SLC3EOL"></a>
<h3>31.10.2007: Scientific Linux CERN 3 end-of-life.</h3>
<tr>
</td>
</tr>
<tr>
<td>
<pre>
Dear all,

as originally announced in February 2006 and stated several times
since then, we will stop all support for Scientific Linux CERN 3
(SLC3) tomorrow, 1st of November 2007.

This means no more updates will be made available as from then. CERN's
helpdesk and higher-level support lines will refuse SLC3 support
requests, and no further installations of SLC3 will be possible from
the CERN installation servers.

Registered admins for CERN SLC3 machines will have received several
mail warning to this effect over the last few months. Remaining CERN
SLC3 machines need to be reinstalled as soon as possible with SLC4, or
will need to be specially secured. Details on this and the migration
campaign can be found at <a
href="http://cern.ch/linux/slc4/docs/migration-campaign">http://cern.ch/linux/slc4/docs/migration-campaign</a>.

Jan Iven for CERN Linux Support
</pre>
</td>
</tr>

<tr>
<td><a name="SLC3EOLpre"></a>
<h3>18.07.2007: Scientific Linux CERN 3 end-of-life.</h3>
<tr>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users,

as discussed at the DTF and LXCERT, general support for our previous
CERN Linux distribution SLC3 will end October 31st, 2007.

This means no more security patches will be made available generally
from then, no more machines can be installed, and no support calls
will be taken by CERN's Helpdesk and Linux support.

Remaining SLC3 machines need to be reinstalled as soon as possible
with SLC4, or will need to be specially secured before
October. Details on this and the migration campaign itself can be
found at <a href="http://cern.ch/linux/slc4/docs/migration-campaign">http://cern.ch/linux/slc4/docs/migration-campaign</a>.

Thanks in advance,
Jan Iven for Linux Support


LXCERT minutes:
<a href="http://linux.web.cern.ch/linux/documentation/LXCERT/20060207/minutes">http://linux.web.cern.ch/linux/documentation/LXCERT/20060207/minutes</a>
relevant DTF minutes:
<a href="http://indico.cern.ch/getFile.py/access?resId=1&amp;materialId=1&amp;confId=1374">March 21st 2006</a>, and repeated several times since then.
</pre>
</td>
</tr>

<tr>
<td><a name="SLC45"></a>
<h3>30.05.2007: Scientific Linux CERN 4.5 becomes default production SLC4 release.</h3>
<tr>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 4 (SLC4) version:

          ***********************************************
          SLC 4.5 becomes default SLC4 production version
          ***********************************************

                as of Wednesday 30.05.2007

* What is Scientific Linux CERN 4.5 (SLC45)  ?

  - A minor release of SLC4X series with all the
    updates since release of 4.4 (and up to 15.05.2007)
    integrated in the base installation tree.

* New feature: Xen paravirtualized guest system

   - As of this update SLC4 i386/x86_64 versions
     can be installed as a paravirtualized Xen hypervisor
     guest systems: For more information see:
       <a href="https://twiki.cern.ch/twiki/bin/view/LinuxSupport/XenHowTo">https://twiki.cern.ch/twiki/bin/view/LinuxSupport/XenHowTo</a>

* Upgrading from previous SLC4X releases:

  - fully updated SLC 4X systems do not need any upgrade to
    4.5 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific4/docs/quickupdate">http://cern.ch/linux/scientific4/docs/quickupdate</a>

* Installation media images for SLC45 can be found at:

    For Pentium/Athlon systems:

        <a href="http://linuxsoft.cern.ch/cern/slc45/i386/images/SL/">http://linuxsoft.cern.ch/cern/slc45/i386/images/SL/</a>
        (install path: linuxsoft:/cern/slc45/i386/)

    For Itanium 2 systems:

        <a href="http://linuxsoft.cern.ch/cern/slc45/ia64/images/">http://linuxsoft.cern.ch/cern/slc45/ia64/images/</a>
        (install path: linuxsoft:/cern/slc45/ia64/)

    For Opteron/Athlon 64/Intel EM64T systems:

        <a href="http://linuxsoft.cern.ch/cern/slc45/x86_64/images/">http://linuxsoft.cern.ch/cern/slc45/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc45/x86_64/)

    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>


* ISO CD/DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc45/iso/">http://linuxsoft.cern.ch/cern/slc45/iso/</a>

* Certification of the SLC 4.5:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="XenHowtoupd"></a>
<h3>23.05.2007: Updated Xen with SLC3/SLC4 HOWTO available.</h3>
</td>
</tr>
<tr>
<td>
<pre>
Dear Xen && Linux users.

We would like to announce the availability of updated
version of Xen with SLC3/SLC4 HOWTO:

<a href="https://twiki.cern.ch/twiki/bin/view/LinuxSupport/XenHowTo">https://twiki.cern.ch/twiki/bin/view/LinuxSupport/XenHowTo</a>

******************************************************
If you have already installed a Xen paravirtualized
SLC4.4 / i386 system, following our former guidelines,
please note that a manual update procedure of such
guests systems is required in order to allow their
proper functioning after applying today's (23.05.2007)
SLC4 update.

For information please see:
<a href="https://twiki.cern.ch/twiki/bin/view/LinuxSupport/UpdatingFromSLC44toSLC45">https://twiki.cern.ch/twiki/bin/view/LinuxSupport/UpdatingFromSLC44toSLC45</a>
******************************************************


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="SLC45test"></a>
<h3>23.05.2007: Scientific Linux CERN 4.5 test release available.</h3>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN (SLC4) version:

           *********************************
            SLC 4.5 is available for tests.
           *********************************

               as of Wednesday 23.05.2007

* What is Scientific Linux CERN 4.5 (SLC45)  ?

   - A minor release of SLC4X series with all the
     updates since release of 4.4 (and up to 16.05.2007)
     integrated in the base installation tree.

* New feature: Xen paravirtualized guest system

   - As of this update SLC4 i386/x86_64 versions
     can be installed as a paravirtualized Xen hypervisor
     guest systems: For more information see:
       <a href="https://twiki.cern.ch/twiki/bin/view/LinuxSupport/XenHowTo">https://twiki.cern.ch/twiki/bin/view/LinuxSupport/XenHowTo</a>

* Upgrading from previous SLC4X releases:

   - fully updated SLC 4X systems do not need any upgrade to
     4.5 version, otherwise standard system upgrade procedure
     will bring systems up to date:
     http://cern.ch/linux/scientific4/docs/quickupdate

* Installation media images for SLC45 can be found at:

     For Pentium/Athlon systems:

         <a href="http://linuxsoft.cern.ch/cern/slc45/i386/images/SL/">http://linuxsoft.cern.ch/cern/slc45/i386/images/SL/</a>
         (install path: linuxsoft:/cern/slc45/i386/)

     For Itanium 2 systems:

         <a href="http://linuxsoft.cern.ch/cern/slc45/ia64/images/SL/">http://linuxsoft.cern.ch/cern/slc45/ia64/images/SL/</a>
         (install path: linuxsoft:/cern/slc45/ia64/)

     For Opteron/Athlon 64/Xeon EM64T systems:

         <a href="http://linuxsoft.cern.ch/cern/slc45/x86_64/images/SL/">http://linuxsoft.cern.ch/cern/slc45/x86_64/images/SL/</a>
         (install path: linuxsoft:/cern/slc45/x86_64/)

    After downloading media images from above location
    standard installation procedure can be followed:

         <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>

* SLC 45 version will become the default SLC4 production
   version as of:

                   Wednesday 30.05.2007
                  **********************

  providing that no show-stopper problems are found.

* ISO CD/DVD images will be provided on 30.05.2007 at:

        <a href="http://linuxsoft.cern.ch/cern/slc44/iso/">http://linuxsoft.cern.ch/cern/slc44/iso/</a>

* Certification of the SLC 45:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="eollxcert34"></a>
<h3>23.11.2006: Closure of public access to CERN Linux certification systems (4.12.2006).</h3>
<tr>
</td>
<tr>
<td>
<pre>
Dear Linux certification participants.

Since Scientific Linux CERN SLC4 certification
has been completed more than 6 months ago:

          On Monday, 4th of December 2006
          *******************************

we will close public access to following certification
systems:

      lxcert-i386, lxcert-ia64, lxcert-amd64

and also:

      refslc3-i386, refslc3-ia64, refslc3-amd64


Please switch to using public service alternatives
instead of above mentioned systems:

    For SLC3/i386:  lxplus.cern.ch
    For SLC4/i386:  lx32slc4.cern.ch
    For SLC4/amd64: lx64slc4.cern.ch

We do not offer any alternative public service for
following platforms:

    SLC3/amd64, SLC3/ia64 -  Support of SLC3 on these
                             architectures stops at the
                             end of this year.

    SLC4/ia64             -  No public service for
                             SLC4 on ia64 is foreseen,

Please note that lxcert systems will become available
again in the future during certification of next
Linux versions.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>.
</pre>
</td>
</tr>
<tr>
<td><a name="PXEinstall"></a>
<h3>03.11.2006: Media-less Linux installation method available.</h3>
<tr>
</td>
<tr>
<td>
<pre>
Dear CERN Linux users.

 We have a pleasure to announce that a new, media-less
method of Scientific Linux CERN installation is available
on the CERN network as of now.

 Instead of burning bootable CD-ROMs, preparing floppies or
USB Memory keys to install your Linux workstation it is possible
now to install directly from the network using PXE
(<a href="http://en.wikipedia.org/wiki/Preboot_Execution_Environment">Preboot Execution Environment</a>) with just few key strokes.

 The new installation method is available on all fixed and
portable sockets on the CERN campus network and requires
a PC computer capable of network boot (this includes all
desktop/laptop PC models sold by CERN PC-shop / Stores
since 2001/2002 as well as some older PCs).

To learn how to use this installation method, please
visit:

     <a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>.
</pre>
</td>
</tr>
<tr>
<td><a name="SLC44"></a>
<h3>25.09.2006: Scientific Linux CERN 4.4 becomes default production SLC4 release.</h3>
<tr>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 4 (SLC4) version:

          ***********************************************
          SLC 4.4 becomes default SLC4 production version
          ***********************************************

                as of Monday 25.09.2006

* What is Scientific Linux CERN 4.4 (SLC44)  ?

  - A minor release of SLC4X series with all the
    updates since release of 4.3 (and up to 12.09.2006)
    integrated in the base installation tree.

* Upgrading from previous SLC4X releases:

  - fully updated SLC 4X systems do not need any upgrade to
    4.4 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific4/docs/quickupdate">http://cern.ch/linux/scientific4/docs/quickupdate</a>

* Installation media images for SLC44 can be found at:

    For Pentium/Athlon systems:

        <a href="http://linuxsoft.cern.ch/cern/slc44/i386/images/SL/">http://linuxsoft.cern.ch/cern/slc44/i386/images/SL/</a>
        (install path: linuxsoft:/cern/slc44/i386/)

    For Itanium 2 systems:

        <a href="http://linuxsoft.cern.ch/cern/slc44/ia64/images/">http://linuxsoft.cern.ch/cern/slc44/ia64/images/</a>
        (install path: linuxsoft:/cern/slc44/ia64/)

    For Opteron/Athlon 64/Intel EM64T systems:

        <a href="http://linuxsoft.cern.ch/cern/slc44/x86_64/images/">http://linuxsoft.cern.ch/cern/slc44/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc44/x86_64/)

    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>


* ISO CD/DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc44/iso/">http://linuxsoft.cern.ch/cern/slc44/iso/</a>

* Certification of the SLC 44:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="SLC308"></a>
<h3>25.09.2006: Scientific Linux CERN 3.0.8 becomes default production SLC3 release.</h3>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 3 (SLC3) version:

          *************************************************
          SLC 3.0.8 becomes default SLC3 production version
          *************************************************

                as of Monday 25.09.2006

* What is Scientific Linux CERN 3.0.8 (SLC308)  ?

  - A minor release of SLC30X series with all the
    updates since release of 3.0.6 (and up to 07.08.2006)
    integrated in the base installation tree.

* Upgrading from previous SLC30X releases:

  - fully updated SLC 30X systems do not need any upgrade to
    3.0.8 version, otherwise standard system upgrade procedure
    will bring systems up to date:
       <a href="http://cern.ch/linux/scientific3/docs/quickupdate">http://cern.ch/linux/scientific3/docs/quickupdate</a>

* Installation media images for SLC308 can be found at:

    For Pentium/Athlon systems:

        <a href="http://linuxsoft.cern.ch/cern/slc308/i386/images/">http://linuxsoft.cern.ch/cern/slc308/i386/images/</a>
        (install path: linuxsoft:/cern/slc308/i386/)

    For Itanium 2 systems:

        <a href="http://linuxsoft.cern.ch/cern/slc308/ia64/images/">http://linuxsoft.cern.ch/cern/slc308/ia64/images/</a>
        (install path: linuxsoft:/cern/slc308/ia64/)

    For Opteron/Athlon 64/Intel EM64T systems:

        <a href="http://linuxsoft.cern.ch/cern/slc308/x86_64/images/">http://linuxsoft.cern.ch/cern/slc308/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc308/x86_64/)

    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific3/docs/install">http://cern.ch/linux/scientific3/docs/install</a>


* ISO CD/DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc308/iso/">http://linuxsoft.cern.ch/cern/slc308/iso/</a>

* Certification of the SLC 308:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="SLC44test"></a>
<h3>18.09.2005: Scientific Linux CERN 4.4 test release available.</h3>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN (SLC4) version:

           *********************************
            SLC 4.4 is available for tests.
           *********************************

                as of Monday 18.09.2006

* What is Scientific Linux CERN 4.4 (SLC44)  ?

   - A minor release of SLC4X series with all the
     updates since release of 4.3 (and up to 10.09.2006)
     integrated in the base installation tree.

* Upgrading from previous SLC4X releases:

   - fully updated SLC 4X systems do not need any upgrade to
     4.4 version, otherwise standard system upgrade procedure
     will bring systems up to date:
     http://cern.ch/linux/scientific4/docs/quickupdate

* Installation media images for SLC44 can be found at:

     For Pentium/Athlon systems:

         <a href="http://linuxsoft.cern.ch/cern/slc44/i386/images/SL/">http://linuxsoft.cern.ch/cern/slc44/i386/images/SL/</a>
         (install path: linuxsoft:/cern/slc44/i386/)

     For Itanium 2 systems:

         <a href="http://linuxsoft.cern.ch/cern/slc44/ia64/images/SL/">http://linuxsoft.cern.ch/cern/slc44/ia64/images/SL/</a>
         (install path: linuxsoft:/cern/slc44/ia64/)

     For Opteron/Athlon 64/Xeon EM64T systems:

         <a href="http://linuxsoft.cern.ch/cern/slc44/x86_64/images/SL/">http://linuxsoft.cern.ch/cern/slc44/x86_64/images/SL/</a>
         (install path: linuxsoft:/cern/slc44/x86_64/)

    After downloading media images from above location
    standard installation procedure can be followed:

         <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>

* SLC 44 version will become the default SLC4 production
   version as of:

                   Monday 25.09.2006
                  *******************

  providing that no show-stopper problems are found.

* ISO CD/DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc44/iso/">http://linuxsoft.cern.ch/cern/slc44/iso/</a>

* Certification of the SLC 44:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="SLC308test"></a>
<h3>11.09.2005: Scientific Linux CERN 3.0.8 test release available.</h3>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN (SLC3) version:

           *********************************
           SLC 3.0.8 is available for tests.
           *********************************

                as of Monday 11.09.2006

* What is Scientific Linux CERN 3.0.8 (SLC308)  ?

   - A minor release of SLC30X series with all the
     updates since release of 3.0.6 (and up to 10.08.2006)
     integrated in the base installation tree.

* Upgrading from previous SLC30X releases:

   - fully updated SLC 30X systems do not need any upgrade to
     3.0.8 version, otherwise standard system upgrade procedure
     will bring systems up to date:
     http://cern.ch/linux/scientific3/docs/quickupdate

* Installation media images for SLC308 can be found at:

     For Pentium/Athlon systems:

         <a href="http://linuxsoft.cern.ch/cern/slc308/i386/images/">http://linuxsoft.cern.ch/cern/slc308/i386/images/</a>
         (install path: linuxsoft:/cern/slc308/i386/)

     For Itanium 2 systems:

         <a href="http://linuxsoft.cern.ch/cern/slc308/ia64/images/">http://linuxsoft.cern.ch/cern/slc308/ia64/images/</a>
         (install path: linuxsoft:/cern/slc308/ia64/)

     For Opteron/Athlon 64/Xeon EM64T systems:

         <a href="http://linuxsoft.cern.ch/cern/slc308/x86_64/images/">http://linuxsoft.cern.ch/cern/slc308/x86_64/images/</a>
         (install path: linuxsoft:/cern/slc308/x86_64/)

    After downloading media images from above location
    standard installation procedure can be followed:

         <a href="http://cern.ch/linux/scientific3/docs/install">http://cern.ch/linux/scientific3/docs/install</a>

* SLC 308 version will become the default SLC3 production
   version as of:

                   Monday 18.09.2006
                  *******************

  providing that no show-stopper problems are found.

* ISO CD/DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc308/iso/">http://linuxsoft.cern.ch/cern/slc308/iso/</a>

* Certification of the SLC 308:

- as there are no other changes than these listed above
  a formal certification process does not need to be handled
  for this minor release.

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="slc4cert"></a><h3>31.03.2006: Scientific Linux CERN 4 certified</h3></td>
</tr>
<tr>
<td>
<pre>
Dear readers.

We would like to announce that:

            ******************************

            Scientific Linux CERN 4 (SLC4)

            ******************************

                  is now certified

	    ******************************

* This Linux version becomes as of 31st March 2006
  the default supported CERN Linux version.

* The previous Scientific Linux CERN 3 (SLC3) version
  is still supported at least until October 2007,
  but we encourage all users and administrators to
  migrate to SLC4 as soon as possible.

* What is Scientific Linux CERN 4 ?
  It is a CERN customized Linux distribution built on
  top of common base platform - Scientific Linux -
  which was in turn built from freely available Red Hat
  Enterprise Linux 4 sources by a joint Fermi and CERN
  effort.

* More information:

  <a href="http://cern.ch/linux/scientific4/">http://cern.ch/linux/scientific4/</a>

  Installation instructions:

  <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>

  Certification overview:

  <a href="http://cern.ch/linux/scientific4/cert/">http://cern.ch/linux/scientific4/cert/</a>

* SLC4 is available for network installs and soon
  on CD (which will be distributed through the
  IT bookshop, <a href="http://consult.cern.ch/books/">http://consult.cern.ch/books/</a>).

Our thanks go to all participants, especially to
the members of the Linux certification coordination
body.

  <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="SLC43"></a><h3>27.03.2006: Scientific Linux CERN 4.3 TEST becomes default SLC4X release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users,

We would like to announce that:

   **************************************
   Scientific Linux CERN 4.3 TEST (SLC43)
   **************************************

   becomes default test release of SLC4X
   as of Monday 27.03.2006.

   This is the last SLC43 TEST version before
   the certification is announced as complete.

* What is Scientific Linux CERN 4 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 4 sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 4.3 (SLC43)  ?

 - A minor release of SLC4X series with all the
   updates since release of 4.1 (and up to 25.03.2006)
   integrated in the base installation tree.

* Upgrading from previous SLC4X release:

 - fully updated SLC 4X systems do not need any upgrade to
   4.3 version, otherwise standard system upgrade procedure
   will bring systems up to date:
   http://cern.ch/linux/scientific4/docs/quickupdate

* Installation media images for SLC43 can be found at:

   For Pentium/Athlon systems:

       <a href="http://linuxsoft.cern.ch/cern/slc43/i386/images/SL/">http://linuxsoft.cern.ch/cern/slc43/i386/images/SL/</a>
       (install path: linuxsoft:/cern/slc43/i386/)

   For Itanium 2 systems:

       <a href="http://linuxsoft.cern.ch/cern/slc43/ia64/images/SL/">http://linuxsoft.cern.ch/cern/slc43/ia64/images/SL</a>
       (install path: linuxsoft:/cern/slc43/ia64/)

   For Opteron/Athlon 64/Xeon EM64T systems:

       <a href="http://linuxsoft.cern.ch/cern/slc43/x86_64/images/">http://linuxsoft.cern.ch/cern/slc43/x86_64/images/SL</a>
       (install path: linuxsoft:/cern/slc43/x86_64/)

   After downloading media images from above location
   standard installation procedure can be followed:

       <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>

* ISO CD/DVD will be available soon at:

       <a href="http://linuxsoft.cern.ch/cern/slc43/iso/">http://linuxsoft.cern.ch/cern/slc43/iso/</a>


* More information:

SLC4 web pages:

  <a href="http://cern.ch/linux/scientific4/">http://cern.ch/linux/scientific4/</a>

SLC4 installation:

  <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>
l</a>).

SLC4 recent updates:

  <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>

* Problem reporting:

 Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>



Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>
<tr>
<td><a name="linuxsoftupgrade"></a><h3>07.03.2006: Linuxsoft service upgrade.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Starting today - 7.3.2006 - we are migrating the
linuxsoft.cern.ch linux installation and updates service
to new hardware.

As a first step two new systems:

  lxnfs1.cern.ch

and

  lxnfs2.cern.ch

have been added to linuxsoft cluster.

Next, the old

  lxnfs4.cern.ch

and

  lxnfs5.cern.ch

will be retired, before end of the week.

The upgrade shall be mostly transparent, except if you
mount linuxsoft filesystems using NFS on your system:

In such case, if you get a stalled mount, when we retire
older linuxsoft nodes, you may need to issue the:

 mount -o remount ...

command in order to re-mount the filesystem from one of
new linuxsoft machines.

Please also check that in your setup you do not access
directly lxnfs1/lxnfs2/lxnfs4/lxnfs5 systems (by name or IP)
but rather use linuxsoft.cern.ch alias.

Please report problems to Linux.Support@cern.ch

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="EOL73"></a><h3>30.01.2006: CERN Linux 7.3 End-of-life announcement</h3></td>
</tr>
<tr>
<td>
<pre>
Dear all,

as originally announced in October 2004 and stated several times since
then, we have stopped all support for the 7.3 release of CERN Linux at
the end of 2005.
This means no more security patches have been made available since then.
We will now proceed to close down the infrastructure (installation
repositories, update scripts, notification scripts and reference
machines).

Remaining 7.3 machines need to be reinstalled as soon as possible with
one of  CERN's supported distributions (SLC3 or SLC4), or will need to
be specially secured. Details on this and the migration campaign can be
found at http://cern.ch/linux/slc3/docs/migration-campaign



In the name of the Linux Support team we would like to thank you for
choosing CERN Linux and hope you have a pleasant time on SLC or SLC4.

Jan
</td>
</tr>
<tr>
<td><a name="SLC42"></a><h3>29.11.2005: Scientific Linux CERN 4.2 TEST becomes default SLC4X release.</h3></td>
<tr>
<td>
<pre>
Dear CERN Linux users,

We would like to announce that:

   **************************************
   Scientific Linux CERN 4.2 TEST (SLC42)
   **************************************

   becomes default test release of SLC4X
   as of Tuesday 29.11.2005.

This is a TEST version NOT SUITABLE FOR PRODUCTION
usage at this time, in particular:

- most of CERN software has not been ported to this
  Linux version yet.

  Please note that following the decision of Linux
Certification Committee, it is not foreseen to deploy
this version widely on central CERN IT services
(minutes of LXCERT meeting are available at:
<a href="http://cern.ch/linux/documentation/LXCERT/20050421/minutes">http://cern.ch/linux/documentation/LXCERT/20050421/minutes</a>).


* What is Scientific Linux CERN 4 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 4 sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 4.2 (SLC42)  ?

 - A minor release of SLC4X series with all the
   updates since release of 4.1 (and up to 07.11.2005)
   integrated in the base installation tree.

* Upgrading from previous SLC4X release:

 - fully updated SLC 4X systems do not need any upgrade to
   4.2 version, otherwise standard system upgrade procedure
   will bring systems up to date:
   http://cern.ch/linux/scientific4/docs/quickupdate

* Installation media images for SLC42 can be found at:

   For Pentium/Athlon systems:

       <a href="http://linuxsoft.cern.ch/cern/slc42/i386/images/SL/">http://linuxsoft.cern.ch/cern/slc42/i386/images/SL/</a>
       (install path: linuxsoft:/cern/slc42/i386/)

   For Itanium 2 systems:

       <a href="http://linuxsoft.cern.ch/cern/slc42/ia64/images/SL/">http://linuxsoft.cern.ch/cern/slc42/ia64/images/SL</a>
       (install path: linuxsoft:/cern/slc42/ia64/)

   For Opteron/Athlon 64/Xeon EM64T systems:

       <a href="http://linuxsoft.cern.ch/cern/slc42/x86_64/images/">http://linuxsoft.cern.ch/cern/slc42/x86_64/images/SL</a>
       (install path: linuxsoft:/cern/slc42/x86_64/)

   After downloading media images from above location
   standard installation procedure can be followed:

       <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>

* ISO CD/DVD images are available at:

       <a href="http://linuxsoft.cern.ch/cern/slc42/iso/">http://linuxsoft.cern.ch/cern/slc42/iso/</a>


* More information:

SLC4 web pages:

  <a href="http://cern.ch/linux/scientific4/">http://cern.ch/linux/scientific4/</a>

SLC4 installation:

  <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>
l</a>).

SLC4 recent updates:

  <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>

* Problem reporting:

For now, please report problems on:

<a href="nntp://cern.linux">nntp://cern.linux</a>

newsgroup (public access).

or

<a href="mailto:linux-certification@cern.ch">linux-certification@cern.ch</a>

mailing list (subscription required).


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="SLC306"></a><h3>29.11.2005: Scientific Linux CERN 3.0.6 becomes default production SLC release.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN (SLC3) version:

~          ********************************************
~          SLC 3.0.6 becomes default production version
~          ********************************************

                as of Tuesday 29.11.2005

* What is Scientific Linux CERN 3.0.6 (SLC306)  ?

~  - A minor release of SLC30X series with all the
~    updates since release of 3.0.5 (and up to 07.11.2005)
~    integrated in the base installation tree.

* Upgrading from previous SLC30X releases:

~  - fully updated SLC 30X systems do not need any upgrade to
~    3.0.6 version, otherwise standard system upgrade procedure
~    will bring systems up to date:
~       http://cern.ch/linux/scientific3/docs/quickupdate

* Installation media images for SLC306 can be found at:

~    For Pentium/Athlon systems:

~        <a href="http://linuxsoft.cern.ch/cern/slc306/i386/images/">http://linuxsoft.cern.ch/cern/slc306/i386/images/</a>
~        (install path: linuxsoft:/cern/slc306/i386/)

~    For Itanium 2 systems:

~        <a href="http://linuxsoft.cern.ch/cern/slc306/ia64/images/">http://linuxsoft.cern.ch/cern/slc306/ia64/images/</a>
~        (install path: linuxsoft:/cern/slc306/ia64/)

~    For Opteron/Athlon 64/Xeon EM64T systems:

~        <a href="http://linuxsoft.cern.ch/cern/slc306/x86_64/images/">http://linuxsoft.cern.ch/cern/slc306/x86_64/images/</a>
~        (install path: linuxsoft:/cern/slc306/x86_64/)

    After downloading media images from above location
    standard installation procedure can be followed:

~        <a href="http://cern.ch/linux/scientific3/docs/install">http://cern.ch/linux/scientific3/docs/install</a>


* ISO CD/DVD images can be found at:

~        <a href="http://linuxsoft.cern.ch/cern/slc306/iso/">http://linuxsoft.cern.ch/cern/slc306/iso/</a>

* Certification of the SLC 306:

- as there are no other changes than these listed above
~  a formal certification process does not need to be handled
~  for this minor release.

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="SLC42test"></a><h3>17.11.2005: Scientific Linux CERN 4.2 TEST release available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users,

We would like to announce that:

   Scientific Linux CERN 4.2 TEST (SLC42)

is available for tests at CERN.

This is a TEST version NOT SUITABLE FOR PRODUCTION
usage at this time, in particular:

- most of CERN software has not been ported to this
  Linux version yet.

  Please note that following the decision of Linux
Certification Committee, it is not foreseen to deploy
this version widely on central CERN IT services
(minutes of LXCERT meeting are available at:
<a href="http://cern.ch/linux/documentation/LXCERT/20050421/minutes">http://cern.ch/linux/documentation/LXCERT/20050421/minutes</a>).


* What is Scientific Linux CERN 4 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 4 sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 4.2 (SLC42)  ?

 - A minor release of SLC4X series with all the
   updates since release of 4.1 (and up to 07.11.2005)
   integrated in the base installation tree.

* Upgrading from previous SLC4X release:

 - fully updated SLC 4X systems do not need any upgrade to
   4.2 version, otherwise standard system upgrade procedure
   will bring systems up to date:
   http://cern.ch/linux/scientific4/docs/quickupdate

* Installation media images for SLC42 can be found at:

   For Pentium/Athlon systems:

       <a href="http://linuxsoft.cern.ch/cern/slc42/i386/images/SL/">http://linuxsoft.cern.ch/cern/slc42/i386/images/SL/</a>
       (install path: linuxsoft:/cern/slc42/i386/)

   For Itanium 2 systems:

       <a href="http://linuxsoft.cern.ch/cern/slc42/ia64/images/SL/">http://linuxsoft.cern.ch/cern/slc42/ia64/images/SL</a>
       (install path: linuxsoft:/cern/slc42/ia64/)

   For Opteron/Athlon 64/Xeon EM64T systems:

       <a href="http://linuxsoft.cern.ch/cern/slc42/x86_64/images/">http://linuxsoft.cern.ch/cern/slc42/x86_64/images/</a>
       (install path: linuxsoft:/cern/slc42/x86_64/)

   After downloading media images from above location
   standard installation procedure can be followed:

       <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>

* SLC 42 version will become the default SLC4 version as of:

                  Thursday 24.11.2005
                  *******************

  providing that no show-stopper problems are found.

* ISO CD/DVD images will be provided next week at:

       <a href="http://linuxsoft.cern.ch/cern/slc42/iso/">http://linuxsoft.cern.ch/cern/slc42/iso/</a>


* More information:

SLC4 web pages:

  <a href="http://cern.ch/linux/scientific4/">http://cern.ch/linux/scientific4/</a>

SLC4 installation:

  <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>
l</a>).

SLC4 recent updates:

  <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>

* Problem reporting:

For now, please report problems on:

<a href="nntp://cern.linux">nntp://cern.linux</a>

newsgroup (public access).

or

<a href="mailto:linux-certification@cern.ch">linux-certification@cern.ch</a>

mailing list (subscription required).


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="SLC306test"></a><h3>17.11.2005: Scientific Linux CERN 3.0.6 test release available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN (SLC3) version:

~          *********************************
~          SLC 3.0.6 is available for tests.
~          *********************************

                as of Thursday 17.11.2005

* What is Scientific Linux CERN 3.0.6 (SLC306)  ?

~  - A minor release of SLC30X series with all the
~    updates since release of 3.0.5 (and up to 07.11.2005)
~    integrated in the base installation tree.

* Upgrading from previous SLC30X releases:

~  - fully updated SLC 30X systems do not need any upgrade to
~    3.0.6 version, otherwise standard system upgrade procedure
~    will bring systems up to date:
~       http://cern.ch/linux/scientific3/docs/quickupdate

* Installation media images for SLC306 can be found at:

~    For Pentium/Athlon systems:

~        <a href="http://linuxsoft.cern.ch/cern/slc306/i386/images/">http://linuxsoft.cern.ch/cern/slc306/i386/images/</a>
~        (install path: linuxsoft:/cern/slc306/i386/)

~    For Itanium 2 systems:

~        <a href="http://linuxsoft.cern.ch/cern/slc306/ia64/images/">http://linuxsoft.cern.ch/cern/slc306/ia64/images/</a>
~        (install path: linuxsoft:/cern/slc306/ia64/)

~    For Opteron/Athlon 64/Xeon EM64T systems:

~        <a href="http://linuxsoft.cern.ch/cern/slc306/x86_64/images/">http://linuxsoft.cern.ch/cern/slc306/x86_64/images/</a>
~        (install path: linuxsoft:/cern/slc306/x86_64/)

    After downloading media images from above location
    standard installation procedure can be followed:

~        <a href="http://cern.ch/linux/scientific3/docs/install">http://cern.ch/linux/scientific3/docs/install</a>

* SLC 306 version will become the default SLC3 production
~  version as of:

~                  Thursday 24.11.2005
~                  *******************

~  providing that no show-stopper problems are found.

* ISO CD/DVD images will be provided next week at:

~        <a href="http://linuxsoft.cern.ch/cern/slc305/iso/">http://linuxsoft.cern.ch/cern/slc305/iso/</a>

* Certification of the SLC 306:

- as there are no other changes than these listed above
~  a formal certification process does not need to be handled
~  for this minor release.

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="SLC41test"></a><h3>30.08.2005: Scientific Linux CERN 4.1 TEST release available.</h3></td>
</tr>
<tr>
<td>
<pre>
Dear CERN Linux users,

following the final "Scientific Linux 4.1" release
we would like to announce that:

   Scientific Linux CERN 4.1 TEST (SLC41)

is available for tests at CERN.

This is a TEST version NOT SUITABLE FOR PRODUCTION
usage at this time, in particular:

- there are known problems with AFS client stability
- there are known problems with SSH interoperability
- most of CERN software has not been ported to this
  Linux version yet.

  Please note that following the decision of Linux
Certification Committee, it is not foreseen to deploy
this version widely on central CERN IT services
(minutes of LXCERT meeting are available at:
<a href="http://cern.ch/linux/documentation/LXCERT/20050421/minutes">http://cern.ch/linux/documentation/LXCERT/20050421/minutes</a>).


* What is Scientific Linux CERN 4 ?

  It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 4 sources by a joint Fermi and CERN
effort.

* More information:

SLC4 web pages:

  <a href="http://cern.ch/linux/scientific4/">http://cern.ch/linux/scientific4/</a>

SLC4 installation:

  <a href="http://cern.ch/linux/scientific4/docs/install">http://cern.ch/linux/scientific4/docs/install</a>

SLC4 recent updates:

  <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>

* Problem reporting:

For now, please report problems on:

<a href="nntp://cern.linux">nntp://cern.linux</a>

newsgroup (public access).

or

<a href="mailto:linux-certification@cern.ch">linux-certification@cern.ch</a>

mailing list (subscription required).


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="2.4.20-31.7.2.cern"></a><h3>13.06.2005: [SECURITY]: CERN Linux 7.3.X -  Kernel upgrade</h3></td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

An update to the current (2.4.20-42.7.cern) kernel addresses several local
vulnerabilities. A complete list can be found here:

http://lwn.net/Articles/138761/

We ask all CERN Linux system administrators to upgrade as soon as possible to
a newer version (2.4.20-43.7.cern) that fixes these problems.  This update
will not be applied automatically for now, since it requires a reboot of the
machine.  Please follow the recipe given below.

The new kernel contains the usual CERN patches.

In order to upgrade your system please apply following procedure (as root):

*****************************************************

# cd /afs/cern.ch/project/linux/redhat/cern/7.3.X/updates/RPMS.updates/

Install new kernel:

# rpm -ivh kernel-2.4.20-43.7.cern.i686.rpm \
  kernel-smp-2.4.20-43.7.cern.i686.rpm \
  kernel-source-2.4.20-43.7.cern.i386.rpm

(change i686 to whatever suits your architecture: athlon or even i386, ignore
kernel-smp for uniprocessor machines -except Elonex PIV 2.8GHz, or similar
with hyperthreading enabled - ignore kernel-source code unless you do kernel
development)

Upgrade OpenAFS: (this version can be used with 2.4.20-42.7.cern
and 2.4.20-43.7.cern kernels):

# rpm -Fvh openafs*1.2.9-rh7.3.19.cern.i386.rpm

Edit your bootloader configuration: (/etc/grub.conf or /etc/lilo.conf):

change the  default=X  line to select the new kernel entry

(in this case, the entry version number 2.4.20-43.7.cern(smp)) (in grub.conf,
the first entry is the zero'th entry and thus default count starts at 0)

Reboot your system:

# /sbin/shutdown -r now

Andras Horvath
</pre>
</td>
</tr>
<tr>
<td><a name="slc305"></a><h3>05.06.2005: Scientific Linux CERN 3.0.5 (SLC305) maintenance release available.</h3></td>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Scientific Linux CERN 3.0.5 maintenance release
enters production today, replacing 3.0.4 release
as the default production version.


* What is Scientific Linux CERN 3.0.5 (SLC305)  ?

~  - A minor release of SLC30X series with all the
~    updates since release of 3.0.4 (and up to 30.05.2005)
~    integrated in the base installation tree.

* Upgrading from previous SLC30X releases:

~  - fully updated SLC 30X systems do not need any upgrade to
~    3.0.5 version, otherwise standard system upgrade procedure
~    will bring systems up to date:
~       http://cern.ch/linux/scientific3/docs/quickupdate

* Installation media images for SLC305 can be found at:

~    For Pentium/Athlon systems:

~        http://linuxsoft.cern.ch/cern/slc305/i386/images/
~        (install path: linuxsoft:/cern/slc305/i386/)

~    For Itanium 2 systems:

~        http://linuxsoft.cern.ch/cern/slc305/ia64/images/
~        (install path: linuxsoft:/cern/slc305/ia64/)

~    For Opteron/Athlon 64/Xeon EM64T systems:

~        http://linuxsoft.cern.ch/cern/slc305/x86_64/images/
~        (install path: linuxsoft:/cern/slc305/x86_64/)

~    After downloading media images from above location
~    standard installation procedure can be followed:

~        http://cern.ch/linux/scientific3/docs/install


* ISO CD/DVD images are available from:

~        http://linuxsoft.cern.ch/cern/slc305/iso/

* Certification of the SLC 305:

- as there are no other changes than these listed above
~  a formal certification process does not need to be handled
~  for this minor release.

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>

<tr>
<td><a name="slc305test">
<h3>02.06.2005: Scientific Linux CERN 3.0.5 test release available.</h3>
<tr>
</a>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN (SLC3) version:

~          *********************************
~          SLC 3.0.5 is available for tests.
~          *********************************

                as of Thursday 02.06.2005

* What is Scientific Linux CERN 3.0.5 (SLC305)  ?

~  - A minor release of SLC30X series with all the
~    updates since release of 3.0.4 (and up to 30.05.2005)
~    integrated in the base installation tree.

* Upgrading from previous SLC30X releases:

~  - fully updated SLC 30X systems do not need any upgrade to
~    3.0.5 version, otherwise standard system upgrade procedure
~    will bring systems up to date:
~       http://cern.ch/linux/scientific3/docs/quickupdate

* Installation media images for SLC305 can be found at:

~    For Pentium/Athlon systems:

~        <a href="http://linuxsoft.cern.ch/cern/slc305/i386/images/">http://linuxsoft.cern.ch/cern/slc305/i386/images/</a>
~        (install path: linuxsoft:/cern/slc305/i386/)

~    For Itanium 2 systems:

~        <a href="http://linuxsoft.cern.ch/cern/slc305/ia64/images/">http://linuxsoft.cern.ch/cern/slc305/ia64/images/</a>
~        (install path: linuxsoft:/cern/slc305/ia64/)

~    For Opteron/Athlon 64/Xeon EM64T systems:

~        <a href="http://linuxsoft.cern.ch/cern/slc305/x86_64/images/">http://linuxsoft.cern.ch/cern/slc305/x86_64/images/</a>
~        (install path: linuxsoft:/cern/slc305/x86_64/)

~    After downloading media images from above location
~    standard installation procedure can be followed:

~        <a href="http://cern.ch/linux/scientific3/docs/install">http://cern.ch/linux/scientific3/docs/install</a>

* SLC 305 version will become the default SLC3 production
~  version as of:

~                  Monday 06.06.2005
~                  *****************

~  providing that no show-stopper problems are found.

* ISO CD/DVD images are available at:

~        <a href="http://linuxsoft.cern.ch/cern/slc305/iso/">http://linuxsoft.cern.ch/cern/slc305/iso/</a>

* Certification of the SLC 305:

- as there are no other changes than these listed above
~  a formal certification process does not need to be handled
~  for this minor release.

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="2.4.20-31.7.2.cern">
<h3>21.03.2005: <font color="red">[SECURITY]</font>: CERN Linux 7.3.X -  Kernel and OpenAFS upgrade</h3>
<tr>
</a>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

An update to the current (2.4.20-31.7.2.cern) addresses several local
vulnerabilities. A complete list can be found here:

http://www.fedoralegacy.org/updates/RH7.3/
2005-02-24-FLSA_2005_2336__Updated_kernel_packages_fix_security_issues.html

We ask all CERN Linux system administrators to upgrade as soon as possible to
a newer version (2.4.20-42.7.cern) that fixes these problems.  This update
will not be applied automatically for now,since it requires a reboot of the
machine.  Please follow the recipe given below.

The new kernel contains the usual CERN patches.

In order to upgrade your system please apply following procedure (as root):

*****************************************************

# cd /afs/cern.ch/project/linux/redhat/cern/7.3.X/updates/RPMS.updates/

Install new kernel:

# rpm -ivh kernel-2.4.20-42.7.cern.i686.rpm \
kernel-smp-2.4.20-42.7.cern.i686.rpm \ kernel-source-2.4.20-42.7.cern.i386.rpm

(change i686 to whatever suits your architecture: athlon or even i386, ignore
kernel-smp for uniprocessor machines -except Elonex PIV 2.8GHz, or similar
with hyperthreading enabled - ignore kernel-source code unless you do kernel
development)

Upgrade OpenAFS: (this version can be used with 2.4.20-31.7.2.cern
and 2.4.20-42.7.cern kernels):

# rpm -Fvh i386/openafs*1.2.9-rh7.3.17.cern.i386.rpm

Edit your bootloader configuration: (/etc/grub.conf or /etc/lilo.conf):

change the  default=X  line to select the new kernel entry

(in this case, the entry version number 2.4.20-42.7.cern(smp)) (in grub.conf,
the first entry is the zero'th entry and thus default count starts at 0)

Reboot your system:

# /sbin/shutdown -r now

Andras Horvath
</pre>
</td>
</tr>
<tr>
<td><a name="slc304">
<h3>02.03.2005: Mozilla: Firefox, Thunderbird and Sunbird available for SLC3 (update: 11.03.2005)</h3>
</a>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux Users.

We have the pleasure to announce that:

 Mozilla Firefox web browser       version 1.0.1
 Mozilla Thunderbird e-mail client version 1.0
 Mozilla Sunbird calendar          version 0.2

are now available on Scientific Linux CERN 3 (SLC3).

To install on your SLC3 system run following command
as root:

#/usr/bin/apt-get update
#/usr/bin/apt-get install firefox thunderbird sunbird

After installation above applications will be
available from:

 KDE/GNOME - Menu -> Internet ->
 More Internet Applications

For more information about above products please visit
Mozilla site:

<a href="http://www.mozilla.org/products/firefox/">http://www.mozilla.org/products/firefox/</a>
<a href="http://www.mozilla.org/products/thunderbird/">http://www.mozilla.org/products/thunderbird/</a>
<a href="http://www.mozilla.org/projects/calendar/">http://www.mozilla.org/projects/calendar/</a>

(please note that sunbird is at early development
stage and 0.2 release we ship is a preview, not
production one).

If you have used Mozilla from your account, above
products will copy your existing Mozilla preferences
to Firefox, Thunderbird and Sunbird ones.

Please note however that this will require
additional disk space on your account, allocated
in ~/.mozilla/firefox , ~/.thunderbird and
 ~/.mozilla/sunbird directories.

<b><u>Update:</u></b>
(Thanks to N.de Metz-Noblat for pointing this out)
In order to open HTTP links properly from Thunderbird, manual action is
required to set the GNOME "preferred" browser (e.g. to Firefox). While running
GNOME, this can be changed by GUI via "Foot" &rarr; "Preferences" &rarr;
"Preferred Applications" &rarr; "Web Browser", select "Custom web browser"
and enter <tt>/usr/bin/firefox %s</tt>
Or use the command line to set this directly:
<pre>gconftool-2 -s --type=string /desktop/gnome/url-handlers/http/command "/usr/bin/firefox %s"
</pre>

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="73phaseout-1">
<h3>02.03.2005: CERN Linux 7.3, start of phase-out</h3>
</a>
</td>
</tr>
<tr>
<td>
<pre>

Dear CERN Linux community,

After the successful switch of central Linux services to SLC3 in
mid-January, we strongly recommend that all Linux machines are upgraded
to this version. Where possible, this should be done by April 2005, as
discussed in the Desktop Forum meeting of 28th Oct 2004. We plan to
contact individual machine administrators via automated mails in the
next days.

Upgrading from 7.3 is necessary due to the limited support lifetime
available for that release through the outsourced support contract
(progeny.com). Earlier versions are already considered a security threat
due to the lack of security updates.

In most cases, a full SLC3 reinstallation will be required (no update
possible), due to the age difference between these distributions
(multiple changes in the package sets, different configuration file
syntax etc). Installation documentation is available at
http://cern.ch/linux/scientific3/docs/install and helpdesk@cern.ch
can be contacted for assistance. We also provide support for installing
multiple machines with the same configuration.

For 7.3 machines that cannot be upgraded by April, their upgrade needs
to be scheduled before the end of 2005 (this is a hard end date). This
is necessary due to the unavailability of security updates after that
date. In the case that a 7.3 system cannot be upgraded within the above
timeframe, it will be considered a security threat due to the lack of
security updates. Additional protection such as a system firewall or
other means of access control will be need to be implemented until it
can be upgraded.

For machines running even older CERN (e.g. 6.1.1, 7.2.x) or unsupported
Linux vendor releases, a reinstallation with a supported version is
urgently required. These machines need to be treated as a security
threat since they have not received any updates (security or otherwise)
from IT since 1st of July 2003 - such machines may already have been
compromised given the long exposure and many break-in attempts already
detected on the CERN site (more than 300 computers were compromised
during 2004 compared to around 30 in the previous years).


Thanks in advance,
Jan Iven for Linux Support


relevant DTF minutes:
<a href="http://agenda.cern.ch/askArchive.php?base=agenda&categ=a044567&id=a044567/minutes">http://agenda.cern.ch/askArchive.php?base=agenda&categ=a044567&id=a044567/minutes</a>
<a href="http://agenda.cern.ch/askArchive.php?base=agenda&categ=a045119&id=a045119/minutes">http://agenda.cern.ch/askArchive.php?base=agenda&categ=a045119&id=a045119/minutes</a>
</pre>
</td>
</tr>

<tr>
<td><a name="slc304">
<h3>28.02.2005: SLC304: maintenance release available.</h3>
</a>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Scientific Linux CERN 3.0.4 maintenance release
enters production today, replacing 3.0.3 release
as the default production version.


* What is Scientific Linux CERN 3.0.4 (SLC304)  ?

~  - A minor release of SLC30X series with all the
~    updates since release of 3.0.3 (and up to 18.02.2005)
~    integrated in the base installation tree.

* What's new in SLC304 version:

~  - installation images have been updated to allow
~    installations on systems equipped with Serial ATA and
~    3ware 9XXX controllers.

~  - x86_64 installation tree has been updated to allow
~    installations on Xeon EM64T systems.


* Upgrading from previous SLC30X releases:

~  - fully updated SLC 30X systems do not need any upgrade to
~    3.0.4 version, otherwise standard system upgrade procedure
~    will bring systems up to date:
~       http://cern.ch/linux/scientific3/docs/quickupdate

* Installation media images for SLC304 can be found at:

~    For Pentium/Athlon systems:

~        http://linuxsoft.cern.ch/cern/slc304/i386/images/
~        (install path: linuxsoft:/cern/slc304/i386/)

~    For Itanium 2 systems:

~        http://linuxsoft.cern.ch/cern/slc304/ia64/images/
~        (install path: linuxsoft:/cern/slc304/ia64/)

~    For Opteron/Athlon 64/Xeon EM64T systems:

~        http://linuxsoft.cern.ch/cern/slc304/x86_64/images/
~        (install path: linuxsoft:/cern/slc304/x86_64/)

~    After downloading media images from above location
~    standard installation procedure can be followed:

~        http://cern.ch/linux/scientific3/docs/install


* ISO CD/DVD images will be made available this week.

* Certification of the SLC 304:

- as there are no other changes than these listed above
~  a formal certification process does not need to be handled
~  for this minor release.

Jaroslaw Polok for Linux.Support@cern.ch

</pre>
</td>
</tr>
<tr>
<td><a name="slc304test">
<h3>21.02.2005: Test version of updated SLC3 - SLC304 available.</h3>
</a>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN (SLC3) version:

~          *********************************
~          SLC 3.0.4 is available for tests.
~          *********************************

as of Monday 21.02.2005

* What is Scientific Linux CERN 3.0.4 (SLC304)  ?

~  - A minor release of SLC30X series with all the
~    updates since release of 3.0.3 (and up to 18.02.2005)
~    integrated in the base installation tree.

* What's new in SLC304 version:

~  - installation images have been updated to allow
~    installations on systems equipped with Serial ATA and
~    3ware 9XXX controllers.

~  - x86_64 installation tree has been updated to allow
~    installations on Xeon EM64T systems.


* Upgrading from previous SLC30X releases:

~  - fully updated SLC 30X systems do not need any upgrade to
~    3.0.4 version, otherwise standard system upgrade procedure
~    will bring systems up to date:
~       http://cern.ch/linux/scientific3/docs/quickupdate

* Installation media images for SLC304 can be found at:

~    For Pentium/Athlon systems:

~        http://linuxsoft.cern.ch/cern/slc304/i386/images/
~        (install path: linuxsoft:/cern/slc304/i386/)

~    For Itanium 2 systems:

~        http://linuxsoft.cern.ch/cern/slc304/ia64/images/
~        (install path: linuxsoft:/cern/slc304/ia64/)

~    For Opteron/Athlon 64/Xeon EM64T systems:

~        http://linuxsoft.cern.ch/cern/slc304/x86_64/images/
~        (install path: linuxsoft:/cern/slc304/x86_64/)

~    After downloading media images from above location
~    standard installation procedure can be followed:

~        http://cern.ch/linux/scientific3/docs/install

* SLC 304 version will become the default SLC3 production
~  version as of:

~                  Monday 28.02.2005
~                  *****************

~  providing that no show-stopper problems are found.

* ISO CD/DVD images will be made available after 28.02.2005.

* Certification of the SLC 304:

- as there are no other changes than these listed above
~  a formal certification process does not need to be handled
~  for this minor release.

Jaroslaw Polok for Linux.Support@cern.ch

</pre>
</td>
</tr>
<tr>
<td><a name="slc3cert">
<h3>01.11.2004: Scientific Linux CERN 3 certified</h3>
</a>
</td>
</tr>
<tr>
<td>
<pre>
Dear readers.

We would like to announce that:

            Scientific Linux CERN 3 (SLC3)

                  is now certified


* This Linux version becomes as of 1st November 2004
  the default supported CERN Linux version.
  The previous CERN Linux 7.3.X series is still
  supported until April 2005, but we encourage all
  users and administrators to migrate away from it
  as soon as possible.

* What is Scientific Linux CERN 3 ?
  It is a CERN customized Linux distribution built on
  top of common base platform - Scientific Linux -
  which was in turn built from freely available Red Hat
  Enterprise Linux 3 sources by a joint Fermi and CERN
  effort.

* More information:

  <a href="http://cern.ch/linux/scientific3/">http://cern.ch/linux/scientific3/</a>

  Installation instructions:

  <a href="http://cern.ch/linux/scientific3/docs/install">http://cern.ch/linux/scientific3/docs/install</a>

  Certification overview:

  <a href="http://cern.ch/linux/scientific3/cert/">http://cern.ch/linux/scientific3/cert/</a>

* SLC3 is available for network installs and soon
  on CD (which will be distributed through the
  IT bookshop, <a href="http://consult.cern.ch/books/">http://consult.cern.ch/books/</a>).

Our thanks go to all participants, especially to
the members of the Linux certification coordination
body.

  <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
</pre>
</td>
</tr>

<tr>
<td><a name="slc303rc">
<h3>19.10.2004: Scientific Linux CERN 3.0.3 Release Candidate available.</h3>
</a>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Following Scientific Linux 3.0.3 final release
and fixing all the certification problems which were
labelled as 'blocking' we would like to announce
that:

    Scientific Linux CERN 3.0.3 Release Candidate

is available for tests.

If no other blocking problems are found we would like
to announce this release as final and certified on:

           Monday 1st of November 2004

pending the agreement of Linux Certification Committee.

* What is Scientific Linux CERN 3.0.3 ?

It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 3 sources by a joint Fermi and CERN
effort.

* Updating to SLC 3.0.3 from previous release
  candidates (SLC 3.0.2):

  As root on your system run following commands:
  # apt-get update
  # apt-get dist-upgrade
  and (if you haven't applied recent kernel update yet):
  # apt-get upgrade-kernel

<font color="red">updated:</font>
If after executing above you do not receive updates please run:
# rpm -Uvh http://linuxsoft.cern.ch/cern/slc30X/i386/SL/RPMS/apt-sourceslist-303-6.cern.i386.rpm
and repeat above procedure.
(substitute i386 by ia64 or x86_64 depending on your architecture).

* More information:

SLC3 web pages:

 <a href="http://cern.ch/linux/scientific3/">http://cern.ch/linux/scientific3/</a>

Installation documentation:

 <a href="http://cern.ch/linux/scientific3/docs/install">http://cern.ch/linux/scientific3/docs/install</a>

(please note that installation path has been changed
 to /cern/slc303/ and that new boot images from above
 page are needed)

SLC3 recent updates:

 <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>


Jaroslaw Polok for Linux.Support@cern.ch

</pre>
</td>
</tr>
<tr>
<td><a name="rh73-external">
<h3>15.07.2004: external access restrictions for CERN Linux 7.3.X</h3>
</a>
</td>
</tr>
<tr>
<td>
<pre>
Dear external CERN Linux users,

this is to inform you that external access to the CERN Linux 7.3.X
repositories [1] and [2] will be restricted soon:

In order to better concentrate our own efforts on the upcoming SLC3
release, CERN has subscribed to the commercial RPM updates service
from Progeny [3], and the terms of the license agreement do not allow
us to redistribute such updates off-site.

External access to the repositories will be turned off as soon as we
have reviewed and integrated the pending updates, but no later than
the 1st of August. This will affects subsequent releases of 7.3.x as
well, since these releases will have all updates integrated. If you
are currently using the 7.3.4 tree from http://linuxsoft.cern.ch/cern,
we recommend that you create a complete local mirror.

Machines on the CERN network will not be affected by this change, nor
are the SLC3 tree(s) [4] affected by this.

Best regards
Jan Iven

[1] <a href="http://linuxsoft.cern.ch/cern/7.3.4">http://linuxsoft.cern.ch/cern/7.3.4</a>
[2] <a href="http://linuxsoft.cern.ch/cern/updates/7.3.x">http://linuxsoft.cern.ch/cern/updates/7.3.x</a>
[3] <a href="http://transition.progeny.com/">http://transition.progeny.com/</a>
[4] <a href="http://linuxsoft.cern.ch/cern/slc302">http://linuxsoft.cern.ch/cern/slc302</a>
</pre>
</td>
<tr valign="Top">
<td><a name="rh73-external-2">
<h3>16.07.2004: clarification on the external access to 7.3.x RPM updates</h3>
</a>
</td>
</tr>
<tr>
<td>
<pre>
 Some clarifications, sorry if my initial announcement was not clear
  enough and has led to confusion:

Since Red Hat stopped support for the 7.3 and 8 desktop releases at
the end of 2003, all sites still running these releases had to patch
security holes (and perhaps "ordinary" bugs) for themselves. Early
efforts at collaboration (like "Fedora Legacy") were sometimes
helpful, but not timely enough to completely rely on them for
production systems - responsibility stayed with the site.

CERN Linux support has provided such fixes for our own distribution
(7.3.4 at the moment), and has made them generally available together
with the full distribution since this incurred no additional cost to
us.  It is important to notice here that _not_ all known security
issues in 7.3 have been fixed at CERN, since we review vulnerabilities
against their impact on CERN services. Likewise the current delay for
deploying fixes is sometimes at the limit of acceptable risk, and the
announcement mechanism was not designed to reach external users.

--> Together these mean that no remote site admin should have felt
comfortable relying only on CERN for security updates, without
tracking security announcements or other distributions.  It should be
clear that the responsibility of the linux service (security etc.)
lies solely with the local system administration on each site.

We are not aware how many sites have installed the CERN Linux version
(since we neither track accesses nor force registration), have not
advertised general support (and indeed receive very few support calls
from outside of CERN), nor have we silently taken over the
responsibility for remote sites' computer security (I hope the general
availability of CERN's distribution in the past is not turned around
now to indicate just this).

I hope this explains why our current decision to buy into a commercial
updating service has not been announced earlier or wider (it was taken
on short notice) -- this is a service change _without_ visible effect
for CERN users or machines.

Some more points to put this into scope:

* The next Linux release (SLC3) that is being put together right now
with input from all CERN experiments is not affected (but CERN will
not take responsibility for remote site security there either even if
updates remain free).

* The currently expected lifetime for 7.3.x is another six months for
general usage, after which we hope to cut access for most users inside
the CERN site while we phase it out.

* Runtime compatibility between sites was apparently not affected by
the 30-odd updates to 7.3.x that have been rolled out (or not) in an
uncoordinated manner until now, so I would not expect distributed
productions to suffer.

* Updates done by CERN (we normally do our own Linux kernels and a few
other packages) can continue to be obtained (and redistributed)
completely legally, even if the mechanism will no longer be a simple
mirroring script.


If this decision and the short deadline now suddenly turn out to have
a wide impact on the HEP community, we are of course willing to work
with everybody on a solution which is more comfortable for all sides.

Best regards
Jan Iven for Linux.Support
</pre>
</tr>
<tr>
<td><a name="slc3-rc3">
<h3>12.07.2004: Scientific Linux CERN 3.0.2 rc3 available</h3>
</a>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Following Scientific Linux 3.0.2 final release
we would like to announce that third test version:

  Scientific Linux CERN 3.0.2 release candidate 3

is available for tests.
This test version replaces SL CERN 3.0.2 rc2 and
should be the final test version before the release.

* Changes between rc2 and rc3 version:

- updated kernel (2.4.21-15.0.3.cern)
- updated openafs (minor startup script fix)
- updated apt-autoupdate (minor fix)
- updated xprint-cups (wrapper scripts)
- updated authconfig
- updated pine
- added fixes for AFS tokens in GNOME / KDE
- updated hepix scripts
- changed location of the distribution (/cern/slc302/ now)

* How to update running system to rc3:

 Please check:

 <a href="http://cern.ch/linux/updates/updates-slc3#12.07.2004">http://cern.ch/linux/updates/updates-slc3#12.07.2004</a>

 (please note that this update will NOT be applied in an
  automated way)

* What is Scientific Linux CERN 3.0.2 ?

 It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 3 sources by a joint Fermi and CERN
effort.

* More information:

SLC3 web pages:

 <a href ="http://cern.ch/linux/scientific3/">http://cern.ch/linux/scientific3/</a>

Installation documentation:

 <a href="http://cern.ch/linux/scientific3/docs/install">http://cern.ch/linux/scientific3/docs/install</a>

 (please note that installation path has been changed
  to /cern/slc302/)

SLC3 recent updates:

 <a href="http://cern.ch/linux/updates/">http://cern.ch/linux/updates/</a>


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="slc3-rc2">
<h3>23.06.2004: Scientific Linux CERN 3.0.2 rc2 available</h3>
</a>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

Following recent Scientific Linux 3.0.2 rc2 release
we would like to announce that second test version:

  Scientific Linux CERN 3.0.2 release candidate 2


is available for tests.
This test release replaces SL CERN 3.0.2 rc1.

* Changes between rc1 and rc2 version:

- updated kernel (2.4.21-15.0.2.cern)
- updated openafs (minor startup script fix)
- few security updates (see link below)
- updated apt-autoupdate (minor fix)
- updated xprint-cups (wrapper scripts)
- added cern-config-printers package

* How to update from rc1 to rc2:

 Please check:

 http://cern.ch/linux/updates/updates-slc3#23.06.2004

* What is Scientific Linux CERN 3.0.2 ?

 It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 3 sources by a joint Fermi and CERN
effort.

* More information:

SLC3 web pages:

 http://cern.ch/linux/scientific3/

SLC3 recent updates:

 http://cern.ch/linux/updates/


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="2.4.20-31.7.2.cern">
<h3>17.06.2004: <font color="red">[SECURITY]</font>: CERN Linux 7.3.X -  Kernel and OpenAFS upgrade</h3>
<tr>
</a>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

An Denial of Service (DoS) security vulnerability
exists in current (2.4.20-30.7.cern) CERN Linux
7.3.X kernel (and ALL previous 2.4 kernel versions).

We ask all CERN Linux system administrators to
upgrade as soon as possible to a newer version
(2.4.20-31.7.2.cern) that fixes the problem.

This update will not be applied automatically
for now,since it requires a reboot of the machine.
Please follow the recipe given below.

The new kernel is very similar to the 2.4.20-30.7.cern,
exception being NPTL and O(1) scheduler are being
disabled in it. (As found to be a source of problems
in some multi-threaded applications).

In order to upgrade your system please apply following
procedure (as root):

*****************************************************

# cd /afs/cern.ch/project/linux/redhat/cern/7.3.4/updates/RPMS/

Install new kernel:

# rpm -ivh kernel-2.4.20-31.7.2.cern.i686.rpm \
           kernel-smp-2.4.20-31.7.2.cern.i686.rpm \
           kernel-source-2.4.20-31.7.2.cern.i386.rpm

  (change i686 to whatever suits your architecture:
   athlon or even i386, ignore kernel-smp for
   uniprocessor machines -except Elonex PIV 2.8GHz,
   or similar with hyperthreading enabled -
   ignore kernel-source code unless you do kernel
   development)

Upgrade OpenAFS:
(this version can be used with 2.4.20-30.7.cern
and 2.4.20-31.7.2.cern kernels)

# rpm -Fvh openafs*1.2.9-rh7.3.16.cern.i386.rpm

Edit your bootloader configuration:
(/etc/grub.conf or /etc/lilo.conf):

change the  default=X  line

      to select the new kernel entry
     (in this case,  the entry version number
      2.4.20-31.7.2.cern(smp))
     (in grub.conf, the first entry is the zero'th
      entry  and thus default count starts at 0)

Reboot your system:

# /sbin/shutdown -r now

*******************************************************

If you want to be notified by e-mail about future
CERN Linux updates please subscribe to:

       linux-announce@cern.ch

list at:
       http://wwwlistbox.cern.ch/

*******************************************************

Please report problems (if any...) to
Linux.Support@cern.ch


Jaroslaw Polok
</pre>
</td>
</tr>
<td><a name="slc3-rc1">
<h3>16.06.2004: CERN E. Linux 3 (CEL3) becomes Scientific Linux CERN 3 (SLC3)</h3>
</a>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

We are happy to announce that following the Linux
collaboration proposal presented at HEPiX meeting
the first version of CERN customized Scientific Linux:

        Scientific Linux CERN 3.0.2 release candidate 1

is available for tests.

* What is Scientific Linux CERN 3.0.2 ?

 It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux - which
was in turn built from freely available Red Hat
Enterprise Linux 3 sources by a joint Fermi and CERN
effort.

* What is the relationship between CERN E. Linux 3
  (CEL3) and Scientific Linux CERN 3.0.2 ?

 CEL3 has been a CERN-only test linux version,
 SCL3 can be seen as updated CEL3 version with much
broader usage scope.

* What are the user-visible differences between CEL3
  and SLC3 ?

 Mostly minor: few software packages has been rebuilt
to be more generic than before and more usable at other
Scientific Linux sites. Other than that, a number of
CERN-recompiled packages have been replaced with
Fermi-recompiled packages.  We expect that the
certification results for CEL3 are valid on SLC3

* More information:

 New web page for SLC3:

       http://cern.ch/linux/scientific3/

 More background information and the joint Fermi/CERN
 proposal:

 http://cern.ch/linux/scientific3/CEL3-vs-SL


Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<td><a name="7.3.4-release">
<b>2.03.2004: CERN Linux 7.3.4: maintenance release available<b>
</a>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux Users

CERN Linux 7.3.4 maintenance release enters production today.

This release contains all Red Hat errata released until end of
December 2003 and Fedora Legacy errata released until end of
January 2004 and should have minimal user-visible changes,
which is why no formal certification has been done.

Properly patched 7.3.x systems should be indistinguishable
from 7.3.4 so if you run one of these and SUE is maintaining
your system updates the only thing which needs to be done is the
kernel update (to version 2.4.20-30.7.cern) announced yesterday.

A complete list of changes is available in
<a href="redhat73/7.3.4/i386/README.updates">README.updates</a> and <a href="redhat73/7.3.4/i386/README.kernel">README.kernel</a>
in the distribution.

Installation floppy images are available at:

<i>/afs/cern.ch/project/linux/redhat/cern/7.3.4/i386/images</i>

(or directly from <a href="redhat73/documentation/installation/">installation instructions page</a>)

The installation path for the distribution is:

<i>(http/NFS/ftp) linuxsoft.cern.ch</i>: <i>/redhat/cern/7.3.4/i386</i>

Please note that 7.3.4 release replaces 7.3.3 as the
default CERN Linux version as of today.

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</td>
</tr>
<tr>
<td><a name="2.4.20-30.7.cern">
<b>1.03.2004: [SECURITY] CERN Linux 7.3.X: Kernel and OpenAFS update.</b>
</a>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

An possibly exploitable security vulnerability
exists in current (2.4.20-28.7.cern(.2)) CERN Linux
7.3.X kernel (and ALL previous kernel versions).

We ask all CERN Linux system administrators to
upgrade as soon as possible to a newer version
(2.4.20-30.7.cern) that fixes the problem.
This update will not be applied automatically
for now,since it requires a reboot of the machine.
Please follow the recipe given below.

The new kernel contains additional functionality,
(comparing to Red Hat 2.4.20-30.7):

    Enabled O(1) scheduler
    3w-xxxx driver update to version 1.02.00.036
    NTFS driver update to version 2.1.4c(read-only)
    qla2x00 driver update to version 6.06.10
    e100 driver update to version 2.3.13-k1-1
    e1000 driver update to version 5.1.11-k1

plus the 'usual' set of CERN applied patches:

    SGI xfs filesystem version 1.3.1
    SGI kdb kernel debugger version 3
    Big Physical Area patch
    (Re)added bcm5700 driver
    CIFS filesystem cvs (~0.8.7)
    LUFS filesystem 0.9.7
    SCSI tape (st) patch
    LKCD 4.1.1 patch

In order to upgrade your system please apply following
procedure (as root):

*****************************************************
# cd /afs/cern.ch/project/linux/redhat/cern/7.3.3/updates/RPMS.updates/

Install new kernel:

 # rpm -ivh kernel-2.4.20-30.7.cern.i686.rpm \
        kernel-smp-2.4.20-30.7.cern.i686.rpm \
        kernel-source-2.4.20-30.7.cern.i386.rpm

    (change i686 to whatever suits your architecture:
     athlon or even i386, ignore kernel-smp for
     uniprocessor machines -except Elonex PIV 2.8GHz,
     or similar with hyperthreading enabled -
     ignore kernel-source code unless you do kernel
     development)

Upgrade OpenAFS:
(this version can be used with 2.4.20-28.7.cern
2.4.20-28.7.cern.2 and 2.4.20-30.7.cern kernels)

 # rpm -Fvh openafs*1.2.9-rh7.3.11.cern.i386.rpm

Upgrade your vixie-cron package (If you haven't done
so on last kernel upgrade):
(In order to avoid filling log files with bug
notification messages)

 # rpm -Fvh vixie-cron-3.0.1-76.cern.i386.rpm

Edit your bootloader configuration:
(/etc/grub.conf or /etc/lilo.conf):

 change the  default=X  line to select the new
 kernel entry

 (in this case,  the entry version number
  2.4.20-30.7.cern(smp))
 (in grub.conf, the first entry is the zero'th entry
  and thus default count starts at 0)

Reboot your system:  #

/sbin/shutdown -r now

*******************************************************

If you want to be notified by e-mail about future
CERN Linux updates please subscribe to:

linux-announce@cern.ch

list at:

http://wwwlistbox.cern.ch/

*******************************************************

Please report problems (if any...) to
Linux.Support@cern.ch

Jaroslaw Polok
</pre>
</td>
</tr>
<tr>
<td><a name="2.4.20-28.7.cern">
<h3>19.01.2004: [SECURITY] CERN Linux 7.3.X: Kernel, OpenAFS and cron update.</h3>
</a>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

An possibly exploitable security vulnerability
exists in current (2.4.20-24.7.cern) CERN Linux
7.3.X kernel
(and ALL previous kernel versions).

We ask all CERN Linux system administrators to
upgrade as soon as possible to a newer version
(2.4.20-28.7.cern) that fixes the problem.

This update will not be applied automatically for now,
since it requires a reboot of the machine. Please follow
the recipe given below.

The new kernel contains additional functionality,
(comparing to Red Hat 2.4.20-28.7):

     Enabled O(1) scheduler
     3w-xxxx driver update to version 1.02.00.036
     NTFS driver update to version 2.1.4c(read-only)
     qla2x00 driver update to version 6.06.10
     e100 driver update to version 2.3.13-k1-1
     e1000 driver update to version 5.1.11-k1

plus the 'usual' set of CERN applied patches:

     SGI xfs filesystem version 1.3.1
     SGI kdb kernel debugger version 3
     Big Physical Area patch
     (Re)added bcm5700 driver
     CIFS filesystem cvs (~0.8.7)
     LUFS filesystem 0.9.7
     SCSI tape (st) patch

Please note that LKCD patch has been removed due to
the conflict with scheduler patch. We plan to re-add
it in future kernel updates.

In order to upgrade your system please apply following
procedure (as root):

*****************************************************

# cd /afs/cern.ch/project/linux/redhat/cern/7.3.3/updates/

Install new kernel:

 # rpm -ivh i686/kernel-2.4.20-28.7.cern.i686.rpm \
         i686/kernel-smp-2.4.20-28.7.cern.i686.rpm \
         i386/kernel-source-2.4.20-28.7.cern.i386.rpm

 (change i686 to whatever suits your architecture:
 i586,athlon or even i386, ignore SMP kernel for
 uniprocessor machines, ignore source code unless you
 do kernel development)


Upgrade OpenAFS:

 (this version can be used with both 2.4.20-24.7.cern
 and 2.4.20-28.7.cern kernels)

 # rpm -Fvh i386/openafs*1.2.9-rh7.3.9.cern.i386.rpm


Upgrade your vixie-cron package:

 (In order to avoid filling log files with
 bug notification messages)

 # rpm -Fvh i386/vixie-cron-3.0.1-76.cern.i386.rpm


Edit your bootloader configuration:

 (/etc/grub.conf or /etc/lilo.conf): change the

 default=X

 line to select the new kernel entry (in this case,
 the entry version number 2.4.20-28.7.cern(smp))
 (in grub.conf, the first entry is the zero'th entry
 and thus default count starts at 0)


Reboot your system:

 # /sbin/shutdown -r now

*******************************************************

If you want to be notified by e-mail about future CERN
Linux updates please subscribe to: linux-announce@cern.ch
list at: http://wwwlistbox.cern.ch/

*******************************************************

Please report problems (if any...) to

               Linux.Support@cern.ch

Jaroslaw Polok
</pre>
</td>
</tr>
<tr>
<td><a name="apache-1.3.27-4">
<h3>13.01.2004: [SECURITY] CERN Linux 7.3.X: Apache and lftp update</h3>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux / SUE users.

Tonight SUE security update will include:

apache update to version 1.3.27-4
       (official Red Hat fix for buffer overflows
        in mod_alias and mod_rewrite)

lftp update to version 2.4.9-2
        (buffer overflow fix)

If you are running a web service on top of CERN
Linux 7.3.X we recommend you to perform this update
manually by running as root:

   /usr/sue/etc/sue.update -uselog security

After the update of apache packages your web server
will be restarted automatically.

Please report problems to Linux.Support@cern.ch

Jaroslaw Polok
</pre>
</td>
</tr>
<tr>
<td><a name="apache-1.3.27-3.cern">
<b>8.12.2003: [SECURITY] CERN Linux 7.3.X: Apache and rsync update</b>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux / SUE users.

Tonight SUE security update will include:

apache update to version 1.3.27-3.cern
        CERN version rebuilt with back ported
        security fix for CVE CAN-2003-0542 advisory
        (buffer overflows in mod_alias and mod_rewrite)

rsync update to version 2.5.7-0.7
        security fix for CVE CAN-2003-0962 advisory
        (heap overflow remote vulnerability)

If you are running a web service on top of CERN
Linux 7.3.X we recommend you to perform this update
manually by running as root:

   /usr/sue/etc/sue.update -uselog security

After the update of apache packages your web server
will be restarted automatically.

Please report problems to Linux.Support@cern.ch

Jaroslaw Polok
</pre>
</td>
</tr>
<tr>
<td><a name="2.4.20-24.7"></a>
<b>3.12.2003: [SECURITY] CERN Linux 7.3.X: Kernel and OpenAFS update.</b>
</td>
</tr>
<tr>
<td>
<pre>

Dear Linux users.

An exploitable security vulnerability exists in current
(2.4.20-18.7.cern) CERN Linux 7.3.X kernel
(and ALL previous kernel versions).

We ask all CERN Linux system administrators to
upgrade as soon as possible to a newer version
(2.4.20-24.7.cern) that fixes the problem.

This update will not be applied automatically for now,
since it requires a reboot of the machine. Please follow
the recipe given below.

The new kernel contains additional functionality,
(comparing to Red Hat 2.4.20-24.7):

     3w-xxxx driver update to version 1.02.00.036
     NTFS driver update to version 2.1.4c(read-only)
     qla2x00 driver update to version 6.06.10
     e100 driver update to version 2.3.13-k1-1
     e1000 driver update to version 5.1.11-k1

plus the 'usual' set of CERN applied patches:

     SGI xfs filesystem version 1.3.1
     SGI kdb kernel debugger version 3
     Big Physical Area patch
     (Re)added bcm5700 driver
     LKCD (Linux Kernel Crash Dump) version 4.1.1
     CIFS filesystem cvs (~0.8.7)
     LUFS filesystem 0.9.7
     SCSI tape (st) patch

In order to upgrade your system please apply following
procedure (as root):

*****************************************************

# cd /afs/cern.ch/project/linux/redhat/cern/7.3.3/updates/

Install new kernel:

# rpm -ivh i686/kernel-2.4.20-24.7.cern.i686.rpm \
         i686/kernel-smp-2.4.20-24.7.cern.i686.rpm \
         i386/kernel-source-2.4.20-24.7.cern.i386.rpm

(change i686 to whatever suits your architecture:
i586,athlon or even i386, ignore SMP kernel for
uniprocessor machines, ignore source code unless you
do kernel development)

Upgrade OpenAFS:

(this version can be used with both 2.4.20-24.7.cern
and 2.4.20-18.7.cern kernels)

# rpm -Fvh i386/openafs*1.2.9-rh7.3.7.cern.i386.rpm

Edit your bootloader configuration:

(/etc/grub.conf or /etc/lilo.conf): change the

default=X

line to select the new kernel entry (in this case,
the entry version number 2.4.20-24.7.cern(smp))
(in grub.conf, the first entry is the zero'th entry
and thus default count starts at 0)

Reboot your system:

# /sbin/shutdown -r now

*******************************************************

Please report problems (if any...) to

               Linux.Support@cern.ch

Jaroslaw Polok
</pre>
</td>
</tr>
<tr>
<td><a name="onhold"></a>
<h3>15.10.2003: Next CERN Linux version certification ON HOLD</h3>
</td>
 </tr>
    <tr>
      <td>
        <pre>
Dear all,

the minutes from the Linux certification coordination meeting on
October 2nd are now available under
<a href="http://cern.ch/linux/documentation/LXCERT/">http://cern.ch/linux/documentation/LXCERT/</a>

Contrary to what is said in the summary there, we have in the mean
time agreed to put the current certification formally on hold until we
have decided which new distribution to take. The two certification
machines (lxcert02 and lxcert03) will stay available with Fedora
(ex-"Red Hat 10 beta") until further notice, for those of you who
would like to continue testing.

On October 9th, we had a Red Hat sales representative at CERN. We will
discuss the issue of which distribution to take as a base for the
future CERN Linux together with the other HEP Labs at HEPiX, next
week. The idea is still to move closer towards a HEP-wide solution.
As a consequence of the current certification not going forward, we
will not make the preferred time window (around Christmas) for a new
release this year.

* Therefore, IT-ADC will support the current 7.3.x CERN Linux release
* until end of 2004 with security fixes.

We still hope to have a new certified version available in spring
2004, and would like all non-server machines to move to the new
version as soon as possible afterwards, all other machines should have
migration at least planned in advance. New hardware coming in 2004 may
only be supported on the new release.

Best regards

Jan Iven
</pre>
</tr>


<tr>
<td><a name="wu-ftpd"></a>
<h3>01.08.2003: vulnerable wu-ftpd</h3>
</td>
 </tr>
    <tr>
      <td>
        <pre>

Dear Linux users,

please be aware of a new security hole in wu-ftpd that is supposed to
be remotely exploitable and would give "root" access.

CERN Linux 7.3.x machines will get updated packages automatically
(SUE/new this evening, SUE/pro will follow at the beginning of next
week). If you are running wu-ftpd on 7.3.x as a service, you are
recommended to upgrade the package yourself as soon as possible to
minimize exposure:

rpm -Uvh http://linuxsoft.cern.ch/redhat/cern/7.3.3/updates/i386/wu-ftpd-2.6.2-11.73.1.i386.rpm

or

rpm -Uvh /afs/cern.ch/project/linux/redhat/cern/7.3.3/updates/i386/wu-ftpd-2.6.2-11.73.1.i386.rpm


In case you are running something different: All major vendors have
released advisories and patches.

RedHat:
http://rhn.redhat.com/errata/RHSA-2003-245.html
http://rhn.redhat.com/errata/RHSA-2003-246.html

SuSE:
http://www.suse.com/de/security/2003_032_wuftpd.html

Debian:
http://www.debian.org/security/2003/dsa-357

Mandrake:
http://www.mandrakesecure.net/en/advisories/advisory.php?name=MDKSA-2003:080

Please note that no patches are available for RedHat-6 based
systems. Please turn off FTP on these machines, and migrate to 7.3.3. asap.


(sorry, I know this is a friday afternoon)
Jan

</pre>
</tr>

<tr>
<td><a name="nfs-utils"></a>
<h3>22.07.2003: CERN Linux 7: SUE security update tonight (22.07.2003)</h3>
</td>
 </tr>
 <tr>
      <td>
          <pre>
Dear Linux / SUE users

Tonight another  security update will be applied on
your CERN Linux 7.3.X systems running SUE.

This update will contain the following:

nfs-utils-0.3.3-6.73

xpdf-1.00-7 (plus additional chinese/japanese support
             packages)

mozilla-1.0.2 (all packages including nss/psm/mail/chat etc)


If your system is acting as NFS file server we
STRONGLY recommend to apply the update immediately

(without waiting for the automatic SUE run at night)

and check your NFS setup.

The update can be applied by executing:

/usr/sue/etc/sue.update -uselog security



Please report problems to Linux.Support@cern.ch

Jaroslaw Polok

</pre>
</tr>

<tr>
<td><a name="733release"></a>
<h3>07.07.2003: CERN Linux 7.3.3 released, replaces 7.3.2 as CERN standard version</h3>
</td>
 </tr>
   <tr>
      <td>
          <pre>
Dear Linux Users

As preannounced CERN Linux 7.3.3 maintenance release enters production
today.

This release contains all Red Hat errata released until end of
June 2003 and should have minimal user-visible changes, which is
why no formal certification has been done.

Properly patched 7.3.1/7.3.2 systems should be indistinguishable
from 7.3.3 so if you run one of these and SUE is maintaining
your system updates the only thing which needs to be done is the
kernel update (to version 2.4.20-18.7.cern) announced last week.

A complete list of changes is available in README.updates
and in README.kernel.
<!-- href="redhat73/7.3.3/i386/README.updates">README.updates</a> and <a
href="redhat73/7.3.3/i386/README.kernel">README.kernel</a> in
the distribution.-->

Jaroslaw Polok for Linux.Support@cern.ch
</pre>
</tr>

<tr>
<td><a name="2.4.20-18.7"></a>
<h3>30.06.2003: [SECURITY] CERN Linux 7.3.X: Kernel and OpenAFS update.</h3>
</td>
</tr>
<tr>
<td>
<pre>

Dear Linux users.

Potentially exploitable security vulnerabilities
exist in current (2.4.18-27.7.x.cern) CERN Linux 7.3.X
kernel (and previous kernel versions).

We ask all CERN Linux system administrators to
upgrade as soon as possible to a newer version
(2.4.20-18.7.cern) that fixes the problem.

This update will not be applied automatically for now,
since it requires a reboot of the machine. Please follow
the recipe given below.

The new kernel contains additional functionality,
(comparing to Red Hat 2.4.20-18.7):

     3w-xxxx driver update to version 1.02.00.033
     NTFS driver update to version 2.1.4a(read-only)

plus the 'usual' set of CERN applied patches:

     SGI xfs filesystem version 1.2
     SGI kdb kernel debugger version 3
     Big Physical Area patch
     Added bcm5700 driver
     qla2x00 driver update to version 6.04.00
     LKCD (Linux Kernel Crash Dump) version 4.1.1

We expect that these changes will have little impact
on most users or machines.


In order to upgrade your system please apply following
procedure (as root):

*****************************************************

# cd /afs/cern.ch/project/linux/redhat/cern/7.3.2/updates

Install new kernel:

# rpm -ivh i686/kernel-2.4.20-18.7.cern.i686.rpm \
         i686/kernel-smp-2.4.20-18.7.cern.i686.rpm \
         i386/kernel-source-2.4.20-18.7.cern.i386.rpm

(change i686 to whatever suits your architecture:
i586,athlon or even i386, ignore SMP kernel for
uniprocessor machines, ignore source code unless you
do kernel development)

Upgrade OpenAFS:

(this version can be used with both 2.4.20-18.7.cern
and 2.4.18-27.7.x.cern kernels)

# rpm -Fvh i386/openafs*1.2.9-rh7.3.3.cern.i386.rpm

Edit your bootloader configuration:

(/etc/grub.conf or /etc/lilo.conf): change the

default=X

line to match new kernel entry (entry count in
grub.conf starts from zero)

Reboot your system:

# /sbin/shutdown -r now

*******************************************************

Please report problems (if any...) to

               Linux.Support@cern.ch


Jaroslaw Polok
</pre>
</td>
</tr>
<tr>
<td><a name="SUEXFreeypserv"></a>
<h3>30.06.2003: CERN Linux 7: SUE security update this Wednesday (2.07.2003)</h3>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux / SUE users.

On Wednesday a  security update larger than usual will
be applied on your CERN Linux 7.2.1/7.3.X systems running
SUE.

This update will contain following:

CERN Linux 7.3.X: ypserv-2.8-0.73E
CERN Linux 7.2.1: ypserv-2.8-0.72E

and

CERN Linux 7.3.X: XFree86-4.2.1-13.73.3 (multiple packages)
CERN Linux 7.2.1: XFree86-4.1.0-49 (multiple packages)


If your system is acting as an NIS server we strongly
suggest you to update it on Wednesday (AFTER 14:00)
by running

      /usr/sue/etc/sue.update -uselog security

(before automatic update happens on Wednesday night)

If it isn't .. you may still want to update the system
manually as the update size approaches 50MB - by running
the above command.


Please report problems to Linux.Support@cern.ch

Jaroslaw Polok
</pre>
</td>
<tr>
<tr>
<td><a name="733test"></a>
<h3>30.06.2003: Call for testers: Upcoming CERN Linux 7.3.3 (Maintenance release)</h3>
</td>
</tr>
<tr>
<td>
<pre>
Dear Linux users.

As the time passes and we collect more and more security
related updates coming from Red Hat time has come to
prepare new CERN Linux 7.3.X maintenance release.

The release candidate for CERN Linux 7.3.3 is available
for testing from now on:

Boot floppies can be found at:

/afs/cern.ch/project/linux/redhat/cern/7.3.3/i386/images/

The installation path for 7.3.3 is:

(nfs|http|ftp) linuxsoft.cern.ch:/redhat/cern/7.3.3/i386/


The major difference between CERN Linux 7.3.2 (7.3.1)
and 7.3.3 is the installation tree built around following
kernels:

7.3.1: 2.4.18-18.7.x.cern
7.3.2: 2.4.18-27.7.x.cern
7.3.3: 2.4.20-18-7.cern

Then of course all Red Hat updates , up to:

RHSA-2003:066
Updated XFree86 packages provide security and bug fixes

were included in this maintenance release.

Please note that if your 7.3.1/7.3.2 system is running
automatic SUE security updates all of these updates were/
or will be soon/ applied to it.
(Except the kernel update which is to be announced
shortly)

Providing the tests are successful this
release will became the default one as of:

Monday 7 July 2003

replacing the current 7.3.2

Please report any problems to Linux.Support@cern.ch

Jaroslaw Polok
</pre>
</td>
</tr>

<tr>
<td><a name="732release"></a>
<h3>09.04.2003: 7.3.2 released, replaces 7.3.1 as CERN standard version</h3>
</td>
 </tr>
   <tr>
      <td>
          <pre>
Dear all,

in order to capture all the security updates released since 7.3.1 came
out (including the latest updates to the kernel and glibc), we have
now created a new CERN Linux 7.3.2 release. Please use 7.3.2 for all
installations from now on, since older version are insecure in the
default installation.

7.3.2 contains only RedHat errata and should have minimal user-visible
changes, which is why no formal certification has been done. A
properly patched 7.3.1 system should be indistinguishable from 7.3.2.
7.3.1 will stay publicly available for a short period in case you
experience problems with 7.3.2, but eventually, we will restrict
access to it. If you continue to use 7.3.1 for now, <b>please</b> make sure to
apply all updates immediately after installation.

A complete list of changes is available in <a
href="redhat73/7.3.2/i386/README.updates">README.updates</a> and <a
href="redhat73/7.3.2/i386/README.kernel">README.kernel</a> in
the distribution.
</pre>
</tr>
<tr>
<td><a name="731kern1"></a>
<h3>27.03.2003: Local security hole in 7.3.1 kernel</h3>
</td>
 </tr>
  <tr>
   <td>
    <pre>
Dear all,

a locally exploitable security hole exists in the Linux kernel
(2.4.18-18.7.x) for our current Linux distribution (7.3.1). We ask all
CERN Linux system administrators to upgrade as soon as possible to a
newer version (2.4.18-27.7.x) that fixes this problem.

This update will not be applied automatically for now, since it
requires a reboot of the machine. Please follow the recipe given below.

The new kernel contains additional functionality, a complete list of
changes to the Red Hat 2.4.18-27 kernel is appended. We expect that
these changes will have little impact most users or machines.


Recipe for upgrading (thanks to Jarek):

  # cd /afs/cern.ch/project/linux/redhat/cern/7.3.1/updates

 Install new kernel:

  # rpm -ivh i686/kernel-2.4.18-27.7.x.cern.i686.rpm \
             i686/kernel-smp-2.4.18-27.7.x.cern.i686.rpm \
             i386/kernel-source-2.4.18-27.7.x.cern.i386.rpm

  (change i686 to whatever suits your arch: i586,athlon or even i386,
  ignore SMP kernel for monoprocessors machines, ignore source code
  unless you do kernel development)

  Upgrade OpenAFS:

  # rpm -Fvh i386/openafs*rh7.3.6*rpm

  (this version supports following kernels:
   2.4.18-18.7.cern and 2.4.18-27.7.cern)

  Upgrade quota package (or you'll get a WARNING that kernel quota
  is newer than system)

  # rpm -Fvh i386/quota-3.06-5.i386.rpm


  Freshen (or install if not included in your setup, and you plan to use
  XFS...) XFS utilities/libraries:

  # rpm -Fvh i386/libattr* i386/libacl* i386/acl* i386/attr* i386/dmapi* i386/xfsdump* i386/xfsprogs*

  (or rpm -ivh ...)



  Edit your bootloader config and reboot
  (/etc/grub.conf or /etc/lilo.conf)
    |
    \---> change default=X line (stanza count starts from zero)
  Have fun

  Jarek

----------------------------------------------------------------------------
Changes in 2.4.18-27.7.x.cern kernel:
(comparing to RH 2.4.18-26.7.x)

1. Upgraded drivers:
        Qlogic qla2X00 drivers to version 6.04.00
        3ware 3w-xxxx drivers to version 1.02.00.027
        i810_audio to version 0.24
        ntfs driver to 2.1.0a (read-only new driver)

2. Added drivers:
        bcm5700 (removed by RH from this update)
        NVIDIA GeForce kernel driver (1.0-4191)
        NVIDIA NForce net & audio support (1.0.4)

3. Added functionality:
        CIFS 0.68 (Common Internet File System)
        SGI XFS filesystem v 1.2
        SGI kdb kernel debugger v 3
        LKCD 4.1.1 Linux Kernel Crash Dump driver.

4. Fixes:
        SMP inode race patch (should correct ext3 corruption seen in afscache)
        workaround for the ServerWorks OSB4 UDMA bug - go to MWDMA2 instead
        Scheduler yield patch
        SCSI st driver fixes for tape servers

18.03.2003
Jaroslaw.Polok@cern.ch
</pre>
</tr>
<tr>
<td><a name="611eolconfirm"></a>
<b>20.01.2003: End-of-Life announcements for CERN Linux 6.1.1 and
7.2.1 (confirmed)</b>
</td>
 </tr>
  <tr>
   <td>
    <pre>
Dear readers,

as announced on 9th of December<a href="#611eol">[1]</a>, the obsolete
CERN Linux 6.1.1 and 7.2.1 are now frozen with immediate effect.  As a
reminder, all support for these distributions will stop completely on
1st of July 2003.

Best regards
Jan
</pre>
</tr>
<tr>
<td><a name="611eol"></a>
<h3>09.12.2002: End-of-Life announcements for CERN Linux 6.1.1 and 7.2.1</h3>
</td>
 </tr>
  <tr>
   <td>
    <pre>

Dear readers,

pending a final decision of the Desktop Forum on January 16th 2003, we
will "freeze" the now obsolete CERN Linux 6.1.1 and 7.2.1
distributions on that day. Support for both will stop on 1st of July
2003. This has been agreed on in the December 5th FOCUS meeting.

(The original proposal to the Desktop Forum and FOCUS was a freeze on
 January 1st, but has been shifted to match the next DTF
 meeting.)

In order to avoid confusion, a short glossary:

"Freeze": no more updates for applications software, e.g. through
        ASIS. Preferred solution to error reports is an upgrade to the
        next supported version, 7.3.1 in this case. Security
        updates will still be provided.

"End of support": no more installations, no more user support, no more
        security updates. Remaining machines have to be secured
        individually or risk being turned off in case of security
        problems.

Best regards
Jan
</pre>
</tr>

<tr>
<td><a name="731release"></a>
<h3>26.11.2002: CERN Linux 7.3.1 released</h3>
</td>
 </tr>
  <tr>
   <td>
    <pre>
Dear readers,

we would like to announce that CERN Linux 7.3.1 (a customised version
of Red Hat 7.3) is now certified. This version replaces the previous
CERN Linux 7.2.1 as the default supported version of Linux at CERN.

CERN Linux 7.3.1 is available for network installs and on CD (which
will be distributed through the IT bookshop,
http://consult.cern.ch/books/). A matching CD with ASIS packages will
be made available.

Please see http://cern.ch/linux/redhat73/ for installation
instructions, more documentation and an overview of the certification
process itself.

Thanks to all participants, especially to the members of the (newly
established) Linux certification coordination body.

Jarek Polok,
Julian Blake,
Jan Iven,
Linux.Support@cern.ch
</pre>
</tr>

</table>

</div>


