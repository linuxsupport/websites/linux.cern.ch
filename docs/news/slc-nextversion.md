
# Scientific Linux @ CERN: Next Version

<p>
On January 7 2014, Red Hat and CentOS announced that they
joined forces (<a href="http://www.centos.org">centos.org</a>).
<p>
Since Scientific Linux relies on Red Hat Enterprise
Linux source code, this is of great interest to the
Scientific Linux project. We have been learning more
about their plans and considering the possibilities for
Scientific Linux.
<p>
We've had conversations with CentOS and Red Hat, and
between Fermilab and CERN. We plan further discussions
with these groups and also with other contributors to
and users of Scientific Linux. <strike>No final decisions have
been made</strike>, but we can provide an update on our thoughts
so far.

<p>
Fermilab and CERN remain committed to the original
goal of Scientific Linux: providing a stable,
well-supported, open-source platform which meets
the needs of high-energy physics experiments.
The fact that this platform is used by people outside
of that community is something we appreciate and will
be a factor in any decisions going forward.
<p>
There are still many questions to pursue as the
details of CentOS Special Interest Groups continue
to evolve. The anticipated release of Red Hat
Enterprise Linux 7 presents an opportunity to
consider forming/joining a CentOS Special Interest
Group (<a href="http://www.centos.org/about/governance/sigs/">centos.org/about/governance/sigs/</a>)
and producing Scientific Linux 7 as a CentOS variant
(<a href="http://www.centos.org/variants/">centos.org/variants/</a>). The variant
structure may allow greater flexibility in
adapting the distribution to scientific needs.
The framework and relationship structure of CentOS
Special Interest Groups is still under heavy discussion
on the CentOS development list. This is only being
evaluated for Scientific Linux version 7.
<p>
Security and other updates for the current Scientific
Linux versions 5 and 6 will continue uninterrupted.
We expect the process for SL 5 and 6 support to remain
essentially the same, with the only substantive change
being that source code will come from centos.org rather
than redhat.com. We expect this change to be
transparent to all users.
<p>
There will be many more details to fill in, and
we'll try to keep everyone in the Scientific Linux
community informed as we continue to explore the
options the Red Hat / CentOS partnership presents.
<p>

<b>Update 2015:</b>
<p>
CERN will base next Linux releases on CentOS releases,
starting with CentOS 7 major version.
<p>
Support for Scientific Linux CERN 5 and 6
releases remains unchanged.
<p>

Linux.Support@cern.ch

</p>

<h2>Updates:</h2>
<ul>
<li>[01 Apr 2015 - CERN CentOS 7.1 release available](centos/centos7)</a>
<li><a href="/docs/GDB 20140212 - Future of Scientific Linux in light of recent CentOS project changes..pdf">12 Feb 2014 - Grid Deployment Board presentation</a>
<li><a href="/docs/GDB 20140312 - Future of Scientific Linux in light of recent CentOS project changes..pdf">12 Mar 2014 - Grid Deployment Board presentation</a>
<li><a href="/docs/Hepix-Spring-2014 Next Linux version at CERN.pdf">19 May 2014 - HEPiX Spring - Annecy - presentation</a>
<li><a href="https://indico.cern.ch/event/274555/session/11/#20140519" target="_new">19 May 2014 - HEPiX 2014 Spring meeting - IT end user services and operating systems track (slides)</a>
<li><a href="/centos7/">17 Jul 2014 - CERN CentOS 7 pre-release available for evaluation/testing</a>
<li><a href="/centos7/">04 Aug 2014 - CERN CentOS 7 TEST release available</a>
<li><a href="https://indico.cern.ch/event/353148/">14 Nov 2014 - Linux Certification Committee Meeting decided to change formal certification process for future Linux releases at CERN"</a>
</ul>



