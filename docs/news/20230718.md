<div class="admonition news_date">2023-07-18</div>

## AlmaLinux focuses on Application Binary Interface compatibility

In response to Red Hat's [recent announcement](https://www.redhat.com/en/blog/furthering-evolution-centos-stream) to restrict public source code releases to CentOS Stream, the AlmaLinux Foundation [has announced](https://almalinux.org/blog/future-of-almalinux/) that they will drop the aim to be "bug-for-bug compatible" with RHEL, and instead focus on Application Binary Interface (ABI) compatibility. They will work to ensure that applications built to run on RHEL can run without any issues on AlmaLinux, even in situations where package versions deviate slightly from those available in RHEL.

The CERN IT Linux Team and others in the WLCG community continue to be in close contact with the AlmaLinux Foundation to express our needs and they have been very receptive. At this time, our commitment to support AlmaLinux and RHEL remains unchanged. We will continue to monitor the situation and work with our users, Fermilab and the rest of our WLCG partners to ensure we maintain a stable base for our services. We will share further updates as more developments arise.
