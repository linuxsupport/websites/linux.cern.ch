<div class="admonition news_date">2022-12-08</div>

## Fermilab/CERN recommendation for Linux distribution

CERN and Fermilab jointly plan to provide AlmaLinux as the standard distribution for experiments at our facilities, reflecting recent experience and discussions with experiments and other stakeholders. AlmaLinux has recently been gaining traction among the community due to its long life cycle for each major version, extended architecture support, rapid release cycle, upstream community contributions, and support for security advisory metadata. In testing, it has demonstrated to be perfectly compatible with the other rebuilds and Red Hat Enterprise Linux.

CERN and, to a lesser extent, Fermilab, will also use Red Hat Enterprise Linux (RHEL) for some services and applications within the respective laboratories. Scientific Linux 7, at Fermilab, and CERN CentOS 7, at CERN, will continue to be supported for their remaining life, until June 2024.
