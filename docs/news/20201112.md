<div class="admonition news_date">2020-11-12</div>

## CERN CentOS 7.9 testing available for installation.

Dear Linux users.

We are pleased to announce the newly updated CERN CentOS Linux (CC7) version:

          *****************************************
          CC7.9 TEST is available for installation
          *****************************************
                as of Thursday 12.11.2020

and will become the default CC7 production version on 16.11.2020.

For information about CC7.9, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Installation:

  - via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

  - non-PXE installation:
    <a href="http://linuxsoft.cern.ch/cern/centos/7.9/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.9/os/x86_64/images/</a>
    and corresponding installation path is:
    <a href="http://linuxsoft.cern.ch/cern/centos/7.9/os/x86_64/">http://linuxsoft.cern.ch/cern/centos/7.9/os/x86_64/</a>

  - Automated PXE installations using AIMS2
    Boot target is:
    * CC79_X86_64_TEST

  - Openstack TEST image is also available:
    * name : CC7 TEST - x86_64 [2020-11-12]
    * id   : 5ce3a956-976b-4425-a480-4f24021fe53e

  - Puppet managed VMs can be created with the following options:
    ai-bs --cc7 --nova-image-edition Test [...]

* Upgrading from a previous CERN CentOS 7.X release:

  All updates will be released to production repositories only on 16.11.2020.
  Until then CC7 systems can be updated by running:

```
# yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing clean all
# yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing update
``

* Problem reporting:
  Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>
