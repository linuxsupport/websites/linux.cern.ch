<div class="admonition news_date">2018-11-19</div>

## CERN CentOS 7.6 testing available.

Dear Linux users.

We have a pleasure to announce that updated CERN Centos Linux (CC7) version:

            *****************************************
            CC 7.6 TEST is available for installation
            *****************************************
                    as of Monday 19.11.2018

and will become default CC7 production version on 10.12.2018.

For information about CC76, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

Updating CERN CentOS 7.5 to 7.6:

All updates will be released to production repositories on Monday 10.12.2017.

Until then CC7 system can be updated by running:

&#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing clean all
&#35; yum --enablerepo=cr --enablerepo={updates,extras,cern,cernonly}-testing update


Thomas Oulevey for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>


