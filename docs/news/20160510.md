<div class="admonition news_date">2016-05-10</div>

## RHEL 6.8 - Red Hat Enterprise Linux 6 Server Update 8 available

Dear Linux users.

Red Hat Enterprise Linux 6 Server Update 8 for i386/x86_64
platforms is available for installation at CERN,
starting today.

AIMS2 PXE boot targets are as follows:

    RHEL_6_8_I386
    RHEL_6_8_X86_64

installation paths are:

<a href="http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.8/i386/">http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.8/i386/</a>
<a href="http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.8/x86_64/">http://linuxsoft.cern.ch/enterprise/rhel/server/6/6.8/x86_64/</a>

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel6">http://cern.ch/linux/rhel/#rhel6</a>


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>


