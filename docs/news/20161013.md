<div class="admonition news_date">2016-10-13</div>

## End of General User Support for SLC5 - March 2017

Dear Linux users.

As approved on September 2016 <a href="https://indico.cern.ch/event/566895/">Linux Certification Committee meeting</a>
and presented on October 2016 <a href="https://indico.cern.ch/event/555742/">IT Technical Users Meeting</a>
general user support for Scientific Linux CERN 5 (SLC5) will end on <b>31st of March 2017</b>.

After that date user requests for SLC5 support will be <em>rejected</em> by
CERN service desk.

Dedicated support for experiments and accelerator controls will be
maintained for <em>four months</em> after LHC Long Shutdown beginning until <b>31st of March 2019</b>.

At the end of the dedicated support phase SLC5 will reach
the End of Life phase when support will not be provided anymore
and no more software updates will be made available.


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>


