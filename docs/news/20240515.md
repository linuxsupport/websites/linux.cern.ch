<div class="admonition news_date">2024-05-15</div>

## AlmaLinux 9.4 is available in production

We are pleased to announce that AlmaLinux 9.4 is available in production as of today.

The list of updated packages can be found [here](/updates/alma9/prod/2024/05/monthly_updates/#2024-05-15).
