<div class="admonition news_date">2024-06-30</div>

## CC7 & RHEL7 have reached end-of-life

As of 30.06.2024, both CC7 (CERN CentOS 7) and RHEL7 (Red Hat Enterprise Linux 7) have reached end-of-life.

These distributions are no longer supported at CERN.

If you are still using CC7 or RHEL7, we encourage you to migrate to a newer supported distribution as soon as possible.

You may refer to the [Which distribution should I use?](/which) page for additional details.
