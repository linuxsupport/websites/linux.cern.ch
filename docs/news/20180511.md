<div class="admonition news_date">2018-05-11</div>

## CERN CentOS 7.5 testing available.

Dear Linux users.

We have a pleasure to announce that updated CERN Centos Linux (CC7) version:

            *****************************************
            CC 7.5 TEST is available for installation
            *****************************************
                    as of Friday 11.05.2018

and will become default CC7 production version on 24.05.2018.

For information about CC75, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Installation:

    - via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

    - non-PXE installation:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/images/</a>

    and corresponding installation path is:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/">http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/</a>

    - Automated PXE installations using AIMS2

    Boot target is:

        CC75_X86_64_TEST

* Upgrading from previous CERN CentOS 7.X release:

    All updates will be released to production repositories only on
24.05.2018.

    Until then CC7 system can be updated by running:

    &#35; yum --enablerepo=cr
--enablerepo={updates,extras,cern,cernonly}-testing clean all
    &#35; yum --enablerepo=cr
--enablerepo={updates,extras,cern,cernonly}-testing update

* Problem reporting:

    Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

--
Thomas Oulevey on behalf of the Linux Team


