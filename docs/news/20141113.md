<div class="admonition news_date">2014-11-13</div>

## Software Collections 1.2 and Developer Toolset 3.0 for Scientific Linux CERN 6

Dear Linux Users,

CERN Software Collections and Developer Toolset are offerings
for developers on the Scientific Linux CERN 6 platform.

Using a framework called Software Collections, an additional
set of tools is installed into the /opt directory,
as recommended by the UNIX Filesystem Hierarchy Standard.

These tools are enabled by the user on demand using the
supplied scl utility.

CERN Software Collections 1.2 provides following tools:

    Developer Toolset version 3.0, including:
    gcc/g++/gfortran - GNU Compiler Collection - version 4.9.1
    gdb - GNU Debugger - version 7.8
    [ ... and more tools, please see link at the bottom ]

    DevAssistant version 0.9 - A tool which helps with
    creating and setting up basic projects in various languages.

    Git version 1.94 - Fast Version Control System

    Httpd version 2.4.6 - Apache HTTP Server

    Maven version 3.0.5 - Java project management and
    project comprehension tool

    Mongodb version 2.4.9 - High-performance, schema-free
    document-oriented database

    Nginx version 1.6.1 -  A high performance web server
    and reverse proxy server

    Php version 5.5.6 - Scripting language for creating
    dynamic web sites

    Ruby version 2.0.0 - An interpreter of object-oriented
    scripting language

    Ruby on Rails version 4.0

    Thermostat version 1.0.4 - A monitoring and serviceability
    tool for OpenJDK


Please note that products released in the past as part of:

- Software Collections 1.0
    <a href="http://cern.ch/linux/scl/">http://cern.ch/linux/scl/</a>
    (Mysql 5.5, Mariadb 5.5, Postgresql 9.2, Python 2.7 and 3.3,
        php 5.4, nodejs 0.10, perl 5.15)

- Developer Toolset 1.1/2.0/2.1
    <a href="http://cern.ch/linux/devtoolset/">http://cern.ch/linux/devtoolset/</a>
    ( gcc/g++/gfortran 4.8.2, eclipse 4.3.1, ...)

remain available for installation.

For more information and installation instructions, please visit:

<a href="http://cern.ch/linux/scl/">http://cern.ch/linux/scl/</a>

<a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

