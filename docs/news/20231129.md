<div class="admonition news_date">2023-11-29</div>

## AlmaLinux 8.9 is available in production

We are pleased to announce that AlmaLinux 8.9 is available in production as of today.

The list of updated packages can be found [here](/updates/alma8/prod/2023/11/monthly_updates/#2023-11-29).
