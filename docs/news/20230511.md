<div class="admonition news_date">2023-05-11</div>

## AlmaLinux 9.2 is available for testing

We are pleased to announce that AlmaLinux 9.2 is available for testing as of today.

AlmaLinux 9.2 will be available in production on 2023-05-24.

The list of updated packages can be found [here](/updates/alma8/test/2023/05/monthly_updates/#2023-05-11).

The release notes can be found [here](https://wiki.almalinux.org/release-notes/9.2.html).
