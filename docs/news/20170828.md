<div class="admonition news_date">2017-08-28</div>

## CERN CentOS 7.4 testing available.

Dear Linux users.

We have a pleasure to announce that updated CERN CentOS Linux
(CC7) version:

            *******************************
            CC 7.4 is available for testing
            *******************************
                as of Monday 28.08.2017

and will become default CC7 production version

                    on Monday 11.09.2017.

For information about CC74, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>


* Updating CERN CentOS 7.3 to 7.4:

Due to summer holidays, all updates will be
released to production repositories only on 11.09.2017.

Until then CC7 system can be updated by running:

&#35; yum --enablerepo=cr \
    --enablerepo={updates,extras,cern}-testing \
    --enablerepo=epel-testing \
    clean all

&#35; yum --enablerepo=cr \
    --enablerepo={updates,extras,cern}-testing \
    --enablerepo=epel-testing \
    update

* Agile infrastructure

As a reminder, all puppet configured machines within the
QA environment will receive automatically those updates.

Thomas Oulevey for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>


