<div class="admonition news_date">2015-11-25</div>

## RHEL7.2 - Red Hat Enterprise Linux 7 Server Update 2 available.

Dear Linux users.

Red Hat Enterprise Linux 7 Server Update 1 for x86_64
platform is available for installation at CERN,
starting today.

AIMS2 PXE boot target:

    RHEL_7_2_X86_64

installation path:

<a href="http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.2/x86_64/">http://linuxsoft.cern.ch/enterprise/rhel/server/7/7.2/x86_64/</a>

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel7">http://cern.ch/linux/rhel/#rhel7</a>


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>


