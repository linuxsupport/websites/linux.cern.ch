<div class="admonition news_date">2017-10-20</div>

## CentOS Dojo @ CERN

The CentOS Dojo's are a one day event, organized
around the world, that bring together people from
the CentOS communities to talk about systems
administration, best practices in Linux centric
activities and emerging technologies of note.

The (almost) final schedule for our CERN event
is now published at:

<a href="https://indico.cern.ch/e/centosdojo2017">https://indico.cern.ch/e/centosdojo2017</a>

Please register for the event: limited number of
places available !


