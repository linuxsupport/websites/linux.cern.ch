<div class="admonition news_date">2018-12-10</div>

## CERN CentOS 7.6 installation available.

Dear Linux users.

We have a pleasure to announce that updated CERN Centos Linux (CC7) version:

                  *******************
                  CC 7.6 is available
                  *******************
                as of Monday 10.12.2018

and is now default CC7 production version.

For information about CC76, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Installation:

    - via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

    - non-PXE installation:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.6/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.6/os/x86_64/images/</a>

    and corresponding installation path is:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.6/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.6/os/x86_64/images/</a>

    - Automated PXE installations using AIMS2

    Boot targets are:

        CC7_X86_64
        CC76_X86_64

    - A new Openstack image is also available:

        name : CC7 - x86_64 [2018-12-03]
        id   : 65bc0185-7fba-4b08-b256-cfc7576d9dda

* Upgrading from previous CERN CentOS 7.X release:

    CC7 system can be updated by running:

    &#35; yum --enablerepo={updates,extras,cern,cernonly} clean all
    &#35; yum --enablerepo={updates,extras,cern,cernonly} update

--
Thomas Oulevey on behalf of the Linux Team


