<div class="admonition news_date">2019-05-13</div>

## RHEL8 - Red Hat Enterprise Linux 8 Server available.

Dear Linux users.

Red Hat Enterprise Linux 8 Server for x86_64
platform is available for installation at CERN,
starting today.

AIMS2 PXE boot target:

    RHEL_8_0_X86_64

installation path:

<a href="http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.0/x86_64">http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.0/x86_64</a>

Interactive network installations are possible from
the 'Red Hat Enterprise Linux' PXE boot menu.

Please note that Red Hat Enterprise Linux Server is a
licensed product installable only on authorized
systems. Therefore before you install your
system please request a license assignment from
linux support.

After installation, please do not forget to setup your
system for updates: <a href="http://cern.ch/linux/rhel/#rhel8">http://cern.ch/linux/rhel/#rhel8</a>


Thomas Oulevey for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>


