<div class="admonition news_date">2016-03-23</div>

## Additional Software Collections available for SLC6 and CC7.

Dear Linux Users,

As of 23.03.2016 additional software collections are available for Scientific Linux CERN 6 and CERN CentOS 7:

    sclo-php54 - provides additional php packages for php54 collection.
    sclo-php55 - provides additional php packages for php55 collection.
    sclo-php56 - provides additional php packages for rh-php56 collection.
    sclo-ror42 - Ruby on Rails - version 4.2

For more information and installation instructions please visit:

- CC7: <a href="http://cern.ch/linux/centos7/docs/softwarecollections">http://cern.ch/linux/centos7/docs/softwarecollections</a>
- SLC6X: <a href="http://cern.ch/linux/scientific6/docs/softwarecollections">http://cern.ch/linux/scientific6/docs/softwarecollections</a>


