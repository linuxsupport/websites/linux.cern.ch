<div class="admonition news_date">2021-06-04</div>

## CentOS 8.4 testing available

We are pleased to announce that the updated CentOS 8 version 8.4 is available for testing as of Friday 2021-06-04.

This version will become the default C8 production version on Wednesday 2021-06-09.

The list of updated packages can be found [here](/updates/c8/test/2021/06/monthly_updates/#2021-06-04).

For information about C8, including installation
instructions please visit [this page](/centos8/).
