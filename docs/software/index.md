# Linuxsoft: Software Repository and Installation service

<p>
<h2>Linuxsoft: Software Repository Service</h2>
<h3>CERN Supported Linux Distributions:</h3>
<ul>
 <li><a href="http://linuxsoft.cern.ch/cern/alma/9/">/cern/alma/9/</a><b>Currently supported CERN Linux distribution: AlmaLinux 9 (ALMA9)</b>
  <ul>
   <li><a href="http://linuxsoft.cern.ch/cern/alma/9/BaseOS/x86_64/os/images/">/cern/alma/9/BaseOS/x86_64/os/images/</a> Installation media images for x86_64
   <li><a href="http://linuxsoft.cern.ch/cern/alma/9/BaseOS/x86_64/os/Packages/">/cern/alma/9/BaseOS/x86_64/os/Packages/</a> Updates (BaseOS)
   <li><a href="http://linuxsoft.cern.ch/cern/alma/9/AppStream/x86_64/os/Packages/">/cern/alma/9/AppStream/x86_64/os/Packages/</a> Updates (AppStream)
   <li><a href="http://linuxsoft.cern.ch/cern/alma/9/CERN/x86_64/Packages/">/cern/alma/9/CERN/x86_64/Packages/</a> CERN packages for ALMA9
  </ul>
 </li>
 <li><a href="http://linuxsoft.cern.ch/cern/alma/8/">/cern/alma/8/</a><b>Currently supported CERN Linux distribution: AlmaLinux 8 (ALMA8)</b>
  <ul>
   <li><a href="http://linuxsoft.cern.ch/cern/alma/8/BaseOS/x86_64/os/images/">/cern/alma/8/BaseOS/x86_64/os/images/</a> Installation media images for x86_64
   <li><a href="http://linuxsoft.cern.ch/cern/alma/8/BaseOS/x86_64/os/Packages/">/cern/alma/8/BaseOS/x86_64/os/Packages/</a> Updates (BaseOS)
   <li><a href="http://linuxsoft.cern.ch/cern/alma/8/AppStream/x86_64/os/Packages/">/cern/alma/8/AppStream/x86_64/os/Packages/</a> Updates (AppStream)
   <li><a href="http://linuxsoft.cern.ch/cern/alma/8/CERN/x86_64/Packages/">/cern/alma/8/CERN/x86_64/Packages/</a> CERN packages for ALMA9
  </ul>
 </li>
 <li><a href="http://linuxsoft.cern.ch/cern/rhel/9/">/cern/rhel/9/</a><b>Currently supported CERN Linux distribution: Red Hat Enterprise Linux 9 (RHEL9)</b>
  <ul>
   <li><a href="http://linuxsoft.cern.ch/cern/rhel/9/baseos/x86_64/os/images/">/cern/rhel/9/baseos/x86_64/os/images/</a> Installation media images for x86_64
   <li><a href="http://linuxsoft.cern.ch/cern/rhel/9/baseos/x86_64/os/Packages/">/cern/rhel/9/baseos/x86_64/os/Packages/</a> Updates (BaseOS)
   <li><a href="http://linuxsoft.cern.ch/cern/rhel/9/appstream/x86_64/os/Packages/">/cern/rhel/9/appstream/x86_64/os/Packages/</a> Updates (AppStream)
   <li><a href="http://linuxsoft.cern.ch/cern/rhel/9/CERN/x86_64/Packages/">/cern/rhel/9/CERN/x86_64/Packages/</a> CERN packages for ALMA9
  </ul>
 </li>
 <li><a href="http://linuxsoft.cern.ch/cern/rhel/8/">/cern/rhel/8/</a><b>Currently supported CERN Linux distribution: Red Hat Enterprise Linux 8 (RHEL8)</b>
  <ul>
   <li><a href="http://linuxsoft.cern.ch/cern/rhel/8/baseos/x86_64/os/images/">/cern/rhel/8/baseos/x86_64/os/images/</a> Installation media images for x86_64
   <li><a href="http://linuxsoft.cern.ch/cern/rhel/8/baseos/x86_64/os/Packages/">/cern/rhel/8/baseos/x86_64/os/Packages/</a> Updates (BaseOS)
   <li><a href="http://linuxsoft.cern.ch/cern/rhel/8/appstream/x86_64/os/Packages/">/cern/rhel/8/appstream/x86_64/os/Packages/</a> Updates (AppStream)
   <li><a href="http://linuxsoft.cern.ch/cern/rhel/8/CERN/x86_64/Packages/">/cern/rhel/8/CERN/x86_64/Packages/</a> CERN packages for ALMA9
  </ul>
 </li>
 <li><a href="http://linuxsoft.cern.ch/cern/centos/s9/">/cern/centos/s9/</a><b>Previous (deprecated) CERN Linux distribution: CentOS Stream 9 (CS9)</b>
  <ul>
   <li><a href="http://linuxsoft.cern.ch/cern/centos/s9/BaseOS/x86_64/os/images/">/cern/centos/s9/BaseOS/x86_64/os/images/</a> Installation media images for x86_64
   <li><a href="http://linuxsoft.cern.ch/cern/centos/s9/BaseOS/x86_64/os/Packages/">/cern/centos/s9/BaseOS/x86_64/os/Packages/</a> Updates (BaseOS)
   <li><a href="http://linuxsoft.cern.ch/cern/centos/s9/AppStream/x86_64/os/Packages/">/cern/centos/s9/AppStream/x86_64/os/Packages/</a> Updates (AppStream)
   <li><a href="http://linuxsoft.cern.ch/cern/centos/s9/CERN/x86_64/Packages/">/cern/centos/s9/CERN/x86_64/Packages/</a> CERN packages for CS9
  </ul>
 </li>
  <li><a href="http://linuxsoft.cern.ch/cern/centos/s8/">/cern/centos/s8/</a> <b>Previous (deprecated) CERN Linux distribution: CentOS Stream 8 (CS8)</b>
  <ul>
   <li><a href="http://linuxsoft.cern.ch/cern/centos/s8/BaseOS/x86_64/os/images/">/cern/centos/s8/BaseOS/x86_64/os/images/</a> Installation media images for x86_64
   <li><a href="http://linuxsoft.cern.ch/cern/centos/s8/BaseOS/x86_64/os/Packages/">/cern/centos/s8/BaseOS/x86_64/os/Packages/</a> Updates (BaseOS)
   <li><a href="http://linuxsoft.cern.ch/cern/centos/s8/AppStream/x86_64/os/Packages/">/cern/centos/s8/AppStream/x86_64/os/Packages/</a> Updates (AppStream)
   <li><a href="http://linuxsoft.cern.ch/cern/centos/s8/CERN/x86_64/Packages/">/cern/centos/s8/CERN/x86_64/Packages/</a> CERN packages for CS8
  </ul>
 </li>
 <li><a href="http://linuxsoft.cern.ch/cern/centos/7/">/cern/centos/7/</a> <b>Previous (deprecated) CERN Linux distribution: CERN CentOS 7 (CC7)</b>
  <ul>
   <li><a href="http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/images/">/cern/centos/7/os/x86_64/images/</a> Installation media images for x86_64
   <li><a href="http://linuxsoft.cern.ch/cern/centos/7/updates/">/cern/centos/7/updates/</a> Updates for CC7
   <li><a href="http://linuxsoft.cern.ch/cern/centos/7/extras/">/cern/centos/7/extras/</a> Extras for CC7
   <li><a href="http://linuxsoft.cern.ch/cern/centos/7/cern/">/cern/centos/7/cern/</a> CERN packages for CC7
  </ul>
 </li>
 <li><a href="http://linuxsoft.cern.ch/cern/centos/8/">/cern/centos/8/</a> <b>Previous (deprecated) CERN Linux distribution: CentOS 8 (C8)</b>
  <ul>
   <li><a href="http://linuxsoft.cern.ch/cern/centos/8/BaseOS/x86_64/os/images/">/cern/centos/8/BaseOS/x86_64/os/images/</a> Installation media images for x86_64
   <li><a href="http://linuxsoft.cern.ch/cern/centos/8/BaseOS/x86_64/os/Packages/">/cern/centos/8/BaseOS/x86_64/os/Packages/</a> Updates (BaseOS)
   <li><a href="http://linuxsoft.cern.ch/cern/centos/8/AppStream/x86_64/os/Packages/">/cern/centos/8/AppStream/x86_64/os/Packages/</a> Updates (AppStream)
   <li><a href="http://linuxsoft.cern.ch/cern/centos/8/CERN/x86_64/Packages/">/cern/centos/8/CERN/x86_64/Packages/</a> CERN packages for C8
  </ul>
 </li>
 <li><a href="http://linuxsoft.cern.ch/cern/slc6X">/cern/slc6X/</a> <b>Previous (deprecated) CERN Linux distribution: Scientific Linux CERN 6 (SLC6)</b>
  <ul>
   <li><a href="http://linuxsoft.cern.ch/cern/slc6X/i386/images/">/cern/slc6X/i386/images/</a> Installation media images for x86 (i386)
   <li><a href="http://linuxsoft.cern.ch/cern/slc6X/x86_64/images/">/cern/slc6X/x86_64/images/</a> Installation media images for x86_64
   <li><a href="http://linuxsoft.cern.ch/cern/slc6X/updates">/cern/slc6X/updates/</a> Updates for SLC6
   <li><a href="http://linuxsoft.cern.ch/cern/slc6X/extras">/cern/slc6X/extras/</a> Extras for SLC6
  </ul>
</ul>

<h3>Browsable Package repositories:</h3>

 <ul>
  <li>ALMA9:
  <ul>
    <li>for x86_64:&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/9/AppStream/x86_64/os/Packages/">AppStream</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/9/BaseOS/x86_64/os/Packages/">BaseOS</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/9/CERN/x86_64/Packages/">CERN</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/9/devel/x86_64/os/Packages/">devel</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/9/HighAvailability/x86_64/os/Packages/">HighAvailability</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/9/CRB/x86_64/os/Packages/">CRB</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/9-testing/AppStream/x86_64/os/Packages/">AppStream (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/9-testing/BaseOS/x86_64/os/Packages/">BaseOS (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/9-testing/CERN/x86_64/Packages/">CERN (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/9-testing/devel/x86_64/os/Packages/">devel (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/9-testing/HighAvailability/x86_64/os/Packages/">HighAvailability (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/9-testing/CRB/x86_64/os/Packages/">CRB (testing)</a>&nbsp;&nbsp;
  </ul>

  <li>ALMA8:
  <ul>
    <li>for x86_64:&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/8/AppStream/x86_64/os/Packages/">AppStream</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/8/BaseOS/x86_64/os/Packages/">BaseOS</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/8/CERN/x86_64/Packages/">CERN</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/8/devel/x86_64/os/Packages/">devel</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/8/HighAvailability/x86_64/os/Packages/">HighAvailability</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/8/CRB/x86_64/os/Packages/">CRB</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/8-testing/AppStream/x86_64/os/Packages/">AppStream (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/8-testing/BaseOS/x86_64/os/Packages/">BaseOS (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/8-testing/CERN/x86_64/Packages/">CERN (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/8-testing/devel/x86_64/os/Packages/">devel (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/8-testing/HighAvailability/x86_64/os/Packages/">HighAvailability (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/alma/8-testing/CRB/x86_64/os/Packages/">CRB (testing)</a>&nbsp;&nbsp;
  </ul>

  <li>RHEL9:
  <ul>
    <li>for x86_64:&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/9/appstream/x86_64/os/Packages/">appstream</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/9/baseos/x86_64/os/Packages/">baseos</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/9/CERN/x86_64/Packages/">CERN</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/9/highavailability/x86_64/os/Packages/">highavailability</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/9/codeready-builder/x86_64/os/Packages/">codeready-builder</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/9-testing/appstream/x86_64/os/Packages/">appstream (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/9-testing/baseos/x86_64/os/Packages/">baseos (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/9-testing/CERN/x86_64/Packages/">CERN (testing)</a></a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/9-testing/highavailability/x86_64/os/Packages/">highavailability (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/9-testing/codeready-builder/x86_64/os/Packages/">codeready-builder (testing)</a>&nbsp;&nbsp;
  </ul>

  <li>RHEL8:
  <ul>
    <li>for x86_64:&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/8/appstream/x86_64/os/Packages/">appstream</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/8/baseos/x86_64/os/Packages/">baseos</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/8/CERN/x86_64/Packages/">CERN</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/8/highavailability/x86_64/os/Packages/">highavailability</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/8/codeready-builder/x86_64/os/Packages/">codeready-builder</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/8-testing/appstream/x86_64/os/Packages/">appstream (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/8-testing/baseos/x86_64/os/Packages/">baseos (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/8-testing/CERN/x86_64/Packages/">CERN (testing)</a></a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/8-testing/highavailability/x86_64/os/Packages/">highavailability (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/rhel/8-testing/codeready-builder/x86_64/os/Packages/">codeready-builder (testing)</a>&nbsp;&nbsp;
  </ul>

  <li>CS9:
  <ul>
  <li>for x86_64:&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9/AppStream/x86_64/os/Packages/">AppStream</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9/BaseOS/x86_64/os/Packages/">BaseOS</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9/CERN/x86_64/Packages/">CERN</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9/Devel/x86_64/os/Packages/">Devel</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9/HighAvailability/x86_64/os/Packages/">HighAvailability</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9/PowerTools/x86_64/os/Packages/">PowerTools</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9/centosplus/x86_64/os/Packages/">centosplus</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9/cloud/x86_64/">cloud</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9/extras/x86_64/os/Packages/">extras</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9/fasttrack/x86_64/os/Packages/">fasttrack</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9/messaging/x86_64/">messaging</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9/openafs/x86_64/Packages/">openafs</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9/sclo/x86_64/">sclo</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9/virt/x86_64/">virt</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9-testing/AppStream/x86_64/os/Packages/">AppStream (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9-testing/BaseOS/x86_64/os/Packages/">BaseOS (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9-testing/CERN/x86_64/Packages/">CERN (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9-testing/Devel/x86_64/os/Packages/">Devel (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9-testing/HighAvailability/x86_64/os/Packages/">HighAvailability (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9-testing/PowerTools/x86_64/os/Packages/">PowerTools (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9-testing/centosplus/x86_64/os/Packages/">centosplus (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9-testing/cloud/x86_64/">cloud (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9-testing/extras/x86_64/os/Packages/">extras (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9-testing/fasttrack/x86_64/os/Packages/">fasttrack (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9-testing/messaging/x86_64/">messaging (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9-testing/openafs/x86_64/Packages/">openafs (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9-testing/sclo/x86_64/">sclo (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s9-testing/virt/x86_64/">virt (testing)</a>&nbsp;&nbsp;
  </ul>

  <li>CS8:
  <ul>
  <li>for x86_64:&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8/AppStream/x86_64/os/Packages/">AppStream</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8/BaseOS/x86_64/os/Packages/">BaseOS</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8/CERN/x86_64/Packages/">CERN</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8/Devel/x86_64/os/Packages/">Devel</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8/HighAvailability/x86_64/os/Packages/">HighAvailability</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8/PowerTools/x86_64/os/Packages/">PowerTools</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8/centosplus/x86_64/os/Packages/">centosplus</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8/cloud/x86_64/">cloud</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8/extras/x86_64/os/Packages/">extras</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8/fasttrack/x86_64/os/Packages/">fasttrack</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8/messaging/x86_64/">messaging</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8/openafs/x86_64/Packages/">openafs</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8/sclo/x86_64/">sclo</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8/virt/x86_64/">virt</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8-testing/AppStream/x86_64/os/Packages/">AppStream (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8-testing/BaseOS/x86_64/os/Packages/">BaseOS (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8-testing/CERN/x86_64/Packages/">CERN (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8-testing/Devel/x86_64/os/Packages/">Devel (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8-testing/HighAvailability/x86_64/os/Packages/">HighAvailability (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8-testing/PowerTools/x86_64/os/Packages/">PowerTools (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8-testing/centosplus/x86_64/os/Packages/">centosplus (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8-testing/cloud/x86_64/">cloud (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8-testing/extras/x86_64/os/Packages/">extras (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8-testing/fasttrack/x86_64/os/Packages/">fasttrack (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8-testing/messaging/x86_64/">messaging (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8-testing/openafs/x86_64/Packages/">openafs (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8-testing/sclo/x86_64/">sclo (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/s8-testing/virt/x86_64/">virt (testing)</a>&nbsp;&nbsp;
  </ul>


  <li>CC 7X:
  <ul>
  <li>for x86_64:&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/centosplus-testing/x86_64/repoview/">centosplus-testing</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/centosplus/x86_64/repoview/">centosplus</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/cern-testing/x86_64/repoview/">cern-testing</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/cern/x86_64/repoview/">cern</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/cr-testing/x86_64/repoview/">cr-testing</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/cr/x86_64/repoview/">cr</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/extras-testing/x86_64/repoview/">extras-testing</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/extras/x86_64/repoview/">extras</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/fasttrack-testing/x86_64/repoview/">fasttrack-testing</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/fasttrack/x86_64/repoview/">fasttrack</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/repoview/">os</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/rhcommon-testing/x86_64/repoview/">rhcommon-testing</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/rhcommon/x86_64/repoview/">rhcommon</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/rt-testing/x86_64/repoview/">rt-testing</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/rt/x86_64/repoview/">rt</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/updates-testing/x86_64/repoview/">updates-testing</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/7/updates/x86_64/repoview/">updates</a>&nbsp;&nbsp;<a href="elrepo/elrepo/el7/x86_64/">elrepo</a>&nbsp;&nbsp;
  </ul>

  <li>C 8X:
  <ul>
  <li>for x86_64:&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8/AppStream/x86_64/os/Packages/">AppStream</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8/BaseOS/x86_64/os/Packages/">BaseOS</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8/CERN/x86_64/Packages/">CERN</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8/Devel/x86_64/os/Packages/">Devel</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8/HighAvailability/x86_64/os/Packages/">HighAvailability</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8/PowerTools/x86_64/os/Packages/">PowerTools</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8/centosplus/x86_64/os/Packages/">centosplus</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8/cloud/x86_64/">cloud</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8/cr/x86_64/os/Packages/">cr</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8/extras/x86_64/os/Packages/">extras</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8/fasttrack/x86_64/os/Packages/">fasttrack</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8/messaging/x86_64/">messaging</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8/openafs/x86_64/Packages/">openafs</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8/sclo/">sclo</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8/virt/x86_64/">virt</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8-testing/AppStream/x86_64/os/Packages/">AppStream (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8-testing/BaseOS/x86_64/os/Packages/">BaseOS (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8-testing/CERN/x86_64/Packages/">CERN (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8-testing/Devel/x86_64/os/Packages/">Devel (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8-testing/HighAvailability/x86_64/os/Packages/">HighAvailability (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8-testing/PowerTools/x86_64/os/Packages/">PowerTools (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8-testing/centosplus/x86_64/os/Packages/">centosplus (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8-testing/cloud/x86_64/">cloud (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8-testing/cr/x86_64/os/Packages/">cr (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8-testing/extras/x86_64/os/Packages/">extras (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8-testing/fasttrack/x86_64/os/Packages/">fasttrack (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8-testing/messaging/x86_64/">messaging (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8-testing/openafs/x86_64/Packages/">openafs (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8-testing/sclo/">sclo (testing)</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/centos/8-testing/virt/x86_64/">virt (testing)</a>&nbsp;&nbsp;
  </ul>



  <li>SLC 6X (and compatible):
   <ul>
   <li>for x86 (i386):&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/slc6X/i386/yum/os/repoview/">os</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/slc6X/i386/yum/updates/repoview/">updates</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/slc6X/i386/yum/extras/repoview/">extras</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/slc6X/i386/yum/testing/repoview/">testing</a>&nbsp;&nbsp;<a href="epel/6/i386/repoview/">EPEL</a>&nbsp;&nbsp;<a href="elrepo/elrepo/el6/i386/">elrepo</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/devtoolset/slc6X/i386/RPMS/repoview">Devtoolset</a>&nbsp;&nbsp;

   <li>for x86_64:&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/slc6X/x86_64/yum/os/repoview/">os</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/slc6X/x86_64/yum/updates/repoview/">updates</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/slc6X/x86_64/yum/extras/repoview/">extras</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/slc6X/x86_64/yum/testing/repoview/">testing</a>&nbsp;&nbsp;<a href="epel/6/x86_64/repoview/">EPEL</a>&nbsp;&nbsp;<a href="elrepo/elrepo/el6/x86_64/">elrepo</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/mrg/slc6X/x86_64/RPMS/repoview/">MRG</a>&nbsp;&nbsp;<a href="http://linuxsoft.cern.ch/cern/devtoolset/slc6X/x86_64/RPMS/repoview">Devtoolset</a>&nbsp;&nbsp;
  </ul>

(Please note that all the repositories listed above are available on SLC6X/CC7/C8 via 'yum')

</ul>
<h4>Distributions/mirrors provided AS-IS without CERN support.</h4>
<ul>
<li><a href="http://linuxsoft.cern.ch/centos/">/centos/</a> CentOS (<a href="http://centos.org">http://centos.org</a>) mirror (updated four times a day)
<li><a href="http://linuxsoft.cern.ch/centos-debuginfo/">/centos-debuginfo/</a> CentOS DEBUGINFO (<a href="http://debuginfo.centos.org">http://debuginfo.centos.org</a>) mirror (updated twice per day)
<li><a href="http://linuxsoft.cern.ch/centos-altarch/">/centos-altarch/</a> CentOS Alternative architectures (<a href="http://centos.org">http://centos.org</a>) mirror (updated twice per day)
<li><a href="http://linuxsoft.cern.ch/epel/">/epel/</a> Extra Packages for Enterprise Linux (<a href="https://fedoraproject.org/wiki/EPEL">https://fedoraproject.org/wiki/EPEL</a>) mirror (updated twice per day)
<li><a href="http://linuxsoft.cern.ch/scientific/">/scientific/</a> Scientific Linux (<a href="http://scientificlinux.org">http://scientificlinux.org</a>) mirror (updated twice per day).
<li><a href="http://linuxsoft.cern.ch/fedora/">/fedora/</a> Fedora Linux (<a href="http://fedoraproject.org">http://fedoraproject.org</a>) mirror (updated twice per day).
<li><a href="http://linuxsoft.cern.ch/elrepo/">/elrepo/</a> El Repo rpm repository (<a href="http://elrepo.org">http://elrepo.org</a>) mirror (updated twice per day).
</ul>
</ul>
<h3>Mirroring</h3>
Distributions listed above are available for mirroring using the rsync protocol from the <b>rsync-linuxsoft.cern.ch</b> address

