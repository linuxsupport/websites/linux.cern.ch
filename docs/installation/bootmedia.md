# Boot Media

Using boot media (CD/DVD/USB key)

If your computer is not capable of PXE network boot you will need to prepare installation media first

# Boot Media preparation

## Download:

Download Boot ISO for (x86_64):

* [AlmaLinux 9](https://linuxsoft.cern.ch/cern/alma/9/BaseOS/x86_64/os/images/boot.iso)
* [AlmaLinux 8](https://linuxsoft.cern.ch/cern/alma/8/BaseOS/x86_64/os/images/boot.iso)
* [Red Hat Enterprise Linux 9](https://linuxsoft.cern.ch/enterprise/rhel/server/9/boot.iso)
* [Red Hat Enterprise Linux 8](https://linuxsoft.cern.ch/enterprise/rhel/server/8/boot.iso)

!!! note "Saving the iso file"
    In some browsers you may need to right-click on the media link, then choose 'Save'


## Create bootable CD:

* On Linux systems:
  * Execute the following command: `cdrecord dev=X,Y,Z boot.iso` (where **X,Y,Z** is the SCSI bus/device/LUN of your CD writer, can be seen with `cdrecord -scanbus`</i>` command) or use a graphical frontend like `xcdroast`.

* On MS Windows systems:
  * Use Nero Burning ROM or similar in **raw** mode.

## Create bootable USB drive:

* On Linux systems:
  * Execute the following command: `/bin/dd if=boot.iso of=DEVICE` (where **DEVICE** corresponds to your USB device, can be found using: `dmesg` after connecting the device)
    * *Note*: Always use the device name (ie: /dev/myusbsdz) not device partition name (ie: /dev/myusbsdz1) as argument to of= option.
    * *Note*: Make sure to choose right /dev/.. device: the dd command will destroy the content of the target .. which could be your system drive ..

* On MS Windows systems:
  * Use [dd for Windows](http://www.chrysocome.net/dd)

For detailed instruction on installation and setup of supported Linux versions please visit:

* AlmaLinux 9 [installation instructions](../almalinux/alma9/install.md).
* AlmaLinux 8 [installation instructions](../almalinux/alma8/install.md).
* Red Hat Enterprise Linux 9 [installation instructions](../rhel/rhel9/install.md).
* Red Hat Enterprise Linux 8 [installation instructions](../rhel/rhel8/install.md).
