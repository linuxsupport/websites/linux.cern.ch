# AIMS2 client

The AIMS2 client allows users to interact with the Linux Automated Installation Management System (AIMS). Using the client users can register/de-register devices for installation, register a Kickstart file for their device, provide Kernel append options and interact with the PXE boot media library.

As a side note, if you plan on performing physical automated installations in the datacenter, refer to <https://configdocs.web.cern.ch/nodes/phyinstall.html>.

## Install the client

The client is available through YUM for all supported CERN Linux distributions. Check <http://linuxsoft.cern.ch/internal/repos/linuxsupport{7/8al/8el/9al/9el}-stable/> for latest versions.

It is recommended to use [`aiadm.cern.ch`](https://cern.service-now.com/service-portal/CERN?id=functional_element&name=aiadm) as it includes many other related tools and will always keep the latest compatible version.

```bash
dnf install epel-release
dnf --repofrompath=linuxsupport,http://linuxsoft.cern.ch/internal/repos/linuxsupport9al-stable/x86_64/os install aims2client
```

## Help

To display a short help page, execute the following.

```bash
aims2client --help
```

For more detailed information, execute either of the following.

```bash
man aims2client
perldoc aims2client
```

## Authentication and Authorisation

### Authentication

To use the installation service you must present valid Kerberos 5 credentials. To see if you have existing credentials, run `klist`. If you have no Kerberos credentials, then you should run kinit.

If you have not done so already, please install the Perl Kerberos 5 libraries and run `kinit` again.

If you experience problems with Kerberos, please contact <Linux.Support@CERN.CH>.

### Authorisation

AIMS2 introduces a new layer of authorisation around hosts/devices and PXE boot media. The following sections explain how this is implemented.

#### Permission to install a device

It is necessary to check if the user has permission to interact with a device to try and prevent accidental/malicious installations. Only LANDB responsible user, main user or manager of the device will be able to operate with a given node. Some administration egroups are also allowed.

In summary, if the user is listed as the device's `MAIN USER` or `RESPONSIBLE`, permission is granted for installation.

### Interacting with PXE boot media

The second layer of authorisation for AIMS2 is centered around managing the PXE media library.

To upload your own images to aims, you need to seek permission from <Linux.Support@CERN.CH> first.

When an image is uploaded, it is owned by you. This means nobody else can remove your image except you, the given egroup and the Linux administration team.

You can share/limit the user of PXE image and kernel via the use of egroups, using the `--egroups` options.

## How to use the client

The following sections should introduce you to the commands to be used with the client.

### Examples

To register the device YOURHOSTNAME:

```bash
aims2client addhost yourhostname
```

To register the device YOURHOSTNAME with a Kickstart file:

```bash
aims2client addhost yourhostname --kickstart /afs/cern.ch/user/u/username/yourhostname.ks
```

To provide Anaconda/Kernel append options for your device:

```bash
aims2client addhost yourhostname --kopts "ksdevice=bootif ramdisk_size=36000"
```

To display information about YOURHOSTNAME:

```bash
aims2client showhost yourhostname
```

To display information about ALL registered yourhostname* nodes:

```bash
aims2client showhost yourhostname*
```

To remove YOURHOSTNAME:

```bash
aims2client remhost yourhostname
```

To show the Kickstart file for YOURHOSTNAME:

```bash
aims2client showks yourhostname
```

To sync the Kickstart file for YOURHOSTNAME:

```bash
aims2client updateks yourhostname
```

To update the Kickstart file for YOURHOSTNAME:

```bash
aims2client updateks yourhostname --kickstart /new/kickstart/path
```

To enable yourhostname for installation with ALMA9 x86_64:

```bash
aims2client pxeon yourhostname ALMA9_X86_64
```

To display information about ALMA9_X86_64:

```bash
aims2client showimage ALMA9_X86_64 --full
```

To see what else is available for installation:

```bash
aims2client showimage all
```

### Registering a device and/or Kickstart

```bash
aims2client addhost HOSTNAME [--kickstart=S] [--kopts=S] [--pxe] [--name=S]
```

To register your host for installation, you must provide the HOSTNAME of the device as given in the Network Database. AIMS2 will then register each interface (hardware address) that is listed in the network database for that HOSTNAME. Only interfaces that are registered should be able to obtain DHCP leases on the CERN network. Should you provide the name of an INTERFACE ALIAS, only the hardware address(es) for that interface will be registered.

To provide a Kickstart file for your installation you should use the `--kickstart` PATH option, where PATH is either a `http://` source, somewhere readable (by `linuxsoft.cern.ch`) within `/afs/cern.ch/`, − (STDIN) or a local file. The following examples are valid PATHs:

*../example.ks
* ../../example.ks
* ./example.ks
* example.ks
* /afs/cern.ch/project/foo/sample.ks
* http://www.foo.com/sample.ks*

#### Kernel append options

Some device hardware may require you to pass additional options to Anaconda or the Kernel at boot. You can specify these options using the −−kopts option.

A useful example of using sing the `--kopts` option is when your device has multiple network interfaces but you are not sure which one has a cable connected or which one is being raised first. During an the installation of a RedHat based image, if Anaconda is unable to decide which interface to use for the installation, you can provide the option `ksdevice=eth0` which instructs Anaconda to use interface eth0 for its installation. Other options for this command include `bootif` which selects the interface used to boot the vmlinuz/initrd, link which is the first interface found with a link established or the MAC address of the interface. Not providing this option to a multiple interface device will cause Anaconda to stop until the user selects the correct interface to use, thus your installation will not be as automated as you would have hoped.

You can also use Anaconda options to redirect the installation output, such as to a serial console. For example, the following options could be used.

```bash
console=ttyS0,9600 console=ttyS1,9600 console=tty0
```

The following links provide more information on the available Anaconda options.

* [Red Hat Enterprise Linux 9 Kickstart Syntax Reference (applicable to Alma9 as well)](https://docs.redhat.com/es/documentation/red_hat_enterprise_linux/9/html-single/performing_an_advanced_rhel_9_installation/index#kickstart-commands-and-options-reference_installing-rhel-as-an-experienced-user)
* [Red Hat Enterprise Linux 8 Kickstart Syntax Reference (applicable to Alma8 as well)](https://docs.redhat.com/it/documentation/red_hat_enterprise_linux/8/html-single/performing_an_advanced_rhel_8_installation/performing_an_advanced_rhel_8_installation#kickstart-commands-and-options-reference_installing-rhel-as-an-experienced-user)

### Enable PXE at registration

When executing the addhost command, if you provide the `--pxe` and `--name=NAME` options your device will be enabled for installation using the PXE target `NAME`. If you do not provide this options the PXE state of your device will not change. To enable the device, see the `pxeon` command later on this page.

#### Multiple hosts at once.

The AIMS2 client supports the ParseHost routine available in other CERN FIO tools. Consider the following example

```bash
aims2client addhost yourhostname[204-209] --kickstart yourhostname_example.ks --kopts "ksdevice=bootif cmdline"
```

The client will parse `yourhostname[204-209]` and the result will be devices `yourhostname204` through to `yourhostname209` being registered with same Kickstart file and Kernel append options.

### Removing a device

```bash
aims2client remhost --hostname=yourhostname
```

To remove (de-register) a host from the installation database you should use the `remhost` command, provide the HOSTNAME of the device.

### Displaying device information

```bash
aims2client showhost HOSTNAME
```

To show information about a particular host, you can use the showhost command, providing the HOSTNAME of the device.

The server will return the results for each interface it has registered for that host. The information contained in the reply includes the interface address, the PXE status of the host, the PXE image to be provided to the host (if defined), Anaconda options which should be passed at work and some time stamp information to create an audit trail.

It should be possible to use the client to build an exact audit trail of the host, from when it was registered, booted, downloaded it’s Kickstart and installed successfully.

`HOSTNAME` supports basic wildcards. Consider the following example

```bash
showhost --hostname yourhostname67*
```

!!! warning
    Please note, in some terminals it is necessary to escape * with either \* or "*"

### Interacting with your Kickstart

#### Displaying the Kickstart file

```bash
aims2client showks HOSTNAME
```

The `showks` command will print to STDOUT the Kickstart file, if register, for `HOSTNAME`

Also displayed is where the Kickstart file was sourced from, who by and when.

#### Updating the Kickstart file

```bash
aims2client updateks HOSTNAME --kickstart [SOURCE]
```

If only `HOSTNAME` is provided, AIMS will update the Kickstart file from the previously given source. If the Kickstart was previously uploaded through the client and not linked too, the Kickstart cannot be updated through this method unless `SOURCE` is provided. The same rule applies if no Kickstart has been defined.

If `SOURCE` is provided, the Kickstart file is updated from `SOURCE`.

Please refer to to the addhost command for valid SOURCE options.

#### Adding new PXE boot media

Please contact Linux.Support for permission to interact with PXE boot media.

```bash
addimage --name=S --arch=S --description=S --vmlinuz=S [--initrd=S] [--kopts=S] [--egroups=S]
```

The option `--name=NAME` is for the name (alias) of your PXE boot media. When defining a name for you media, try to use the distro-arch combination and try to use a name which is meaningful to other users.

Options `--initrd=path` and `--vmlinuz=path` refer to the file system locations of your image and kernel respectively. These locations must be readable by the AIMS2 servers. Some users choose to use their AFS public/scratch directories.

#### Kernel append options

If you know that your home-brewed image/kernel combo requires certain Kernel append options at boot then you can use provide these using the --kopts option.

These options will then be used when installing this media. An example is the providing of `noipv6`. It's OK to duplicate options on images and hosts, as AIMS will weed out the duplicate entries for you. AIMS will not however check that your options are syntactically correct. It is presumed you have read the appropriate documentation on your Kernel and know what you are doing to some degree. A common example is:

```bash
--kopts="ramdisk_size=36000 noipv6 ksdevice=link"
```

#### Controlling deployment with e-groups

If you wish to limit the deployment of your image to a select group of users, you can provide the --egroups option where your e-groups should be comma separated. For example:

```bash
--egroups="egroup-name1,egroup-name2,egroup-name3"
```

These e-groups should then be defined through the CRA interface. Only yourself and those inside these groups will be able to deploy devices using this media. If not e-groups are defined, your PXE media will be available for user by any user.

### Removing PXE boot media

```bash
aims2client remimage NAME
```

To remove PXE boot media from AIMS, using the `remimage` command providing the `NAME` of the media.

You can only remove PXE boot media if you are the owner or a member of Linux Support.

### Display information about PXE boot media.

```bash
aims2client showimage NAME
```

To display information about PXE boot media, `NAME `is the name of the PXE boot media. The results returned are ordered as follows:

```bash
name, architecture, kernel append options, description
```

To display more information about PXE boot media add the `--full` or `--all` options as per the example

```bash
aims2client showimage ALMA9_X86_64 --full
```

The additional information displayed, extending the above is:

```bash
source of Kernel, md5sum, size (kb), source of initrd, md5sum, size (kb), e-groups, owner, upload time-stamp
```

`NAME` can also be provided with basic wildcards. Consider the example below.

```bash
showimage CS8*
```

This will return PXE boot media whose name begings with `CS8`

!!! warning
    Please note, in some terminals it is necessary to escape * with either \* or "*"

### Enabling PXE for a device

```bash
aims2client pxeon HOSTNAME NAME
```

To enable PXE boot for a device use the `pxeon` command. This command takes two options, the `HOSTNAME` you are enabling and the `NAME` of the PXE boot media you wish to use.

If `noexpiry` flag is specified the pxeboot will not be cleaned up 24 hours after enabling. Default behaviour is to automatically disable pxeboot 24 hours after registration.
Boot type option may be specified as `pxe|bios` (default, PXE/bios boot),`uefi` (PXE/UEFI boot) or `arm64` (PXE/UEFI boot for `aarch64`, `ARM64` platform).

If you want to check the generated configuration for your enabled interface, please refer to:

* **UEFI x86_64**: <http://aims.cern.ch/aims/config/uefi/grub.cfg-01-MACADDRESS>.
* **UEFI ARM64**: <http://aims.cern.ch/aims/config/arm64/grub.cfg-01-MACADDRESS>.
* **BIOS**: <http://aims.cern.ch/aims/config/bios/01-MACADDRESS>.

### Displaying a list of PXE boot targets

To display a list of PXE boot targets available for installation please use the `showimage` command. The following example will show all the PXE boot targets available.

```bash
aims2client showimage \*
```

If you just wanted to see what RHEL8 targets are available you would execute

```bash
aims2client showimage RHEL9*
```

### Disabling PXE for a device

```bash
aims2client pxeoff HOSTNAME
```

To disable PXE boot for a device, use the `pxeoff` command providing the `HOSTNAME` of the device. Disabling PXE for your device will set your device to localboot, thus no PXE menu will be available until you remove the device from AIMS or re-add it using the `addhost` command.

**NOTE: All registered systems will be set to localboot after 24 hours and deleted after 60 days.**

## Misc. Options

### Displaying verbose

Providing the `-v(erbose)` option will instruct the client to provide verbose to STDOUT. Verbose is disabled by default.

### Using a different server

(Optional) To communicate with a particular server, for example the testing server, the `--server` option is available. For example:

```bash
aims2client --server=aimstest.cern.ch addhost yourhost
```

### Using BIOS Legacy workaround

Please check [Legacy workaround documentation for some specific cases where the usual BIOS workflow is not enough.](/installation/aims/aims2lgcyworkaround).
