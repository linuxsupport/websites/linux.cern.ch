stages:
  - build-static-content
  - start-okd-build

# https://docs.gitlab.com/ee/ci/yaml/workflow.html#switch-between-branch-pipelines-and-merge-request-pipelines
# The following example is for a project that runs branch and merge request pipelines only, but does not run pipelines for any other case. It runs:
# Branch pipelines when a merge request is not open for the branch.
# Merge request pipelines when a merge request is open for the branch.
workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  # If you update these, you may need to update the theme customisations
  MKDOCS_VERSION: '1.4.3'
  MATERIAL_VERSION: '9.1.19'
  I18NSTATIC_VERSION: '0.56'
  OKD_SERVER: https://api.paas.okd.cern.ch
  RELEASE_NAME: mkdocs

.prod_variables: &prod_variables
  PROJECT: linux-paas
  LEGACY_URL: linux.cern.ch
  PAAS_URL: linux.web.cern.ch
  OKD_TOKEN: $OKD_TOKEN_LINUXWEB_PROD

.dev_variables: &dev_variables
  PROJECT: linux-qa-paas
  LEGACY_URL: linux-qa.cern.ch
  PAAS_URL: linux-qa.web.cern.ch
  OKD_TOKEN: $OKD_TOKEN_LINUXWEB_QA

.common_functions: &common_functions |
  if [[ -z "${OKD_TOKEN}" || -z "${PROJECT}" ]]; then echo "Please make sure that OKD_TOKEN and PROJECT variables are set"; exit 1;fi
  # OKD_TOKEN must be created as one of the project CI/CD variables
  # This variable should hold a token of a ServiceAccount allowed to edit the OKD project (specified by $PROJECT variable)
  oc login --token="${OKD_TOKEN}" --server="${OKD_SERVER}"
  oc project "${PROJECT}"
  helm template "${RELEASE_NAME}" ./chart -f ./chart/values-override.yaml --set route.host=${PAAS_URL} | oc apply --prune-whitelist route.openshift.io/v1/Route --prune -l "app.kubernetes.io/instance=${RELEASE_NAME}" -f -
  oc start-build "${RELEASE_NAME}-artifacts" --from-dir=public/

spellcheck:
  # We don't want to slow things down if we can avoid it
  stage: build-static-content
  script:
    - dnf install -y python3-pip
    - pip install codespell
    - >
      codespell --ignore-words=.ignore_words
      --skip="docs/docs/LXCERT,docs/mrg,docs/rhel/obsolete,docs/rhel/releasenotes"
      docs/

Build static site content:
  image: registry.cern.ch/docker.io/library/python:3.11-alpine
  stage: build-static-content
  script:
    - apk add --no-cache git subversion
    - pip install --upgrade pip setuptools wheel
    - pip install mkdocs==${MKDOCS_VERSION} mkdocs-material==${MATERIAL_VERSION} mkdocs-static-i18n==${I18NSTATIC_VERSION}
    - if [[ -f requirements.txt ]]; then pip install -r requirements.txt; fi
    - >
      echo "repo_name: \"$CI_PROJECT_NAME - $CI_COMMIT_SHORT_SHA\"" >> mkdocs.yml
    - mkdocs build -d public
    - mkdir -p nginx-default-cfg
    - echo "port_in_redirect off;" >> nginx-default-cfg/port_in_redirect.conf
    - if [[ -f nginx.conf ]]; then cp nginx.conf public; fi
    - if [[ -d nginx-default-cfg ]]; then cp -r nginx-default-cfg/ public; fi
    - if [[ -d nginx-cfg ]]; then cp -r nginx-cfg/ public; fi
  artifacts:
    public: false
    paths:
      - public
    expire_in: 1m
  interruptible: true

check_links:
  stage: build-static-content
  image: registry.cern.ch/docker.io/becheran/mlc:0.16.1
  script:
    - mlc . | tee output.log | grep -v '\[\(WARN\|Warn\| OK \)\]'
  # For now...
  when: manual
  artifacts:
    paths:
      - output.log
    when: always

Deploy to OKD4 test:
  stage: start-okd-build
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:latest
  script:
    - *common_functions
  rules:
    - if: '$CI_COMMIT_BRANCH != "master"'
      when: always
  variables:
    <<: *dev_variables
  interruptible: true

Deploy to OKD4 prod:
  stage: start-okd-build
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:latest
  script:
    - *common_functions
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: always
  variables:
    <<: *prod_variables
  interruptible: true

# We make linux-qa have the latest pushed changes to master as well so they are synchronized
Sync to OKD4 test:
  stage: start-okd-build
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:latest
  script:
    - *common_functions
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: always
  variables:
    <<: *dev_variables
  interruptible: true
