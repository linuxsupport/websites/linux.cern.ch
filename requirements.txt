# Our custom plugin to autogenerate latest updates
git+https://gitlab.cern.ch/linuxsupport/websites/updates-autogeneration-plugin.git@master#egg=updates-autogeneration-plugin
# In case we want to exclude files that are not relevant to the site
mkdocs-exclude
mkdocs-exclude-search
mkdocs-minify-plugin
lunr
