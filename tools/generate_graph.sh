#!/bin/bash

git_root=`git rev-parse --show-toplevel 2>/dev/null`
startdate=19700101
enddate=`date +%Y%m%d`
excludenum=20000
colours=('red' 'blue' 'green' 'brown' 'orange' 'purple' 'yellow' 'cyan')
rm -f *dat

usage() {
cat << EOF
usage: $0 -t -p

This script creates a gnuplot by parsing the markdown from within 'docs'

OPTIONS:

  -t        include testing updates
  -p        include prod updates
  -d        distributions (cc7,c8,cs8). default is 'all'
  -s        filter start date (YYYYMMDD). default is to not filter
  -e        filter end date (YYYMMDD). default is to not filter
  -x        exclude number of packages over this figure
  -h        display this help
EOF
exit
}

while getopts "tphd:s:e:x:" option
do
  case $option in
    h)
      usage
      ;;
    t)
      testing=1
      ;;
    p)
      production=1
      ;;
    d)
      distributions=$OPTARG
      ;;
    s)
      startdate=$OPTARG
      ;;
    e)
      enddate=$OPTARG
      ;;
    x)
      excludenum=$OPTARG
  esac
done

if [[ "$distributions" == "" ]] || [[ "$distributions" == "all" ]]; then
  distributions=""
  tmp=`ls -1d $git_root/docs/updates/*/`
  for t in $tmp
  do
    distributions="$distributions `basename $t`"
  done
else
  distributions=`echo $distributions | sed 's/,/ /'`
fi

if [[ $testing -ne 1 ]] && [[ $production -ne 1 ]]; then
  usage
elif [[ $testing -eq 1 ]] && [[ $production -eq 1 ]]; then
  releases="prod test"
elif [[ $testing -eq 1 ]]; then
  releases="test"
else
  releases="prod"
fi

for dist in $distributions
do
  for release in $releases
  do
    rm -f $dist-$release.dat
    tmpcandidates=`ls -1 $git_root/docs/updates/$dist/$release/*`
    candidates=''
    for filename in $tmpcandidates
    do
      tmp=`basename $filename`
      if [[ ${tmp%.md} -gt $startdate ]] && [[ ${tmp%.md} -lt $enddate ]]; then
        candidates="$candidates $filename"
      fi
    done
    for filename in $candidates
    do
      date=`basename $git_root/docs/updates/$dist/$release/$filename .md`
      if [ "$dist" == "cc7" ]; then
        packages=`egrep -v "#|^$|Package |-------" $filename |wc -l`
	if [ $packages -lt $excludenum ]; then
          echo "$date $packages" >> $dist-$release.dat
	fi
      else
        appstream=`sed -n '/### AppStream x86_64/,/###/p' $filename | egrep -v "#|^$|Package |-------" |wc -l`
        baseos=`sed -n '/### BaseOS x86_64/,/###/p' $filename | egrep -v "#|^$|Package |-------" |wc -l`
        if [ $appstream -ne 0 ] || [ $baseos -ne 0 ]; then
	  packages=`echo "$baseos+$appstream" | bc`
	  if [ $packages -lt $excludenum ]; then
            echo "$date $packages" >> $dist-$release.dat
	  fi
        fi
      fi
    done
  done
  echo -n "Total packages for $dist-$release during the period $startdate -> $enddate: "
  awk '{sum+=$2;} END{print sum;}' $dist-$release.dat
done


cat << EOF > plot.gp
set title 'Distribution updates'
set ylabel 'Number of Packages'
set xlabel 'Date'
set xdata time
set xtics rotate by -45
set timefmt '%Y%m%d'
set format x '%Y%m%d'
set key top left autotitle columnheader
set grid
set autoscale
EOF
colourindex=0
for plot in `ls -1 *dat`
do
  sort -o $plot $plot
  colour=${colours[$colourindex]}
  let "colourindex+=1"
  grep -q plot plot.gp
  if [ $? -eq 0 ]; then
    echo -n ", '$plot' using 1:2 lt rgb '$colour' w l title '${plot%.dat}'" >> plot.gp
  else
    echo -n "plot '$plot' using 1:2 lt rgb '$colour' w l title '${plot%.dat}'" >> plot.gp
  fi
done

gnuplot -persist plot.gp
